package util

object MiscellaneousImplicits {
  implicit class SeqExtensions[A](seq: Seq[A]) {
    def groupByFirst[K, V](implicit ev: A <:< (K, V)): Map[K, Seq[V]] =
      seq.groupBy(_._1).mapValues(_.map(_._2))
  }

  implicit class MapExtensions[K, V](m: Map[K, V]) {
    def merge(other: Map[K, V], simpleMergeFun: (V, V) => V): Map[K, V] =
      merge(other, (k, v1, v2) => simpleMergeFun(v1, v2))

    def merge(
        other: Map[K, V], mergeFun: (K, V, V) => V = ((k, v, otherV) => otherV)
    ): Map[K, V] = {
      val commonKeys = m.keySet.intersect(other.keySet)
      val common = commonKeys.map(k => (k -> mergeFun(k, m(k), other(k))))

      val nonCommon = (m -- commonKeys) ++ (other -- commonKeys)

      nonCommon ++ common
    }

    def filterValues(p: V => Boolean): Map[K, V] = m.filter {
      case (_, value) => p(value)
    }
  }
}
