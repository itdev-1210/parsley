package object util {
  def OptionGetPf[A]: PartialFunction[Option[A], A] = {
    case Some(v) => v
  }

}
