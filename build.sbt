import play.core.PlayVersion

import com.typesafe.config.ConfigFactory
import play.sbt.PlayInternalKeys

import java.nio.file.Files

import scala.sys.process.ProcessLogger

name := """parsley"""
version := "1.0-SNAPSHOT"
cancelable in Global := true

// developer quality-of-life stuff
clippyColorsEnabled := true

val pgDriver = RootProject(uri("git://github.com/parsleysoftware/parsley-pg-driver#057883c"))

lazy val util = project
lazy val codegen = project
    .dependsOn(pgDriver)
    .dependsOn(util)

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)
    .dependsOn(pgDriver)
    .dependsOn(util)

(scalaVersion in ThisBuild) := "2.12.6"
(scalacOptions in ThisBuild) ++= Seq("-Xmax-classfile-name", "130")

libraryDependencies ++= Seq(
  ehcache,
  ws,
  specs2 % Test,
  guice,
  "org.specs2" %% "specs2-matcher-extra" % "3.9.5" % Test,

  compilerPlugin("com.github.ghik" %% "silencer-plugin" % "1.3.2"),
  "com.github.ghik" %% "silencer-lib" % "1.3.2" % Provided
)

// Uncomment this line to debug properly in unit tests
// fork in Test := false

resolvers ++= Seq(
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  "Atlassian Releases" at "https://maven.atlassian.com/public/"
)


// Configuration of the LESS compiler - by default it just cares about main.less
includeFilter in (Assets, LessKeys.less) := Seq(
  "app/main.less",
  "app/print.less",
  "plan/plan.less",
  "auth/login.less"
).map { relpath =>
  new SimpleFileFilter({ file => file
      .relativeTo((sourceDirectory in Assets).value) // Option[File]
      .exists(_.toString == relpath)
  })
}.reduce[FileFilter](_ || _)

val dbDependencies = Seq(
  // necessary to get all the right config stuff
  "com.iheart" %% "ficus" % "1.4.3",
  // DB stuff
  "com.typesafe.play" %% "play-slick"  % "3.0.3",
  "com.typesafe.play" %% "play-slick-evolutions"  % "3.0.3",
  "com.typesafe.slick" %% "slick-codegen" % "3.2.3",
)

(libraryDependencies in codegen) ++= dbDependencies ++ Seq(
  // still require a subset of the full Play libraries
  "com.typesafe.play" %% "play-logback"  % PlayVersion.current,
  "com.typesafe.play" %% "play-jdbc-evolutions" % PlayVersion.current,
)

libraryDependencies ++= dbDependencies ++ Seq(
  "net.codingwell" %% "scala-guice" % "4.2.1",

  // Silhouette and friends
  "com.mohiva" %% "play-silhouette" % "5.0.7",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "5.0.7",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "5.0.7",
  "com.mohiva" %% "play-silhouette-persistence" % "5.0.7",
  "com.mohiva" %% "play-silhouette-testkit" % "5.0.7" % Test,

  // Stripe
  "com.stripe" % "stripe-java" % "6.12.0",

  // SendGrid
  "com.sendgrid" % "sendgrid-java" % "4.2.1",

  // AWS S3
  "net.kaliber" %% "play-s3" % "9.0.0",

  "com.typesafe.play" %% "play-json" % "2.6.9",

  "org.postgresql" % "postgresql" % "42.2.4",

  // image processing
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.8"
)

/*
 * Web pipeline
 */
lazy val develWebpackTargetDirectory = settingKey[File]("the destination to which webpack should live-compile javascript for development purposes")

develWebpackTargetDirectory := target.value / "dev-webpack"

PlayKeys.playRunHooks += Webpack.develRunHook(streams.value.log)

// production
lazy val prodWebpackTargetDirectory = settingKey[File]("the location for webpack to stick production, minified versions of JS files")

lazy val prodWebpackMappings = taskKey[Seq[(File, String)]]("the webpack artifacts built for production")

prodWebpackTargetDirectory := target.value / "prod-webpack"

// not just dropping these into webPublic because I don't want any chance of these getting in the devel
// instance
prodWebpackMappings := {
  val targetDir = prodWebpackTargetDirectory.value

  // with webpack selecting cache-busting names for each file, we can't rely on
  // it to blow away old ones; so delete them manually
  targetDir.**(AllPassFilter).get.map(_.delete())

  val logger = streams.value.log
  val procLogger = ProcessLogger(s => logger.info(s))
  if (Webpack.makeBuildProcess(Some(targetDir), "production", logger).!(procLogger) != 0)
    throw new Exception("failed to build javascript")

  val webpackFiles = PathFinder(targetDir).**(AllPassFilter)
  val directOutputFiles = webpackFiles.pair(
    { f =>
      if (f.isFile)
        f.relativeTo(targetDir).map(_.getPath)
      else None
    },
    errorIfNone = false
  )

  val noHashPrefixCopies = directOutputFiles.flatMap { case (f, _) =>
    if (f.isFile && f.getName.contains('-')) { // format for tagged is [hash]-*.bundle.js
      import scala.collection.JavaConverters._
      val Seq(hash, nameSansHash) = f.getName.split("-").toSeq

      val plainNamedFile = new File(f.getParentFile, nameSansHash)
      Files.copy(f.toPath, plainNamedFile.toPath)

      val hashFile = new File(f.getParentFile, nameSansHash + ".md5")
      Files.write(hashFile.toPath, hash.getBytes)

      Seq(hashFile, plainNamedFile).flatMap { newFile =>
        newFile.relativeTo(targetDir).map { f =>
          (newFile, f.getPath)
        }.toSeq
      }
    } else Seq()
  }

  directOutputFiles ++ noHashPrefixCopies
}

// The following is copied from SbtWeb.scala, but adding prodWebpackMappings
// to the pipelineStages argument
(WebKeys.pipeline in packageBin) := WebKeys.allPipelineStages.value(
  (mappings in Assets).value ++ prodWebpackMappings.value
)

(WebKeys.pipeline in packageBin) := SbtWeb.deduplicateMappings(
  (WebKeys.pipeline in packageBin).value, WebKeys.deduplicators.value
)
// end duplicated code from SbtWeb.scala

// versioning/digests handled by webpack
excludeFilter in digest := "*.js" || "*.js.map"
pipelineStages := Seq(digest, gzip)

/*
 * packaging stuff
 */
daemonUser in Docker := "app"

defaultLinuxInstallLocation in Docker := "/opt/parsley"

// General SBT performance
updateOptions := updateOptions.value.withCachedResolution(true)

/*
 *NewRelic
 */

// run newrelic only in a packaged version of the app, not in people's local development version
// the -J-someoption syntax is required to force the sbt-native-packager startup script to pass the option
// to the JVM instead of to the application
javaOptions in Universal ++= Seq(
  "-J-javaagent:lib/newrelic.jar",
  "-J-XX:MaxHeapSize=600M"
)

// package the newrelic config file in the same directory as the jar
(includeFilter in (Compile, unmanagedJars)) :=
    (includeFilter in (Compile, unmanagedJars)).value || "*.yml" || "nrcerts"

val slickCodegen = taskKey[Seq[File]]("generate models/Table.scala")

slickCodegen := {

  val outputDir = (scalaSource in Compile).value
  val outputPackage = "models"
  val classpath = Attributed.data((fullClasspath in (codegen, Compile)).value) ++ (unmanagedResourceDirectories in Compile).value

  (runner in codegen).value.run(
    "CustomSlickCodeGenerator", classpath,
    Seq(outputDir.getPath, outputPackage),
    streams.value.log
  ).get // this is a Try; we want to make sure that a failure crashes early

  // reconstruct a reference to the file
  val leafOutputDir = outputDir / outputPackage.replace(".", "/")
  Seq(leafOutputDir / "Tables.scala")
}

val runEvolutions = taskKey[Unit]("run database migrations/evolutions")

runEvolutions := {
  val classpath = Attributed.data((fullClasspath in (codegen, Compile)).value) ++ (unmanagedResourceDirectories in Compile).value
  (runner in codegen).value.run(
    "RunEvolutions", classpath,
    Seq(
      ((resourceDirectory in Compile).value / "application.conf").getAbsolutePath,
      baseDirectory.value.getAbsolutePath,
    ),
    streams.value.log
  )
}
