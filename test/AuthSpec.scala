import util.{FakeAuthAppScope, FakeDBScope}

import com.mohiva.play.silhouette.test._
import com.mohiva.play.silhouette.api.LoginInfo
import fixtures.AuthFixtures

import play.api.Application

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import play.api.libs.json._
import play.api.test._
import play.api.test.Helpers._

import org.specs2.matcher.{Expectable, MatchResult, Matcher}
import play.api.i18n.{Messages, MessagesApi}
import play.api.mvc.{Cookie, Result}

import scala.concurrent.Future

@RunWith(classOf[JUnitRunner])
class AuthSpec extends Specification {

  implicit val LoginInfoFormat = Json.format[LoginInfo]
  abstract class WithPasswordAuth extends FakeDBScope(this.getClass, Seq(AuthFixtures))

  "Secured application actions" should {
    "redirect unknown users to the signup page" in new WithApplication {
      val Some(resp) = route(app, FakeRequest(GET, "/app"))

      status(resp) must equalTo(SEE_OTHER)
      redirectLocation(resp).get must equalTo("/auth/signup?redirect=%2Fapp")
    }

    "recognize a logged-in user" in new FakeAuthAppScope {
      val Some(resp) = route(app, FakeRequest(GET, "/logintest")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentAsJson(resp).as[LoginInfo] must equalTo(LoginInfo("mybook.com", "test@example.com"))
    }
  }

  "Secured API actions" should {
    "401 on unknown users" in new WithApplication {
      val Some(resp) = route(app, FakeRequest(GET, "/api/me"))

      status(resp) must equalTo(UNAUTHORIZED)
    }

    "recognize a logged-in user" in new FakeDBScope(this.getClass, Seq(AuthFixtures)) {

      val Some(resp) = route(app, FakeRequest(GET, "/api/me")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")
    }
  }

  "/auth/logout endpoint" should {
    "log out users" in new FakeAuthAppScope {
      val Some(resp) = route(app, FakeRequest(GET, "/auth/logout")
          .withAuthenticator(fakeLoginInfos.head)
      )

      // TODO: Make this test end result, and not just implementation
      // clearing of id cookie
      cookies(resp).exists(p => p.name == "id" && p.value == "") must beTrue
    }
  }

  def haveSignedInWith(app: Application, loginResult: Future[Result]) = new Matcher[String] {
    import Matcher.result
    override def apply[S <: String](t: Expectable[S]): MatchResult[S] = {
      val maybeResult = for {
        authCookie <- cookies(loginResult).find(_.name == "id")
        authTestResp <- route(app, FakeRequest(GET, "/logintest")
            .withCookies(authCookie))
      } yield result(status(authTestResp) == OK &&
          contentAsJson(authTestResp).as[LoginInfo].providerKey == t.value,
        s"logged-in user is ${t.value}", s"logged-in user is not ${t.value}", t)

      maybeResult.getOrElse(failure("response did not log in any user", t))

    }
  }

  "Password login" should {
    "accept logins with correct passwords" in new WithPasswordAuth {
      val Some(resp) = route(app, FakeRequest(POST, "/auth/login").withJsonBody(Json.obj(
        "email" -> "other@example.com",
        "password" -> "password"
      )))

      status(resp) must equalTo(OK)

      "other@example.com" must haveSignedInWith(app, resp)
    }

    "reject logins for existing users with incorrect passwords" in new WithPasswordAuth {
      val Some(resp) = route(app, FakeRequest(POST, "/auth/login").withJsonBody(Json.obj(
        "email" -> "other@example.com",
        "password" -> "bogus"
      )))

      status(resp) must equalTo(BAD_REQUEST)

      "other@example.com" must not(haveSignedInWith(app, resp))
    }

    "reject logins for nonexistent users" in new WithPasswordAuth {
      val Some(resp) = route(app, FakeRequest(POST, "/auth/login").withJsonBody(Json.obj(
        "email" -> "nonexistent@example.com",
        "password" -> "bogus"
      )))

      status(resp) must equalTo(BAD_REQUEST)

      "nonexistent@example.com" must not(haveSignedInWith(app, resp))
    }
  }

  "Password sign-up" should {
    "accept user creation and leave the new user logged-in" in new WithPasswordAuth {
      val Some(resp) = route(app, FakeRequest(POST, "/auth/signup").withJsonBody(Json.obj(
        "email" -> "newuser@example.com",
        "password" -> "Look I'm New!",
        "firstName" -> "Newbie"
      )))

      status(resp) must equalTo(OK)

      "newuser@example.com" must haveSignedInWith(app, resp)
    }
  }

}
