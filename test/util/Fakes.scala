package util

import java.time.Instant

import javax.inject.Inject
import akka.stream.Materializer
import models._
import models.Tables._
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import com.mohiva.play.silhouette.password.BCryptPasswordHasher
import com.mohiva.play.silhouette.test.{FakeCookieAuthenticatorService, FakeEnvironment}
import controllers.Env
import net.codingwell.scalaguice.ScalaModule
import play.api.http.HttpFilters
import play.api.i18n.MessagesApi
import play.api.inject.guice.{GuiceApplicationBuilder, GuiceApplicationLoader}
import play.api.mvc._
import play.api.test.WithApplicationLoader
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import com.mohiva.play.silhouette.api.services.AuthenticatorService
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator

import scala.concurrent.Future

class WithFakedApplication(fakes: FakeAppBuilder = new FakeAppBuilder())
    extends WithApplicationLoader(new GuiceApplicationLoader(fakes.builder))

class FakeAppBuilder(val builder: GuiceApplicationBuilder = new GuiceApplicationBuilder()) {

  def copy(builder: GuiceApplicationBuilder = builder) =
    new FakeAppBuilder(builder)

  def build = builder.build()

  def withFakeDB(rootUrl: String, dbName: String): FakeAppBuilder = {
    // use the regular dev pg instance, but with a fake DB name?
    val fakeDbUrl = "slick.dbs.default.db.url" -> (rootUrl + dbName)
    val mod = new ScalaModule {
      override def configure() = {
        bind[AuthenticatorService[CookieAuthenticator]].toInstance(FakeCookieAuthenticatorService())
      }
    }
    copy(builder = builder.configure(fakeDbUrl).overrides(mod))
  }

  def withFakeAuth: FakeAppBuilder = {
    val mod = new ScalaModule with FakeLogins {
      override def configure() = {
        bind[Environment[Env]].toInstance(env)
      }
    }
    copy(builder = builder.overrides(mod))
  }

  def withAlwaysAuth: FakeAppBuilder = {
    val newBuilder =
      builder.configure("play.http.filters" -> classOf[AlwaysAuthFilters].getName)
    copy(builder = newBuilder)
  }
}

class AlwaysAuthFilter @Inject()(
    silhouette: Silhouette[Env],
    implicit val mat: Materializer
) extends Filter with FakeLogins {
  import silhouette.UserAwareAction

  override def apply(next: RequestHeader => Future[Result])(request: RequestHeader): Future[Result] = {
    val action = UserAwareAction.async { implicit r =>
      // embed an authenticator in the *request*
      for {
        a <- env.authenticatorService.create(fakeLoginInfos.head)
        v <- env.authenticatorService.init(a)
        authedReq = env.authenticatorService.embed(v, r)
        res <- next(authedReq)
      } yield res

    }


    action(request).run
  }
}

class AlwaysAuthFilters @Inject()(a: AlwaysAuthFilter) extends HttpFilters {
  override val filters = Seq(a)
}


trait FakeLogins {
  // Kind of mixes fixture logic in, because it's so intimately linked with
  // authentication logic in this case. On which note: these two are vars,
  // so they can be updated with the concrete user IDs when used with
  // AuthFixture
  val passwordHasher = new BCryptPasswordHasher(10)
  def userWithPassword(baseRow: UsersRow, password: String) = {
    val PasswordInfo(hasher_id, hashed_password, salt) = passwordHasher.hash(password)
    baseRow.copy(hasher = Some(hasher_id), hashedPassword=Some(hashed_password), salt=salt)
  }

  var fakeAccounts = Seq(
    AccountsRow(0,
      accountOwner=Some(0),
      ccEmails=List(),
      addons=List(), createdAt = Instant.EPOCH
    ),
    AccountsRow(1,
      accountOwner=Some(1),
      ccEmails=List(),
      addons=List(), createdAt = Instant.EPOCH
    ),
    AccountsRow(2,
      accountOwner=Some(2),
      isAdmin=true,
      ccEmails=List(),
      addons=List(), createdAt = Instant.EPOCH
    ))

  var fakeUsers = Seq(
    UsersRow(0,
      email=Some("test@example.com"),
      firstName=Some("Test"), lastName=Some("User"),
      volumeSystem = MeasureSystemsType.Imperial, weightSystem = MeasureSystemsType.Metric,
      purchaseVolumeSystem = MeasureSystemsType.Imperial, purchaseWeightSystem = MeasureSystemsType.Metric,
      subtractInventoryPreference = SubtractInventoryType.SubtractNone,
    ),
    UsersRow(1,
      email=Some("other@example.com"),
      firstName=Some("Other"), lastName=Some("User"),
      volumeSystem = MeasureSystemsType.Imperial, weightSystem = MeasureSystemsType.Metric,
      purchaseVolumeSystem = MeasureSystemsType.Imperial, purchaseWeightSystem = MeasureSystemsType.Metric,
      subtractInventoryPreference = SubtractInventoryType.SubtractNone,
    ),
    UsersRow(2,
      email=Some("admin@example.com"),
      firstName=Some("Admin"), lastName=Some("User"),
      volumeSystem = MeasureSystemsType.Imperial, weightSystem = MeasureSystemsType.Metric,
      purchaseVolumeSystem = MeasureSystemsType.Imperial, purchaseWeightSystem = MeasureSystemsType.Metric,
      subtractInventoryPreference = SubtractInventoryType.SubtractNone,
    ),
  )
  fakeUsers = fakeUsers.updated(1, userWithPassword(fakeUsers(1), "password"))

  var fakeUserAccounts = fakeUsers.map { u => UserAccountsRow(u.id,
    owner = u.id, userId = u.id, isActive = true, joinedAt = Instant.EPOCH, permissionLevel = AccountPermissionLevel.Shared,
  )}

  val fakeUserModels = fakeUsers.zip(fakeUserAccounts).zip(fakeAccounts).map { case ((u, ua), a) =>
    new UserModel(u, Seq(ua), Seq(a))
  }

  var fakeUserLogins = Seq(
    new UserLoginsRow("mybook.com", "test@example.com", 0,
      firstName=Some("Test"), lastName=Some("User")),
    new UserLoginsRow("credentials", "other@example.com", 1,
      firstName=Some("Other"), lastName=Some("User")),
    new UserLoginsRow("mybook.com", "admin@example.com", 2,
      firstName=Some("Admin"), lastName=Some("User")))

  var fakeLoginInfos = fakeUserLogins.map { ul =>
    LoginInfo(ul.providerId, ul.providerKey)
  }

  val fakeProfiles = fakeLoginInfos.zip(fakeUserLogins).map {
    case (li, ul) =>
      CommonSocialProfile(li, firstName=ul.firstName, lastName=ul.lastName)
  }

  implicit val env = new FakeEnvironment[Env](
    fakeLoginInfos.zip(fakeUserModels)
  )
}
