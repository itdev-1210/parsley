package util

import fixtures.Fixture
import models.DBDriver
import util.AsyncUtils.DBExtensions

import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.slick.DatabaseConfigProvider
import play.api.test.WithBrowser

import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.specs2.execute.AsResult

import java.sql.{Connection, DriverManager}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

abstract class FakeAuthAppScope // lighter-weight than FakeDBScope, but faster setup
    extends WithFakedApplication(new FakeAppBuilder().withFakeAuth)
    with FakeLogins

trait AlwaysAuthedBrowserScope extends WithBrowser[HtmlUnitDriver] with FakeLogins {
  override val app = new FakeAppBuilder().withFakeAuth.withAlwaysAuth.build
}

object FakeDBScope {
  val rootUrl = "jdbc:postgresql://db:5432/"

  def sqlLabelFromClass(cl: Class[_]): String = {
    cl.getName.replace(".", "_").toLowerCase
  }
}

abstract class FakeDBScope(val specClass: Class[_], startupFixtures: Seq[Fixture] = Nil)
    extends WithFakedApplication(
      new FakeAppBuilder().withFakeDB(
        FakeDBScope.rootUrl, FakeDBScope.sqlLabelFromClass(specClass)))
    with FakeLogins {

  import slick.jdbc.PostgresProfile.api._

  def withRawConn(rootUrl: String)(block: Connection => Unit): Unit = {
    val conn = DriverManager.getConnection(rootUrl + "?user=postgres")
    block(conn)
    conn.close()
  }

  def createDatabase(dbName: String) = withRawConn(FakeDBScope.rootUrl) { conn =>
    conn.createStatement().execute(s"create database $dbName;")
  }

  def dropDatabase(dbName: String) = withRawConn(FakeDBScope.rootUrl) { conn =>
    conn.createStatement().execute(s"drop database $dbName;")
  }

  override def around[T: AsResult](block: => T) = {
    val dbName = FakeDBScope.sqlLabelFromClass(specClass)
    createDatabase(dbName)
    val dbApi = app.injector.instanceOf[DBApi]
    Evolutions.applyEvolutions(dbApi.database("default"))
    addFixtures(startupFixtures: _*)
    val r = Try(super.around(block))
    dropDatabase(dbName) // "finally"-like cleanup
    r.get // want exceptions to go to specs2 infrastructure!
  }

  def addFixtures(f: Fixture*) = {
    val configProvider = app.injector.instanceOf[DatabaseConfigProvider]

    val setup = DBIO.seq(f.map(_.up(configProvider.get[DBDriver])): _*)
    Await.result(configProvider.get[DBDriver].db.runInTransaction(setup), 4.seconds)
  }
}
