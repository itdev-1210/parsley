package fixtures

import models.DBDriver

import slick.basic.DatabaseConfig
import slick.dbio.DBIO

trait Fixture {

  def up(config: DatabaseConfig[DBDriver]) : DBIO[_]

  implicit val ec = play.api.libs.concurrent.Execution.Implicits.defaultContext
}
