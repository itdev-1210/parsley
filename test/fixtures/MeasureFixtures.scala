package fixtures

import models.Tables._
import models._

import slick.basic.DatabaseConfig
import slick.lifted

trait MeasureFixtures extends AuthFixtures {

  var measureRows = Seq[MeasuresRow]()
  var unitRows = Seq[UnitsRow]()

  override def up(config: DatabaseConfig[DBDriver]): slick.dbio.DBIO[_] = {
    import config.profile.api._
    import QueryExtensions._

    for {
      // migrations handle inserting everything, so no actual creation of fixtures
      // just bookkeeping to make tests-writing easier
      _ <- super[AuthFixtures].up(config)
      fetchedMeasures <- Measures.result
      fetchedUnits <- Units.result
    } yield {
      measureRows = fetchedMeasures
      unitRows = fetchedUnits
    }
  }
}

object MeasureFixtures extends MeasureFixtures {}
