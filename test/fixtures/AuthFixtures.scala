package fixtures

import models.Tables._
import models._
import util.FakeLogins

import slick.basic.DatabaseConfig

trait AuthFixtures extends Fixture with FakeLogins {

  override def up(config: DatabaseConfig[DBDriver]): slick.dbio.DBIO[_] = {
    import config.profile.api._
    import QueryExtensions._

    for {
      insertedUsers <- (Users returning Users) ++= fakeUsers
      insertedUserIds = insertedUsers.map(_.id)
      insertedUserPrintPrefs <- UserPrintPreferences ++= insertedUserIds.map(id => UserPrintPreferencesRow(
        userId = id, recipePrintFormatPreference = RecipePrintFormatType.StepByStep,
      ))

      loginsWithIds = fakeUserLogins.zip(insertedUserIds).map { case (ul, uid) =>
        ul.copy(userId = uid)
      }
      insertedUserLogins <- {
        (UserLogins returning UserLogins) ++= loginsWithIds
      }

      accountsWithOwners = fakeAccounts.zip(insertedUserIds).map { case (acc, uid) =>
          acc.copy(accountOwner = Some(uid))
      }
      insertedAccounts <- (Accounts returning Accounts) ++= accountsWithOwners
      insertedAccountIds = insertedAccounts.map(_.id)

      userAccountsWithFkeys = fakeUserAccounts.zip(insertedUserIds).zip(insertedAccountIds).map { case ((ua, uid), aid) =>
          ua.copy(owner = aid, userId = uid)
      }
      insertedUserAccounts <- (UserAccounts returning UserAccounts) ++= userAccountsWithFkeys

    } yield {
      fakeUsers = insertedUsers
      fakeAccounts = insertedAccounts
      fakeUserLogins = insertedUserLogins
      fakeUserAccounts = insertedUserAccounts
      Unit
    }
  }
}

object AuthFixtures extends AuthFixtures
