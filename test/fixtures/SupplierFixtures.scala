package fixtures

import models.Tables._
import models._

import slick.basic.DatabaseConfig
trait SupplierFixtures extends AuthFixtures {

  var fakeSuppliers = Seq[SuppliersRow]()
  var fakeContacts = Seq[SupplierContactsRow]()

  override def up(config: DatabaseConfig[DBDriver]): slick.dbio.DBIO[_] = {
    import config.profile.api._
    import QueryExtensions._

    for {
      // fakeUsers must be referenced after super.up to have correct IDs!
      _ <- super[AuthFixtures].up(config)
      mainUserId = fakeAccounts(0).id
      otherUserId = fakeAccounts(1).id

      insertedSuppliers <- (Suppliers returning Suppliers) ++= Seq(
        SuppliersRow(0, mainUserId, Some("Some Contacts")),
        SuppliersRow(0, mainUserId, Some("No Contacts")),
        SuppliersRow(0, otherUserId, Some("Inaccessible")))

      insertedContacts <- (SupplierContacts returning SupplierContacts) ++= Seq(
        SupplierContactsRow(0, insertedSuppliers(0).id, 0, Some("First Contact")),
        SupplierContactsRow(0, insertedSuppliers(0).id, 1, Some("Second Contact")),
        SupplierContactsRow(0, insertedSuppliers(2).id, 0, Some("Inaccessible Contact")))
    } yield {
      fakeSuppliers = insertedSuppliers
      fakeContacts = insertedContacts
    }
  }
}

object SupplierFixtures extends SupplierFixtures {}
