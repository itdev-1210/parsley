import java.util.concurrent.TimeUnit

import com.gargoylesoftware.htmlunit.BrowserVersion
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._
import util.AlwaysAuthedBrowserScope

/**
 * add your integration spec here.
 * An integration test will fire up a whole play application in a real (or headless) browser
 */
@RunWith(classOf[JUnitRunner])
class IntegrationSpec extends Specification {
  // BrowserVersion.setDefault(BrowserVersion.INTERNET_EXPLORER_11)

  /*
  "Application" should {

    "work within a browser" in new AlwaysAuthedBrowserScope {
      browser.goTo("http://localhost:" + port)
      browser.url must equalTo("/") // no redirects - easier to diagnose than timeout

      browser.await.atMost(5, TimeUnit.SECONDS).until("nav").isPresent

      browser.pageSource must contain("Orders")

    }
  }
  */
}
