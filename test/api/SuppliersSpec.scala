package api

import fixtures.{SupplierFixtures, AuthFixtures}
import util.FakeDBScope

import models.{SupplierContactsRow, SuppliersRow}
import models.JsonConversions._

import com.mohiva.play.silhouette.test._
import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import play.api.libs.json._
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class SuppliersSpec extends Specification {

  implicit val iSupplierFormat = supplierFormat
  implicit val iSupplierContactFormat = supplierContactFormat

  // TODO: test individual reads, contacts, test writes and deletes
  abstract class SupplierTestScope extends FakeDBScope(this.getClass, Seq(SupplierFixtures))

  "/api/suppliers" should {
    "be sane" in new SupplierTestScope {

      val Some(resp) = route(app, FakeRequest(GET, "/api/suppliers")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      contentAsJson(resp).asInstanceOf[JsArray].value.foreach { s =>
          // Suppliers should be minimal, id + name subsets of the full
          // supplier JSON format.
        val supplierJson = s.asInstanceOf[JsObject] +
            ("owner" -> JsNumber(fakeAccounts.head.id))
        supplierJson.validate[SuppliersRow].isSuccess must beTrue
      }
    }

    "return all/only current user's suppliers" in new SupplierTestScope {

      val Some(resp) = route(app, FakeRequest(GET, "/api/suppliers")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      val suppliers = contentAsJson(resp).asInstanceOf[JsArray].value
      suppliers must not contain { s: JsValue =>
        (s \ "name").as[JsString].value == "Inaccessible Contact"
      }
      suppliers.size must equalTo(2)
    }
  }

  "/api/suppliers/:id" should {
    "be sane" in new SupplierTestScope {

      val supplierId: Int = SupplierFixtures.fakeSuppliers(0).id
      val Some(resp) = route(app, FakeRequest(GET, s"/api/suppliers/${supplierId}")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      val supplierJson = contentAsJson(resp).asInstanceOf[JsObject] -
          "suppliers" +
          ("id" -> JsNumber(supplierId)) +
          ("owner" -> JsNumber(fakeAccounts.head.id)) +
          ("tombstone" -> JsBoolean(false))
      supplierJson.validate[SuppliersRow].isSuccess must beTrue

      // check valid contacts JSON
      val contactsJson =
        (supplierJson.asInstanceOf[JsObject] \ "contacts").as[JsArray]
      contactsJson.value.length must equalTo(2)
      contactsJson.value.zipWithIndex.foreach { case (contact: JsObject, i) =>
        val cleanedContact = contact ++
            Json.obj(
              "supplierId" -> supplierId,
              "displayOrder" -> i,
              "id" -> 0)
        cleanedContact.validate[SupplierContactsRow].isSuccess must beTrue
      }
    }

    "404 on request for non-existent supplier" in new SupplierTestScope {
      val bogusId = 42

      val Some(resp) = route(app, FakeRequest(GET, s"/api/suppliers/${42}")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(NOT_FOUND)
    }

    "not allow users to see or enumerate other user's suppliers" in new SupplierTestScope {

      val supplierId = SupplierFixtures.fakeSuppliers(2).id
      val Some(resp) = route(app, FakeRequest(GET, s"/api/suppliers/${supplierId}")
                             .withAuthenticator(fakeLoginInfos.head))

       status(resp) must equalTo(NOT_FOUND)
    }

  }
}
