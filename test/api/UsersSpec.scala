package api

import com.mohiva.play.silhouette.test._
import fixtures.AuthFixtures
import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._
import util.FakeDBScope

@RunWith(classOf[JUnitRunner])
class UsersSpec extends Specification {

  "/api/me" should {
    "return current user's name" in new FakeDBScope(this.getClass, Seq(AuthFixtures)) {

      val Some(resp) = route(app, FakeRequest(GET, "/api/me")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      val js = contentAsJson(resp)
      (js \ "firstName").as[String] must equalTo("Test")
      (js \ "lastName").as[String] must equalTo("User")
    }
  }
}
