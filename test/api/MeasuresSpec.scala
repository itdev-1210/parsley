package api

import fixtures.MeasureFixtures
import util.FakeDBScope

import models.{MeasuresRow, UnitsRow}
import models.JsonConversions._

import com.mohiva.play.silhouette.test._
import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import org.specs2.matcher.MatchersImplicits._
import play.api.libs.json.Json.toJsObject
import play.api.libs.json._
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class MeasuresSpec extends Specification {
  implicit val iMeasuresFormat = measuresFormat
  implicit val iUnitsFormat = unitsFormat

  // TODO: test writes and deletes
  abstract class MeasureTestScope extends FakeDBScope(this.getClass, Seq(MeasureFixtures))

  "/api/measures" should {
    "be sane" in new MeasureTestScope {

      val Some(resp) = route(app, FakeRequest(GET, "/api/measures")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      contentAsJson(resp) must beAnInstanceOf[JsArray]

      contentAsJson(resp).asInstanceOf[JsArray].value.foreach { m =>
        val measureJson = m.asInstanceOf[JsObject]
        measureJson.value("units").validate[Seq[UnitsRow]]
        (measureJson - "units").validate[MeasuresRow]
      }
    }

    "return all measures with all units" in new MeasureTestScope {

      val Some(resp) = route(app, FakeRequest(GET, "/api/measures")
                             .withAuthenticator(fakeLoginInfos.head))

      status(resp) must equalTo(OK)
      contentType(resp) must beSome("application/json")

      val measures = contentAsJson(resp).asInstanceOf[JsArray].value
      measures.size must equalTo(MeasureFixtures.measureRows.size)
      measures.map(_.asInstanceOf[JsObject].value("units"))
        .map(_.asInstanceOf[JsArray].value.size)
        .sum must equalTo(MeasureFixtures.unitRows.size)
    }

    "create a new measure properly iff this is an admin user" in new MeasureTestScope {

      val m = MeasuresRow(id=0, name="count")
      val units = Seq(
        UnitsRow(id=0, measure=0, size=1, name="each"),
        UnitsRow(id=0, measure=0, size=12, name="dozen")
      )
      val toInsert = toJsObject(m) - "id" + ("units" ->
          JsArray(units.map { u =>
            toJsObject(u) - "id" - "measure"
          }))

      val Some(badResp) = route(app, FakeRequest(POST, "/api/measures")
                                .withAuthenticator(fakeLoginInfos.head)
                                .withBody(toInsert))

      status(badResp) must equalTo(FORBIDDEN)

      val Some(resp) = route(app, FakeRequest(POST, "/api/measures")
                             .withAuthenticator(fakeLoginInfos(2))
                             .withBody(toInsert))

      status(resp) must equalTo(CREATED)

      val newId = contentAsJson(resp).asInstanceOf[JsObject] \ "id"

      // Check that we can read the measure back out

      val Some(validateResp) = route(app, FakeRequest(GET, s"/api/measures")
                                     .withAuthenticator(fakeLoginInfos.head))

      val measures = contentAsJson(validateResp).asInstanceOf[JsArray].value
      measures.size must equalTo(MeasureFixtures.measureRows.size + 1)
      val newMeasure = measures.find(
        _.asInstanceOf[JsObject] \ "id" == newId
      ).get.asInstanceOf[JsObject]

      (newMeasure \ "units").get.asInstanceOf[JsArray].value
        .size must equalTo(units.size)
    }
  }

  "/api/measures/*" should {
    "allow measure modification" in new MeasureTestScope {
      import org.specs2.control.consoleLogging

      def getMeasures() = {
        val Some(resp) = route(app, FakeRequest(GET, "/api/measures")
                               .withAuthenticator(fakeLoginInfos.head))

        status(resp) must equalTo(OK)
        contentType(resp) must beSome("application/json")

        val js = contentAsJson(resp)
        js must beAnInstanceOf[JsArray]
        js.asInstanceOf[JsArray].value.map(_.asInstanceOf[JsObject])
      }

      val measureId = MeasureFixtures.measureRows.head.id
      val allUnits = MeasureFixtures.unitRows.filter(_.measure == measureId)
      allUnits.size must beGreaterThan(1)

      val unitToDelete = allUnits.last.id
      val unitToModify = allUnits.head.id
      val unitToAdd = UnitsRow(0, 0, size=454, name="pound", abbr=Some("lb"))

      val originalJs = getMeasures().find { m =>
        (m \ "id").get == JsNumber(measureId)
      }.get

      val originalUnits = (originalJs \ "units").get.asInstanceOf[JsArray]

      val modifiedUnit = originalUnits.value.find { u =>
        (u \ "id").get == JsNumber(unitToModify)
      }.get.asInstanceOf[JsObject] ++ Json.obj("name" -> "grammy")

      val modifiedJs = originalJs - "id" - "units" + ("units" ->
          Json.arr(
            modifiedUnit,
            toJsObject(unitToAdd) - "id" - "measure"))

      val Some(badResp) = route(app,
        FakeRequest(PUT, s"/api/measures/$measureId")
        .withAuthenticator(fakeLoginInfos.head)
        .withBody(modifiedJs))

      status(badResp) must equalTo(FORBIDDEN)

      val Some(resp) = route(app, FakeRequest(PUT, s"/api/measures/$measureId")
                             .withAuthenticator(fakeLoginInfos(2))
                             .withBody(modifiedJs))

      status(resp) must equalTo(OK)

      val newMeasures = getMeasures()

      newMeasures.size must equalTo(MeasureFixtures.measureRows.size)

      val modifiedMeasure = newMeasures.find { m =>
        (m \ "id").get == JsNumber(measureId)
      }.get

      val newUnits = (modifiedMeasure \ "units").get.asInstanceOf[JsArray].value
      newUnits.size must equalTo(2)

      newUnits.exists { u: JsValue =>
        (u \ "id").get == JsNumber(unitToModify) &&
            (u \ "name").get == JsString("grammy")
      } must beTrue

      newUnits.exists { u: JsValue =>
        (u \ "name").get == JsString("pound")
      } must beTrue

    }

    "allow measure deletion iff user is admin" in new MeasureTestScope {

      def getMeasures() = {
        val Some(resp) = route(app, FakeRequest(GET, "/api/measures")
                               .withAuthenticator(fakeLoginInfos.head))
        val js = contentAsJson(resp)
        js must beAnInstanceOf[JsArray]
        js.asInstanceOf[JsArray].value.map(_.asInstanceOf[JsObject])
      }

      val idToDelete = MeasureFixtures.measureRows.head.id

      val Some(badResp) = route(app,
        FakeRequest(DELETE, s"/api/measures/$idToDelete")
        .withAuthenticator(fakeLoginInfos.head))

      status(badResp) must equalTo(FORBIDDEN)

      getMeasures().size must equalTo(MeasureFixtures.measureRows.size)

      val Some(resp) = route(app,
        FakeRequest(DELETE, s"/api/measures/$idToDelete")
        .withAuthenticator(fakeLoginInfos(2)))

      status(resp) must equalTo(OK)

      val newMeasures = getMeasures()

      newMeasures.size must equalTo(MeasureFixtures.measureRows.size - 1)
      newMeasures.exists(_ \ "id" == idToDelete) must beFalse
    }

  }

}
