import subprocess
from subprocess import PIPE, DEVNULL, CalledProcessError
import os
import sys
from io import TextIOBase

from lib.environ import docker

docker_repo_namespace = "quay.io/parsley/"
dockerfile_dir = os.path.dirname(__file__) + "/../../docker/"

def make_img_tag():
    if "BUILD_NUMBER" in os.environ:
        return "build" + os.environ["BUILD_NUMBER"]
    else:
        return get_git_hash()

def make_img_name(repo, tag=None):
    if tag is None:
        tag = make_img_tag()
    return docker_repo_namespace + repo + ":" + tag

def get_git_hash():
    """Does not include untracked files!"""
    tree_hash = None
    try:
        tree_hash = subprocess.check_output(["git", "stash", "create"]).strip()[:10]
    except CalledProcessError:
        pass

    if not tree_hash:
        tree_hash = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip()

    return tree_hash.decode("utf-8")

# raises exception if docker build returns non-zero exit code
def build_image(img_name, stage_dir, dockerfile_path=None, dockerfile_string=None, *args):
    build_stdin = None

    real_args = docker("build", "-t", img_name) + list(args)
    if stage_dir is None:
        assert(dockerfile_string is not None and dockerfile_path is None)
        real_args.append("-")
    else:
        assert(dockerfile_string is None)
        if dockerfile_path is not None:
            real_args += ["-f", dockerfile_path]
        real_args.append(stage_dir)

    # docker build sends progress info to stdout; we want to use stdout for
    # other things, but it's nice to see this on the terminal, so stderr it is
    p = subprocess.Popen(real_args, stdout=sys.stderr, stdin=PIPE)
    if dockerfile_string is not None:
        p.communicate(dockerfile_string.encode())
    else:
        p.wait()

    if p.returncode != 0:
        raise subprocess.CalledProcessError(p.returncode, p.args)

def cont_is_running(cont_name):
    # errors out if container isn't running
    cmd = docker("top", cont_name)
    return not subprocess.call(cmd, stdout=DEVNULL, stderr=DEVNULL)

def compose_opts(proj_name):
    compose_opts = []
    compose_opts.append("--project-name={}".format(proj_name))
    compose_opts.append("--file={}".format(
        dockerfile_dir + "devel-compose.yml"))
    return compose_opts
