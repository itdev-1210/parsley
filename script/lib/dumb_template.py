def format_file(f, *args, **kwargs):
    return f.read().format(*args, **kwargs)
