import os
from sys import platform

ENVS = {
        "prod": {
            "public_host": "app.parsleycooks.com",
            "private_host": "ip-10-0-2-178.us-west-2.compute.internal",
            "conf": "application.prod.conf",
            "newrelic_env_name": "prod"
            },
        "test": {
            "public_host": "test.parsleycooks.com",
            "private_host": "ip-10-1-0-213.us-west-2.compute.internal",
            "conf": "application.test.conf",
            "newrelic_env_name": "test"
            },
        "staging": {
            "public_host": "staging.parsleycooks.com",
            "private_host": "ip-10-2-0-236.us-west-2.compute.internal",
            "conf": "application.staging.conf",
            "newrelic_env_name": "staging"
            }
        }

def get_local_environ():
    return get_environ_by_name(os.environ.get("PARSLEY_ENV", None))

def get_environ_by_name(name):
    return ENVS.get(name, None)

def maybe_sudo(cmd_name, opts):
    if platform == "darwin":
        return [cmd_name] + opts
    elif platform == "linux":
        return ["sudo", cmd_name] + opts
    else:
        raise "unknown platform " + platform

def docker(*opts):
    return maybe_sudo("docker", list(opts))

def dockercompose(*opts):
    return maybe_sudo("docker-compose", list(opts))
