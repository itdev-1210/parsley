import lib.imgutil as imgutil
from warnings import warn
import subprocess
from subprocess import DEVNULL
from sys import stdin, stdout
import os

from make_base import make_base
import lib.dumb_template as dumb_template

from lib.environ import docker, dockercompose

def start_devel(proj_name):

    for path in (".ivy2", ".sbt"):
        dir = os.path.expanduser("~") + "/" + path
        # want to make sure dir is not created by docker run as root-owned
        os.makedirs(dir, mode=0o755, exist_ok=True)

    subprocess.check_call(dockercompose() + imgutil.compose_opts(proj_name) + ["up", "-d"])

def run_devel(sbt_targets, proj_name=None, debug=False):
    if proj_name is None:
        proj_name = "devel"

    cont_name = "{proj_name}_app_1".format(proj_name=proj_name)
    if not imgutil.cont_is_running(cont_name):
        # DOES NOT check if the container is up to date, only if it has
        # the right name; hopefully changes to the base image are rare enough
        # that this isn't an issue
        print("no container {} to run_devel in; dying!".format(cont_name))
        exit(1)

    cmd = docker("exec")
    if stdin.isatty():
        cmd += ["--interactive", "--tty"]
    cmd += ["--user=app"]
    cmd += [cont_name, "./activator"]
    cmd += ["-mem", "2048"]
    if debug:
        cmd += ["-jvm-debug", "9001"]
    if not stdout.isatty():
        cmd.append("-Dsbt.log.noformat=true")

    cmd += sbt_targets

    subprocess.check_call(cmd)

def make_devel(proj_name=None):
    if proj_name is None:
        proj_name = "devel"

    dockerfile_path = imgutil.dockerfile_dir + "devel/Dockerfile"
    dockerfile_templ = open(dockerfile_path + ".templ")
    env_settings = ""

    passthrough_envs = [
        "TEAMCITY_VERSION", # so sbt-teamcity-logger knows when to do its things
        "BUILD_VCS_NUMBER", # so webpack.config.js can plop a git revision into the app build
    ]


    for varname in passthrough_envs:
        if varname in os.environ:
            env_settings += "ENV {} {}\n".format(varname, os.environ[varname])

    contents = dumb_template.format_file(dockerfile_templ,
            uid=os.geteuid(),
            env_settings=env_settings,
            base_name=make_base())

    dockerfile = open(dockerfile_path, 'w')
    dockerfile.write(contents)
    dockerfile.close()

    subprocess.check_call(dockercompose() + imgutil.compose_opts(proj_name) + ["build", "app"])

    return proj_name
