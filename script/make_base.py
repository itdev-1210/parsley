#!/usr/bin/env python3

import lib.imgutil as imgutil
import subprocess

def make_base(img_tag=None):
    img_name = imgutil.make_img_name("base", img_tag)
    dockerfile = imgutil.dockerfile_dir + "base.dockerfile"

    imgutil.build_image(img_name, imgutil.dockerfile_dir, dockerfile_path=dockerfile)
    return img_name

if __name__ == '__main__':
    print(make_base())
