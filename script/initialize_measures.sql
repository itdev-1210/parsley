--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: measures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY measures (id, name, allows_custom_names) FROM stdin;
3	Volume	f
1	Count	t
2	Weight	f
\.


--
-- Name: measures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('measures_id_seq', 4, true);


--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY units (id, measure, size, name, abbr, metric, scale_up_limit, scale_down_limit, "precision", quantum, common_purchase_unit) FROM stdin;
1	1	1	each	EA	t	\N	\N	2	0	f
9	3	1	mililiter	ml	t	\N	\N	2	0	f
10	3	1000	liter	liter	t	\N	\N	2	0	t
11	3	237	cup	cup	f	4	0.25	2	0	f
12	3	948	quart	qt	f	4	1	2	0	f
13	3	474	pint	pt	f	4	1	2	0	f
14	3	4.92999982833862305	teaspoon	tsp	f	3	\N	2	0	f
15	3	14.7899999618530273	tablespoon	T	f	4	1	2	0	f
16	3	29.5599994659423828	fluid ounce	fl oz	f	16	1	2	0	f
17	3	3785.409912109375	gallon	gal	f	\N	0.5	2	0	t
2	2	1	gram	g	t	\N	\N	2	0	f
5	2	28.349499999999999	ounce	oz	f	\N	\N	2	0	f
4	2	453.591999999999985	pound	lb	f	\N	\N	2	0	t
3	2	1000	kilogram	kg	t	\N	\N	2	0	t
\.


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('units_id_seq', 18, true);


--
-- PostgreSQL database dump complete
--

