#!/usr/bin/env python3

import lib.imgutil as imgutil
import subprocess
import sys
from lib.devel_img import run_devel
from make_base import make_base

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--project",
        help="the name of the Docker Compose project to run SBT in",
        default=None)
parser.add_argument("tag",
        help="the tag for both the prod image and the base image",
        default=imgutil.make_img_tag(),
        nargs="?")

args = parser.parse_args()

base_name = make_base(args.tag)
run_devel(['set dockerBaseImage := "{}"'.format(base_name), "docker:stage"],
        args.project)

img_name = imgutil.make_img_name("prod", args.tag)
imgutil.build_image(img_name, "target/docker/stage")

print(img_name)
