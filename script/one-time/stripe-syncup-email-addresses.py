#!/usr/bin/python3

import stripe
import psycopg2
import os

stripe.api_key = os.environ["STRIPE_KEY"]
dbhost = os.environ["DBHOST"]
dbpass = os.environ["DBPASS"]

with psycopg2.connect(user="master", password=dbpass, host=dbhost, database="parsley") as db:
	with db.cursor() as cur:
		cur.execute("""
			SELECT u.id, u.email, a.stripe_customer_id
			FROM users u JOIN accounts a ON u.id = a.account_owner
			WHERE stripe_customer_id IS NOT NULL;
		"""
		)
		for (user_id, email, stripe_customer_id) in cur:
			cust = stripe.Customer.retrieve(stripe_customer_id)
			if email is not None and cust.email != email:
				print(cust.email, email, stripe_customer_id)
				cust.email = email
				cust.save()
