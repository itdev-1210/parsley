#!/usr/bin/python3

import stripe
import psycopg2
import os
from datetime import datetime

stripe.api_key = os.environ["STRIPE_KEY"]
dbhost = os.environ["DBHOST"]
dbpass = os.environ["DBPASS"]

with psycopg2.connect(user="master", password=dbpass, host=dbhost, database="parsley") as db, db.cursor() as cur:
    cur.execute("""
        SELECT users.id, stripe_customer_id
        FROM users LEFT OUTER JOIN subscriptions ON users.id=subscriptions.user_id
        WHERE stripe_customer_id IS NOT NULL and stripe_subscription_id IS NULL;
    """)
    work_items = {user_id: stripe_customer_id for (user_id, stripe_customer_id) in cur}
    for (user_id, stripe_customer_id) in work_items.items():
        cust = stripe.Customer.retrieve(stripe_customer_id)
        subscription = cust['subscriptions']['data'][0]
        promo = subscription['discount']['coupon'].stripe_id if subscription['discount'] else None
        print(
            user_id, stripe_customer_id, subscription.stripe_id, promo,
            datetime.fromtimestamp(subscription['created']),
            datetime.fromtimestamp(subscription['current_period_end'])
        )
        cur.execute("""
            INSERT INTO subscriptions (
                user_id, stripe_subscription_id, plan, plan_started, plan_valid_until, promo_code
            ) VALUES (%s, %s, %s, to_timestamp(%s), to_timestamp(%s), %s);
        """, (
            user_id, subscription.stripe_id, "chef",
            subscription['created'], subscription['current_period_end'],
            promo
        ))
