#!/usr/bin/python3

import stripe
import psycopg2
import os

stripe.api_key = os.environ["STRIPE_KEY"]
dbhost = os.environ["DBHOST"]
dbpass = os.environ["DBPASS"]

with psycopg2.connect(user="master", password=dbpass, host=dbhost, database="parsley") as db:
	with db.cursor() as cur:
		cur.execute("""
			SELECT email, stripe_customer_id
			FROM users
			WHERE stripe_customer_id is NOT NULL;
		"""
		)
		for (email, stripe_customer_id) in cur:
			cust = stripe.Customer.retrieve(stripe_customer_id)
			cust.email = email
			cust.save()
			print(email, stripe_customer_id)
