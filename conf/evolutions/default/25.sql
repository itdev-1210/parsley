# --- !Ups
ALTER TABLE recipe_steps
  ALTER COLUMN description DROP NOT NULL,
  ALTER COLUMN description DROP DEFAULT;

# --- !Downs
ALTER TABLE recipe_steps
  ALTER COLUMN description SET NOT NULL,
  ALTER COLUMN description SET DEFAULT '';
