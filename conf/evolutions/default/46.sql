# --- !Ups
ALTER TABLE suppliers
    ADD COLUMN contact_name TEXT DEFAULT NULL;
# --- !Downs
ALTER TABLE suppliers
    DROP COLUMN contact_name;
