# --- !Ups

CREATE TABLE measures (
  id SERIAL PRIMARY KEY,
  -- in the i18n future, these should be per-locale
  name TEXT NOT NULL
);

CREATE TABLE units (
  id SERIAL PRIMARY KEY,
  measure INT REFERENCES measures NOT NULL,

  size REAL NOT NULL, -- base unit is defined as unit with size == 1

  name TEXT NOT NULL,
  abbr TEXT, -- if null, use name in places where abbr would be used

  UNIQUE (measure, size) INITIALLY DEFERRED
);

# --- !Downs

DROP TABLE units;

DROP TABLE measures;

