# -- !Ups
ALTER TABLE users ADD COLUMN phone_number TEXT;

UPDATE users u
SET phone_number = a.shipping_phone_number
FROM accounts a
WHERE a.account_owner = u.id;

ALTER TABLE accounts DROP COLUMN shipping_phone_number;

# --- !Downs
ALTER TABLE accounts ADD COLUMN shipping_phone_number text;

UPDATE accounts a
SET shipping_phone_number = u.phone_number
FROM users u
WHERE a.account_owner = u.id;

ALTER TABLE users DROP COLUMN phone_number;
