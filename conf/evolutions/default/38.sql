# --- !Ups
ALTER TABLE subscriptions RENAME COLUMN active TO cancelled; -- rename can't go with other actions

ALTER TABLE subscriptions ALTER COLUMN cancelled SET DEFAULT 'false';

UPDATE subscriptions
SET cancelled = (NOT cancelled);

# --- !Downs
ALTER TABLE subscriptions RENAME COLUMN cancelled TO active;
ALTER TABLE subscriptions ALTER COLUMN active SET DEFAULT 'true';

UPDATE subscriptions
SET active = (NOT active);
