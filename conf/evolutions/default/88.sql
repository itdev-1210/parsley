# --- !Ups
ALTER TABLE user_info
ADD COLUMN hide_all_recipes BOOLEAN NOT NULL DEFAULT FALSE;


# --- !Downs
ALTER TABLE user_info
DROP COLUMN hide_all_recipes;