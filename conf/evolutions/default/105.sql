# --- !Ups
-- this migration is step 1 - create the tables and columns we'll need for
-- three-way merge syncing

ALTER TABLE products RENAME last_stdlib_expansion TO last_expansion;
ALTER TABLE products RENAME last_stdlib_correction TO last_correction;
ALTER TABLE products RENAME stdlib_original TO sync_upstream;

ALTER INDEX stdlib_user_copies RENAME TO sync_downstream_copies;

ALTER TABLE products
  -- column exists only as a cache to make sending info to browser easier
  ADD COLUMN sync_upstream_owner INT,
  ADD CONSTRAINT sync_upstream_valid_owner
    FOREIGN KEY (sync_upstream, sync_upstream_owner)
    REFERENCES products (id, owner);

# --- !Downs
ALTER TABLE products RENAME last_expansion TO last_stdlib_expansion;
ALTER TABLE products RENAME last_correction TO last_stdlib_correction;
ALTER TABLE products RENAME sync_upstream TO stdlib_original;

ALTER TABLE products
  DROP CONSTRAINT sync_upstream_valid_owner,
  DROP COLUMN sync_upstream_owner;

ALTER INDEX sync_downstream_copies RENAME TO stdlib_user_copies;
