# --- !Ups
CREATE TABLE ingredient_pars (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users(id) NOT NULL,
  product INT REFERENCES products NOT NULL,
  amount DOUBLE PRECISION,
  measure INT REFERENCES measures(id),
  unit INT REFERENCES units(id),
  packaged BOOLEAN NOT NULL DEFAULT FALSE,
  package_name TEXT,
  sub_package_name TEXT,
  package_size DOUBLE PRECISION NOT NULL,
  super_package_size DOUBLE PRECISION NOT NULL DEFAULT 1,
  supplier_name TEXT,

  CONSTRAINT inventory_item_product_valid
  FOREIGN KEY (product, owner) REFERENCES products (id, owner),

  CONSTRAINT inventory_item_unit_valid
  FOREIGN KEY (unit, measure) REFERENCES units (id, measure),

  CONSTRAINT inventory_item_product_measure_valid
  FOREIGN KEY (product, measure) REFERENCES product_measures (product, measure)
);

# --- !Downs
DROP TABLE ingredient_pars;
