# --- !Ups
ALTER TABLE recipes
  ADD COLUMN created_by INT REFERENCES users(id);

# --- !Downs
ALTER TABLE recipes
  DROP COLUMN created_by;
