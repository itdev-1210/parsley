# --- !Ups
ALTER TABLE recipes
ADD COLUMN batch_size DECIMAL,
ADD COLUMN batch_label TEXT;

# --- !Downs
ALTER TABLE recipes
DROP COLUMN batch_size,
DROP COLUMN batch_label;
