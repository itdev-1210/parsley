# --- !Ups
ALTER TABLE users
ADD COLUMN is_readonly BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE user_shared_accounts
ADD COLUMN is_readonly BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE user_shared_invites
ADD COLUMN is_readonly BOOLEAN NOT NULL DEFAULT false;

# --- !Downs
ALTER TABLE users
DROP COLUMN is_readonly;

ALTER TABLE user_shared_accounts
DROP COLUMN is_readonly;

ALTER TABLE user_shared_invites
DROP COLUMN is_readonly;
