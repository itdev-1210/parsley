# --- !Ups
ALTER TABLE recipes
  ADD COLUMN item_number TEXT;

# --- !Downs
ALTER TABLE recipes
  DROP COLUMN item_number;
