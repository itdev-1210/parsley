# --- !Ups
ALTER TABLE users
  ADD COLUMN last_usage TIMESTAMP;

# --- !Downs
ALTER TABLE users
  DROP COLUMN last_usage;
