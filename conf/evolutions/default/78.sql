# --- !Ups
UPDATE products p
SET salable = false
FROM recipes r
WHERE r.product = p.id AND p.ingredient;

-- get rid of only future transactions that used sub-recipes
-- manually checked that these were safe - only used by inactive users
DELETE FROM transaction_deltas delt
USING transactions xact
WHERE delt.xact = xact.id AND -- join cond
      xact.xact_type = 'customer-order' AND
      delt.measure IS NOT NULL AND -- has measures is right now good proxy for ingredient
      xact.time > now();

-- get rid of the order-attached menu items that correspond to transaction deltas
-- above
DELETE FROM menu_items mi
USING orders o, menus m, products p, transactions xact
WHERE o.xact=xact.id AND m.id=o.menu and m.id=mi.menu AND p.id=mi.product AND --join cond
      p.ingredient AND
      NOT m.user_visible AND -- implied by m.id=o.menu, but doesn't hurt to check
      xact.time > now();

-- get rid of user-visible menu items for sub-recipes
DELETE FROM menu_items mi
USING menus m, products p
WHERE m.id = mi.menu AND mi.product = p.id AND -- join conds
      m.user_visible AND p.ingredient;

# --- !Downs
-- copied Ups from evolution 50.sql
UPDATE products SET salable = true WHERE id IN (SELECT product FROM recipes);

-- the DELETE statements are naturally destructive - no corresponding downs
