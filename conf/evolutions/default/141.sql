# --- !Ups
ALTER TABLE products
  DROP CONSTRAINT products_preferred_source_fkey;

ALTER TABLE products
  ADD CONSTRAINT products_preferred_source_fkey
  FOREIGN KEY (preferred_source) REFERENCES product_sources (id)
  ON DELETE SET NULL
  DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE transaction_deltas
  DROP CONSTRAINT transaction_deltas_product_source_fkey;

ALTER TABLE transaction_deltas
  ADD CONSTRAINT transaction_deltas_product_source_fkey
  FOREIGN KEY (product_source) REFERENCES product_sources (id)
  ON DELETE SET NULL;

ALTER TABLE locations_synced_suppliers
  DROP CONSTRAINT locations_synced_suppliers_supplier_id_fkey;

ALTER TABLE locations_synced_suppliers
  ADD CONSTRAINT locations_synced_suppliers_supplier_id_fkey
  FOREIGN KEY (supplier_id) REFERENCES suppliers (id)
  ON DELETE CASCADE;

# --- !Downs
ALTER TABLE products
  DROP CONSTRAINT products_preferred_source_fkey;

ALTER TABLE products
  ADD CONSTRAINT products_preferred_source_fkey
  FOREIGN KEY (preferred_source) REFERENCES product_sources (id)
  DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE transaction_deltas
  DROP CONSTRAINT transaction_deltas_product_source_fkey;

ALTER TABLE transaction_deltas
  ADD CONSTRAINT transaction_deltas_product_source_fkey
  FOREIGN KEY (product_source) REFERENCES product_sources (id);

ALTER TABLE locations_synced_suppliers
  DROP CONSTRAINT locations_synced_suppliers_supplier_id_fkey;

ALTER TABLE locations_synced_suppliers
  ADD CONSTRAINT locations_synced_suppliers_supplier_id_fkey
  FOREIGN KEY (supplier_id) REFERENCES suppliers (id);
