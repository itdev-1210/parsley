# --- !Ups
ALTER TABLE user_info
ADD COLUMN source_price_change_warning_fraction NUMERIC NOT NULL DEFAULT 0.2;

# --- !Downs
ALTER TABLE user_info
DROP COLUMN source_price_change_warning_fraction;