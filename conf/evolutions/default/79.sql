# --- !Ups
-- first off, delete the invalid ones (measured product, non-measured order)
DELETE FROM transaction_deltas d
USING products p, product_measures pm
WHERE p.id = d.product
      AND p.id = pm.product -- this join is tricky - restricts us to products with measures defined
      AND d.measure IS NULL;

-- then delete the no-ops that introduce unhelpful constraints
DELETE FROM transaction_deltas d
WHERE d.amount = 0 AND d.cash_flow IS NULL;

# --- !Downs
