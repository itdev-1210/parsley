# --- !Ups
UPDATE products
SET name = prep.name
FROM (SELECT output_product, name FROM simple_preparations) prep
WHERE id = prep.output_product;

# --- !Downs
UPDATE products
SET name = CONCAT(p.original_ingredient_name, ', ', p.prep_name)
FROM
(
  SELECT
  original_ingredient.name as original_ingredient_name, prep.name as prep_name, prep.output_product as prep_id
  FROM products original_ingredient
  INNER JOIN simple_preparations prep ON original_ingredient.id = prep.input_product
) p
WHERE id = p.prep_id;
