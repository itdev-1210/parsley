# --- !Ups
ALTER TABLE subscriptions
  RENAME COLUMN plan TO stripe_plan_id;

ALTER TABLE users
  RENAME COLUMN plan TO subscription_level;

# --- !Downs
ALTER TABLE users
  RENAME COLUMN subscription_level TO plan;

ALTER TABLE subscriptions
  RENAME COLUMN stripe_plan_id TO plan;
