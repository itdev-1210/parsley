# --- !Ups

-- common table for recipes and ingredients
CREATE TABLE products (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users,
  name TEXT NOT NULL,
  tags TEXT NOT NULL,

  ingredient BOOL DEFAULT FALSE NOT NULL,
  salable BOOL DEFAULT FALSE NOT NULL
);
COMMENT ON COLUMN products.owner IS
'can be "global" - part of library';
COMMENT ON COLUMN products.ingredient IS
'whether it can be used in recipes. write-only for now';
COMMENT ON COLUMN products.salable IS
'whether it can be used in menus/orders. write-only for now';
COMMENT ON COLUMN products.tags IS
'each prefixed by semicolon. same namespace for all types of'
'products';

CREATE INDEX ON products (owner);

CREATE TABLE product_measures (

  id SERIAL PRIMARY KEY,
  product INT REFERENCES products NOT NULL,

  measure INT REFERENCES measures NOT NULL,
  preferred_unit INT REFERENCES units NOT NULL,

  CONSTRAINT product_measures_valid_preferred_unit
  FOREIGN KEY (preferred_unit, measure)
  REFERENCES units (id, measure),

  conversion_measure INT REFERENCES measures NOT NULL,
  conversion_unit INT REFERENCES units NOT NULL,

  CONSTRAINT product_measures_valid_conversion_unit
  FOREIGN KEY (conversion_unit, conversion_measure)
  REFERENCES units (id, measure),

  CONSTRAINT product_measures_conversion_measure_defined
  FOREIGN KEY (product, conversion_measure)
  REFERENCES product_measures (product, measure)
  DEFERRABLE INITIALLY DEFERRED,

  conversion FLOAT NOT NULL, -- size of preferred_unit in units of conversion_unit

  UNIQUE (product, measure)
);

COMMENT ON COLUMN product_measures.conversion_measure IS
'for product''s preferred measure, equals measure';
COMMENT ON COLUMN product_measures.conversion_unit IS
'for product''s preferred measure, equals preferred_unit';
COMMENT ON COLUMN product_measures.conversion IS
'size of preferred_unit in units of conversion_unit';

# --- !Downs
DROP TABLE product_measures;
DROP TABLE products;

