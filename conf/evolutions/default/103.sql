# --- !Ups

ALTER TABLE user_info
  ADD COLUMN include_photo_in_recipe_print BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs

ALTER TABLE user_info
  DROP COLUMN include_photo_in_recipe_print;
