# --- !Ups
ALTER TABLE recipe_step_ingredients
  ALTER COLUMN amount DROP NOT NULL,
  ALTER COLUMN unit DROP NOT NULL,
  ALTER COLUMN measure DROP NOT NULL;

# --- !Downs
ALTER TABLE recipe_step_ingredients
  ALTER COLUMN amount SET NOT NULL,
  ALTER COLUMN unit SET NOT NULL,
  ALTER COLUMN measure SET NOT NULL;
