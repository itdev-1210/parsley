# --- !Ups
UPDATE products
SET name = CONCAT(p.original_ingredient_name, ', ', p.prep_name)
FROM
(
  SELECT
  original_ingredient.name as original_ingredient_name, prep.name as prep_name, prep.output_product as prep_id
  FROM products original_ingredient
  INNER JOIN simple_preparations prep ON original_ingredient.id = prep.input_product
) p
WHERE id = p.prep_id;

ALTER TABLE products ALTER COLUMN name SET NOT NULL;

# --- !Downs
ALTER TABLE products ALTER COLUMN name DROP NOT NULL;

UPDATE products
SET name = NULL WHERE id IN (SELECT output_product FROM simple_preparations);
