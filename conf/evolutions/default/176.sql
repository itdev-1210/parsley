# -- !Ups
UPDATE recipes r SET item_number = NULL WHERE r.product IN (
  SELECT id FROM products WHERE ingredient = true
);

# --- !Downs
