# --- !Ups


CREATE TABLE user_logins (
    provider_id TEXT NOT NULL,
    provider_key TEXT NOT NULL,
    user_id INT NOT NULL REFERENCES Users,
    PRIMARY KEY (provider_id, provider_key) -- Unfortunately, slick doesn't do its client-side upsert with non-primary keys
);

# --- !Downs

DROP TABLE user_logins;
