# -- !Ups
ALTER TABLE transactions
ADD COLUMN include_in_calculated_inventory BOOLEAN NOT NULL DEFAULT TRUE;
# --- !Downs
ALTER TABLE transactions
DROP COLUMN include_in_calculated_inventory;