# --- !Ups
ALTER TABLE suppliers
  ADD COLUMN tombstone BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE product_sources
  ADD COLUMN tombstone BOOLEAN NOT NULL DEFAULT FALSE;
# --- !Downs
ALTER TABLE product_sources
  DROP COLUMN tombstone;

ALTER TABLE suppliers
  DROP COLUMN tombstone;
