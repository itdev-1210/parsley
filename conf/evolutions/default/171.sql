# -- !Ups
update transaction_deltas as d1
set cash_flow = subquery.price_flow
from (
	select deltas.id, deltas.amount, deltas.cash_flow, r.price,  COALESCE(r.price, 0) * deltas.amount as price_flow from transactions xact
	inner join transaction_deltas deltas on xact.id = deltas.xact
	inner join recipes r on deltas.product = r.product where xact.xact_type = 'customer-order'
) as subquery
WHERE d1.id = subquery.id;

# --- !Downs

