# --- !Ups
-- stealing a few pages from Meraki's book

-- users table gets frequently-read, frequently-written, relatively-small columns
ALTER TABLE users
  ADD COLUMN created_at TIMESTAMP,
  ADD COLUMN last_login TIMESTAMP;

-- user_info gets potentially-large, infrequently-accessed and very-infrequently-written info
CREATE TABLE user_info (
  user_id INT PRIMARY KEY REFERENCES users,
  shipping_phone_number TEXT,
  shipping_address TEXT,
  cc_emails TEXT ARRAY NOT NULL DEFAULT  '{}'
);

INSERT INTO user_info (user_id) SELECT id from users;

# --- !Downs
ALTER TABLE users
  DROP COLUMN created_at,
  DROP COLUMN last_login;

DELETE from user_info;
DROP TABLE user_info;
