# --- !Ups
ALTER TABLE products
  ADD COLUMN hidden BOOL NOT NULL DEFAULT FALSE,
  -- for prepared ingredients, no name - name derived from parent ingredient
  ALTER COLUMN name DROP NOT NULL;

CREATE TABLE simple_preparations (
  output_product INT PRIMARY KEY REFERENCES products(id),
  input_product INT NOT NULL REFERENCES products(id),
  name TEXT NOT NULL, -- name of the preparation itself
  advance_prep BOOL NOT NULL,
  uses_parent_measures BOOL NOT NULL, -- purely a UI flag - backend keeps the measures stored separately for main and preps
  yield_fraction DOUBLE PRECISION NOT NULL DEFAULT 1,
  yield_measure INT NOT NULL,
  -- the following would be nice, but incompatible in the uses_parent_measures case
--  CONSTRAINT simple_prep_valid_yield_measure_output
--    FOREIGN KEY (output_product, yield_measure)
--    REFERENCES product_measures (product, measure)
--    DEFERRABLE INITIALLY DEFERRED,
  -- but this is fine, because ingredients always have measures, and preps cannot be preps of each other
  CONSTRAINT simple_prep_valid_yield_measure_input
    FOREIGN KEY (input_product, yield_measure)
    REFERENCES product_measures (product, measure)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT simple_prep_input_output_distinct
    CHECK (output_product <> input_product), -- TODO: figure out if index on input_product is necessary
  CONSTRAINT simple_prep_no_duplicate_names UNIQUE (input_product, name)
    DEFERRABLE INITIALLY DEFERRED
);

# --- !Downs

DROP TABLE simple_preparations;
ALTER TABLE products
  DROP COLUMN hidden,
  ALTER COLUMN name SET NOT NULL;
