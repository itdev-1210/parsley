# --- !Ups
UPDATE products
SET last_modified = '2016-01-01 00:00'
WHERE last_modified IS NULL;

ALTER TABLE products
  ALTER COLUMN last_modified SET NOT NULL;

# --- !Downs
ALTER TABLE products
  ALTER COLUMN last_modified DROP NOT NULL;

UPDATE products
SET last_modified = NULL
WHERE last_modified = '2016-01-01 00:00';
