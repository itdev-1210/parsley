# --- !Ups
ALTER TABLE inventories
  ADD COLUMN merged_previous_inventory BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE inventories DROP COLUMN merged_previous_inventory;
