# --- !Ups
ALTER TABLE recipes
  ADD COLUMN created_at TIMESTAMP;

# --- !Downs
ALTER TABLE recipes
  DROP COLUMN created_at;
