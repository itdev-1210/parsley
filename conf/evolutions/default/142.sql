# --- !Ups

-- change file name to url
UPDATE onboarding_steps
  SET left_image = CONCAT('/assets/images/', left_image)
  WHERE left_image IS NOT NULL;

-- change file name to url
UPDATE onboarding_steps
  SET right_image = CONCAT('/assets/images/', right_image)
  WHERE right_image IS NOT NULL;

# --- !Downs

/*
 * NOTE: This will clear all files referenced by onboarding steps.
 * Before running this DOWN migration,
 * also remove file from storage service matching this query.
 */
UPDATE onboarding_steps
  SET left_image = NULL
  WHERE left_image ~* '[0-9]+\/';

-- pick file name only
UPDATE onboarding_steps
  SET left_image = OVERLAY(left_image placing '' FROM 1 FOR 15)
  WHERE left_image ~* '^\/assets\/images\/';

/*
 * NOTE: This will clear all files referenced by onboarding steps.
 * Before running this DOWN migration,
 * also remove file from storage service matching this query.
 */
UPDATE onboarding_steps
  SET right_image = NULL
  WHERE right_image ~* '[0-9]+\/';

UPDATE onboarding_steps
  SET right_image = OVERLAY(right_image placing '' FROM 1 FOR 15)
  WHERE right_image ~* '^\/assets\/images\/';
