# --- !Ups#
CREATE TYPE measure_systems_type AS ENUM ('imperial', 'metric');

ALTER TABLE user_info
  ADD COLUMN weight_system measure_systems_type NOT NULL DEFAULT 'metric',
  ADD COLUMN volume_system measure_systems_type NOT NULL DEFAULT 'imperial';

# --- !Downs

ALTER TABLE user_info
	DROP COLUMN weight_system,
	DROP COLUMN volume_system;

DROP TYPE measure_systems_type;