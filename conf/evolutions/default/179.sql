# -- !Ups
CREATE TEMP TABLE dummy_users_from_location_invites (
    user_id INT NOT NULL,
    account_id INT NOT NULL
) ON COMMIT DROP;

INSERT INTO dummy_users_from_location_invites
SELECT u.id, a.id FROM
  accounts a JOIN
  upstream_invites i ON i.invitee = a.id JOIN
  users u ON a.account_owner = u.id
WHERE u.first_name IS NULL; -- only case where current code allows null first_name

UPDATE accounts a
SET account_owner = NULL
FROM dummy_users_from_location_invites d
WHERE a.id = d.account_id;

DELETE FROM user_print_preferences upp
            USING dummy_users_from_location_invites d
WHERE upp.user_id = d.user_id;

DELETE FROM user_accounts ua
            USING dummy_users_from_location_invites d
WHERE ua.user_id = d.user_id;

DELETE FROM users u
            USING dummy_users_from_location_invites d
WHERE u.id = d.user_id;

# --- !Downs
CREATE TEMP TABLE accounts_needing_dummy_user (
  account_id INT NOT NULL,
  email TEXT NOT NULL
) ON COMMIT DROP;


INSERT INTO accounts_needing_dummy_user
SELECT a.id, a.email
FROM accounts a JOIN
     upstream_invites i ON i.invitee = a.id
WHERE a.account_owner IS NULL;

CREATE TEMPORARY TABLE dummy_users (
    account_id INT NOT NULL,
    user_id INT NOT NULL
) ON COMMIT DROP;

WITH inserted_users AS (
  INSERT INTO users (email)
    SELECT d.email from accounts_needing_dummy_user d
    RETURNING id AS user_id, email
)
INSERT INTO dummy_users (account_id, user_id)
SELECT a.account_id, u.user_id
FROM accounts_needing_dummy_user a JOIN inserted_users u ON u.email = a.email;

INSERT INTO user_accounts (owner, user_id, is_active, permission_level)
SELECT d.account_id, d.user_id, true, 'shared'
FROM dummy_users d;

INSERT INTO user_print_preferences (user_id)
SELECT user_id from dummy_users;

UPDATE accounts a
SET account_owner = d.user_id
FROM dummy_users d
WHERE a.id = d.account_id;
