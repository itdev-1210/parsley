# --- !Ups
ALTER TABLE measures
    ADD COLUMN allows_custom_names BOOL NOT NULL DEFAULT FALSE;

ALTER TABLE product_measures
    ADD COLUMN custom_name TEXT;

# --- !Downs
ALTER TABLE product_measures
  DROP COLUMN custom_name;

ALTER TABLE measures
    DROP COLUMN allows_custom_names;
