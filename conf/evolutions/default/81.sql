# --- !Ups
ALTER TABLE product_characteristics
ADD COLUMN ingredients TEXT;

# --- !Downs
ALTER TABLE product_characteristics
DROP COLUMN ingredients;
