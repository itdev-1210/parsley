# -- !Ups
INSERT INTO recipe_nutrients (product, packaged)
SELECT r.product, FALSE
FROM recipes r
ON CONFLICT (product) DO NOTHING;

# --- !Downs
-- no downs, format is compatible and up is idempotent