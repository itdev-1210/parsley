# --- !Ups
CREATE TABLE locations_synced_recipes (
  owner INT REFERENCES users(id) NOT NULL,
  location_id INT REFERENCES users(id) NOT NULL,
  recipe_category TEXT NOT NULL,

  UNIQUE (owner, location_id, recipe_category)
);

CREATE TABLE locations_synced_suppliers (
  owner INT REFERENCES users(id) NOT NULL,
  location_id INT REFERENCES users(id) NOT NULL,
  supplier_id INT REFERENCES suppliers(id) NOT NULL,

  UNIQUE (owner, location_id, supplier_id)
);

# --- !Downs
DROP TABLE locations_synced_recipes;
DROP TABLE locations_synced_suppliers;
