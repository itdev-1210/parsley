# --- !Ups

CREATE TYPE account_permission_level AS ENUM ('recipe-read-only', 'shared', 'operations');

ALTER TABLE users ADD COLUMN permission_level account_permission_level;
UPDATE users SET permission_level = 'recipe-read-only' WHERE is_readonly = TRUE;
UPDATE users SET permission_level = 'shared' WHERE is_readonly = FALSE;
ALTER TABLE users DROP COLUMN is_readonly;

-- remove redundancy
ALTER TABLE user_shared_accounts DROP COLUMN is_readonly;

ALTER TABLE user_shared_invites ADD COLUMN invite_level account_permission_level;
UPDATE user_shared_invites set invite_level = 'recipe-read-only' WHERE is_readonly = TRUE;
UPDATE user_shared_invites set invite_level = 'shared' WHERE is_readonly = FALSE;
ALTER TABLE user_shared_invites ALTER COLUMN invite_level SET NOT NULL;
ALTER TABLE user_shared_invites DROP COLUMN is_readonly;

# --- !Downs

ALTER TABLE user_shared_invites ADD COLUMN is_readonly BOOLEAN DEFAULT FALSE;
UPDATE user_shared_invites SET is_readonly = TRUE WHERE invite_level = 'recipe-read-only';
ALTER TABLE user_shared_invites DROP COLUMN invite_level;

ALTER TABLE users ADD COLUMN is_readonly BOOLEAN DEFAULT FALSE;
UPDATE users SET is_readonly = TRUE WHERE permission_level = 'recipe-read-only';
ALTER TABLE users DROP COLUMN permission_level;

ALTER TABLE user_shared_accounts ADD COLUMN is_readonly BOOLEAN DEFAULT FALSE;
UPDATE user_shared_accounts SET is_readonly = u.is_readonly FROM users u WHERE u.id = shared_account;

DROP TYPE account_permission_level;
