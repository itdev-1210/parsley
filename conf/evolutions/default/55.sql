# --- !Ups
ALTER TABLE user_info 
  ADD COLUMN company_logo TEXT DEFAULT NULL;

# --- !Downs
ALTER TABLE user_info 
  DROP COLUMN company_logo;
