# --- !Ups
INSERT INTO measures (id, name, allows_custom_names)
VALUES (2, 'Weight', false), (3, 'Volume', false)
ON CONFLICT (id) DO NOTHING; -- this should handle the prod/test case - conflicting IDs will sink it

UPDATE measures set allows_custom_names=TRUE where id=1;

SELECT setval('measures_id_seq', 4);

INSERT INTO units (id, measure, size, name, abbr, metric, scale_up_limit, scale_down_limit)
VALUES
  -- volume
  (9, 3, 1, 'mililiter', 'ml', true, 500, NULL),
  (10, 3, 1000, 'liter', 'L', true, NULL, 0.1),
  (11, 3, 236.588, 'cup', 'cup', false, 8, 0.25),
  (12, 3, 946.353, 'quart', 'qt', false, 4, 0.25),
  (13, 3, 473.176, 'pint', 'pt', false, 2, 0.25),
  (14, 3, 4.92892, 'teaspoon', 'tsp', false, 6, NULL),
  (15, 3, 14.7868, 'tablespoon', 'T', false, 8, 1),
  (16, 3, 29.5735, 'fluid ounce', 'fl oz', false, 8, 1),
  (17, 3, 3785.409912109375, 'gallon', 'gal', false, NULL, 0.25),
  --weight
  (2, 2, 1, 'gram', 'g', true, 2000, NULL),
  (3, 2, 1000, 'kilogram', 'kg', true, NULL, 0.25),
  (4, 2, 453.592, 'pound', 'lb', false, NULL, 0.25),
  (5, 2, 28.3495, 'ounce', 'oz', false, 16, NULL)
ON CONFLICT (id) DO NOTHING;

SELECT setval('units_id_seq', 18);

# --- !Downs
