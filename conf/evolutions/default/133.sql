# --- !Ups

ALTER TABLE onboarding_steps
  ADD COLUMN close_btn_color TEXT NOT NULL DEFAULT 'ffffff'
    CHECK(
      close_btn_color ~ '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
    );

# --- !Downs

ALTER TABLE onboarding_steps
  DROP COLUMN close_btn_color;
