# --- !Ups
ALTER TABLE product_sources 
  ADD COLUMN super_package_size INT,
  ADD COLUMN sub_package_name TEXT;

# --- !Downs
ALTER TABLE product_sources 
  DROP COLUMN super_package_size,
  DROP COLUMN sub_package_name;
