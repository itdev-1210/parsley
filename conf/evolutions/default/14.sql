# --- !Ups
ALTER TABLE orders
ADD COLUMN date DATE NOT NULL DEFAULT CURRENT_DATE;

-- once the default from previous has dealt with all current rows, get rid of the default
ALTER TABLE orders
ALTER COLUMN date DROP DEFAULT;

# --- !Downs
ALTER TABLE orders
DROP COLUMN date;
