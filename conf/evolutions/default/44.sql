# --- !Ups
ALTER TABLE suppliers
    ADD COLUMN instructions text;

# --- !Downs
ALTER TABLE suppliers
    DROP COLUMN instructions;
