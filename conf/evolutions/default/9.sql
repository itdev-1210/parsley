# --- !Ups
CREATE TABLE product_sources (
  id SERIAL PRIMARY KEY,
  product INT REFERENCES products NOT NULL,
  supplier INT REFERENCES suppliers,
  measure INT REFERENCES measures NOT NULL,
  unit INT REFERENCES units NOT NULL,

  CONSTRAINT product_sources_valid_units
  FOREIGN KEY (unit, measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED,

  CONSTRAINT product_sources_measure_defined
  FOREIGN KEY (product, measure)
  REFERENCES product_measures (product, measure)
  DEFERRABLE INITIALLY DEFERRED,

  cost DECIMAL,

  -- if FALSE, package_size must equal 1 and package_name must be NULL
  packaged BOOL NOT NULL DEFAULT FALSE,
  package_name TEXT,
  package_size REAL NOT NULL -- in units of `unit`

);
COMMENT ON TABLE product_sources IS
'entry''s ownership is defined by the supplier, not the product -
products might be shared!';
COMMENT ON COLUMN product_sources.supplier IS
'can be null in standard library case';
COMMENT ON COLUMN product_sources.cost IS
'per-package. if unpackaged, per-unit. in standard-library case, NULL';
COMMENT ON COLUMN product_sources.packaged IS
'if FALSE, package_size=1 and package_name IS NULL';

CREATE INDEX ON product_sources (supplier, product);
CREATE INDEX ON product_sources (product);

ALTER TABLE products
ADD COLUMN preferred_source INT REFERENCES product_sources
DEFERRABLE INITIALLY DEFERRED;

COMMENT ON COLUMN products.preferred_source IS
'if NULL, either no sources or recipe is preferred over them';

# --- !Downs
ALTER TABLE products DROP COLUMN preferred_source;

DROP TABLE product_sources;
