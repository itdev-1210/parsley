# --- !Ups
-- Conversion of existing orders to use the transactions rows for the bulk of data

ALTER TABLE orders
  ADD COLUMN xact INT;

-- TEMP and HACKY - can't return non-inserted columns from insert, so temp column instead
ALTER TABLE transactions
    ADD COLUMN orig_order_id INT REFERENCES orders;

INSERT INTO transactions (orig_order_id, owner, time, confirmed, xact_type)
  SELECT id, owner, date, true, 'customer-order' FROM orders;

INSERT INTO transaction_deltas (xact, owner, product, amount, measure, unit)
  SELECT xact.id, xact.owner, oi.product, oi.ordered_amount, oi.ordered_measure, oi.ordered_unit
  FROM transactions xact join order_items oi on (xact.orig_order_id=oi.order);

UPDATE orders as o
SET xact=x.id
FROM transactions x
WHERE x.orig_order_id=o.id;

-- drop temp column
ALTER TABLE transactions
  DROP COLUMN orig_order_id;

ALTER TABLE orders
  ALTER COLUMN xact SET NOT NULL,
  ADD FOREIGN KEY (xact) REFERENCES transactions(id) DEFERRABLE INITIALLY DEFERRED,
  DROP COLUMN date,
  DROP COLUMN has_forecasts;

# --- !Downs

DELETE FROM transaction_deltas where xact in (SELECT xact from orders);

-- see up re TEMP and HACKY
ALTER TABLE transactions ADD COLUMN orig_order_id INT;

UPDATE transactions as x
SET orig_order_id=o.id
FROM orders o
WHERE x.id=o.xact;

ALTER TABLE orders
  DROP COLUMN xact,
  ADD COLUMN date DATE,
  ADD COLUMN has_forecasts BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE orders as o
SET date=x.time, owner=x.owner
FROM transactions x
WHERE x.orig_order_id=o.id;

DELETE FROM transactions
WHERE orig_order_id IS NOT NULL;

-- drop temp column
ALTER TABLE transactions DROP COLUMN orig_order_id;

ALTER TABLE orders
  ALTER COLUMN date SET NOT NULL,
  ALTER COLUMN owner SET NOT NULL;
