# --- !Ups
ALTER TABLE categories
    DROP CONSTRAINT categories_unique;

ALTER TABLE categories
  ADD CONSTRAINT categories_unique UNIQUE (owner, name, category_type)
  DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE categories
  DROP CONSTRAINT categories_unique;

ALTER TABLE categories
  ADD CONSTRAINT categories_unique UNIQUE (owner, name, category_type)
  NOT DEFERRABLE;
