
# -- !Ups
CREATE TABLE user_print_preferences (
  user_id INT PRIMARY KEY REFERENCES users(id),
  order_include_preps BOOLEAN NOT NULL DEFAULT False,
  order_include_recipes BOOLEAN NOT NULL DEFAULT False,
  order_include_sub_recipes BOOLEAN NOT NULL DEFAULT False,
  print_sub_recipe_preferences BOOLEAN NOT NULL DEFAULT False,
  break_page_on_recipe_print BOOLEAN NOT NULL DEFAULT False,
  recipe_print_format_preference recipe_print_format_type NOT NULL DEFAULT 'step-by-step',
  include_photo_in_recipe_print BOOLEAN NOT NULL DEFAULT False,
  include_nutrition_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT False,
  include_cost_breakdown_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT False
);

INSERT INTO user_print_preferences (
  user_id,
  print_sub_recipe_preferences,
  break_page_on_recipe_print,
  recipe_print_format_preference,
  include_photo_in_recipe_print,
  include_nutrition_in_multiple_recipe_print,
  include_cost_breakdown_in_multiple_recipe_print
) SELECT
    id,
    print_sub_recipe_preferences,
    break_page_on_recipe_print,
    recipe_print_format_preference,
    include_photo_in_recipe_print,
    include_nutrition_in_multiple_recipe_print,
    include_cost_breakdown_in_multiple_recipe_print FROM users;

ALTER TABLE users
  DROP COLUMN print_sub_recipe_preferences,
  DROP COLUMN break_page_on_recipe_print,
  DROP COLUMN recipe_print_format_preference,
  DROP COLUMN include_photo_in_recipe_print,
  DROP COLUMN include_nutrition_in_multiple_recipe_print,
  DROP COLUMN include_cost_breakdown_in_multiple_recipe_print;

# --- !Downs

ALTER TABLE users
  ADD COLUMN print_sub_recipe_preferences BOOLEAN DEFAULT True,
  ADD COLUMN break_page_on_recipe_print BOOLEAN NOT NULL DEFAULT False,
  ADD COLUMN recipe_print_format_preference recipe_print_format_type NOT NULL DEFAULT 'step-by-step',
  ADD COLUMN include_photo_in_recipe_print BOOLEAN NOT NULL DEFAULT False,
  ADD COLUMN include_nutrition_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT False,
  ADD COLUMN include_cost_breakdown_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT False;

UPDATE users u SET
  print_sub_recipe_preferences = pref.print_sub_recipe_preferences,
  break_page_on_recipe_print = pref.break_page_on_recipe_print,
  recipe_print_format_preference = pref.recipe_print_format_preference,
  include_photo_in_recipe_print = pref.include_photo_in_recipe_print,
  include_nutrition_in_multiple_recipe_print = pref.include_cost_breakdown_in_multiple_recipe_print,
  include_cost_breakdown_in_multiple_recipe_print = pref.include_cost_breakdown_in_multiple_recipe_print
  FROM
    user_print_preferences pref
  WHERE u.id = pref.user_id;

DROP TABLE user_print_preferences;
