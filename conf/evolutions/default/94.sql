# --- !Ups
ALTER TABLE purchase_orders ADD COLUMN created_by INT REFERENCES users;
UPDATE purchase_orders SET created_by = owner;
ALTER TABLE purchase_orders ALTER COLUMN created_by SET NOT NULL;

# --- !Downs
ALTER TABLE purchase_orders DROP COLUMN created_by;