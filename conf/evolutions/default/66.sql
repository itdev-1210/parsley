# --- !Ups
--- Change non-custom order to customs
--- copy menu details for each non-custom order
--- first copy menu details and then assign back id of newly created menu

-- table is now redundant - transactions/transaction_deltas version has all the kinks worked out
DROP TABLE order_items;

ALTER TABLE menus ADD COLUMN order_ref INT NULL REFERENCES orders(id), ADD COLUMN menu_ref INT NULL REFERENCES menus(id);
ALTER TABLE menu_sections ADD COLUMN section_ref INT NULL REFERENCES menu_sections(id), ADD COLUMN menu_ref INT NULL REFERENCES menus(id);

WITH non_custom_orders AS (
    SELECT id, owner, menu FROM orders WHERE menu IN (SELECT ID FROM menus WHERE user_visible = true )
)
INSERT INTO menus (owner, name, user_visible, order_ref, menu_ref) SELECT owner, 'Custom Order', false, id, menu FROM non_custom_orders;

INSERT INTO menu_sections (menu, display_order, name, owner, section_ref, menu_ref)
  SELECT m.id, ms.display_order, ms.name, ms.owner, ms.id, ms.menu FROM menu_sections ms INNER JOIN menus m ON ms.menu = m.menu_ref WHERE m.menu_ref IS NOT NULL;

INSERT INTO menu_items (section, display_order, product, owner, menu)
  SELECT ms.id, mi.display_order, mi.product, mi.owner, ms.menu FROM menu_items mi INNER JOIN menu_sections ms ON mi.section = ms.section_ref
  WHERE mi.menu = ms.menu_ref AND ms.section_ref IS NOT NULL;

UPDATE orders SET menu = m.id FROM (SELECT id, order_ref FROM menus WHERE order_ref IS NOT NULL) m WHERE m.order_ref = orders.id;

ALTER TABLE menu_sections DROP COLUMN section_ref, DROP COLUMN menu_ref;
ALTER TABLE menus DROP COLUMN order_ref, DROP COLUMN menu_ref;

# --- !Downs
