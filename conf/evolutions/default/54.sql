# --- !Ups
CREATE TYPE email_token_type AS ENUM ('email-confirm', 'password-reset');
ALTER TABLE email_tokens
  -- got lucky, sign_up = false is the only value in use
  ADD COLUMN token_type email_token_type NOT NULL DEFAULT 'password-reset',
  DROP COLUMN sign_up;

# --- !Downs
ALTER TABLE email_tokens
  DROP COLUMN token_type,
  ADD COLUMN sign_up BOOLEAN NOT NULL DEFAULT FALSE;
DROP TYPE email_token_type;
