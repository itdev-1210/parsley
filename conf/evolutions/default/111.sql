# --- !Ups
-- append 'upstream-invite' to values of email_token_type
-- ALTER TYPE ... ADD VALUE is not allowed inside transaction
-- so temporarily copy type/column, create new type/column with additional value and then copy from old column

ALTER TYPE email_token_type RENAME TO temp_email_token_type;
CREATE TYPE email_token_type AS ENUM ('email-confirm', 'password-reset', 'user-shared-invite', 'upstream-invite');
ALTER TABLE email_tokens RENAME COLUMN token_type TO temp_token_type;
ALTER TABLE email_tokens ADD token_type email_token_type NOT NULL DEFAULT 'password-reset';
UPDATE email_tokens SET token_type = temp_token_type::text::email_token_type;
ALTER TABLE email_tokens DROP COLUMN temp_token_type;
DROP TYPE temp_email_token_type;

CREATE TABLE upstream_invites (
  id SERIAL PRIMARY KEY,
  email_token INT NOT NULL REFERENCES email_tokens(id),
  invitee INT NOT NULL REFERENCES users(id),
  inviter INT NOT NULL REFERENCES users(id)
);

# --- !Downs

DROP TABLE upstream_invites;

-- remove 'user-shared-invite' from list of values for email_token_type
DELETE FROM email_tokens WHERE token_type = 'upstream-invite';
DELETE FROM pg_enum WHERE enumlabel = 'upstream-invite' AND enumtypid = (SELECT oid FROM pg_type WHERE typname = 'upstream-invite');
