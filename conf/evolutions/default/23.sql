# --- !Ups
ALTER TABLE orders
    ALTER COLUMN description DROP NOT NULL;

# --- !Downs
ALTER TABLE orders
  ALTER COLUMN description SET NOT NULL;
