# --- !Ups

CREATE TYPE change_type AS ENUM (
  'price-change',
  'receiving'
);

CREATE TABLE ingredient_price_history (
  id SERIAL PRIMARY KEY,
  product INT REFERENCES products NOT NULL,
  measure INT REFERENCES measures NOT NULL,
  unit INT REFERENCES units NOT NULL,

  CONSTRAINT ingredient_price_history_valid_units
  FOREIGN KEY (unit, measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED,

  cost DECIMAL,
  -- if FALSE, package_size must equal 1 and package_name must be NULL
  packaged BOOL NOT NULL DEFAULT FALSE,
  package_size REAL NOT NULL,
  super_package_size REAL NOT NULL DEFAULT 1,

  price_per price_per_type NOT NULL DEFAULT 'super-package',
  pricing_measure INT,
  pricing_unit INT,

  change_type change_type NOT NULL,
  create_at TIMESTAMP NOT NULL

);

CREATE TABLE ingredient_price_history_measures (
  id SERIAL PRIMARY KEY,
  ingredient_price INT NOT NULL REFERENCES ingredient_price_history(id),
  measure INT NOT NULL,
  preferred_unit INT NOT NULL,
  conversion_measure INT NOT NULL,
  conversion_unit INT NOT NULL,
  conversion DOUBLE PRECISION NOT NULL,
  amount DOUBLE PRECISION NOT NULL,

  UNIQUE (ingredient_price, measure)
);

# --- !Downs
DROP TABLE ingredient_price_history_measures;
DROP TABLE ingredient_price_history;
DROP TYPE change_type;