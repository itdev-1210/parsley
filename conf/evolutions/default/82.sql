# --- !Ups
CREATE TYPE recipe_print_format_type AS ENUM ('step-by-step', 'ingredients-first');

ALTER TABLE user_info
  ADD COLUMN recipe_print_format_preference recipe_print_format_type NOT NULL DEFAULT 'step-by-step';

# --- !Downs

ALTER TABLE user_info
  DROP COLUMN recipe_print_format_preference;

DROP TYPE recipe_print_format_type;
