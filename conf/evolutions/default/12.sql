# --- !Ups
CREATE TABLE menus (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users NOT NULL,
  name TEXT NOT NULL,

  UNIQUE (id, owner) -- just to make it possible to enforce consistency with uniqueness constraints
);

CREATE TABLE menu_sections (
  id SERIAL PRIMARY KEY,
  menu INT NOT NULL,
  display_order INT NOT NULL,
  name TEXT NOT NULL,

  owner INT NOT NULL,

  UNIQUE (menu, display_order) DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (menu, owner) REFERENCES menus (id, owner),

  UNIQUE (id, owner) -- just to make it possible to enforce consistency with uniqueness constraints
);

ALTER TABLE products ADD CONSTRAINT product_id_owner_references_hack_constraint UNIQUE (id, owner);

CREATE TABLE menu_items (
  id SERIAL PRIMARY KEY,
  section int NOT NULL,
  display_order INT NOT NULL,
  product INT REFERENCES products NOT NULL,

  owner INT NOT NULL,

  UNIQUE (section, display_order) DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (product, owner) REFERENCES products (id, owner),
  FOREIGN KEY (section, owner)  REFERENCES menu_sections (id, owner)
);


# --- !Downs

DROP TABLE menu_items;
ALTER TABLE products DROP CONSTRAINT product_id_owner_references_hack_constraint;
DROP TABLE menu_sections;
DROP TABLE menus;

