# --- !Ups
ALTER TABLE user_logins
    ADD COLUMN old_username TEXT DEFAULT NULL;

UPDATE user_logins
SET old_username=user_logins.provider_key
WHERE provider_id='credentials';

UPDATE user_logins
SET provider_key=users.email
FROM users
WHERE user_id=users.id AND provider_id='credentials';

# --- !Downs

UPDATE user_logins
SET provider_key=user_logins.old_username
WHERE provider_id='credentials';

ALTER TABLE user_logins
    DROP COLUMN old_username;