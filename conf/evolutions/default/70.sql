# --- !Ups
ALTER TABLE user_info
  ALTER COLUMN print_sub_recipe_preferences SET NOT NULL;

# --- !Downs
ALTER TABLE user_info
  ALTER COLUMN print_sub_recipe_preferences DROP NOT NULL;
