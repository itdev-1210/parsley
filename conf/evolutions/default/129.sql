# --- !Ups
ALTER TABLE user_info
ADD COLUMN new_recipe_threshold_days INT NOT NULL DEFAULT 14;

# --- !Downs
ALTER TABLE user_info
DROP COLUMN new_recipe_threshold_days;