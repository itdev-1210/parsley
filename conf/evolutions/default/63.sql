# --- !Ups
ALTER TABLE transaction_deltas
  ALTER CONSTRAINT transaction_item_product_measure_valid DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE transaction_deltas
  ALTER CONSTRAINT transaction_item_product_measure_valid NOT DEFERRABLE;
