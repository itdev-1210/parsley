# --- !Ups

ALTER TABLE onboarding_steps
  ALTER COLUMN title DROP NOT NULL,
  ADD COLUMN name TEXT;

UPDATE onboarding_steps SET name = title;

ALTER TABLE onboarding_steps ALTER COLUMN name SET NOT NULL;

# --- !Downs

UPDATE onboarding_steps SET title = name;

ALTER TABLE onboarding_steps
  DROP COLUMN name,
  ALTER COLUMN title SET NOT NULL;
