# --- !Ups
ALTER TABLE product_measures
  ADD COLUMN amount DOUBLE PRECISION NOT NULL DEFAULT 1;

ALTER TABLE transaction_measures
  ADD COLUMN amount DOUBLE PRECISION NOT NULL DEFAULT 1;

# --- !Downs
ALTER TABLE product_measures
  DROP COLUMN amount;

ALTER TABLE transaction_measures
  DROP COLUMN amount;
