# --- !Ups
ALTER TABLE users
    ADD COLUMN addons TEXT ARRAY NOT NULL DEFAULT '{}';

ALTER TABLE subscriptions
  ADD COLUMN addons TEXT ARRAY NOT NULL DEFAULT '{}';

# --- !Downs
ALTER TABLE users
  DROP COLUMN addons;

ALTER TABLE subscriptions
  DROP COLUMN addons;
