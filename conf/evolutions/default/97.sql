# --- !Ups
UPDATE product_sources
SET price_per = 'package'::price_per_type
WHERE price_per = 'super-package'::price_per_type
      AND NOT packaged;

UPDATE product_sources
SET price_per = 'package'::price_per_type
WHERE price_per = 'super-package'::price_per_type
      AND sub_package_name IS NULL;

# --- !Downs

-- no downs - don't know which ones are user information
