# --- !Ups
CREATE INDEX locations_synced_recipes_upstream_user
  ON locations_synced_recipes (owner);

CREATE INDEX locations_synced_recipes_downstream_user
  ON locations_synced_recipes (location_id);

CREATE INDEX locations_synced_suppliers_upstream_user
  ON locations_synced_suppliers (owner);

CREATE INDEX locations_synced_suppliers_downstream_user
  ON locations_synced_suppliers (location_id);

# --- !Downs
DROP INDEX locations_synced_recipes_upstream_user;
DROP INDEX locations_synced_recipes_downstream_user;
DROP INDEX locations_synced_suppliers_upstream_user;
DROP INDEX locations_synced_suppliers_downstream_user;
