# --- !Ups
-- see https://github.com/parsleysoftware/parsley-new/wiki/The-Standard-Library for expansion/correction
-- semantics
ALTER TABLE products
  ADD COLUMN nndbsr_last_import TIMESTAMP,
  ADD COLUMN stdlib_original INT REFERENCES products,
  ADD COLUMN last_stdlib_expansion TIMESTAMP,
  ADD COLUMN last_stdlib_correction TIMESTAMP,
  DROP CONSTRAINT products_nndbsr_id_key;

ALTER TABLE users
  ADD COLUMN is_stdlib BOOLEAN NOT NULL DEFAULT FALSE;

-- for index-assisted updates on new stdlib modifications
CREATE INDEX stdlib_user_copies
ON products (stdlib_original)
  WHERE owner IS NOT NULL;

CREATE INDEX products_nndbsr_stdlib
ON products (nndbsr_id)
  WHERE owner IS NULL;

# --- !Downs
DROP INDEX products_nndbsr_stdlib;
DROP INDEX stdlib_user_copies;

ALTER TABLE products
  DROP COLUMN nndbsr_last_import,
  DROP COLUMN stdlib_original,
  DROP COLUMN last_stdlib_expansion,
  DROP COLUMN last_stdlib_correction,
  ADD CONSTRAINT products_nndbsr_id_key UNIQUE (nndbsr_id);

ALTER TABLE users
  DROP COLUMN is_stdlib;
