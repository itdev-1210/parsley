# --- !Ups

-- this measure will be used as the default for recipe output. that logic
-- will be implemented in the application, not in the DB schema, because it's a
-- pain to implement in evolutions an PL/PostgreSQL
INSERT INTO measures (name)
VALUES ('count');

INSERT INTO units (measure, size, name, abbr)
  SELECT id, 1.0, 'each', 'ea'
  FROM measures
  where name='count';

-- doesn't actually constrain more than id's primary key constraint, but does
-- allow foreign keys to reference this pair and therefore do some useful
-- integrity checks
ALTER TABLE units
ADD CONSTRAINT units_unique_id_measure
UNIQUE (id, measure);


# --- !Downs
-- kill 'em all
DELETE FROM units;
DELETE FROM measures;

ALTER TABLE units
DROP CONSTRAINT units_unique_id_measure;
