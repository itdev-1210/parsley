# --- !Ups
ALTER TABLE user_info
  DROP COLUMN food_cost_percent;

# --- !Downs
ALTER TABLE user_info
  ADD COLUMN food_cost_percent NUMERIC;
