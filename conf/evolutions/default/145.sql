# --- !Ups
UPDATE users u
SET multi_location_upstream = NULL, upstream_relative_name = NULL
FROM user_shared_accounts usa
WHERE u.id = usa.shared_account;

# --- !Downs
UPDATE users sharedU
SET multi_location_upstream = mainU.multi_location_upstream,
    upstream_relative_name = mainU.upstream_relative_name
FROM user_shared_accounts usa, users mainU
WHERE usa.shared_account = sharedU.id AND usa.owner = mainU.id;
