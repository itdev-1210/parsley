# --- !Ups
DELETE FROM transaction_deltas WHERE cash_flow IS NULL AND xact IN (
  SELECT finalizedxact FROM transactions WHERE finalizedxact IS NOT NULL
  AND id IN (SELECT xact FROM purchase_orders)
);

# --- !Downs

-- no downs
