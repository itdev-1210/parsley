# --- !Ups
ALTER TABLE subscriptions
  ALTER COLUMN user_id SET NOT NULL,
  ALTER COLUMN stripe_subscription_id SET NOT NULL,
  ALTER COLUMN plan_started SET NOT NULL,
  ALTER COLUMN plan_valid_until SET NOT NULL;

# --- !Downs
ALTER TABLE subscriptions
  ALTER COLUMN user_id DROP NOT NULL,
  ALTER COLUMN stripe_subscription_id DROP NOT NULL,
  ALTER COLUMN plan_started DROP NOT NULL,
  ALTER COLUMN plan_valid_until DROP NOT NULL;
