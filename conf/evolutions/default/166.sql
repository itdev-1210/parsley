# -- !Ups
ALTER TABLE recipes DROP CONSTRAINT recipes_created_by_fkey;

ALTER TABLE recipes ADD CONSTRAINT recipes_created_by_fkey FOREIGN KEY (created_by) REFERENCES users (id);

# --- !Downs
ALTER TABLE recipes DROP CONSTRAINT recipes_created_by_fkey;

ALTER TABLE recipes ADD CONSTRAINT recipes_created_by_fkey FOREIGN KEY (created_by) REFERENCES accounts (id);
