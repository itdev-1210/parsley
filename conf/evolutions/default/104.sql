# --- !Ups
ALTER TABLE products
  ADD COLUMN tombstone BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE product_measures
  ADD COLUMN tombstone BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE products
  DROP COLUMN tombstone;

ALTER TABLE product_measures
  DROP COLUMN tombstone;
