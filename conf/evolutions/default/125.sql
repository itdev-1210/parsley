# --- !Ups

UPDATE users
SET
multi_location_upstream = t.multi_location_upstream,
upstream_relative_name = t.upstream_relative_name
FROM
(
    SELECT
    ua.shared_account, uu.multi_location_upstream, uu.upstream_relative_name
    FROM users uu
    INNER JOIN user_shared_accounts ua ON uu.id = ua.owner
) t
WHERE id = t.shared_account;

# --- !Downs
