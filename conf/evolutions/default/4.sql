# --- !Ups
CREATE TABLE suppliers (
  id         SERIAL PRIMARY KEY,
  owner      INT REFERENCES users NOT NULL,
  name       TEXT,
  account_id TEXT,
  phone      TEXT,
  email      TEXT,
  comments   TEXT
);
CREATE INDEX ON suppliers (owner);

CREATE TABLE supplier_contacts (
  id            SERIAL PRIMARY KEY,
  supplier_id   INT NOT NULL REFERENCES suppliers,
  display_order INT NOT NULL,
  name          TEXT,
  phone         TEXT,
  email         TEXT,
  comments      TEXT
);
CREATE INDEX ON supplier_contacts (supplier_id);

# --- !Downs
DROP TABLE supplier_contacts;
DROP TABLE suppliers;