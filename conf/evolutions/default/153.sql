# --- !Ups
CREATE TABLE product_infos (
  product INT REFERENCES products NOT NULL,
  lowest_price_preferred BOOLEAN NOT NULL DEFAULT True
);

INSERT INTO product_infos
SELECT id, true FROM products WHERE ingredient = true;

# --- !Downs
DROP TABLE product_infos;
