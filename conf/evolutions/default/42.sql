# --- !Ups
CREATE TABLE nutrients (
  id SERIAL PRIMARY KEY,
  nndbsr_id TEXT UNIQUE NOT NULL,
  nndbsr_name TEXT NOT NULL,
  parsley_name TEXT, -- where this is not the same as nndbsr_name
  unit TEXT NOT NULL,
  active BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE product_nutrients (
  product INT NOT NULL REFERENCES products(id),
  nutrient INT NOT NULL REFERENCES nutrients(id),
  amount DECIMAL NOT NULL,
  PRIMARY KEY (product, nutrient)
);

# --- !Downs
DROP TABLE product_nutrients;
DROP TABLE nutrients;
