# --- !Ups

-- inventory_items
ALTER TABLE inventory_items
  ADD COLUMN supplier INT
  REFERENCES suppliers (id)
  ON DELETE SET NULL;

UPDATE inventory_items inv
SET supplier=s.id
FROM suppliers s
WHERE inv.owner = s.owner AND supplier_name = s.name;

-- ingredient_pars
ALTER TABLE ingredient_pars
  ADD COLUMN supplier INT
  REFERENCES suppliers (id)
  ON DELETE SET NULL;

UPDATE ingredient_pars par
SET supplier=s.id
FROM suppliers s
WHERE par.owner = s.owner AND par.supplier_name = s.name;

# --- !Downs
ALTER TABLE inventory_items
DROP COLUMN supplier;

ALTER TABLE ingredient_pars
DROP COLUMN supplier;
