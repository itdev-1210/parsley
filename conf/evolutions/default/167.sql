# --- !Ups
CREATE TABLE onboarding_widgets (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  default_message TEXT NOT NULL,
  custom_message TEXT
);

CREATE TABLE user_onboarding_widgets (
  user_id INT NOT NULL REFERENCES users,
  widget INT NOT NULL REFERENCES onboarding_widgets,

  UNIQUE (user_id, widget) INITIALLY DEFERRED
);

INSERT INTO onboarding_widgets(name, default_message) VALUES
  ('num_recipes', 'There are not data'),
  ('new_update_recipes', 'There are not data'),
  ('purchase_history', 'There are not data'),
  ('ingredient_price_history', 'There are not data'),
  ('biggest_recent_price_changes', 'There are not data');

# --- !Downs
DROP TABLE user_onboarding_widgets;
DROP TABLE onboarding_widgets;