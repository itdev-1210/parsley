# --- !Ups

ALTER TABLE orders ADD COLUMN covers_count INT;

# --- !Downs

ALTER TABLE orders DROP COLUMN covers_count;
