# --- !Ups
-- if any of these have performance issues, consider clustering (item rows by parent object id, parent object
-- rows by (owner, time)

CREATE TABLE inventories (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users(id) NOT NULL,
  time TIMESTAMP NOT NULL,

  name TEXT,

  UNIQUE (id, owner)
);
CREATE INDEX inventory_timeseries_fetch_idx ON inventories (owner, time);

CREATE TABLE inventory_items (
  id SERIAL PRIMARY KEY,
  inventory INT REFERENCES inventories(id) NOT NULL,
  owner INT REFERENCES users(id) NOT NULL, -- for consistency checking
  product INT REFERENCES products,
  amount DOUBLE PRECISION NOT NULL,
  measure INT REFERENCES measures(id),
  unit INT REFERENCES units(id),

  CONSTRAINT inventory_item_owner_valid
  FOREIGN KEY (inventory, owner) REFERENCES inventories (id, owner),

  CONSTRAINT inventory_item_product_valid
  FOREIGN KEY (product, owner) REFERENCES products (id, owner),

  CONSTRAINT inventory_item_unit_valid
  FOREIGN KEY (unit, measure) REFERENCES units (id, measure),

  CONSTRAINT inventory_item_product_measure_valid
  FOREIGN KEY (product, measure) REFERENCES product_measures (product, measure)
);

CREATE TYPE transaction_type AS ENUM (
  'customer-order', -- orders from the *user's* customers
  'purchase-order' -- orders from the user to their suppliers
);
CREATE TABLE transactions (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users(id) NOT NULL,
  time TIMESTAMP NOT NULL,

  confirmed BOOL NOT NULL DEFAULT TRUE,
  -- if !confirmed, finalizedXact refers to a transaction that *is* confirmed
  -- that represents the final version of this one
  finalizedXact INT REFERENCES transactions(id),

  xact_type transaction_type NOT NULL,

  UNIQUE (id, owner)
);
CREATE INDEX transaction_timeseries_fetch_idx ON transactions (owner, time);

CREATE TABLE transaction_deltas (
  id SERIAL PRIMARY KEY,
  xact INT REFERENCES transactions(id) NOT NULL,
  owner INT REFERENCES users(id) NOT NULL, -- for consistency checking

  product INT REFERENCES products,
  amount DOUBLE PRECISION NOT NULL, -- positive means produced, negative means used
  measure INT REFERENCES measures(id),
  unit INT REFERENCES units(id),
  -- positive numbers mean income, negative numbers mean expenditure
  cash_flow NUMERIC,

  -- quick sketch of future per-transaction-type bits of info we might want to keep
  -- for ingredient usage, which recipe used it (WITHIN same transaction)
  -- cause INT REFERENCES transaction_deltas(id),

  -- for purchase orders, which supplier was used. note that suppliers will *need*
  -- to be archived for accurate information! unlike recipes, where storage of
  -- quantities used of each ingredient is really all we need
  -- source INT REFERENCES product_sources(id),


  CONSTRAINT transaction_item_owner_valid
  FOREIGN KEY (xact, owner) REFERENCES transactions (id, owner),

  CONSTRAINT transaction_item_product_valid
  FOREIGN KEY (product, owner) REFERENCES products (id, owner),

  CONSTRAINT transaction_item_unit_valid
  FOREIGN KEY (unit, measure) REFERENCES units (id, measure),

  CONSTRAINT transaction_item_product_measure_valid
  FOREIGN KEY (product, measure) REFERENCES product_measures (product, measure)
);
# --- !Downs

DROP TABLE transaction_deltas, inventory_items;
DROP TABLE inventories, transactions;
DROP TYPE transaction_type;
