# --- !Ups
UPDATE users
SET accepts_terms='false'
WHERE accepts_terms IS NULL;

ALTER TABLE users
  ALTER COLUMN accepts_terms SET NOT NULL,
  ALTER COLUMN accepts_terms SET DEFAULT 'false';

# --- !Downs
ALTER TABLE users
  ALTER COLUMN accepts_terms DROP NOT NULL,
  ALTER COLUMN accepts_terms DROP DEFAULT;

UPDATE users
SET accepts_terms=NULL
WHERE NOT accepts_terms;
