# --- !Ups
ALTER TABLE products
ADD COLUMN last_removed_from_pars TIMESTAMP;

# --- !Downs
ALTER TABLE products
DROP COLUMN last_removed_from_pars;
