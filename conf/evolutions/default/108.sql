# --- !Ups

ALTER TABLE user_info ALTER COLUMN include_photo_in_recipe_print SET DEFAULT TRUE;

# --- !Downs

ALTER TABLE user_info ALTER COLUMN include_photo_in_recipe_print SET DEFAULT FALSE;
