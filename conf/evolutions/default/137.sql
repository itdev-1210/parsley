# --- !Ups
-- speed up the slow consistency checks on product deletion
CREATE INDEX products_sync_upstream_idx
  ON products (sync_upstream);
CREATE INDEX suppliers_sync_upstream_idx
  ON suppliers (sync_upstream);
CREATE INDEX product_sources_sync_upstream_idx
  ON product_sources (sync_upstream);
CREATE INDEX categories_sync_upstream_idx
  ON categories (sync_upstream);

# --- !Downs
DROP INDEX products_sync_upstream_idx;
DROP INDEX suppliers_sync_upstream_idx;
DROP INDEX product_sources_sync_upstream_idx;
DROP INDEX categories_sync_upstream_idx;
