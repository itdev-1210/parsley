# --- !Ups
ALTER TABLE user_info
    ADD COLUMN business_name TEXT DEFAULT NULL;
# --- !Downs
ALTER TABLE user_info
    DROP COLUMN business_name;
