# --- !Ups#
ALTER TABLE user_info
  ADD COLUMN purchase_weight_system measure_systems_type NOT NULL DEFAULT 'imperial',
  ADD COLUMN purchase_volume_system measure_systems_type NOT NULL DEFAULT 'imperial';

# --- !Downs
ALTER TABLE user_info
  DROP COLUMN purchase_weight_system,
  DROP COLUMN purchase_volume_system;