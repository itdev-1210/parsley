# --- !Ups
CREATE TABLE recipes_revision_history (
  id SERIAL PRIMARY KEY,
  product INT REFERENCES recipes(product) NOT NULL,
  owner INT REFERENCES users(id),
  last_modified TIMESTAMP NOT NULL
);

# --- !Downs
DROP TABLE recipes_revision_history;