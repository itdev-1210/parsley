# --- !Ups
ALTER TABLE users
  ADD CONSTRAINT users_unique_email UNIQUE(email),
  ADD COLUMN hasher TEXT,
  ADD COLUMN hashed_password TEXT,
  ADD COLUMN salt TEXT;  -- BCryptHasher happens to include salt inline in hashed_password

# --- !Downs
ALTER TABLE users
  DROP CONSTRAINT users_unique_email,
  DROP COLUMN hasher,
  DROP COLUMN hashed_password,
  DROP COLUMN salt;
