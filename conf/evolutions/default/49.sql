# --- !Ups
ALTER TABLE product_sources
    ADD COLUMN sku TEXT DEFAULT NULL;
# --- !Downs
ALTER TABLE product_sources
    DROP COLUMN sku;
