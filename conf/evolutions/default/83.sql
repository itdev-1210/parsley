# --- !Ups
DELETE FROM product_nutrients pn
USING products p
WHERE p.id = pn.product
    AND p.hidden;

DELETE FROM product_allergens pa
USING products p
WHERE p.id = pa.product
  AND p.hidden;

DELETE FROM product_characteristics pc
USING products p
WHERE p.id = pc.product
      AND p.hidden;

# --- !Downs
INSERT INTO product_nutrients (product, nutrient, amount)
  SELECT prep.output_product, original_nutrient.nutrient, original_nutrient.amount
  FROM products original_product
    JOIN product_nutrients original_nutrient ON original_product.id = original_nutrient.product
    JOIN simple_preparations prep ON original_product.id = prep.input_product;

INSERT INTO product_characteristics (product, added_sugar, meat, pork, corn, poultry, non_edible)
  SELECT prep.output_product,
    original_characteristics.added_sugar,
    original_characteristics.meat,
    original_characteristics.pork,
    original_characteristics.corn,
    original_characteristics.poultry,
    original_characteristics.non_edible
  FROM products original_product
    JOIN product_characteristics original_characteristics ON original_product.id = original_characteristics.product
    JOIN simple_preparations prep ON original_product.id = prep.input_product;

INSERT INTO product_allergens (product, fish, crustacean_shellfish, tree_nuts)
  SELECT prep.output_product,
    original_allergens.fish,
    original_allergens.crustacean_shellfish,
    original_allergens.tree_nuts
  FROM products original_product
    JOIN product_allergens original_allergens ON original_product.id = original_allergens.product
    JOIN simple_preparations prep ON original_product.id = prep.input_product;
