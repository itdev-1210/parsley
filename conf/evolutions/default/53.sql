# --- !Ups
ALTER TABLE order_items
  ALTER CONSTRAINT forecast_measure_defined DEFERRABLE INITIALLY DEFERRED,
  ALTER CONSTRAINT ordered_measure_defined DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE order_items
  ALTER CONSTRAINT forecast_measure_defined NOT DEFERRABLE,
  ALTER CONSTRAINT ordered_measure_defined NOT DEFERRABLE;
