# --- !Ups
ALTER TABLE subscriptions
    ALTER COLUMN plan SET NOT NULL;

# --- !Downs
ALTER TABLE subscriptions
  ALTER COLUMN plan DROP NOT NULL;
