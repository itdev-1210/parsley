# --- !Ups

INSERT INTO onboarding_pages VALUES
  ('receiving_list', 'Receiving List'),
  ('receiving_editor', 'Receiving Editor'),
  ('location_list', 'Location List'),
  ('location_editor', 'Location Editor');

# --- !Downs

DELETE FROM onboarding_steps WHERE page = 'receiving_list';
DELETE FROM onboarding_pages WHERE key = 'receiving_list';
DELETE FROM onboarding_steps WHERE page = 'receiving_editor';
DELETE FROM onboarding_pages WHERE key = 'receiving_editor';
DELETE FROM onboarding_steps WHERE page = 'location_list';
DELETE FROM onboarding_pages WHERE key = 'location_list';
DELETE FROM onboarding_steps WHERE page = 'location_editor';
DELETE FROM onboarding_pages WHERE key = 'location_editor';
