# --- !Ups
CREATE TYPE product_category_type AS ENUM ('recipe', 'ingredient', 'inventory');

ALTER TABLE categories ADD COLUMN category_type product_category_type;

UPDATE categories c
SET category_type = (CASE
             WHEN ingredient THEN 'ingredient'::product_category_type
             WHEN recipe THEN 'recipe'::product_category_type
             WHEN inventory THEN 'inventory'::product_category_type
             ELSE 'inventory'::product_category_type
          END);

ALTER TABLE categories
  ALTER COLUMN category_type SET NOT NULL,
  DROP COLUMN ingredient,
  DROP COLUMN recipe,
  DROP COLUMN inventory;

-- dropping above columns drops the constraints/indices that use them
ALTER TABLE categories
  ADD CONSTRAINT categories_unique UNIQUE(owner, name, category_type);


CREATE UNIQUE INDEX stdlib_categories_uniqueness_idx ON categories (
  name, category_type
) WHERE owner IS NULL;


# --- !Downs
ALTER TABLE categories
  ADD COLUMN ingredient BOOLEAN,
  ADD COLUMN recipe BOOLEAN,
  ADD COLUMN inventory BOOLEAN;

UPDATE categories
SET ingredient = (category_type = 'ingredient'::product_category_type),
    recipe = (category_type = 'recipe'::product_category_type),
    inventory = (category_type = 'inventory'::product_category_type);

ALTER TABLE categories DROP COLUMN category_type;

ALTER TABLE categories
  ADD CONSTRAINT categories_unique UNIQUE(owner, name, ingredient, recipe, inventory);

CREATE UNIQUE INDEX stdlib_categories_uniqueness_idx ON categories (
  name, ingredient, recipe, inventory
) WHERE owner IS NULL;

DROP TYPE product_category_type;
