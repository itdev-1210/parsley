# --- !Ups
ALTER TABLE menu_items
DROP CONSTRAINT menu_items_section_fkey,
ADD COLUMN menu INT NOT NULL DEFAULT 0;

UPDATE menu_items
SET menu=menu_sections.menu
FROM menu_sections
where section = menu_sections.id;

ALTER TABLE menu_sections
DROP CONSTRAINT menu_sections_id_owner_key,
ADD UNIQUE (id, menu, owner);

ALTER TABLE menu_items
ADD FOREIGN KEY (section, menu, owner) REFERENCES menu_sections(id, menu, owner),
ADD UNIQUE (menu, product);

CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT NOT NULL,

  has_forecasts BOOLEAN NOT NULL DEFAULT FALSE,

  menu INT NOT NULL,
  owner INT REFERENCES users NOT NULL,

  FOREIGN KEY (menu, owner) REFERENCES menus(id, owner)
  DEFERRABLE INITIALLY DEFERRED,
  UNIQUE (id, menu, owner)
);

CREATE TABLE order_items (
  id SERIAL PRIMARY KEY,
  product INT NOT NULL,

  forecast_amount REAL,
  forecast_cost DECIMAL,

  ordered_amount REAL,
  ordered_cost DECIMAL, -- not "real cost" - reflects projections of ingredient costs

  "order" INT REFERENCES orders NOT NULL,
  menu INT NOT NULL,
  owner INT REFERENCES users NOT NULL,

  FOREIGN KEY ("order", menu, owner) REFERENCES orders(id, menu, owner)
  DEFERRABLE INITIALLY DEFERRED,

  -- menu_item makes sure product owner == menu owner
  FOREIGN KEY (menu, product) REFERENCES menu_items (menu, product)
  DEFERRABLE INITIALLY DEFERRED
);


ALTER TABLE menus
ADD COLUMN user_visible BOOLEAN NOT NULL DEFAULT TRUE;


# --- !Downs

ALTER TABLE menus DROP COLUMN user_visible;

DROP TABLE order_items;
DROP TABLE orders;

ALTER TABLE menu_items
DROP CONSTRAINT menu_items_section_fkey,
DROP CONSTRAINT menu_items_menu_product_key;

ALTER TABLE menu_sections
DROP CONSTRAINT menu_sections_id_menu_owner_key,
ADD UNIQUE (id, owner);

ALTER TABLE menu_items
DROP COLUMN menu,
ADD FOREIGN KEY (section, owner) REFERENCES menu_sections(id, owner);
