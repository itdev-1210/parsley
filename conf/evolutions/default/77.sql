# --- !Ups
INSERT INTO product_allergens (
  product,
  milk,
  eggs,
  fish,
  crustacean_shellfish,
  tree_nuts,
  wheat,
  peanuts,
  soybeans,
  molluscs,
  cereals_gluten,
  celery,
  mustard,
  sesame_seeds,
  sulphur_dioxide_sulphites,
  lupin
)
  SELECT
    prepped_prod.id,
    a.milk,
    a.eggs,
    a.fish,
    a.crustacean_shellfish,
    a.tree_nuts,
    a.wheat,
    a.peanuts,
    a.soybeans,
    a.molluscs,
    a.cereals_gluten,
    a.celery,
    a.mustard,
    a.sesame_seeds,
    a.sulphur_dioxide_sulphites,
    a.lupin
  FROM product_allergens a
    JOIN products raw_prod ON a.product = raw_prod.id
    JOIN simple_preparations prep ON prep.input_product = raw_prod.id
    JOIN products prepped_prod ON prep.output_product = prepped_prod.id
ON CONFLICT DO NOTHING;

INSERT INTO product_characteristics (
  product,
  added_sugar,
  meat,
  pork,
  corn,
  poultry,
  non_edible
)
  SELECT
    prepped_prod.id,
    c.added_sugar,
    c.meat,
    c.pork,
    c.corn,
    c.poultry,
    c.non_edible
  FROM product_characteristics c
    JOIN products raw_prod ON c.product = raw_prod.id
    JOIN simple_preparations prep ON prep.input_product = raw_prod.id
    JOIN products prepped_prod ON prep.output_product = prepped_prod.id
ON CONFLICT DO NOTHING;

# --- !Downs
