# --- !Ups
ALTER TABLE product_infos
  ADD CONSTRAINT product_infos_pkey PRIMARY KEY (product);

# --- !Downs
ALTER TABLE product_infos
  DROP CONSTRAINT product_infos_pkey;
