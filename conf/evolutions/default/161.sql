# --- !Ups
ALTER TABLE users RENAME TO accounts;
ALTER TABLE user_info RENAME TO users;
ALTER TABLE user_shared_accounts RENAME TO user_accounts;

ALTER TABLE users
  RENAME COLUMN user_id TO id;

# --- !Downs
ALTER TABLE users RENAME TO user_info;
ALTER TABLE user_accounts RENAME TO user_shared_accounts;
ALTER TABLE accounts RENAME TO users;
ALTER TABLE user_info
  RENAME COLUMN id TO user_id;
