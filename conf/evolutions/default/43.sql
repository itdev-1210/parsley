# --- !Ups
CREATE TABLE email_tokens (
  id SERIAL PRIMARY KEY,
  secret TEXT UNIQUE NOT NULL,
  used BOOLEAN NOT NULL DEFAULT FALSE,
  sign_up BOOLEAN NOT NULL, -- sign-up tokens are distinct from password-reset tokens
  expiration TIMESTAMP NOT NULL,
  owner INT REFERENCES users(id) NOT NULL
);

# --- !Downs
DROP TABLE email_tokens;
