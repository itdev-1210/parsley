# -- !Ups
UPDATE product_measures
SET amount = 1, conversion = 1
WHERE measure = conversion_measure; -- where this is the primary conversion

# --- !Downs
-- no downs, format is compatible and up is idempotent
