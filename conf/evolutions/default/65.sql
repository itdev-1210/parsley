# --- !Ups
CREATE TABLE product_allergens (
  product INT PRIMARY KEY REFERENCES products,
  milk BOOLEAN NOT NULL DEFAULT FALSE,
  eggs BOOLEAN NOT NULL DEFAULT FALSE,
  fish TEXT,
  crustacean_shellfish TEXT,
  tree_nuts TEXT,
  wheat BOOLEAN NOT NULL DEFAULT FALSE,
  peanuts BOOLEAN NOT NULL DEFAULT FALSE,
  soybeans BOOLEAN NOT NULL DEFAULT FALSE,
  molluscs BOOLEAN NOT NULL DEFAULT FALSE,
  cereals_gluten BOOLEAN NOT NULL DEFAULT FALSE,
  celery BOOLEAN NOT NULL DEFAULT FALSE,
  mustard BOOLEAN NOT NULL DEFAULT FALSE,
  sesame_seeds BOOLEAN NOT NULL DEFAULT FALSE,
  sulphur_dioxide_sulphites BOOLEAN NOT NULL DEFAULT FALSE,
  lupin BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE product_characteristics (
  product INT PRIMARY KEY REFERENCES products,
  added_sugar BOOLEAN NOT NULL DEFAULT FALSE,
  meat BOOLEAN NOT NULL DEFAULT FALSE,
  pork BOOLEAN NOT NULL DEFAULT FALSE,
  corn BOOLEAN NOT NULL DEFAULT FALSE,
  poultry BOOLEAN NOT NULL DEFAULT FALSE,
  non_edible BOOLEAN NOT NULL DEFAULT FALSE
);

ALTER TABLE products
ADD COLUMN nutrient_serving_amount DECIMAL,
ADD COLUMN nutrient_serving_measure INT,
ADD COLUMN nutrient_serving_unit INT;

ALTER TABLE products
ADD CONSTRAINT nutrient_serving_measure_defined FOREIGN KEY (id, nutrient_serving_measure) REFERENCES product_measures(product, measure) DEFERRABLE INITIALLY DEFERRED,
ADD CONSTRAINT nutrient_serving_valid_units FOREIGN KEY (nutrient_serving_unit, nutrient_serving_measure) REFERENCES units(id, measure) DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
DROP TABLE product_allergens;
DROP TABLE product_characteristics;

ALTER TABLE products
DROP CONSTRAINT nutrient_serving_measure_defined,
DROP CONSTRAINT nutrient_serving_valid_units;

ALTER TABLE products
DROP COLUMN nutrient_serving_amount,
DROP COLUMN nutrient_serving_measure,
DROP COLUMN nutrient_serving_unit;
