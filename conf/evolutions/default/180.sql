# -- !Ups
ALTER TABLE upstream_invites ADD COLUMN invitee_email TEXT;
UPDATE upstream_invites i
SET invitee_email = a.email
  FROM accounts a
WHERE a.id = i.invitee;
ALTER TABLE upstream_invites ALTER COLUMN invitee_email SET NOT NULL;

ALTER TABLE accounts DROP COLUMN email;

# --- !Downs

ALTER TABLE accounts ADD COLUMN email TEXT;

UPDATE accounts a
SET email = u.email
FROM users u
WHERE u.id = a.account_owner;

UPDATE accounts a
SET email = i.invitee_email
FROM upstream_invites i
WHERE i.invitee = a.id AND a.account_owner IS NULL;

ALTER TABLE upstream_invites DROP COLUMN invitee_email;
