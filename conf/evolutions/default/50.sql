# --- !Ups
UPDATE products SET salable = true WHERE id IN (SELECT product FROM recipes);

# --- !Downs
UPDATE products SET salable = false WHERE id IN (SELECT p.product FROM recipes r INNER JOIN product_measures p ON r.product = p.product GROUP BY p.product HAVING COUNT( DISTINCT( p.measure ) ) > 1);
