# --- !Ups
ALTER TABLE transaction_deltas
  ADD COLUMN caused_by INT REFERENCES transaction_deltas(id);

# --- !Downs
ALTER TABLE transaction_deltas
  DROP COLUMN caused_by;
