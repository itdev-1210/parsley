# -- !Ups
ALTER TABLE transactions
  RENAME COLUMN confirmed TO is_finalized;

# --- !Downs
ALTER TABLE transactions
  RENAME COLUMN is_finalized TO confirmed;

