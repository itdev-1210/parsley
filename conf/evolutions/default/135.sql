# --- !Ups

ALTER TABLE transaction_deltas
  ADD COLUMN source_measure INT REFERENCES measures(id),
  ADD COLUMN source_unit INT REFERENCES units(id),
  ADD COLUMN packaged BOOLEAN,
  ADD COLUMN price_per price_per_type,
  ADD COLUMN pricing_measure INT REFERENCES measures(id),
  ADD COLUMN pricing_unit INT REFERENCES units(id),
  ADD COLUMN package_name TEXT,
  ADD COLUMN sub_package_name TEXT,
  ADD COLUMN package_size DOUBLE PRECISION,
  ADD COLUMN super_package_size DOUBLE PRECISION;

UPDATE transaction_deltas SET
  source_measure = ps.measure,
  source_unit = ps.unit,
  packaged = ps.packaged,
  price_per = ps.price_per,
  pricing_measure = ps.pricing_measure,
  pricing_unit = ps.pricing_unit,
  package_name = ps.package_name,
  sub_package_name = ps.sub_package_name,
  package_size = ps.package_size,
  super_package_size = ps.super_package_size
  FROM product_sources ps WHERE product_source = ps.id AND xact IN (
    SELECT finalizedxact
    FROM transactions
    WHERE xact_type = 'purchase-order' AND confirmed = false
  );

CREATE TABLE transaction_measures (
  transaction_delta INT NOT NULL REFERENCES transaction_deltas(id),
  measure INT NOT NULL,
  preferred_unit INT NOT NULL,
  conversion_measure INT NOT NULL,
  conversion_unit INT NOT NULL,
  conversion DOUBLE PRECISION NOT NULL,
  custom_name TEXT
);

INSERT INTO transaction_measures (
    transaction_delta,
    measure,
    preferred_unit,
    conversion_measure,
    conversion_unit,
    conversion,
    custom_name
  ) SELECT td.id, pm.measure, pm.preferred_unit,
  pm.conversion_measure, pm.conversion_unit,
  pm.conversion, pm.custom_name
  FROM product_measures pm INNER JOIN transaction_deltas td
  ON td.product = pm.product AND td.product IS NOT NULL
  WHERE td.xact IN (SELECT finalizedxact FROM transactions WHERE xact_type = 'purchase-order' AND confirmed = false);

# --- !Downs

DROP TABLE transaction_measures;

ALTER TABLE transaction_deltas
  DROP COLUMN source_measure,
  DROP COLUMN source_unit,
  DROP COLUMN packaged,
  DROP COLUMN price_per,
  DROP COLUMN pricing_measure,
  DROP COLUMN pricing_unit,
  DROP COLUMN package_name,
  DROP COLUMN sub_package_name,
  DROP COLUMN package_size,
  DROP COLUMN super_package_size;
