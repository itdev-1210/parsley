# --- !Ups
ALTER TABLE users
  DROP COLUMN shipping_phone_number,
  DROP COLUMN shipping_address,
  DROP COLUMN cc_emails,
  DROP COLUMN business_name,
  DROP COLUMN company_logo;

ALTER TABLE accounts
  DROP COLUMN hasher,
  DROP COLUMN hashed_password,
  DROP COLUMN salt,
  DROP COLUMN first_name,
  DROP COLUMN last_name,
  DROP COLUMN full_name,
  DROP COLUMN permission_level;


-- TODO: clear out 'accounts' that are derived from shared users

# --- !Downs

ALTER TABLE accounts
  ADD COLUMN hasher TEXT,
  ADD COLUMN hashed_password TEXT,
  ADD COLUMN salt TEXT,
  ADD COLUMN first_name TEXT,
  ADD COLUMN last_name TEXT,
  ADD COLUMN full_name TEXT,
  ADD COLUMN permission_level account_permission_level;

ALTER TABLE users
  ADD COLUMN shipping_phone_number TEXT,
  ADD COLUMN shipping_address TEXT,
  ADD COLUMN cc_emails TEXT[] NOT NULL DEFAULT '{}',
  ADD COLUMN business_name TEXT,
  ADD COLUMN company_logo TEXT;

-- restore the dropped columns (copy-pasted from previous migration's ups,
-- but with DB names swapped and e-mail col ignored)
UPDATE accounts a
SET
  hasher = u.hasher,
  hashed_password = u.hashed_password,
  salt = u.salt,
  first_name = u.first_name,
  last_name = u.last_name,
  full_name = u.full_name
FROM users u
WHERE u.id = a.id;

UPDATE accounts a
SET permission_level = ua.permission_level
FROM user_accounts ua
WHERE ua.user_id = a.id; -- note - user_id, not owner!

UPDATE users u
SET
  shipping_phone_number = a.shipping_phone_number,
  shipping_address = a.shipping_address,
  cc_emails = a.cc_emails,
  business_name = a.business_name,
  company_logo = a.company_logo
FROM accounts a
WHERE u.id = a.id;
