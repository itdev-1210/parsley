# --- !Ups
ALTER TABLE onboarding_steps
  RENAME COLUMN style_id TO target_selector;

UPDATE onboarding_steps
SET target_selector = '#' || target_selector
WHERE target_selector IS NOT NULL;

# --- !Downs
ALTER TABLE onboarding_steps
  RENAME COLUMN target_selector TO style_id;

UPDATE onboarding_steps
SET style_id = substring(style_id from 2) -- cut off the '#'
WHERE style_id IS NOT NULL;

UPDATE onboarding_steps
SET style_id = split_part(style_id, ' ', 1) -- full selector entered, take first token
