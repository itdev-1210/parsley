# --- !Ups
ALTER TABLE product_sources
  ADD COLUMN manufacturer_name text,
  ADD COLUMN manufacturer_number text;

# --- !Downs
ALTER TABLE product_sources
  DROP COLUMN manufacturer_name,
  DROP COLUMN manufacturer_number;