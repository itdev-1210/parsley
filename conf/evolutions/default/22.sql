# --- !Ups
ALTER TABLE order_items
  ADD COLUMN ordered_unit INT REFERENCES units(id),
  ADD COLUMN ordered_measure INT REFERENCES measures(id),
  ADD COLUMN forecast_unit INT REFERENCES units(id),
  ADD COLUMN forecast_measure INT REFERENCES measures(id),
  ADD CONSTRAINT ordered_measure_defined FOREIGN KEY (product, ordered_measure)
    REFERENCES product_measures (product, measure),
  ADD CONSTRAINT ordered_valid_unit FOREIGN KEY (ordered_unit, ordered_measure)
    REFERENCES units (id, measure),
  ADD CONSTRAINT forecast_measure_defined FOREIGN KEY (product, forecast_measure)
    REFERENCES product_measures (product, measure),
  ADD CONSTRAINT forecast_valid_unit FOREIGN KEY (forecast_unit, forecast_measure)
    REFERENCES units (id, measure);
# --- !Downs
ALTER TABLE order_items
  DROP CONSTRAINT ordered_measure_defined,
  DROP CONSTRAINT ordered_valid_unit,
  DROP CONSTRAINT forecast_measure_defined,
  DROP CONSTRAINT forecast_valid_unit,
  DROP COLUMN ordered_unit,
  DROP COLUMN ordered_measure,
  DROP COLUMN forecast_unit,
  DROP COLUMN forecast_measure;
