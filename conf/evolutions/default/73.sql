# --- !Ups
UPDATE recipes r SET price = NULL WHERE r.product IN (
  SELECT id FROM products WHERE salable = true AND ingredient = true
);

# --- !Downs
