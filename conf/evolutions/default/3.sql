# --- !Ups
ALTER TABLE user_logins
ADD COLUMN first_name TEXT,
ADD COLUMN last_name TEXT,
ADD COLUMN full_name TEXT;

# --- !Downs
ALTER TABLE user_logins
DROP COLUMN first_name,
DROP COLUMN last_name,
DROP COLUMN full_name;
