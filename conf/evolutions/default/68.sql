# --- !Ups#
ALTER TABLE user_info
  ALTER COLUMN weight_system SET DEFAULT 'imperial',
  ADD COLUMN print_sub_recipe_preferences BOOLEAN DEFAULT true;
# --- !Downs
ALTER TABLE user_info
  ALTER COLUMN weight_system SET DEFAULT 'imperial',
  DROP COLUMN print_sub_recipe_preferences;
