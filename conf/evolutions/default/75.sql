# --- !Ups
INSERT INTO product_nutrients (product, nutrient, amount)
  SELECT prepped_prod.id, pn.nutrient, pn.amount
  FROM product_nutrients pn
    JOIN products raw_prod ON pn.product = raw_prod.id
    JOIN simple_preparations prep ON prep.input_product = raw_prod.id
    JOIN products prepped_prod ON prep.output_product = prepped_prod.id
ON CONFLICT DO NOTHING;

# --- !Downs
