# --- !Ups

CREATE TABLE onboarding_pages (
  key CHAR(120) PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TYPE onboarding_placement AS ENUM ('center', 'top', 'right', 'bottom', 'left');

CREATE TABLE onboarding_steps (
  id SERIAL PRIMARY KEY,
  page CHAR(120) NOT NULL REFERENCES onboarding_pages,
  index SMALLINT NOT NULL
    CHECK (index >= 0),
  placement onboarding_placement NOT NULL, -- placement of box
  style_id TEXT, -- css id of element to place this box
  title TEXT NOT NULL,
  description TEXT NOT NULL,
  left_image TEXT,
  right_image TEXT,
  next_button_text TEXT,
  back_button_text TEXT,
  body_width INT NOT NULL,
  title_bg_color TEXT NOT NULL DEFAULT '80cc28'
    CHECK(
      title_bg_color ~ '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
    ),
  desc_bg_color TEXT NOT NULL DEFAULT 'ffffff'
    CHECK(
      desc_bg_color ~ '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
    ),
  border_color TEXT NOT NULL DEFAULT 'e7e7e7'
    CHECK(
      border_color ~ '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
    ),
  next_bg_color TEXT DEFAULT '80cc28'
    CHECK(
      next_bg_color ~ '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'
    )
);

CREATE TABLE user_onboarding_boxes (
  user_id INT NOT NULL REFERENCES users,
  page CHAR(120) NOT NULL REFERENCES onboarding_pages,
  next_step SMALLINT NOT NULL CHECK(next_step >= 0)
);

CREATE UNIQUE INDEX unique_user_last_visited_step_per_page ON user_onboarding_boxes(user_id, page);


INSERT INTO onboarding_pages VALUES
  ('order_list', 'Order List'),
  ('order_editor', 'Order Editor'),
  ('inventory_list', 'Inventory List'),
  ('inventory_editor', 'Inventory Editor'),
  ('menu_list', 'Menu List'),
  ('menu_editor', 'Menu Editor'),
  ('recipe_list', 'Recipe List'),
  ('recipe_editor', 'Recipe Editor'),
  ('ingredient_list', 'Ingredient List'),
  ('ingredient_editor', 'Ingredient Editor'),
  ('supplier_list', 'Supplier List'),
  ('supplier_editor', 'Supplier Editor'),
  ('account_settings', 'Account Settings'),
  ('measure_settings', 'Measure Settings'),
  ('manage_users', 'Manage Users'),
  ('ingredient_categories', 'Ingredient Categories'),
  ('recipe_categories', 'Recipe Categories');

# --- !Downs

DROP INDEX unique_user_last_visited_step_per_page;

DROP TABLE user_onboarding_boxes;
DROP TABLE onboarding_steps;
DROP TYPE onboarding_placement;
DROP TABLE onboarding_pages;
