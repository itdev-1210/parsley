# --- !Ups
ALTER TABLE purchase_orders
ADD COLUMN imported_at TIMESTAMP,
ADD COLUMN imported BOOLEAN NOT NULL DEFAULT FALSE;


# --- !Downs
ALTER TABLE purchase_orders
DROP COLUMN imported_at,
DROP COLUMN imported;
