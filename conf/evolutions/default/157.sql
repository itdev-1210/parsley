# -- !Ups
UPDATE users SET created_at = 'epoch' WHERE created_at IS NULL;

ALTER TABLE users
  ALTER COLUMN created_at SET NOT NULL;

# --- !Downs
ALTER TABLE users
  ALTER COLUMN created_at DROP NOT NULL;

UPDATE users SET created_at = NULL WHERE created_at = 'epoch';