# --- !Ups
UPDATE product_measures pm
SET custom_name = 'portion'
FROM recipes r, measures m
WHERE pm.product = r.product AND m.id = pm.measure
      AND m.allows_custom_names;

# --- !Downs
UPDATE product_measures pm
SET custom_name = NULL
FROM recipes r, measures m
WHERE pm.product = r.product AND m.id = pm.measure
      AND m.allows_custom_names;
