# --- !Ups

ALTER TABLE users
ADD COLUMN is_admin BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs

ALTER TABLE users
DROP COLUMN is_admin;