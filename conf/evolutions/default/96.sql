# --- !Ups
CREATE TYPE price_per_type AS ENUM ('super-package', 'package', 'unit');

ALTER TABLE product_sources
  ADD COLUMN price_per price_per_type NOT NULL DEFAULT 'super-package',
  ADD COLUMN pricing_measure INT,
  ADD COLUMN pricing_unit INT;

UPDATE product_sources ps
	SET price_per='unit', pricing_measure=ps.measure, pricing_unit=ps.unit
	WHERE ps.packaged=false;

# --- !Downs
ALTER TABLE product_sources
  DROP COLUMN price_per,
  DROP COLUMN pricing_measure,
  DROP COLUMN pricing_unit;

DROP TYPE price_per_type;
