# --- !Ups
ALTER TABLE recipes
  ALTER CONSTRAINT recipe_output_measure_defined DEFERRABLE INITIALLY DEFERRED,
  ALTER CONSTRAINT recipe_max_batch_measure_defined DEFERRABLE INITIALLY DEFERRED,
  ALTER CONSTRAINT recipe_min_batch_measure_defined DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE recipes
  ALTER CONSTRAINT recipe_output_measure_defined NOT DEFERRABLE ,
  ALTER CONSTRAINT recipe_max_batch_measure_defined NOT DEFERRABLE,
  ALTER CONSTRAINT recipe_min_batch_measure_defined NOT DEFERRABLE;
