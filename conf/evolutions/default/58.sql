# --- !Ups
-- index to allow foreign key indexing to work for consistency checking
CREATE UNIQUE INDEX supplier_owner_and_id_for_fks_idx ON suppliers (owner, id);

CREATE TABLE purchase_orders (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users(id),
  xact INT REFERENCES transactions(id),
  supplier INT,
  emailed_at TIME,
  printed_at TIME,

  CONSTRAINT purchase_order_supplier_valid
  FOREIGN KEY (owner, supplier)
  REFERENCES suppliers (owner, id)
);

-- join table preserving causal relationship between customer orders and purchase orders
CREATE TABLE purchase_order_customer_orders (
  -- all customer orders that are relevant to this purchase order
  purchase_order INT NOT NULL REFERENCES purchase_orders(id),
  customer_order INT NOT NULL REFERENCES orders(id)
);

# --- !Downs
DROP TABLE purchase_order_customer_orders;
DROP TABLE purchase_orders;

DROP INDEX supplier_owner_and_id_for_fks_idx
