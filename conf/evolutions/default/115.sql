# --- !Ups
-- direct continuation of 114.sql - couldn't have alter table after update on
-- same table in same transaction
-- keep us under the 22-column limit imposed by Scala tuples
ALTER TABLE products
  DROP COLUMN last_expansion,
  DROP COLUMN last_correction;

# --- !Downs
ALTER TABLE products
  ADD COLUMN last_expansion TIMESTAMP,
  ADD COLUMN last_correction TIMESTAMP;

UPDATE products
SET last_expansion = last_modified,
  last_correction = last_modified;
