# --- !Ups
ALTER TABLE subscriptions
  ADD COLUMN past_due BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE subscriptions
  DROP COLUMN past_due;
