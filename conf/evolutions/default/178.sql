# -- !Ups
ALTER TABLE accounts
  ALTER COLUMN account_owner DROP NOT NULL;

# --- !Downs
ALTER TABLE accounts
  ALTER COLUMN account_owner SET NOT NULL;
