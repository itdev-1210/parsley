# --- !Ups
ALTER TABLE suppliers
  ADD COLUMN sync_upstream INT,
  -- as in products table, cache for easy access by API code
  ADD COLUMN sync_upstream_owner INT,
  ADD CONSTRAINT sync_upstream_valid_owner
    FOREIGN KEY (sync_upstream, sync_upstream_owner)
    REFERENCES suppliers (id, owner);

CREATE INDEX sync_downstream_suppliers
  ON suppliers (sync_upstream);

# --- !Downs
ALTER TABLE suppliers
  DROP COLUMN sync_upstream,
  DROP COLUMN sync_upstream_owner;
