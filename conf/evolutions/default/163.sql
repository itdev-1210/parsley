# --- !Ups

-----------------------------------------------------
-- DISCONNECT accounts and users tables (previously users and user_infos)
-----------------------------------------------------
-- make user IDs sequential and not reference to accounts
CREATE SEQUENCE new_user_id_seq;
ALTER TABLE users ALTER COLUMN id SET DEFAULT nextval('new_user_id_seq');
ALTER SEQUENCE new_user_id_seq OWNED BY users.id;

SELECT setval('new_user_id_seq', max(id)) FROM users;
ALTER TABLE users
  DROP CONSTRAINT user_info_user_id_fkey;

-- some "accounts" were created for shared users that don't actually use most of the info involved;
-- we'll drop those eventually, but they're kind of difficult to clean up, so just mark them for now
ALTER TABLE accounts ADD COLUMN fake_account BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE accounts a
SET fake_account = TRUE
FROM user_accounts ua WHERE ua.shared_account = a.id;

-----------------------------------------------------
-- USER_ACCOUNTS CHANGES
-----------------------------------------------------

-- move permission_level from accounts to user_accounts
ALTER TABLE user_accounts
  ADD COLUMN permission_level account_permission_level;

UPDATE user_accounts ua
SET permission_level = COALESCE(a.permission_level, 'shared'::account_permission_level)
FROM accounts a WHERE a.id = ua.shared_account;

ALTER TABLE user_accounts
  ALTER COLUMN permission_level SET NOT NULL;

-- change semantics of user_accounts - instead of linking two accounts, it links a user and an account.
-- permissions are per (user, account) tuple, and even the admin user has a row (where permission_level = 'shared' i.e. full access)
ALTER TABLE user_accounts
  RENAME COLUMN shared_account TO user_id;
ALTER TABLE user_accounts
  DROP CONSTRAINT user_shared_accounts_shared_account_fkey,
  ADD CONSTRAINT user_accounts_user_fkey FOREIGN KEY (user_id) REFERENCES users (id);

INSERT INTO user_accounts (owner, user_id, is_active, joined_at, permission_level)
SELECT a.id, u.id, NOT a.fake_account, a.created_at, COALESCE(a.permission_level, 'shared'::account_permission_level)
FROM accounts a JOIN users u ON a.id = u.id;

# --- !Downs
-----------------------------------------------------
-- USER_ACCOUNTS CHANGES
-----------------------------------------------------

-- delete the user_accounts entries for account owners
DELETE FROM user_accounts WHERE user_id = owner;

-- and undo schema changes
ALTER TABLE user_accounts
  DROP COLUMN permission_level,
  DROP CONSTRAINT user_accounts_user_fkey,
  ADD CONSTRAINT user_shared_accounts_shared_account_fkey FOREIGN KEY (user_id) REFERENCES accounts (id);
ALTER TABLE user_accounts
  RENAME COLUMN user_id TO shared_account;

ALTER TABLE users ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE new_user_id_seq;
ALTER TABLE users
  ADD CONSTRAINT user_info_user_id_fkey FOREIGN KEY (id) REFERENCES accounts (id);

ALTER TABLE accounts DROP COLUMN fake_account;
