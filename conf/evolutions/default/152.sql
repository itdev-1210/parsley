# --- !Ups
ALTER TABLE recipe_step_ingredients
  ALTER CONSTRAINT recipe_step_ingredients_ingredient_fkey DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE recipe_step_ingredients
  ALTER CONSTRAINT recipe_step_ingredients_ingredient_fkey NOT DEFERRABLE;
