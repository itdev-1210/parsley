# --- !Ups
ALTER TABLE units
ADD COLUMN common_purchase_unit BOOLEAN NOT NULL DEFAULT false;

UPDATE units
SET common_purchase_unit = true
WHERE id IN (
  3,  -- kg for (weight, metric)
  4,  -- pound for (weight, imperial)
  10, -- liter for (volume, metric)
  17  -- gallon for (weight, imperial)
);

# --- !Downs
ALTER TABLE units
DROP COLUMN common_purchase_unit;
