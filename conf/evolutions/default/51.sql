# --- !Ups
ALTER TABLE units
    ADD COLUMN quantum FLOAT NOT NULL DEFAULT 0;
# --- !Downs
ALTER TABLE units
    DROP COLUMN quantum;
