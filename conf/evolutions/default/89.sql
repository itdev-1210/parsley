# --- !Ups
ALTER TABLE products
ADD COLUMN last_added_to_recipe TIMESTAMP,
ADD COLUMN last_removed_from_inventory TIMESTAMP;

# --- !Downs
ALTER TABLE products
DROP COLUMN last_added_to_recipe,
DROP COLUMN last_removed_from_inventory;
