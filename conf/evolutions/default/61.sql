# --- !Ups

-- append 'user-shared-invite' to values of email_token_type
-- ALTER TYPE ... ADD VALUE is not allowed inside transaction
-- so temporarily copy type/column, create new type/column with additional value and then copy from old column

ALTER TYPE email_token_type RENAME TO temp_email_token_type;
CREATE TYPE email_token_type AS ENUM ('email-confirm', 'password-reset', 'user-shared-invite');
ALTER TABLE email_tokens RENAME COLUMN token_type TO temp_token_type;
ALTER TABLE email_tokens ADD token_type email_token_type NOT NULL DEFAULT 'password-reset';
UPDATE email_tokens SET token_type = temp_token_type::text::email_token_type;
ALTER TABLE email_tokens DROP COLUMN temp_token_type;
DROP TYPE temp_email_token_type;

CREATE TABLE user_shared_invites (
  id SERIAL PRIMARY KEY,
  email_token INT NOT NULL REFERENCES email_tokens(id),
  invitee_email TEXT NOT NULL,
  inviter INT NOT NULL REFERENCES users(id)
);
CREATE TABLE user_shared_accounts (
  id SERIAL PRIMARY KEY,
  owner INT NOT NULL REFERENCES users(id),
  shared_account INT NOT NULL REFERENCES users(id),
  is_active BOOLEAN NOT NULL,
  joined_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (owner, shared_account)
);

# --- !Downs

-- remove shared accounts data
ALTER TABLE user_shared_accounts DROP CONSTRAINT user_shared_accounts_shared_account_fkey;
DELETE FROM user_logins WHERE user_id IN (SELECT u.shared_account FROM user_shared_accounts u);
DELETE FROM users WHERE id IN (SELECT u.shared_account FROM user_shared_accounts u);

DROP TABLE user_shared_accounts;
DROP TABLE user_shared_invites;

-- remove 'user-shared-invite' from list of values for email_token_type
DELETE FROM email_tokens WHERE token_type = 'user-shared-invite';
DELETE FROM pg_enum WHERE enumlabel = 'user-shared-invite' AND enumtypid = (SELECT oid FROM pg_type WHERE typname = 'email_token_type');
