# --- !Ups
ALTER TABLE products
  ADD COLUMN last_synced TIMESTAMP,
  ADD COLUMN last_modified TIMESTAMP;

UPDATE products
SET last_synced = GREATEST(last_expansion, last_correction),
  last_modified = GREATEST(last_expansion, last_correction);

ALTER TABLE suppliers
  ADD COLUMN last_synced TIMESTAMP,
  ADD COLUMN last_modified TIMESTAMP;

# --- !Downs
ALTER TABLE products
  DROP COLUMN last_synced;

ALTER TABLE products
  DROP COLUMN last_modified;

ALTER TABLE suppliers
  DROP COLUMN last_synced,
  DROP COLUMN last_modified;
