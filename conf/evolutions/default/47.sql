# --- !Ups
ALTER TABLE units
    ADD COLUMN precision INTEGER NOT NULL DEFAULT 2;
# --- !Downs
ALTER TABLE units
    DROP COLUMN precision;
