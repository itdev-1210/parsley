# --- !Ups

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  email text
);

# --- !Downs

DROP TABLE users;