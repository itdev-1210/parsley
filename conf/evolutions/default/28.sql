# --- !Ups
ALTER TABLE simple_preparations
  ADD CONSTRAINT simple_prep_valid_yield_measure_output
  FOREIGN KEY (output_product, yield_measure)
  REFERENCES product_measures (product, measure)
  DEFERRABLE INITIALLY DEFERRED;

# --- !Downs
ALTER TABLE simple_preparations
  DROP CONSTRAINT simple_prep_valid_yield_measure_output;
