# --- !Ups
ALTER TABLE recipes
  ADD COLUMN advance_prep BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE recipes SET advance_prep = true WHERE product IN (SELECT id FROM products where ingredient = true);

# --- !Downs
ALTER TABLE recipes
  DROP COLUMN advance_prep;
