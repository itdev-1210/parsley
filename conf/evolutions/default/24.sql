# --- !Ups
ALTER TABLE recipe_step_ingredients
    ALTER COLUMN description DROP NOT NULL;

# --- !Downs
ALTER TABLE recipe_step_ingredients
  ALTER COLUMN description SET NOT NULL;
