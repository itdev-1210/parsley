# --- !Ups
-- RENAME COLUMN is a special-case ALTER TABLE action that cannot be listed in comma-separated form
ALTER TABLE recipes RENAME COLUMN min_batch_amount TO quantum_amount;
ALTER TABLE recipes RENAME COLUMN min_batch_unit TO quantum_unit;
ALTER TABLE recipes RENAME COLUMN min_batch_measure TO quantum_measure;

# --- !Downs
ALTER TABLE recipes RENAME COLUMN quantum_amount TO min_batch_amount;
ALTER TABLE recipes RENAME COLUMN quantum_unit TO min_batch_unit;
ALTER TABLE recipes RENAME COLUMN quantum_measure TO min_batch_measure;
