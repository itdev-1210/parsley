# --- !Ups
ALTER TABLE user_info ADD COLUMN break_page_on_recipe_print BOOLEAN NOT NULL DEFAULT false;

# --- !Downs
ALTER TABLE user_info DROP COLUMN break_page_on_recipe_print;
