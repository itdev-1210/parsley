# --- !Ups
ALTER TABLE subscriptions
  ADD COLUMN subscription_level TEXT;

UPDATE subscriptions
SET subscription_level = stripe_plan_id;

ALTER TABLE subscriptions
  ALTER COLUMN subscription_level SET NOT NULL;

# --- !Downs
ALTER TABLE subscriptions
  DROP COLUMN subscription_level;
