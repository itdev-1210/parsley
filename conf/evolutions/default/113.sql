# --- !Ups
ALTER TABLE product_sources
  ADD COLUMN supplier_ingredient_name text;

# --- !Downs
ALTER TABLE product_sources
  DROP COLUMN supplier_ingredient_name;