# --- !Ups
ALTER TABLE product_sources
  ADD COLUMN sync_upstream_owner INT REFERENCES users(id);

UPDATE product_sources source
SET sync_upstream_owner = supplier.sync_upstream_owner
FROM suppliers supplier
WHERE source.sync_upstream IS NOT NULL AND source.supplier = supplier.id;

# --- !Downs
ALTER TABLE product_sources DROP COLUMN sync_upstream_owner;
