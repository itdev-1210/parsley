# -- !Ups
CREATE TABLE pos_import_preferences(
  owner INT PRIMARY KEY REFERENCES accounts,
  item_code_header TEXT NOT NULL,
  date_sold_header TEXT NOT NULL,
  quantity_sold_header TEXT NOT NULL,
  average_price_header TEXT
);
# --- !Downs
DROP TABLE pos_import_preferences;
