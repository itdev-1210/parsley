# --- !Ups
ALTER TABLE products
ADD COLUMN nndbsr_id char(5) UNIQUE;

# --- !Downs
ALTER TABLE products
DROP COLUMN nndbsr_id;
