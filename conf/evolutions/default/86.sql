# --- !Ups 
ALTER TABLE purchase_orders
DROP COLUMN emailed_at,
ADD COLUMN emailed_at TIMESTAMP,
DROP COLUMN printed_at,
ADD COLUMN printed_at TIMESTAMP,
ALTER COLUMN xact SET NOT NULL,
ALTER COLUMN supplier SET NOT NULL;

ALTER TABLE transaction_deltas
ADD COLUMN product_source INT REFERENCES product_sources;

# --- !Downs
ALTER TABLE purchase_orders
DROP COLUMN emailed_at,
ADD COLUMN emailed_at TIME,
DROP COLUMN printed_at,
ADD COLUMN printed_at TIME,
ALTER COLUMN xact DROP NOT NULL,
ALTER COLUMN supplier DROP NOT NULL;

ALTER TABLE transaction_deltas
DROP COLUMN product_source;
