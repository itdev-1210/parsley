# --- !Ups
ALTER TABLE users
  ADD COLUMN multi_location_upstream INT REFERENCES users;

# --- !Downs
ALTER TABLE users DROP COLUMN multi_location_upstream;
