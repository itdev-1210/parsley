# --- !Ups
CREATE TYPE subtract_inventory_type AS ENUM ('subtract-none', 'subtract-current-inventory', 'subtract-latest-inventory');

ALTER TABLE users
  ADD COLUMN subtract_inventory_preference subtract_inventory_type NOT NULL DEFAULT 'subtract-none';

# --- !Downs

ALTER TABLE user_info
  DROP COLUMN subtract_inventory_preference;

DROP TYPE subtract_inventory_type;
