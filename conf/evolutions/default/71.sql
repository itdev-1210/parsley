# --- !Ups
ALTER TABLE nutrients
ADD COLUMN daily_value DECIMAL;

UPDATE nutrients SET daily_value=65 WHERE nndbsr_id='204'; -- Total Fat
UPDATE nutrients SET daily_value=20 WHERE nndbsr_id='606'; -- Saturated Fat
UPDATE nutrients SET daily_value=300 WHERE nndbsr_id='601'; -- Cholesterol
UPDATE nutrients SET daily_value=2400 WHERE nndbsr_id='307'; -- Sodium
UPDATE nutrients SET daily_value=300 WHERE nndbsr_id='205'; -- Total Carbohydrate
UPDATE nutrients SET daily_value=25 WHERE nndbsr_id='291'; -- Dietary Fiber
UPDATE nutrients SET daily_value=10 WHERE nndbsr_id='324'; -- Vitamin D
UPDATE nutrients SET daily_value=1000 WHERE nndbsr_id='301'; -- Calcium
UPDATE nutrients SET daily_value=18 WHERE nndbsr_id='303'; -- Iron
UPDATE nutrients SET daily_value=3500 WHERE nndbsr_id='306'; -- Potassium

# --- !Downs
ALTER TABLE nutrients
DROP COLUMN daily_value;
