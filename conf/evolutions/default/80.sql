# --- !Ups
-- Need extra fields to display past packages
-- Simply saving a source id won't work as product sources can change anytime
ALTER TABLE inventory_items
ALTER COLUMN product SET NOT NULL,
ALTER COLUMN measure SET NOT NULL,
ALTER COLUMN unit SET NOT NULL,
ADD COLUMN cost DOUBLE PRECISION,
ADD COLUMN packaged BOOLEAN NOT NULL DEFAULT FALSE,
ADD COLUMN package_name TEXT,
ADD COLUMN sub_package_name TEXT,
ADD COLUMN package_size DOUBLE PRECISION NOT NULL,
ADD COLUMN super_package_size DOUBLE PRECISION NOT NULL DEFAULT 1,
ADD COLUMN supplier_name TEXT,

-- So past inventory items won't prevent us from modifying corresponding sources & products, etc
DROP CONSTRAINT inventory_item_owner_valid,
DROP CONSTRAINT inventory_item_product_valid,
DROP CONSTRAINT inventory_item_unit_valid,
DROP CONSTRAINT inventory_item_product_measure_valid;

ALTER TABLE inventories
ADD COLUMN date DATE NOT NULL,
ADD CONSTRAINT one_inventory_per_day UNIQUE (owner, date);

# --- !Downs
DELETE FROM inventory_items;
DELETE FROM inventories;

ALTER TABLE inventories
DROP CONSTRAINT one_inventory_per_day,
DROP COLUMN date;

ALTER TABLE inventory_items
ALTER COLUMN product DROP NOT NULL,
ALTER COLUMN measure DROP NOT NULL,
ALTER COLUMN unit DROP NOT NULL,
DROP COLUMN cost,
DROP COLUMN packaged,
DROP COLUMN package_name,
DROP COLUMN sub_package_name,
DROP COLUMN package_size,
DROP COLUMN super_package_size,
DROP COLUMN supplier_name,

ADD CONSTRAINT inventory_item_owner_valid
FOREIGN KEY (inventory, owner) REFERENCES inventories (id, owner),

ADD CONSTRAINT inventory_item_product_valid
FOREIGN KEY (product, owner) REFERENCES products (id, owner),

ADD CONSTRAINT inventory_item_unit_valid
FOREIGN KEY (unit, measure) REFERENCES units (id, measure),

ADD CONSTRAINT inventory_item_product_measure_valid
FOREIGN KEY (product, measure) REFERENCES product_measures (product, measure);
