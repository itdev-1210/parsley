# --- !Ups
ALTER TABLE products 
  ADD COLUMN description text DEFAULT NULL;

# --- !Downs
ALTER TABLE products 
  DROP COLUMN description;
