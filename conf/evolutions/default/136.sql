# --- !Ups
ALTER TABLE categories
  ADD COLUMN inventory BOOLEAN DEFAULT FALSE,
  ADD COLUMN sync_upstream INT REFERENCES categories,
  ALTER COLUMN owner DROP NOT NULL,
  ADD CONSTRAINT categories_unique UNIQUE(owner, name, ingredient, recipe, inventory);

CREATE UNIQUE INDEX stdlib_categories_uniqueness_idx ON categories (
  name, ingredient, recipe, inventory
) WHERE owner IS NULL;

CREATE TABLE product_categories (
  product INT REFERENCES products NOT NULL,
  category INT REFERENCES categories NOT NULL,
  CONSTRAINT product_categories_unique UNIQUE(product, category)
);

-- Migrate tags from the products table to other tables
-- Recipe Categories
INSERT INTO categories (owner, name, ingredient, recipe)
  SELECT owner, unnest(regexp_split_to_array(tags, chr(31))), FALSE, TRUE
  FROM products
  WHERE tags != '' AND id IN (SELECT product FROM recipes)
  ON CONFLICT DO NOTHING;

-- Ingredient Categories
INSERT INTO categories (owner, name, ingredient, recipe)
  SELECT owner, unnest(regexp_split_to_array(tags, chr(31))), TRUE, FALSE
  FROM products
  WHERE tags != '' AND id NOT IN (SELECT product FROM recipes)
  ON CONFLICT DO NOTHING;

INSERT INTO categories (owner, name, ingredient, recipe)
  SELECT owner, recipe_category, FALSE, TRUE
  FROM locations_synced_recipes
  ON CONFLICT DO NOTHING;

-- product_categories
INSERT INTO product_categories (product, category)
  SELECT p.id, c.id
  FROM (SELECT id, owner, unnest(regexp_split_to_array(tags, chr(31))) FROM products WHERE tags != '') p JOIN categories c
  ON
      (c.owner = p.owner -- user case
         OR ((c.owner IS NULL) AND (p.owner IS NULL)) -- stdlib case
      )
        AND c.name = p.unnest;

-- Convert the TEXT recipe_category column of locations_synced_recipes to an INT column
ALTER TABLE locations_synced_recipes
  DROP CONSTRAINT locations_synced_recipes_owner_location_id_recipe_category_key;

ALTER TABLE locations_synced_recipes RENAME recipe_category TO deprecated_category_column;

ALTER TABLE locations_synced_recipes
  ADD COLUMN recipe_category INT REFERENCES categories,
  ADD CONSTRAINT locations_synced_recipes_unique_category UNIQUE(location_id, recipe_category);

UPDATE locations_synced_recipes lsr
SET recipe_category=c.id
FROM categories c
WHERE lsr.owner = c.owner AND lsr.deprecated_category_column = c.name;

ALTER TABLE locations_synced_recipes
  ALTER COLUMN recipe_category SET NOT NULL;

ALTER TABLE products
  DROP COLUMN tags;

# --- !Downs
ALTER TABLE categories
  DROP CONSTRAINT categories_unique,
  DROP COLUMN inventory,
  DROP COLUMN sync_upstream;

-- Restore the tags column of products
ALTER TABLE products
  ADD COLUMN tags TEXT NOT NULL DEFAULT '';

UPDATE products
SET tags=string_agg
FROM (SELECT product, string_agg(DISTINCT name, chr(31)) from (product_categories join categories c on category = c.id) GROUP BY product) pc
WHERE id = pc.product;

-- Restore the TEXT recipe_category column of locations_synced_recipes
DELETE FROM locations_synced_recipes WHERE deprecated_category_column = '';

ALTER TABLE locations_synced_recipes
  DROP CONSTRAINT locations_synced_recipes_unique_category,
  DROP COLUMN recipe_category;
ALTER TABLE locations_synced_recipes RENAME deprecated_category_column TO recipe_category;
ALTER TABLE locations_synced_recipes ADD UNIQUE (owner, location_id, recipe_category);

DROP TABLE product_categories;

DELETE FROM categories WHERE owner IS NULL;
ALTER TABLE categories
  ALTER COLUMN owner SET NOT NULL;
