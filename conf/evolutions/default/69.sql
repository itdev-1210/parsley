# --- !Ups

-- list the products to kill off
CREATE TEMP TABLE stdlib_bad_prods
(id INT PRIMARY KEY)
ON COMMIT DROP;

INSERT INTO stdlib_bad_prods
  SELECT p.id
  FROM products p LEFT OUTER JOIN recipes r ON p.id = r.product
  WHERE
    p.owner IS NULL AND -- is stdlib
    (not p.ingredient OR r.product IS NOT NULL); -- non-ingredient, or has recipe

CREATE TEMP TABLE stdlib_bad_copies
(id INT PRIMARY KEY)
ON COMMIT DROP;

INSERT INTO stdlib_bad_copies
  SELECT p.id
  FROM products p
  WHERE p.stdlib_original IN (SELECT id FROM stdlib_bad_prods);

CREATE TEMP VIEW all_bad_prods
AS (SELECT id from stdlib_bad_prods) UNION (SELECT id FROM stdlib_bad_copies);

DELETE FROM recipe_step_ingredients ingredient
USING recipe_steps step
WHERE ingredient.step = step.id AND
      step.product IN (SELECT id from all_bad_prods);

DELETE FROM recipe_steps
WHERE product IN (SELECT id from all_bad_prods);

DELETE FROM recipes
WHERE product IN (SELECT id from all_bad_prods);

DELETE FROM product_measures
WHERE product IN (SELECT id from all_bad_prods);

-- delete the copies first to avoid foreign key errors from deleting the stdlib_original first
DELETE FROM products
WHERE id IN (SELECT id from stdlib_bad_copies);

DELETE FROM products
WHERE id IN (SELECT id FROM stdlib_bad_prods);

# --- !Downs
