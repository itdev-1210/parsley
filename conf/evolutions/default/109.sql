# --- !Ups
ALTER TABLE users
  ADD COLUMN upstream_relative_name TEXT;

# --- !Downs
ALTER TABLE users
  DROP COLUMN upstream_relative_name;
