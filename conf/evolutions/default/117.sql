# --- !Ups
ALTER TABLE product_sources
  -- no timestamp, will rely on supplier timestamps. used purely
  -- for matching up values while syncing, not selecting set of items to sync
  ADD COLUMN sync_upstream INT
    REFERENCES product_sources
    ON DELETE SET NULL; -- don't delete information that the downstream may be using

# --- !Downs
ALTER TABLE product_sources
  DROP COLUMN sync_upstream;
