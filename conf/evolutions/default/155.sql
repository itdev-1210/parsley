# --- !Ups
CREATE TABLE recipe_nutrients (
  product INT PRIMARY KEY REFERENCES products,
  packaged BOOLEAN NOT NULL DEFAULT False,
  portion_amount DECIMAL,
  portion_measure INT REFERENCES measures(id),
  portion_unit INT REFERENCES units(id),
  portions_per_package DECIMAL,
  servings_per_package DECIMAL,
  serving_name TEXT,
  -- avoid using `nutrient_serving` fields on `products`
  -- due to the `nutrient_serving_measure_defined` constraint
  serving_amount DECIMAL,
  serving_measure INT REFERENCES measures(id),
  serving_unit INT REFERENCES units(id),

  CONSTRAINT portion_unit_valid
  FOREIGN KEY (portion_unit, portion_measure) REFERENCES units (id, measure),

  CONSTRAINT serving_unit_valid
  FOREIGN KEY (serving_unit, serving_measure) REFERENCES units (id, measure)
);

INSERT INTO recipe_nutrients (product, packaged, serving_amount)
SELECT p.id, FALSE, p.nutrient_serving_amount
FROM products p LEFT JOIN recipes r ON p.id = r.product WHERE p.ingredient = FALSE;

# --- !Downs
DROP TABLE recipe_nutrients;
