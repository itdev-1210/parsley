# --- !Ups
ALTER TABLE users
  ADD COLUMN first_name text,
  ADD COLUMN last_name text,
  ADD COLUMN full_name text;

UPDATE users u
SET
  first_name=ul.first_name,
  last_name=ul.last_name,
  full_name=ul.full_name
FROM user_logins ul
WHERE ul.user_id=u.id;

ALTER TABLE user_logins
    ADD COLUMN email text;

UPDATE user_logins ul
SET email=u.email
FROM users u
WHERE ul.user_id=u.id;

# --- !Downs
UPDATE users
  SET first_name=NULL, last_name=NULL, full_name=NULL;

ALTER TABLE users
  DROP COLUMN first_name,
  DROP COLUMN last_name,
  DROP COLUMN full_name;

UPDATE user_logins
  SET email=NULL;

ALTER TABLE user_logins
  DROP COLUMN email;
