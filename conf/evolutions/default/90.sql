# --- !Ups
ALTER TABLE products
ADD COLUMN photo TEXT;

ALTER TABLE recipe_steps
ADD COLUMN photo TEXT;

# --- !Downs
ALTER TABLE products
DROP COLUMN photo;

ALTER TABLE recipe_steps
DROP COLUMN photo;
