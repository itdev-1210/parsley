# --- !Ups
CREATE TABLE recipes (
  product INT PRIMARY KEY REFERENCES products,

  output_amount REAL NOT NULL,
  output_measure INT REFERENCES measures,
  output_unit INT references units,

  min_batch_amount REAL,
  min_batch_measure INT REFERENCES measures,
  min_batch_unit INT references units,

  max_batch_amount REAL,
  max_batch_measure INT REFERENCES measures,
  max_batch_unit INT references units,

  CONSTRAINT recipe_output_measure_defined
  FOREIGN KEY (product, output_measure)
  REFERENCES product_measures (product, measure),

  CONSTRAINT recipe_output_valid_units
  FOREIGN KEY (output_unit, output_measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED,

  CONSTRAINT recipe_min_batch_measure_defined
  FOREIGN KEY (product, min_batch_measure)
  REFERENCES product_measures (product, measure),

  CONSTRAINT recipe_min_batch_valid_units
  FOREIGN KEY (min_batch_unit, min_batch_measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED,

  CONSTRAINT recipe_max_batch_measure_defined
  FOREIGN KEY (product, max_batch_measure)
  REFERENCES product_measures (product, measure),

  CONSTRAINT recipe_max_batch_valid_units
  FOREIGN KEY (max_batch_unit, max_batch_measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED

);

COMMENT ON COLUMN recipes.output_measure IS
'null for the very common case of recipes measuring output in "portions"';

COMMENT ON COLUMN recipes.min_batch_measure IS
'must be null if output_measure is null';

COMMENT ON COLUMN recipes.max_batch_measure IS
'must be null if output_measure is null';


CREATE TABLE recipe_steps (
  id SERIAL PRIMARY KEY,

  product INT REFERENCES recipes NOT NULL,
  display_order INT NOT NULL,
  UNIQUE (product, display_order) DEFERRABLE INITIALLY DEFERRED, -- also gives us an index

  description TEXT NOT NULL DEFAULT ''
);

CREATE TABLE recipe_step_ingredients (
  id SERIAL PRIMARY KEY,

  step INT REFERENCES recipe_steps NOT NULL,
  display_order INT NOT NULL,
  UNIQUE (step, display_order) DEFERRABLE INITIALLY DEFERRED, -- also gives us an index

  ingredient INT REFERENCES products NOT NULL ,
  description TEXT NOT NULL DEFAULT '',

  amount REAL NOT NULL,
  measure INT REFERENCES measures NOT NULL,
  unit INT REFERENCES units NOT NULL,

  CONSTRAINT recipe_step_ingredients_valid_units
  FOREIGN KEY (unit, measure)
  REFERENCES units (id, measure)
  DEFERRABLE INITIALLY DEFERRED,

  CONSTRAINT recipe_step_ingredients_measure_defined
  FOREIGN KEY (ingredient, measure)
  REFERENCES product_measures (product, measure)
  DEFERRABLE INITIALLY DEFERRED
);
-- enables fast server-side aggregation of ingredients by measure if we want - conversions between units
-- in the same measure are SQL-izable
CREATE INDEX ON recipe_step_ingredients (ingredient, measure);

# --- !Downs

drop table recipe_step_ingredients;
drop table recipe_steps;
drop table recipes;
