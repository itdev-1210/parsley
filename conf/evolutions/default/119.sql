# --- !Ups

ALTER TABLE user_info
ADD COLUMN include_nutrition_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE user_info
ADD COLUMN include_cost_breakdown_in_multiple_recipe_print BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs

ALTER TABLE user_info
DROP COLUMN include_nutrition_in_multiple_recipe_print;
ALTER TABLE user_info
DROP COLUMN include_cost_breakdown_in_multiple_recipe_print;
