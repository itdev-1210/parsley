# --- !Ups
ALTER TABLE units 
    ADD COLUMN scale_up_limit DOUBLE PRECISION DEFAULT NULL;

ALTER TABLE units
    ADD COLUMN scale_down_limit DOUBLE PRECISION DEFAULT NULL;

# --- !Downs
ALTER TABLE units
    DROP COLUMN scale_up_limit;

ALTER TABLE units
    DROP COLUMN scale_down_limit;
