# --- !Ups
ALTER TABLE users
  ADD COLUMN email TEXT UNIQUE,
  ADD COLUMN hasher TEXT,
  ADD COLUMN hashed_password TEXT,
  ADD COLUMN salt TEXT,
  ADD COLUMN first_name TEXT,
  ADD COLUMN last_name TEXT,
  ADD COLUMN full_name TEXT;

ALTER TABLE accounts
  ADD COLUMN shipping_phone_number TEXT,
  ADD COLUMN shipping_address TEXT,
  ADD COLUMN cc_emails TEXT[] NOT NULL DEFAULT '{}',
  ADD COLUMN business_name TEXT,
  ADD COLUMN company_logo TEXT,
  ADD COLUMN account_owner INT;

UPDATE accounts a
SET
  shipping_phone_number = u.shipping_phone_number,
  shipping_address = u.shipping_address,
  cc_emails = u.cc_emails,
  business_name = u.business_name,
  company_logo = u.company_logo,
  account_owner = u.id
FROM users u
WHERE u.id = a.id;

ALTER TABLE accounts
  ALTER COLUMN account_owner SET NOT NULL,
  ADD FOREIGN KEY (account_owner) REFERENCES users (id);

UPDATE users u
SET
  email = a.email,
  hasher = a.hasher,
  hashed_password = a.hashed_password,
  salt = a.salt,
  first_name = a.first_name,
  last_name = a.last_name,
  full_name = a.full_name
FROM accounts a
WHERE u.id = a.id;

-----------------------------------------------------
-- MISCELLANEOUS TABLES
-----------------------------------------------------
-- change up the foreign keys that should be pointing to users rather than accounts
ALTER TABLE recipes_revision_history
  DROP CONSTRAINT recipes_revision_history_owner_fkey,
  ADD CONSTRAINT recipes_revision_history_owner_fkey FOREIGN KEY (owner) REFERENCES users (id);

ALTER TABLE purchase_orders
  DROP CONSTRAINT purchase_orders_created_by_fkey,
  DROP CONSTRAINT purchase_orders_received_by_fkey,
  ADD CONSTRAINT purchase_orders_created_by_fkey FOREIGN KEY (created_by) REFERENCES users (id),
  ADD CONSTRAINT purchase_orders_received_by_fkey FOREIGN KEY (received_by) REFERENCES users (id);

ALTER TABLE user_logins
  DROP CONSTRAINT user_logins_user_id_fkey,
  ADD CONSTRAINT user_logins_user_id_fkey FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE user_onboarding_boxes
  DROP CONSTRAINT user_onboarding_boxes_user_id_fkey,
  ADD CONSTRAINT user_onboarding_boxes_user_id_fkey FOREIGN KEY (user_id) REFERENCES users (id);

# --- !Downs

-----------------------------------------------------
-- MISCELLANEOUS TABLES
-----------------------------------------------------

-- revert foreign key changes
ALTER TABLE recipes_revision_history
  DROP CONSTRAINT recipes_revision_history_owner_fkey,
  ADD CONSTRAINT recipes_revision_history_owner_fkey FOREIGN KEY (owner) REFERENCES accounts (id);

ALTER TABLE purchase_orders
  DROP CONSTRAINT purchase_orders_created_by_fkey,
  DROP CONSTRAINT purchase_orders_received_by_fkey,
  ADD CONSTRAINT purchase_orders_created_by_fkey FOREIGN KEY (created_by) REFERENCES accounts (id),
  ADD CONSTRAINT purchase_orders_received_by_fkey FOREIGN KEY (received_by) REFERENCES accounts (id);

ALTER TABLE user_logins
  DROP CONSTRAINT user_logins_user_id_fkey,
  ADD CONSTRAINT user_logins_user_id_fkey FOREIGN KEY (user_id) REFERENCES accounts (id);

ALTER TABLE user_onboarding_boxes
  DROP CONSTRAINT user_onboarding_boxes_user_id_fkey,
  ADD CONSTRAINT user_onboarding_boxes_user_id_fkey FOREIGN KEY (user_id) REFERENCES accounts (id);


----------------------------------------------------
-- ACCOUNTS AND USERS TABLE MODIFICATIONS
----------------------------------------------------
ALTER TABLE accounts
  DROP COLUMN shipping_phone_number,
  DROP COLUMN shipping_address,
  DROP COLUMN cc_emails,
  DROP COLUMN business_name,
  DROP COLUMN company_logo,
  DROP COLUMN account_owner;

ALTER TABLE users
  DROP COLUMN email,
  DROP COLUMN hasher,
  DROP COLUMN hashed_password,
  DROP COLUMN salt,
  DROP COLUMN first_name,
  DROP COLUMN last_name,
  DROP COLUMN full_name;
