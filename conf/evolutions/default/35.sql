# --- !Ups
ALTER TABLE user_info
  ADD COLUMN food_cost_percent NUMERIC;

ALTER TABLE recipes
  ADD COLUMN price NUMERIC;

# --- !Downs
ALTER TABLE user_info
  DROP COLUMN food_cost_percent;

ALTER TABLE recipes
  DROP COLUMN price; 
