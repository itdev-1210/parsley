# --- !Ups
-- list the xacts to kill off
CREATE TEMP TABLE orphan_xacts
(id INT PRIMARY KEY)
ON COMMIT DROP;

INSERT INTO orphan_xacts
  SELECT id
  FROM transactions
  WHERE
    id NOT IN (SELECT xact from orders) AND
    id NOT IN (SELECT xact from purchase_orders);

DELETE FROM transaction_deltas delta
USING orphan_xacts todelete
WHERE delta.xact = todelete.id;

DELETE from transactions xact
USING orphan_xacts todelete
WHERE xact.id = todelete.id;

# --- !Downs
