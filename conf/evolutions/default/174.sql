# -- !Ups
ALTER TABLE pos_import_preferences
ADD COLUMN name_header TEXT;
# --- !Downs
ALTER TABLE pos_import_preferences
DROP COLUMN name_header;
