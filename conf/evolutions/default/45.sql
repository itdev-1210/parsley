# --- !Ups
ALTER TABLE menu_sections
    ALTER COLUMN name DROP NOT NULL;

# --- !Downs
ALTER TABLE menu_sections
  ALTER COLUMN name SET NOT NULL;
