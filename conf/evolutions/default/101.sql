# --- !Ups
ALTER TABLE ingredient_pars
  ALTER CONSTRAINT inventory_item_product_measure_valid DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE ingredient_pars
  RENAME CONSTRAINT inventory_item_product_measure_valid TO ingredient_pars_product_measure_valid;
ALTER TABLE ingredient_pars
  RENAME CONSTRAINT inventory_item_product_valid TO ingredient_pars_product_valid;
ALTER TABLE ingredient_pars
  RENAME CONSTRAINT inventory_item_unit_valid TO ingredient_pars_unit_valid;

# --- !Downs
ALTER TABLE ingredient_pars
  RENAME CONSTRAINT ingredient_pars_product_measure_valid TO inventory_item_product_measure_valid;
ALTER TABLE ingredient_pars
  RENAME CONSTRAINT ingredient_pars_product_valid TO inventory_item_product_valid;
ALTER TABLE ingredient_pars
  RENAME CONSTRAINT ingredient_pars_unit_valid TO inventory_item_unit_valid;

ALTER TABLE ingredient_pars
  ALTER CONSTRAINT inventory_item_product_measure_valid NOT DEFERRABLE;
