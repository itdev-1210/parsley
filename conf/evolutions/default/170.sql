# -- !Ups
UPDATE transaction_deltas
SET amount = -amount
WHERE caused_by IS NOT NULL;

# --- !Downs
UPDATE transaction_deltas
SET amount = -amount
WHERE caused_by IS NOT NULL;

