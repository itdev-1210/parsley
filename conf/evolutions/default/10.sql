# --- !Ups
COMMENT ON COLUMN products.tags IS
'separated by \u001F ("information separator 1" - purpose-built '
'delimiter character). same namespace for all types of products';

# --- !Downs
COMMENT ON COLUMN products.tags IS
'each prefixed by semicolon. same namespace for all types of'
'products';
