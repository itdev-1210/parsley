# --- !Ups
UPDATE products prepped
  SET
    (nutrient_serving_amount, nutrient_serving_measure, nutrient_serving_unit)
    =
    (raw.nutrient_serving_amount, raw.nutrient_serving_measure, raw.nutrient_serving_unit)
  FROM products raw, simple_preparations prep, product_measures m
  WHERE raw.id = prep.input_product and prepped.id = prep.output_product
        AND m.product=prep.output_product and m.measure=raw.nutrient_serving_measure
        AND prepped.nutrient_serving_amount IS NULL AND prepped.nutrient_serving_unit IS NULL;

# --- !Downs
