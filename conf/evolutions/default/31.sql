# --- !Ups
ALTER TABLE users
  ADD COLUMN stripe_email TEXT,
  ADD COLUMN stripe_customer_id TEXT,
  ADD COLUMN accepts_terms BOOL,
  ADD COLUMN promo_code TEXT,
  ADD COLUMN plan TEXT;

CREATE TABLE subscriptions (
  id SERIAL PRIMARY KEY,
  user_id INT REFERENCES users(id),
  active BOOL NOT NULL DEFAULT TRUE,
  stripe_subscription_id TEXT,
  plan TEXT,
  plan_started TIMESTAMP,
  plan_valid_until TIMESTAMP,
  promo_code TEXT
);

CREATE TABLE flags (
  user_id INT PRIMARY KEY REFERENCES users(id),
  subscription_chosen BOOL NOT NULL DEFAULT FALSE
);

# --- !Downs
ALTER TABLE users
  DROP COLUMN stripe_email,
  DROP COLUMN stripe_customer_id,
  DROP COLUMN accepts_terms,
  DROP COLUMN promo_code,
  DROP COLUMN plan;

DROP TABLE subscriptions;

DROP TABLE flags;
