# --- !Ups
CREATE TABLE categories (
  id SERIAL PRIMARY KEY,
  owner INT REFERENCES users NOT NULL,
  name TEXT NOT NULL,
  ingredient BOOLEAN,
  recipe BOOLEAN
);

# --- !Downs
DROP TABLE categories;
