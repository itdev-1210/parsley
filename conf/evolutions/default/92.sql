# --- !Ups
ALTER TABLE purchase_orders ADD COLUMN received_by INT REFERENCES users;
UPDATE purchase_orders SET received_by = owner WHERE emailed_at IS NOT NULL OR printed_at IS NOT NULL;

# --- !Downs
ALTER TABLE purchase_orders DROP COLUMN received_by;
