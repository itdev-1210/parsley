# --- !Ups
UPDATE product_sources
SET super_package_size = 1
WHERE super_package_size IS NULL;

ALTER TABLE product_sources
  ALTER COLUMN super_package_size SET DEFAULT 1,
  ALTER COLUMN super_package_size SET NOT NULL;

# --- !Downs
ALTER TABLE product_sources
  ALTER COLUMN super_package_size DROP NOT NULL,
  ALTER COLUMN super_package_size DROP DEFAULT;

UPDATE product_sources
SET super_package_size = NULL
WHERE NOT sub_package_name IS NULL;
