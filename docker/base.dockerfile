# possible future improvement: make jessie-based 8-jdk
FROM openjdk:8u141-jre
# MACRO-REPLACE to git username
MAINTAINER Asa Zernik <asa@parsleycooks.com>

ENV server_port 9000
EXPOSE $server_port

RUN ["useradd", "--create-home", "app"]
