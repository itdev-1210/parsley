server {
    listen      80;
    server_name parsleycooks.com;
    include     conf.d/servername.include;
    return      301 https://$hostname.parsleycooks.com$request_uri;
}

server {
    listen      443 ssl;
    server_name parsleycooks.com;
    return      301 https://$hostname.parsleycooks.com$request_uri;
}

upstream backend {
  keepalive 12;
  ## 'server X.X.X.X max_conns=N' or 'server localhost:80 down'
  include             conf.d/play_proxy.include;
}

server {
    listen  443 ssl;
    include conf.d/servername.include;
    client_max_body_size 2G;

    location / {
        location ~ ^/api/(global_stdlib_sync|nndbsr_import|locations/) {
          proxy_read_timeout 10m;
          proxy_pass http://backend;
        }

        proxy_pass http://backend;
    }

    # For ACME/letsencrypt usage
    location /.well-known {
        root /usr/share/nginx/html/;
    }
}

# Drawn from https://wiki.mozilla.org/Security/Server_Side_TLS#Nginx
# certs sent to the client in SERVER HELLO are concatenated in ssl_certificate
ssl_certificate /etc/ssl/certs/letsencrypt/fullchain.pem;
ssl_certificate_key /etc/ssl/certs/letsencrypt/privkey.pem;
ssl_session_timeout 5m;
ssl_session_cache shared:SSL:5m;

# Diffie-Hellman parameter for DHE ciphersuites, recommended 2048 bits
ssl_dhparam /etc/ssl/certs/dhparam.pem;

# Intermediate configuration. tweak to your needs.
ssl_protocols TLSv1.1 TLSv1.2;
ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK;
ssl_prefer_server_ciphers on;

add_header Strict-Transport-Security max-age=15768000;

# OCSP Stapling ---
# fetch OCSP records from URL in ssl_certificate and cache them
ssl_stapling on;
ssl_stapling_verify on;
## verify chain of trust of OCSP response using Root CA and Intermediate certs
resolver 8.8.8.8 8.8.4.4;

## Proxy settings; drawn from:
## https://www.playframework.com/documentation/2.4.x/HTTPServer
proxy_set_header    X-Real-IP $remote_addr;
proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header    X-Forwarded-Proto $scheme;
proxy_set_header    X-Forwarded-Ssl $https;
proxy_set_header    X-Scheme $scheme;
proxy_set_header    Host $hostname.parsleycooks.com;
proxy_set_header    Upgrade $http_upgrade;
proxy_http_version  1.1;

# enable keepalive for proxy/app conns
proxy_set_header Connection ""; # default is "close"
