
declare module lunr {

  declare type TokenUpdateFunction<M> = (string, metadata: M) => string

  declare export class Token<M> {

    str: string;
    metadata: M;

    constructor(str: string, metadata: M): Token<M>;

    toString(): string;

    update(TokenUpdateFunction<M>): Token<M>;

    clone(?TokenUpdateFunction<M>): Token<M>;
  }

  declare export class Query {
    static wildcard: {
      NONE: number,
      LEADING: number,
      TRAILING: number,
    };

    term<M>(term: (Token<M> | Array<Token<M>>), opts: *): Query;
  }

  declare export type QueryBuilder = Query => void;

  declare export type Result = {
    ref: string,
    score: number,
    matchData: $FlowTODO,
  }

  declare export class Index {
    query(QueryBuilder): Array<Result>;

    toJSON(): JSON;
  }

  declare export type PipelineFunction<M> = (t: Token<M>, i: number, tokens: Array<Token<M>>) =>
      ?(Token<M> | Array<Token<M>>)

  declare export class Pipeline {
    static registerFunction<M>(PipelineFunction<M>, string): void;

    add<M>(...Array<PipelineFunction<M>>): void;

  }

  declare var utils: {
    asString(any): string;
    clone(any): any;
  };

  declare export type Tokenizer<O, M> = (object: O, metadata: M) => Array<Token<M>>;
  declare var tokenizer: Tokenizer<mixed, any> & { separator: RegExp };

  /*
   * default pipeline functions
   */
  declare var stemmer: (t: Token<any>) => Token<any>;

  declare export default ((void => void) => Index);
}
