The main Parsley repo.

## Contents

Layout is as follows

### Scripts

Python scripts are in `script/`; some of these are for devel usage, some
are for the usage of the deploy system. Deploy scripts *should* refuse
to run if you try to run them on your devel system by accident, if there
are no bugs. I'm sure there are bugs.

### Scala code (i.e. server-side stuff)

App code lives in `app/`, with the exception of `app/assets/\*` (see
below). Special notes:

* app/views contains Twirl templates, which get preprocessed into Scala
  classes.
* `app/models/Tables.scala` contains code generated from the database
  automatically; do not edit it manually.

Test code lives in `test/`.

### JSX/LESS code (i.e. pre-processed browser-side stuff).

Everything in `app/assets/` is pre-processed in various ways:

* JS/JSX files are pre-processed by webpack. In development, this means babel
  transpilation with `NODE_ENV="development"`; in production, this means babel
  transpilation with `NODE_ENV="production"`, followed by UglifyJs minification,
  followed by gzip compression.
* LESS is preprocessed into CSS by webpack.

Both types of code get to reference libraries held in node modules; LESS code
through `@import $LIB_NAME/path/to/foo.less`, and JSX code with `import
someIdent from $LIBNAME`.

Assets are split into `app/assets/app` for the Parsley app itself,
`app/assets/auth` for the login/signup page, `app/assets/plan` for the
sign-up flow app, and `app/assets/common` for libraries available to both.
Those shared libaries are accessible at the import path `common/*`.

### Static assets (images only, for now)

In `public/\*`.

### Configuration

`conf/application\*.conf` contains Play Framework configurations;
`application.conf` contains general and devel-only options,
`application.test.conf` overrides those options for the test and prod environments, and
`application.prod.conf` overrides `application.test.conf` for the
production environment only.

### Database schemas

`conf/evolutions/default` contains migrations (ups/downs) for the
database.

## IDE setup

### IntelliJ

1. Make sure Play Framework support is installed
1. Add as an SBT project.
1. Set the Javascript dialect to "JSX Harmony"
1. If you're going to be doing stuff with the database, set the SQL dialect for
   the evolutions directory to PostgreSQL, and the data source to
   localhost:6543, user postgres, database postgres

### Eclipse setup - NOT WORKED OUT

Not well tested - this is stuff I (Asa) did last time I tried setting
this up. And it didn't work too well.

1. Add `target/scala-2.11/classes_managed` as a library dependency.
2. Remove the deprecated `play.api.templates._` as an auto-include for
   Eclipse purposes in templates.
