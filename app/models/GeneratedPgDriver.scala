package models

object ProductCategoryType extends Enumeration {
  val Recipe = Value("recipe")
  val Ingredient = Value("ingredient")
  val Inventory = Value("inventory")
}


object EmailTokenType extends Enumeration {
  val EmailConfirm = Value("email-confirm")
  val PasswordReset = Value("password-reset")
  val UserSharedInvite = Value("user-shared-invite")
  val UpstreamInvite = Value("upstream-invite")
}


object SubtractInventoryType extends Enumeration {
  val SubtractNone = Value("subtract-none")
  val SubtractCurrentInventory = Value("subtract-current-inventory")
  val SubtractLatestInventory = Value("subtract-latest-inventory")
}


object MeasureSystemsType extends Enumeration {
  val Imperial = Value("imperial")
  val Metric = Value("metric")
}


object PricePerType extends Enumeration {
  val SuperPackage = Value("super-package")
  val Package = Value("package")
  val Unit = Value("unit")
}


object AccountPermissionLevel extends Enumeration {
  val RecipeReadOnly = Value("recipe-read-only")
  val Shared = Value("shared")
  val Operations = Value("operations")
}


object OnboardingPlacement extends Enumeration {
  val Center = Value("center")
  val Top = Value("top")
  val Right = Value("right")
  val Bottom = Value("bottom")
  val Left = Value("left")
}


object RecipePrintFormatType extends Enumeration {
  val StepByStep = Value("step-by-step")
  val IngredientsFirst = Value("ingredients-first")
}


object TransactionType extends Enumeration {
  val CustomerOrder = Value("customer-order")
  val PurchaseOrder = Value("purchase-order")
}


object ChangeType extends Enumeration {
  val PriceChange = Value("price-change")
  val Receiving = Value("receiving")
}


// AUTO-GENERATED Slick profile
trait GeneratedPgDriver extends com.parsleycooks.pgdriver.PgDriver {

  trait GeneratedPgDriverAPI extends PgDriverAPI {
    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)

    implicit val TransactionTypeTypeMapper = createEnumJdbcType("transaction_type", TransactionType)
    implicit val TransactionTypeListTypeMapper = createEnumListJdbcType("transaction_type", TransactionType)
    implicit val TransactionTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(TransactionType)
    implicit val TransactionTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(TransactionType)


implicit val ProductCategoryTypeTypeMapper = createEnumJdbcType("product_category_type", ProductCategoryType)
    implicit val ProductCategoryTypeListTypeMapper = createEnumListJdbcType("product_category_type", ProductCategoryType)
    implicit val ProductCategoryTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(ProductCategoryType)
    implicit val ProductCategoryTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(ProductCategoryType)


implicit val AccountPermissionLevelTypeMapper = createEnumJdbcType("account_permission_level", AccountPermissionLevel)
    implicit val AccountPermissionLevelListTypeMapper = createEnumListJdbcType("account_permission_level", AccountPermissionLevel)
    implicit val AccountPermissionLevelColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(AccountPermissionLevel)
    implicit val AccountPermissionLevelOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(AccountPermissionLevel)


implicit val EmailTokenTypeTypeMapper = createEnumJdbcType("email_token_type", EmailTokenType)
    implicit val EmailTokenTypeListTypeMapper = createEnumListJdbcType("email_token_type", EmailTokenType)
    implicit val EmailTokenTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(EmailTokenType)
    implicit val EmailTokenTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(EmailTokenType)


implicit val SubtractInventoryTypeTypeMapper = createEnumJdbcType("subtract_inventory_type", SubtractInventoryType)
    implicit val SubtractInventoryTypeListTypeMapper = createEnumListJdbcType("subtract_inventory_type", SubtractInventoryType)
    implicit val SubtractInventoryTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(SubtractInventoryType)
    implicit val SubtractInventoryTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(SubtractInventoryType)


implicit val MeasureSystemsTypeTypeMapper = createEnumJdbcType("measure_systems_type", MeasureSystemsType)
    implicit val MeasureSystemsTypeListTypeMapper = createEnumListJdbcType("measure_systems_type", MeasureSystemsType)
    implicit val MeasureSystemsTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(MeasureSystemsType)
    implicit val MeasureSystemsTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(MeasureSystemsType)


implicit val PricePerTypeTypeMapper = createEnumJdbcType("price_per_type", PricePerType)
    implicit val PricePerTypeListTypeMapper = createEnumListJdbcType("price_per_type", PricePerType)
    implicit val PricePerTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(PricePerType)
    implicit val PricePerTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(PricePerType)


implicit val ChangeTypeTypeMapper = createEnumJdbcType("change_type", ChangeType)
    implicit val ChangeTypeListTypeMapper = createEnumListJdbcType("change_type", ChangeType)
    implicit val ChangeTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(ChangeType)
    implicit val ChangeTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(ChangeType)


implicit val OnboardingPlacementTypeMapper = createEnumJdbcType("onboarding_placement", OnboardingPlacement)
    implicit val OnboardingPlacementListTypeMapper = createEnumListJdbcType("onboarding_placement", OnboardingPlacement)
    implicit val OnboardingPlacementColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(OnboardingPlacement)
    implicit val OnboardingPlacementOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(OnboardingPlacement)


implicit val RecipePrintFormatTypeTypeMapper = createEnumJdbcType("recipe_print_format_type", RecipePrintFormatType)
    implicit val RecipePrintFormatTypeListTypeMapper = createEnumListJdbcType("recipe_print_format_type", RecipePrintFormatType)
    implicit val RecipePrintFormatTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(RecipePrintFormatType)
    implicit val RecipePrintFormatTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(RecipePrintFormatType)

  }
  override val api = new GeneratedPgDriverAPI {}
}

object GeneratedPgDriver extends GeneratedPgDriver
