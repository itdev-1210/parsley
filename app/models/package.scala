package object models {

  import Tables._

  import com.mohiva.play.silhouette.api.Identity
  import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile

  import scala.language.implicitConversions

  implicit def modelToRow[R](m: BaseModel[R]): R = m.dbRow

  class UserModel(r: UsersRow, val userAccounts: Seq[UserAccountsRow], val accounts: Seq[AccountsRow]) extends BaseModel[UsersRow](r) with Identity {

    def withSocialLoginInfo(commonSocialProfile: CommonSocialProfile): UsersRow = {
      dbRow.copy(email = commonSocialProfile.email)
    }
  }

  implicit class UserLogin(r: UserLogins) extends BaseModel[UserLogin](r)

  type DBDriver = Tables.profile.type
}
