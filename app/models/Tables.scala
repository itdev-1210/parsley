package models

import java.time.{ Instant, LocalDate }

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = GeneratedPgDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: GeneratedPgDriver
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(Accounts.schema, Categories.schema, EmailTokens.schema, Flags.schema, IngredientPars.schema, IngredientPriceHistory.schema, IngredientPriceHistoryMeasures.schema, Inventories.schema, InventoryItems.schema, LocationsSyncedRecipes.schema, LocationsSyncedSuppliers.schema, Measures.schema, MenuItems.schema, Menus.schema, MenuSections.schema, Nutrients.schema, OnboardingPages.schema, OnboardingSteps.schema, OnboardingWidgets.schema, Orders.schema, PosImportPreferences.schema, ProductAllergens.schema, ProductCategories.schema, ProductCharacteristics.schema, ProductInfos.schema, ProductMeasures.schema, ProductNutrients.schema, Products.schema, ProductSources.schema, PurchaseOrderCustomerOrders.schema, PurchaseOrders.schema, RecipeNutrients.schema, Recipes.schema, RecipesRevisionHistory.schema, RecipeStepIngredients.schema, RecipeSteps.schema, SimplePreparations.schema, Subscriptions.schema, SupplierContacts.schema, Suppliers.schema, TransactionDeltas.schema, TransactionMeasures.schema, Transactions.schema, Units.schema, UpstreamInvites.schema, UserAccounts.schema, UserLogins.schema, UserOnboardingBoxes.schema, UserOnboardingWidgets.schema, UserPrintPreferences.schema, Users.schema, UserSharedInvites.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema


  /** GetResult implicit for fetching AccountsRow objects using plain SQL queries */
  implicit def GetResultAccountsRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[Option[String]], e3: GR[Instant], e4: GR[Option[Instant]], e5: GR[Option[Int]], e6: GR[List[String]]): GR[AccountsRow] = GR{
    prs => import prs._
    AccountsRow.tupled((<<[Int], <<[Boolean], <<[Boolean], <<?[String], <<?[String], <<[Boolean], <<?[String], <<?[String], <<[Instant], <<?[Instant], <<?[Instant], <<?[Int], <<?[String], <<[List[String]], <<?[String], <<[List[String]], <<?[String], <<?[String], <<?[Int], <<[Boolean]))
  }
  /** Table description of table accounts. Objects of this class serve as prototypes for rows in queries. */
  class Accounts(_tableTag: Tag) extends profile.api.Table[AccountsRow](_tableTag, "accounts") {
    def * = (id, isAdmin, isStdlib, stripeEmail, stripeCustomerId, acceptsTerms, promoCode, subscriptionLevel, createdAt, lastLogin, lastUsage, multiLocationUpstream, upstreamRelativeName, addons, shippingAddress, ccEmails, businessName, companyLogo, accountOwner, fakeAccount) <> (AccountsRow.tupled, AccountsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(isAdmin), Rep.Some(isStdlib), stripeEmail, stripeCustomerId, Rep.Some(acceptsTerms), promoCode, subscriptionLevel, Rep.Some(createdAt), lastLogin, lastUsage, multiLocationUpstream, upstreamRelativeName, Rep.Some(addons), shippingAddress, Rep.Some(ccEmails), businessName, companyLogo, accountOwner, Rep.Some(fakeAccount)).shaped.<>({r=>import r._; _1.map(_=> AccountsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6.get, _7, _8, _9.get, _10, _11, _12, _13, _14.get, _15, _16.get, _17, _18, _19, _20.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column is_admin SqlType(bool), Default(false) */
    val isAdmin: Rep[Boolean] = column[Boolean]("is_admin", O.Default(false))
    /** Database column is_stdlib SqlType(bool), Default(false) */
    val isStdlib: Rep[Boolean] = column[Boolean]("is_stdlib", O.Default(false))
    /** Database column stripe_email SqlType(text), Default(None) */
    val stripeEmail: Rep[Option[String]] = column[Option[String]]("stripe_email", O.Default(None))
    /** Database column stripe_customer_id SqlType(text), Default(None) */
    val stripeCustomerId: Rep[Option[String]] = column[Option[String]]("stripe_customer_id", O.Default(None))
    /** Database column accepts_terms SqlType(bool), Default(false) */
    val acceptsTerms: Rep[Boolean] = column[Boolean]("accepts_terms", O.Default(false))
    /** Database column promo_code SqlType(text), Default(None) */
    val promoCode: Rep[Option[String]] = column[Option[String]]("promo_code", O.Default(None))
    /** Database column subscription_level SqlType(text), Default(None) */
    val subscriptionLevel: Rep[Option[String]] = column[Option[String]]("subscription_level", O.Default(None))
    /** Database column created_at SqlType(timestamp) */
    val createdAt: Rep[Instant] = column[Instant]("created_at")
    /** Database column last_login SqlType(timestamp), Default(None) */
    val lastLogin: Rep[Option[Instant]] = column[Option[Instant]]("last_login", O.Default(None))
    /** Database column last_usage SqlType(timestamp), Default(None) */
    val lastUsage: Rep[Option[Instant]] = column[Option[Instant]]("last_usage", O.Default(None))
    /** Database column multi_location_upstream SqlType(int4), Default(None) */
    val multiLocationUpstream: Rep[Option[Int]] = column[Option[Int]]("multi_location_upstream", O.Default(None))
    /** Database column upstream_relative_name SqlType(text), Default(None) */
    val upstreamRelativeName: Rep[Option[String]] = column[Option[String]]("upstream_relative_name", O.Default(None))
    /** Database column addons SqlType(_text), Length(2147483647,false) */
    val addons: Rep[List[String]] = column[List[String]]("addons", O.Length(2147483647,varying=false))
    /** Database column shipping_address SqlType(text), Default(None) */
    val shippingAddress: Rep[Option[String]] = column[Option[String]]("shipping_address", O.Default(None))
    /** Database column cc_emails SqlType(_text), Length(2147483647,false) */
    val ccEmails: Rep[List[String]] = column[List[String]]("cc_emails", O.Length(2147483647,varying=false))
    /** Database column business_name SqlType(text), Default(None) */
    val businessName: Rep[Option[String]] = column[Option[String]]("business_name", O.Default(None))
    /** Database column company_logo SqlType(text), Default(None) */
    val companyLogo: Rep[Option[String]] = column[Option[String]]("company_logo", O.Default(None))
    /** Database column account_owner SqlType(int4), Default(None) */
    val accountOwner: Rep[Option[Int]] = column[Option[Int]]("account_owner", O.Default(None))
    /** Database column fake_account SqlType(bool), Default(false) */
    val fakeAccount: Rep[Boolean] = column[Boolean]("fake_account", O.Default(false))

    /** Foreign key referencing Accounts (database name users_multi_location_upstream_fkey) */
    lazy val accountsFk = foreignKey("users_multi_location_upstream_fkey", multiLocationUpstream, Accounts)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name accounts_account_owner_fkey) */
    lazy val usersFk = foreignKey("accounts_account_owner_fkey", accountOwner, Users)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Accounts */
  lazy val Accounts = new TableQuery(tag => new Accounts(tag))


  /** GetResult implicit for fetching CategoriesRow objects using plain SQL queries */
  implicit def GetResultCategoriesRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[String], e3: GR[ProductCategoryType.Value]): GR[CategoriesRow] = GR{
    prs => import prs._
    CategoriesRow.tupled((<<[Int], <<?[Int], <<[String], <<?[Int], <<[ProductCategoryType.Value]))
  }
  /** Table description of table categories. Objects of this class serve as prototypes for rows in queries. */
  class Categories(_tableTag: Tag) extends profile.api.Table[CategoriesRow](_tableTag, "categories") {
    def * = (id, owner, name, syncUpstream, categoryType) <> (CategoriesRow.tupled, CategoriesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), owner, Rep.Some(name), syncUpstream, Rep.Some(categoryType)).shaped.<>({r=>import r._; _1.map(_=> CategoriesRow.tupled((_1.get, _2, _3.get, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4), Default(None) */
    val owner: Rep[Option[Int]] = column[Option[Int]]("owner", O.Default(None))
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column sync_upstream SqlType(int4), Default(None) */
    val syncUpstream: Rep[Option[Int]] = column[Option[Int]]("sync_upstream", O.Default(None))
    /** Database column category_type SqlType(product_category_type) */
    val categoryType: Rep[ProductCategoryType.Value] = column[ProductCategoryType.Value]("category_type")

    /** Foreign key referencing Accounts (database name categories_owner_fkey) */
    lazy val accountsFk = foreignKey("categories_owner_fkey", owner, Accounts)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Categories (database name categories_sync_upstream_fkey) */
    lazy val categoriesFk = foreignKey("categories_sync_upstream_fkey", syncUpstream, Categories)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (owner,name,categoryType) (database name categories_unique) */
    val index1 = index("categories_unique", (owner, name, categoryType), unique=true)
    /** Uniqueness Index over (name,categoryType) (database name stdlib_categories_uniqueness_idx) */
    val index2 = index("stdlib_categories_uniqueness_idx", (name, categoryType), unique=true)
  }
  /** Collection-like TableQuery object for table Categories */
  lazy val Categories = new TableQuery(tag => new Categories(tag))


  /** GetResult implicit for fetching EmailTokensRow objects using plain SQL queries */
  implicit def GetResultEmailTokensRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean], e3: GR[Instant], e4: GR[EmailTokenType.Value]): GR[EmailTokensRow] = GR{
    prs => import prs._
    EmailTokensRow.tupled((<<[Int], <<[String], <<[Boolean], <<[Instant], <<[Int], <<[EmailTokenType.Value]))
  }
  /** Table description of table email_tokens. Objects of this class serve as prototypes for rows in queries. */
  class EmailTokens(_tableTag: Tag) extends profile.api.Table[EmailTokensRow](_tableTag, "email_tokens") {
    def * = (id, secret, used, expiration, owner, tokenType) <> (EmailTokensRow.tupled, EmailTokensRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(secret), Rep.Some(used), Rep.Some(expiration), Rep.Some(owner), Rep.Some(tokenType)).shaped.<>({r=>import r._; _1.map(_=> EmailTokensRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column secret SqlType(text) */
    val secret: Rep[String] = column[String]("secret")
    /** Database column used SqlType(bool), Default(false) */
    val used: Rep[Boolean] = column[Boolean]("used", O.Default(false))
    /** Database column expiration SqlType(timestamp) */
    val expiration: Rep[Instant] = column[Instant]("expiration")
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column token_type SqlType(email_token_type) */
    val tokenType: Rep[EmailTokenType.Value] = column[EmailTokenType.Value]("token_type")

    /** Foreign key referencing Accounts (database name email_tokens_owner_fkey) */
    lazy val accountsFk = foreignKey("email_tokens_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (secret) (database name email_tokens_secret_key) */
    val index1 = index("email_tokens_secret_key", secret, unique=true)
  }
  /** Collection-like TableQuery object for table EmailTokens */
  lazy val EmailTokens = new TableQuery(tag => new EmailTokens(tag))


  /** GetResult implicit for fetching FlagsRow objects using plain SQL queries */
  implicit def GetResultFlagsRow(implicit e0: GR[Int], e1: GR[Boolean]): GR[FlagsRow] = GR{
    prs => import prs._
    FlagsRow.tupled((<<[Int], <<[Boolean]))
  }
  /** Table description of table flags. Objects of this class serve as prototypes for rows in queries. */
  class Flags(_tableTag: Tag) extends profile.api.Table[FlagsRow](_tableTag, "flags") {
    def * = (userId, subscriptionChosen) <> (FlagsRow.tupled, FlagsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(subscriptionChosen)).shaped.<>({r=>import r._; _1.map(_=> FlagsRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(int4), PrimaryKey */
    val userId: Rep[Int] = column[Int]("user_id", O.PrimaryKey)
    /** Database column subscription_chosen SqlType(bool), Default(false) */
    val subscriptionChosen: Rep[Boolean] = column[Boolean]("subscription_chosen", O.Default(false))

    /** Foreign key referencing Accounts (database name flags_user_id_fkey) */
    lazy val accountsFk = foreignKey("flags_user_id_fkey", userId, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Flags */
  lazy val Flags = new TableQuery(tag => new Flags(tag))


  /** GetResult implicit for fetching IngredientParsRow objects using plain SQL queries */
  implicit def GetResultIngredientParsRow(implicit e0: GR[Int], e1: GR[Option[Double]], e2: GR[Option[Int]], e3: GR[Boolean], e4: GR[Option[String]], e5: GR[Double]): GR[IngredientParsRow] = GR{
    prs => import prs._
    IngredientParsRow.tupled((<<[Int], <<[Int], <<[Int], <<?[Double], <<?[Int], <<?[Int], <<[Boolean], <<?[String], <<?[String], <<[Double], <<[Double], <<?[String], <<?[Int]))
  }
  /** Table description of table ingredient_pars. Objects of this class serve as prototypes for rows in queries. */
  class IngredientPars(_tableTag: Tag) extends profile.api.Table[IngredientParsRow](_tableTag, "ingredient_pars") {
    def * = (id, owner, product, amount, measure, unit, packaged, packageName, subPackageName, packageSize, superPackageSize, supplierName, supplier) <> (IngredientParsRow.tupled, IngredientParsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), Rep.Some(product), amount, measure, unit, Rep.Some(packaged), packageName, subPackageName, Rep.Some(packageSize), Rep.Some(superPackageSize), supplierName, supplier).shaped.<>({r=>import r._; _1.map(_=> IngredientParsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7.get, _8, _9, _10.get, _11.get, _12, _13)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column amount SqlType(float8), Default(None) */
    val amount: Rep[Option[Double]] = column[Option[Double]]("amount", O.Default(None))
    /** Database column measure SqlType(int4), Default(None) */
    val measure: Rep[Option[Int]] = column[Option[Int]]("measure", O.Default(None))
    /** Database column unit SqlType(int4), Default(None) */
    val unit: Rep[Option[Int]] = column[Option[Int]]("unit", O.Default(None))
    /** Database column packaged SqlType(bool), Default(false) */
    val packaged: Rep[Boolean] = column[Boolean]("packaged", O.Default(false))
    /** Database column package_name SqlType(text), Default(None) */
    val packageName: Rep[Option[String]] = column[Option[String]]("package_name", O.Default(None))
    /** Database column sub_package_name SqlType(text), Default(None) */
    val subPackageName: Rep[Option[String]] = column[Option[String]]("sub_package_name", O.Default(None))
    /** Database column package_size SqlType(float8) */
    val packageSize: Rep[Double] = column[Double]("package_size")
    /** Database column super_package_size SqlType(float8), Default(1.0) */
    val superPackageSize: Rep[Double] = column[Double]("super_package_size", O.Default(1.0))
    /** Database column supplier_name SqlType(text), Default(None) */
    val supplierName: Rep[Option[String]] = column[Option[String]]("supplier_name", O.Default(None))
    /** Database column supplier SqlType(int4), Default(None) */
    val supplier: Rep[Option[Int]] = column[Option[Int]]("supplier", O.Default(None))

    /** Foreign key referencing Accounts (database name ingredient_pars_owner_fkey) */
    lazy val accountsFk = foreignKey("ingredient_pars_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name ingredient_pars_measure_fkey) */
    lazy val measuresFk = foreignKey("ingredient_pars_measure_fkey", measure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name ingredient_pars_product_measure_valid) */
    lazy val productMeasuresFk = foreignKey("ingredient_pars_product_measure_valid", (product, measure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name ingredient_pars_product_fkey) */
    lazy val productsFk4 = foreignKey("ingredient_pars_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name ingredient_pars_product_valid) */
    lazy val productsFk5 = foreignKey("ingredient_pars_product_valid", (product, Rep.Some(owner)), Products)(r => (r.id, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name ingredient_pars_supplier_fkey) */
    lazy val suppliersFk = foreignKey("ingredient_pars_supplier_fkey", supplier, Suppliers)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing Units (database name ingredient_pars_unit_fkey) */
    lazy val unitsFk7 = foreignKey("ingredient_pars_unit_fkey", unit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name ingredient_pars_unit_valid) */
    lazy val unitsFk8 = foreignKey("ingredient_pars_unit_valid", (unit, measure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table IngredientPars */
  lazy val IngredientPars = new TableQuery(tag => new IngredientPars(tag))


  /** GetResult implicit for fetching IngredientPriceHistoryRow objects using plain SQL queries */
  implicit def GetResultIngredientPriceHistoryRow(implicit e0: GR[Int], e1: GR[Option[scala.math.BigDecimal]], e2: GR[Boolean], e3: GR[Float], e4: GR[PricePerType.Value], e5: GR[Option[Int]], e6: GR[ChangeType.Value], e7: GR[Instant]): GR[IngredientPriceHistoryRow] = GR{
    prs => import prs._
    IngredientPriceHistoryRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<?[scala.math.BigDecimal], <<[Boolean], <<[Float], <<[Float], <<[PricePerType.Value], <<?[Int], <<?[Int], <<[ChangeType.Value], <<[Instant]))
  }
  /** Table description of table ingredient_price_history. Objects of this class serve as prototypes for rows in queries. */
  class IngredientPriceHistory(_tableTag: Tag) extends profile.api.Table[IngredientPriceHistoryRow](_tableTag, "ingredient_price_history") {
    def * = (id, product, measure, unit, cost, packaged, packageSize, superPackageSize, pricePer, pricingMeasure, pricingUnit, changeType, createAt) <> (IngredientPriceHistoryRow.tupled, IngredientPriceHistoryRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(product), Rep.Some(measure), Rep.Some(unit), cost, Rep.Some(packaged), Rep.Some(packageSize), Rep.Some(superPackageSize), Rep.Some(pricePer), pricingMeasure, pricingUnit, Rep.Some(changeType), Rep.Some(createAt)).shaped.<>({r=>import r._; _1.map(_=> IngredientPriceHistoryRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7.get, _8.get, _9.get, _10, _11, _12.get, _13.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column unit SqlType(int4) */
    val unit: Rep[Int] = column[Int]("unit")
    /** Database column cost SqlType(numeric), Default(None) */
    val cost: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("cost", O.Default(None))
    /** Database column packaged SqlType(bool), Default(false) */
    val packaged: Rep[Boolean] = column[Boolean]("packaged", O.Default(false))
    /** Database column package_size SqlType(float4) */
    val packageSize: Rep[Float] = column[Float]("package_size")
    /** Database column super_package_size SqlType(float4), Default(1.0) */
    val superPackageSize: Rep[Float] = column[Float]("super_package_size", O.Default(1.0F))
    /** Database column price_per SqlType(price_per_type) */
    val pricePer: Rep[PricePerType.Value] = column[PricePerType.Value]("price_per")
    /** Database column pricing_measure SqlType(int4), Default(None) */
    val pricingMeasure: Rep[Option[Int]] = column[Option[Int]]("pricing_measure", O.Default(None))
    /** Database column pricing_unit SqlType(int4), Default(None) */
    val pricingUnit: Rep[Option[Int]] = column[Option[Int]]("pricing_unit", O.Default(None))
    /** Database column change_type SqlType(change_type) */
    val changeType: Rep[ChangeType.Value] = column[ChangeType.Value]("change_type")
    /** Database column create_at SqlType(timestamp) */
    val createAt: Rep[Instant] = column[Instant]("create_at")

    /** Foreign key referencing Measures (database name ingredient_price_history_measure_fkey) */
    lazy val measuresFk = foreignKey("ingredient_price_history_measure_fkey", measure, Measures)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name ingredient_price_history_product_fkey) */
    lazy val productsFk = foreignKey("ingredient_price_history_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name ingredient_price_history_unit_fkey) */
    lazy val unitsFk3 = foreignKey("ingredient_price_history_unit_fkey", unit, Units)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name ingredient_price_history_valid_units) */
    lazy val unitsFk4 = foreignKey("ingredient_price_history_valid_units", (unit, measure), Units)(r => (r.id, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table IngredientPriceHistory */
  lazy val IngredientPriceHistory = new TableQuery(tag => new IngredientPriceHistory(tag))


  /** GetResult implicit for fetching IngredientPriceHistoryMeasuresRow objects using plain SQL queries */
  implicit def GetResultIngredientPriceHistoryMeasuresRow(implicit e0: GR[Int], e1: GR[Double]): GR[IngredientPriceHistoryMeasuresRow] = GR{
    prs => import prs._
    IngredientPriceHistoryMeasuresRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Double], <<[Double]))
  }
  /** Table description of table ingredient_price_history_measures. Objects of this class serve as prototypes for rows in queries. */
  class IngredientPriceHistoryMeasures(_tableTag: Tag) extends profile.api.Table[IngredientPriceHistoryMeasuresRow](_tableTag, "ingredient_price_history_measures") {
    def * = (id, ingredientPrice, measure, preferredUnit, conversionMeasure, conversionUnit, conversion, amount) <> (IngredientPriceHistoryMeasuresRow.tupled, IngredientPriceHistoryMeasuresRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(ingredientPrice), Rep.Some(measure), Rep.Some(preferredUnit), Rep.Some(conversionMeasure), Rep.Some(conversionUnit), Rep.Some(conversion), Rep.Some(amount)).shaped.<>({r=>import r._; _1.map(_=> IngredientPriceHistoryMeasuresRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column ingredient_price SqlType(int4) */
    val ingredientPrice: Rep[Int] = column[Int]("ingredient_price")
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column preferred_unit SqlType(int4) */
    val preferredUnit: Rep[Int] = column[Int]("preferred_unit")
    /** Database column conversion_measure SqlType(int4) */
    val conversionMeasure: Rep[Int] = column[Int]("conversion_measure")
    /** Database column conversion_unit SqlType(int4) */
    val conversionUnit: Rep[Int] = column[Int]("conversion_unit")
    /** Database column conversion SqlType(float8) */
    val conversion: Rep[Double] = column[Double]("conversion")
    /** Database column amount SqlType(float8) */
    val amount: Rep[Double] = column[Double]("amount")

    /** Foreign key referencing IngredientPriceHistory (database name ingredient_price_history_measures_ingredient_price_fkey) */
    lazy val ingredientPriceHistoryFk = foreignKey("ingredient_price_history_measures_ingredient_price_fkey", ingredientPrice, IngredientPriceHistory)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (ingredientPrice,measure) (database name ingredient_price_history_measures_ingredient_price_measure_key) */
    val index1 = index("ingredient_price_history_measures_ingredient_price_measure_key", (ingredientPrice, measure), unique=true)
  }
  /** Collection-like TableQuery object for table IngredientPriceHistoryMeasures */
  lazy val IngredientPriceHistoryMeasures = new TableQuery(tag => new IngredientPriceHistoryMeasures(tag))


  /** GetResult implicit for fetching InventoriesRow objects using plain SQL queries */
  implicit def GetResultInventoriesRow(implicit e0: GR[Int], e1: GR[Instant], e2: GR[Option[String]], e3: GR[LocalDate], e4: GR[Boolean]): GR[InventoriesRow] = GR{
    prs => import prs._
    InventoriesRow.tupled((<<[Int], <<[Int], <<[Instant], <<?[String], <<[LocalDate], <<[Boolean]))
  }
  /** Table description of table inventories. Objects of this class serve as prototypes for rows in queries. */
  class Inventories(_tableTag: Tag) extends profile.api.Table[InventoriesRow](_tableTag, "inventories") {
    def * = (id, owner, time, name, date, mergedPreviousInventory) <> (InventoriesRow.tupled, InventoriesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), Rep.Some(time), name, Rep.Some(date), Rep.Some(mergedPreviousInventory)).shaped.<>({r=>import r._; _1.map(_=> InventoriesRow.tupled((_1.get, _2.get, _3.get, _4, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column time SqlType(timestamp) */
    val time: Rep[Instant] = column[Instant]("time")
    /** Database column name SqlType(text), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Default(None))
    /** Database column date SqlType(date) */
    val date: Rep[LocalDate] = column[LocalDate]("date")
    /** Database column merged_previous_inventory SqlType(bool), Default(false) */
    val mergedPreviousInventory: Rep[Boolean] = column[Boolean]("merged_previous_inventory", O.Default(false))

    /** Foreign key referencing Accounts (database name inventories_owner_fkey) */
    lazy val accountsFk = foreignKey("inventories_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (id,owner) (database name inventories_id_owner_key) */
    val index1 = index("inventories_id_owner_key", (id, owner), unique=true)
    /** Index over (owner,time) (database name inventory_timeseries_fetch_idx) */
    val index2 = index("inventory_timeseries_fetch_idx", (owner, time))
    /** Uniqueness Index over (owner,date) (database name one_inventory_per_day) */
    val index3 = index("one_inventory_per_day", (owner, date), unique=true)
  }
  /** Collection-like TableQuery object for table Inventories */
  lazy val Inventories = new TableQuery(tag => new Inventories(tag))


  /** GetResult implicit for fetching InventoryItemsRow objects using plain SQL queries */
  implicit def GetResultInventoryItemsRow(implicit e0: GR[Int], e1: GR[Option[Double]], e2: GR[Option[Int]], e3: GR[Boolean], e4: GR[Option[String]], e5: GR[Double]): GR[InventoryItemsRow] = GR{
    prs => import prs._
    InventoryItemsRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<?[Double], <<?[Int], <<?[Int], <<?[Double], <<[Boolean], <<?[String], <<?[String], <<[Double], <<[Double], <<?[String], <<?[Int]))
  }
  /** Table description of table inventory_items. Objects of this class serve as prototypes for rows in queries. */
  class InventoryItems(_tableTag: Tag) extends profile.api.Table[InventoryItemsRow](_tableTag, "inventory_items") {
    def * = (id, inventory, owner, product, amount, measure, unit, cost, packaged, packageName, subPackageName, packageSize, superPackageSize, supplierName, supplier) <> (InventoryItemsRow.tupled, InventoryItemsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(inventory), Rep.Some(owner), Rep.Some(product), amount, measure, unit, cost, Rep.Some(packaged), packageName, subPackageName, Rep.Some(packageSize), Rep.Some(superPackageSize), supplierName, supplier).shaped.<>({r=>import r._; _1.map(_=> InventoryItemsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6, _7, _8, _9.get, _10, _11, _12.get, _13.get, _14, _15)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column inventory SqlType(int4) */
    val inventory: Rep[Int] = column[Int]("inventory")
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column amount SqlType(float8), Default(None) */
    val amount: Rep[Option[Double]] = column[Option[Double]]("amount", O.Default(None))
    /** Database column measure SqlType(int4), Default(None) */
    val measure: Rep[Option[Int]] = column[Option[Int]]("measure", O.Default(None))
    /** Database column unit SqlType(int4), Default(None) */
    val unit: Rep[Option[Int]] = column[Option[Int]]("unit", O.Default(None))
    /** Database column cost SqlType(float8), Default(None) */
    val cost: Rep[Option[Double]] = column[Option[Double]]("cost", O.Default(None))
    /** Database column packaged SqlType(bool), Default(false) */
    val packaged: Rep[Boolean] = column[Boolean]("packaged", O.Default(false))
    /** Database column package_name SqlType(text), Default(None) */
    val packageName: Rep[Option[String]] = column[Option[String]]("package_name", O.Default(None))
    /** Database column sub_package_name SqlType(text), Default(None) */
    val subPackageName: Rep[Option[String]] = column[Option[String]]("sub_package_name", O.Default(None))
    /** Database column package_size SqlType(float8) */
    val packageSize: Rep[Double] = column[Double]("package_size")
    /** Database column super_package_size SqlType(float8), Default(1.0) */
    val superPackageSize: Rep[Double] = column[Double]("super_package_size", O.Default(1.0))
    /** Database column supplier_name SqlType(text), Default(None) */
    val supplierName: Rep[Option[String]] = column[Option[String]]("supplier_name", O.Default(None))
    /** Database column supplier SqlType(int4), Default(None) */
    val supplier: Rep[Option[Int]] = column[Option[Int]]("supplier", O.Default(None))

    /** Foreign key referencing Accounts (database name inventory_items_owner_fkey) */
    lazy val accountsFk = foreignKey("inventory_items_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Inventories (database name inventory_items_inventory_fkey) */
    lazy val inventoriesFk = foreignKey("inventory_items_inventory_fkey", inventory, Inventories)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name inventory_items_measure_fkey) */
    lazy val measuresFk = foreignKey("inventory_items_measure_fkey", measure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name inventory_items_product_fkey) */
    lazy val productsFk = foreignKey("inventory_items_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name inventory_items_supplier_fkey) */
    lazy val suppliersFk = foreignKey("inventory_items_supplier_fkey", supplier, Suppliers)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing Units (database name inventory_items_unit_fkey) */
    lazy val unitsFk = foreignKey("inventory_items_unit_fkey", unit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table InventoryItems */
  lazy val InventoryItems = new TableQuery(tag => new InventoryItems(tag))


  /** GetResult implicit for fetching LocationsSyncedRecipesRow objects using plain SQL queries */
  implicit def GetResultLocationsSyncedRecipesRow(implicit e0: GR[Int], e1: GR[String]): GR[LocationsSyncedRecipesRow] = GR{
    prs => import prs._
    LocationsSyncedRecipesRow.tupled((<<[Int], <<[Int], <<[String], <<[Int]))
  }
  /** Table description of table locations_synced_recipes. Objects of this class serve as prototypes for rows in queries. */
  class LocationsSyncedRecipes(_tableTag: Tag) extends profile.api.Table[LocationsSyncedRecipesRow](_tableTag, "locations_synced_recipes") {
    def * = (owner, locationId, deprecatedCategoryColumn, recipeCategory) <> (LocationsSyncedRecipesRow.tupled, LocationsSyncedRecipesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(owner), Rep.Some(locationId), Rep.Some(deprecatedCategoryColumn), Rep.Some(recipeCategory)).shaped.<>({r=>import r._; _1.map(_=> LocationsSyncedRecipesRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column location_id SqlType(int4) */
    val locationId: Rep[Int] = column[Int]("location_id")
    /** Database column deprecated_category_column SqlType(text) */
    val deprecatedCategoryColumn: Rep[String] = column[String]("deprecated_category_column")
    /** Database column recipe_category SqlType(int4) */
    val recipeCategory: Rep[Int] = column[Int]("recipe_category")

    /** Foreign key referencing Accounts (database name locations_synced_recipes_location_id_fkey) */
    lazy val accountsFk1 = foreignKey("locations_synced_recipes_location_id_fkey", locationId, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Accounts (database name locations_synced_recipes_owner_fkey) */
    lazy val accountsFk2 = foreignKey("locations_synced_recipes_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Categories (database name locations_synced_recipes_recipe_category_fkey) */
    lazy val categoriesFk = foreignKey("locations_synced_recipes_recipe_category_fkey", recipeCategory, Categories)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (locationId,recipeCategory) (database name locations_synced_recipes_unique_category) */
    val index1 = index("locations_synced_recipes_unique_category", (locationId, recipeCategory), unique=true)
  }
  /** Collection-like TableQuery object for table LocationsSyncedRecipes */
  lazy val LocationsSyncedRecipes = new TableQuery(tag => new LocationsSyncedRecipes(tag))


  /** GetResult implicit for fetching LocationsSyncedSuppliersRow objects using plain SQL queries */
  implicit def GetResultLocationsSyncedSuppliersRow(implicit e0: GR[Int]): GR[LocationsSyncedSuppliersRow] = GR{
    prs => import prs._
    LocationsSyncedSuppliersRow.tupled((<<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table locations_synced_suppliers. Objects of this class serve as prototypes for rows in queries. */
  class LocationsSyncedSuppliers(_tableTag: Tag) extends profile.api.Table[LocationsSyncedSuppliersRow](_tableTag, "locations_synced_suppliers") {
    def * = (owner, locationId, supplierId) <> (LocationsSyncedSuppliersRow.tupled, LocationsSyncedSuppliersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(owner), Rep.Some(locationId), Rep.Some(supplierId)).shaped.<>({r=>import r._; _1.map(_=> LocationsSyncedSuppliersRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column location_id SqlType(int4) */
    val locationId: Rep[Int] = column[Int]("location_id")
    /** Database column supplier_id SqlType(int4) */
    val supplierId: Rep[Int] = column[Int]("supplier_id")

    /** Foreign key referencing Accounts (database name locations_synced_suppliers_location_id_fkey) */
    lazy val accountsFk1 = foreignKey("locations_synced_suppliers_location_id_fkey", locationId, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Accounts (database name locations_synced_suppliers_owner_fkey) */
    lazy val accountsFk2 = foreignKey("locations_synced_suppliers_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name locations_synced_suppliers_supplier_id_fkey) */
    lazy val suppliersFk = foreignKey("locations_synced_suppliers_supplier_id_fkey", supplierId, Suppliers)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)

    /** Uniqueness Index over (owner,locationId,supplierId) (database name locations_synced_suppliers_owner_location_id_supplier_id_key) */
    val index1 = index("locations_synced_suppliers_owner_location_id_supplier_id_key", (owner, locationId, supplierId), unique=true)
  }
  /** Collection-like TableQuery object for table LocationsSyncedSuppliers */
  lazy val LocationsSyncedSuppliers = new TableQuery(tag => new LocationsSyncedSuppliers(tag))


  /** GetResult implicit for fetching MeasuresRow objects using plain SQL queries */
  implicit def GetResultMeasuresRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean]): GR[MeasuresRow] = GR{
    prs => import prs._
    MeasuresRow.tupled((<<[Int], <<[String], <<[Boolean]))
  }
  /** Table description of table measures. Objects of this class serve as prototypes for rows in queries. */
  class Measures(_tableTag: Tag) extends profile.api.Table[MeasuresRow](_tableTag, "measures") {
    def * = (id, name, allowsCustomNames) <> (MeasuresRow.tupled, MeasuresRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), Rep.Some(allowsCustomNames)).shaped.<>({r=>import r._; _1.map(_=> MeasuresRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column allows_custom_names SqlType(bool), Default(false) */
    val allowsCustomNames: Rep[Boolean] = column[Boolean]("allows_custom_names", O.Default(false))
  }
  /** Collection-like TableQuery object for table Measures */
  lazy val Measures = new TableQuery(tag => new Measures(tag))


  /** GetResult implicit for fetching MenuItemsRow objects using plain SQL queries */
  implicit def GetResultMenuItemsRow(implicit e0: GR[Int]): GR[MenuItemsRow] = GR{
    prs => import prs._
    MenuItemsRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table menu_items. Objects of this class serve as prototypes for rows in queries. */
  class MenuItems(_tableTag: Tag) extends profile.api.Table[MenuItemsRow](_tableTag, "menu_items") {
    def * = (id, section, displayOrder, product, owner, menu) <> (MenuItemsRow.tupled, MenuItemsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(section), Rep.Some(displayOrder), Rep.Some(product), Rep.Some(owner), Rep.Some(menu)).shaped.<>({r=>import r._; _1.map(_=> MenuItemsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column section SqlType(int4) */
    val section: Rep[Int] = column[Int]("section")
    /** Database column display_order SqlType(int4) */
    val displayOrder: Rep[Int] = column[Int]("display_order")
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column menu SqlType(int4), Default(0) */
    val menu: Rep[Int] = column[Int]("menu", O.Default(0))

    /** Foreign key referencing MenuSections (database name menu_items_section_fkey) */
    lazy val menuSectionsFk = foreignKey("menu_items_section_fkey", (section, menu, owner), MenuSections)(r => (r.id, r.menu, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name menu_items_product_fkey) */
    lazy val productsFk2 = foreignKey("menu_items_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name menu_items_product_fkey1) */
    lazy val productsFk3 = foreignKey("menu_items_product_fkey1", (product, Rep.Some(owner)), Products)(r => (r.id, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (menu,product) (database name menu_items_menu_product_key) */
    val index1 = index("menu_items_menu_product_key", (menu, product), unique=true)
    /** Uniqueness Index over (section,displayOrder) (database name menu_items_section_display_order_key) */
    val index2 = index("menu_items_section_display_order_key", (section, displayOrder), unique=true)
  }
  /** Collection-like TableQuery object for table MenuItems */
  lazy val MenuItems = new TableQuery(tag => new MenuItems(tag))


  /** GetResult implicit for fetching MenusRow objects using plain SQL queries */
  implicit def GetResultMenusRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean]): GR[MenusRow] = GR{
    prs => import prs._
    MenusRow.tupled((<<[Int], <<[Int], <<[String], <<[Boolean]))
  }
  /** Table description of table menus. Objects of this class serve as prototypes for rows in queries. */
  class Menus(_tableTag: Tag) extends profile.api.Table[MenusRow](_tableTag, "menus") {
    def * = (id, owner, name, userVisible) <> (MenusRow.tupled, MenusRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), Rep.Some(name), Rep.Some(userVisible)).shaped.<>({r=>import r._; _1.map(_=> MenusRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column user_visible SqlType(bool), Default(true) */
    val userVisible: Rep[Boolean] = column[Boolean]("user_visible", O.Default(true))

    /** Foreign key referencing Accounts (database name menus_owner_fkey) */
    lazy val accountsFk = foreignKey("menus_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (id,owner) (database name menus_id_owner_key) */
    val index1 = index("menus_id_owner_key", (id, owner), unique=true)
  }
  /** Collection-like TableQuery object for table Menus */
  lazy val Menus = new TableQuery(tag => new Menus(tag))


  /** GetResult implicit for fetching MenuSectionsRow objects using plain SQL queries */
  implicit def GetResultMenuSectionsRow(implicit e0: GR[Int], e1: GR[Option[String]]): GR[MenuSectionsRow] = GR{
    prs => import prs._
    MenuSectionsRow.tupled((<<[Int], <<[Int], <<[Int], <<?[String], <<[Int]))
  }
  /** Table description of table menu_sections. Objects of this class serve as prototypes for rows in queries. */
  class MenuSections(_tableTag: Tag) extends profile.api.Table[MenuSectionsRow](_tableTag, "menu_sections") {
    def * = (id, menu, displayOrder, name, owner) <> (MenuSectionsRow.tupled, MenuSectionsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(menu), Rep.Some(displayOrder), name, Rep.Some(owner)).shaped.<>({r=>import r._; _1.map(_=> MenuSectionsRow.tupled((_1.get, _2.get, _3.get, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column menu SqlType(int4) */
    val menu: Rep[Int] = column[Int]("menu")
    /** Database column display_order SqlType(int4) */
    val displayOrder: Rep[Int] = column[Int]("display_order")
    /** Database column name SqlType(text), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Default(None))
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")

    /** Foreign key referencing Menus (database name menu_sections_menu_fkey) */
    lazy val menusFk = foreignKey("menu_sections_menu_fkey", (menu, owner), Menus)(r => (r.id, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (id,menu,owner) (database name menu_sections_id_menu_owner_key) */
    val index1 = index("menu_sections_id_menu_owner_key", (id, menu, owner), unique=true)
    /** Uniqueness Index over (menu,displayOrder) (database name menu_sections_menu_display_order_key) */
    val index2 = index("menu_sections_menu_display_order_key", (menu, displayOrder), unique=true)
  }
  /** Collection-like TableQuery object for table MenuSections */
  lazy val MenuSections = new TableQuery(tag => new MenuSections(tag))


  /** GetResult implicit for fetching NutrientsRow objects using plain SQL queries */
  implicit def GetResultNutrientsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]], e3: GR[Boolean], e4: GR[Option[scala.math.BigDecimal]]): GR[NutrientsRow] = GR{
    prs => import prs._
    NutrientsRow.tupled((<<[Int], <<[String], <<[String], <<?[String], <<[String], <<[Boolean], <<?[scala.math.BigDecimal]))
  }
  /** Table description of table nutrients. Objects of this class serve as prototypes for rows in queries. */
  class Nutrients(_tableTag: Tag) extends profile.api.Table[NutrientsRow](_tableTag, "nutrients") {
    def * = (id, nndbsrId, nndbsrName, parsleyName, unit, active, dailyValue) <> (NutrientsRow.tupled, NutrientsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(nndbsrId), Rep.Some(nndbsrName), parsleyName, Rep.Some(unit), Rep.Some(active), dailyValue).shaped.<>({r=>import r._; _1.map(_=> NutrientsRow.tupled((_1.get, _2.get, _3.get, _4, _5.get, _6.get, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column nndbsr_id SqlType(text) */
    val nndbsrId: Rep[String] = column[String]("nndbsr_id")
    /** Database column nndbsr_name SqlType(text) */
    val nndbsrName: Rep[String] = column[String]("nndbsr_name")
    /** Database column parsley_name SqlType(text), Default(None) */
    val parsleyName: Rep[Option[String]] = column[Option[String]]("parsley_name", O.Default(None))
    /** Database column unit SqlType(text) */
    val unit: Rep[String] = column[String]("unit")
    /** Database column active SqlType(bool), Default(false) */
    val active: Rep[Boolean] = column[Boolean]("active", O.Default(false))
    /** Database column daily_value SqlType(numeric), Default(None) */
    val dailyValue: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("daily_value", O.Default(None))

    /** Uniqueness Index over (nndbsrId) (database name nutrients_nndbsr_id_key) */
    val index1 = index("nutrients_nndbsr_id_key", nndbsrId, unique=true)
  }
  /** Collection-like TableQuery object for table Nutrients */
  lazy val Nutrients = new TableQuery(tag => new Nutrients(tag))


  /** GetResult implicit for fetching OnboardingPagesRow objects using plain SQL queries */
  implicit def GetResultOnboardingPagesRow(implicit e0: GR[String]): GR[OnboardingPagesRow] = GR{
    prs => import prs._
    OnboardingPagesRow.tupled((<<[String], <<[String]))
  }
  /** Table description of table onboarding_pages. Objects of this class serve as prototypes for rows in queries. */
  class OnboardingPages(_tableTag: Tag) extends profile.api.Table[OnboardingPagesRow](_tableTag, "onboarding_pages") {
    def * = (key, name) <> (OnboardingPagesRow.tupled, OnboardingPagesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(key), Rep.Some(name)).shaped.<>({r=>import r._; _1.map(_=> OnboardingPagesRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column key SqlType(bpchar), PrimaryKey, Length(120,false) */
    val key: Rep[String] = column[String]("key", O.PrimaryKey, O.Length(120,varying=false))
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
  }
  /** Collection-like TableQuery object for table OnboardingPages */
  lazy val OnboardingPages = new TableQuery(tag => new OnboardingPages(tag))


  /** GetResult implicit for fetching OnboardingStepsRow objects using plain SQL queries */
  implicit def GetResultOnboardingStepsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Short], e3: GR[OnboardingPlacement.Value], e4: GR[Option[String]]): GR[OnboardingStepsRow] = GR{
    prs => import prs._
    OnboardingStepsRow.tupled((<<[Int], <<[String], <<[Short], <<[OnboardingPlacement.Value], <<?[String], <<?[String], <<[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[Int], <<[String], <<[String], <<[String], <<?[String], <<[String], <<[String]))
  }
  /** Table description of table onboarding_steps. Objects of this class serve as prototypes for rows in queries. */
  class OnboardingSteps(_tableTag: Tag) extends profile.api.Table[OnboardingStepsRow](_tableTag, "onboarding_steps") {
    def * = (id, page, index, placement, targetSelector, title, description, leftImage, rightImage, nextButtonText, backButtonText, bodyWidth, titleBgColor, descBgColor, borderColor, nextBgColor, closeBtnColor, name) <> (OnboardingStepsRow.tupled, OnboardingStepsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(page), Rep.Some(index), Rep.Some(placement), targetSelector, title, Rep.Some(description), leftImage, rightImage, nextButtonText, backButtonText, Rep.Some(bodyWidth), Rep.Some(titleBgColor), Rep.Some(descBgColor), Rep.Some(borderColor), nextBgColor, Rep.Some(closeBtnColor), Rep.Some(name)).shaped.<>({r=>import r._; _1.map(_=> OnboardingStepsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6, _7.get, _8, _9, _10, _11, _12.get, _13.get, _14.get, _15.get, _16, _17.get, _18.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column page SqlType(bpchar), Length(120,false) */
    val page: Rep[String] = column[String]("page", O.Length(120,varying=false))
    /** Database column index SqlType(int2) */
    val index: Rep[Short] = column[Short]("index")
    /** Database column placement SqlType(onboarding_placement) */
    val placement: Rep[OnboardingPlacement.Value] = column[OnboardingPlacement.Value]("placement")
    /** Database column target_selector SqlType(text), Default(None) */
    val targetSelector: Rep[Option[String]] = column[Option[String]]("target_selector", O.Default(None))
    /** Database column title SqlType(text), Default(None) */
    val title: Rep[Option[String]] = column[Option[String]]("title", O.Default(None))
    /** Database column description SqlType(text) */
    val description: Rep[String] = column[String]("description")
    /** Database column left_image SqlType(text), Default(None) */
    val leftImage: Rep[Option[String]] = column[Option[String]]("left_image", O.Default(None))
    /** Database column right_image SqlType(text), Default(None) */
    val rightImage: Rep[Option[String]] = column[Option[String]]("right_image", O.Default(None))
    /** Database column next_button_text SqlType(text), Default(None) */
    val nextButtonText: Rep[Option[String]] = column[Option[String]]("next_button_text", O.Default(None))
    /** Database column back_button_text SqlType(text), Default(None) */
    val backButtonText: Rep[Option[String]] = column[Option[String]]("back_button_text", O.Default(None))
    /** Database column body_width SqlType(int4) */
    val bodyWidth: Rep[Int] = column[Int]("body_width")
    /** Database column title_bg_color SqlType(text), Default(80cc28) */
    val titleBgColor: Rep[String] = column[String]("title_bg_color", O.Default("80cc28"))
    /** Database column desc_bg_color SqlType(text), Default(ffffff) */
    val descBgColor: Rep[String] = column[String]("desc_bg_color", O.Default("ffffff"))
    /** Database column border_color SqlType(text), Default(e7e7e7) */
    val borderColor: Rep[String] = column[String]("border_color", O.Default("e7e7e7"))
    /** Database column next_bg_color SqlType(text), Default(Some(80cc28)) */
    val nextBgColor: Rep[Option[String]] = column[Option[String]]("next_bg_color", O.Default(Some("80cc28")))
    /** Database column close_btn_color SqlType(text), Default(ffffff) */
    val closeBtnColor: Rep[String] = column[String]("close_btn_color", O.Default("ffffff"))
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")

    /** Foreign key referencing OnboardingPages (database name onboarding_steps_page_fkey) */
    lazy val onboardingPagesFk = foreignKey("onboarding_steps_page_fkey", page, OnboardingPages)(r => r.key, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table OnboardingSteps */
  lazy val OnboardingSteps = new TableQuery(tag => new OnboardingSteps(tag))


  /** GetResult implicit for fetching OnboardingWidgetsRow objects using plain SQL queries */
  implicit def GetResultOnboardingWidgetsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[OnboardingWidgetsRow] = GR{
    prs => import prs._
    OnboardingWidgetsRow.tupled((<<[Int], <<[String], <<[String], <<?[String]))
  }
  /** Table description of table onboarding_widgets. Objects of this class serve as prototypes for rows in queries. */
  class OnboardingWidgets(_tableTag: Tag) extends profile.api.Table[OnboardingWidgetsRow](_tableTag, "onboarding_widgets") {
    def * = (id, name, defaultMessage, customMessage) <> (OnboardingWidgetsRow.tupled, OnboardingWidgetsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), Rep.Some(defaultMessage), customMessage).shaped.<>({r=>import r._; _1.map(_=> OnboardingWidgetsRow.tupled((_1.get, _2.get, _3.get, _4)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column default_message SqlType(text) */
    val defaultMessage: Rep[String] = column[String]("default_message")
    /** Database column custom_message SqlType(text), Default(None) */
    val customMessage: Rep[Option[String]] = column[Option[String]]("custom_message", O.Default(None))
  }
  /** Collection-like TableQuery object for table OnboardingWidgets */
  lazy val OnboardingWidgets = new TableQuery(tag => new OnboardingWidgets(tag))


  /** GetResult implicit for fetching OrdersRow objects using plain SQL queries */
  implicit def GetResultOrdersRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]], e3: GR[Option[Int]]): GR[OrdersRow] = GR{
    prs => import prs._
    OrdersRow.tupled((<<[Int], <<[String], <<?[String], <<[Int], <<[Int], <<[Int], <<?[Int]))
  }
  /** Table description of table orders. Objects of this class serve as prototypes for rows in queries. */
  class Orders(_tableTag: Tag) extends profile.api.Table[OrdersRow](_tableTag, "orders") {
    def * = (id, name, description, menu, owner, xact, coversCount) <> (OrdersRow.tupled, OrdersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), description, Rep.Some(menu), Rep.Some(owner), Rep.Some(xact), coversCount).shaped.<>({r=>import r._; _1.map(_=> OrdersRow.tupled((_1.get, _2.get, _3, _4.get, _5.get, _6.get, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column description SqlType(text), Default(None) */
    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
    /** Database column menu SqlType(int4) */
    val menu: Rep[Int] = column[Int]("menu")
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column xact SqlType(int4) */
    val xact: Rep[Int] = column[Int]("xact")
    /** Database column covers_count SqlType(int4), Default(None) */
    val coversCount: Rep[Option[Int]] = column[Option[Int]]("covers_count", O.Default(None))

    /** Foreign key referencing Accounts (database name orders_owner_fkey) */
    lazy val accountsFk = foreignKey("orders_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Menus (database name orders_menu_fkey) */
    lazy val menusFk = foreignKey("orders_menu_fkey", (menu, owner), Menus)(r => (r.id, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Transactions (database name orders_xact_fkey) */
    lazy val transactionsFk = foreignKey("orders_xact_fkey", xact, Transactions)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (id,menu,owner) (database name orders_id_menu_owner_key) */
    val index1 = index("orders_id_menu_owner_key", (id, menu, owner), unique=true)
  }
  /** Collection-like TableQuery object for table Orders */
  lazy val Orders = new TableQuery(tag => new Orders(tag))


  /** GetResult implicit for fetching PosImportPreferencesRow objects using plain SQL queries */
  implicit def GetResultPosImportPreferencesRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[PosImportPreferencesRow] = GR{
    prs => import prs._
    PosImportPreferencesRow.tupled((<<[Int], <<[String], <<[String], <<[String], <<?[String], <<?[String]))
  }
  /** Table description of table pos_import_preferences. Objects of this class serve as prototypes for rows in queries. */
  class PosImportPreferences(_tableTag: Tag) extends profile.api.Table[PosImportPreferencesRow](_tableTag, "pos_import_preferences") {
    def * = (owner, itemCodeHeader, dateSoldHeader, quantitySoldHeader, averagePriceHeader, nameHeader) <> (PosImportPreferencesRow.tupled, PosImportPreferencesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(owner), Rep.Some(itemCodeHeader), Rep.Some(dateSoldHeader), Rep.Some(quantitySoldHeader), averagePriceHeader, nameHeader).shaped.<>({r=>import r._; _1.map(_=> PosImportPreferencesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column owner SqlType(int4), PrimaryKey */
    val owner: Rep[Int] = column[Int]("owner", O.PrimaryKey)
    /** Database column item_code_header SqlType(text) */
    val itemCodeHeader: Rep[String] = column[String]("item_code_header")
    /** Database column date_sold_header SqlType(text) */
    val dateSoldHeader: Rep[String] = column[String]("date_sold_header")
    /** Database column quantity_sold_header SqlType(text) */
    val quantitySoldHeader: Rep[String] = column[String]("quantity_sold_header")
    /** Database column average_price_header SqlType(text), Default(None) */
    val averagePriceHeader: Rep[Option[String]] = column[Option[String]]("average_price_header", O.Default(None))
    /** Database column name_header SqlType(text), Default(None) */
    val nameHeader: Rep[Option[String]] = column[Option[String]]("name_header", O.Default(None))

    /** Foreign key referencing Accounts (database name pos_import_preferences_owner_fkey) */
    lazy val accountsFk = foreignKey("pos_import_preferences_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table PosImportPreferences */
  lazy val PosImportPreferences = new TableQuery(tag => new PosImportPreferences(tag))


  /** GetResult implicit for fetching ProductAllergensRow objects using plain SQL queries */
  implicit def GetResultProductAllergensRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[Option[String]]): GR[ProductAllergensRow] = GR{
    prs => import prs._
    ProductAllergensRow.tupled((<<[Int], <<[Boolean], <<[Boolean], <<?[String], <<?[String], <<?[String], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean]))
  }
  /** Table description of table product_allergens. Objects of this class serve as prototypes for rows in queries. */
  class ProductAllergens(_tableTag: Tag) extends profile.api.Table[ProductAllergensRow](_tableTag, "product_allergens") {
    def * = (product, milk, eggs, fish, crustaceanShellfish, treeNuts, wheat, peanuts, soybeans, molluscs, cerealsGluten, celery, mustard, sesameSeeds, sulphurDioxideSulphites, lupin) <> (ProductAllergensRow.tupled, ProductAllergensRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(milk), Rep.Some(eggs), fish, crustaceanShellfish, treeNuts, Rep.Some(wheat), Rep.Some(peanuts), Rep.Some(soybeans), Rep.Some(molluscs), Rep.Some(cerealsGluten), Rep.Some(celery), Rep.Some(mustard), Rep.Some(sesameSeeds), Rep.Some(sulphurDioxideSulphites), Rep.Some(lupin)).shaped.<>({r=>import r._; _1.map(_=> ProductAllergensRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7.get, _8.get, _9.get, _10.get, _11.get, _12.get, _13.get, _14.get, _15.get, _16.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4), PrimaryKey */
    val product: Rep[Int] = column[Int]("product", O.PrimaryKey)
    /** Database column milk SqlType(bool), Default(false) */
    val milk: Rep[Boolean] = column[Boolean]("milk", O.Default(false))
    /** Database column eggs SqlType(bool), Default(false) */
    val eggs: Rep[Boolean] = column[Boolean]("eggs", O.Default(false))
    /** Database column fish SqlType(text), Default(None) */
    val fish: Rep[Option[String]] = column[Option[String]]("fish", O.Default(None))
    /** Database column crustacean_shellfish SqlType(text), Default(None) */
    val crustaceanShellfish: Rep[Option[String]] = column[Option[String]]("crustacean_shellfish", O.Default(None))
    /** Database column tree_nuts SqlType(text), Default(None) */
    val treeNuts: Rep[Option[String]] = column[Option[String]]("tree_nuts", O.Default(None))
    /** Database column wheat SqlType(bool), Default(false) */
    val wheat: Rep[Boolean] = column[Boolean]("wheat", O.Default(false))
    /** Database column peanuts SqlType(bool), Default(false) */
    val peanuts: Rep[Boolean] = column[Boolean]("peanuts", O.Default(false))
    /** Database column soybeans SqlType(bool), Default(false) */
    val soybeans: Rep[Boolean] = column[Boolean]("soybeans", O.Default(false))
    /** Database column molluscs SqlType(bool), Default(false) */
    val molluscs: Rep[Boolean] = column[Boolean]("molluscs", O.Default(false))
    /** Database column cereals_gluten SqlType(bool), Default(false) */
    val cerealsGluten: Rep[Boolean] = column[Boolean]("cereals_gluten", O.Default(false))
    /** Database column celery SqlType(bool), Default(false) */
    val celery: Rep[Boolean] = column[Boolean]("celery", O.Default(false))
    /** Database column mustard SqlType(bool), Default(false) */
    val mustard: Rep[Boolean] = column[Boolean]("mustard", O.Default(false))
    /** Database column sesame_seeds SqlType(bool), Default(false) */
    val sesameSeeds: Rep[Boolean] = column[Boolean]("sesame_seeds", O.Default(false))
    /** Database column sulphur_dioxide_sulphites SqlType(bool), Default(false) */
    val sulphurDioxideSulphites: Rep[Boolean] = column[Boolean]("sulphur_dioxide_sulphites", O.Default(false))
    /** Database column lupin SqlType(bool), Default(false) */
    val lupin: Rep[Boolean] = column[Boolean]("lupin", O.Default(false))

    /** Foreign key referencing Products (database name product_allergens_product_fkey) */
    lazy val productsFk = foreignKey("product_allergens_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table ProductAllergens */
  lazy val ProductAllergens = new TableQuery(tag => new ProductAllergens(tag))


  /** GetResult implicit for fetching ProductCategoriesRow objects using plain SQL queries */
  implicit def GetResultProductCategoriesRow(implicit e0: GR[Int]): GR[ProductCategoriesRow] = GR{
    prs => import prs._
    ProductCategoriesRow.tupled((<<[Int], <<[Int]))
  }
  /** Table description of table product_categories. Objects of this class serve as prototypes for rows in queries. */
  class ProductCategories(_tableTag: Tag) extends profile.api.Table[ProductCategoriesRow](_tableTag, "product_categories") {
    def * = (product, category) <> (ProductCategoriesRow.tupled, ProductCategoriesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(category)).shaped.<>({r=>import r._; _1.map(_=> ProductCategoriesRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column category SqlType(int4) */
    val category: Rep[Int] = column[Int]("category")

    /** Foreign key referencing Categories (database name product_categories_category_fkey) */
    lazy val categoriesFk = foreignKey("product_categories_category_fkey", category, Categories)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name product_categories_product_fkey) */
    lazy val productsFk = foreignKey("product_categories_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (product,category) (database name product_categories_unique) */
    val index1 = index("product_categories_unique", (product, category), unique=true)
  }
  /** Collection-like TableQuery object for table ProductCategories */
  lazy val ProductCategories = new TableQuery(tag => new ProductCategories(tag))


  /** GetResult implicit for fetching ProductCharacteristicsRow objects using plain SQL queries */
  implicit def GetResultProductCharacteristicsRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[Option[String]]): GR[ProductCharacteristicsRow] = GR{
    prs => import prs._
    ProductCharacteristicsRow.tupled((<<[Int], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<?[String]))
  }
  /** Table description of table product_characteristics. Objects of this class serve as prototypes for rows in queries. */
  class ProductCharacteristics(_tableTag: Tag) extends profile.api.Table[ProductCharacteristicsRow](_tableTag, "product_characteristics") {
    def * = (product, addedSugar, meat, pork, corn, poultry, nonEdible, ingredients) <> (ProductCharacteristicsRow.tupled, ProductCharacteristicsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(addedSugar), Rep.Some(meat), Rep.Some(pork), Rep.Some(corn), Rep.Some(poultry), Rep.Some(nonEdible), ingredients).shaped.<>({r=>import r._; _1.map(_=> ProductCharacteristicsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4), PrimaryKey */
    val product: Rep[Int] = column[Int]("product", O.PrimaryKey)
    /** Database column added_sugar SqlType(bool), Default(false) */
    val addedSugar: Rep[Boolean] = column[Boolean]("added_sugar", O.Default(false))
    /** Database column meat SqlType(bool), Default(false) */
    val meat: Rep[Boolean] = column[Boolean]("meat", O.Default(false))
    /** Database column pork SqlType(bool), Default(false) */
    val pork: Rep[Boolean] = column[Boolean]("pork", O.Default(false))
    /** Database column corn SqlType(bool), Default(false) */
    val corn: Rep[Boolean] = column[Boolean]("corn", O.Default(false))
    /** Database column poultry SqlType(bool), Default(false) */
    val poultry: Rep[Boolean] = column[Boolean]("poultry", O.Default(false))
    /** Database column non_edible SqlType(bool), Default(false) */
    val nonEdible: Rep[Boolean] = column[Boolean]("non_edible", O.Default(false))
    /** Database column ingredients SqlType(text), Default(None) */
    val ingredients: Rep[Option[String]] = column[Option[String]]("ingredients", O.Default(None))

    /** Foreign key referencing Products (database name product_characteristics_product_fkey) */
    lazy val productsFk = foreignKey("product_characteristics_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table ProductCharacteristics */
  lazy val ProductCharacteristics = new TableQuery(tag => new ProductCharacteristics(tag))


  /** GetResult implicit for fetching ProductInfosRow objects using plain SQL queries */
  implicit def GetResultProductInfosRow(implicit e0: GR[Int], e1: GR[Boolean]): GR[ProductInfosRow] = GR{
    prs => import prs._
    ProductInfosRow.tupled((<<[Int], <<[Boolean]))
  }
  /** Table description of table product_infos. Objects of this class serve as prototypes for rows in queries. */
  class ProductInfos(_tableTag: Tag) extends profile.api.Table[ProductInfosRow](_tableTag, "product_infos") {
    def * = (product, lowestPricePreferred) <> (ProductInfosRow.tupled, ProductInfosRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(lowestPricePreferred)).shaped.<>({r=>import r._; _1.map(_=> ProductInfosRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4), PrimaryKey */
    val product: Rep[Int] = column[Int]("product", O.PrimaryKey)
    /** Database column lowest_price_preferred SqlType(bool), Default(true) */
    val lowestPricePreferred: Rep[Boolean] = column[Boolean]("lowest_price_preferred", O.Default(true))

    /** Foreign key referencing Products (database name product_infos_product_fkey) */
    lazy val productsFk = foreignKey("product_infos_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table ProductInfos */
  lazy val ProductInfos = new TableQuery(tag => new ProductInfos(tag))


  /** GetResult implicit for fetching ProductMeasuresRow objects using plain SQL queries */
  implicit def GetResultProductMeasuresRow(implicit e0: GR[Int], e1: GR[Double], e2: GR[Option[String]], e3: GR[Boolean]): GR[ProductMeasuresRow] = GR{
    prs => import prs._
    ProductMeasuresRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Double], <<?[String], <<[Boolean], <<[Double]))
  }
  /** Table description of table product_measures. Objects of this class serve as prototypes for rows in queries. */
  class ProductMeasures(_tableTag: Tag) extends profile.api.Table[ProductMeasuresRow](_tableTag, "product_measures") {
    def * = (id, product, measure, preferredUnit, conversionMeasure, conversionUnit, conversion, customName, tombstone, amount) <> (ProductMeasuresRow.tupled, ProductMeasuresRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(product), Rep.Some(measure), Rep.Some(preferredUnit), Rep.Some(conversionMeasure), Rep.Some(conversionUnit), Rep.Some(conversion), customName, Rep.Some(tombstone), Rep.Some(amount)).shaped.<>({r=>import r._; _1.map(_=> ProductMeasuresRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8, _9.get, _10.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column preferred_unit SqlType(int4) */
    val preferredUnit: Rep[Int] = column[Int]("preferred_unit")
    /** Database column conversion_measure SqlType(int4) */
    val conversionMeasure: Rep[Int] = column[Int]("conversion_measure")
    /** Database column conversion_unit SqlType(int4) */
    val conversionUnit: Rep[Int] = column[Int]("conversion_unit")
    /** Database column conversion SqlType(float8) */
    val conversion: Rep[Double] = column[Double]("conversion")
    /** Database column custom_name SqlType(text), Default(None) */
    val customName: Rep[Option[String]] = column[Option[String]]("custom_name", O.Default(None))
    /** Database column tombstone SqlType(bool), Default(false) */
    val tombstone: Rep[Boolean] = column[Boolean]("tombstone", O.Default(false))
    /** Database column amount SqlType(float8), Default(1.0) */
    val amount: Rep[Double] = column[Double]("amount", O.Default(1.0))

    /** Foreign key referencing Measures (database name product_measures_conversion_measure_fkey) */
    lazy val measuresFk1 = foreignKey("product_measures_conversion_measure_fkey", conversionMeasure, Measures)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name product_measures_measure_fkey) */
    lazy val measuresFk2 = foreignKey("product_measures_measure_fkey", measure, Measures)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name product_measures_conversion_measure_defined) */
    lazy val productMeasuresFk = foreignKey("product_measures_conversion_measure_defined", (product, conversionMeasure), ProductMeasures)(r => (r.product, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name product_measures_product_fkey) */
    lazy val productsFk = foreignKey("product_measures_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_measures_conversion_unit_fkey) */
    lazy val unitsFk5 = foreignKey("product_measures_conversion_unit_fkey", conversionUnit, Units)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_measures_preferred_unit_fkey) */
    lazy val unitsFk6 = foreignKey("product_measures_preferred_unit_fkey", preferredUnit, Units)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_measures_valid_conversion_unit) */
    lazy val unitsFk7 = foreignKey("product_measures_valid_conversion_unit", (conversionUnit, conversionMeasure), Units)(r => (r.id, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_measures_valid_preferred_unit) */
    lazy val unitsFk8 = foreignKey("product_measures_valid_preferred_unit", (preferredUnit, measure), Units)(r => (r.id, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (product,measure) (database name product_measures_product_measure_key) */
    val index1 = index("product_measures_product_measure_key", (product, measure), unique=true)
  }
  /** Collection-like TableQuery object for table ProductMeasures */
  lazy val ProductMeasures = new TableQuery(tag => new ProductMeasures(tag))


  /** GetResult implicit for fetching ProductNutrientsRow objects using plain SQL queries */
  implicit def GetResultProductNutrientsRow(implicit e0: GR[Int], e1: GR[scala.math.BigDecimal]): GR[ProductNutrientsRow] = GR{
    prs => import prs._
    ProductNutrientsRow.tupled((<<[Int], <<[Int], <<[scala.math.BigDecimal]))
  }
  /** Table description of table product_nutrients. Objects of this class serve as prototypes for rows in queries. */
  class ProductNutrients(_tableTag: Tag) extends profile.api.Table[ProductNutrientsRow](_tableTag, "product_nutrients") {
    def * = (product, nutrient, amount) <> (ProductNutrientsRow.tupled, ProductNutrientsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(nutrient), Rep.Some(amount)).shaped.<>({r=>import r._; _1.map(_=> ProductNutrientsRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column nutrient SqlType(int4) */
    val nutrient: Rep[Int] = column[Int]("nutrient")
    /** Database column amount SqlType(numeric) */
    val amount: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("amount")

    /** Primary key of ProductNutrients (database name product_nutrients_pkey) */
    val pk = primaryKey("product_nutrients_pkey", (product, nutrient))

    /** Foreign key referencing Nutrients (database name product_nutrients_nutrient_fkey) */
    lazy val nutrientsFk = foreignKey("product_nutrients_nutrient_fkey", nutrient, Nutrients)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name product_nutrients_product_fkey) */
    lazy val productsFk = foreignKey("product_nutrients_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table ProductNutrients */
  lazy val ProductNutrients = new TableQuery(tag => new ProductNutrients(tag))


  /** GetResult implicit for fetching ProductsRow objects using plain SQL queries */
  implicit def GetResultProductsRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[String], e3: GR[Boolean], e4: GR[Option[String]], e5: GR[Option[Instant]], e6: GR[Option[scala.math.BigDecimal]], e7: GR[Instant]): GR[ProductsRow] = GR{
    prs => import prs._
    ProductsRow.tupled((<<[Int], <<?[Int], <<[String], <<[Boolean], <<[Boolean], <<?[Int], <<?[String], <<?[Instant], <<?[Int], <<[Boolean], <<?[String], <<?[scala.math.BigDecimal], <<?[Int], <<?[Int], <<?[Instant], <<?[Instant], <<?[String], <<[Boolean], <<?[Int], <<?[Instant], <<[Instant], <<?[Instant]))
  }
  /** Table description of table products. Objects of this class serve as prototypes for rows in queries. */
  class Products(_tableTag: Tag) extends profile.api.Table[ProductsRow](_tableTag, "products") {
    def * = (id, owner, name, ingredient, salable, preferredSource, nndbsrId, nndbsrLastImport, syncUpstream, hidden, description, nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit, lastAddedToRecipe, lastRemovedFromInventory, photo, tombstone, syncUpstreamOwner, lastSynced, lastModified, lastRemovedFromPars) <> (ProductsRow.tupled, ProductsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), owner, Rep.Some(name), Rep.Some(ingredient), Rep.Some(salable), preferredSource, nndbsrId, nndbsrLastImport, syncUpstream, Rep.Some(hidden), description, nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit, lastAddedToRecipe, lastRemovedFromInventory, photo, Rep.Some(tombstone), syncUpstreamOwner, lastSynced, Rep.Some(lastModified), lastRemovedFromPars).shaped.<>({r=>import r._; _1.map(_=> ProductsRow.tupled((_1.get, _2, _3.get, _4.get, _5.get, _6, _7, _8, _9, _10.get, _11, _12, _13, _14, _15, _16, _17, _18.get, _19, _20, _21.get, _22)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4), Default(None) */
    val owner: Rep[Option[Int]] = column[Option[Int]]("owner", O.Default(None))
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column ingredient SqlType(bool), Default(false) */
    val ingredient: Rep[Boolean] = column[Boolean]("ingredient", O.Default(false))
    /** Database column salable SqlType(bool), Default(false) */
    val salable: Rep[Boolean] = column[Boolean]("salable", O.Default(false))
    /** Database column preferred_source SqlType(int4), Default(None) */
    val preferredSource: Rep[Option[Int]] = column[Option[Int]]("preferred_source", O.Default(None))
    /** Database column nndbsr_id SqlType(bpchar), Length(5,false), Default(None) */
    val nndbsrId: Rep[Option[String]] = column[Option[String]]("nndbsr_id", O.Length(5,varying=false), O.Default(None))
    /** Database column nndbsr_last_import SqlType(timestamp), Default(None) */
    val nndbsrLastImport: Rep[Option[Instant]] = column[Option[Instant]]("nndbsr_last_import", O.Default(None))
    /** Database column sync_upstream SqlType(int4), Default(None) */
    val syncUpstream: Rep[Option[Int]] = column[Option[Int]]("sync_upstream", O.Default(None))
    /** Database column hidden SqlType(bool), Default(false) */
    val hidden: Rep[Boolean] = column[Boolean]("hidden", O.Default(false))
    /** Database column description SqlType(text), Default(None) */
    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
    /** Database column nutrient_serving_amount SqlType(numeric), Default(None) */
    val nutrientServingAmount: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("nutrient_serving_amount", O.Default(None))
    /** Database column nutrient_serving_measure SqlType(int4), Default(None) */
    val nutrientServingMeasure: Rep[Option[Int]] = column[Option[Int]]("nutrient_serving_measure", O.Default(None))
    /** Database column nutrient_serving_unit SqlType(int4), Default(None) */
    val nutrientServingUnit: Rep[Option[Int]] = column[Option[Int]]("nutrient_serving_unit", O.Default(None))
    /** Database column last_added_to_recipe SqlType(timestamp), Default(None) */
    val lastAddedToRecipe: Rep[Option[Instant]] = column[Option[Instant]]("last_added_to_recipe", O.Default(None))
    /** Database column last_removed_from_inventory SqlType(timestamp), Default(None) */
    val lastRemovedFromInventory: Rep[Option[Instant]] = column[Option[Instant]]("last_removed_from_inventory", O.Default(None))
    /** Database column photo SqlType(text), Default(None) */
    val photo: Rep[Option[String]] = column[Option[String]]("photo", O.Default(None))
    /** Database column tombstone SqlType(bool), Default(false) */
    val tombstone: Rep[Boolean] = column[Boolean]("tombstone", O.Default(false))
    /** Database column sync_upstream_owner SqlType(int4), Default(None) */
    val syncUpstreamOwner: Rep[Option[Int]] = column[Option[Int]]("sync_upstream_owner", O.Default(None))
    /** Database column last_synced SqlType(timestamp), Default(None) */
    val lastSynced: Rep[Option[Instant]] = column[Option[Instant]]("last_synced", O.Default(None))
    /** Database column last_modified SqlType(timestamp) */
    val lastModified: Rep[Instant] = column[Instant]("last_modified")
    /** Database column last_removed_from_pars SqlType(timestamp), Default(None) */
    val lastRemovedFromPars: Rep[Option[Instant]] = column[Option[Instant]]("last_removed_from_pars", O.Default(None))

    /** Foreign key referencing Accounts (database name products_owner_fkey) */
    lazy val accountsFk = foreignKey("products_owner_fkey", owner, Accounts)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name nutrient_serving_measure_defined) */
    lazy val productMeasuresFk = foreignKey("nutrient_serving_measure_defined", (id, nutrientServingMeasure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductSources (database name products_preferred_source_fkey) */
    lazy val productSourcesFk = foreignKey("products_preferred_source_fkey", preferredSource, ProductSources)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing Products (database name products_stdlib_original_fkey) */
    lazy val productsFk4 = foreignKey("products_stdlib_original_fkey", syncUpstream, Products)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name sync_upstream_valid_owner) */
    lazy val productsFk5 = foreignKey("sync_upstream_valid_owner", (syncUpstream, syncUpstreamOwner), Products)(r => (Rep.Some(r.id), r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name nutrient_serving_valid_units) */
    lazy val unitsFk = foreignKey("nutrient_serving_valid_units", (nutrientServingUnit, nutrientServingMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (id,owner) (database name product_id_owner_references_hack_constraint) */
    val index1 = index("product_id_owner_references_hack_constraint", (id, owner), unique=true)
    /** Index over (nndbsrId) (database name products_nndbsr_stdlib) */
    val index2 = index("products_nndbsr_stdlib", nndbsrId)
  }
  /** Collection-like TableQuery object for table Products */
  lazy val Products = new TableQuery(tag => new Products(tag))


  /** GetResult implicit for fetching ProductSourcesRow objects using plain SQL queries */
  implicit def GetResultProductSourcesRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[Option[scala.math.BigDecimal]], e3: GR[Boolean], e4: GR[Option[String]], e5: GR[Double], e6: GR[PricePerType.Value]): GR[ProductSourcesRow] = GR{
    prs => import prs._
    ProductSourcesRow.tupled((<<[Int], <<[Int], <<?[Int], <<[Int], <<[Int], <<?[scala.math.BigDecimal], <<[Boolean], <<?[String], <<[Double], <<?[String], <<[Int], <<?[String], <<[PricePerType.Value], <<?[Int], <<?[Int], <<?[String], <<?[Int], <<?[Int], <<?[String], <<?[String], <<[Boolean]))
  }
  /** Table description of table product_sources. Objects of this class serve as prototypes for rows in queries. */
  class ProductSources(_tableTag: Tag) extends profile.api.Table[ProductSourcesRow](_tableTag, "product_sources") {
    def * = (id, product, supplier, measure, unit, cost, packaged, packageName, packageSize, sku, superPackageSize, subPackageName, pricePer, pricingMeasure, pricingUnit, supplierIngredientName, syncUpstream, syncUpstreamOwner, manufacturerName, manufacturerNumber, tombstone) <> (ProductSourcesRow.tupled, ProductSourcesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(product), supplier, Rep.Some(measure), Rep.Some(unit), cost, Rep.Some(packaged), packageName, Rep.Some(packageSize), sku, Rep.Some(superPackageSize), subPackageName, Rep.Some(pricePer), pricingMeasure, pricingUnit, supplierIngredientName, syncUpstream, syncUpstreamOwner, manufacturerName, manufacturerNumber, Rep.Some(tombstone)).shaped.<>({r=>import r._; _1.map(_=> ProductSourcesRow.tupled((_1.get, _2.get, _3, _4.get, _5.get, _6, _7.get, _8, _9.get, _10, _11.get, _12, _13.get, _14, _15, _16, _17, _18, _19, _20, _21.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column supplier SqlType(int4), Default(None) */
    val supplier: Rep[Option[Int]] = column[Option[Int]]("supplier", O.Default(None))
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column unit SqlType(int4) */
    val unit: Rep[Int] = column[Int]("unit")
    /** Database column cost SqlType(numeric), Default(None) */
    val cost: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("cost", O.Default(None))
    /** Database column packaged SqlType(bool), Default(false) */
    val packaged: Rep[Boolean] = column[Boolean]("packaged", O.Default(false))
    /** Database column package_name SqlType(text), Default(None) */
    val packageName: Rep[Option[String]] = column[Option[String]]("package_name", O.Default(None))
    /** Database column package_size SqlType(float8) */
    val packageSize: Rep[Double] = column[Double]("package_size")
    /** Database column sku SqlType(text), Default(None) */
    val sku: Rep[Option[String]] = column[Option[String]]("sku", O.Default(None))
    /** Database column super_package_size SqlType(int4), Default(1) */
    val superPackageSize: Rep[Int] = column[Int]("super_package_size", O.Default(1))
    /** Database column sub_package_name SqlType(text), Default(None) */
    val subPackageName: Rep[Option[String]] = column[Option[String]]("sub_package_name", O.Default(None))
    /** Database column price_per SqlType(price_per_type) */
    val pricePer: Rep[PricePerType.Value] = column[PricePerType.Value]("price_per")
    /** Database column pricing_measure SqlType(int4), Default(None) */
    val pricingMeasure: Rep[Option[Int]] = column[Option[Int]]("pricing_measure", O.Default(None))
    /** Database column pricing_unit SqlType(int4), Default(None) */
    val pricingUnit: Rep[Option[Int]] = column[Option[Int]]("pricing_unit", O.Default(None))
    /** Database column supplier_ingredient_name SqlType(text), Default(None) */
    val supplierIngredientName: Rep[Option[String]] = column[Option[String]]("supplier_ingredient_name", O.Default(None))
    /** Database column sync_upstream SqlType(int4), Default(None) */
    val syncUpstream: Rep[Option[Int]] = column[Option[Int]]("sync_upstream", O.Default(None))
    /** Database column sync_upstream_owner SqlType(int4), Default(None) */
    val syncUpstreamOwner: Rep[Option[Int]] = column[Option[Int]]("sync_upstream_owner", O.Default(None))
    /** Database column manufacturer_name SqlType(text), Default(None) */
    val manufacturerName: Rep[Option[String]] = column[Option[String]]("manufacturer_name", O.Default(None))
    /** Database column manufacturer_number SqlType(text), Default(None) */
    val manufacturerNumber: Rep[Option[String]] = column[Option[String]]("manufacturer_number", O.Default(None))
    /** Database column tombstone SqlType(bool), Default(false) */
    val tombstone: Rep[Boolean] = column[Boolean]("tombstone", O.Default(false))

    /** Foreign key referencing Accounts (database name product_sources_sync_upstream_owner_fkey) */
    lazy val accountsFk = foreignKey("product_sources_sync_upstream_owner_fkey", syncUpstreamOwner, Accounts)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name product_sources_measure_fkey) */
    lazy val measuresFk = foreignKey("product_sources_measure_fkey", measure, Measures)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name product_sources_measure_defined) */
    lazy val productMeasuresFk = foreignKey("product_sources_measure_defined", (product, measure), ProductMeasures)(r => (r.product, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductSources (database name product_sources_sync_upstream_fkey) */
    lazy val productSourcesFk = foreignKey("product_sources_sync_upstream_fkey", syncUpstream, ProductSources)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing Products (database name product_sources_product_fkey) */
    lazy val productsFk = foreignKey("product_sources_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name product_sources_supplier_fkey) */
    lazy val suppliersFk = foreignKey("product_sources_supplier_fkey", supplier, Suppliers)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_sources_unit_fkey) */
    lazy val unitsFk7 = foreignKey("product_sources_unit_fkey", unit, Units)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name product_sources_valid_units) */
    lazy val unitsFk8 = foreignKey("product_sources_valid_units", (unit, measure), Units)(r => (r.id, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Index over (supplier,product) (database name product_sources_supplier_product_idx) */
    val index1 = index("product_sources_supplier_product_idx", (supplier, product))
  }
  /** Collection-like TableQuery object for table ProductSources */
  lazy val ProductSources = new TableQuery(tag => new ProductSources(tag))


  /** GetResult implicit for fetching PurchaseOrderCustomerOrdersRow objects using plain SQL queries */
  implicit def GetResultPurchaseOrderCustomerOrdersRow(implicit e0: GR[Int]): GR[PurchaseOrderCustomerOrdersRow] = GR{
    prs => import prs._
    PurchaseOrderCustomerOrdersRow.tupled((<<[Int], <<[Int]))
  }
  /** Table description of table purchase_order_customer_orders. Objects of this class serve as prototypes for rows in queries. */
  class PurchaseOrderCustomerOrders(_tableTag: Tag) extends profile.api.Table[PurchaseOrderCustomerOrdersRow](_tableTag, "purchase_order_customer_orders") {
    def * = (purchaseOrder, customerOrder) <> (PurchaseOrderCustomerOrdersRow.tupled, PurchaseOrderCustomerOrdersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(purchaseOrder), Rep.Some(customerOrder)).shaped.<>({r=>import r._; _1.map(_=> PurchaseOrderCustomerOrdersRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column purchase_order SqlType(int4) */
    val purchaseOrder: Rep[Int] = column[Int]("purchase_order")
    /** Database column customer_order SqlType(int4) */
    val customerOrder: Rep[Int] = column[Int]("customer_order")

    /** Foreign key referencing Orders (database name purchase_order_customer_orders_customer_order_fkey) */
    lazy val ordersFk = foreignKey("purchase_order_customer_orders_customer_order_fkey", customerOrder, Orders)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing PurchaseOrders (database name purchase_order_customer_orders_purchase_order_fkey) */
    lazy val purchaseOrdersFk = foreignKey("purchase_order_customer_orders_purchase_order_fkey", purchaseOrder, PurchaseOrders)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table PurchaseOrderCustomerOrders */
  lazy val PurchaseOrderCustomerOrders = new TableQuery(tag => new PurchaseOrderCustomerOrders(tag))


  /** GetResult implicit for fetching PurchaseOrdersRow objects using plain SQL queries */
  implicit def GetResultPurchaseOrdersRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[Option[Instant]], e3: GR[Boolean]): GR[PurchaseOrdersRow] = GR{
    prs => import prs._
    PurchaseOrdersRow.tupled((<<[Int], <<?[Int], <<[Int], <<[Int], <<?[Instant], <<?[Instant], <<?[Int], <<[Int], <<?[Instant], <<[Boolean]))
  }
  /** Table description of table purchase_orders. Objects of this class serve as prototypes for rows in queries. */
  class PurchaseOrders(_tableTag: Tag) extends profile.api.Table[PurchaseOrdersRow](_tableTag, "purchase_orders") {
    def * = (id, owner, xact, supplier, emailedAt, printedAt, receivedBy, createdBy, importedAt, imported) <> (PurchaseOrdersRow.tupled, PurchaseOrdersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), owner, Rep.Some(xact), Rep.Some(supplier), emailedAt, printedAt, receivedBy, Rep.Some(createdBy), importedAt, Rep.Some(imported)).shaped.<>({r=>import r._; _1.map(_=> PurchaseOrdersRow.tupled((_1.get, _2, _3.get, _4.get, _5, _6, _7, _8.get, _9, _10.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4), Default(None) */
    val owner: Rep[Option[Int]] = column[Option[Int]]("owner", O.Default(None))
    /** Database column xact SqlType(int4) */
    val xact: Rep[Int] = column[Int]("xact")
    /** Database column supplier SqlType(int4) */
    val supplier: Rep[Int] = column[Int]("supplier")
    /** Database column emailed_at SqlType(timestamp), Default(None) */
    val emailedAt: Rep[Option[Instant]] = column[Option[Instant]]("emailed_at", O.Default(None))
    /** Database column printed_at SqlType(timestamp), Default(None) */
    val printedAt: Rep[Option[Instant]] = column[Option[Instant]]("printed_at", O.Default(None))
    /** Database column received_by SqlType(int4), Default(None) */
    val receivedBy: Rep[Option[Int]] = column[Option[Int]]("received_by", O.Default(None))
    /** Database column created_by SqlType(int4) */
    val createdBy: Rep[Int] = column[Int]("created_by")
    /** Database column imported_at SqlType(timestamp), Default(None) */
    val importedAt: Rep[Option[Instant]] = column[Option[Instant]]("imported_at", O.Default(None))
    /** Database column imported SqlType(bool), Default(false) */
    val imported: Rep[Boolean] = column[Boolean]("imported", O.Default(false))

    /** Foreign key referencing Accounts (database name purchase_orders_owner_fkey) */
    lazy val accountsFk = foreignKey("purchase_orders_owner_fkey", owner, Accounts)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name purchase_order_supplier_valid) */
    lazy val suppliersFk = foreignKey("purchase_order_supplier_valid", (owner, supplier), Suppliers)(r => (Rep.Some(r.owner), r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Transactions (database name purchase_orders_xact_fkey) */
    lazy val transactionsFk = foreignKey("purchase_orders_xact_fkey", xact, Transactions)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name purchase_orders_created_by_fkey) */
    lazy val usersFk4 = foreignKey("purchase_orders_created_by_fkey", createdBy, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name purchase_orders_received_by_fkey) */
    lazy val usersFk5 = foreignKey("purchase_orders_received_by_fkey", receivedBy, Users)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table PurchaseOrders */
  lazy val PurchaseOrders = new TableQuery(tag => new PurchaseOrders(tag))


  /** GetResult implicit for fetching RecipeNutrientsRow objects using plain SQL queries */
  implicit def GetResultRecipeNutrientsRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[Option[scala.math.BigDecimal]], e3: GR[Option[Int]], e4: GR[Option[String]]): GR[RecipeNutrientsRow] = GR{
    prs => import prs._
    RecipeNutrientsRow.tupled((<<[Int], <<[Boolean], <<?[scala.math.BigDecimal], <<?[Int], <<?[Int], <<?[scala.math.BigDecimal], <<?[scala.math.BigDecimal], <<?[String], <<?[scala.math.BigDecimal], <<?[Int], <<?[Int]))
  }
  /** Table description of table recipe_nutrients. Objects of this class serve as prototypes for rows in queries. */
  class RecipeNutrients(_tableTag: Tag) extends profile.api.Table[RecipeNutrientsRow](_tableTag, "recipe_nutrients") {
    def * = (product, packaged, portionAmount, portionMeasure, portionUnit, portionsPerPackage, servingsPerPackage, servingName, servingAmount, servingMeasure, servingUnit) <> (RecipeNutrientsRow.tupled, RecipeNutrientsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(packaged), portionAmount, portionMeasure, portionUnit, portionsPerPackage, servingsPerPackage, servingName, servingAmount, servingMeasure, servingUnit).shaped.<>({r=>import r._; _1.map(_=> RecipeNutrientsRow.tupled((_1.get, _2.get, _3, _4, _5, _6, _7, _8, _9, _10, _11)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4), PrimaryKey */
    val product: Rep[Int] = column[Int]("product", O.PrimaryKey)
    /** Database column packaged SqlType(bool), Default(false) */
    val packaged: Rep[Boolean] = column[Boolean]("packaged", O.Default(false))
    /** Database column portion_amount SqlType(numeric), Default(None) */
    val portionAmount: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("portion_amount", O.Default(None))
    /** Database column portion_measure SqlType(int4), Default(None) */
    val portionMeasure: Rep[Option[Int]] = column[Option[Int]]("portion_measure", O.Default(None))
    /** Database column portion_unit SqlType(int4), Default(None) */
    val portionUnit: Rep[Option[Int]] = column[Option[Int]]("portion_unit", O.Default(None))
    /** Database column portions_per_package SqlType(numeric), Default(None) */
    val portionsPerPackage: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("portions_per_package", O.Default(None))
    /** Database column servings_per_package SqlType(numeric), Default(None) */
    val servingsPerPackage: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("servings_per_package", O.Default(None))
    /** Database column serving_name SqlType(text), Default(None) */
    val servingName: Rep[Option[String]] = column[Option[String]]("serving_name", O.Default(None))
    /** Database column serving_amount SqlType(numeric), Default(None) */
    val servingAmount: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("serving_amount", O.Default(None))
    /** Database column serving_measure SqlType(int4), Default(None) */
    val servingMeasure: Rep[Option[Int]] = column[Option[Int]]("serving_measure", O.Default(None))
    /** Database column serving_unit SqlType(int4), Default(None) */
    val servingUnit: Rep[Option[Int]] = column[Option[Int]]("serving_unit", O.Default(None))

    /** Foreign key referencing Measures (database name recipe_nutrients_portion_measure_fkey) */
    lazy val measuresFk1 = foreignKey("recipe_nutrients_portion_measure_fkey", portionMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name recipe_nutrients_serving_measure_fkey) */
    lazy val measuresFk2 = foreignKey("recipe_nutrients_serving_measure_fkey", servingMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name recipe_nutrients_product_fkey) */
    lazy val productsFk = foreignKey("recipe_nutrients_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name portion_unit_valid) */
    lazy val unitsFk4 = foreignKey("portion_unit_valid", (portionUnit, portionMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_nutrients_portion_unit_fkey) */
    lazy val unitsFk5 = foreignKey("recipe_nutrients_portion_unit_fkey", portionUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_nutrients_serving_unit_fkey) */
    lazy val unitsFk6 = foreignKey("recipe_nutrients_serving_unit_fkey", servingUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name serving_unit_valid) */
    lazy val unitsFk7 = foreignKey("serving_unit_valid", (servingUnit, servingMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table RecipeNutrients */
  lazy val RecipeNutrients = new TableQuery(tag => new RecipeNutrients(tag))


  /** GetResult implicit for fetching RecipesRow objects using plain SQL queries */
  implicit def GetResultRecipesRow(implicit e0: GR[Int], e1: GR[Double], e2: GR[Option[Int]], e3: GR[Option[Double]], e4: GR[Option[scala.math.BigDecimal]], e5: GR[Option[String]], e6: GR[Boolean], e7: GR[Option[Instant]]): GR[RecipesRow] = GR{
    prs => import prs._
    RecipesRow.tupled((<<[Int], <<[Double], <<?[Int], <<?[Int], <<?[Double], <<?[Int], <<?[Int], <<?[Double], <<?[Int], <<?[Int], <<?[scala.math.BigDecimal], <<?[scala.math.BigDecimal], <<?[String], <<[Boolean], <<?[Instant], <<?[Int], <<?[String]))
  }
  /** Table description of table recipes. Objects of this class serve as prototypes for rows in queries. */
  class Recipes(_tableTag: Tag) extends profile.api.Table[RecipesRow](_tableTag, "recipes") {
    def * = (product, outputAmount, outputMeasure, outputUnit, quantumAmount, quantumMeasure, quantumUnit, maxBatchAmount, maxBatchMeasure, maxBatchUnit, price, batchSize, batchLabel, advancePrep, createdAt, createdBy, itemNumber) <> (RecipesRow.tupled, RecipesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(product), Rep.Some(outputAmount), outputMeasure, outputUnit, quantumAmount, quantumMeasure, quantumUnit, maxBatchAmount, maxBatchMeasure, maxBatchUnit, price, batchSize, batchLabel, Rep.Some(advancePrep), createdAt, createdBy, itemNumber).shaped.<>({r=>import r._; _1.map(_=> RecipesRow.tupled((_1.get, _2.get, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14.get, _15, _16, _17)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column product SqlType(int4), PrimaryKey */
    val product: Rep[Int] = column[Int]("product", O.PrimaryKey)
    /** Database column output_amount SqlType(float8) */
    val outputAmount: Rep[Double] = column[Double]("output_amount")
    /** Database column output_measure SqlType(int4), Default(None) */
    val outputMeasure: Rep[Option[Int]] = column[Option[Int]]("output_measure", O.Default(None))
    /** Database column output_unit SqlType(int4), Default(None) */
    val outputUnit: Rep[Option[Int]] = column[Option[Int]]("output_unit", O.Default(None))
    /** Database column quantum_amount SqlType(float8), Default(None) */
    val quantumAmount: Rep[Option[Double]] = column[Option[Double]]("quantum_amount", O.Default(None))
    /** Database column quantum_measure SqlType(int4), Default(None) */
    val quantumMeasure: Rep[Option[Int]] = column[Option[Int]]("quantum_measure", O.Default(None))
    /** Database column quantum_unit SqlType(int4), Default(None) */
    val quantumUnit: Rep[Option[Int]] = column[Option[Int]]("quantum_unit", O.Default(None))
    /** Database column max_batch_amount SqlType(float8), Default(None) */
    val maxBatchAmount: Rep[Option[Double]] = column[Option[Double]]("max_batch_amount", O.Default(None))
    /** Database column max_batch_measure SqlType(int4), Default(None) */
    val maxBatchMeasure: Rep[Option[Int]] = column[Option[Int]]("max_batch_measure", O.Default(None))
    /** Database column max_batch_unit SqlType(int4), Default(None) */
    val maxBatchUnit: Rep[Option[Int]] = column[Option[Int]]("max_batch_unit", O.Default(None))
    /** Database column price SqlType(numeric), Default(None) */
    val price: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("price", O.Default(None))
    /** Database column batch_size SqlType(numeric), Default(None) */
    val batchSize: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("batch_size", O.Default(None))
    /** Database column batch_label SqlType(text), Default(None) */
    val batchLabel: Rep[Option[String]] = column[Option[String]]("batch_label", O.Default(None))
    /** Database column advance_prep SqlType(bool), Default(false) */
    val advancePrep: Rep[Boolean] = column[Boolean]("advance_prep", O.Default(false))
    /** Database column created_at SqlType(timestamp), Default(None) */
    val createdAt: Rep[Option[Instant]] = column[Option[Instant]]("created_at", O.Default(None))
    /** Database column created_by SqlType(int4), Default(None) */
    val createdBy: Rep[Option[Int]] = column[Option[Int]]("created_by", O.Default(None))
    /** Database column item_number SqlType(text), Default(None) */
    val itemNumber: Rep[Option[String]] = column[Option[String]]("item_number", O.Default(None))

    /** Foreign key referencing Measures (database name recipes_max_batch_measure_fkey) */
    lazy val measuresFk1 = foreignKey("recipes_max_batch_measure_fkey", maxBatchMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name recipes_min_batch_measure_fkey) */
    lazy val measuresFk2 = foreignKey("recipes_min_batch_measure_fkey", quantumMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name recipes_output_measure_fkey) */
    lazy val measuresFk3 = foreignKey("recipes_output_measure_fkey", outputMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name recipe_max_batch_measure_defined) */
    lazy val productMeasuresFk4 = foreignKey("recipe_max_batch_measure_defined", (product, maxBatchMeasure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name recipe_min_batch_measure_defined) */
    lazy val productMeasuresFk5 = foreignKey("recipe_min_batch_measure_defined", (product, quantumMeasure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name recipe_output_measure_defined) */
    lazy val productMeasuresFk6 = foreignKey("recipe_output_measure_defined", (product, outputMeasure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name recipes_product_fkey) */
    lazy val productsFk = foreignKey("recipes_product_fkey", product, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_max_batch_valid_units) */
    lazy val unitsFk8 = foreignKey("recipe_max_batch_valid_units", (maxBatchUnit, maxBatchMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_min_batch_valid_units) */
    lazy val unitsFk9 = foreignKey("recipe_min_batch_valid_units", (quantumUnit, quantumMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_output_valid_units) */
    lazy val unitsFk10 = foreignKey("recipe_output_valid_units", (outputUnit, outputMeasure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipes_max_batch_unit_fkey) */
    lazy val unitsFk11 = foreignKey("recipes_max_batch_unit_fkey", maxBatchUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipes_min_batch_unit_fkey) */
    lazy val unitsFk12 = foreignKey("recipes_min_batch_unit_fkey", quantumUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipes_output_unit_fkey) */
    lazy val unitsFk13 = foreignKey("recipes_output_unit_fkey", outputUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name recipes_created_by_fkey) */
    lazy val usersFk = foreignKey("recipes_created_by_fkey", createdBy, Users)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Recipes */
  lazy val Recipes = new TableQuery(tag => new Recipes(tag))


  /** GetResult implicit for fetching RecipesRevisionHistoryRow objects using plain SQL queries */
  implicit def GetResultRecipesRevisionHistoryRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[Instant]): GR[RecipesRevisionHistoryRow] = GR{
    prs => import prs._
    RecipesRevisionHistoryRow.tupled((<<[Int], <<[Int], <<?[Int], <<[Instant]))
  }
  /** Table description of table recipes_revision_history. Objects of this class serve as prototypes for rows in queries. */
  class RecipesRevisionHistory(_tableTag: Tag) extends profile.api.Table[RecipesRevisionHistoryRow](_tableTag, "recipes_revision_history") {
    def * = (id, product, owner, lastModified) <> (RecipesRevisionHistoryRow.tupled, RecipesRevisionHistoryRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(product), owner, Rep.Some(lastModified)).shaped.<>({r=>import r._; _1.map(_=> RecipesRevisionHistoryRow.tupled((_1.get, _2.get, _3, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column owner SqlType(int4), Default(None) */
    val owner: Rep[Option[Int]] = column[Option[Int]]("owner", O.Default(None))
    /** Database column last_modified SqlType(timestamp) */
    val lastModified: Rep[Instant] = column[Instant]("last_modified")

    /** Foreign key referencing Recipes (database name recipes_revision_history_product_fkey) */
    lazy val recipesFk = foreignKey("recipes_revision_history_product_fkey", product, Recipes)(r => r.product, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name recipes_revision_history_owner_fkey) */
    lazy val usersFk = foreignKey("recipes_revision_history_owner_fkey", owner, Users)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table RecipesRevisionHistory */
  lazy val RecipesRevisionHistory = new TableQuery(tag => new RecipesRevisionHistory(tag))


  /** GetResult implicit for fetching RecipeStepIngredientsRow objects using plain SQL queries */
  implicit def GetResultRecipeStepIngredientsRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Option[Double]], e3: GR[Option[Int]]): GR[RecipeStepIngredientsRow] = GR{
    prs => import prs._
    RecipeStepIngredientsRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<?[String], <<?[Double], <<?[Int], <<?[Int]))
  }
  /** Table description of table recipe_step_ingredients. Objects of this class serve as prototypes for rows in queries. */
  class RecipeStepIngredients(_tableTag: Tag) extends profile.api.Table[RecipeStepIngredientsRow](_tableTag, "recipe_step_ingredients") {
    def * = (id, step, displayOrder, ingredient, description, amount, measure, unit) <> (RecipeStepIngredientsRow.tupled, RecipeStepIngredientsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(step), Rep.Some(displayOrder), Rep.Some(ingredient), description, amount, measure, unit).shaped.<>({r=>import r._; _1.map(_=> RecipeStepIngredientsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6, _7, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column step SqlType(int4) */
    val step: Rep[Int] = column[Int]("step")
    /** Database column display_order SqlType(int4) */
    val displayOrder: Rep[Int] = column[Int]("display_order")
    /** Database column ingredient SqlType(int4) */
    val ingredient: Rep[Int] = column[Int]("ingredient")
    /** Database column description SqlType(text), Default(Some()) */
    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(Some("")))
    /** Database column amount SqlType(float8), Default(None) */
    val amount: Rep[Option[Double]] = column[Option[Double]]("amount", O.Default(None))
    /** Database column measure SqlType(int4), Default(None) */
    val measure: Rep[Option[Int]] = column[Option[Int]]("measure", O.Default(None))
    /** Database column unit SqlType(int4), Default(None) */
    val unit: Rep[Option[Int]] = column[Option[Int]]("unit", O.Default(None))

    /** Foreign key referencing Measures (database name recipe_step_ingredients_measure_fkey) */
    lazy val measuresFk = foreignKey("recipe_step_ingredients_measure_fkey", measure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name recipe_step_ingredients_measure_defined) */
    lazy val productMeasuresFk = foreignKey("recipe_step_ingredients_measure_defined", (ingredient, measure), ProductMeasures)(r => (r.product, Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name recipe_step_ingredients_ingredient_fkey) */
    lazy val productsFk = foreignKey("recipe_step_ingredients_ingredient_fkey", ingredient, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing RecipeSteps (database name recipe_step_ingredients_step_fkey) */
    lazy val recipeStepsFk = foreignKey("recipe_step_ingredients_step_fkey", step, RecipeSteps)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_step_ingredients_unit_fkey) */
    lazy val unitsFk5 = foreignKey("recipe_step_ingredients_unit_fkey", unit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name recipe_step_ingredients_valid_units) */
    lazy val unitsFk6 = foreignKey("recipe_step_ingredients_valid_units", (unit, measure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (step,displayOrder) (database name recipe_step_ingredients_step_display_order_key) */
    val index1 = index("recipe_step_ingredients_step_display_order_key", (step, displayOrder), unique=true)
  }
  /** Collection-like TableQuery object for table RecipeStepIngredients */
  lazy val RecipeStepIngredients = new TableQuery(tag => new RecipeStepIngredients(tag))


  /** GetResult implicit for fetching RecipeStepsRow objects using plain SQL queries */
  implicit def GetResultRecipeStepsRow(implicit e0: GR[Int], e1: GR[Option[String]]): GR[RecipeStepsRow] = GR{
    prs => import prs._
    RecipeStepsRow.tupled((<<[Int], <<[Int], <<[Int], <<?[String], <<?[String]))
  }
  /** Table description of table recipe_steps. Objects of this class serve as prototypes for rows in queries. */
  class RecipeSteps(_tableTag: Tag) extends profile.api.Table[RecipeStepsRow](_tableTag, "recipe_steps") {
    def * = (id, product, displayOrder, description, photo) <> (RecipeStepsRow.tupled, RecipeStepsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(product), Rep.Some(displayOrder), description, photo).shaped.<>({r=>import r._; _1.map(_=> RecipeStepsRow.tupled((_1.get, _2.get, _3.get, _4, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column product SqlType(int4) */
    val product: Rep[Int] = column[Int]("product")
    /** Database column display_order SqlType(int4) */
    val displayOrder: Rep[Int] = column[Int]("display_order")
    /** Database column description SqlType(text), Default(None) */
    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
    /** Database column photo SqlType(text), Default(None) */
    val photo: Rep[Option[String]] = column[Option[String]]("photo", O.Default(None))

    /** Foreign key referencing Recipes (database name recipe_steps_product_fkey) */
    lazy val recipesFk = foreignKey("recipe_steps_product_fkey", product, Recipes)(r => r.product, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (product,displayOrder) (database name recipe_steps_product_display_order_key) */
    val index1 = index("recipe_steps_product_display_order_key", (product, displayOrder), unique=true)
  }
  /** Collection-like TableQuery object for table RecipeSteps */
  lazy val RecipeSteps = new TableQuery(tag => new RecipeSteps(tag))


  /** GetResult implicit for fetching SimplePreparationsRow objects using plain SQL queries */
  implicit def GetResultSimplePreparationsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean], e3: GR[Double]): GR[SimplePreparationsRow] = GR{
    prs => import prs._
    SimplePreparationsRow.tupled((<<[Int], <<[Int], <<[String], <<[Boolean], <<[Boolean], <<[Double], <<[Int]))
  }
  /** Table description of table simple_preparations. Objects of this class serve as prototypes for rows in queries. */
  class SimplePreparations(_tableTag: Tag) extends profile.api.Table[SimplePreparationsRow](_tableTag, "simple_preparations") {
    def * = (outputProduct, inputProduct, name, advancePrep, usesParentMeasures, yieldFraction, yieldMeasure) <> (SimplePreparationsRow.tupled, SimplePreparationsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(outputProduct), Rep.Some(inputProduct), Rep.Some(name), Rep.Some(advancePrep), Rep.Some(usesParentMeasures), Rep.Some(yieldFraction), Rep.Some(yieldMeasure)).shaped.<>({r=>import r._; _1.map(_=> SimplePreparationsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column output_product SqlType(int4), PrimaryKey */
    val outputProduct: Rep[Int] = column[Int]("output_product", O.PrimaryKey)
    /** Database column input_product SqlType(int4) */
    val inputProduct: Rep[Int] = column[Int]("input_product")
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column advance_prep SqlType(bool) */
    val advancePrep: Rep[Boolean] = column[Boolean]("advance_prep")
    /** Database column uses_parent_measures SqlType(bool) */
    val usesParentMeasures: Rep[Boolean] = column[Boolean]("uses_parent_measures")
    /** Database column yield_fraction SqlType(float8), Default(1.0) */
    val yieldFraction: Rep[Double] = column[Double]("yield_fraction", O.Default(1.0))
    /** Database column yield_measure SqlType(int4) */
    val yieldMeasure: Rep[Int] = column[Int]("yield_measure")

    /** Foreign key referencing ProductMeasures (database name simple_prep_valid_yield_measure_input) */
    lazy val productMeasuresFk1 = foreignKey("simple_prep_valid_yield_measure_input", (inputProduct, yieldMeasure), ProductMeasures)(r => (r.product, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name simple_prep_valid_yield_measure_output) */
    lazy val productMeasuresFk2 = foreignKey("simple_prep_valid_yield_measure_output", (outputProduct, yieldMeasure), ProductMeasures)(r => (r.product, r.measure), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name simple_preparations_input_product_fkey) */
    lazy val productsFk3 = foreignKey("simple_preparations_input_product_fkey", inputProduct, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name simple_preparations_output_product_fkey) */
    lazy val productsFk4 = foreignKey("simple_preparations_output_product_fkey", outputProduct, Products)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (inputProduct,name) (database name simple_prep_no_duplicate_names) */
    val index1 = index("simple_prep_no_duplicate_names", (inputProduct, name), unique=true)
  }
  /** Collection-like TableQuery object for table SimplePreparations */
  lazy val SimplePreparations = new TableQuery(tag => new SimplePreparations(tag))


  /** GetResult implicit for fetching SubscriptionsRow objects using plain SQL queries */
  implicit def GetResultSubscriptionsRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[String], e3: GR[Instant], e4: GR[Option[String]], e5: GR[List[String]]): GR[SubscriptionsRow] = GR{
    prs => import prs._
    SubscriptionsRow.tupled((<<[Int], <<[Int], <<[Boolean], <<[String], <<[String], <<[Instant], <<[Instant], <<?[String], <<[Boolean], <<[String], <<[List[String]]))
  }
  /** Table description of table subscriptions. Objects of this class serve as prototypes for rows in queries. */
  class Subscriptions(_tableTag: Tag) extends profile.api.Table[SubscriptionsRow](_tableTag, "subscriptions") {
    def * = (id, userId, cancelled, stripeSubscriptionId, stripePlanId, planStarted, planValidUntil, promoCode, pastDue, subscriptionLevel, addons) <> (SubscriptionsRow.tupled, SubscriptionsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(userId), Rep.Some(cancelled), Rep.Some(stripeSubscriptionId), Rep.Some(stripePlanId), Rep.Some(planStarted), Rep.Some(planValidUntil), promoCode, Rep.Some(pastDue), Rep.Some(subscriptionLevel), Rep.Some(addons)).shaped.<>({r=>import r._; _1.map(_=> SubscriptionsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8, _9.get, _10.get, _11.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column cancelled SqlType(bool), Default(false) */
    val cancelled: Rep[Boolean] = column[Boolean]("cancelled", O.Default(false))
    /** Database column stripe_subscription_id SqlType(text) */
    val stripeSubscriptionId: Rep[String] = column[String]("stripe_subscription_id")
    /** Database column stripe_plan_id SqlType(text) */
    val stripePlanId: Rep[String] = column[String]("stripe_plan_id")
    /** Database column plan_started SqlType(timestamp) */
    val planStarted: Rep[Instant] = column[Instant]("plan_started")
    /** Database column plan_valid_until SqlType(timestamp) */
    val planValidUntil: Rep[Instant] = column[Instant]("plan_valid_until")
    /** Database column promo_code SqlType(text), Default(None) */
    val promoCode: Rep[Option[String]] = column[Option[String]]("promo_code", O.Default(None))
    /** Database column past_due SqlType(bool), Default(false) */
    val pastDue: Rep[Boolean] = column[Boolean]("past_due", O.Default(false))
    /** Database column subscription_level SqlType(text) */
    val subscriptionLevel: Rep[String] = column[String]("subscription_level")
    /** Database column addons SqlType(_text), Length(2147483647,false) */
    val addons: Rep[List[String]] = column[List[String]]("addons", O.Length(2147483647,varying=false))

    /** Foreign key referencing Accounts (database name subscriptions_user_id_fkey) */
    lazy val accountsFk = foreignKey("subscriptions_user_id_fkey", userId, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Subscriptions */
  lazy val Subscriptions = new TableQuery(tag => new Subscriptions(tag))


  /** GetResult implicit for fetching SupplierContactsRow objects using plain SQL queries */
  implicit def GetResultSupplierContactsRow(implicit e0: GR[Int], e1: GR[Option[String]]): GR[SupplierContactsRow] = GR{
    prs => import prs._
    SupplierContactsRow.tupled((<<[Int], <<[Int], <<[Int], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table supplier_contacts. Objects of this class serve as prototypes for rows in queries. */
  class SupplierContacts(_tableTag: Tag) extends profile.api.Table[SupplierContactsRow](_tableTag, "supplier_contacts") {
    def * = (id, supplierId, displayOrder, name, phone, email, comments) <> (SupplierContactsRow.tupled, SupplierContactsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(supplierId), Rep.Some(displayOrder), name, phone, email, comments).shaped.<>({r=>import r._; _1.map(_=> SupplierContactsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column supplier_id SqlType(int4) */
    val supplierId: Rep[Int] = column[Int]("supplier_id")
    /** Database column display_order SqlType(int4) */
    val displayOrder: Rep[Int] = column[Int]("display_order")
    /** Database column name SqlType(text), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Default(None))
    /** Database column phone SqlType(text), Default(None) */
    val phone: Rep[Option[String]] = column[Option[String]]("phone", O.Default(None))
    /** Database column email SqlType(text), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Default(None))
    /** Database column comments SqlType(text), Default(None) */
    val comments: Rep[Option[String]] = column[Option[String]]("comments", O.Default(None))

    /** Foreign key referencing Suppliers (database name supplier_contacts_supplier_id_fkey) */
    lazy val suppliersFk = foreignKey("supplier_contacts_supplier_id_fkey", supplierId, Suppliers)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table SupplierContacts */
  lazy val SupplierContacts = new TableQuery(tag => new SupplierContacts(tag))


  /** GetResult implicit for fetching SuppliersRow objects using plain SQL queries */
  implicit def GetResultSuppliersRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Option[Int]], e3: GR[Option[Instant]], e4: GR[Boolean]): GR[SuppliersRow] = GR{
    prs => import prs._
    SuppliersRow.tupled((<<[Int], <<[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[Int], <<?[Int], <<?[Instant], <<?[Instant], <<[Boolean]))
  }
  /** Table description of table suppliers. Objects of this class serve as prototypes for rows in queries. */
  class Suppliers(_tableTag: Tag) extends profile.api.Table[SuppliersRow](_tableTag, "suppliers") {
    def * = (id, owner, name, accountId, phone, email, comments, instructions, contactName, syncUpstream, syncUpstreamOwner, lastSynced, lastModified, tombstone) <> (SuppliersRow.tupled, SuppliersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), name, accountId, phone, email, comments, instructions, contactName, syncUpstream, syncUpstreamOwner, lastSynced, lastModified, Rep.Some(tombstone)).shaped.<>({r=>import r._; _1.map(_=> SuppliersRow.tupled((_1.get, _2.get, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column name SqlType(text), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Default(None))
    /** Database column account_id SqlType(text), Default(None) */
    val accountId: Rep[Option[String]] = column[Option[String]]("account_id", O.Default(None))
    /** Database column phone SqlType(text), Default(None) */
    val phone: Rep[Option[String]] = column[Option[String]]("phone", O.Default(None))
    /** Database column email SqlType(text), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Default(None))
    /** Database column comments SqlType(text), Default(None) */
    val comments: Rep[Option[String]] = column[Option[String]]("comments", O.Default(None))
    /** Database column instructions SqlType(text), Default(None) */
    val instructions: Rep[Option[String]] = column[Option[String]]("instructions", O.Default(None))
    /** Database column contact_name SqlType(text), Default(None) */
    val contactName: Rep[Option[String]] = column[Option[String]]("contact_name", O.Default(None))
    /** Database column sync_upstream SqlType(int4), Default(None) */
    val syncUpstream: Rep[Option[Int]] = column[Option[Int]]("sync_upstream", O.Default(None))
    /** Database column sync_upstream_owner SqlType(int4), Default(None) */
    val syncUpstreamOwner: Rep[Option[Int]] = column[Option[Int]]("sync_upstream_owner", O.Default(None))
    /** Database column last_synced SqlType(timestamp), Default(None) */
    val lastSynced: Rep[Option[Instant]] = column[Option[Instant]]("last_synced", O.Default(None))
    /** Database column last_modified SqlType(timestamp), Default(None) */
    val lastModified: Rep[Option[Instant]] = column[Option[Instant]]("last_modified", O.Default(None))
    /** Database column tombstone SqlType(bool), Default(false) */
    val tombstone: Rep[Boolean] = column[Boolean]("tombstone", O.Default(false))

    /** Foreign key referencing Accounts (database name suppliers_owner_fkey) */
    lazy val accountsFk = foreignKey("suppliers_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Suppliers (database name sync_upstream_valid_owner) */
    lazy val suppliersFk = foreignKey("sync_upstream_valid_owner", (syncUpstream, syncUpstreamOwner), Suppliers)(r => (Rep.Some(r.id), Rep.Some(r.owner)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (owner,id) (database name supplier_owner_and_id_for_fks_idx) */
    val index1 = index("supplier_owner_and_id_for_fks_idx", (owner, id), unique=true)
    /** Index over (syncUpstream) (database name suppliers_sync_upstream_idx) */
    val index2 = index("suppliers_sync_upstream_idx", syncUpstream)
    /** Index over (syncUpstream) (database name sync_downstream_suppliers) */
    val index3 = index("sync_downstream_suppliers", syncUpstream)
  }
  /** Collection-like TableQuery object for table Suppliers */
  lazy val Suppliers = new TableQuery(tag => new Suppliers(tag))


  /** GetResult implicit for fetching TransactionDeltasRow objects using plain SQL queries */
  implicit def GetResultTransactionDeltasRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[Double], e3: GR[Option[scala.math.BigDecimal]], e4: GR[Option[Boolean]], e5: GR[Option[PricePerType.Value]], e6: GR[Option[String]], e7: GR[Option[Double]]): GR[TransactionDeltasRow] = GR{
    prs => import prs._
    TransactionDeltasRow.tupled((<<[Int], <<[Int], <<[Int], <<?[Int], <<[Double], <<?[Int], <<?[Int], <<?[scala.math.BigDecimal], <<?[Int], <<?[Int], <<?[Int], <<?[Boolean], <<?[PricePerType.Value], <<?[Int], <<?[Int], <<?[String], <<?[String], <<?[Double], <<?[Double], <<?[Int]))
  }
  /** Table description of table transaction_deltas. Objects of this class serve as prototypes for rows in queries. */
  class TransactionDeltas(_tableTag: Tag) extends profile.api.Table[TransactionDeltasRow](_tableTag, "transaction_deltas") {
    def * = (id, xact, owner, product, amount, measure, unit, cashFlow, productSource, sourceMeasure, sourceUnit, packaged, pricePer, pricingMeasure, pricingUnit, packageName, subPackageName, packageSize, superPackageSize, causedBy) <> (TransactionDeltasRow.tupled, TransactionDeltasRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(xact), Rep.Some(owner), product, Rep.Some(amount), measure, unit, cashFlow, productSource, sourceMeasure, sourceUnit, packaged, pricePer, pricingMeasure, pricingUnit, packageName, subPackageName, packageSize, superPackageSize, causedBy).shaped.<>({r=>import r._; _1.map(_=> TransactionDeltasRow.tupled((_1.get, _2.get, _3.get, _4, _5.get, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column xact SqlType(int4) */
    val xact: Rep[Int] = column[Int]("xact")
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column product SqlType(int4), Default(None) */
    val product: Rep[Option[Int]] = column[Option[Int]]("product", O.Default(None))
    /** Database column amount SqlType(float8) */
    val amount: Rep[Double] = column[Double]("amount")
    /** Database column measure SqlType(int4), Default(None) */
    val measure: Rep[Option[Int]] = column[Option[Int]]("measure", O.Default(None))
    /** Database column unit SqlType(int4), Default(None) */
    val unit: Rep[Option[Int]] = column[Option[Int]]("unit", O.Default(None))
    /** Database column cash_flow SqlType(numeric), Default(None) */
    val cashFlow: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("cash_flow", O.Default(None))
    /** Database column product_source SqlType(int4), Default(None) */
    val productSource: Rep[Option[Int]] = column[Option[Int]]("product_source", O.Default(None))
    /** Database column source_measure SqlType(int4), Default(None) */
    val sourceMeasure: Rep[Option[Int]] = column[Option[Int]]("source_measure", O.Default(None))
    /** Database column source_unit SqlType(int4), Default(None) */
    val sourceUnit: Rep[Option[Int]] = column[Option[Int]]("source_unit", O.Default(None))
    /** Database column packaged SqlType(bool), Default(None) */
    val packaged: Rep[Option[Boolean]] = column[Option[Boolean]]("packaged", O.Default(None))
    /** Database column price_per SqlType(price_per_type), Default(None) */
    val pricePer: Rep[Option[PricePerType.Value]] = column[Option[PricePerType.Value]]("price_per", O.Default(None))
    /** Database column pricing_measure SqlType(int4), Default(None) */
    val pricingMeasure: Rep[Option[Int]] = column[Option[Int]]("pricing_measure", O.Default(None))
    /** Database column pricing_unit SqlType(int4), Default(None) */
    val pricingUnit: Rep[Option[Int]] = column[Option[Int]]("pricing_unit", O.Default(None))
    /** Database column package_name SqlType(text), Default(None) */
    val packageName: Rep[Option[String]] = column[Option[String]]("package_name", O.Default(None))
    /** Database column sub_package_name SqlType(text), Default(None) */
    val subPackageName: Rep[Option[String]] = column[Option[String]]("sub_package_name", O.Default(None))
    /** Database column package_size SqlType(float8), Default(None) */
    val packageSize: Rep[Option[Double]] = column[Option[Double]]("package_size", O.Default(None))
    /** Database column super_package_size SqlType(float8), Default(None) */
    val superPackageSize: Rep[Option[Double]] = column[Option[Double]]("super_package_size", O.Default(None))
    /** Database column caused_by SqlType(int4), Default(None) */
    val causedBy: Rep[Option[Int]] = column[Option[Int]]("caused_by", O.Default(None))

    /** Foreign key referencing Accounts (database name transaction_deltas_owner_fkey) */
    lazy val accountsFk = foreignKey("transaction_deltas_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name transaction_deltas_measure_fkey) */
    lazy val measuresFk2 = foreignKey("transaction_deltas_measure_fkey", measure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name transaction_deltas_pricing_measure_fkey) */
    lazy val measuresFk3 = foreignKey("transaction_deltas_pricing_measure_fkey", pricingMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Measures (database name transaction_deltas_source_measure_fkey) */
    lazy val measuresFk4 = foreignKey("transaction_deltas_source_measure_fkey", sourceMeasure, Measures)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductMeasures (database name transaction_item_product_measure_valid) */
    lazy val productMeasuresFk = foreignKey("transaction_item_product_measure_valid", (product, measure), ProductMeasures)(r => (Rep.Some(r.product), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing ProductSources (database name transaction_deltas_product_source_fkey) */
    lazy val productSourcesFk = foreignKey("transaction_deltas_product_source_fkey", productSource, ProductSources)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing Products (database name transaction_deltas_product_fkey) */
    lazy val productsFk7 = foreignKey("transaction_deltas_product_fkey", product, Products)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Products (database name transaction_item_product_valid) */
    lazy val productsFk8 = foreignKey("transaction_item_product_valid", (product, Rep.Some(owner)), Products)(r => (Rep.Some(r.id), r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing TransactionDeltas (database name transaction_deltas_caused_by_fkey) */
    lazy val transactionDeltasFk = foreignKey("transaction_deltas_caused_by_fkey", causedBy, TransactionDeltas)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Transactions (database name transaction_deltas_xact_fkey) */
    lazy val transactionsFk10 = foreignKey("transaction_deltas_xact_fkey", xact, Transactions)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Transactions (database name transaction_item_owner_valid) */
    lazy val transactionsFk11 = foreignKey("transaction_item_owner_valid", (xact, owner), Transactions)(r => (r.id, r.owner), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name transaction_deltas_pricing_unit_fkey) */
    lazy val unitsFk12 = foreignKey("transaction_deltas_pricing_unit_fkey", pricingUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name transaction_deltas_source_unit_fkey) */
    lazy val unitsFk13 = foreignKey("transaction_deltas_source_unit_fkey", sourceUnit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name transaction_deltas_unit_fkey) */
    lazy val unitsFk14 = foreignKey("transaction_deltas_unit_fkey", unit, Units)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Units (database name transaction_item_unit_valid) */
    lazy val unitsFk15 = foreignKey("transaction_item_unit_valid", (unit, measure), Units)(r => (Rep.Some(r.id), Rep.Some(r.measure)), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table TransactionDeltas */
  lazy val TransactionDeltas = new TableQuery(tag => new TransactionDeltas(tag))


  /** GetResult implicit for fetching TransactionMeasuresRow objects using plain SQL queries */
  implicit def GetResultTransactionMeasuresRow(implicit e0: GR[Int], e1: GR[Double], e2: GR[Option[String]]): GR[TransactionMeasuresRow] = GR{
    prs => import prs._
    TransactionMeasuresRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Double], <<?[String], <<[Double]))
  }
  /** Table description of table transaction_measures. Objects of this class serve as prototypes for rows in queries. */
  class TransactionMeasures(_tableTag: Tag) extends profile.api.Table[TransactionMeasuresRow](_tableTag, "transaction_measures") {
    def * = (transactionDelta, measure, preferredUnit, conversionMeasure, conversionUnit, conversion, customName, amount) <> (TransactionMeasuresRow.tupled, TransactionMeasuresRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(transactionDelta), Rep.Some(measure), Rep.Some(preferredUnit), Rep.Some(conversionMeasure), Rep.Some(conversionUnit), Rep.Some(conversion), customName, Rep.Some(amount)).shaped.<>({r=>import r._; _1.map(_=> TransactionMeasuresRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7, _8.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column transaction_delta SqlType(int4) */
    val transactionDelta: Rep[Int] = column[Int]("transaction_delta")
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column preferred_unit SqlType(int4) */
    val preferredUnit: Rep[Int] = column[Int]("preferred_unit")
    /** Database column conversion_measure SqlType(int4) */
    val conversionMeasure: Rep[Int] = column[Int]("conversion_measure")
    /** Database column conversion_unit SqlType(int4) */
    val conversionUnit: Rep[Int] = column[Int]("conversion_unit")
    /** Database column conversion SqlType(float8) */
    val conversion: Rep[Double] = column[Double]("conversion")
    /** Database column custom_name SqlType(text), Default(None) */
    val customName: Rep[Option[String]] = column[Option[String]]("custom_name", O.Default(None))
    /** Database column amount SqlType(float8), Default(1.0) */
    val amount: Rep[Double] = column[Double]("amount", O.Default(1.0))

    /** Foreign key referencing TransactionDeltas (database name transaction_measures_transaction_delta_fkey) */
    lazy val transactionDeltasFk = foreignKey("transaction_measures_transaction_delta_fkey", transactionDelta, TransactionDeltas)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table TransactionMeasures */
  lazy val TransactionMeasures = new TableQuery(tag => new TransactionMeasures(tag))


  /** GetResult implicit for fetching TransactionsRow objects using plain SQL queries */
  implicit def GetResultTransactionsRow(implicit e0: GR[Int], e1: GR[Instant], e2: GR[Boolean], e3: GR[Option[Int]], e4: GR[TransactionType.Value]): GR[TransactionsRow] = GR{
    prs => import prs._
    TransactionsRow.tupled((<<[Int], <<[Int], <<[Instant], <<[Boolean], <<?[Int], <<[TransactionType.Value], <<[Boolean]))
  }
  /** Table description of table transactions. Objects of this class serve as prototypes for rows in queries. */
  class Transactions(_tableTag: Tag) extends profile.api.Table[TransactionsRow](_tableTag, "transactions") {
    def * = (id, owner, time, isFinalized, finalizedxact, xactType, includeInCalculatedInventory) <> (TransactionsRow.tupled, TransactionsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), Rep.Some(time), Rep.Some(isFinalized), finalizedxact, Rep.Some(xactType), Rep.Some(includeInCalculatedInventory)).shaped.<>({r=>import r._; _1.map(_=> TransactionsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column time SqlType(timestamp) */
    val time: Rep[Instant] = column[Instant]("time")
    /** Database column is_finalized SqlType(bool), Default(true) */
    val isFinalized: Rep[Boolean] = column[Boolean]("is_finalized", O.Default(true))
    /** Database column finalizedxact SqlType(int4), Default(None) */
    val finalizedxact: Rep[Option[Int]] = column[Option[Int]]("finalizedxact", O.Default(None))
    /** Database column xact_type SqlType(transaction_type) */
    val xactType: Rep[TransactionType.Value] = column[TransactionType.Value]("xact_type")
    /** Database column include_in_calculated_inventory SqlType(bool), Default(true) */
    val includeInCalculatedInventory: Rep[Boolean] = column[Boolean]("include_in_calculated_inventory", O.Default(true))

    /** Foreign key referencing Accounts (database name transactions_owner_fkey) */
    lazy val accountsFk = foreignKey("transactions_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Transactions (database name transactions_finalizedxact_fkey) */
    lazy val transactionsFk = foreignKey("transactions_finalizedxact_fkey", finalizedxact, Transactions)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Index over (owner,time) (database name transaction_timeseries_fetch_idx) */
    val index1 = index("transaction_timeseries_fetch_idx", (owner, time))
    /** Uniqueness Index over (id,owner) (database name transactions_id_owner_key) */
    val index2 = index("transactions_id_owner_key", (id, owner), unique=true)
  }
  /** Collection-like TableQuery object for table Transactions */
  lazy val Transactions = new TableQuery(tag => new Transactions(tag))


  /** GetResult implicit for fetching UnitsRow objects using plain SQL queries */
  implicit def GetResultUnitsRow(implicit e0: GR[Int], e1: GR[Double], e2: GR[String], e3: GR[Option[String]], e4: GR[Boolean], e5: GR[Option[Double]]): GR[UnitsRow] = GR{
    prs => import prs._
    UnitsRow.tupled((<<[Int], <<[Int], <<[Double], <<[String], <<?[String], <<[Boolean], <<?[Double], <<?[Double], <<[Int], <<[Double], <<[Boolean]))
  }
  /** Table description of table units. Objects of this class serve as prototypes for rows in queries. */
  class Units(_tableTag: Tag) extends profile.api.Table[UnitsRow](_tableTag, "units") {
    def * = (id, measure, size, name, abbr, metric, scaleUpLimit, scaleDownLimit, precision, quantum, commonPurchaseUnit) <> (UnitsRow.tupled, UnitsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(measure), Rep.Some(size), Rep.Some(name), abbr, Rep.Some(metric), scaleUpLimit, scaleDownLimit, Rep.Some(precision), Rep.Some(quantum), Rep.Some(commonPurchaseUnit)).shaped.<>({r=>import r._; _1.map(_=> UnitsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7, _8, _9.get, _10.get, _11.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column measure SqlType(int4) */
    val measure: Rep[Int] = column[Int]("measure")
    /** Database column size SqlType(float8) */
    val size: Rep[Double] = column[Double]("size")
    /** Database column name SqlType(text) */
    val name: Rep[String] = column[String]("name")
    /** Database column abbr SqlType(text), Default(None) */
    val abbr: Rep[Option[String]] = column[Option[String]]("abbr", O.Default(None))
    /** Database column metric SqlType(bool), Default(true) */
    val metric: Rep[Boolean] = column[Boolean]("metric", O.Default(true))
    /** Database column scale_up_limit SqlType(float8), Default(None) */
    val scaleUpLimit: Rep[Option[Double]] = column[Option[Double]]("scale_up_limit", O.Default(None))
    /** Database column scale_down_limit SqlType(float8), Default(None) */
    val scaleDownLimit: Rep[Option[Double]] = column[Option[Double]]("scale_down_limit", O.Default(None))
    /** Database column precision SqlType(int4), Default(2) */
    val precision: Rep[Int] = column[Int]("precision", O.Default(2))
    /** Database column quantum SqlType(float8), Default(0.0) */
    val quantum: Rep[Double] = column[Double]("quantum", O.Default(0.0))
    /** Database column common_purchase_unit SqlType(bool), Default(false) */
    val commonPurchaseUnit: Rep[Boolean] = column[Boolean]("common_purchase_unit", O.Default(false))

    /** Foreign key referencing Measures (database name units_measure_fkey) */
    lazy val measuresFk = foreignKey("units_measure_fkey", measure, Measures)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (measure,size) (database name units_measure_size_key) */
    val index1 = index("units_measure_size_key", (measure, size), unique=true)
    /** Uniqueness Index over (id,measure) (database name units_unique_id_measure) */
    val index2 = index("units_unique_id_measure", (id, measure), unique=true)
  }
  /** Collection-like TableQuery object for table Units */
  lazy val Units = new TableQuery(tag => new Units(tag))


  /** GetResult implicit for fetching UpstreamInvitesRow objects using plain SQL queries */
  implicit def GetResultUpstreamInvitesRow(implicit e0: GR[Int], e1: GR[String]): GR[UpstreamInvitesRow] = GR{
    prs => import prs._
    UpstreamInvitesRow.tupled((<<[Int], <<[Int], <<[Int], <<[Int], <<[String]))
  }
  /** Table description of table upstream_invites. Objects of this class serve as prototypes for rows in queries. */
  class UpstreamInvites(_tableTag: Tag) extends profile.api.Table[UpstreamInvitesRow](_tableTag, "upstream_invites") {
    def * = (id, emailToken, invitee, inviter, inviteeEmail) <> (UpstreamInvitesRow.tupled, UpstreamInvitesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(emailToken), Rep.Some(invitee), Rep.Some(inviter), Rep.Some(inviteeEmail)).shaped.<>({r=>import r._; _1.map(_=> UpstreamInvitesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column email_token SqlType(int4) */
    val emailToken: Rep[Int] = column[Int]("email_token")
    /** Database column invitee SqlType(int4) */
    val invitee: Rep[Int] = column[Int]("invitee")
    /** Database column inviter SqlType(int4) */
    val inviter: Rep[Int] = column[Int]("inviter")
    /** Database column invitee_email SqlType(text) */
    val inviteeEmail: Rep[String] = column[String]("invitee_email")

    /** Foreign key referencing Accounts (database name upstream_invites_invitee_fkey) */
    lazy val accountsFk1 = foreignKey("upstream_invites_invitee_fkey", invitee, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Accounts (database name upstream_invites_inviter_fkey) */
    lazy val accountsFk2 = foreignKey("upstream_invites_inviter_fkey", inviter, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing EmailTokens (database name upstream_invites_email_token_fkey) */
    lazy val emailTokensFk = foreignKey("upstream_invites_email_token_fkey", emailToken, EmailTokens)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table UpstreamInvites */
  lazy val UpstreamInvites = new TableQuery(tag => new UpstreamInvites(tag))


  /** GetResult implicit for fetching UserAccountsRow objects using plain SQL queries */
  implicit def GetResultUserAccountsRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[Instant], e3: GR[AccountPermissionLevel.Value]): GR[UserAccountsRow] = GR{
    prs => import prs._
    UserAccountsRow.tupled((<<[Int], <<[Int], <<[Int], <<[Boolean], <<[Instant], <<[AccountPermissionLevel.Value]))
  }
  /** Table description of table user_accounts. Objects of this class serve as prototypes for rows in queries. */
  class UserAccounts(_tableTag: Tag) extends profile.api.Table[UserAccountsRow](_tableTag, "user_accounts") {
    def * = (id, owner, userId, isActive, joinedAt, permissionLevel) <> (UserAccountsRow.tupled, UserAccountsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(owner), Rep.Some(userId), Rep.Some(isActive), Rep.Some(joinedAt), Rep.Some(permissionLevel)).shaped.<>({r=>import r._; _1.map(_=> UserAccountsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column is_active SqlType(bool) */
    val isActive: Rep[Boolean] = column[Boolean]("is_active")
    /** Database column joined_at SqlType(timestamp) */
    val joinedAt: Rep[Instant] = column[Instant]("joined_at")
    /** Database column permission_level SqlType(account_permission_level) */
    val permissionLevel: Rep[AccountPermissionLevel.Value] = column[AccountPermissionLevel.Value]("permission_level")

    /** Foreign key referencing Accounts (database name user_shared_accounts_owner_fkey) */
    lazy val accountsFk = foreignKey("user_shared_accounts_owner_fkey", owner, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name user_accounts_user_fkey) */
    lazy val usersFk = foreignKey("user_accounts_user_fkey", userId, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (owner,userId) (database name user_shared_accounts_owner_shared_account_key) */
    val index1 = index("user_shared_accounts_owner_shared_account_key", (owner, userId), unique=true)
  }
  /** Collection-like TableQuery object for table UserAccounts */
  lazy val UserAccounts = new TableQuery(tag => new UserAccounts(tag))


  /** GetResult implicit for fetching UserLoginsRow objects using plain SQL queries */
  implicit def GetResultUserLoginsRow(implicit e0: GR[String], e1: GR[Int], e2: GR[Option[String]]): GR[UserLoginsRow] = GR{
    prs => import prs._
    UserLoginsRow.tupled((<<[String], <<[String], <<[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String]))
  }
  /** Table description of table user_logins. Objects of this class serve as prototypes for rows in queries. */
  class UserLogins(_tableTag: Tag) extends profile.api.Table[UserLoginsRow](_tableTag, "user_logins") {
    def * = (providerId, providerKey, userId, firstName, lastName, fullName, email, oldUsername) <> (UserLoginsRow.tupled, UserLoginsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(providerId), Rep.Some(providerKey), Rep.Some(userId), firstName, lastName, fullName, email, oldUsername).shaped.<>({r=>import r._; _1.map(_=> UserLoginsRow.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column provider_id SqlType(text) */
    val providerId: Rep[String] = column[String]("provider_id")
    /** Database column provider_key SqlType(text) */
    val providerKey: Rep[String] = column[String]("provider_key")
    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column first_name SqlType(text), Default(None) */
    val firstName: Rep[Option[String]] = column[Option[String]]("first_name", O.Default(None))
    /** Database column last_name SqlType(text), Default(None) */
    val lastName: Rep[Option[String]] = column[Option[String]]("last_name", O.Default(None))
    /** Database column full_name SqlType(text), Default(None) */
    val fullName: Rep[Option[String]] = column[Option[String]]("full_name", O.Default(None))
    /** Database column email SqlType(text), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Default(None))
    /** Database column old_username SqlType(text), Default(None) */
    val oldUsername: Rep[Option[String]] = column[Option[String]]("old_username", O.Default(None))

    /** Primary key of UserLogins (database name user_logins_pkey) */
    val pk = primaryKey("user_logins_pkey", (providerId, providerKey))

    /** Foreign key referencing Users (database name user_logins_user_id_fkey) */
    lazy val usersFk = foreignKey("user_logins_user_id_fkey", userId, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table UserLogins */
  lazy val UserLogins = new TableQuery(tag => new UserLogins(tag))


  /** GetResult implicit for fetching UserOnboardingBoxesRow objects using plain SQL queries */
  implicit def GetResultUserOnboardingBoxesRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Short]): GR[UserOnboardingBoxesRow] = GR{
    prs => import prs._
    UserOnboardingBoxesRow.tupled((<<[Int], <<[String], <<[Short]))
  }
  /** Table description of table user_onboarding_boxes. Objects of this class serve as prototypes for rows in queries. */
  class UserOnboardingBoxes(_tableTag: Tag) extends profile.api.Table[UserOnboardingBoxesRow](_tableTag, "user_onboarding_boxes") {
    def * = (userId, page, nextStep) <> (UserOnboardingBoxesRow.tupled, UserOnboardingBoxesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(page), Rep.Some(nextStep)).shaped.<>({r=>import r._; _1.map(_=> UserOnboardingBoxesRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column page SqlType(bpchar), Length(120,false) */
    val page: Rep[String] = column[String]("page", O.Length(120,varying=false))
    /** Database column next_step SqlType(int2) */
    val nextStep: Rep[Short] = column[Short]("next_step")

    /** Foreign key referencing OnboardingPages (database name user_onboarding_boxes_page_fkey) */
    lazy val onboardingPagesFk = foreignKey("user_onboarding_boxes_page_fkey", page, OnboardingPages)(r => r.key, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name user_onboarding_boxes_user_id_fkey) */
    lazy val usersFk = foreignKey("user_onboarding_boxes_user_id_fkey", userId, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (userId,page) (database name unique_user_last_visited_step_per_page) */
    val index1 = index("unique_user_last_visited_step_per_page", (userId, page), unique=true)
  }
  /** Collection-like TableQuery object for table UserOnboardingBoxes */
  lazy val UserOnboardingBoxes = new TableQuery(tag => new UserOnboardingBoxes(tag))


  /** GetResult implicit for fetching UserOnboardingWidgetsRow objects using plain SQL queries */
  implicit def GetResultUserOnboardingWidgetsRow(implicit e0: GR[Int]): GR[UserOnboardingWidgetsRow] = GR{
    prs => import prs._
    UserOnboardingWidgetsRow.tupled((<<[Int], <<[Int]))
  }
  /** Table description of table user_onboarding_widgets. Objects of this class serve as prototypes for rows in queries. */
  class UserOnboardingWidgets(_tableTag: Tag) extends profile.api.Table[UserOnboardingWidgetsRow](_tableTag, "user_onboarding_widgets") {
    def * = (userId, widget) <> (UserOnboardingWidgetsRow.tupled, UserOnboardingWidgetsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(widget)).shaped.<>({r=>import r._; _1.map(_=> UserOnboardingWidgetsRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(int4) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column widget SqlType(int4) */
    val widget: Rep[Int] = column[Int]("widget")

    /** Foreign key referencing OnboardingWidgets (database name user_onboarding_widgets_widget_fkey) */
    lazy val onboardingWidgetsFk = foreignKey("user_onboarding_widgets_widget_fkey", widget, OnboardingWidgets)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name user_onboarding_widgets_user_id_fkey) */
    lazy val usersFk = foreignKey("user_onboarding_widgets_user_id_fkey", userId, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (userId,widget) (database name user_onboarding_widgets_user_id_widget_key) */
    val index1 = index("user_onboarding_widgets_user_id_widget_key", (userId, widget), unique=true)
  }
  /** Collection-like TableQuery object for table UserOnboardingWidgets */
  lazy val UserOnboardingWidgets = new TableQuery(tag => new UserOnboardingWidgets(tag))


  /** GetResult implicit for fetching UserPrintPreferencesRow objects using plain SQL queries */
  implicit def GetResultUserPrintPreferencesRow(implicit e0: GR[Int], e1: GR[Boolean], e2: GR[RecipePrintFormatType.Value]): GR[UserPrintPreferencesRow] = GR{
    prs => import prs._
    UserPrintPreferencesRow.tupled((<<[Int], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean], <<[RecipePrintFormatType.Value], <<[Boolean], <<[Boolean], <<[Boolean]))
  }
  /** Table description of table user_print_preferences. Objects of this class serve as prototypes for rows in queries. */
  class UserPrintPreferences(_tableTag: Tag) extends profile.api.Table[UserPrintPreferencesRow](_tableTag, "user_print_preferences") {
    def * = (userId, orderIncludePreps, orderIncludeRecipes, orderIncludeSubRecipes, printSubRecipePreferences, breakPageOnRecipePrint, recipePrintFormatPreference, includePhotoInRecipePrint, includeNutritionInMultipleRecipePrint, includeCostBreakdownInMultipleRecipePrint) <> (UserPrintPreferencesRow.tupled, UserPrintPreferencesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(orderIncludePreps), Rep.Some(orderIncludeRecipes), Rep.Some(orderIncludeSubRecipes), Rep.Some(printSubRecipePreferences), Rep.Some(breakPageOnRecipePrint), Rep.Some(recipePrintFormatPreference), Rep.Some(includePhotoInRecipePrint), Rep.Some(includeNutritionInMultipleRecipePrint), Rep.Some(includeCostBreakdownInMultipleRecipePrint)).shaped.<>({r=>import r._; _1.map(_=> UserPrintPreferencesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9.get, _10.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(int4), PrimaryKey */
    val userId: Rep[Int] = column[Int]("user_id", O.PrimaryKey)
    /** Database column order_include_preps SqlType(bool), Default(false) */
    val orderIncludePreps: Rep[Boolean] = column[Boolean]("order_include_preps", O.Default(false))
    /** Database column order_include_recipes SqlType(bool), Default(false) */
    val orderIncludeRecipes: Rep[Boolean] = column[Boolean]("order_include_recipes", O.Default(false))
    /** Database column order_include_sub_recipes SqlType(bool), Default(false) */
    val orderIncludeSubRecipes: Rep[Boolean] = column[Boolean]("order_include_sub_recipes", O.Default(false))
    /** Database column print_sub_recipe_preferences SqlType(bool), Default(false) */
    val printSubRecipePreferences: Rep[Boolean] = column[Boolean]("print_sub_recipe_preferences", O.Default(false))
    /** Database column break_page_on_recipe_print SqlType(bool), Default(false) */
    val breakPageOnRecipePrint: Rep[Boolean] = column[Boolean]("break_page_on_recipe_print", O.Default(false))
    /** Database column recipe_print_format_preference SqlType(recipe_print_format_type) */
    val recipePrintFormatPreference: Rep[RecipePrintFormatType.Value] = column[RecipePrintFormatType.Value]("recipe_print_format_preference")
    /** Database column include_photo_in_recipe_print SqlType(bool), Default(false) */
    val includePhotoInRecipePrint: Rep[Boolean] = column[Boolean]("include_photo_in_recipe_print", O.Default(false))
    /** Database column include_nutrition_in_multiple_recipe_print SqlType(bool), Default(false) */
    val includeNutritionInMultipleRecipePrint: Rep[Boolean] = column[Boolean]("include_nutrition_in_multiple_recipe_print", O.Default(false))
    /** Database column include_cost_breakdown_in_multiple_recipe_print SqlType(bool), Default(false) */
    val includeCostBreakdownInMultipleRecipePrint: Rep[Boolean] = column[Boolean]("include_cost_breakdown_in_multiple_recipe_print", O.Default(false))

    /** Foreign key referencing Users (database name user_print_preferences_user_id_fkey) */
    lazy val usersFk = foreignKey("user_print_preferences_user_id_fkey", userId, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table UserPrintPreferences */
  lazy val UserPrintPreferences = new TableQuery(tag => new UserPrintPreferences(tag))


  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[Int], e1: GR[MeasureSystemsType.Value], e2: GR[Boolean], e3: GR[scala.math.BigDecimal], e4: GR[Option[String]], e5: GR[SubtractInventoryType.Value]): GR[UsersRow] = GR{
    prs => import prs._
    UsersRow.tupled((<<[Int], <<[MeasureSystemsType.Value], <<[MeasureSystemsType.Value], <<[MeasureSystemsType.Value], <<[MeasureSystemsType.Value], <<[Boolean], <<[scala.math.BigDecimal], <<[Int], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String], <<[SubtractInventoryType.Value], <<?[String]))
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends profile.api.Table[UsersRow](_tableTag, "users") {
    def * = (id, weightSystem, volumeSystem, purchaseWeightSystem, purchaseVolumeSystem, hideAllRecipes, sourcePriceChangeWarningFraction, newRecipeThresholdDays, email, hasher, hashedPassword, salt, firstName, lastName, fullName, subtractInventoryPreference, phoneNumber) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(weightSystem), Rep.Some(volumeSystem), Rep.Some(purchaseWeightSystem), Rep.Some(purchaseVolumeSystem), Rep.Some(hideAllRecipes), Rep.Some(sourcePriceChangeWarningFraction), Rep.Some(newRecipeThresholdDays), email, hasher, hashedPassword, salt, firstName, lastName, fullName, Rep.Some(subtractInventoryPreference), phoneNumber).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9, _10, _11, _12, _13, _14, _15, _16.get, _17)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column weight_system SqlType(measure_systems_type) */
    val weightSystem: Rep[MeasureSystemsType.Value] = column[MeasureSystemsType.Value]("weight_system")
    /** Database column volume_system SqlType(measure_systems_type) */
    val volumeSystem: Rep[MeasureSystemsType.Value] = column[MeasureSystemsType.Value]("volume_system")
    /** Database column purchase_weight_system SqlType(measure_systems_type) */
    val purchaseWeightSystem: Rep[MeasureSystemsType.Value] = column[MeasureSystemsType.Value]("purchase_weight_system")
    /** Database column purchase_volume_system SqlType(measure_systems_type) */
    val purchaseVolumeSystem: Rep[MeasureSystemsType.Value] = column[MeasureSystemsType.Value]("purchase_volume_system")
    /** Database column hide_all_recipes SqlType(bool), Default(false) */
    val hideAllRecipes: Rep[Boolean] = column[Boolean]("hide_all_recipes", O.Default(false))
    /** Database column source_price_change_warning_fraction SqlType(numeric), Default(0.2) */
    val sourcePriceChangeWarningFraction: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("source_price_change_warning_fraction", O.Default(scala.math.BigDecimal("0.2")))
    /** Database column new_recipe_threshold_days SqlType(int4), Default(14) */
    val newRecipeThresholdDays: Rep[Int] = column[Int]("new_recipe_threshold_days", O.Default(14))
    /** Database column email SqlType(text), Default(None) */
    val email: Rep[Option[String]] = column[Option[String]]("email", O.Default(None))
    /** Database column hasher SqlType(text), Default(None) */
    val hasher: Rep[Option[String]] = column[Option[String]]("hasher", O.Default(None))
    /** Database column hashed_password SqlType(text), Default(None) */
    val hashedPassword: Rep[Option[String]] = column[Option[String]]("hashed_password", O.Default(None))
    /** Database column salt SqlType(text), Default(None) */
    val salt: Rep[Option[String]] = column[Option[String]]("salt", O.Default(None))
    /** Database column first_name SqlType(text), Default(None) */
    val firstName: Rep[Option[String]] = column[Option[String]]("first_name", O.Default(None))
    /** Database column last_name SqlType(text), Default(None) */
    val lastName: Rep[Option[String]] = column[Option[String]]("last_name", O.Default(None))
    /** Database column full_name SqlType(text), Default(None) */
    val fullName: Rep[Option[String]] = column[Option[String]]("full_name", O.Default(None))
    /** Database column subtract_inventory_preference SqlType(subtract_inventory_type) */
    val subtractInventoryPreference: Rep[SubtractInventoryType.Value] = column[SubtractInventoryType.Value]("subtract_inventory_preference")
    /** Database column phone_number SqlType(text), Default(None) */
    val phoneNumber: Rep[Option[String]] = column[Option[String]]("phone_number", O.Default(None))

    /** Uniqueness Index over (email) (database name users_email_key) */
    val index1 = index("users_email_key", email, unique=true)
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))


  /** GetResult implicit for fetching UserSharedInvitesRow objects using plain SQL queries */
  implicit def GetResultUserSharedInvitesRow(implicit e0: GR[Int], e1: GR[String], e2: GR[AccountPermissionLevel.Value]): GR[UserSharedInvitesRow] = GR{
    prs => import prs._
    UserSharedInvitesRow.tupled((<<[Int], <<[Int], <<[String], <<[Int], <<[AccountPermissionLevel.Value]))
  }
  /** Table description of table user_shared_invites. Objects of this class serve as prototypes for rows in queries. */
  class UserSharedInvites(_tableTag: Tag) extends profile.api.Table[UserSharedInvitesRow](_tableTag, "user_shared_invites") {
    def * = (id, emailToken, inviteeEmail, inviter, inviteLevel) <> (UserSharedInvitesRow.tupled, UserSharedInvitesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(emailToken), Rep.Some(inviteeEmail), Rep.Some(inviter), Rep.Some(inviteLevel)).shaped.<>({r=>import r._; _1.map(_=> UserSharedInvitesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column email_token SqlType(int4) */
    val emailToken: Rep[Int] = column[Int]("email_token")
    /** Database column invitee_email SqlType(text) */
    val inviteeEmail: Rep[String] = column[String]("invitee_email")
    /** Database column inviter SqlType(int4) */
    val inviter: Rep[Int] = column[Int]("inviter")
    /** Database column invite_level SqlType(account_permission_level) */
    val inviteLevel: Rep[AccountPermissionLevel.Value] = column[AccountPermissionLevel.Value]("invite_level")

    /** Foreign key referencing Accounts (database name user_shared_invites_inviter_fkey) */
    lazy val accountsFk = foreignKey("user_shared_invites_inviter_fkey", inviter, Accounts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing EmailTokens (database name user_shared_invites_email_token_fkey) */
    lazy val emailTokensFk = foreignKey("user_shared_invites_email_token_fkey", emailToken, EmailTokens)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table UserSharedInvites */
  lazy val UserSharedInvites = new TableQuery(tag => new UserSharedInvites(tag))
}
/** Entity class storing rows of table Accounts
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param isAdmin Database column is_admin SqlType(bool), Default(false)
   *  @param isStdlib Database column is_stdlib SqlType(bool), Default(false)
   *  @param stripeEmail Database column stripe_email SqlType(text), Default(None)
   *  @param stripeCustomerId Database column stripe_customer_id SqlType(text), Default(None)
   *  @param acceptsTerms Database column accepts_terms SqlType(bool), Default(false)
   *  @param promoCode Database column promo_code SqlType(text), Default(None)
   *  @param subscriptionLevel Database column subscription_level SqlType(text), Default(None)
   *  @param createdAt Database column created_at SqlType(timestamp)
   *  @param lastLogin Database column last_login SqlType(timestamp), Default(None)
   *  @param lastUsage Database column last_usage SqlType(timestamp), Default(None)
   *  @param multiLocationUpstream Database column multi_location_upstream SqlType(int4), Default(None)
   *  @param upstreamRelativeName Database column upstream_relative_name SqlType(text), Default(None)
   *  @param addons Database column addons SqlType(_text), Length(2147483647,false)
   *  @param shippingAddress Database column shipping_address SqlType(text), Default(None)
   *  @param ccEmails Database column cc_emails SqlType(_text), Length(2147483647,false)
   *  @param businessName Database column business_name SqlType(text), Default(None)
   *  @param companyLogo Database column company_logo SqlType(text), Default(None)
   *  @param accountOwner Database column account_owner SqlType(int4), Default(None)
   *  @param fakeAccount Database column fake_account SqlType(bool), Default(false) */
  case class AccountsRow(id: Int, isAdmin: Boolean = false, isStdlib: Boolean = false, stripeEmail: Option[String] = None, stripeCustomerId: Option[String] = None, acceptsTerms: Boolean = false, promoCode: Option[String] = None, subscriptionLevel: Option[String] = None, createdAt: Instant, lastLogin: Option[Instant] = None, lastUsage: Option[Instant] = None, multiLocationUpstream: Option[Int] = None, upstreamRelativeName: Option[String] = None, addons: List[String], shippingAddress: Option[String] = None, ccEmails: List[String], businessName: Option[String] = None, companyLogo: Option[String] = None, accountOwner: Option[Int] = None, fakeAccount: Boolean = false)

  /** Entity class storing rows of table Categories
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4), Default(None)
   *  @param name Database column name SqlType(text)
   *  @param syncUpstream Database column sync_upstream SqlType(int4), Default(None)
   *  @param categoryType Database column category_type SqlType(product_category_type) */
  case class CategoriesRow(id: Int, owner: Option[Int] = None, name: String, syncUpstream: Option[Int] = None, categoryType: ProductCategoryType.Value)

  /** Entity class storing rows of table EmailTokens
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param secret Database column secret SqlType(text)
   *  @param used Database column used SqlType(bool), Default(false)
   *  @param expiration Database column expiration SqlType(timestamp)
   *  @param owner Database column owner SqlType(int4)
   *  @param tokenType Database column token_type SqlType(email_token_type) */
  case class EmailTokensRow(id: Int, secret: String, used: Boolean = false, expiration: Instant, owner: Int, tokenType: EmailTokenType.Value)

  /** Entity class storing rows of table Flags
   *  @param userId Database column user_id SqlType(int4), PrimaryKey
   *  @param subscriptionChosen Database column subscription_chosen SqlType(bool), Default(false) */
  case class FlagsRow(userId: Int, subscriptionChosen: Boolean = false)

  /** Entity class storing rows of table IngredientPars
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param product Database column product SqlType(int4)
   *  @param amount Database column amount SqlType(float8), Default(None)
   *  @param measure Database column measure SqlType(int4), Default(None)
   *  @param unit Database column unit SqlType(int4), Default(None)
   *  @param packaged Database column packaged SqlType(bool), Default(false)
   *  @param packageName Database column package_name SqlType(text), Default(None)
   *  @param subPackageName Database column sub_package_name SqlType(text), Default(None)
   *  @param packageSize Database column package_size SqlType(float8)
   *  @param superPackageSize Database column super_package_size SqlType(float8), Default(1.0)
   *  @param supplierName Database column supplier_name SqlType(text), Default(None)
   *  @param supplier Database column supplier SqlType(int4), Default(None) */
  case class IngredientParsRow(id: Int, owner: Int, product: Int, amount: Option[Double] = None, measure: Option[Int] = None, unit: Option[Int] = None, packaged: Boolean = false, packageName: Option[String] = None, subPackageName: Option[String] = None, packageSize: Double, superPackageSize: Double = 1.0, supplierName: Option[String] = None, supplier: Option[Int] = None)

  /** Entity class storing rows of table IngredientPriceHistory
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param product Database column product SqlType(int4)
   *  @param measure Database column measure SqlType(int4)
   *  @param unit Database column unit SqlType(int4)
   *  @param cost Database column cost SqlType(numeric), Default(None)
   *  @param packaged Database column packaged SqlType(bool), Default(false)
   *  @param packageSize Database column package_size SqlType(float4)
   *  @param superPackageSize Database column super_package_size SqlType(float4), Default(1.0)
   *  @param pricePer Database column price_per SqlType(price_per_type)
   *  @param pricingMeasure Database column pricing_measure SqlType(int4), Default(None)
   *  @param pricingUnit Database column pricing_unit SqlType(int4), Default(None)
   *  @param changeType Database column change_type SqlType(change_type)
   *  @param createAt Database column create_at SqlType(timestamp) */
  case class IngredientPriceHistoryRow(id: Int, product: Int, measure: Int, unit: Int, cost: Option[scala.math.BigDecimal] = None, packaged: Boolean = false, packageSize: Float, superPackageSize: Float = 1.0F, pricePer: PricePerType.Value, pricingMeasure: Option[Int] = None, pricingUnit: Option[Int] = None, changeType: ChangeType.Value, createAt: Instant)

  /** Entity class storing rows of table IngredientPriceHistoryMeasures
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param ingredientPrice Database column ingredient_price SqlType(int4)
   *  @param measure Database column measure SqlType(int4)
   *  @param preferredUnit Database column preferred_unit SqlType(int4)
   *  @param conversionMeasure Database column conversion_measure SqlType(int4)
   *  @param conversionUnit Database column conversion_unit SqlType(int4)
   *  @param conversion Database column conversion SqlType(float8)
   *  @param amount Database column amount SqlType(float8) */
  case class IngredientPriceHistoryMeasuresRow(id: Int, ingredientPrice: Int, measure: Int, preferredUnit: Int, conversionMeasure: Int, conversionUnit: Int, conversion: Double, amount: Double)

  /** Entity class storing rows of table Inventories
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param time Database column time SqlType(timestamp)
   *  @param name Database column name SqlType(text), Default(None)
   *  @param date Database column date SqlType(date)
   *  @param mergedPreviousInventory Database column merged_previous_inventory SqlType(bool), Default(false) */
  case class InventoriesRow(id: Int, owner: Int, time: Instant, name: Option[String] = None, date: LocalDate, mergedPreviousInventory: Boolean = false)

  /** Entity class storing rows of table InventoryItems
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param inventory Database column inventory SqlType(int4)
   *  @param owner Database column owner SqlType(int4)
   *  @param product Database column product SqlType(int4)
   *  @param amount Database column amount SqlType(float8), Default(None)
   *  @param measure Database column measure SqlType(int4), Default(None)
   *  @param unit Database column unit SqlType(int4), Default(None)
   *  @param cost Database column cost SqlType(float8), Default(None)
   *  @param packaged Database column packaged SqlType(bool), Default(false)
   *  @param packageName Database column package_name SqlType(text), Default(None)
   *  @param subPackageName Database column sub_package_name SqlType(text), Default(None)
   *  @param packageSize Database column package_size SqlType(float8)
   *  @param superPackageSize Database column super_package_size SqlType(float8), Default(1.0)
   *  @param supplierName Database column supplier_name SqlType(text), Default(None)
   *  @param supplier Database column supplier SqlType(int4), Default(None) */
  case class InventoryItemsRow(id: Int, inventory: Int, owner: Int, product: Int, amount: Option[Double] = None, measure: Option[Int] = None, unit: Option[Int] = None, cost: Option[Double] = None, packaged: Boolean = false, packageName: Option[String] = None, subPackageName: Option[String] = None, packageSize: Double, superPackageSize: Double = 1.0, supplierName: Option[String] = None, supplier: Option[Int] = None)

  /** Entity class storing rows of table LocationsSyncedRecipes
   *  @param owner Database column owner SqlType(int4)
   *  @param locationId Database column location_id SqlType(int4)
   *  @param deprecatedCategoryColumn Database column deprecated_category_column SqlType(text)
   *  @param recipeCategory Database column recipe_category SqlType(int4) */
  case class LocationsSyncedRecipesRow(owner: Int, locationId: Int, deprecatedCategoryColumn: String, recipeCategory: Int)

  /** Entity class storing rows of table LocationsSyncedSuppliers
   *  @param owner Database column owner SqlType(int4)
   *  @param locationId Database column location_id SqlType(int4)
   *  @param supplierId Database column supplier_id SqlType(int4) */
  case class LocationsSyncedSuppliersRow(owner: Int, locationId: Int, supplierId: Int)

  /** Entity class storing rows of table Measures
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(text)
   *  @param allowsCustomNames Database column allows_custom_names SqlType(bool), Default(false) */
  case class MeasuresRow(id: Int, name: String, allowsCustomNames: Boolean = false)

  /** Entity class storing rows of table MenuItems
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param section Database column section SqlType(int4)
   *  @param displayOrder Database column display_order SqlType(int4)
   *  @param product Database column product SqlType(int4)
   *  @param owner Database column owner SqlType(int4)
   *  @param menu Database column menu SqlType(int4), Default(0) */
  case class MenuItemsRow(id: Int, section: Int, displayOrder: Int, product: Int, owner: Int, menu: Int = 0)

  /** Entity class storing rows of table Menus
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param name Database column name SqlType(text)
   *  @param userVisible Database column user_visible SqlType(bool), Default(true) */
  case class MenusRow(id: Int, owner: Int, name: String, userVisible: Boolean = true)

  /** Entity class storing rows of table MenuSections
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param menu Database column menu SqlType(int4)
   *  @param displayOrder Database column display_order SqlType(int4)
   *  @param name Database column name SqlType(text), Default(None)
   *  @param owner Database column owner SqlType(int4) */
  case class MenuSectionsRow(id: Int, menu: Int, displayOrder: Int, name: Option[String] = None, owner: Int)

  /** Entity class storing rows of table Nutrients
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param nndbsrId Database column nndbsr_id SqlType(text)
   *  @param nndbsrName Database column nndbsr_name SqlType(text)
   *  @param parsleyName Database column parsley_name SqlType(text), Default(None)
   *  @param unit Database column unit SqlType(text)
   *  @param active Database column active SqlType(bool), Default(false)
   *  @param dailyValue Database column daily_value SqlType(numeric), Default(None) */
  case class NutrientsRow(id: Int, nndbsrId: String, nndbsrName: String, parsleyName: Option[String] = None, unit: String, active: Boolean = false, dailyValue: Option[scala.math.BigDecimal] = None)

  /** Entity class storing rows of table OnboardingPages
   *  @param key Database column key SqlType(bpchar), PrimaryKey, Length(120,false)
   *  @param name Database column name SqlType(text) */
  case class OnboardingPagesRow(key: String, name: String)

  /** Entity class storing rows of table OnboardingSteps
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param page Database column page SqlType(bpchar), Length(120,false)
   *  @param index Database column index SqlType(int2)
   *  @param placement Database column placement SqlType(onboarding_placement)
   *  @param targetSelector Database column target_selector SqlType(text), Default(None)
   *  @param title Database column title SqlType(text), Default(None)
   *  @param description Database column description SqlType(text)
   *  @param leftImage Database column left_image SqlType(text), Default(None)
   *  @param rightImage Database column right_image SqlType(text), Default(None)
   *  @param nextButtonText Database column next_button_text SqlType(text), Default(None)
   *  @param backButtonText Database column back_button_text SqlType(text), Default(None)
   *  @param bodyWidth Database column body_width SqlType(int4)
   *  @param titleBgColor Database column title_bg_color SqlType(text), Default(80cc28)
   *  @param descBgColor Database column desc_bg_color SqlType(text), Default(ffffff)
   *  @param borderColor Database column border_color SqlType(text), Default(e7e7e7)
   *  @param nextBgColor Database column next_bg_color SqlType(text), Default(Some(80cc28))
   *  @param closeBtnColor Database column close_btn_color SqlType(text), Default(ffffff)
   *  @param name Database column name SqlType(text) */
  case class OnboardingStepsRow(id: Int, page: String, index: Short, placement: OnboardingPlacement.Value, targetSelector: Option[String] = None, title: Option[String] = None, description: String, leftImage: Option[String] = None, rightImage: Option[String] = None, nextButtonText: Option[String] = None, backButtonText: Option[String] = None, bodyWidth: Int, titleBgColor: String = "80cc28", descBgColor: String = "ffffff", borderColor: String = "e7e7e7", nextBgColor: Option[String] = Some("80cc28"), closeBtnColor: String = "ffffff", name: String)

  /** Entity class storing rows of table OnboardingWidgets
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(text)
   *  @param defaultMessage Database column default_message SqlType(text)
   *  @param customMessage Database column custom_message SqlType(text), Default(None) */
  case class OnboardingWidgetsRow(id: Int, name: String, defaultMessage: String, customMessage: Option[String] = None)

  /** Entity class storing rows of table Orders
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param name Database column name SqlType(text)
   *  @param description Database column description SqlType(text), Default(None)
   *  @param menu Database column menu SqlType(int4)
   *  @param owner Database column owner SqlType(int4)
   *  @param xact Database column xact SqlType(int4)
   *  @param coversCount Database column covers_count SqlType(int4), Default(None) */
  case class OrdersRow(id: Int, name: String, description: Option[String] = None, menu: Int, owner: Int, xact: Int, coversCount: Option[Int] = None)

  /** Entity class storing rows of table PosImportPreferences
   *  @param owner Database column owner SqlType(int4), PrimaryKey
   *  @param itemCodeHeader Database column item_code_header SqlType(text)
   *  @param dateSoldHeader Database column date_sold_header SqlType(text)
   *  @param quantitySoldHeader Database column quantity_sold_header SqlType(text)
   *  @param averagePriceHeader Database column average_price_header SqlType(text), Default(None)
   *  @param nameHeader Database column name_header SqlType(text), Default(None) */
  case class PosImportPreferencesRow(owner: Int, itemCodeHeader: String, dateSoldHeader: String, quantitySoldHeader: String, averagePriceHeader: Option[String] = None, nameHeader: Option[String] = None)

  /** Entity class storing rows of table ProductAllergens
   *  @param product Database column product SqlType(int4), PrimaryKey
   *  @param milk Database column milk SqlType(bool), Default(false)
   *  @param eggs Database column eggs SqlType(bool), Default(false)
   *  @param fish Database column fish SqlType(text), Default(None)
   *  @param crustaceanShellfish Database column crustacean_shellfish SqlType(text), Default(None)
   *  @param treeNuts Database column tree_nuts SqlType(text), Default(None)
   *  @param wheat Database column wheat SqlType(bool), Default(false)
   *  @param peanuts Database column peanuts SqlType(bool), Default(false)
   *  @param soybeans Database column soybeans SqlType(bool), Default(false)
   *  @param molluscs Database column molluscs SqlType(bool), Default(false)
   *  @param cerealsGluten Database column cereals_gluten SqlType(bool), Default(false)
   *  @param celery Database column celery SqlType(bool), Default(false)
   *  @param mustard Database column mustard SqlType(bool), Default(false)
   *  @param sesameSeeds Database column sesame_seeds SqlType(bool), Default(false)
   *  @param sulphurDioxideSulphites Database column sulphur_dioxide_sulphites SqlType(bool), Default(false)
   *  @param lupin Database column lupin SqlType(bool), Default(false) */
  case class ProductAllergensRow(product: Int, milk: Boolean = false, eggs: Boolean = false, fish: Option[String] = None, crustaceanShellfish: Option[String] = None, treeNuts: Option[String] = None, wheat: Boolean = false, peanuts: Boolean = false, soybeans: Boolean = false, molluscs: Boolean = false, cerealsGluten: Boolean = false, celery: Boolean = false, mustard: Boolean = false, sesameSeeds: Boolean = false, sulphurDioxideSulphites: Boolean = false, lupin: Boolean = false)

  /** Entity class storing rows of table ProductCategories
   *  @param product Database column product SqlType(int4)
   *  @param category Database column category SqlType(int4) */
  case class ProductCategoriesRow(product: Int, category: Int)

  /** Entity class storing rows of table ProductCharacteristics
   *  @param product Database column product SqlType(int4), PrimaryKey
   *  @param addedSugar Database column added_sugar SqlType(bool), Default(false)
   *  @param meat Database column meat SqlType(bool), Default(false)
   *  @param pork Database column pork SqlType(bool), Default(false)
   *  @param corn Database column corn SqlType(bool), Default(false)
   *  @param poultry Database column poultry SqlType(bool), Default(false)
   *  @param nonEdible Database column non_edible SqlType(bool), Default(false)
   *  @param ingredients Database column ingredients SqlType(text), Default(None) */
  case class ProductCharacteristicsRow(product: Int, addedSugar: Boolean = false, meat: Boolean = false, pork: Boolean = false, corn: Boolean = false, poultry: Boolean = false, nonEdible: Boolean = false, ingredients: Option[String] = None)

  /** Entity class storing rows of table ProductInfos
   *  @param product Database column product SqlType(int4), PrimaryKey
   *  @param lowestPricePreferred Database column lowest_price_preferred SqlType(bool), Default(true) */
  case class ProductInfosRow(product: Int, lowestPricePreferred: Boolean = true)

  /** Entity class storing rows of table ProductMeasures
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param product Database column product SqlType(int4)
   *  @param measure Database column measure SqlType(int4)
   *  @param preferredUnit Database column preferred_unit SqlType(int4)
   *  @param conversionMeasure Database column conversion_measure SqlType(int4)
   *  @param conversionUnit Database column conversion_unit SqlType(int4)
   *  @param conversion Database column conversion SqlType(float8)
   *  @param customName Database column custom_name SqlType(text), Default(None)
   *  @param tombstone Database column tombstone SqlType(bool), Default(false)
   *  @param amount Database column amount SqlType(float8), Default(1.0) */
  case class ProductMeasuresRow(id: Int, product: Int, measure: Int, preferredUnit: Int, conversionMeasure: Int, conversionUnit: Int, conversion: Double, customName: Option[String] = None, tombstone: Boolean = false, amount: Double = 1.0)

  /** Entity class storing rows of table ProductNutrients
   *  @param product Database column product SqlType(int4)
   *  @param nutrient Database column nutrient SqlType(int4)
   *  @param amount Database column amount SqlType(numeric) */
  case class ProductNutrientsRow(product: Int, nutrient: Int, amount: scala.math.BigDecimal)

  /** Entity class storing rows of table Products
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4), Default(None)
   *  @param name Database column name SqlType(text)
   *  @param ingredient Database column ingredient SqlType(bool), Default(false)
   *  @param salable Database column salable SqlType(bool), Default(false)
   *  @param preferredSource Database column preferred_source SqlType(int4), Default(None)
   *  @param nndbsrId Database column nndbsr_id SqlType(bpchar), Length(5,false), Default(None)
   *  @param nndbsrLastImport Database column nndbsr_last_import SqlType(timestamp), Default(None)
   *  @param syncUpstream Database column sync_upstream SqlType(int4), Default(None)
   *  @param hidden Database column hidden SqlType(bool), Default(false)
   *  @param description Database column description SqlType(text), Default(None)
   *  @param nutrientServingAmount Database column nutrient_serving_amount SqlType(numeric), Default(None)
   *  @param nutrientServingMeasure Database column nutrient_serving_measure SqlType(int4), Default(None)
   *  @param nutrientServingUnit Database column nutrient_serving_unit SqlType(int4), Default(None)
   *  @param lastAddedToRecipe Database column last_added_to_recipe SqlType(timestamp), Default(None)
   *  @param lastRemovedFromInventory Database column last_removed_from_inventory SqlType(timestamp), Default(None)
   *  @param photo Database column photo SqlType(text), Default(None)
   *  @param tombstone Database column tombstone SqlType(bool), Default(false)
   *  @param syncUpstreamOwner Database column sync_upstream_owner SqlType(int4), Default(None)
   *  @param lastSynced Database column last_synced SqlType(timestamp), Default(None)
   *  @param lastModified Database column last_modified SqlType(timestamp)
   *  @param lastRemovedFromPars Database column last_removed_from_pars SqlType(timestamp), Default(None) */
  case class ProductsRow(id: Int, owner: Option[Int] = None, name: String, ingredient: Boolean = false, salable: Boolean = false, preferredSource: Option[Int] = None, nndbsrId: Option[String] = None, nndbsrLastImport: Option[Instant] = None, syncUpstream: Option[Int] = None, hidden: Boolean = false, description: Option[String] = None, nutrientServingAmount: Option[scala.math.BigDecimal] = None, nutrientServingMeasure: Option[Int] = None, nutrientServingUnit: Option[Int] = None, lastAddedToRecipe: Option[Instant] = None, lastRemovedFromInventory: Option[Instant] = None, photo: Option[String] = None, tombstone: Boolean = false, syncUpstreamOwner: Option[Int] = None, lastSynced: Option[Instant] = None, lastModified: Instant, lastRemovedFromPars: Option[Instant] = None)

  /** Entity class storing rows of table ProductSources
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param product Database column product SqlType(int4)
   *  @param supplier Database column supplier SqlType(int4), Default(None)
   *  @param measure Database column measure SqlType(int4)
   *  @param unit Database column unit SqlType(int4)
   *  @param cost Database column cost SqlType(numeric), Default(None)
   *  @param packaged Database column packaged SqlType(bool), Default(false)
   *  @param packageName Database column package_name SqlType(text), Default(None)
   *  @param packageSize Database column package_size SqlType(float8)
   *  @param sku Database column sku SqlType(text), Default(None)
   *  @param superPackageSize Database column super_package_size SqlType(int4), Default(1)
   *  @param subPackageName Database column sub_package_name SqlType(text), Default(None)
   *  @param pricePer Database column price_per SqlType(price_per_type)
   *  @param pricingMeasure Database column pricing_measure SqlType(int4), Default(None)
   *  @param pricingUnit Database column pricing_unit SqlType(int4), Default(None)
   *  @param supplierIngredientName Database column supplier_ingredient_name SqlType(text), Default(None)
   *  @param syncUpstream Database column sync_upstream SqlType(int4), Default(None)
   *  @param syncUpstreamOwner Database column sync_upstream_owner SqlType(int4), Default(None)
   *  @param manufacturerName Database column manufacturer_name SqlType(text), Default(None)
   *  @param manufacturerNumber Database column manufacturer_number SqlType(text), Default(None)
   *  @param tombstone Database column tombstone SqlType(bool), Default(false) */
  case class ProductSourcesRow(id: Int, product: Int, supplier: Option[Int] = None, measure: Int, unit: Int, cost: Option[scala.math.BigDecimal] = None, packaged: Boolean = false, packageName: Option[String] = None, packageSize: Double, sku: Option[String] = None, superPackageSize: Int = 1, subPackageName: Option[String] = None, pricePer: PricePerType.Value, pricingMeasure: Option[Int] = None, pricingUnit: Option[Int] = None, supplierIngredientName: Option[String] = None, syncUpstream: Option[Int] = None, syncUpstreamOwner: Option[Int] = None, manufacturerName: Option[String] = None, manufacturerNumber: Option[String] = None, tombstone: Boolean = false)

  /** Entity class storing rows of table PurchaseOrderCustomerOrders
   *  @param purchaseOrder Database column purchase_order SqlType(int4)
   *  @param customerOrder Database column customer_order SqlType(int4) */
  case class PurchaseOrderCustomerOrdersRow(purchaseOrder: Int, customerOrder: Int)

  /** Entity class storing rows of table PurchaseOrders
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4), Default(None)
   *  @param xact Database column xact SqlType(int4)
   *  @param supplier Database column supplier SqlType(int4)
   *  @param emailedAt Database column emailed_at SqlType(timestamp), Default(None)
   *  @param printedAt Database column printed_at SqlType(timestamp), Default(None)
   *  @param receivedBy Database column received_by SqlType(int4), Default(None)
   *  @param createdBy Database column created_by SqlType(int4)
   *  @param importedAt Database column imported_at SqlType(timestamp), Default(None)
   *  @param imported Database column imported SqlType(bool), Default(false) */
  case class PurchaseOrdersRow(id: Int, owner: Option[Int] = None, xact: Int, supplier: Int, emailedAt: Option[Instant] = None, printedAt: Option[Instant] = None, receivedBy: Option[Int] = None, createdBy: Int, importedAt: Option[Instant] = None, imported: Boolean = false)

  /** Entity class storing rows of table RecipeNutrients
   *  @param product Database column product SqlType(int4), PrimaryKey
   *  @param packaged Database column packaged SqlType(bool), Default(false)
   *  @param portionAmount Database column portion_amount SqlType(numeric), Default(None)
   *  @param portionMeasure Database column portion_measure SqlType(int4), Default(None)
   *  @param portionUnit Database column portion_unit SqlType(int4), Default(None)
   *  @param portionsPerPackage Database column portions_per_package SqlType(numeric), Default(None)
   *  @param servingsPerPackage Database column servings_per_package SqlType(numeric), Default(None)
   *  @param servingName Database column serving_name SqlType(text), Default(None)
   *  @param servingAmount Database column serving_amount SqlType(numeric), Default(None)
   *  @param servingMeasure Database column serving_measure SqlType(int4), Default(None)
   *  @param servingUnit Database column serving_unit SqlType(int4), Default(None) */
  case class RecipeNutrientsRow(product: Int, packaged: Boolean = false, portionAmount: Option[scala.math.BigDecimal] = None, portionMeasure: Option[Int] = None, portionUnit: Option[Int] = None, portionsPerPackage: Option[scala.math.BigDecimal] = None, servingsPerPackage: Option[scala.math.BigDecimal] = None, servingName: Option[String] = None, servingAmount: Option[scala.math.BigDecimal] = None, servingMeasure: Option[Int] = None, servingUnit: Option[Int] = None)

  /** Entity class storing rows of table Recipes
   *  @param product Database column product SqlType(int4), PrimaryKey
   *  @param outputAmount Database column output_amount SqlType(float8)
   *  @param outputMeasure Database column output_measure SqlType(int4), Default(None)
   *  @param outputUnit Database column output_unit SqlType(int4), Default(None)
   *  @param quantumAmount Database column quantum_amount SqlType(float8), Default(None)
   *  @param quantumMeasure Database column quantum_measure SqlType(int4), Default(None)
   *  @param quantumUnit Database column quantum_unit SqlType(int4), Default(None)
   *  @param maxBatchAmount Database column max_batch_amount SqlType(float8), Default(None)
   *  @param maxBatchMeasure Database column max_batch_measure SqlType(int4), Default(None)
   *  @param maxBatchUnit Database column max_batch_unit SqlType(int4), Default(None)
   *  @param price Database column price SqlType(numeric), Default(None)
   *  @param batchSize Database column batch_size SqlType(numeric), Default(None)
   *  @param batchLabel Database column batch_label SqlType(text), Default(None)
   *  @param advancePrep Database column advance_prep SqlType(bool), Default(false)
   *  @param createdAt Database column created_at SqlType(timestamp), Default(None)
   *  @param createdBy Database column created_by SqlType(int4), Default(None)
   *  @param itemNumber Database column item_number SqlType(text), Default(None) */
  case class RecipesRow(product: Int, outputAmount: Double, outputMeasure: Option[Int] = None, outputUnit: Option[Int] = None, quantumAmount: Option[Double] = None, quantumMeasure: Option[Int] = None, quantumUnit: Option[Int] = None, maxBatchAmount: Option[Double] = None, maxBatchMeasure: Option[Int] = None, maxBatchUnit: Option[Int] = None, price: Option[scala.math.BigDecimal] = None, batchSize: Option[scala.math.BigDecimal] = None, batchLabel: Option[String] = None, advancePrep: Boolean = false, createdAt: Option[Instant] = None, createdBy: Option[Int] = None, itemNumber: Option[String] = None)

  /** Entity class storing rows of table RecipesRevisionHistory
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param product Database column product SqlType(int4)
   *  @param owner Database column owner SqlType(int4), Default(None)
   *  @param lastModified Database column last_modified SqlType(timestamp) */
  case class RecipesRevisionHistoryRow(id: Int, product: Int, owner: Option[Int] = None, lastModified: Instant)

  /** Entity class storing rows of table RecipeStepIngredients
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param step Database column step SqlType(int4)
   *  @param displayOrder Database column display_order SqlType(int4)
   *  @param ingredient Database column ingredient SqlType(int4)
   *  @param description Database column description SqlType(text), Default(Some())
   *  @param amount Database column amount SqlType(float8), Default(None)
   *  @param measure Database column measure SqlType(int4), Default(None)
   *  @param unit Database column unit SqlType(int4), Default(None) */
  case class RecipeStepIngredientsRow(id: Int, step: Int, displayOrder: Int, ingredient: Int, description: Option[String] = Some(""), amount: Option[Double] = None, measure: Option[Int] = None, unit: Option[Int] = None)

  /** Entity class storing rows of table RecipeSteps
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param product Database column product SqlType(int4)
   *  @param displayOrder Database column display_order SqlType(int4)
   *  @param description Database column description SqlType(text), Default(None)
   *  @param photo Database column photo SqlType(text), Default(None) */
  case class RecipeStepsRow(id: Int, product: Int, displayOrder: Int, description: Option[String] = None, photo: Option[String] = None)

  /** Entity class storing rows of table SimplePreparations
   *  @param outputProduct Database column output_product SqlType(int4), PrimaryKey
   *  @param inputProduct Database column input_product SqlType(int4)
   *  @param name Database column name SqlType(text)
   *  @param advancePrep Database column advance_prep SqlType(bool)
   *  @param usesParentMeasures Database column uses_parent_measures SqlType(bool)
   *  @param yieldFraction Database column yield_fraction SqlType(float8), Default(1.0)
   *  @param yieldMeasure Database column yield_measure SqlType(int4) */
  case class SimplePreparationsRow(outputProduct: Int, inputProduct: Int, name: String, advancePrep: Boolean, usesParentMeasures: Boolean, yieldFraction: Double = 1.0, yieldMeasure: Int)

  /** Entity class storing rows of table Subscriptions
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param userId Database column user_id SqlType(int4)
   *  @param cancelled Database column cancelled SqlType(bool), Default(false)
   *  @param stripeSubscriptionId Database column stripe_subscription_id SqlType(text)
   *  @param stripePlanId Database column stripe_plan_id SqlType(text)
   *  @param planStarted Database column plan_started SqlType(timestamp)
   *  @param planValidUntil Database column plan_valid_until SqlType(timestamp)
   *  @param promoCode Database column promo_code SqlType(text), Default(None)
   *  @param pastDue Database column past_due SqlType(bool), Default(false)
   *  @param subscriptionLevel Database column subscription_level SqlType(text)
   *  @param addons Database column addons SqlType(_text), Length(2147483647,false) */
  case class SubscriptionsRow(id: Int, userId: Int, cancelled: Boolean = false, stripeSubscriptionId: String, stripePlanId: String, planStarted: Instant, planValidUntil: Instant, promoCode: Option[String] = None, pastDue: Boolean = false, subscriptionLevel: String, addons: List[String])

  /** Entity class storing rows of table SupplierContacts
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param supplierId Database column supplier_id SqlType(int4)
   *  @param displayOrder Database column display_order SqlType(int4)
   *  @param name Database column name SqlType(text), Default(None)
   *  @param phone Database column phone SqlType(text), Default(None)
   *  @param email Database column email SqlType(text), Default(None)
   *  @param comments Database column comments SqlType(text), Default(None) */
  case class SupplierContactsRow(id: Int, supplierId: Int, displayOrder: Int, name: Option[String] = None, phone: Option[String] = None, email: Option[String] = None, comments: Option[String] = None)

  /** Entity class storing rows of table Suppliers
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param name Database column name SqlType(text), Default(None)
   *  @param accountId Database column account_id SqlType(text), Default(None)
   *  @param phone Database column phone SqlType(text), Default(None)
   *  @param email Database column email SqlType(text), Default(None)
   *  @param comments Database column comments SqlType(text), Default(None)
   *  @param instructions Database column instructions SqlType(text), Default(None)
   *  @param contactName Database column contact_name SqlType(text), Default(None)
   *  @param syncUpstream Database column sync_upstream SqlType(int4), Default(None)
   *  @param syncUpstreamOwner Database column sync_upstream_owner SqlType(int4), Default(None)
   *  @param lastSynced Database column last_synced SqlType(timestamp), Default(None)
   *  @param lastModified Database column last_modified SqlType(timestamp), Default(None)
   *  @param tombstone Database column tombstone SqlType(bool), Default(false) */
  case class SuppliersRow(id: Int, owner: Int, name: Option[String] = None, accountId: Option[String] = None, phone: Option[String] = None, email: Option[String] = None, comments: Option[String] = None, instructions: Option[String] = None, contactName: Option[String] = None, syncUpstream: Option[Int] = None, syncUpstreamOwner: Option[Int] = None, lastSynced: Option[Instant] = None, lastModified: Option[Instant] = None, tombstone: Boolean = false)

  /** Entity class storing rows of table TransactionDeltas
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param xact Database column xact SqlType(int4)
   *  @param owner Database column owner SqlType(int4)
   *  @param product Database column product SqlType(int4), Default(None)
   *  @param amount Database column amount SqlType(float8)
   *  @param measure Database column measure SqlType(int4), Default(None)
   *  @param unit Database column unit SqlType(int4), Default(None)
   *  @param cashFlow Database column cash_flow SqlType(numeric), Default(None)
   *  @param productSource Database column product_source SqlType(int4), Default(None)
   *  @param sourceMeasure Database column source_measure SqlType(int4), Default(None)
   *  @param sourceUnit Database column source_unit SqlType(int4), Default(None)
   *  @param packaged Database column packaged SqlType(bool), Default(None)
   *  @param pricePer Database column price_per SqlType(price_per_type), Default(None)
   *  @param pricingMeasure Database column pricing_measure SqlType(int4), Default(None)
   *  @param pricingUnit Database column pricing_unit SqlType(int4), Default(None)
   *  @param packageName Database column package_name SqlType(text), Default(None)
   *  @param subPackageName Database column sub_package_name SqlType(text), Default(None)
   *  @param packageSize Database column package_size SqlType(float8), Default(None)
   *  @param superPackageSize Database column super_package_size SqlType(float8), Default(None)
   *  @param causedBy Database column caused_by SqlType(int4), Default(None) */
  case class TransactionDeltasRow(id: Int, xact: Int, owner: Int, product: Option[Int] = None, amount: Double, measure: Option[Int] = None, unit: Option[Int] = None, cashFlow: Option[scala.math.BigDecimal] = None, productSource: Option[Int] = None, sourceMeasure: Option[Int] = None, sourceUnit: Option[Int] = None, packaged: Option[Boolean] = None, pricePer: Option[PricePerType.Value] = None, pricingMeasure: Option[Int] = None, pricingUnit: Option[Int] = None, packageName: Option[String] = None, subPackageName: Option[String] = None, packageSize: Option[Double] = None, superPackageSize: Option[Double] = None, causedBy: Option[Int] = None)

  /** Entity class storing rows of table TransactionMeasures
   *  @param transactionDelta Database column transaction_delta SqlType(int4)
   *  @param measure Database column measure SqlType(int4)
   *  @param preferredUnit Database column preferred_unit SqlType(int4)
   *  @param conversionMeasure Database column conversion_measure SqlType(int4)
   *  @param conversionUnit Database column conversion_unit SqlType(int4)
   *  @param conversion Database column conversion SqlType(float8)
   *  @param customName Database column custom_name SqlType(text), Default(None)
   *  @param amount Database column amount SqlType(float8), Default(1.0) */
  case class TransactionMeasuresRow(transactionDelta: Int, measure: Int, preferredUnit: Int, conversionMeasure: Int, conversionUnit: Int, conversion: Double, customName: Option[String] = None, amount: Double = 1.0)

  /** Entity class storing rows of table Transactions
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param time Database column time SqlType(timestamp)
   *  @param isFinalized Database column is_finalized SqlType(bool), Default(true)
   *  @param finalizedxact Database column finalizedxact SqlType(int4), Default(None)
   *  @param xactType Database column xact_type SqlType(transaction_type)
   *  @param includeInCalculatedInventory Database column include_in_calculated_inventory SqlType(bool), Default(true) */
  case class TransactionsRow(id: Int, owner: Int, time: Instant, isFinalized: Boolean = true, finalizedxact: Option[Int] = None, xactType: TransactionType.Value, includeInCalculatedInventory: Boolean = true)

  /** Entity class storing rows of table Units
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param measure Database column measure SqlType(int4)
   *  @param size Database column size SqlType(float8)
   *  @param name Database column name SqlType(text)
   *  @param abbr Database column abbr SqlType(text), Default(None)
   *  @param metric Database column metric SqlType(bool), Default(true)
   *  @param scaleUpLimit Database column scale_up_limit SqlType(float8), Default(None)
   *  @param scaleDownLimit Database column scale_down_limit SqlType(float8), Default(None)
   *  @param precision Database column precision SqlType(int4), Default(2)
   *  @param quantum Database column quantum SqlType(float8), Default(0.0)
   *  @param commonPurchaseUnit Database column common_purchase_unit SqlType(bool), Default(false) */
  case class UnitsRow(id: Int, measure: Int, size: Double, name: String, abbr: Option[String] = None, metric: Boolean = true, scaleUpLimit: Option[Double] = None, scaleDownLimit: Option[Double] = None, precision: Int = 2, quantum: Double = 0.0, commonPurchaseUnit: Boolean = false)

  /** Entity class storing rows of table UpstreamInvites
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param emailToken Database column email_token SqlType(int4)
   *  @param invitee Database column invitee SqlType(int4)
   *  @param inviter Database column inviter SqlType(int4)
   *  @param inviteeEmail Database column invitee_email SqlType(text) */
  case class UpstreamInvitesRow(id: Int, emailToken: Int, invitee: Int, inviter: Int, inviteeEmail: String)

  /** Entity class storing rows of table UserAccounts
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param userId Database column user_id SqlType(int4)
   *  @param isActive Database column is_active SqlType(bool)
   *  @param joinedAt Database column joined_at SqlType(timestamp)
   *  @param permissionLevel Database column permission_level SqlType(account_permission_level) */
  case class UserAccountsRow(id: Int, owner: Int, userId: Int, isActive: Boolean, joinedAt: Instant, permissionLevel: AccountPermissionLevel.Value)

  /** Entity class storing rows of table UserLogins
   *  @param providerId Database column provider_id SqlType(text)
   *  @param providerKey Database column provider_key SqlType(text)
   *  @param userId Database column user_id SqlType(int4)
   *  @param firstName Database column first_name SqlType(text), Default(None)
   *  @param lastName Database column last_name SqlType(text), Default(None)
   *  @param fullName Database column full_name SqlType(text), Default(None)
   *  @param email Database column email SqlType(text), Default(None)
   *  @param oldUsername Database column old_username SqlType(text), Default(None) */
  case class UserLoginsRow(providerId: String, providerKey: String, userId: Int, firstName: Option[String] = None, lastName: Option[String] = None, fullName: Option[String] = None, email: Option[String] = None, oldUsername: Option[String] = None)

  /** Entity class storing rows of table UserOnboardingBoxes
   *  @param userId Database column user_id SqlType(int4)
   *  @param page Database column page SqlType(bpchar), Length(120,false)
   *  @param nextStep Database column next_step SqlType(int2) */
  case class UserOnboardingBoxesRow(userId: Int, page: String, nextStep: Short)

  /** Entity class storing rows of table UserOnboardingWidgets
   *  @param userId Database column user_id SqlType(int4)
   *  @param widget Database column widget SqlType(int4) */
  case class UserOnboardingWidgetsRow(userId: Int, widget: Int)

  /** Entity class storing rows of table UserPrintPreferences
   *  @param userId Database column user_id SqlType(int4), PrimaryKey
   *  @param orderIncludePreps Database column order_include_preps SqlType(bool), Default(false)
   *  @param orderIncludeRecipes Database column order_include_recipes SqlType(bool), Default(false)
   *  @param orderIncludeSubRecipes Database column order_include_sub_recipes SqlType(bool), Default(false)
   *  @param printSubRecipePreferences Database column print_sub_recipe_preferences SqlType(bool), Default(false)
   *  @param breakPageOnRecipePrint Database column break_page_on_recipe_print SqlType(bool), Default(false)
   *  @param recipePrintFormatPreference Database column recipe_print_format_preference SqlType(recipe_print_format_type)
   *  @param includePhotoInRecipePrint Database column include_photo_in_recipe_print SqlType(bool), Default(false)
   *  @param includeNutritionInMultipleRecipePrint Database column include_nutrition_in_multiple_recipe_print SqlType(bool), Default(false)
   *  @param includeCostBreakdownInMultipleRecipePrint Database column include_cost_breakdown_in_multiple_recipe_print SqlType(bool), Default(false) */
  case class UserPrintPreferencesRow(userId: Int, orderIncludePreps: Boolean = false, orderIncludeRecipes: Boolean = false, orderIncludeSubRecipes: Boolean = false, printSubRecipePreferences: Boolean = false, breakPageOnRecipePrint: Boolean = false, recipePrintFormatPreference: RecipePrintFormatType.Value, includePhotoInRecipePrint: Boolean = false, includeNutritionInMultipleRecipePrint: Boolean = false, includeCostBreakdownInMultipleRecipePrint: Boolean = false)

  /** Entity class storing rows of table Users
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param weightSystem Database column weight_system SqlType(measure_systems_type)
   *  @param volumeSystem Database column volume_system SqlType(measure_systems_type)
   *  @param purchaseWeightSystem Database column purchase_weight_system SqlType(measure_systems_type)
   *  @param purchaseVolumeSystem Database column purchase_volume_system SqlType(measure_systems_type)
   *  @param hideAllRecipes Database column hide_all_recipes SqlType(bool), Default(false)
   *  @param sourcePriceChangeWarningFraction Database column source_price_change_warning_fraction SqlType(numeric), Default(0.2)
   *  @param newRecipeThresholdDays Database column new_recipe_threshold_days SqlType(int4), Default(14)
   *  @param email Database column email SqlType(text), Default(None)
   *  @param hasher Database column hasher SqlType(text), Default(None)
   *  @param hashedPassword Database column hashed_password SqlType(text), Default(None)
   *  @param salt Database column salt SqlType(text), Default(None)
   *  @param firstName Database column first_name SqlType(text), Default(None)
   *  @param lastName Database column last_name SqlType(text), Default(None)
   *  @param fullName Database column full_name SqlType(text), Default(None)
   *  @param subtractInventoryPreference Database column subtract_inventory_preference SqlType(subtract_inventory_type)
   *  @param phoneNumber Database column phone_number SqlType(text), Default(None) */
  case class UsersRow(id: Int, weightSystem: MeasureSystemsType.Value, volumeSystem: MeasureSystemsType.Value, purchaseWeightSystem: MeasureSystemsType.Value, purchaseVolumeSystem: MeasureSystemsType.Value, hideAllRecipes: Boolean = false, sourcePriceChangeWarningFraction: scala.math.BigDecimal = scala.math.BigDecimal("0.2"), newRecipeThresholdDays: Int = 14, email: Option[String] = None, hasher: Option[String] = None, hashedPassword: Option[String] = None, salt: Option[String] = None, firstName: Option[String] = None, lastName: Option[String] = None, fullName: Option[String] = None, subtractInventoryPreference: SubtractInventoryType.Value, phoneNumber: Option[String] = None)

  /** Entity class storing rows of table UserSharedInvites
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param emailToken Database column email_token SqlType(int4)
   *  @param inviteeEmail Database column invitee_email SqlType(text)
   *  @param inviter Database column inviter SqlType(int4)
   *  @param inviteLevel Database column invite_level SqlType(account_permission_level) */
  case class UserSharedInvitesRow(id: Int, emailToken: Int, inviteeEmail: String, inviter: Int, inviteLevel: AccountPermissionLevel.Value)
