package models

import models.Tables._
import com.mohiva.play.silhouette.api.LoginInfo
import java.time.Instant
import java.time.temporal.ChronoUnit

import slick.lifted.{OptionColumnExtensionMethods, OptionLift}

import scala.concurrent.ExecutionContext

object QueryExtensions {

  import Tables.profile.api._

  type AllColumnsQuery[T <: Table[R], R] = Query[T, T#TableElementType, Seq]
  type TableId = TableQuery[_] // when needing to preserve set or map of tables

  // Going to stay Options; really just adds an isDefined check
  implicit class OptionRepExtensions[T, P](o: Rep[Option[T]])(implicit ol: OptionLift[P, Rep[Option[T]]]) {
    def exists(f: Rep[Option[T]] => Rep[Option[Boolean]]): Rep[Option[Boolean]] = o.isDefined && f(o)
  }

  implicit class UserLoginExtension(
      val l: AllColumnsQuery[UserLogins, UserLoginsRow]
  ) {

    def byLoginInfo(li: LoginInfo) = l.filter { login =>
      login.providerKey === li.providerKey && login.providerId === li.providerID
    }

    def user = Users.filter { u =>
      u.id in l.map(_.userId)
    }
  }

  def ownedByAccount(acc: Accounts, ownerField: Rep[Option[Int]]) =
    acc.id === ownerField || (acc.isStdlib && ownerField.isEmpty)

  implicit class AccountExtensions(
      val a: AllColumnsQuery[Accounts, AccountsRow]
  ) {
    def suppliers = Suppliers.filter { s =>
      s.owner in a.map(_.id)
    }

    def products = Products.filter(_.id in (a join Products on { (joinAccount, joinProduct) =>
      ownedByAccount(joinAccount, joinProduct.owner)
    }).map(_._2.id))

    def transactions = Transactions.filter { xact =>
      xact.owner in a.map(_.id)
    }

    def menus = Menus.filter { menu =>
      menu.owner in a.map(_.id)
    }

    def orders = Orders.filter { order =>
      order.owner in a.map(_.id)
    }

    def inventories = Inventories.filter { inventory =>
      inventory.owner in a.map(_.id)
    }

    def inventoryItems = InventoryItems.filter { inventoryItem =>
      inventoryItem.owner in a.map(_.id)
    }

    def ingredientPars = IngredientPars.filter { par =>
      par.owner in a.map(_.id)
    }

    def purchaseOrders = PurchaseOrders.filter { purchaseOrder =>
      purchaseOrder.owner in a.map(_.id)
    }

    def subscriptionHistory = Subscriptions.filter { s =>
      s.userId in a.map(_.id)
    }

    private def subscriptionIsCancelled(s: Tables.Subscriptions): Rep[Boolean] =
      s.cancelled && s.planValidUntil < Instant.now

    private def subscriptionIsValid(s: Tables.Subscriptions): Rep[Boolean] =
      !subscriptionIsCancelled(s) && !s.pastDue

    def currentSubscription = subscriptionHistory.filter(subscriptionIsValid)
    def stripeActiveSubscription = subscriptionHistory.filter(!subscriptionIsCancelled(_))

    def lastSubscription = subscriptionHistory.filter(!subscriptionIsValid(_))
        .sortBy(_.planValidUntil.desc.nullsFirst)
        .take(1)

    def lastStripeExpiredSubscription = subscriptionHistory.filter(subscriptionIsCancelled)
        .sortBy(_.planValidUntil.desc.nullsFirst)
        .take(1)

    def categories = Categories.filter(_.id in (a join Categories on { (joinAccount, joinCategory) =>
      ownedByAccount(joinAccount, joinCategory.owner)
    }).map(_._2.id))

    def ingredientCategories = categories.filter(_.categoryType === ProductCategoryType.Ingredient)
    def recipeCategories = categories.filter(_.categoryType === ProductCategoryType.Recipe)
    def inventoryCategories = categories.filter(_.categoryType === ProductCategoryType.Inventory)

    def owner = Users.filter { u =>
      u.id in a.map(_.accountOwner)
    }
    def userAccounts = UserAccounts.filter { ua =>
      ua.owner in a.map(_.id)
    }
    def userInvites = UserSharedInvites.filter { inv =>
      inv.inviter in a.map(_.id)
    }
    def isUpstreamInvited = UpstreamInvites.filter(_.invitee in a.map(_.id)).exists
    def downstreams = Accounts.filter(_.multiLocationUpstream in a.map(_.id))
    def upstream = Accounts.filter(_.id in a.map(_.multiLocationUpstream))

    def locationsSyncedRecipes = LocationsSyncedRecipes.filter { syncedRecipe =>
      syncedRecipe.owner in a.map(_.id)
    }

    def locationsSyncedSuppliers = LocationsSyncedSuppliers.filter { syncedSupplier =>
      syncedSupplier.owner in a.map(_.id)
    }

    def posImportPreference = PosImportPreferences.filter { posImport =>
      posImport.owner in a.map(_.id)
    }
  }

  implicit class UserExtensions(
      val u: AllColumnsQuery[Users, UsersRow]
  ) {

    def printPreferences = UserPrintPreferences.filter { pref =>
      pref.userId in u.map(_.id)
    }

    def userLogins = UserLogins.filter { li =>
      li.userId in u.map(_.id)
    }

    def visitedBoxes = UserOnboardingBoxes.filter { ob =>
      ob.userId in u.map(_.id)
    }

    def visitedWidgets = UserOnboardingWidgets.filter { ob =>
      ob.userId in u.map(_.id)
    }

    def userAccounts = UserAccounts.filter { ua =>
      ua.userId in u.map(_.id)
    }

    def account = Accounts.filter { ac =>
      ac.accountOwner in u.map(_.id)
    }
  }

  implicit class UserAccountExtensions(
      val ua: AllColumnsQuery[UserAccounts, UserAccountsRow]
  ) {
    def user = Users.filter { u =>
      u.id in ua.map(_.userId)
    }

    def account = Accounts.filter { a =>
      a.id in ua.map(_.owner)
    }
  }

  implicit class SubscriptionExtensions(
      val s: AllColumnsQuery[Subscriptions, SubscriptionsRow]
  ) {
    def account = {
      Accounts.filter { a =>
        a.id in s.map(_.userId)
      }
    }
  }

  implicit class SupplierExtensions(
      val s: AllColumnsQuery[Suppliers, SuppliersRow]
  ) {
    def contacts = {
      SupplierContacts.filter { c =>
        c.supplierId in s.map(_.id)
      }
    }

    def sourcedProducts = {
      ProductSources.filter { source =>
        source.supplier in s.map(_.id)
      }
    }

    def purchaseOrders = {
      PurchaseOrders.filter { po =>
        po.supplier in s.map(_.id)
      }
    }

    def syncDownstreams = {
      Suppliers.filter { downstream =>
        downstream.syncUpstream in s.map(_.id)
      }
    }
    def syncUpstream = Suppliers.filter { upstream =>
      upstream.id in s.map(_.syncUpstream)
    }
  }

  implicit class ProductSourceExtensions(
    val ps: AllColumnsQuery[ProductSources, ProductSourcesRow]
  ) {
    def products = {
      Products.filter { p =>
        p.id in ps.map(_.product)
      }
    }

    def syncDownstreams = {
      ProductSources.filter { s =>
        s.syncUpstream in ps.map(_.id)
      }
    }
  }

  implicit class InventoryExtensions(
    val invens: AllColumnsQuery[Inventories, InventoriesRow]
  ) {
    def items = {
      InventoryItems.filter { i =>
        i.inventory in invens.map(_.id)
      }
    }
  }


  implicit class MeasureExtensions(
      val m: AllColumnsQuery[Measures, MeasuresRow]
  ) {
    def units = {
      Units.filter { u =>
        u.measure in m.map(_.id)
      }
    }
  }

  /*
   * all the product stuff
   */
  implicit class ProductExtensions(
      val prods: AllColumnsQuery[Products, ProductsRow]
  ) {

    def info = ProductInfos.filter { info =>
      info.product in prods.map(_.id)
    }

    def userVisible = prods.filter(!_.hidden)

    def sources = ProductSources.filter { s =>
      s.product in prods.map(_.id)
    }

    def explicitlyPreferredSource = ProductSources.filter { s =>
      prods.filter(p => p.preferredSource === s.id && s.product === p.id).exists
    }

    def measures = ProductMeasures.filter { m =>
      m.product in prods.map(_.id)
    }

    def recipe = Recipes.filter { r =>
      r.product in prods.map(_.id)
    }

    def recipeNutrients = RecipeNutrients.filter { rn =>
      rn.product in prods.map(_.id)
    }

    def simplePreps = SimplePreparations.filter { prep =>
      prep.inputProduct in prods.map(_.id)
    }

    def preppedBy = SimplePreparations.filter { prep =>
      prep.outputProduct in prods.map(_.id)
    }

    def usages = RecipeStepIngredients.filter { u =>
      u.ingredient in prods.map(_.id)
    }

    def nutrients = ProductNutrients.filter { n =>
      n.product in prods.map(_.id)
    }

    def allergens = ProductAllergens.filter { a =>
      a.product in prods.map(_.id)
    }

    def characteristics = ProductCharacteristics.filter { c =>
      c.product in prods.map(_.id)
    }

    def parLevel = IngredientPars.filter { p =>
      p.product in prods.map(_.id)
    }

    def syncDownstreams = Products.filter { p =>
      p.syncUpstream in prods.map(_.id)
    }
    def syncUpstream = Products.filter { p =>
      p.id in prods.map(_.syncUpstream)
    }

    def user = Users.filter { u =>
      u.id in prods.map(_.owner)
    }

    def categories = ProductCategories.filter { pc =>
      pc.product in prods.map(_.id)
    }

    def priceHistory = IngredientPriceHistory.filter { ph =>
      ph.product in prods.map(_.id)
    }

    // the following are read-only
    private def categoriesOfType(t: ProductCategoryType.Value) = Categories.filter(_.categoryType === t)
    private def categoriesOfTypeForProduct(t: ProductCategoryType.Value) =
      (categoriesOfType(t) join ProductCategories on (_.id === _.category))
          .filter { case (c, pc ) => pc.product in prods.map(_.id) }
          .map { case (c, pc) => c }

    def ingredientCategories = categoriesOfTypeForProduct(ProductCategoryType.Ingredient)
    def recipeCategories = categoriesOfTypeForProduct(ProductCategoryType.Recipe)
    def inventoryCategories = categoriesOfTypeForProduct(ProductCategoryType.Inventory)
  }

  implicit class CategoriesExtensions(
      val c: AllColumnsQuery[Categories, CategoriesRow]
  ) {
    def productCategories = ProductCategories.filter { pc =>
      pc.category in c.map(_.id)
    }

    def downstreamCategories = Categories.filter { otherC =>
      otherC.syncUpstream in c.map(_.id)
    }
  }

  implicit class ProductCategoriesExtensions(
      val pc: AllColumnsQuery[ProductCategories, ProductCategoriesRow]
  ) {
    def products = Products.filter { p =>
      p.id in pc.map(_.product)
    }
    def categories = Categories.filter { c =>
      c.id in pc.map(_.category)
    }

    def names = Categories.filter { c =>
      c.id in pc.map(_.category)
    }.map(_.name)
  }

  implicit class SimplePreparationExtensions(
      val preps: AllColumnsQuery[SimplePreparations, SimplePreparationsRow]
  ) {
    def outputProducts = Products.filter { p =>
      p.id in preps.map(_.outputProduct)
    }

    def inputProducts = Products.filter { p =>
      p.id in preps.map(_.inputProduct)
    }
  }

  implicit class RecipeExtensions(
      val recipes: AllColumnsQuery[Recipes, RecipesRow]
  ) {
    def product = Products.filter { p =>
      p.id in recipes.map(_.product)
    }

    def steps = RecipeSteps.filter { s =>
      s.product in recipes.map(_.product)
    }

    def revisionHistory = RecipesRevisionHistory.filter { h =>
      h.product in recipes.map(_.product)
    }

    def createdBy =  Users.filter { u =>
      u.id in recipes.map(_.createdBy)
    }
  }

  implicit class RecipeRevisionHistoryExtensions(
     val recipeRevisionHistory: AllColumnsQuery[RecipesRevisionHistory, RecipesRevisionHistoryRow]
  ) {
    def user = Users.filter { u =>
      u.id in recipeRevisionHistory.map(_.owner)
    }

    def recipe = Recipes.filter { r =>
      r.product in recipeRevisionHistory.map(_.product)
    }
  }

  implicit class RecipeStepExtensions(
      val steps: AllColumnsQuery[RecipeSteps, RecipeStepsRow]
  ) {
    def ingredientUsage = RecipeStepIngredients.filter { i =>
      i.step in steps.map(_.id)
    }

    def recipe = Recipes.filter { r =>
      r.product in steps.map(_.product)
    }
  }

  implicit class RecipeStepIngredientExtensions(
      val usages: AllColumnsQuery[RecipeStepIngredients, RecipeStepIngredientsRow]
  ) {
    def step = RecipeSteps.filter { s =>
      s.id in usages.map(_.step)
    }

    def ingredients = Products.filter { p =>
      p.id in usages.map(_.ingredient)
    }
  }
  // end product stuff

  /*
   * all the menu stuff
   */
  implicit class MenuExtensions(
      val menus: AllColumnsQuery[Menus, MenusRow]
  ) {
    def sections = MenuSections filter { sect =>
      sect.menu in menus.map(_.id)
    }
  }

  implicit class MenuSectionExtensions(
      val sections: AllColumnsQuery[MenuSections, MenuSectionsRow]
  ) {
    def items = MenuItems.filter { item =>
      item.section in sections.map(_.id)
    }
  }

  implicit class MenuItemExtensions(
      val items: AllColumnsQuery[MenuItems, MenuItemsRow]
  ) {
    def ingredients = Products.filter { prod =>
      prod.id in items.map(_.product)
    }
  }
  // end menu stuff

  /*
   * all the transactions stuff
   */

  // helper functions
  def archival(xact: Tables.Transactions, now: Instant) = {
    // can't store timestamp with timezone easily in postgresql, so just being paranoid
    // about it
    val oldOrderCutoff = now.minus(2, ChronoUnit.DAYS)
    xact.isClosed && xact.time < oldOrderCutoff
  }

  implicit class TransactionExtensions(
      val xacts: AllColumnsQuery[Transactions, TransactionsRow]
  ) {
    def deltas = TransactionDeltas.filter { delta =>
      delta.xact in xacts.map(_.id)
    }

    def open(now: Instant) = xacts.filter(!archival(_, now))

    def closed(now: Instant) = xacts.filter(archival(_, now))
  }

  implicit class TransactionRepExtensions(
      val xact: Transactions
  ) {
    def isClosed: Rep[Boolean] = xact.isFinalized || xact.finalizedxact.isDefined
  }

  implicit class TransactionDeltaExtensions(
      val deltas: AllColumnsQuery[TransactionDeltas, TransactionDeltasRow]
  ) {
    def xact = Transactions.filter { xact =>
      xact.id in deltas.map(_.xact)
    }

    def product = Products.filter { p =>
      p.id in deltas.map(_.product)
    }

    def measures = TransactionMeasures.filter { tm =>
      tm.transactionDelta in deltas.map(_.id)
    }
  }
  // end transactions stuff

  /*
   * all the purchaseOrders stuff
   */
  implicit class PurchaseOrderExtensions(
      val purchaseOrders: AllColumnsQuery[PurchaseOrders, PurchaseOrdersRow]
  ) {
    def orders = PurchaseOrderCustomerOrders.filter { m =>
      m.purchaseOrder in purchaseOrders.map(_.id)
    }

  }
  // end purchaseOrders stuff

  /*
   * all the orders stuff
   */
  implicit class OrderExtensions(
      val orders: AllColumnsQuery[Orders, OrdersRow]
  ) {
    def menu = Menus.filter { m =>
      m.id in orders.map(_.menu)
    }

    def transaction = Transactions.filter { xact =>
      xact.id in orders.map(_.xact)
    }
  }
  // end orders stuff

  implicit class IngredientPriceHistoryExtensions(
      val ingredientPriceHistory: AllColumnsQuery[IngredientPriceHistory, IngredientPriceHistoryRow]
  ) {
    def measures = IngredientPriceHistoryMeasures.filter { measure =>
      measure.ingredientPrice in ingredientPriceHistory.map(_.id)
    }
  }
}
