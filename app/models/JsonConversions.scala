package models

import models.Tables._
import controllers.api.RecipeUtils.{ProductId, ProductSourceId, Quantity}
import util.MiscellaneousImplicits._

import play.api.libs.functional.syntax._
import play.api.libs.json.Json.{WithDefaultValues, toJsObject}
import play.api.libs.json._

import java.time.format.DateTimeFormatter

// Instant.parse() and instant.format() both both use this internally
// import java.time.format.DateTimeFormatter.ISO_INSTANT
import java.time.{Instant, LocalDate}

object JsonConversions {

  implicit lazy val dateFormat: Format[Instant] = DateFormatting.iso8601DateTimeFormat

  lazy val supplierFormat = Json.format[SuppliersRow]
  lazy val supplierContactFormat = Json.format[SupplierContactsRow]

  lazy val measuresFormat = Json.format[MeasuresRow]
  lazy val unitsFormat = Json.format[UnitsRow]

  lazy val nutrientsFormat = Json.format[NutrientsRow]

  lazy val inventoryItemsFormat = Json.format[InventoryItemsRow]
  lazy val ingredientParFormat = Json.format[IngredientParsRow]

  implicit lazy val pricePerTypeFormat = FancyFormat.enum(PricePerType)
  lazy val productsFormat = Json.format[ProductsRow]
  lazy val productInfosFormat = Json.using[WithDefaultValues].format[ProductInfosRow]
  lazy val productMeasuresFormat = Json.format[ProductMeasuresRow]
  lazy val productSourcesFormat = Json.format[ProductSourcesRow]
  lazy val productNutrientsFormat = Json.format[ProductNutrientsRow]
  lazy val productAllergensFormat = Json.using[WithDefaultValues].format[ProductAllergensRow]
  lazy val productCharacteristicsFormat = Json.using[WithDefaultValues].format[ProductCharacteristicsRow]

  lazy val recipesFormat = Json.format[RecipesRow]
  lazy val recipeStepsFormat = Json.format[RecipeStepsRow]
  lazy val recipeStepIngredientsFormat = Json.format[RecipeStepIngredientsRow]
  lazy val recipeNutrientsFormat = Json.format[RecipeNutrientsRow]
  lazy val simplePrepsFormat = Json.format[SimplePreparationsRow]

  lazy val menusFormat = Json.format[MenusRow]
  lazy val menuSectionsFormat = Json.format[MenuSectionsRow]
  lazy val menuItemsFormat = Json.format[MenuItemsRow]

  implicit lazy val xactTypeFormat = FancyFormat.enum(TransactionType)
  lazy val transactionsFormat = Json.using[WithDefaultValues].format[TransactionsRow]
  lazy val transactionDeltasFormat = Json.format[TransactionDeltasRow]
  lazy val transactionMeasuresFormat = Json.format[TransactionMeasuresRow]

  lazy val purchaseOrdersFormat = Json.format[PurchaseOrdersRow]

  lazy val ordersFormat = Json.format[OrdersRow]

  implicit lazy val accountPermissionLevelFormat = FancyFormat.enum(AccountPermissionLevel)
  implicit lazy val recipePrintFormatTypeFormat = FancyFormat.enum(RecipePrintFormatType)
  implicit lazy val userPrintPreferencesFormat = Json.format[UserPrintPreferencesRow]
  lazy val userSharedInvitesFormat = Json.format[UserSharedInvitesRow]
  lazy val userAccountsFormat = Json.format[UserAccountsRow]

  lazy val upstreamInvitesFormat = Json.format[UpstreamInvitesRow]

  implicit lazy val onboardingPlacementFormat = FancyFormat.enum(OnboardingPlacement)
  lazy val onboardingPagesFormat = Json.format[OnboardingPagesRow]
  lazy val onboardingStepsFormat = Json.format[OnboardingStepsRow]
  lazy val onboardingBoxesFormat = Json.format[UserOnboardingBoxesRow]

  implicit lazy val productCategoryTypeFormat = FancyFormat.enum(ProductCategoryType)
  lazy val categoriesFormat = Json.format[CategoriesRow]

  lazy val onboardingWidgetsFormat = Json.format[OnboardingWidgetsRow]

 lazy val posPreferenceFormat = Json.format[PosImportPreferencesRow]

  object DateFormatting {

    // copied from Play Framework JSON libs (Apache License)
    private def parseDate(input: String): Option[Instant] =
      scala.util.control.Exception.allCatch[Instant] opt DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(input, Instant.from(_))

    val iso8601DateTimeFormat = Format[Instant](
      Reads { v =>
        for {
          s <- Reads.of[JsString].reads(v)
          date <- parseDate(s.value) match {
            case Some(dateTime) => JsSuccess(dateTime)
            case None => JsError(Seq(JsPath() -> Seq(JsonValidationError("error.expected.date"))))
          }
        } yield date
      },
      Writes(instant => JsString(instant.toString))
    )
  }

  object FancyFormat {
    def either[A, B](implicit fa: Format[A], fb: Format[B]): Format[Either[A, B]] = Format(
      fa.map[Either[A,B]](Left(_)) orElse fb.map[Either[A,B]](Right(_)),
      Writes (_
          .left.map(fa.writes)
          .right.map(fb.writes)
          .merge)
    )

    def enum[E <: Enumeration](e: E): Format[E#Value] = Format(
      Reads.enumNameReads(e),
      Writes.enumNameWrites[E]
    )

    val identityReads = Reads(JsSuccess(_))

    def insertField(path: JsPath, value: JsValue) =
      __.json.update(path.json.put(value))

    def insertMockFields(fields: Map[String, JsValue]) = {
      fields.map { case (fieldName, mockValue) =>
        insertField(__ \ fieldName, mockValue)
      }.fold(identityReads)(_ andThen _)
    }

    def banFields(fields: Seq[String]) = new Reads[JsObject] {
      override def reads(json: JsValue) = {
        val o = json.asInstanceOf[JsObject]
        val violations = o.keys.intersect(fields.toSet)
        if (violations.isEmpty) {
          JsSuccess(o)
        } else {
          JsError(s"request contained the following banned fields: ${violations.mkString(", ")}")
        }
      }
    }

    def unhoistFields(fields: Seq[String]) = {
      // super hacky - just put the *entire* value into each of the sub-fields
      fields.map { f =>
        __.json.update((__ \ f).json.copyFrom(__.json.pick))
      }.fold(identityReads)(_ andThen _)
    }

    def hoistFields(fields: Iterable[String], js: JsObject) = {
      val raw = js.value
      var hoisted = JsObject(raw -- fields)
      fields.foreach { f =>
        val inner = raw.getOrElse(f, JsObject.empty).asInstanceOf[JsObject]
        hoisted ++= inner
      }
      hoisted
    }


    def removeFields(fields: Seq[String], js: JsObject) =
      JsObject(js.value -- fields)

    def setDefault(keyValues: Map[String, JsValue]) = {
      keyValues.map { case (field, defaultValue) =>
        val path = __ \ field
        for {
          original <- __.read[JsObject]
          value <- path.json.pick.orElse(__.read(defaultValue))
          newObject <- __.json.update(path.json.put(value))
        } yield newObject
      }.fold(identityReads)(_ andThen _)
    }

    def apply[A](
        f: OFormat[A],
        mockIdFields: Seq[String] = Nil, // left out of Writes, substituted with "-1" in Reads
        mockFields: Map[String, JsValue] = Map(), // left out of Writes, substituted with given value in Reads
        readOnlyFields: Map[String, JsValue] = Map(), // include in Writes, substituted with given value in Reads
        omittedFields: Seq[String] = Nil, // left out of Writes, ignored in Reads (must be Option[_])
        defaultFields: Map[String, JsValue] = Map(),
        hoistedFields: Seq[String] = Nil, // object field whose sub-fields show up at the root level
    ): OFormat[A] =
      OFormat[A](banFields(mockIdFields ++ omittedFields)
          andThen insertMockFields(mockFields ++ mockIdFields.map(_ -> JsNumber(-1)) ++ readOnlyFields)
          andThen setDefault(defaultFields)
          andThen unhoistFields(hoistedFields)
          andThen f,
        f
            .transform((o: JsObject) => removeFields(mockIdFields ++ mockFields.keys ++ omittedFields, o))
            .transform((o: JsObject) => hoistFields(hoistedFields, o))
      )
  }

  case class OnboardingVisited(pageKey: String, step: Short)
  implicit lazy val OnboardingVisitedFormat = Json.format[OnboardingVisited]

  case class OnboardingApiEntity(
    visitedBoxes: Seq[UserOnboardingBoxesRow],
    pages: Seq[OnboardingPagesRow],
    steps: Seq[OnboardingStepsRow]
  )
  object onboardingApiFormats {
    private implicit val boxFormat = onboardingBoxesFormat
    private implicit val pageFormat = onboardingPagesFormat
    private implicit val stepFormat = onboardingStepsFormat
    implicit val onboardingFormat = Json.format[OnboardingApiEntity]
  }

  case class SupplierAPIEntity(
      s: SuppliersRow,
      contacts: Seq[SupplierContactsRow] = Seq(),
      sources: Seq[ProductSourcesRow] = Seq(),
  )

  case class SendOrderItem(
    product: ProductId, numPackages: Double, selectedSource: ProductSourceId, unitCost: Option[BigDecimal], cashFlow: Option[BigDecimal],
  )
  case class SendOrderRequest(
    orders: List[Int],
    items: List[SendOrderItem],
    receivedDirectly: Boolean = false,
  )

  implicit lazy val sendOrderItemFormat = Json.format[SendOrderItem]
  implicit lazy val sendOrderRequestFormat = Json.format[SendOrderRequest]

  object suppliersAPIFormats {

    implicit private val mainInfoFormat = FancyFormat[SuppliersRow](supplierFormat,
      mockIdFields=Seq("id", "owner"),
      mockFields=Map("tombstone" -> JsBoolean(false)),
    )

    implicit private val contactAPIFormat = FancyFormat[SupplierContactsRow](supplierContactFormat,
      mockIdFields=Seq("id", "supplierId", "displayOrder"))

    implicit private val sourceAPIFormat = FancyFormat[ProductSourcesRow](productSourcesFormat,
      mockIdFields=Seq("supplier"),
      mockFields=Map("tombstone" -> JsBoolean(false)),
    )

    implicit lazy val supplierAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[SupplierAPIEntity],
      hoistedFields = Seq("s"),
    )

    implicit private val supplierImportedSourceAPIFormat = FancyFormat[SupplierImportedSource](importedSupplierSourceFormat,
      hoistedFields = Seq("source")
    )
    implicit lazy val supplierImportedSourcesApiFormat = FancyFormat(Json.using[WithDefaultValues].format[SupplierImportedSources])
    case class SupplierImportedSources(id: Int, sources: Seq[SupplierImportedSource] = Seq())

    lazy val importedSupplierSourceFormat = Json.format[SupplierImportedSource]
    case class SupplierImportedSource(source: ProductSourcesRow, date: Option[Instant])
  }

  case class MeasureAPIEntity(
      m: MeasuresRow,
      units: Seq[UnitsRow])

  object measuresAPIFormats {
    implicit private val mainInfoFormat = FancyFormat[MeasuresRow](measuresFormat,
      mockIdFields=Seq("id"))

    implicit private val unitAPIFormat = FancyFormat[UnitsRow](unitsFormat,
      mockIdFields=Seq("measure"),
      defaultFields=Map("id" -> JsNumber(-1)))

    implicit lazy val measureAPIFormat = FancyFormat(Json.format[MeasureAPIEntity],
      hoistedFields = Seq("m"),
    )

    // for use only as a Writes, in places where the frontend will not be writing
    // the values back to the API
    val productMeasureReadOnlyFormat = FancyFormat[ProductMeasuresRow](productMeasuresFormat,
      mockIdFields=Seq("id", "product"),
    )
    // any place where this is applied should first filter out tombstoned measures
    val productMeasureEditableFormat = FancyFormat[ProductMeasuresRow](productMeasureReadOnlyFormat,
      mockFields=Map("tombstone" -> JsBoolean(false)),
    )

    def mergeProductMeasures(
      target : Seq[ProductMeasuresRow], source: Seq[ProductMeasuresRow], allowCorrection: Boolean = true
    ) : (Seq[ProductMeasuresRow], Boolean, Boolean) = {

      var isExpansion = false
      var isCorrection = false

      val Seq(thisMeasures, otherMeasures) = Seq(target, source).map(
        _.map(m => (m.measure -> m)).toMap
      )

      val mergedMeasures = thisMeasures.merge(otherMeasures,
        (k, thisM, otherM) => if (allowCorrection) {
          // same code in both cases, just flipping which one takes precedence
          otherM.copy(customName = otherM.customName.orElse(thisM.customName))
        } else {
          thisM.copy(customName = thisM.customName.orElse(otherM.customName))
        }
      ).values

      mergedMeasures.foreach { newMeasure => thisMeasures.get(newMeasure.measure) match {
        case Some(oldMeasure) => {
          // customName - only overwriting a defined customName is a correction
          if (oldMeasure.customName.isEmpty && newMeasure.customName.isDefined) {
            isExpansion = true
          } else if (oldMeasure.customName != newMeasure.customName) {
            isCorrection = true
          }
          // all other variables - any change is a correction
          if (oldMeasure.copy(customName = None) != newMeasure.copy(customName = None)) {
            isCorrection = true
          }
        }

        case None => isExpansion = true // new measure - expansion
      }}

      (mergedMeasures.toSeq, isExpansion, isCorrection)
    }
  }

  object quantityAPIFormats {
    /*
     * We have a minor problem - we'd like to treat quantities as a structured
     * value, but relational DBs are all about flat values (e.g. can't define
     * FK on a member of a composite type).
     *
     * So, for the API to look nice while still using the SomeTablesRow generated
     * classes, we have these utils to translate back and forth in JSON
     * (de)serialization
     */

    // used for both DB names and JSON field names
    private def dbNameFromPrefix(prefix: Option[String], baseFieldName: String) =
      prefix.map(_ + baseFieldName.capitalize).getOrElse(baseFieldName)

    private def translateQuantityFieldsToDB(prefix: Option[String]) =
      (__ \ "amount").read[JsNumber]
          .and((__ \ "unit").readNullable[JsNumber])
          .and((__ \ "measure").readNullable[JsNumber]) {
            (amount, unit, measure) => Json.obj(
              dbNameFromPrefix(prefix, "amount") -> amount,
              dbNameFromPrefix(prefix, "unit") -> unit,
              dbNameFromPrefix(prefix, "measure") -> measure)
          }

    def quantityReadTransformer(
        apiPath: JsPath,
        dbPrefix: Option[String],
        required: Boolean=false) = Reads[JsObject] { v =>
      val o = v.asInstanceOf[JsObject]
      val justQuantityBranch = if (required) {
        apiPath.read[JsObject].map(Some(_))
      } else {
        apiPath.readNullable[JsObject]
      }

      for {
        maybeQuantity <- o.validate(justQuantityBranch)
        toAdd <- maybeQuantity match {
          case Some(quantityObject) =>
            quantityObject.transform(translateQuantityFieldsToDB(dbPrefix))
          case None => JsSuccess(Json.obj())
        }
      } yield o ++ toAdd
    }

    def quantityWriteTransformer(apiName: String, dbPrefix: Option[String], v: JsValue) = {
      val o = v.asInstanceOf[JsObject]
      val dbAttributes = List("amount", "unit", "measure")
          .map(dbNameFromPrefix(dbPrefix, _))
      val List(dbAmountAttr, dbUnitAttr, dbMeasureAttr) = dbAttributes

      o.value.get(dbAmountAttr) match {
        case Some(amount) =>
          val amountPart = Json.obj("amount" -> amount)
          val unitPart = if (o.value.contains(dbUnitAttr)) {
            Json.obj(
              "unit" -> o.value(dbUnitAttr),
              "measure" -> o.value(dbMeasureAttr))
          } else {
            Json.obj()
          }
          dbAttributes.foldLeft(o)(_ - _) + // remove the db-named attributes
              (apiName -> (amountPart ++ unitPart)) // and then add the new ones
        case None => o
      }
    }

  }

  case class RecipeStepAPIEntity(
      step: RecipeStepsRow,
      ingredients: Seq[RecipeStepIngredientsRow] = Seq(),
  )

  case class RecipeRevisionHistoryAPIEntity(
      id: Int, product: Int, owner: Option[Int], lastModified: Instant, ownerName:Option[String])

  case class RecipeAPIEntity(
      recipeInfo: RecipesRow,
      steps: Seq[RecipeStepAPIEntity] = Seq(),
      history: Seq[RecipeRevisionHistoryAPIEntity],
      recipeNutrients: Option[RecipeNutrientsRow]
  )

  /*
   * these don't (now) write out a top-level API endpoint's content, but it's a big enough chunk of code
   * to split out from the general productAPIFormat's object
   */
  object recipeAPIFormats {
    implicit private val mainInfoFormat = {
      /*
       * the main info requires some massaging to get it into the nice friendly format the frontend wants.
       * basically, the JS code finds it a lot easier to have three attributes of a composite "quantity" type,
       * while PG is quite hard to make work with custom composite datatypes.
       */
      val base: OFormat[RecipesRow] = FancyFormat[RecipesRow](recipesFormat,
        mockIdFields=Seq("product"))

      import quantityAPIFormats._

      val mainInfoRead = quantityReadTransformer(__ \ "recipeSize", Some("output"), required=true)
          .andThen(quantityReadTransformer(__ \ "quantum", Some("quantum")))
          .andThen(quantityReadTransformer(__ \ "maxBatch", Some("maxBatch")))
          .andThen(base)

      val mainInfoWrite = base
          .transform(quantityWriteTransformer("recipeSize", Some("output"), _: JsValue))
          .transform(quantityWriteTransformer("quantum", Some("quantum"), _: JsValue))
          .transform(quantityWriteTransformer("maxBatch", Some("maxBatch"), _: JsValue))
      OFormat(mainInfoRead, mainInfoWrite)
    }

    implicit private val stepInfoFormat = FancyFormat[RecipeStepsRow](recipeStepsFormat,
      mockIdFields=Seq("id", "product", "displayOrder")) // so basically just the description

    implicit private val stepIngredientFormat = FancyFormat[RecipeStepIngredientsRow](recipeStepIngredientsFormat,
      mockIdFields=Seq("id", "step", "displayOrder"))

    implicit private val stepFormat = FancyFormat(Json.using[WithDefaultValues].format[RecipeStepAPIEntity],
      hoistedFields = Seq("step"),
    )

    implicit private val historyInfoWriter = Json.format[RecipeRevisionHistoryAPIEntity]

    implicit private val recipeNutrients = FancyFormat[RecipeNutrientsRow](recipeNutrientsFormat,
        mockIdFields=Seq("product"))

    implicit lazy val recipeAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[RecipeAPIEntity],
      readOnlyFields = Map("history" -> JsArray()),
      hoistedFields = Seq("recipeInfo"),
    )
  }

  object simplePrepAPIFormats {
    implicit private val productMeasureFormat = measuresAPIFormats.productMeasureEditableFormat

    implicit private val simplePrepMainInfoFormat = FancyFormat[SimplePreparationsRow](simplePrepsFormat,
      mockFields=Map(
        "inputProduct" -> JsNumber(-1),
        "name" -> JsString("") // name will be pulled from the json object key
      ),
      // can't infer is-new from method, so '-1' means new internally
      defaultFields=Map("outputProduct" -> JsNumber(-1))
    )

    implicit lazy val simplePrepAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[PreparationAPIEntity],
      hoistedFields = Seq("preparationInfo"),
    )
  }

  object nutrientsAPIFormats {
    // plain Format so ConstraintReads implicits play nicely
    implicit lazy val nutrientAPIFormat: Format[NutrientsRow] = FancyFormat[NutrientsRow](nutrientsFormat,
      defaultFields=Map("id" -> JsNumber(-1))
    )

    implicit lazy val productNutrientInfoAPIFormat = FancyFormat[ProductNutrientsRow](productNutrientsFormat,
      mockIdFields = Seq("product")
    )
  }

  object allergensAPIFormats {
    implicit lazy val productAllergensAPIFormat = FancyFormat[ProductAllergensRow](productAllergensFormat,
      mockIdFields = Seq("product")
    )

    def mergeStringField(source: Option[String], target: Option[String]) : Option[String] = {
      (source, target) match {
        case (None, target) => target
        case (source, None) => source
        case (Some(s), Some(t)) => Some(
          (s ++ "," ++ t).split(",").filter(_.nonEmpty).distinct.mkString(", ")
        )
      }
    }

    def addAllergensWithJSON(source: ProductAllergensRow, target: Map[String, JsValue]) : ProductAllergensRow = {
      source.copy(
        // Boolean fields
        milk = target.getOrElse("milk", JsBoolean(source.milk)).as[Boolean],
        eggs = target.getOrElse("eggs", JsBoolean(source.eggs)).as[Boolean],
        wheat = target.getOrElse("wheat", JsBoolean(source.wheat)).as[Boolean],
        peanuts = target.getOrElse("peanuts", JsBoolean(source.peanuts)).as[Boolean],
        soybeans = target.getOrElse("soybeans", JsBoolean(source.soybeans)).as[Boolean],
        molluscs = target.getOrElse("molluscs", JsBoolean(source.molluscs)).as[Boolean],
        cerealsGluten = target.getOrElse("cerealsGluten", JsBoolean(source.cerealsGluten)).as[Boolean],
        celery = target.getOrElse("celery", JsBoolean(source.celery)).as[Boolean],
        mustard = target.getOrElse("mustard", JsBoolean(source.mustard)).as[Boolean],
        sesameSeeds = target.getOrElse("sesameSeeds", JsBoolean(source.sesameSeeds)).as[Boolean],
        sulphurDioxideSulphites = target.getOrElse("sulphurDioxideSulphites", JsBoolean(source.sulphurDioxideSulphites)).as[Boolean],
        lupin = target.getOrElse("lupin", JsBoolean(source.lupin)).as[Boolean],
        // String fields
        fish = mergeStringField(source.fish, target.getOrElse("fish", JsNull).asOpt[String]),
        crustaceanShellfish = mergeStringField(source.crustaceanShellfish, target.getOrElse("crustaceanShellfish", JsNull).asOpt[String]),
        treeNuts = mergeStringField(source.treeNuts, target.getOrElse("treeNuts", JsNull).asOpt[String])
      )
    }

    def removeAllergensWithJSON(source: ProductAllergensRow, toRemove: Seq[String]) : ProductAllergensRow = {
      source.copy(
        // Boolean fields
        milk = if (toRemove.contains("milk")) false else source.milk,
        eggs = if (toRemove.contains("eggs")) false else source.eggs,
        wheat = if (toRemove.contains("wheat")) false else source.wheat,
        peanuts = if (toRemove.contains("peanuts")) false else source.peanuts,
        soybeans = if (toRemove.contains("soybeans")) false else source.soybeans,
        molluscs = if (toRemove.contains("molluscs")) false else source.molluscs,
        cerealsGluten = if (toRemove.contains("cerealsGluten")) false else source.cerealsGluten,
        celery = if (toRemove.contains("celery")) false else source.celery,
        mustard = if (toRemove.contains("mustard")) false else source.mustard,
        sesameSeeds = if (toRemove.contains("sesameSeeds")) false else source.sesameSeeds,
        sulphurDioxideSulphites = if (toRemove.contains("sulphurDioxideSulphites")) false else source.sulphurDioxideSulphites,
        lupin = if (toRemove.contains("lupin")) false else source.lupin,
        // // String fields
        fish = if (toRemove.contains("fish")) None else source.fish,
        crustaceanShellfish = if (toRemove.contains("crustaceanShellfish")) None else source.crustaceanShellfish,
        treeNuts = if (toRemove.contains("treeNuts")) None else source.treeNuts,
      )
    }
  }

  object characteristicsAPIFormats {
    implicit lazy val productCharacteristicsAPIFormat = FancyFormat[ProductCharacteristicsRow](productCharacteristicsFormat,
      mockIdFields = Seq("product")
    )

    def addCharacteristicsWithJSON(source: ProductCharacteristicsRow, target: Map[String, Boolean]) : ProductCharacteristicsRow = {
      source.copy(
        addedSugar = target.getOrElse("addedSugar", source.addedSugar),
        meat = target.getOrElse("meat", source.meat),
        pork = target.getOrElse("pork", source.pork),
        corn = target.getOrElse("corn", source.corn),
        poultry = target.getOrElse("poultry", source.poultry),
        nonEdible = target.getOrElse("nonEdible", source.nonEdible)
      )
    }

    def removeCharacteristicsWithJSON(source: ProductCharacteristicsRow, toRemove: Seq[String]) : ProductCharacteristicsRow = {
      source.copy(
        addedSugar = if (toRemove.contains("addedSugar")) false else source.addedSugar,
        meat = if (toRemove.contains("meat")) false else source.meat,
        pork = if (toRemove.contains("pork")) false else source.pork,
        corn = if (toRemove.contains("corn")) false else source.corn,
        poultry = if (toRemove.contains("poultry")) false else source.poultry,
        nonEdible = if (toRemove.contains("nonEdible")) false else source.nonEdible
      )
    }

    def characteristicsToSeqString(characteristics: ProductCharacteristicsRow) : Seq[String] = {
      Seq(
        if (characteristics.addedSugar) "added sugar" else "",
        if (characteristics.meat) "meat" else "",
        if (characteristics.pork) "pork" else "",
        if (characteristics.corn) "corn" else "",
        if (characteristics.poultry) "poultry" else "",
        if (characteristics.nonEdible) "non-edible" else ""
      ).filter(_.nonEmpty)
    }
  }

  case class PurchaseInfoAPIEntity(
      preferredSource: Option[ProductSourcesRow],
      otherSources: Seq[ProductSourcesRow] = Seq(),
  ) {
    def combinedSources = preferredSource.map(ps => otherSources :+ ps).getOrElse(otherSources)
  }

  case class PreparationAPIEntity( // don't even need the full ProductsRow
      preparationInfo: SimplePreparationsRow,
      measures: Seq[ProductMeasuresRow] = Seq(),
  )

  case class UpstreamProductInfoAPIEntity(
      measureIds: Seq[Int] = Seq(), prepKeys: Seq[String] = Seq(),
  )
  implicit lazy val upstreamProductInfoApiFormat = Json.format[UpstreamProductInfoAPIEntity]

  case class TagEntity(
    id: Int, // id from the `cateogires` table
    name: String,
    upstreamId: Option[Int] = None
  )
  object TagEntity {
    def fromDB(dbRow: CategoriesRow): TagEntity =
      TagEntity(dbRow.id, dbRow.name, dbRow.syncUpstream)
  }
  implicit lazy val tagEntityAPIFormat = Json.format[TagEntity]

  case class ProductAPIEntity(
      product: ProductsRow,
      info: ProductInfosRow,
      measures: Seq[ProductMeasuresRow] = Seq(),
      purchaseInfo: Option[PurchaseInfoAPIEntity],
      upstreamInfo: Option[UpstreamProductInfoAPIEntity],
      recipe: Option[RecipeAPIEntity],
      simplePreps: Map[String, PreparationAPIEntity] = Map(),
      nutrientInfo: Seq[ProductNutrientsRow] = Seq(),
      allergens: Option[ProductAllergensRow],
      characteristics: Option[ProductCharacteristicsRow],
      ownerName: Option[String] = None,
      ingredientTags: Seq[TagEntity] = Seq[TagEntity](),
      recipeTags: Seq[TagEntity] = Seq[TagEntity](),
      inventoryTags: Seq[TagEntity] = Seq[TagEntity]()
  ) {
    /**
      *
      * @param other
      * @param allowCorrection
      * @return tuple - mergedEntity, isExpansion, isCorrection
      */
    def merge(
        other: ProductAPIEntity, allowCorrection: Boolean = true
    ): (ProductAPIEntity, Boolean, Boolean) = {
      import measuresAPIFormats.mergeProductMeasures

      // TODO: calculate these!
      var isExpansion = false
      var isCorrection = false

      /*
       * Note for all of the following: when merger function not specified,
       * values in arg of MapImplicits.merge takes precedence over those in
       * receiver
       */

      val (mergedMeasures, measureIsExpansion, measureIsCorrection) = mergeProductMeasures(this.measures, other.measures, allowCorrection)
      isExpansion |= measureIsExpansion
      isCorrection |= measureIsCorrection

      val Seq(thisRecipe, otherRecipe) = Seq(this, other).map(_.recipe)
      val mergedRecipe = (thisRecipe, otherRecipe) match {
        case (None, Some(otherR)) => otherRecipe
        case (Some(thisR), None) => thisRecipe
        case (None, None) => None
        case (Some(thisR), Some(otherR)) => {
          if (allowCorrection) {
            Some(thisR.copy(
              recipeInfo = thisR.recipeInfo.copy(
                outputAmount = otherR.recipeInfo.outputAmount,
                outputMeasure = otherR.recipeInfo.outputMeasure,
                outputUnit = otherR.recipeInfo.outputUnit,
                quantumAmount = otherR.recipeInfo.quantumAmount,
                quantumMeasure = otherR.recipeInfo.quantumMeasure,
                quantumUnit = otherR.recipeInfo.quantumUnit,
                maxBatchAmount = otherR.recipeInfo.maxBatchAmount,
                maxBatchMeasure = otherR.recipeInfo.maxBatchMeasure,
                maxBatchUnit = otherR.recipeInfo.maxBatchUnit,
                price = otherR.recipeInfo.price,
                batchSize = otherR.recipeInfo.batchSize,
                batchLabel = otherR.recipeInfo.batchLabel,
                advancePrep = otherR.recipeInfo.advancePrep,
              )
              // recipe steps are more complex and out of scope of this `merge` function
              // for more reference, see `ProductSync::expandProduct`
            ))
          } else {
            thisRecipe
          }
        }
      }

      val Seq(thisPreps, otherPreps) = Seq(this, other).map(_.simplePreps)
      if (otherPreps.keySet.diff(thisPreps.keySet).size > 0) {
        isExpansion = true
      }

      val mergedPreps = thisPreps.merge(otherPreps,
        (k, thisP, otherP) => {

          val (mergedPrepMeasures, prepMeasureIsExpansion, prepMeasureIsCorrection) = mergeProductMeasures(thisP.measures, otherP.measures, allowCorrection)
          isExpansion |= prepMeasureIsExpansion
          isCorrection |= prepMeasureIsCorrection

          if (allowCorrection) {

            var mergedPreparationInfo = thisP.preparationInfo

            if (thisP.preparationInfo != otherP.preparationInfo) {
              mergedPreparationInfo = thisP.preparationInfo.copy(
                advancePrep = otherP.preparationInfo.advancePrep,
                usesParentMeasures = otherP.preparationInfo.usesParentMeasures,
                yieldFraction = otherP.preparationInfo.yieldFraction,
                yieldMeasure = otherP.preparationInfo.yieldMeasure
              )
              isCorrection = true
            }

            thisP.copy(
              preparationInfo = mergedPreparationInfo,
              measures = mergedPrepMeasures
            )
          } else {
            thisP.copy(measures = mergedPrepMeasures)
          }
        }
      )

      val Seq(thisNutrients, otherNutrients) = Seq(this, other).map(
        _.nutrientInfo.map(n => (n.nutrient -> n)).toMap
      )
      if (otherNutrients.keySet.diff(thisNutrients.keySet).size > 0) {
        isExpansion = true
      }

      val mergedNutrients = {
        if (allowCorrection) {
          thisNutrients.merge(otherNutrients,
            (k, thisN, otherN) => {
              if (thisN != otherN) isCorrection = true
              otherN
            }
          )
        } else {
          otherNutrients.merge(thisNutrients)
        }
      }.values

      val allergens = {
        if (this.allergens.isEmpty) {
          if (other.allergens.isDefined) {
            isExpansion = true
            other.allergens
          } else {
            None
          }
        } else {
          if (other.allergens.isDefined && allowCorrection && other.allergens != this.allergens) {
            isCorrection = true
            other.allergens
          } else {
            this.allergens
          }
        }
      }

      val characteristics = {
        if (this.characteristics.isEmpty) {
          if (other.characteristics.isDefined) {
            isExpansion = true
            other.characteristics
          } else {
            None
          }
        } else {
          if (other.characteristics.isDefined && allowCorrection && other.characteristics != this.characteristics) {
            isCorrection = true
            other.characteristics
          } else {
            this.characteristics
          }
        }
      }

      val newProduct = product.copy(
        name = if (allowCorrection) other.product.name else product.name,
        nndbsrId = product.nndbsrId.orElse(other.product.nndbsrId),
        description = product.description.orElse(other.product.description),
        nutrientServingAmount = product.nutrientServingAmount.orElse(other.product.nutrientServingAmount),
        nutrientServingMeasure = product.nutrientServingMeasure.orElse(other.product.nutrientServingMeasure),
        nutrientServingUnit = product.nutrientServingUnit.orElse(other.product.nutrientServingUnit),
      )

      (copy(
        product = newProduct,
        info = other.info,
        measures = mergedMeasures,
        recipe = mergedRecipe,
        simplePreps = mergedPreps,
        nutrientInfo = mergedNutrients.toSeq,
        allergens = allergens,
        characteristics = characteristics,
      ), isExpansion, isCorrection)
    }
  }

  object productsAPIFormats {
    val mainInfoFormat: OFormat[ProductsRow] = FancyFormat[ProductsRow](productsFormat,
      mockIdFields=Seq("id", "owner"),
      omittedFields=Seq(
        "preferredSource"
      ),
      mockFields=Map(
        "hidden" -> JsBoolean(false),
        "tombstone" -> JsBoolean(false),
      ),
      defaultFields = Map(
        "lastModified" -> Json.toJson(Instant.now()), // probably going to be time at app init, but don't really care
      ),
    )

    implicit val infoFormat = FancyFormat[ProductInfosRow](productInfosFormat,
      mockIdFields=Seq("product")
    )

    implicit private val sourceAPIFormat = FancyFormat[ProductSourcesRow](productSourcesFormat,
      mockIdFields=Seq("product"),
      mockFields=Map("tombstone" -> JsBoolean(false)),
      defaultFields=Map("id" -> JsNumber(-1)))

    implicit val purchaseInfoAPIFormat = Json.using[WithDefaultValues].format[PurchaseInfoAPIEntity]

    import recipeAPIFormats.recipeAPIFormat
    import simplePrepAPIFormats.simplePrepAPIFormat
    import allergensAPIFormats.productAllergensAPIFormat
    import nutrientsAPIFormats.productNutrientInfoAPIFormat
    import characteristicsAPIFormats.productCharacteristicsAPIFormat

    implicit private val m = mainInfoFormat // reassigning this because we want it exportable, but not as implicit
    implicit private val productMeasureFormat = measuresAPIFormats.productMeasureEditableFormat

    implicit val productAPIFormat = FancyFormat[ProductAPIEntity](Json.using[WithDefaultValues].format[ProductAPIEntity],
      hoistedFields = Seq("product"),
    )

    implicit val categoriesFormat = FancyFormat(JsonConversions.categoriesFormat,
      mockIdFields = Seq("owner", "syncUpstream", "categoryType"),
    )

    val baseListFormat = OWrites[ProductsRow] { p =>
      Json.obj(
        "id" -> p.id,
        "name" -> p.name,
        "tombstone" -> p.tombstone,
      )
    }

    val recipeListFormat = OWrites[(ProductsRow, Seq[Int])] { case (p, tags) =>
      toJsObject(p)(baseListFormat) +
          ("ingredient" -> JsBoolean(p.ingredient)) +
          ("tags" -> JsArray(tags.map(JsNumber(_))))
    }

    val ingredientListFormat = OWrites[(ProductsRow, Boolean, Boolean, Boolean, Boolean, Seq[Int])] { case (p, isUsed, isPriced, isSubRecipe, manuallyCreated, tags) =>
      toJsObject(p)(baseListFormat) +
          ("isUsed" -> JsBoolean(isUsed)) +
          ("isPriced" -> JsBoolean(isPriced)) +
          ("isSubRecipe" -> JsBoolean(isSubRecipe)) +
          ("manuallyCreated" -> JsBoolean(manuallyCreated)) +
          ("tags" -> JsArray(tags.map(JsNumber(_))))
    }

    val menuItemListFormat = OWrites[(ProductsRow, Seq[Int])] { case (p, tags) =>
      toJsObject(p, tags)(recipeListFormat) + ("description" -> Json.toJson(p.description))
    }

    val recipeListAPIEntityFormat = OWrites[(ProductsRow, Seq[Int], Option[String], Option[Instant], Option[Instant])] {
      case (p, tags, itemNumber, createdAt, lastModified) =>
        toJsObject(p, tags)(recipeListFormat) +
        ("itemNumber" -> Json.toJson(itemNumber)) +
        ("createdAt" -> Json.toJson(createdAt)) +
        ("lastModified" -> Json.toJson(lastModified))
    }

    implicit val recipeAddedChangedAPIEntityFormat = OWrites[RecipeAddedChanged] {
      case recipeAddedChanged =>
        toJsObject(recipeAddedChanged.product)(baseListFormat) +
        ("date" -> Json.toJson(recipeAddedChanged.date)) +
        ("by" -> Json.toJson(recipeAddedChanged.ownerName)) +
        ("type" -> Json.toJson(recipeAddedChanged.event))
    }

    def concatOption[A](a: Option[A], b: Option[A] )(conc: (A, A) => A): Option[A] = (a, b) match {
      case (Some(sa), Some(sb)) => Some(conc(sa, sb))
      case (Some(sa), None) => Some(sa)
      case (None, Some(sb)) => Some(sb)
      case _ => None
    }

    implicit  val posImportPrefenceFormat = FancyFormat[PosImportPreferencesRow](posPreferenceFormat,
      mockIdFields=Seq("owner")
    )

    case class RecentPriceChange(productId: Int, percentage: BigDecimal, date: Option[Instant])
    implicit val recentPriceChangeFormat = Json.format[RecentPriceChange]

  }

  case class RecipeAddedChanged(
      product: ProductsRow,
      date: Instant,
      ownerName: Option[String],
      event: String
  )

  // An entry in the ingredient list in Nutrition Info in RecipeEdit
  case class IngredientWithWeight(
      id: Int,
      name: String,
      weight: BigDecimal
  )
  implicit lazy val ingredientWithWeightFormat = Json.format[IngredientWithWeight]

  case class ProductNutritionInfo(
      nutritions: Map[String, BigDecimal],
      ingredientList: Seq[IngredientWithWeight], // For recipe only
      allergenList: Map[String, String],
      characteristicList: Seq[String],
      nonEdible: Boolean,
      // For sub-recipes only
      recipeSize: Option[Quantity]
  )
  implicit lazy val productNutritionInfoFormat = Json.format[ProductNutritionInfo]

  case class IngredientConsumerAPIEntity(
      main: ProductsRow,
      measures: Seq[ProductMeasuresRow],

      ingredientTags: Seq[TagEntity],
      recipeTags: Seq[TagEntity],
      inventoryTags: Seq[TagEntity],

      // TODO: add source details?
      preferredSource: Option[Int],
      inHouse: Boolean,

      // if this product *has* preps - map to preparation IDs
      // if one is chosen, then we can fetch the full info
      simplePreps: Map[String, Int],

      // is this product *is* a simple prep, backreference to original, mostly for convenience of UI code
      isPrepOf: Option[Int],

      // these first two are redundant with measures, but want to be extra-sure client has right unit for unitCost
      primaryMeasure: Option[Int],
      primaryUnit: Option[Int],
      uncostedIngredients: Seq[Int], // for display editing links to the user
      unitCost: Option[BigDecimal],
      portionPrice: Option[BigDecimal],

      nutritionInfo: ProductNutritionInfo,

      // the user-defined nutrient serving amount
      dbNutrientServingAmount: Option[Double] = None,
      itemNumber: Option[String],
  )

  object ingredientConsumerAPIFormats {

    import productsAPIFormats.{mainInfoFormat => fatMainInfoFormat}
    implicit val productMeasureFormat = measuresAPIFormats.productMeasureReadOnlyFormat

    val mainInfoWrite = fatMainInfoFormat.transform { v: JsValue =>
      v match {
        case o: JsObject =>
          JsObject(o.value.filterKeys(Set(
            "name",
            "nutrientServingAmount", "nutrientServingMeasure", "nutrientServingUnit"
          )))
        case _ => throw new Exception(
          "productsAPIFormats.mainInfoWrite: OWrites did not produce a JsObject")
      }
    }
    // making a full Format not because we're actually going to read this, but
    // so we can use the fancy helpers
    implicit val mainInfoFormat = OFormat(fatMainInfoFormat, mainInfoWrite)
    // in here so it sees the above custom mainInfoWrite
    private val rawIngredientConsumerAPIEntityFormat = Json.format[IngredientConsumerAPIEntity]
    implicit lazy val ingredientConsumerAPIWrite = FancyFormat(rawIngredientConsumerAPIEntityFormat,
      hoistedFields = Seq("main"),
    )
  }

  case class MenuAPIEntity(
      main: MenusRow,
      sections: Seq[MenuSectionAPIEntity] = Seq(),
  )

  case class MenuSectionAPIEntity(
      main: MenuSectionsRow,
      items: Seq[MenuItemsRow] = Seq(),
  )

  object menusAPIFormats {
    implicit private val mainInfoFormat = FancyFormat[MenusRow](menusFormat,
      mockIdFields=Seq("id", "owner"),
      mockFields=Map("userVisible" -> JsBoolean(true))
    )

    implicit private val sectionFormat = FancyFormat[MenuSectionsRow](menuSectionsFormat,
      mockIdFields=Seq("id", "menu", "displayOrder", "owner")
    )

    implicit private val itemFormat = FancyFormat[MenuItemsRow](menuItemsFormat,
      mockIdFields=Seq("id", "menu", "section", "displayOrder", "owner")
    )

    implicit private val sectionAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[MenuSectionAPIEntity],
      hoistedFields = Seq("main"),
    )

    implicit lazy val menuAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[MenuAPIEntity],
      hoistedFields = Seq("main"),
    )
  }

  case class TransactionDeltaEntity(
    main: TransactionDeltasRow,
    measures: Seq[TransactionMeasuresRow] = Seq(),
  )
  case class TransactionAPIEntity(
      main: TransactionsRow,
      deltas: Seq[TransactionDeltaEntity] = Seq(),
  )

  case class TransactionDeltasDateAPIEntity(
     date: String,
     delta: TransactionDeltaEntity
 )

  object transactionsAPIFormats {
    implicit val mainInfoFormat = FancyFormat[TransactionsRow](transactionsFormat,
      mockIdFields = Seq("id", "owner", "finalizedXact"),
      mockFields = Map("xactType" -> JsString("customer-order")), // had to pick *a* valid value
    )

    import quantityAPIFormats._
    private val deltaBaseFormat = FancyFormat[TransactionDeltasRow](transactionDeltasFormat,
      mockIdFields = Seq("id", "owner", "xact")
    )
    val deltaRead = quantityReadTransformer(__ \ "quantity", None, required=true)
        .andThen(deltaBaseFormat)
    val deltaWrite = deltaBaseFormat
        .transform(quantityWriteTransformer("quantity", None, _))
    implicit private[JsonConversions] val deltaFormat = OFormat(deltaRead, deltaWrite)

    implicit private val measuresFormat = transactionMeasuresFormat

    implicit private val deltaEntityFormat = FancyFormat(Json.format[TransactionDeltaEntity],
      readOnlyFields = Map("measures" -> JsArray()),
      hoistedFields = Seq("main"),
    )

    private val unvalidatedTransactionAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[TransactionAPIEntity],
      hoistedFields = Seq("main")
    )

    def transactionAPIFormat(xactType: TransactionType.Value) = OFormat(
      unvalidatedTransactionAPIFormat.map { case TransactionAPIEntity(main, deltas) =>
          TransactionAPIEntity(main.copy(xactType = xactType), deltas)
      },
      unvalidatedTransactionAPIFormat,
    )

    implicit val transactionDeltaAPIEntityFormat = OWrites[TransactionDeltasDateAPIEntity] {
      case tdd =>
        toJsObject(tdd.delta)(deltaEntityFormat) +
        ("date" -> Json.toJson(tdd.date))
    }
  }

  case class PurchaseOrderAPIEntity(
    main: PurchaseOrdersRow,
    transaction: TransactionAPIEntity,
    orders: Seq[Int] = Seq(),
    confirmedTransaction: Option[TransactionAPIEntity] = None,
  )

  object purchaseOrdersAPIFormats {
    import transactionsAPIFormats.transactionAPIFormat

    implicit private val mainInfoFormat = FancyFormat[PurchaseOrdersRow](purchaseOrdersFormat,
      mockIdFields=Seq("id", "owner", "xact", "createdBy"),
      mockFields=Map("receivedBy" -> JsNull)
    )
    implicit private val purchaseTransactionFormat = transactionAPIFormat(TransactionType.PurchaseOrder)

    implicit lazy val purchaseOrdersAPIFormat = FancyFormat(Json.using[WithDefaultValues].format[PurchaseOrderAPIEntity],
      hoistedFields = Seq("main"),
    )
  }

  case class ReceivePurchaseOrderAPIEntity(
    supplier: Int,
    transaction: ReceiveTransactionAPIEntity
  )
  case class ReceiveTransactionAPIEntity(
    main: TransactionsRow,
    deltas: Seq[ReceiveTransactionDeltaEntity] = Seq(),
  )
  case class ReceiveTransactionDeltaEntity(
    unitCost: BigDecimal,
    row: TransactionDeltasRow
  )

  object receivePurchaseOrderAPIFormats {

    implicit def toTransactionApiEntity(entity: ReceiveTransactionAPIEntity) = TransactionAPIEntity(
      entity.main,
      entity.deltas.map(d => TransactionDeltaEntity(d.row, Seq.empty)),
    )

    import transactionsAPIFormats.deltaFormat
    implicit lazy val receiveTransactionDeltaRead = FancyFormat(Json.format[ReceiveTransactionDeltaEntity],
      hoistedFields = Seq("row"),
    )

    private val transactionRead = transactionsAPIFormats.mainInfoFormat
        .map(_.copy(xactType = TransactionType.PurchaseOrder))
    implicit private val transactionFormat = OFormat(
      transactionRead,
      transactionsAPIFormats.mainInfoFormat,
    )

    implicit private val receiveTransactionAPIReads = FancyFormat(Json.using[WithDefaultValues].format[ReceiveTransactionAPIEntity],
      hoistedFields = Seq("main"),
    )

    implicit lazy val receivePurchaseOrderAPIReads = Json.format[ReceivePurchaseOrderAPIEntity]
  }

  case class OrderAPIEntity(
      main: OrdersRow,
      transaction: TransactionAPIEntity,
      usedMenu: Either[MenuAPIEntity,Int] // MenuAPIEntity for custom menu, int for existing menu
  )

  object ordersAPIFormats {
    import menusAPIFormats.menuAPIFormat
    import transactionsAPIFormats.transactionAPIFormat

    implicit private val mainInfoFormat = FancyFormat[OrdersRow](ordersFormat,
      mockIdFields=Seq("id", "owner", "menu", "xact")
    )
    implicit private val orderTransactionFormat = transactionAPIFormat(TransactionType.CustomerOrder)

    implicit private val menuFormat = FancyFormat.either[MenuAPIEntity, Int]

    implicit lazy val ordersAPIFormat = FancyFormat(Json.format[OrderAPIEntity],
      hoistedFields = Seq("main"),
    )
  }

  case class NewInventoryItemAPIEntity(
    id: Int, // Set this to the corresponding ingredient's id to fit what `SearchableList.jsx` wants
    product: Int,
    amount: Option[Double],
    measure: Option[Int],
    unit: Option[Int],
    packaged: Boolean,
    packageName: Option[String],
    subPackageName: Option[String],
    packageSize: Option[Double],
    superPackageSize: Option[Double],
    supplier: Option[Int],
    supplierName: Option[String],
    name: Option[String],
    tags: Seq[Int] = Seq(),  // id from the `categories` table
    // for advanced preps
    yieldFraction: Option[Double] = None,
    preferredSource: Option[ProductSourcesRow] = None,
  )

  case class InventoryAPIEntity(
    id: Int,
    date: LocalDate,
    name: Option[String],
    time: Instant,
    items: Seq[InventoryItemsRow] = Seq(),
  )

  case class CurrentInventoryItem(
    product: Int,
    name: Option[String],
    originalIngredientName: Option[String], // for advance preps
    measures: Seq[ProductMeasuresRow] = Seq(),
    quantity: Quantity,
    packaged: Boolean,
    packageName: Option[String],
    subPackageName: Option[String],
    packageSize: Double,
    superPackageSize: Double,
  )

  case class CurrentInventory(
    items: Seq[CurrentInventoryItem] = Seq(),
  )

  object inventoryAPIFormats {

    implicit private val inventoryItemsFormat = JsonConversions.inventoryItemsFormat
    implicit private val productSourcesFormat = JsonConversions.productSourcesFormat
    implicit private val productMeasureFormat = measuresAPIFormats.productMeasureReadOnlyFormat

    implicit lazy val inventoryFormat = Json.format[InventoryAPIEntity]

    implicit lazy val currentInventoryItemFormat = Json.format[CurrentInventoryItem]
    implicit lazy val currentInventoryFormat = Json.format[CurrentInventory]

    implicit lazy val inventoryItemAPIFormat = FancyFormat[InventoryItemsRow](inventoryItemsFormat,
      mockIdFields=Seq("id", "owner", "inventory"))

    implicit lazy val newInventoryItemAPIFormat = Json.format[NewInventoryItemAPIEntity]

    def ingredientToNewInventoryItem(
      i: ProductsRow,
      tags: Seq[Int]  // id from the `categories` table
    ) = NewInventoryItemAPIEntity(
      i.id,
      i.id,
      None, None, None,
      false, None, None, None, None,
      None, None,
      Some(i.name),
      tags
    )

    def advancedPrepToNewInventoryItem(
      p: SimplePreparationsRow,
      name: String,
      tags: Seq[Int]  // id from the `categories` table
    ) = NewInventoryItemAPIEntity(
      p.outputProduct,
      p.outputProduct,
      None, Some(p.yieldMeasure), None,
      false, None, None, None, None,
      None, None,
      Some(name),
      tags,
    )

    def inventoryItemToNewInventoryItem(
      continueTodayInventory: Boolean,
      i: InventoryItemsRow,
      name: String,
      tags: Seq[Int],  // id from the `categories` table
    ) = NewInventoryItemAPIEntity(
      i.product,
      i.product,
      if (continueTodayInventory) i.amount else None,
      i.measure, i.unit,
      i.packaged, i.packageName, i.subPackageName, Some(i.packageSize), Some(i.superPackageSize),
      i.supplier,
      i.supplierName,
      Some(name),
      tags
    )

    def ingredientParToNewInventoryItem(
      p: IngredientParsRow,
      name: String,
      tags: Seq[Int],  // id from the `categories` table
    ) = NewInventoryItemAPIEntity(
      p.product,
      p.product,
      p.amount,
      p.measure, p.unit,
      p.packaged, p.packageName, p.subPackageName, Some(p.packageSize), Some(p.superPackageSize),
      p.supplier,
      p.supplierName,
      Some(name),
      tags
    )
  }

  object invitesAPIFormats {
    implicit val upstreamInvitesFormat = FancyFormat(JsonConversions.upstreamInvitesFormat,
      mockIdFields = Seq("id", "invitee", "inviter"), // basically the whole row
    )
    implicit val userSharedInvitesFormat = FancyFormat(JsonConversions.userSharedInvitesFormat,
      mockIdFields = Seq("id", "inviter"),
    )
    implicit val userAccountsFormat = FancyFormat(JsonConversions.userAccountsFormat,
      mockIdFields = Seq("id", "owner"),
    )
  }

  case class OnboardingWidgetsVisited(widgetId: Int)
  implicit lazy val OnboardingWidgetsVisitedFormat = Json.format[OnboardingWidgetsVisited]

  case class OnboardingWidgetsApiEntity(
    visitedWidgetsKey: Seq[Int],
    widgets: Seq[OnboardingWidgetsRow]
  )
  object onboardingWidgetApiFormats {
    implicit val widgetFormat = onboardingWidgetsFormat
    implicit val onboardingWidgetFormat = Json.format[OnboardingWidgetsApiEntity]
  }


  case class PricesHistoryAPIEntity(
    priceInfo: IngredientPriceHistoryRow,
    measures: Seq[IngredientPriceHistoryMeasuresRow]
  )

  object ingredientPriceHistoryAPIFormats {
    implicit private val changeTypeFormat = FancyFormat.enum(ChangeType)
    implicit private val ingredientPriceHistoryFormat = Json.format[IngredientPriceHistoryRow]
    implicit private val ingredientPriceHistoryMeasureFormat = Json.format[IngredientPriceHistoryMeasuresRow]
    implicit val ingredientPriceHistoryDateAPIEntityFormat = Json.format[PricesHistoryAPIEntity]
  }
}
