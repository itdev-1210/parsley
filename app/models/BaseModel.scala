package models

abstract case class BaseModel[RowType](val dbRow: RowType)

