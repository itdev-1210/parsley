package controllers

import controllers.api.{ApiController, JsonRestUtils}
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.{BillingUtils, EmailUtils, ModelUtils, ParsleyEnvironment}
import util.Exceptions._

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.http.Status._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsResultException, JsString, Json}
import play.api.mvc._
import play.api.mvc.Results._
import play.api.{Configuration, Logger}

import com.github.ghik.silencer.silent
import com.google.gson.JsonSyntaxException
import com.google.inject.Inject
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.DefaultSecuredErrorHandler
import com.newrelic.api.agent.NewRelic
import com.sendgrid.Email
import com.stripe.exception.{CardException, InvalidRequestException => StripeInvalidRequestException}
import com.stripe.model.{Customer, Event, Invoice, StripeObject, Subscription, SubscriptionItem, Card => StripeCard, Coupon => StripeCoupon, Plan => StripePlan, Product => StripeProduct}
import com.stripe.net.ApiResource

import java.io.IOException
import java.time.{Instant, ZonedDateTime}
import java.util.NoSuchElementException

import scala.collection.JavaConverters._
import scala.concurrent.{Future, blocking}
import scala.reflect.ClassTag
import scala.util.{Failure, Try}

class Plan @Inject() (
    override val controllerComponents: ControllerComponents,

    val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider,
    val config: Configuration,
    val emailUtils: EmailUtils,
    val modelUtils: ModelUtils,
    val billingUtils: BillingUtils,

    val planView: views.html.Plan,
) extends ApiController
    with HasDatabaseConfigProvider[models.Tables.profile.type] with I18nSupport
    with JsonRestUtils with AuthUtils
{

  private val emptyStripeParams = Map[String, Object]()

  val userVisibleErrorHandler = new DefaultSecuredErrorHandler(messagesApi) {
    override def onNotAuthenticated(implicit request: RequestHeader) = {
      Future.successful(Redirect(routes.Auth.signUpPage(Some(request.uri))))
    }
  }

  import dbConfig.profile.api._

  def subscriptionCheckEndpoint(
      subscribedResponse: => Result = Redirect(routes.Application.main(path = "")),
      reactivateResponse: => Result = Redirect(routes.Plan.activate()),
      newSubscriberResponse: => Result = Redirect(routes.Plan.main()),
      newUpstreamSubscriberResponse: => Result = Redirect(routes.Plan.upstreamActivate())
  )(implicit r: SecuredRequest[AnyContent]) = {
    db.runInTransaction {
      for {
        hasPastSubscriptions <- r.accountQuery.subscriptionHistory.exists.result
        isSubscribed <- r.accountQuery.currentSubscription.exists.result
        isUpstreamInvited <- UpstreamInvites.filter(_.invitee === r.accountId).exists.result
        _ <- r.accountQuery.map(_.lastLogin)
            .update(Some(Instant.now))
        _ <- if (isSubscribed) {
          r.accountQuery.map(_.lastUsage)
              .update(Some(Instant.now))
        } else DBIO.successful(Unit)
      } yield {
        if (isSubscribed) {
          subscribedResponse
        } else if (hasPastSubscriptions) {
          reactivateResponse
        } else if (!hasPastSubscriptions && isUpstreamInvited) {
          newUpstreamSubscriberResponse
        } else {
          newSubscriberResponse
        }
      }
    }
  }

  // path is ignored; it's consumed by the browser-side parts of the application
  def main(path: String) = SecuredAction(userVisibleErrorHandler).async { implicit r =>
    subscriptionCheckEndpoint(newSubscriberResponse = Ok(planView()))
  }

  def activate() = SecuredAction(userVisibleErrorHandler).async { implicit r =>
    subscriptionCheckEndpoint(reactivateResponse = Ok(planView()))
  }

  def upstreamActivate() = SecuredAction(userVisibleErrorHandler).async { implicit r =>
    subscriptionCheckEndpoint(newUpstreamSubscriberResponse = Ok(planView()))
  }

  private case class Plan(
      id: String,
      // pulled from metadata, not Stripe 'active' field, since we may want to
      // put new users on these plans manually sometimes
      active: Boolean,
      price: Long,
      trialPeriodDays: Option[Long],
  )
  private implicit val plansResponseItemFormat = Json.format[Plan]
  private object Plan {
    def fromStripe(plan: StripePlan): Plan = {
      val metadata = plan.getMetadata.asScala
      Plan(
        plan.getId,
        active = plan.getActive && metadata.contains("active"),
        price = plan.getAmount,
        // having trialPeriodDays attached to a Plan instead of Subscription is deprecated, but we still do it
        trialPeriodDays = Option(plan.getTrialPeriodDays: @silent),
      )
    }
  }

  private case class Product(
      id: String,
      subscriptionLevel: String, // chef/business/etc.
      contactUs: Boolean,
      active: Boolean,
      name: String,
      description: Option[String],
      plans: Seq[Plan]
  )
  private implicit val productsResponseItemFormat = Json.format[Product]
  private object Product {
    def fromStripe(product: StripeProduct, plans: Seq[StripePlan]): Product = {
      val metadata = product.getMetadata.asScala
      Product(
        product.getId,
        metadata.getOrElse("level", "chef"),
        metadata.contains("contact_us"),
        product.getActive && metadata.contains("active"),
        // some absolute essentials to correctly use a product
        metadata.getOrElse("display_name", product.getName),
        metadata.get("description"),
        plans.map(Plan.fromStripe(_)),
      )
    }
  }

  def getStripeProducts() = SecuredApiAction.async { implicit r =>
    for {
      stripeApiProducts <- blockingOp(StripeProduct.list(emptyStripeParams.asJava))
      stripeConcreteProducts <- blockingOp {
        stripeApiProducts.autoPagingIterable.asScala.toSeq
      }
      stripeApiPlans <- blockingOp(StripePlan.list(emptyStripeParams.asJava))
      stripeConcretePlans <- blockingOp {
        stripeApiPlans.autoPagingIterable.asScala.toSeq
      }
    } yield {
      val productPlans = stripeConcretePlans.groupBy(_.getProduct)
      val responseItems = stripeConcreteProducts.sortBy { p =>
        // if we get an NPE at any point here, put the plan last in line
        Try(p.getMetadata.asScala.getOrElse("display_order", "").toInt)
            .getOrElse(1000)
      }.map { product =>
        Product.fromStripe(product, productPlans.getOrElse(product.getId, Seq()))
      }
      Ok(Json.toJson(responseItems))
    }
  }

  def getStripeCoupons() = SecuredApiAction.async { implicit r =>
    var couponParams = emptyStripeParams + ("limit" -> new Integer(100))

    for {
      coupons <- blockingOp(StripeCoupon.list(couponParams.asJava))
    } yield Ok(coupons.toJson).as("application/json")
  }

  def getStripePublishableKey() = SecuredApiAction { implicit r =>
    val stripePublishableKey = config.get[String]("stripe.publishableKey")
    Ok("{\"data\": \"" + stripePublishableKey + "\"}").as("application/json")
  }

  private case class Card(
      brand: String,
      last4: String,
  )
  implicit private val stripeCardFormat = Json.format[Card]
  private case class Coupon(
      percentOff: Option[Long],
      centsOff: Option[Long],
  )
  implicit private val stripeCouponFormat = Json.format[Coupon]
  private case class StripeSummary(
      accountBalance: Long,
      card: Option[Card],
      coupon: Option[Coupon],
      // in the summary, the Product object contains only one Plan, the active one
      activeProduct: Option[Product],
      periodEnd: Option[Instant],
      isCancelled: Boolean,
      isTrialing: Boolean,
  )
  implicit private val stripeSummaryFormat = Json.format[StripeSummary]
  def getStripeSummary() = SecuredApiAction.async { implicit r =>

    db.runInTransaction {
      for {
        account <- r.accountQuery.result.head
        customerId <- failIfNone(account.stripeCustomerId,
          HttpCodeException(NotFound, "account has no associated Stripe customer")
        ).toDBIO
        customer <- blockingOp(Customer.retrieve(
          customerId,
          Map[String, AnyRef]("expand" -> Seq("default_source").asJava).asJava,
          null,
        )).toDBIO
        card = Option(customer.getDefaultSourceObject).map {
          case c: StripeCard => c
        }
        allSubscriptions <- blockingOp(
          customer.getSubscriptions.autoPagingIterable.asScala.toSeq
        ).toDBIO
        // Parsley happens to only keep one Stripe subscription open at a time per customer
        subscription = allSubscriptions.headOption
        product <- blockingOp(subscription.map(
          s => StripeProduct.retrieve(s.getPlan.getProduct)
        )).toDBIO

      } yield {
        val coupon = subscription.flatMap(s => Option(s.getDiscount)).map(_.getCoupon)

        val summary = StripeSummary(
          customer.getAccountBalance,
          card.map(stripeCard => Card(stripeCard.getBrand, stripeCard.getLast4)),
          coupon.map(stripeCoupon => Coupon(
            Option(stripeCoupon.getPercentOff),
            Option(stripeCoupon.getAmountOff),
          )),
          product.map(p => Product.fromStripe(p, subscription.map(_.getPlan).toSeq)),
          subscription.map(s => Instant.ofEpochSecond(s.getCurrentPeriodEnd)),
          isCancelled = subscription
              .exists(s => Boolean.unbox(s.getCancelAtPeriodEnd)),
          isTrialing = subscription
              .exists(s => s.getStatus == "trialing")
        )
        Ok(Json.toJson(summary))
      }
    }
  }

  def receiveStripeWebhooks() = ApiAction.async(parse.json) { implicit request =>
    val applicationUrl = config.get[String]("applicationUrl")
    val env = ParsleyEnvironment.getFrom(config)

    val from = new Email("service@parsleycooks.com", "Service at Parsley Software")

    def castEventTo[O <: StripeObject](e: Event)(implicit ct: ClassTag[O]): O = {
      e.getData.getObject match {
        case rightType: O => rightType
        case wrongType => throw HttpCodeException(BadRequest,
          s"expected ${ct.toString} for event type ${e.getType}" +
              s" got ${wrongType.getClass.getName}")
      }
    }

    db.runInTransaction {
      for {
        eventId <- Try((request.body \ "id").as[String]).toDBIO // JsResultException will make BadRequest, not InternalServerError
        event <- if (env != ParsleyEnvironment.DEV) {
          blockingOp(
            Event.retrieve(eventId)
          ).toDBIO.recoverWith {
            // if just asking for the event is an error, we should blame the request
            case e: StripeInvalidRequestException => DBIO.failed(HttpCodeException(BadRequest, s"${e.getMessage} (code: ${e.getStatusCode})"))
          }
        } else { // local devel setup
          Try {
            // JSON-parsing code from LiveStripeResponseGetter
            ApiResource.GSON.fromJson[Event](request.body.toString, classOf[Event])
          }.recoverWith {
            case e: JsonSyntaxException => Failure(
              HttpCodeException(BadRequest, e.getMessage))
          }.toDBIO
        }
        maybeEmailInfo <- event.getType() match {
          case "invoice.payment_succeeded" => {
            // we update the subscription record in our database
            val invoice = castEventTo[Invoice](event)
            val subscriptionId = invoice.getSubscription
            val subscriptionQuery = Subscriptions.filter(s => !s.cancelled && s.stripeSubscriptionId === subscriptionId)
            val newEndTime = ZonedDateTime.now.plusMonths(1).toInstant
            subscriptionQuery.map(s => (s.planValidUntil, s.pastDue))
                .update((newEndTime, false))
                .map(_ => None)
          }
          case "invoice.payment_failed" => {
            // we send a notification email to our user
            val invoice = castEventTo[Invoice](event)
            val customerId = invoice.getCustomer
            val accountQuery = Accounts.filter(_.stripeCustomerId === customerId)
            val subscriptionId = invoice.getSubscription

            val subscriptionQuery = accountQuery.subscriptionHistory
                // make sure we have matching information
                .filter(_.stripeSubscriptionId === subscriptionId)

            val firstTry = (invoice.getAttemptCount == 1)

            if (!firstTry) {
              // make sure that users who didn't get marked as pastDue on first
              // failure (e.g. those with no card entered) are so marked now
              subscriptionQuery.map(_.pastDue).update(true)
                  .map(_ => None)
            } else {
              for {
                maybeAccount <- accountQuery.result.headOption
                account <- failIfNone(maybeAccount,
                  HttpCodeException(BadRequest, s"no account for stripe customer $customerId")
                ).toDBIO
                owner <- accountQuery.owner.result.headOption
                email <- failIfNone(owner.flatMap(_.email),
                  HttpCodeException(InternalServerError, "account has no e-mail address")
                ).toDBIO

                stripeCustomer <- blockingOp(Customer.retrieve(customerId)).toDBIO
                stripeSubscription <- blockingOp(Subscription.retrieve(subscriptionId)).toDBIO

                hasLocalMatch <- subscriptionQuery.exists.result
                _ <- failIf(!hasLocalMatch,
                  HttpCodeException(BadRequest, s"no local subscription found for stripe ID $subscriptionId")
                ).toDBIO

                hasPaymentMethod = Option(stripeCustomer.getDefaultSource).isDefined
                // period for which the invoice is charging, not for current (post-invoice) period
                periodStart = invoice.getPeriodStart
                isTrialPeriodEnd = (periodStart == stripeSubscription.getCreated)
                trialGracePeriod = (!hasPaymentMethod && isTrialPeriodEnd)

                // let new users (those with no card entered) have until their
                // second payment failure. see !firstTry case above
                _ <- if (trialGracePeriod) {
                  subscriptionQuery.map(_.pastDue).update(true)
                } else DBIO.successful(Unit)
              } yield {
                val firstName = owner.flatMap(_.firstName).getOrElse("Subscriber")

                val emailText = if (trialGracePeriod) {
                  s"""
                  <p>Dear $firstName,</p>
                  <p></p>
                  <p>Your free trial period for Parsley has ended. We do not have
                  a credit card on file for you;
                  if you would like to continue using Parsley, please log in at
                  <a href="$applicationUrl" target='_blank'>$applicationUrl</a>
                  and add your credit card information by clicking Account
                  Settings at the top right.
                  If you have any questions or need additional information, please
                  contact us at service@parsleycooks.com
                  </p>
                  <p></p>
                  <p>Thank you!</p>
                  <p></p>
                  <p>The Parsley Team</p>"""
                } else {
                  s"""
                  <p>Dear $firstName,</p>
                  <p></p>
                  <p>There was an error processing your Parsley payment today.
                  To avoid an interruption in service, please log in at
                  <a href="$applicationUrl" target='_blank'>$applicationUrl</a>
                  and check your payment details by clicking Account Settings
                  at the top right.</p>
                  <p></p>
                  <p>Thank you!</p>
                  <p></p>
                  <p>The Parsley Team</p>"""
                }
                Some((new Email(email), "Billing for your subscription failed", emailText))
              }
            }
          }
          case "customer.subscription.deleted" => { // despite the name, just means subscription has ended
            // TODO: a farewell e-mail?
            val subscription = castEventTo[Subscription](event)

            val subscriptionId = subscription.getId
            val subscriptionQuery = Subscriptions.filter(_.stripeSubscriptionId === subscriptionId)

            for {
              // usually optional, but should be set for ended subscriptions
              endTime <- failIfNone(Option(subscription.getEndedAt),
                HttpCodeException(BadRequest, "customer.subscription.deleted webhook called with no endTime")
              ).toDBIO
              numUpdated <- subscriptionQuery
                  .map(s => (s.cancelled, s.planValidUntil))
                  .update((true, Instant.ofEpochSecond(endTime)))
              _ <- subscriptionQuery.account.map(_.subscriptionLevel).update(None)
            } yield {
              if (numUpdated > 0) None
              else throw new HttpCodeException(BadRequest, s"no subscription under the id $subscriptionId")
            }
          }
          case "customer.subscription.updated" => { // make sure to sync up changes made in Stripe to our DB
            // in the common case, changes made here will have already been applied in our endpoints; so this must
            // be idempotent
            val subscription = castEventTo[Subscription](event)
            val subscriptionId = subscription.getId
            val subscriptionQuery = Subscriptions.filter(_.stripeSubscriptionId === subscriptionId)

            // periodEnd reflects the end of the period last *invoiced*, and so includes unpaid months. so don't use it
            // val periodEnd = Option(subscription.getCurrentPeriodEnd)
            val endTime = Option(subscription.getEndedAt)
            // includes subscriptions that *will* be cancelled at the end of the month
            val cancelled = subscription.getCancelAtPeriodEnd || endTime.isDefined

            // will be false if subscription has been cancelled due to non-payment,
            // but who cares about that case
            val pastDue = Set("past_due", "unpaid").contains(subscription.getStatus)

            val plan = subscription.getPlan.getId

            for {
              _ <- subscriptionQuery.map(_.cancelled).update(cancelled)
              product <- blockingOp(StripeProduct.retrieve(subscription.getPlan.getProduct)).toDBIO
              // just using the conversion code to grab subscription level
              subscriptionLevel = Product.fromStripe(product, Seq()).subscriptionLevel

              _ <- if (!pastDue) {
                // so that we reactivate past-due accounts if they pay up
                subscriptionQuery.map(_.pastDue).update(false)

              // if payment fails, let invoice.payment_failed handle it; that
              // handler is aware of when to give the customer a grace period
              // and when not to
              } else DBIO.successful(Unit)
              _ <- subscriptionQuery.map(s => (s.stripePlanId, s.subscriptionLevel))
                  .update((plan, subscriptionLevel))
              _ <- subscriptionQuery.account.map(u => u.subscriptionLevel)
                  .update(Some(subscriptionLevel))
            } yield None
          }
          case "customer.subscription.trial_will_end" => {
            // we send a notification email to our user
            val subscription = castEventTo[Subscription](event)
            val customerId = subscription.getCustomer
            val accountQuery = Accounts.filter(_.stripeCustomerId === customerId)
            for {
              maybeAccount <- accountQuery.result.headOption
              account <- failIfNone(maybeAccount,
                HttpCodeException(BadRequest, s"no account for stripe customer $customerId")
              ).toDBIO
              owner <- accountQuery.owner.result.headOption
              email <- failIfNone(owner.flatMap(_.email),
                HttpCodeException(InternalServerError, "account has no e-mail address")
              ).toDBIO

              stripeCustomer <- blockingOp(Customer.retrieve(customerId)).toDBIO
            } yield {
              val hasPaymentMethod = Option(stripeCustomer.getDefaultSource).isDefined
              val hasMonthlyPayment = {
                val plan = subscription.getPlan
                val coupon = Option(subscription.getDiscount).map(_.getCoupon)
                val amountOff = coupon.flatMap(c => Option(c.getAmountOff))
                val percentOff = coupon.flatMap(c => Option(c.getPercentOff))
                !amountOff.exists(_ >= plan.getAmount) && !percentOff.exists(_ >= 100)
              }

              if (hasPaymentMethod || !hasMonthlyPayment) {
                // customer's all good, no action required; they'll get their
                // receipt
                None
              } else {
                val firstName = owner.flatMap(_.firstName).getOrElse("Subscriber")

                val emailText = s"""<p>Dear $firstName,</p>
                <p></p>
                <p>Your free trial for Parsley is about to expire in 3 days.
                To avoid an interruption in service, please log in at
                <a href="$applicationUrl" target='_blank'>$applicationUrl</a>
                and update your payment details by clicking Account Settings at the top right.</p>
                <p></p>
                <p>Thank you!</p>
                <p></p>
                <p>The Parsley Team</p>"""

                Some((new Email(email), "Your Parsley trial period is ending soon", emailText))
              }
            }
          }
          case unknownType => {
            DBIO.failed(HttpCodeException(BadRequest, s"unrecognized event type: $unknownType"))
          }
        }
        _ <- maybeEmailInfo match {
          case Some((to, subject, content)) =>
            emailUtils.send(to, from, subject, content, bcc=Seq(from)).toDBIO
          case None => DBIO.successful(Unit)
        }
      } yield Ok(JsString("success"))
    }.recoverWith {
      // by design, handleAPIError doesn't log certain errors that are the client's fault, both because people may be
      // experimenting with our private API, and because we log all actual buggy errors in NewRelic on the browser side.
      // For Stripe issues, though, we want to know exactly what happened
      // Purely side-effect, though! not changing the value of the Future
      case httpError: HttpCodeException =>
        Logger.error("Stripe webhook client error")
        Logger.error(httpError.getMessage)
        NewRelic.noticeError(httpError, Map("notes" -> "in stripe webhook").asJava)
        Future.failed(httpError);
      case parseError: JsResultException =>
        Logger.error("Stripe webhook client error (JsResultException)")
        Logger.error(parseError.getMessage)
        Logger.error(parseError.getStackTrace.mkString("\n"))
        NewRelic.noticeError(parseError, Map("notes" -> "in stripe webhook").asJava)
        Future.failed(parseError)
    }
  }


  // TODO: Only used to accept terms; rename/rework
  private case class UserUpdateRequest(
      acceptsTerms: Option[Boolean],
  )
  implicit private val userUpdateReqReads = Json.reads[UserUpdateRequest]

  def updateUser = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        updateReq <- Try(r.body.as[UserUpdateRequest]).toDBIO

        _ <- if (updateReq.acceptsTerms.exists(identity)) { // exists(identity) => exists and is true
          r.accountQuery.map(a => a.acceptsTerms).update(true)
        } else DBIO.successful(Unit) // never *clear* this flag

      } yield Ok(JsString("success"))
    }
  }

  private def updateOrCreateStripeCustomer(
      accountQuery: AllColumnsQuery[Accounts, AccountsRow],
      customerParams: Map[String, Object],
  ) = for {
    account <- accountQuery.result.head
    owner <- accountQuery.owner.result.headOption
    customer <- blockingOp {
      account.stripeCustomerId match {
        case Some(id) =>
          Customer.retrieve(id).update(customerParams.asJava)
        case None =>
          Customer.create((customerParams +
              ("email" -> owner.flatMap(_.email).orNull) +
              ("name" -> account.businessName.orNull)
          ).asJava)
      }
    }.toDBIO
    _ <- accountQuery.map(u => u.stripeCustomerId)
        .update(Option(customer.getId))
  } yield customer

  private def subscriptionLevelForPlan(planId: String): Future[String] = for {
    plan <- blockingOp(
      StripePlan.retrieve(planId,
        (
            emptyStripeParams +
                ("expand" -> List("product").asJava)
            ).asJava,
        null)
    ).recoverWith {
      case e: StripeInvalidRequestException if e.getStatusCode == NOT_FOUND =>
        Future.failed(HttpCodeException(BadRequest,
          "attempted to subscribe with nonexistent plan $planId"
        ))
    }
    level <- failIfNone(
      // validate that it's a base plan and not an addon
      plan.getProductObject.getMetadata.asScala.get("level"),
      HttpCodeException(BadRequest, s"attempted to use addon plan ${planId} as main plan")
    ).toFuture
  } yield level

  private def updateStripeSubscription(accountQuery: AllColumnsQuery[Accounts, AccountsRow], planId: String, proRationDate: Long) = {
    for {
      account <- accountQuery.result.head
      owner <- accountQuery.owner.result.headOption
      email = owner.flatMap(_.email).getOrElse("unknown address")
      subscriptionQuery = accountQuery.currentSubscription
      maybeLocalSubscription <- subscriptionQuery.result.headOption
      localSubscription <- failIfNone(maybeLocalSubscription, ???).toDBIO

      subscription <- blockingOp(Subscription.retrieve(localSubscription.stripeSubscriptionId)).toDBIO
      newLevel <- subscriptionLevelForPlan(planId).toDBIO

      // push through DB update before Stripe update so we can roll back xact on error
      _ <- subscriptionQuery.map(_.cancelled)
          .update(false)
      _ <- subscriptionQuery.map(_.stripePlanId)
          .update(planId)
      _ <- subscriptionQuery.map(_.subscriptionLevel)
          .update(newLevel)
      _ <- accountQuery.map(_.subscriptionLevel)
          .update(Some(newLevel))

      _ <- {
        var subscriptionParams = emptyStripeParams +
            ("plan" -> planId) +
            ("proration_date" -> Long.box(proRationDate))

        // any call to this function should uncancel the subscription
        if (subscription.getCancelAtPeriodEnd) {
          subscriptionParams += ("cancel_at_period_end" -> Boolean.box(false))
        }
        blockingOp(subscription.update(subscriptionParams.asJava))
      }.toDBIO
      // bill immediately
      stripeCustomerId <- failIfNone(account.stripeCustomerId,
        HttpCodeException(BadRequest, "updateStripeSubscription() endpoint called for account without stripeCustomerId")
      ).toDBIO
      maybeInvoice <- {
        val invoiceParams = emptyStripeParams + ("customer" -> stripeCustomerId)
        blockingOp(
          Some(Invoice.create(invoiceParams.asJava))
        ).recover {
          // check for the need to bill by just trying to create the invoice and
          // catching the error. (God I'm too much of a Python coder.)
          case e: StripeInvalidRequestException if e.getCode == "invoice_no_customer_line_items" =>
            None
        }
      }.toDBIO

      _ <- maybeInvoice match {
        case Some(invoice) => blockingOp {
          invoice.pay()
        }.recoverWith { case e =>
          // closing invoice to prevent future billing attempts
          val invoiceParams = emptyStripeParams + ("closed" -> Boolean.box(true))
          val subscriptionParams = emptyStripeParams ++ Seq(
            "plan" -> localSubscription.stripePlanId,
            // already cancelled the initial proration by closing the invoice, don't
            // want to bill/credit for the correction a second time
            "prorate" -> Boolean.box(false)
          )

          for {
            _ <- blockingOp(invoice.update(invoiceParams.asJava))
            _ <- blockingOp(subscription.update(subscriptionParams.asJava))
            // still want to abort DB xact and return a failure to the client
            _ <- Future.failed(e)
          } yield Unit
        }.toDBIO
        case None => DBIO.successful(Unit)
      }

      _ <- {
        // notify service@parsleycooks.com that account has changed their subscription level
        val accountname = owner.map(modelUtils.getUsername).orElse(owner.flatMap(_.email)).getOrElse(s"stripe customer ${localSubscription.stripeSubscriptionId}")
        val subject = s"$accountname changed their subscription level"
        val content = s"$accountname, $email changed their subscription level from ${localSubscription.subscriptionLevel} to $newLevel"
        emailUtils.send(
          new Email("service@parsleycooks.com", "Service at Parsley Software"),
          new Email("service@parsleycooks.com", "Service at Parsley Software"),
          subject,
          content
        )
      }.recover {
        case HttpCodeException(InternalServerError, jsBody) => Future.successful(Unit) // discard API error
        case e: IOException => Future.successful(Unit) // also discard connection errors that sendgrid lib throws directly
      }.toDBIO
    } yield Unit
  }


  private def createStripeSubscription(
      accountQuery: AllColumnsQuery[Accounts, AccountsRow],
      coupon: Option[String], planId: String,
  ) = for {
    account <- accountQuery.result.head
    owner <- accountQuery.owner.result.headOption
    isUpstreamInvited <- accountQuery.isUpstreamInvited.result
    hasUsedTrial <- accountQuery.subscriptionHistory.exists.result

    subscriptionQuery = accountQuery.currentSubscription
    localSubscription <- subscriptionQuery.result.headOption
    _ <- failIf(localSubscription.isDefined, HttpCodeException(BadRequest, ReadableError(
      "error" -> ("You have already subscribed in another tab." +
          "Please refresh the page to begin using Parsley.")
    ).toJson)).toDBIO

    newLevel <- subscriptionLevelForPlan(planId).toDBIO

    stripeSubscription <- {
      var subscriptionParams = emptyStripeParams + ("plan" -> planId)

      var stripeCustomerId = account.stripeCustomerId.getOrElse {
        throw HttpCodeException(BadRequest, "createStripeSubscription() endpoint called for account without stripeCustomerId")
      }

      subscriptionParams += ("customer" -> stripeCustomerId)
      coupon.foreach(c => subscriptionParams += ("coupon" -> c))

      // multi-location downstreams get no trial, neither do users
      // re-subscribing after some time off
      if (!hasUsedTrial && !isUpstreamInvited) {
        subscriptionParams += ("trial_from_plan" -> Boolean.box(true))
      }

      blockingOp(Subscription.create(subscriptionParams.asJava))
    }.toDBIO

    _ <- {
      // report this user to service@parsleycooks.com
      val name = owner.map(modelUtils.getUsername).orElse(owner.flatMap(_.email)).getOrElse("unknown user")

      // not using this to actually send, just to report in email body
      val email = owner.flatMap(_.email).getOrElse("unknown address")

      emailUtils.send(
        new Email("service@parsleycooks.com", "Service at Parsley Software"),
        new Email("service@parsleycooks.com", "Service at Parsley Software"),
        "New Parsley Subscription by " ++ name,
        s"""
                <p>${name} has ${if (hasUsedTrial) "re" else ""}subscribed to Parsley.</p>
                <p>Phone Number: ${owner.flatMap(_.phoneNumber).getOrElse("")}</p>
                <p>Subscription Level: $newLevel</p>
                <p>Email Address: ${email}</p>
                <p>Company Name: ${account.businessName.getOrElse("")}</p>
              """
      )
    }.recover {
      case HttpCodeException(InternalServerError, jsBody) => Future.successful(Unit) // discard API error
      case e: IOException => Future.successful(Unit) // also discard connection errors that sendgrid lib throws directly
    }.toDBIO

    _ <- accountQuery.map(a => a.subscriptionLevel)
        .update(Option(newLevel))
    _ <- accountQuery.map(a => a.promoCode)
        .update(Option(stripeSubscription.getDiscount).map(_.getCoupon).map(_.getId))

    _ <- Subscriptions += SubscriptionsRow(
      id = 0,
      userId = account.id,
      stripeSubscriptionId = stripeSubscription.getId,
      planValidUntil = Instant.ofEpochSecond(stripeSubscription.getCurrentPeriodEnd),
      planStarted = Instant.now,
      stripePlanId = stripeSubscription.getPlan.getId,
      subscriptionLevel = newLevel,
      addons = List(),
    )

  } yield stripeSubscription

  private case class ChangeSubscriptionRequest(
      plan: String,
      proRationDate: Long,
  )
  implicit private val changeSubscriptionReqFormat = Json.format[ChangeSubscriptionRequest]
  def changeSubscriptionPlan = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        ChangeSubscriptionRequest(newPlan, proRationDate) <- Try(r.body.as[ChangeSubscriptionRequest]).toDBIO
        _ <- updateStripeSubscription(r.accountQuery, newPlan, proRationDate)
      } yield Ok(JsString("success"))
    }
  }

  private case class SubscribeRequest(
      token: Option[String],
      coupon: Option[String],
      plan: String,
  )
  private implicit val subscribeRequestReads = Json.reads[SubscribeRequest]
  def subscribe = UserRestrictedAction().async(parse.json) { implicit r =>

    var customerParams = emptyStripeParams

    // do we need to add the customer's payment info?

    db.runInTransaction {
      for {
        // can be combined into one req; play-json is okay with parsing subset
        // of object's fields
        // running first for transactional integrity reasons
        updateReq <- Try(r.body.as[UserUpdateRequest]).toDBIO
        _ <- if (updateReq.acceptsTerms.exists(identity)) { // exists(identity) => exists and is true
          r.accountQuery.map(u => u.acceptsTerms).update(true)
        } else DBIO.successful(Unit) // never *clear* this flag

        subscribeRequest <- Try(r.request.body.as[SubscribeRequest]).toDBIO
        _ = subscribeRequest.token.foreach(t => customerParams += ("source" -> t))

        // Stripe modifications pushed as late as possible, so failures in DB are
        // unlikely to cause bad data in Stripe
        _ <- updateOrCreateStripeCustomer(r.accountQuery, customerParams)
        _ <- createStripeSubscription(r.accountQuery,
          subscribeRequest.coupon, subscribeRequest.plan,
        )

      } yield Ok(JsString("success"))
    }
  }

  def cancelSubscription = UserRestrictedAction().async { implicit r =>
    db.runInTransaction {
      val localSubscriptionQuery = r.accountQuery.currentSubscription
      for {
        subscriptionId <- localSubscriptionQuery.map(_.stripeSubscriptionId).result.head
            .recoverWith {
              case _: NoSuchElementException => DBIO.failed(HttpCodeException(BadRequest, JsString("user not subscribed")))
            }
        stripeSubscription <- blockingOp(Subscription.retrieve(subscriptionId)).toDBIO
        cancellationParams = emptyStripeParams + ("cancel_at_period_end" -> Boolean.box(true))
        _ <- blockingOp(stripeSubscription.update(cancellationParams.asJava)).toDBIO
        _ <- localSubscriptionQuery.map(_.cancelled).update(true)
        _ <- blockingOp { // notify service@parsleycooks.com that user has cancel subscription
          val userRow = r.identity.dbRow
          val username = modelUtils.getUsername(userRow)
          val email = userRow.email.getOrElse("unknown address")
          val subject = s"$username has cancelled their Parsley Account"
          val content = s"$username, $email has cancelled their Parsley Account"
          val from = new Email("service@parsleycooks.com", "Service at Parsley Software")
          emailUtils.send(from, from, subject, content)
        }.toDBIO.recover {
          case HttpCodeException(InternalServerError, jsBody) => Unit // discard API error
          case e: IOException => Unit // also discard connection errors that sendgrid lib throws directly
        }
      } yield Ok(JsString("so sad!"))
    }
  }

  def uncancelSubscription = UserRestrictedAction().async { implicit r =>
    db.runInTransaction {
      for {
        maybeSubscription <- r.accountQuery.currentSubscription.result.headOption
        subscription <- failIfNone(maybeSubscription,
          HttpCodeException(BadRequest, ReadableError("_error" -> "You are not subscribed. Please refresh the page and then reactivate your subscription.").toJson)
        ).toDBIO
        stripeSubscription <- blockingOp(Subscription.retrieve(subscription.stripeSubscriptionId)).toDBIO
        _ <- blockingOp {
          // any call to this function should uncancel the subscription
          val subscriptionParams = emptyStripeParams + ("cancel_at_period_end" -> Boolean.box(false))
          stripeSubscription.update(subscriptionParams.asJava)
        }.toDBIO
      } yield Ok(JsString("success"))
    }
  }

  def itemsWithChangedPlan(newPlan: String, originalSubscription: Subscription) = {
    val deletedItems = originalSubscription.getSubscriptionItems
        .autoPagingIterable.asScala.map { originalItem  =>
      emptyStripeParams ++ Seq(
        "id" -> originalItem.getId,
        "deleted" -> Boolean.box(true),
      )
    }.toSeq

    val newItem = emptyStripeParams ++ Seq(
      "plan" -> newPlan,
      "quantity" -> new Integer(1),
    )
    (deletedItems :+ newItem).map(_.asJava).asJava
  }

  case class ProRationPreview(
      proRationDate: Instant, // echo back to client for convenience

      dueNow: Long,
      dueAtPeriodEnd: Long,
      periodEnd: Instant,
  )
  private implicit val proRationFormat = Json.format[ProRationPreview]
  def previewProRation(plan: String, proRationDate: Long) = UserRestrictedAction().async { implicit r =>
    db.runInTransaction {
      for {

        customerId <- failIfNone(r.account.stripeCustomerId,
          HttpCodeException(BadRequest, ReadableError("_error" -> Messages("switchplan.preview.notcustomer")).toJson)
        ).toDBIO

        maybeCurrentSubscriptionId <- r.accountQuery.currentSubscription.map(_.stripeSubscriptionId).result.headOption
        subscriptionId <- failIfNone(maybeCurrentSubscriptionId,
          HttpCodeException(Unauthorized, ReadableError("_error" -> Messages("switchplan.preview.notcustomer")).toJson)
        ).toDBIO
        stripeCustomer <- blockingOp(Customer.retrieve(customerId)).toDBIO
        stripeSubscription <- blockingOp(Subscription.retrieve(subscriptionId)).toDBIO

        invoice <- blockingOp {
          var params = emptyStripeParams ++ Seq(
            "customer" -> customerId,
            "subscription" -> subscriptionId,
            "subscription_items" -> itemsWithChangedPlan(plan, stripeSubscription),
            "subscription_proration_date" -> Long.box(proRationDate),
          )
          Try(stripeSubscription.getDiscount().getCoupon().getId()).foreach { couponId =>
            params += ("coupon" -> couponId)
          }
          Invoice.upcoming(params.asJava)
        }.toDBIO
        invoiceLines <- blockingOp(invoice.getLines.autoPagingIterable.asScala.toSeq).toDBIO
      } yield {
        val (billedNow, billedLater) = invoiceLines.partition { line =>
          line.getPeriod.getStart <= proRationDate
        }

        val preview = ProRationPreview(
          proRationDate = Instant.ofEpochSecond(proRationDate),
          dueNow = billedNow.map(_.getAmount).map(Long2long).sum,

          dueAtPeriodEnd = billedLater.map(_.getAmount).map(Long2long).sum,
          periodEnd = Instant.ofEpochSecond(billedLater.map(_.getPeriod.getStart).min),
        )
        Ok(Json.toJson(preview))
      }
    }
  }

  def previewReactivation(plan: String) = UserRestrictedAction().async { implicit r =>

    db.runInTransaction {
      for {
        customerId <- failIfNone(r.account.stripeCustomerId,
          HttpCodeException(BadRequest, "previewReactivation called with new user")
        ).toDBIO

        maybeCurrentSubscription <- r.accountQuery.currentSubscription.result.headOption

        invoice <- blockingOp {
          var upcomingInvoiceParams = emptyStripeParams + (
            "customer" -> customerId
          )
          maybeCurrentSubscription match {
            case Some(subscription) if subscription.pastDue =>
              upcomingInvoiceParams += ("subscription" -> subscription.stripeSubscriptionId)
            case None => blockingOp {
              val subscriptionItem = (emptyStripeParams ++ Seq(
                "plan" -> plan,
                "quantity" -> new Integer(1),
              )).asJava
              upcomingInvoiceParams += ("subscription_items" -> Seq(subscriptionItem).asJava)
            }
            case _ => throw new HttpCodeException(BadRequest, "previewReactivation called for subscription in good standing")
          }
          Invoice.upcoming(upcomingInvoiceParams.asJava)
        }.toDBIO
      } yield Ok(invoice.toJson).as("application/json")
    }
  }


  private case class PaymentMethodRequest(
      token: Option[String],
  )
  implicit private val paymentMethodFormat = Json.format[PaymentMethodRequest]
  def paymentMethod = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        requestBody <- Try(r.request.body.as[PaymentMethodRequest]).toDBIO
        account <- r.accountQuery.result.head
        accountOwner <- r.accountQuery.owner.result.head
        stripeCustomerIdToAdd <- blockingOp {
          var customerParams = emptyStripeParams

          // do we need to add the customer's payment info?
          requestBody.token match {
            case Some(t) => customerParams += ("source" -> t)
            case None => {}
          }

          account.stripeCustomerId match {
            case Some(id) => {
              val customer = Customer.retrieve(id)
              customer.update(customerParams.asJava)
              None
            }
            case None => {
              customerParams += ("email" -> accountOwner.email.orNull)
              val customer = Customer.create(customerParams.asJava)
              Some(customer.getId)
            }
          }
        }.toDBIO

        _ <- stripeCustomerIdToAdd match {
          case Some(stripeCustomerId) => r.accountQuery.map(u => u.stripeCustomerId)
              .update(Some(stripeCustomerId))
          case None => DBIO.successful(Unit)
        }
      } yield Ok(JsString("success"))
    }
  }

  private case class ReactivatePlan(
    token: String,
    firstName: Option[String],
    lastName: Option[String],
    businessName: Option[String],
    shippingPhoneNumber: Option[String]
  )
  implicit private val reactivatePlanReads = Json.reads[ReactivatePlan]

  def reactivatePlan() = SecuredApiAction.async(parse.json) { implicit r =>

    val applicationUrl = config.get[String]("applicationUrl")

    db.runInTransaction {
      for {
        ReactivatePlan(token, firstName, lastName, businessName, shippingPhoneNumber) <- Try(r.body.as[ReactivatePlan]).toDBIO
        // check if user is invited user
        _ <- failIf(
          !r.isAccountOwner,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Shared users cannot reactivate plans. Please contact your account's owner.").toJson)
        ).toDBIO

        // check if there's a completely cancelled plan to restart
        maybeExpiredSubscription <- r.accountQuery.lastStripeExpiredSubscription.result.headOption

        // check if there's a plan that's expired but active from the stripe
        // perspective (i.e. hasn't transitioned to "cancelled", stripe is still
        // retrying the invoice for this billing period)
        maybeActiveSubscription <- r.accountQuery.stripeActiveSubscription.result.headOption
        _ <- failIf(maybeActiveSubscription.exists(!_.pastDue),
          HttpCodeException(BadRequest, ReadableError("_error" -> "You already have an active subscription. Please refresh the page to start using Parsley").toJson)
        ).toDBIO
        hasActiveSubscription = maybeActiveSubscription.isDefined

        subscriptionToReactivate <- failIfNone(
          maybeActiveSubscription.orElse(maybeExpiredSubscription),
          HttpCodeException(BadRequest, ReadableError("_error" -> s"You have not subscribed to Parsley in the past. Please [subscribe to a plan]($applicationUrl)").toJson)
        ).toDBIO
        // update start

        // update user info
        _ <- r.authedUserQuery.map(u => (u.firstName, u.lastName, u.fullName, u.phoneNumber))
          .update((firstName, lastName, None, shippingPhoneNumber))
        _ <- r.accountQuery.map(_.businessName).update(businessName)
        account <- r.accountQuery.result.head

        /*
         * sync Stripe and DB
         */
        stripeCustomerId <- failIfNone(account.stripeCustomerId,
          HttpCodeException(BadRequest, "reactivatePlan() endpoint called for account without stripeCustomerId")
        ).toDBIO

        // set card to customer
        customer <- blockingOp {
          Customer.retrieve(stripeCustomerId)
              .update((emptyStripeParams + ("source" -> token)).asJava)
        }.toDBIO

        _ <- { // retry billing, and update database
          if (hasActiveSubscription) {
            // subscription should re-bill when we set a new payment source,
            // but won't return the error immediately, so we manually put through
            // the charge to get any errors
            val invoiceDateFilter = (
                emptyStripeParams + ("gte" -> ZonedDateTime.now.minusMonths(1).toEpochSecond.toString)
            ).asJava
            for {
              invoices <- blockingOp {
                Invoice.list((emptyStripeParams ++ Seq(
                  "customer" -> customer.getId,
                  "subscription" -> subscriptionToReactivate.stripeSubscriptionId,
                  "date" -> invoiceDateFilter,
                )).asJava)
              }.toDBIO
              toRetry <- blockingOp { // blocking because autoPagingIterable may make more network requests
                invoices.autoPagingIterable.asScala
                    .filter(!_.getClosed)
              }.toDBIO
              _ <- Future.sequence( // don't care about return values, only about errors
                toRetry.map(i => blockingOp(i.pay()))
              ).toDBIO
              _ <- Subscriptions.filter(_.id === subscriptionToReactivate.id)
                  .map(_.pastDue).update(false)
            } yield Unit
          } else {
            for {
              // Stripe modifications pushed as late as possible, so failures in DB are
              // unlikely to cause bad data in Stripe
              reactivatedSubs <- createStripeSubscription(
                r.accountQuery,
                None,
                subscriptionToReactivate.stripePlanId,
              )
              _ <- Subscriptions += SubscriptionsRow(
                id = 0,
                userId = account.id,
                stripeSubscriptionId = reactivatedSubs.getId,
                planValidUntil = Instant.ofEpochSecond(reactivatedSubs.getCurrentPeriodEnd),
                planStarted = Instant.ofEpochSecond(reactivatedSubs.getStart),
                stripePlanId = reactivatedSubs.getPlan.getId,
                subscriptionLevel = reactivatedSubs.getPlan.getId,
                addons = List(),
              )
            } yield Unit
          }
        }.recoverWith { case e: CardException => DBIO.failed(
          HttpCodeException(PaymentRequired, // yeah, yeah, not well-defined, but Stripe uses it :-P
            ReadableError("_error" -> s"Reactivation failed: ${e.getMessage}").toJson
          )
        )}
        // and then sync up accounts and subscriptions tables
        _ <- r.accountQuery.map(_.subscriptionLevel).update(Some(subscriptionToReactivate.subscriptionLevel))
      } yield Ok(JsString("success"))
    }
  }

}
