package controllers.api

import models.JsonConversions.{FancyFormat, IngredientWithWeight, ProductNutritionInfo, characteristicsAPIFormats}
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.MiscellaneousImplicits._
import util.DirectedGraph
import util.Exceptions._

import play.api.data.validation.ValidationError
import play.api.db.slick.HasDatabaseConfig
import play.api.libs.json.{JsBoolean, Json, JsonValidationError, OFormat}
import play.api.mvc.Results._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}
import scala.collection.mutable
import java.text.DecimalFormat

object RecipeUtils extends JsonRestUtils { self: HasDatabaseConfig[DBDriver] =>

  import Tables.profile.api._

  type ProductId = Int
  type MeasureId = Int
  type UnitId = Int
  type ProductSourceId = Int

  private def forgivingCeil(x: Double): Double = {
    math.ceil(
      // toFloat gives cheap truncation - loss of precision is fine, because we're going to nearest whole number anyway
      x.toFloat
    ).toDouble // but still want same datatype, for convenience more than precision at this point
  }

  case class Quantity(
      amount: Double,
      measure: Option[MeasureId],
      unit: Option[UnitId],
      plusIndeterminate: Boolean = false
  ) {

    private def quantityInOurUnits(other: Quantity)(implicit conversions: Conversions) =
      conversions.convertUnsafe(this.unit, other).amount

    def +(other: Quantity)(implicit conversions: Conversions) =
    // first need to handle the cases of pure-indeterminate values. in those cases,
    // we ignore the (fake) measure and unit
      if (this.amount == 0) {
        // also works in the case where both are indeterminate
        other.copy(plusIndeterminate = this.plusIndeterminate || other.plusIndeterminate)
      } else if (other.amount == 0) {
        this.copy(plusIndeterminate = this.plusIndeterminate || other.plusIndeterminate)
      } else {
        // whew, actual addition. probably the common case
        this.copy(
          amount = this.amount + quantityInOurUnits(other),
          plusIndeterminate = this.plusIndeterminate || other.plusIndeterminate
        )
      }

    def -(other: Quantity)(implicit conversions : Conversions) = this + other.copy(amount = -other.amount)

    // throws away plusIndeterminate values in numerator, denominator should (and for now, does)
    // NOT have plusIndeterminate set to true
    def /(other: Quantity)(implicit conversions: Conversions) = {
      if (this.amount == 0) { // covers case of purely-indeterminate quantities
        0
      } else {
        this.amount / quantityInOurUnits(other)
      }
    }

    def *(x: Double) = copy(amount = amount * x)

  }

  object Quantity {
    private val baseFormat = Json.format[Quantity]
    private val fancyFormat = FancyFormat[Quantity](baseFormat,
      defaultFields = Map(
        "plusIndeterminate" -> JsBoolean(false)
      )
    )

    implicit val apiFormat = OFormat(
      fancyFormat.filter(JsonValidationError(
        Seq("quantity had only measure or only unit, must have both or none")
      ))(
        q => q.measure.isDefined == q.unit.isDefined
      ),
      baseFormat
    )
  }

  case class MissingConversionException(
      originalUnit: Option[UnitId], destinationUnit: Option[UnitId], productId: ProductId
  ) extends Exception {
    override def getMessage = s"could not convert from unit $originalUnit " +
        s"to unit $destinationUnit " +
        s"for product $productId"
  }

  case class Conversions(
      dbRepresentation: Seq[ProductMeasuresRow],
      units: Map[UnitId, UnitsRow],
      product: ProductId // for debugging/error-reporting only
  ) {
    val byMeasure = Map(dbRepresentation.map(r => (r.measure: MeasureId) -> r): _*)

    def validUnit(id: UnitId) = byMeasure.contains(units(id).measure)

    private def convertedSize(unitId: Int) = { // size in terms of the base measure's base unit
      val unit = units(unitId)
      val conversion = byMeasure(unit.measure)
      val preferredUnit = units(conversion.preferredUnit)
      val conversionUnit = units(conversion.conversionUnit)
      unit.size / preferredUnit.size * conversionUnit.size / conversion.amount * conversion.conversion
    }

    // should match unit-conversions:getSizeInPrimary
    def getSizeInPrimary(unitId: UnitId): Double = {
      val unit = units(unitId)

      val thisConversion = dbRepresentation.find(_.measure == unit.measure) match {
        case Some(c) => c
        case None => return 0
      }

      val primaryConversion = maybePrimaryConversion match {
        case Some(c) => c
        case None => throw new Exception(
          s"could not calculate size in primary." +
              "No primary conversion found for product: $product"
        )
      }

      val preferredUnitSize = units(thisConversion.preferredUnit).size
      val conversionUnitSize = if (thisConversion.conversionMeasure != primaryConversion.measure) {
        val nextConversion = dbRepresentation.find(_.measure == thisConversion.conversionMeasure) match {
          case Some(c) => c
          case None => throw new Exception(s"no conversion found for measure ${thisConversion.conversionMeasure} for product $product")
        }
        if (nextConversion.conversionMeasure == thisConversion.measure) {
          throw new Exception(s"circular measure dependency for product $product - ${nextConversion.measure} and ${thisConversion.measure} are defined in terms of each other")
        }
        getSizeInPrimary(thisConversion.conversionUnit)
      } else {
        units(thisConversion.conversionUnit).size
      }

      (unit.size / preferredUnitSize) * (conversionUnitSize / thisConversion.amount) * thisConversion.conversion
    }

    def convert(maybeDestinationUnit: Option[UnitId], original: Quantity): Option[Quantity] = {
      val Quantity(originalAmount, maybeOriginalMeasure, maybeOriginalUnit, plusIndeterminate) = original
      (maybeOriginalUnit, maybeDestinationUnit) match {
        case (Some(originalUnit), Some(destinationUnit)) if validUnit(originalUnit) && validUnit(destinationUnit) =>
          Some(Quantity(
            originalAmount * convertedSize(originalUnit) / convertedSize(destinationUnit),
            Some(units(destinationUnit).measure),
            Some(destinationUnit),
            plusIndeterminate,
          ))
        case (None, None) => Some(original) // no conversion required if both are in portions
        case _ => None
      }
    }

    def unitDisplayName(unitId: Int, abbr: Boolean): String = {
      // based on unitconversions.jsx
      val unit = units(unitId)
      val productMeasure = byMeasure(unit.measure)
      val defaultName = if (abbr) unit.abbr getOrElse unit.name else unit.name
      val isMeasureBaseUnit = unit.size == 1 // only replace name for "each", not "dozen" or other things

      // hacky - custom names are usually not suited to being put after their quantity without space in between
      // HTML is nice to us and will collapse multiple spaces into one
      // otherwise, callers would have to know whether the string they got back was a custom name - even worse
      val customName = if (isMeasureBaseUnit) productMeasure.customName getOrElse defaultName else defaultName

      s" $customName"
    }


    // should match with PackagedAmountInput.fullPackageName in ShoppingList.jsx
    def abbrPackageName(source: ProductSourcesRow): String = {
      val packageUnitName = unitDisplayName(source.unit, true).trim

      if (source.packaged) {
        val packageName = source.subPackageName.orElse(source.packageName).getOrElse("pkg")

        val formatter = new DecimalFormat("#.##")
        val packageSize = formatter.format(source.packageSize)

        var returnValue = s"$packageSize$packageUnitName $packageName"

        if (source.subPackageName.isDefined) {
          val superPackageName = source.packageName.getOrElse("pkg")
          returnValue = s"${superPackageName} of ${source.superPackageSize}\u00d7${returnValue}"
        }
        returnValue
      } else {
        packageUnitName
      }
    }


    /**
      *  for use when internal DB constraints should have ensured the existence of a correct conversion.
      *  More specifically, the lack of such a conversion is a fatal exception
      *
      * @param maybeDestinationUnit
      * @param original
      * @return
      */
    def convertUnsafe(maybeDestinationUnit: Option[UnitId], original: Quantity): Quantity = {
      val maybeValid = convert(maybeDestinationUnit, original)
      if (maybeValid.isDefined) maybeValid.get
      else throw new MissingConversionException(original.unit, maybeDestinationUnit, product)
    }

    def divideQuantities(numerator: Quantity, denominator: Quantity) =
      numerator.amount / convertUnsafe(numerator.unit, denominator).amount

    def maybePrimaryConversion = dbRepresentation
        .find(m => m.measure == m.conversionMeasure)
  }

  object Conversions {
    val COUNT_MEASURE = 1
  }

  case class Recipe(
      outputSize: Quantity,
      quantum: Option[Quantity],
      maxBatch: Option[Quantity],
      ingredients: Map[ProductId, Seq[Quantity]],
      simplePrep: Option[SimplePreparationsRow] = None,
      advancePrep: Boolean = false,
      price: Option[BigDecimal] = None,
      itemNumber: Option[String] = None
  )

  case class ProductionInfo(
      product: ProductsRow,
      conversions: Conversions,
      productionMethod: Either[Recipe, Option[ProductSourcesRow]],
      subIngredients: Option[String]
  )

  type BillOfMaterials = Map[ProductId, Quantity]
  // AnyVal extension is performance optimization
  // see https://docs.scala-lang.org/overviews/core/value-classes.html
  implicit class BillOfMaterialsExtensions(val bom: BillOfMaterials) extends AnyVal {
    def +(other: BillOfMaterials)(implicit allConversions: Map[ProductId, Conversions]): BillOfMaterials = bom.merge(other,
      { (productId, q1, q2) =>
        implicit val conversions = allConversions(productId)
        q1 + q2
      }
    )
    def -(other: BillOfMaterials)(implicit allConversions: Map[ProductId, Conversions]): BillOfMaterials = {
      bom + other.mapValues(q => q.copy(amount = -q.amount))
    }
  }

  case class ProductionOrderEntry(
      productId: Int,
      name: String,
      required: Quantity,

      // amount actually bought/produced, after taking into account inventory and batching
      // the change in inventory is (procured - requiredAmount)
      procured: Quantity,

      ingredientsUsed: Option[BillOfMaterials] = None,
      cashUsed: Option[BigDecimal] = None,

      source: Option[ProductSourcesRow] = None,
      isSimplePrep: Boolean = false,
      isAdvancePrep: Boolean = false,
  ) {
    def isPreparedFromRecipe = ingredientsUsed.isDefined
  }
  type ProductionOrder = Map[ProductId, ProductionOrderEntry]

  def getTransactedQuantity(txn: TransactionDeltasRow) = Quantity(
    txn.amount, txn.measure, txn.unit
  )

  // should match with packaging.jsx:costingSize
  def getCostingSize(source: ProductSourcesRow) = {
    val (amount, measure, unit) = source.pricePer match {
      case PricePerType.SuperPackage  =>
        (source.packageSize * source.superPackageSize, Some(source.measure), Some(source.unit))
      case PricePerType.Package =>
        (source.packageSize, Some(source.measure), Some(source.unit))
      case PricePerType.Unit =>
        (1.0, Some(source.pricingMeasure.getOrElse(source.measure)), Some(source.pricingUnit.getOrElse(source.unit)))
    }
    Quantity(amount, measure, unit)
  }

  def getProductMeasureFromIngredientPriceHistoryMeasure(phMeasure: IngredientPriceHistoryMeasuresRow) = {
    ProductMeasuresRow(
      -1,
      -1,
      phMeasure.measure,
      phMeasure.preferredUnit,
      phMeasure.conversionMeasure,
      phMeasure.conversionUnit,
      phMeasure.conversion,
      None,
      false,
      phMeasure.amount
    )
  }

  def getProductSourceRowFromIngredientPriceHistory(priceHistory: IngredientPriceHistoryRow) = {
    ProductSourcesRow(
      id=priceHistory.id,
      product=priceHistory.product,
      measure=priceHistory.measure,
      unit=priceHistory.unit,
      cost=priceHistory.cost,
      packaged=priceHistory.packaged,
      packageSize=priceHistory.packageSize,
      superPackageSize=priceHistory.superPackageSize.toInt,
      pricePer=priceHistory.pricePer,
      pricingMeasure=priceHistory.pricingMeasure,
      pricingUnit=priceHistory.pricingUnit
    )
  }

  // should match with unit-conversions.jsx:convertPerQuantityValue
  def convertPerQuantityValue(
      value: BigDecimal,
      sourceAmount: Double, sourceUnitId: UnitId,
      destAmount: Double, destUnitId: UnitId,
      conversions: Conversions,
    ) = {

    val sourceUnitSize = conversions.getSizeInPrimary(sourceUnitId)
    val destUnitSize = conversions.getSizeInPrimary(destUnitId)

    val sourcePackageSize = sourceAmount * sourceUnitSize
    val destPackageSize = destAmount * destUnitSize

    value / sourcePackageSize * destPackageSize
  }

  // should match with packaging.jsx:effectivePackageSize
  def effectivePackageSize(source: ProductSourcesRow) =
    Quantity(source.packageSize * source.superPackageSize, Some(source.measure), Some(source.unit))

  case class SourceCostException(product: ProductId) extends Exception {
    override def getMessage = s"could not find lowest cost source for product $product." +
      "Either source list is empty or have no source with cost defined."
  }
  def getLowestCostSource(
      sources: Seq[ProductSourcesRow],
    )(implicit conversions: Conversions) : Option[ProductSourcesRow] = {

    val refSources = sources.filter(_.cost.nonEmpty)
    if (refSources.isEmpty) {
      None
    } else {
      val headSourceCostSize = getCostingSize(refSources.head)
      Some(refSources.minBy { s =>
        val thisSourceCostingSize = getCostingSize(s)
        convertPerQuantityValue(s.cost.get,
          thisSourceCostingSize.amount, thisSourceCostingSize.unit.get,
          headSourceCostSize.amount, headSourceCostSize.unit.get,
          conversions,
        )
      })
    }
  }

  def generateProductionOrder(
      rootBOM: BillOfMaterials,
      productionInfo: Map[ProductId, ProductionInfo],
      withBatching: Boolean = false,
      requireNetQuantiy: Boolean = false // for nutritional calculations for consumer info, see PARS-2349
  ): Try[ProductionOrder] = {
    val productConversions = productionInfo.mapValues(_.conversions)
    val productionMethod = productionInfo.mapValues(_.productionMethod)

    val graph = DirectedGraph[ProductId] { product =>
      productionMethod(product) match {
        case Left(recipe) => recipe.ingredients.keys
        case Right(_) => Nil
      }
    }

    for { // monad: Try
    // processing in topological sort order makes sure that we have a full picture of the required quantity
    // of each product before we start processing it
      toResolve <- graph.topologicalSort(starters=rootBOM.keys) match {
        case Some(sorted) => Success(sorted)
        case None => Failure(HttpCodeException(InternalServerError, "dependencies are not a DAG"))
      }

      orderEntries <- {
        var requiredQuantities = rootBOM

        sequenceTrys(toResolve.map { product =>
          val quantity = requiredQuantities.getOrElse(product, Quantity(0, None, None))
          for { // monad: Try
            entry <- productionInfo(product).productionMethod match {
              case Right(maybeSource) => {
                implicit val conversions = productConversions(product)

                val (purchaseQuantity, cost) = maybeSource match {
                  case Some(source) => {
                    val packageSize = effectivePackageSize(source)
                    val purchaseQuantity = {
                      if (withBatching &&
                          (source.packaged || packageSize.measure.contains(Conversions.COUNT_MEASURE))
                      ) {
                        val quantum =
                          if (source.packaged) packageSize
                          else packageSize.copy(amount=1) // quantum is 1 ea

                        quantum * forgivingCeil(quantity / quantum)
                      } else quantity
                    }
                    (purchaseQuantity, source.cost.map(_ * (purchaseQuantity / getCostingSize(source))))
                  }
                  case None => (quantity, None) // we assume bulk purchasing if no source defined
                }

                Success(product -> ProductionOrderEntry(
                  product, productionInfo(product).product.name,
                  quantity, purchaseQuantity,
                  source = maybeSource,
                  cashUsed = cost
                ))
              }
              case Left(recipe) => {
                val maybeQuantum = if (withBatching) {
                  if (recipe.quantum.isDefined) {
                    recipe.quantum
                  } else if (recipe.outputSize.measure.contains(Conversions.COUNT_MEASURE)) {
                    Some(recipe.outputSize.copy(amount=1)) // 1 ea
                  } else None
                } else None

                val productionQuantity = maybeQuantum match {
                  case Some(quantum) =>
                    implicit val conv = productConversions(product)
                    quantum * forgivingCeil(quantity / quantum)
                  case None => quantity
                }

                scaledBillOfMaterials(
                  product, recipe, productionQuantity, productionInfo, requireNetQuantiy
                ).map { addedQuantities =>
                  addedQuantities.foreach { case (ingredient, q) =>
                    implicit val ingredientConversions = productConversions(ingredient)
                    val primaryConversion: Option[ProductMeasuresRow] = ingredientConversions.maybePrimaryConversion

                    val oldQuantity = requiredQuantities.getOrElse(ingredient,
                      Quantity(0, primaryConversion.map(_.measure), primaryConversion.map(_.preferredUnit))
                    )

                    requiredQuantities += ingredient -> (oldQuantity + q)
                  }
                  product -> ProductionOrderEntry(
                    product, productionInfo(product).product.name,
                    quantity, productionQuantity,
                    ingredientsUsed = Some(addedQuantities),
                    isSimplePrep = recipe.simplePrep.nonEmpty,
                    isAdvancePrep = recipe.advancePrep
                  )
                }
              }
            }
          } yield entry
        })
      }
    } yield orderEntries.toMap
  }

  /**
    * returns a bill of materials for a recipe, scaled to the desired output quantity
    */
  def scaledBillOfMaterials(
    output: ProductId, recipe: Recipe, quantity: Quantity, productionInfo: Map[ProductId, ProductionInfo],
    requireNetQuantiy: Boolean = false
  ): Try[BillOfMaterials] = for {
    multiplier <- Try {
      var q = recipe.outputSize
      // See PARS-1632, PARS-2349
      if (requireNetQuantiy && recipe.simplePrep.isDefined) {
        q = q.copy(amount = Math.max(recipe.simplePrep.get.yieldFraction, 1))
      }
      // can't easily stuff "plusIndeterminate" into this Double...
      implicit val outputConversions = productionInfo(output).conversions
      quantity / q
    }
    bom <- Try {
      recipe.ingredients.map { case (ingredient, quantities) =>
        // ...so propagating it through this dummy value
        val zeroQuantity = Quantity(0, None, None, quantity.plusIndeterminate)

        implicit val ingredientConversions = productionInfo(ingredient).conversions
        ingredient -> (quantities.fold(zeroQuantity)(_ + _) * multiplier)
      }
    }
  } yield bom

  /**
    * fetches conversions, supplier info, etc. for toFetch <b>and</b> all their dependencies
    *
    * @param alreadyFetched should not be passed by end-users; used internally
    *                       by the recursive implementation.
    */
  def deepFetchProductionInfo(
      toFetch: Traversable[ProductId],
      alreadyFetched: Map[ProductId, ProductionInfo]=Map()
  )(implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow], ec: ExecutionContext)
  : DBIO[Map[ProductId, ProductionInfo]] = {

    if (toFetch.isEmpty) return DBIO.successful(Map()) // base case

    for {
      newlyFetched <- fetchOneLevelProductionInfo(toFetch)
      all = alreadyFetched ++ newlyFetched
      newDependencies = all.values.map(_.productionMethod).collect {
        case Left(recipe) => recipe.ingredients.keySet
      }.reduceOption(_ ++ _).getOrElse(Set()) -- all.keys
      dependenciesProductionInfo <- deepFetchProductionInfo(newDependencies, all)
    } yield all ++ dependenciesProductionInfo
  }

  private def fetchOneLevelProductionInfo(toFetch: Traversable[ProductId])(
      implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow],
      ec: ExecutionContext
  ): DBIO[Traversable[(ProductId, ProductionInfo)]] = {
    val products = account.products.filter(_.id inSet toFetch)
    for {
      allUnits <- Units.map(u => (u.id, u)).result // TODO: cache this thing at a higher level

      productRows <- products.result
      productsMap = productRows.map(p => p.id -> p).toMap

      productInfoRows <- products.info.result
      productsInfoMap = productInfoRows.map(pInfo => pInfo.product -> pInfo).toMap

      measureRows <- products.measures.result
      measures = measureRows.groupBy(_.product: ProductId)

      preferredSourceRows <- products.explicitlyPreferredSource.result
      preferredSources = preferredSourceRows.map(r => (r.product: ProductId) -> r).toMap
      allSourceRows <- products.sources.filter(!_.tombstone).result
      allSources = allSourceRows.groupBy(_.product)

      recipeRows <- products.recipe.result
      recipes = recipeRows.map(r => (r.product: ProductId) -> r).toMap

      simplePrepRows <- products.preppedBy.result
      simplePreps = simplePrepRows.map(prep => (prep.outputProduct: ProductId) -> prep).toMap

      ingredientUsageRows <- (
          (products.recipe.steps join RecipeStepIngredients) on (_.id === _.step))
          .map { case (step, usage) => (step.product: Rep[ProductId]) -> usage }
          .result
      ingredientUsages = ingredientUsageRows.groupByFirst

      characteristicsRows <- (products ++ products.preppedBy.inputProducts).characteristics.result
      characteristics = characteristicsRows.map(c => (c.product: ProductId) -> c).toMap

    } yield toFetch.map { product =>
      implicit val conversions = Conversions(measures.getOrElse(product, Nil), Map(allUnits: _*), product)
      val maybeSource = if (productsInfoMap.get(product).exists(_.lowestPricePreferred)) {
        getLowestCostSource(allSources.getOrElse(product, Seq.empty))
      } else {
        preferredSources.get(product).orElse {
          // pick an arbitrary source for pricing purposes
          allSources.get(product).map(all => all.minBy(_.id))
        }
      }

      val production = (maybeSource, simplePreps.get(product), recipes.get(product)) match {
        // no info means by default purchased ingredient with no sourcing info - e.g. raw stdlib
        case (None, None, None) => Right(None)

        case (Some(source), _, _) => Right(Some(source))

        case (_, Some(simplePrep), _) => Left {
          val outputQuantity = Quantity(
            simplePrep.yieldFraction,
            Some(simplePrep.yieldMeasure),
            Some(conversions.byMeasure(simplePrep.yieldMeasure).preferredUnit) // make sure we get the error here, not down the line if this is None
          )
          val inputQuantity = outputQuantity.copy(amount = 1)
          Recipe(
            outputQuantity, quantum=None, maxBatch=None,
            Map(simplePrep.inputProduct -> Seq(inputQuantity)),
            simplePrep = Some(simplePrep)
          )
        }

        case (None, _, Some(recipe)) => Left {
          val outputQuantity = Quantity(recipe.outputAmount, recipe.outputMeasure, recipe.outputUnit)
          val quantum = recipe.quantumAmount.map(Quantity(_, recipe.quantumMeasure, recipe.quantumUnit))
          val maxBatch = recipe.maxBatchAmount.map(Quantity(_, recipe.maxBatchMeasure, recipe.maxBatchUnit))
          Recipe(
            outputQuantity,
            quantum,
            maxBatch,
            ingredientUsages.getOrElse(product, Seq())
                .groupBy(_.ingredient: ProductId)
                .mapValues { usages =>
                  usages.map(u => (u.amount, u.measure, u.unit)).map {
                    case (Some(amount), Some(measure), Some(unit)) =>
                      Quantity(amount, Some(measure), Some(unit))
                    case _ =>
                      Quantity(0, None, None, plusIndeterminate = true)
                  }
                },
            advancePrep = recipe.advancePrep,
            price = recipe.price,
            itemNumber = recipe.itemNumber
          )
        }
      }

      val rootIngredient = simplePreps.get(product) match {
        case None => product
        case Some(p) => p.inputProduct
      }

      product -> ProductionInfo(
        productsMap(product),
        conversions,
        production,
        if (characteristics.contains(rootIngredient)) characteristics(rootIngredient).ingredients else None
      )
    }
  }

  case class IngredientNutritionInfo(
      productId: ProductId,
      nutritions: Map[String, BigDecimal],
  )

  // TODO: Move to a util file?
  // Get a `nndbsrId -> nutrientId` map
  private def getNutrientIdMap()(
    implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow], ec: ExecutionContext
  ) : DBIO[Map[String, Int]] = for {
    nutrients <- Nutrients.result
  } yield nutrients.map(n => (n.nndbsrId -> n.id)).toMap

  def calculateNutritionalInfo(
      productId: ProductId,
      productionInfo: Map[ProductId, ProductionInfo], // Reuse fetched data from deepFetchProductionInfo
      multiplier: Option[Double],
      ingredientQuantities: Map[ProductId, Quantity]
  )(implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow], ec: ExecutionContext)
  : DBIO[ProductNutritionInfo] = {
    val productQuery = account.products.filter(_.id === productId)
    val ingredients = account.products.filter(_.id inSet ingredientQuantities.keys)
    for {
      nutrientIdMap <- getNutrientIdMap()

      ingredientNutritionalInfo <- multiplier match {
        case Some(m) => DBIO.sequence(ingredientQuantities.map { case (ingredient, quantity) =>
          fetchIngredientNutritions(
            ingredient,
            nutrientIdMap,
            productionInfo(ingredient),
            m,
            quantity,
          )
        })
        case None => DBIO.successful(Seq()) // no nutritional info
      }

      allergensRows <- ingredients.allergens.result
      characteristicsRows <- ingredients.characteristics.result

      characteristicRow <- productQuery.characteristics.result.headOption

    } yield {

      val nonEdibleIngredients = characteristicsRows.filter(_.nonEdible).map(_.product).toSet
      val addedSugarIngredients = characteristicsRows.filter(_.addedSugar).map(_.product).toSet

      val ingredientList = ingredientQuantities
        .filterKeys(!nonEdibleIngredients.contains(_))
        .map { case (ingredient, quantity) =>
          val conversions = productionInfo(ingredient).conversions
          // keep in sync with NutritionDisplay.jsx:initNutritions(), calculation
          // of weightMultiplier
          val contributionWeight = multiplier.getOrElse(1.toDouble) * conversions
              // if possible, convert to gram
              .convert(Some(2), quantity)
              // if possible, convert to milliliter
              .orElse(conversions.convert(Some(9), quantity))
              .map(_.amount).getOrElse(0.toDouble)

          val name = productionInfo(ingredient).product.name.replaceAll(",", "")
          val fullname = productionInfo(ingredient).subIngredients match {
            case None => name
            case Some(s) => name ++ " (" ++ s ++ ")"
          }

          IngredientWithWeight(ingredient,
            fullname,
            contributionWeight
          )
        }.toSeq.sortBy(_.weight)

      // Haven't taken recipe size & nutrient serving size into account
      // So client-side calculations can be more flexible
      val nutritions = ingredientNutritionalInfo.foldLeft(Map[String, BigDecimal]()) { (m, i) =>
        if (nonEdibleIngredients.contains(i.productId)) {
          m
        } else {
          var res = m.merge(i.nutritions, _ + _)
          if (addedSugarIngredients.contains(i.productId)) {
            res = res + (
              "added_sugar" -> (
                res.getOrElse("added_sugar", BigDecimal(0)) + i.nutritions.getOrElse("total_sugars", BigDecimal(0))
              )
            )
          }
          res.toMap
        }
      }

      var allergenList = allergensRows.foldLeft(mutable.Map[String, String]()) { (m, a) =>
        // Boolean fields
        if (a.milk) m("milk") = ""
        if (a.eggs) m("eggs") = ""
        if (a.wheat) m("wheat") = ""
        if (a.peanuts) m("peanuts") = ""
        if (a.soybeans) m("soybeans") = ""
        if (a.molluscs) m("molluscs") = ""
        if (a.cerealsGluten) m("cerealsGluten") = ""
        if (a.celery) m("celery") = ""
        if (a.mustard) m("mustard") = ""
        if (a.sesameSeeds) m("sesameSeeds") = ""
        if (a.sulphurDioxideSulphites) m("sulphurDioxideSulphites") = ""
        if (a.lupin) m("lupin") = ""
        m
      }

      // String fields
      Seq(("fish", allergensRows.flatMap(_.fish))
        , ("crustaceanShellfish", allergensRows.flatMap(_.crustaceanShellfish))
        , ("treeNuts", allergensRows.flatMap(_.treeNuts)) )
        .foreach { case (field, list) =>
          if (list.length > 0) {
            allergenList += field -> list.filter(_.nonEmpty).mkString(", ")
          }
        }

      val characteristicList = characteristicsRows.flatMap(characteristicsAPIFormats.characteristicsToSeqString).distinct

      val nonEdible = characteristicRow match {
        case None => false
        case Some(c) => c.nonEdible
      }

      val recipeSize = productionInfo(productId)
          .productionMethod.left.toOption
          .map(_.outputSize)

      ProductNutritionInfo(
        nutritions,
        ingredientList,
        allergenList.toMap,
        characteristicList,
        nonEdible,
        recipeSize,
      )
    }
  }

  private def fetchIngredientNutritions(
    productId: ProductId,
    nutrientIdMap: Map[String, Int],
    productionInfo: ProductionInfo,
    // multiplied by each nutrients' value
    multiplier: Double,
    // This ingredient's size in its parent recipe's step
    quantity: Quantity,
  )(
    implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    ec: ExecutionContext
  ): DBIO[IngredientNutritionInfo] = {
    val productQuery = account.products.filter(_.id === productId)
    val productRow = productionInfo.product
    for {
      nutrientRows <- productQuery.nutrients.result
    } yield {

      implicit val conversions = productionInfo.conversions

      val nutrientServingAmount = productRow.nutrientServingAmount.map(_.toDouble)
      val nutrientServingMeasure = productRow.nutrientServingMeasure
      val nutrientServingUnit = productRow.nutrientServingUnit

      // two ways for a bad serving size to appear in the DB: null amount, or
      // null/invalid measure
      val maybeServingSize = nutrientServingAmount.map(
        Quantity(_, nutrientServingMeasure, nutrientServingUnit)
      ).filter { _.measure match {
        case None => conversions.byMeasure.isEmpty
        case Some(m) => conversions.byMeasure.contains(m)
      }}

      val nutritionFacts : Map[String, BigDecimal] = maybeServingSize match {
        case Some(servingSize) => {
          nutrientRows.length match {
            case 0 => Map()
            case _ => {
              val nutritionalValueMultiplier = multiplier * (quantity / servingSize)

              val nutrientMap = nutrientRows.map(n => n.nutrient -> n.amount).toMap
              val getNutrientValue : String => BigDecimal = id => {
                val value = nutrientMap.getOrElse(nutrientIdMap(id), BigDecimal(0))
                value * nutritionalValueMultiplier
              }

              Map(
                "calories" -> getNutrientValue("208"),
                "totalFat" -> getNutrientValue("204"),
                "saturatedFat" -> getNutrientValue("606"),
                "transFat" -> getNutrientValue("605"),
                "cholesterol" -> getNutrientValue("601"),
                "sodium" -> getNutrientValue("307"),
                "total_carbohydrate" -> getNutrientValue("205"),
                "dietary_fiber" -> getNutrientValue("291"),
                "total_sugars" -> getNutrientValue("269"),
                "added_sugar" -> 0,
                "protein" -> getNutrientValue("203"),
                "vitamin_d" -> getNutrientValue("324"),
                "calcium" -> getNutrientValue("301"),
                "iron" -> getNutrientValue("303"),
                "potassium" -> getNutrientValue("306")
              )
            }
          }
        }
        case None => Map()
      }

      IngredientNutritionInfo(
        productRow.id,
        nutritionFacts,
      )
    }
  }
}
