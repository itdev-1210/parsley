package controllers.api

import controllers._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions._

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json.Json.toJsObject
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

import scala.concurrent.ExecutionContext
import scala.util._

class Measures @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val messagesApi: MessagesApi,
  override val dbConfigProvider: DatabaseConfigProvider
) extends ApiController {

  import dbConfig.profile.api._

  def getMeasures() = SecuredApiAction.async { r =>
    // The user-facing version; should include units, since this is going
    // to be a VERY frequent call. Also aggressively cached?
    db.runInTransaction {
      for {
        allUnits <- Tables.Units.result
        measures <- Tables.Measures.result
      } yield {
        // grouping/joining in Scala, not SQL; dataset is tiny, and SQL
        // aggregation is super limiting
        val groupedUnits = allUnits.groupBy(_.measure)
        val joined = measures.map { m =>
          // get into right conceptual data structure
          (m, groupedUnits(m.id))
        }.map { case (measure, units) =>
          if (!units.exists(_.size == 1.0))
            throw HttpCodeException(
              InternalServerError, s"no base unit for ${measure.name}")

          // actually turn into (Scala array of) well-formed measure JSON serialization
          implicit val measuresFormat = JsonConversions.measuresFormat
          implicit val unitsFormat = JsonConversions.unitsFormat
          toJsObject(measure) + ("units" -> JsArray(units.map {
            toJsObject(_) - "measure"
          }))
        }

        Ok(JsArray(joined))
      }
    }
  }

  def measureFromJson(json: JsValue, id: Option[Int]=None): Try[MeasureAPIEntity] = {
    import JsonConversions.measuresAPIFormats.measureAPIFormat

    Try(json.as[MeasureAPIEntity])
    .map { apiEntity =>
      id match {
        case None => apiEntity
        case Some(originalId) => {
          val measure = apiEntity.m.copy(id=originalId)
          val units = apiEntity.units.map(_.copy(measure=originalId))
          MeasureAPIEntity(measure, units)
        }
      }
    // this can't be checked in SQL; other constraints can, so let the DB handle them
    }.filter(_.units.exists(_.size == 1)).recoverWith { case e: NoSuchElementException =>
      Failure(HttpCodeException(BadRequest, s"measure has no base unit (size == 1)"))
    }
  }

  def createMeasure() = AdminOnlyAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        MeasureAPIEntity(measure, units) <- measureFromJson(r.body).toDBIO
        measureId <- (Measures returning Measures.map(_.id)) += measure
        _ <- Units ++= units.map(_.copy(measure = measureId))
      } yield Created(Json.obj("id" -> measureId))
    }
  }

  def modifyMeasure(id: Int) = AdminOnlyAction.async(parse.json) { r =>
    db.runInTransaction {
      val measure = Measures.filter(_.id === id)
      for {
        MeasureAPIEntity(newMeasure, newUnits) <- measureFromJson(r.body, id=Some(id)).toDBIO
        numModified <- measure.update(newMeasure)
        _ <- failIf(numModified == 0, HttpCodeException(NotFound, "no such measure")).toDBIO
        // ugly and hacky - using Int < 0 (which is invalid for postgres 'SERIAL')
        // to represent new units
        unitIds = newUnits.map(_.id).filter(_ >= 0)
        // units to delete are those that exist, but aren't in the put data
        _ <- measure.units.filter(!_.id.inSet(unitIds)).delete
        // update the ones that do appear - using upsert because its API is neater
        _ <- DBIO.seq(newUnits.filter(_.id >= 0).map(Units.insertOrUpdate): _*)
        // finally, insert the new ones
        _ <- Units ++= newUnits.filter(_.id < 0)
      } yield Ok(JsString("success"))
    }
  }

  def deleteMeasure(id: Int) = AdminOnlyAction.async { r =>
    db.runInTransaction {
      val measure = Measures.filter(_.id === id)
      for {
        _ <- measure.units.delete
        numDeleted <- measure.delete
        _ <- Try {
          if (numDeleted == 0) throw HttpCodeException(NotFound, "no such measure")
        }.toDBIO
      } yield Ok(JsString("success"))
    }
  }
}
