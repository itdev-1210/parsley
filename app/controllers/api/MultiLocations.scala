package controllers.api

import controllers.Env
import models.AccountPermissionLevel._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions.HttpCodeException
import util.{AccountProfile, AccountSetup, EmailUtils, EmailTokenReference, EmailTokenVerifier, ProductSync, SupplierSync}

import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.Silhouette
import com.sendgrid.Email

import java.time.Instant

import scala.util.Try

class MultiLocations @Inject() (
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val emailUtils: EmailUtils,
  val config: Configuration,
  val synController: controllers.api.Sync,
  val productSync: ProductSync,
  val supplierSync: SupplierSync,
  val emailTokenVerifier: EmailTokenVerifier,
  val accountSetup: AccountSetup,
) extends ApiController {

  import dbConfig.profile.api._
  import synController.DBActions._

  object DBActions {
    def getDownstreamLocations(upstreamAccountId: Int) : DBIO[Seq[AccountsRow]] = for {
      downstreamAccounts <- Accounts.filter(_.multiLocationUpstream === upstreamAccountId).result
    } yield downstreamAccounts

    def validateDownstream(dsQuery: AllColumnsQuery[Tables.Accounts, AccountsRow]): DBIO[Unit] = {
      for {
        isDownstream <- dsQuery.exists.result
        _ <- failIf(
            !isDownstream,
            HttpCodeException(BadRequest, ReadableError("_error" -> "Invalid Location").toJson)
          ).toDBIO
      } yield ()
    }

    // Input: A downstream user/location id
    // Output: The supplier ids and recipe categories that this location is "subscribing" to
    def getSyncState(
      account: AllColumnsQuery[Tables.Accounts, AccountsRow],
      id : Int
    ) : DBIO[LocationSyncState] = for {
      recipes <- account.locationsSyncedRecipes.filter(_.locationId === id).map(_.recipeCategory).result
      suppliers <- account.locationsSyncedSuppliers.filter(_.locationId === id).map(_.supplierId).result
    } yield LocationSyncState(recipes, suppliers)

    /**
      * Check whether any of a filtered subset of synced suppliers need resyncing
      *
      * @param upstreamSuppliers A query filtering which suppliers should be in syncUpstream
      * @param downstreamUserIds A query filtering which users should own the downstreams
      * @return true if any of the suppliers in question are out of date
      */
    def downstreamSuppliersNeedUpdate(
      upstreamSuppliers: AllColumnsQuery[Tables.Suppliers, SuppliersRow],
      downstreamUserIds: Seq[Int]
    ) : DBIO[Boolean] = {
      val downstreamSuppliers = Suppliers.filter(_.owner inSet downstreamUserIds)
      def nullSafeSqlNonequal(v1: String, v2: String) = {
        s"(($v1 IS NULL) != ($v2 IS NULL)) OR ($v1 IS NOT NULL AND $v1 != $v2)"
      }
      for {
        mainInfosOutOfDate <- (
            upstreamSuppliers join downstreamSuppliers on (_.id === _.syncUpstream)
        ).filter { case (upstream, downstream) =>
          upstream.name =!= downstream.name
        }.exists.result


        productSourcesUnsynced <- (
            upstreamSuppliers.sourcedProducts.filter(!_.tombstone)
                joinLeft downstreamSuppliers.sourcedProducts on (_.id === _.syncUpstream)
        ).filter { case (upstreamSource, downstreamSource) =>
          downstreamSource.isEmpty
        }.exists.result

        upstreamSupplierIds <- upstreamSuppliers.map(_.id).result
        upstreamSuppliersSqlList = if (upstreamSupplierIds.isEmpty) {
          "NULL"
        } else {
          upstreamSupplierIds.mkString(",")
        }
        // TODO: Comparison needs to take into account weird SQL behavior, where
        // 1 = NULL and 1 != NULL both return NULL instead of TRUE or FALSE
        productSourcesOutOfDate <- sql"""
         SELECT upstream.id
         FROM product_sources upstream JOIN product_sources downstream ON upstream.id = downstream.sync_upstream
         WHERE upstream.supplier IN (#${upstreamSuppliersSqlList})
           AND #${supplierSync.overwrittenProductSourceFields.map(f => s"upstream.$f != downstream.$f").mkString("(", ") OR (", ")")}
        """.as[Int].map(_.nonEmpty)

      } yield mainInfosOutOfDate || productSourcesUnsynced || productSourcesOutOfDate
    }

    // Sync a single location
    def syncIndividualLocation(
      account: AllColumnsQuery[Tables.Accounts, AccountsRow],
      accountId: Int,
      downstreamId: Int
    ) : DBIO[Unit] = for {
      syncState <- getSyncState(account, downstreamId)
      _ <- syncLocation(account, accountId, downstreamId, syncState.recipes, syncState.suppliers)
    } yield Unit
  }

  def sendLocationInviteMail(
      tokenID: Int,
      tokenSecret: String,
      inviterName: String,
      inviteeEmailAddress: String,
      businessName: Option[String] = None,
  ) = {
    val applicationUrl = config.getOptional[String]("applicationUrl").get
    val contentString = s"""
      <p></p>
      <p>
        $inviterName has invited you to create a Parsley account for your location, and has shared information with this new account.
        To activate it, please <a href="$applicationUrl/auth/confirm_upstream_invite/$tokenID/$tokenSecret">click here</a>, and follow the instructions to activate your account.
      </p>
      <p></p>
      <p></p>
      <p>The Parsley Team</p>
    """
    emailUtils.send(
      new Email(inviteeEmailAddress, "Parsley User"),
      new Email("service@parsleycooks.com", "Service at Parsley Software"),
      s"Invitation to Create a Local Account${businessName.map(str => " for " + str).getOrElse("")}", contentString
    )
  }


  private sealed case class AddDownstreamRequest(
      email: String,
      upstreamRelativeName: String,
      recipes: Seq[Int],  // id from the `categories` table
      suppliers: Seq[Int]
  )
  def addDownstream = SecuredApiAction.async(parse.json) { implicit r =>

    db.runInTransaction {
      for {

        AddDownstreamRequest(email, upstreamRelativeName, recipeCategories, suppliers) <- Try(r.body.as(Json.reads[AddDownstreamRequest])).toDBIO
        emailAddress = email.trim.toLowerCase

        EmailTokenReference(tokenID, tokenSecret) <- emailTokenVerifier.createToken(EmailTokenType.UpstreamInvite, r.accountId)

        inviterInfo <- r.accountQuery.result.head // must exist

        invitedAccount <- accountSetup.create(
          accountOwner = None,
          AccountProfile(
            company = Some(upstreamRelativeName),
            multiLocationUpstream = Some(r.accountId), upstreamRelativeName = Some(upstreamRelativeName)
          ),
        )

        _ <- UpstreamInvites += UpstreamInvitesRow(0, tokenID, invitedAccount.id, r.accountId, email)
        _ <- syncLocation(r.accountQuery, r.accountId, invitedAccount.id, recipeCategories, suppliers)

        _ <- sendLocationInviteMail(
          tokenID,
          tokenSecret,
          r.identity.dbRow.firstName.map(_.capitalize).getOrElse("Someone"),
          emailAddress,
          inviterInfo.businessName,
        ).toDBIO
      } yield Ok(JsNumber(invitedAccount.id))
    }
  }

  case class UpdateDownstream(
    upstreamRelativeName: String,
    recipes: Seq[Int],  // id in the `categories` table
    suppliers: Seq[Int]
  )
  def updateDownstream(id: Int) = UserRestrictedAction().async(parse.json) { implicit r =>
    val downstreamQuery = r.accountQuery.downstreams.filter(_.id === id)
    db.runInTransaction {
      for {
        _ <- DBActions.validateDownstream(downstreamQuery)

        UpdateDownstream(upstreamRelativeName, recipes, suppliers) <- Try(r.body.as(Json.reads[UpdateDownstream])).toDBIO
        locationName = upstreamRelativeName.trim.toLowerCase
        locationNameExists <- r.accountQuery.downstreams
          .filter(_.id =!= id)
          .filter(_.upstreamRelativeName.trim.toLowerCase === locationName)
          .exists.result
        _ <- failIf(
          locationNameExists,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Location name exists").toJson)
        ).toDBIO

        _ <- downstreamQuery.map(_.upstreamRelativeName).update(Some(upstreamRelativeName))

        _ <- syncLocation(r.accountQuery, r.accountId, id, recipes, suppliers)

        inviteQuery = UpstreamInvites.filter(_.invitee === id)
        existingToken <- EmailTokens.filter(_.id in inviteQuery.map(_.emailToken)).result.head

        // decide whether to resend e-mail, and if so what token to send
        tokenToResend <- if (existingToken.used) {
          DBIO.successful(None)
        } else if (Instant.now.isAfter(existingToken.expiration)) {
          for {
            // invitation not accepted, and token expired, so reset token and send the new one
            newToken <- emailTokenVerifier.createToken(EmailTokenType.UpstreamInvite, r.accountId)
            _ <- UpstreamInvites.filter(_.emailToken === existingToken.id)
                .map(_.emailToken).update(newToken.id)
          } yield Some(newToken)
        } else {
          // invitation not accepted, token not expired, just resend it
          DBIO.successful(Some(EmailTokenReference(existingToken.id, existingToken.secret)))
        }

        // send e-mail with new token ID/secret
        _ <- tokenToResend match {
          case Some(emailToken) => for {
            businessName <- r.accountQuery.map(_.businessName).result.head // must exist
            inviteeEmail <- inviteQuery.map(_.inviteeEmail).result.head // after validation this must exist
            _ <- sendLocationInviteMail(
              emailToken.id,
              emailToken.secret,
              r.identity.dbRow.firstName.map(_.capitalize).getOrElse("Someone"),
              inviteeEmail,
              businessName,
            ).toDBIO
          } yield Unit
          case None => DBIO.successful(Unit)
        }
      } yield Ok(JsString("success"))
    }
  }

  def removeDownstream(id: Int) = UserRestrictedAction().async { implicit r =>
    val downstreamQuery = r.accountQuery.downstreams.filter(_.id === id)
    db.runInTransaction {
      for {
        _ <- DBActions.validateDownstream(downstreamQuery)

        _ <- downstreamQuery
              .map(u => (u.multiLocationUpstream, u.upstreamRelativeName))
              .update((None, None))
      } yield Ok(JsString("success"))
    }
  }

  case class LocationSyncState(
    recipes: Seq[Int], // id in the `categories` table
    suppliers: Seq[Int]
  )
  implicit val locationSyncStateFormat = Json.format[LocationSyncState]

  def getSyncState(id: Int) = UserRestrictedAction(Shared).async { r =>
    db.runInTransaction {
      for {
        syncState <- DBActions.getSyncState(r.accountQuery, id)
      } yield Ok(Json.toJsObject(syncState))
    }
  }

  // Check if any downstream supplier or product is out of date
  def canSyncLocations() = UserRestrictedAction(Shared).async { r =>
    db.runInTransaction {
      for {
        downstreamAccounts <- DBActions.getDownstreamLocations(r.accountId)
        downstreamAccountIds = downstreamAccounts.map(_.id)
        downstreamAccountsSyncState <- DBIO.sequence(downstreamAccountIds.map(downstreamAccountId =>
          for {
            syncState <- DBActions.getSyncState(r.accountQuery, downstreamAccountId)
          } yield (downstreamAccountId, syncState)
        ))

        // Check suppliers
        upstreamSuppliersId = downstreamAccountsSyncState.flatMap(_._2.suppliers).distinct
        upstreamSuppliersQuery = r.accountQuery.suppliers.filter(s => s.id.inSet(upstreamSuppliersId))
        canSyncSuppliers <- DBActions.downstreamSuppliersNeedUpdate(upstreamSuppliersQuery, downstreamAccountIds)

        canSync <- if (canSyncSuppliers) {
          DBIO.successful(true)
        } else {
          // Check products
          for {
            downstreamAccountsUpToDateState <- DBIO.sequence(downstreamAccountsSyncState.map { case (downstreamAccountId, syncState) =>
              val downstreamProducts = Accounts.filter(_.id === downstreamAccountId).products
              val upstreamSyncCategories = r.accountQuery.recipeCategories
                  .filter(_.id.inSet(syncState.recipes))
              val upstreamRecipesQuery = upstreamSyncCategories
                  .productCategories.products.distinct

              for {
                upstreamProductsQuery <- productSync.collectDependencies(upstreamRecipesQuery)

                toCopyProductsCount <- upstreamProductsQuery.filter { upstreamProd =>
                  !downstreamProducts.filter(_.syncUpstream === upstreamProd.id).exists
                }.length.result

                toExpandProductsCount <- {
                  downstreamProducts join upstreamProductsQuery on (_.syncUpstream === _.id)
                }.filter { case (downstreamProd, upstreamProd) =>
                  downstreamProd.lastSynced.isEmpty ||
                      downstreamProd.lastSynced < upstreamProd.lastModified
                }.length.result
              } yield toCopyProductsCount == 0 && toExpandProductsCount == 0
            })
          } yield downstreamAccountsUpToDateState.exists(b => !b)
        }
      } yield Ok(JsBoolean(canSync))
    }
  }

  // Attempt to sync all locations
  def syncAllLocations() = UserRestrictedAction(Shared).async { r =>
    db.runInTransaction {
      for {
        downstreamUsers <- DBActions.getDownstreamLocations(r.accountId)
        _ <- DBIO.sequence(downstreamUsers.map(u => DBActions.syncIndividualLocation(r.accountQuery, r.accountId, u.id)))
      } yield Ok(JsString("success"))
    }
  }

  case class LookupLocationRequest(name: String)
  def lookup = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        LookupLocationRequest(name) <- Try(r.body.as(Json.reads[LookupLocationRequest])).toDBIO
        locationName = name.trim.toLowerCase
        mayBeLocation <- r.accountQuery.downstreams
          .filter(_.upstreamRelativeName.trim.toLowerCase === locationName)
          .map(_.id)
          .result.headOption
      } yield Ok(mayBeLocation.map(locationId => Json.obj("id" -> locationId)).getOrElse(Json.obj()))
    }
  }
}
