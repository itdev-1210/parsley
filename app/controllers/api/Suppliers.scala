package controllers.api

import controllers._
import controllers.api.RecipeUtils._
import models.JsonConversions.suppliersAPIFormats.{SupplierImportedSource, SupplierImportedSources}
import models.JsonConversions.{SupplierAPIEntity, _}
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.{EmailUtils, ModelUtils}
import util.Exceptions._

import play.api.db.slick.DatabaseConfigProvider
import play.api.Configuration
import play.api.i18n.{Messages, MessagesApi}
import play.api.libs.json._
import play.api.libs.json.Json.WithDefaultValues
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.sendgrid.Email
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider

import java.text.DecimalFormat

import javax.inject.Inject
import java.time.Instant

import scala.concurrent.{ExecutionContext, Future, blocking}
import scala.util.{Failure, Success, Try}

class Suppliers @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val emailUtils: EmailUtils,
  val modelUtils: ModelUtils,
  val config: Configuration,
  val purchaseOrders: PurchaseOrders,
  val transactionsController: Transactions,
  val ingredientPriceHistoryController: IngredientPriceHistory,
) extends ApiController {

  import dbConfig.profile.api._
  import purchaseOrders.{ DBActions => PurchaseOrderDBActions }
  import transactionsController.{ DBActions => TransactionDBActions }
  import ingredientPriceHistoryController.{ DBActions => PriceHistoryDBActions }

  object DBActions {

    /**
     *
     * @return Two sets of tables: one of tables blocking deletion of the product,
     *         and the other requiring a soft-delete
     */
    def checkSupplierDeletionBlockers(
        account: AllColumnsQuery[Tables.Accounts, AccountsRow],
        supplierId: Int,
    ): DBIO[(Set[TableId], Set[TableId])] = {
      val supplier = account.suppliers.filter(_.id === supplierId)

      def inPurchaseOrders(transactionsFilter: AllColumnsQuery[Tables.Transactions, TransactionsRow]) =
        supplier.purchaseOrders.filter(_.xact in
            transactionsFilter.map(_.id)).exists.result

      val now = Instant.now() // stable value

      for {
        hasOpenPurchaseOrders <- inPurchaseOrders(account.transactions.open(now))
        hasClosedPurchaseOrders <- inPurchaseOrders(account.transactions.closed(now))
      } yield {
        var softUsages = Set[TableId]()
        var hardUsages = Set[TableId]()


        if (hasOpenPurchaseOrders) hardUsages += PurchaseOrders
        if (hasClosedPurchaseOrders) softUsages += PurchaseOrders

        (hardUsages, softUsages)
      }
    }

    private def softDeleteSupplier(
        supplier: AllColumnsQuery[Tables.Suppliers, SuppliersRow],
    ) = for {
      numSoftDeleted <- supplier.map(_.tombstone).update(true)

      // should no longer be sync upstream for anything
      _ <- Suppliers.filter(_.syncUpstream in supplier.map(_.id))
          .map(s => (s.syncUpstream, s.syncUpstreamOwner))
          .update((None, None))
    } yield numSoftDeleted

    private def softDeleteSources(
        sources: AllColumnsQuery[ProductSources, ProductSourcesRow], account: AllColumnsQuery[Tables.Accounts, AccountsRow]
    ) = for {
      _ <- sources.map(_.tombstone).update(true)
      // should no longer be preferred source for anything
      _ <- account.products
          .filter(_.preferredSource in sources.map(_.id))
          .map(_.preferredSource).update(None)
      // should no longer be sync upstream for anything
      _ <- ProductSources.filter(_.syncUpstream in sources.map(_.id))
          .map(s => (s.syncUpstream, s.syncUpstreamOwner))
          .update((None, None))
    } yield Unit

    def deleteSupplier(
        accountQuery: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
        id: Int,
        skipIfHardBlockers: Boolean = false,
    ): DBIO[Boolean] = for {
      (hardUsages, softUsages) <- checkSupplierDeletionBlockers(accountQuery, id)

      _ <- failIf(!skipIfHardBlockers && hardUsages.contains(PurchaseOrders),
        HttpCodeException(BadRequest,
          ReadableError("_error" ->
              Messages("api.deletesupplier.openpurchaseorders")).toJson
        )
      ).toDBIO
      // catch usages for which we don't have a good error messages
      _ <- failIf(!skipIfHardBlockers && hardUsages.nonEmpty,
        HttpCodeException(BadRequest, "supplier row in use by unknown table")
      ).toDBIO

      // deletion
      supplier = accountQuery.suppliers.filter(_.id === id)
      numDeleted <- if (hardUsages.nonEmpty) {
        if (skipIfHardBlockers) {
          DBIO.successful(0) // no-op - skipped
        } else {
          throw new Exception("didn't error out, yet hard blockers existed")
        }
      } else if (softUsages.nonEmpty) {
        for {
          numDeleted <- softDeleteSupplier(supplier)
          _ <- softDeleteSources(supplier.sourcedProducts, accountQuery)
        } yield numDeleted
      } else {
        for {
          _ <- supplier.contacts.delete
          _ <- supplier.sourcedProducts.delete
          numDeleted <- supplier.delete
        } yield numDeleted
      }
      _ <- failIf(
        numDeleted > 1,
        new RuntimeException("more than one product deleted in DBActions.deleteProduct")
      ).toDBIO
    } yield numDeleted == 1

    def fetchSupplierAPIEntity(
        id: Int,
        allowedSuppliers: AllColumnsQuery[models.Tables.Suppliers, SuppliersRow]
    ): DBIO[Option[SupplierAPIEntity]] = {
      import JsonConversions.suppliersAPIFormats.supplierAPIFormat
      val supplier = allowedSuppliers.filter(_.id === id)
      for {
        concreteSupplier <- supplier.result.head.recoverWith { case _: NoSuchElementException =>
          DBIO.failed(HttpCodeException(NotFound, "no such supplier"))
        }
        contacts <- supplier.contacts.sortBy(_.displayOrder).result
        sources <- supplier.sourcedProducts.filter(!_.tombstone).result
      } yield Option(SupplierAPIEntity(concreteSupplier, contacts, sources))
    }.recover { case e: NoSuchElementException => None }

    def importReceivablesPricingBySupplier(
        supplierId: Int,
        sources: Seq[SupplierImportedSource],
        allowedSuppliers: AllColumnsQuery[models.Tables.Suppliers, SuppliersRow]): DBIO[Seq[Int]] = {
      val supplier = allowedSuppliers.filter(_.id === supplierId)
      val productsSource = sources.map(_.source)
      val (importedSources, originalSources) = sources.partition(_.date.isDefined)
      val importedSourcesByDate = importedSources
          .groupBy(_.source.product)
          .map{ case (productId, seq) => productId -> seq.sortBy(_.date.get)(Ordering[Instant].reverse)}

      for {
        _ <- DBIO.sequence(productsSource.map(s => PriceHistoryDBActions.addBasePrice(s.product)))
        // Delete
        _ <- supplier.sourcedProducts.filter(_.id inSet productsSource.map(_.id)).delete
        // Insert untouched sources
        _ <- ProductSources ++= originalSources.map(_.source).map { s => s.copy(supplier=Some(supplierId))}
        // Insert imported sources with lastDate
        importedSourcesResult <-
            (ProductSources returning ProductSources.map(_.id)) ++=
                importedSourcesByDate.map(_._2.head.source.copy(supplier=Some(supplierId)))

        _ <- DBIO.sequence(originalSources.map(s => PriceHistoryDBActions.addPriceHistoryFromProductSource(s.source, s.date)))
        _ <- DBIO.sequence(importedSourcesByDate
            .map{ case (productId, importedSourcesSeq) =>
              PriceHistoryDBActions.addPriceHistoryFromImportReceivableSupplier(supplierId, productId, importedSourcesSeq)
            }
        )
      } yield {
        importedSourcesResult
      }
    }
  }

  /*
   * Suppliers
   */

  // Returns SORTED list by name using DB locale, since I can't be bothered
  // to figure out the mess that is Javascript collation (i.e. the Intl API)
  // right now
  def getSuppliers = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      r.accountQuery.suppliers
      .sortBy(_.name)
      .map(s => (s.id, s.name, s.tombstone))
      .result
    }.map(foundSuppliers =>
      foundSuppliers.map {
        case (id, name, tombstone) => Json.obj("id" -> id, "name" -> name, "tombstone" -> tombstone)
      }
    ).map(seq => Ok(Json.toJson(seq)))
  }

  def supplierFromJson(json: JsValue, ownerId: Int, id: Int = 0): Try[SupplierAPIEntity] = {
    import JsonConversions.suppliersAPIFormats.supplierAPIFormat

    Try(json.as[SupplierAPIEntity])
    .map { case SupplierAPIEntity(rawSupplier, rawContacts, rawSources) =>
        val supplier = rawSupplier.copy(id=id, owner=ownerId)
        val contacts =
          rawContacts.zipWithIndex.map { case (c, i) =>
            c.copy(displayOrder=i, supplierId=id)
          }
        val sources = rawSources.map { s => s.copy(supplier=Some(id)) }
        SupplierAPIEntity(supplier, contacts, sources)
    }
  }

  def lookupName = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>

    val name = (r.body \ "name").as[String]

    db.run {
      for {
        supplierId <- r.accountQuery.suppliers
            .filter(_.name.toLowerCase === name.toLowerCase)
            .map(_.id).result.headOption
      } yield {
        val jsonId = supplierId.map(JsNumber(_)).getOrElse(JsNull)
        Ok(Json.obj("id" -> jsonId))
      }
    }
  }

  def getSupplierDetails(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    import JsonConversions.suppliersAPIFormats.supplierAPIFormat
    db.run {
      for {
        maybeSupplier <- DBActions.fetchSupplierAPIEntity(id, r.accountQuery.suppliers)
        supplier <- maybeSupplier match {
          case Some(supplier) => DBIO.successful(supplier)
          case None => DBIO.failed(HttpCodeException(NotFound, "no such supplier"))
        }
      } yield Ok(Json.toJsObject(supplier))
    }


  }

  def getMultipleSupplierDetails(ids : String) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    import JsonConversions.suppliersAPIFormats.supplierAPIFormat
    val idsSeq = ids.split(',').map(_.toInt).toSeq
    db.run {
      for {
        filteredIds <- r.accountQuery.suppliers.filter(_.id inSet (idsSeq)).map(_.id).result
        results <- DBIO.sequence(filteredIds.map(id => DBActions.fetchSupplierAPIEntity(id, r.accountQuery.suppliers)))
      } yield Ok(Json.toJsObject(results.flatten.map(p => p.s.id.toString -> p).toMap))
    }
  }

  def createSupplier = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        SupplierAPIEntity(s, contacts, sources) <- supplierFromJson(r.body, r.accountId).toDBIO
        supplierId <- (Suppliers returning Suppliers.map(_.id)) += s.copy(lastModified = Some(Instant.now()))
        _ <- SupplierContacts ++= contacts.map(_.copy(supplierId=supplierId))
        _ <- ProductSources ++= sources.map(_.copy(supplier=Some(supplierId)))
        currentSources <- r.accountQuery.suppliers.filter(_.id === supplierId).sourcedProducts.result
        _ <- DBIO.sequence(currentSources.map(s => PriceHistoryDBActions.addBasePrice(s.product)))
        _ <- DBIO.sequence(currentSources.map(s => PriceHistoryDBActions.addPriceHistoryFromProductSource(s)))
      } yield supplierId
    }.map(newId => Created(Json.obj("id" -> newId)))
  }

  def modifySupplier(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      val supplier = r.accountQuery.suppliers.filter(_.id === id)
      val products = supplier.sourcedProducts.products
      for {
        SupplierAPIEntity(newSupplier, contacts, requestSources) <- supplierFromJson(r.body, r.accountId, id).toDBIO

        // collect information necessary for updating dependent inventory/par items
        oldName <- supplier.map(_.name).result.head
        newName = newSupplier.name

        numModified <- supplier.update(newSupplier.copy(lastModified = Some(Instant.now())))
        _ <- failIf(numModified == 0,
          HttpCodeException(NotFound, "no such supplier")
        ).toDBIO
        // Contacts
        _ <- supplier.contacts.delete
        _ <- SupplierContacts ++= contacts
        // Sources (new sources have negative ids)
        mySources <- supplier.sourcedProducts.result
        requestSourceIds = requestSources.map(_.id)
        _ <- DBIO.sequence(requestSources.map(_.product).map(pId => PriceHistoryDBActions.addBasePrice(pId)))
        // Delete
        _ <- DBIO.sequence(mySources.filter(s => !requestSourceIds.contains(s.id)).map { s =>
          products.filter(p => p.id === s.product && p.preferredSource === s.id).map(_.preferredSource).update(None)
        })
        _ <- supplier.sourcedProducts
            .filter(s => !s.tombstone && !s.id.inSet(requestSourceIds))
            .delete
        // Update
        _ <- DBIO.sequence(requestSources.filter(_.id >= 0).map { s => supplier.sourcedProducts.filter(_.id === s.id).update(s) })
        // Insert
        _ <-  ProductSources ++= requestSources.filter(_.id < 0)

        _ <- DBIO.sequence(requestSources.map(s => PriceHistoryDBActions.addPriceHistoryFromProductSource(s)))

        // Update supplier_names in ingredient_pars if needed
        // Keeping old names in inventory_items for archival purposes; browser-side
        // will make sure new inventories use the new name
        _ <- if (oldName != newName) {
          r.accountQuery.ingredientPars.filter(_.supplier === id).map(_.supplierName).update(newName)
        } else DBIO.successful(Unit)
      } yield Ok(JsString("success"))
    }
  }

  def deleteSupplier(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      for {
        found <- DBActions.deleteSupplier(r.accountQuery, id)
        _ <- failIf(!found,
          HttpCodeException(NotFound, "no such supplier")
        ).toDBIO
      } yield Ok(JsString("success"))
    }
  }

  def sendOrder(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>

    db.runInTransaction {

      val s3Region = config.getOptional[String]("s3.region").get
      val bucket = config.getOptional[String]("s3.bucket").get

      val supplierQuery = r.accountQuery.suppliers.filter(_.id === id)
      val products = supplierQuery.sourcedProducts.products
      val userRow = r.identity.dbRow
      for {
        reqBody <- Try(r.body.as[SendOrderRequest]).toDBIO
        s <- supplierQuery.result.headOption

        supplier <- failIfNone(s,
          HttpCodeException(BadRequest, ReadableError("_error" -> "No such supplier").toJson)
        ).toDBIO

        rawProductMeasures <- supplierQuery.sourcedProducts.products.measures.result
        rawProducts <- products.result

        sources <- supplierQuery.sourcedProducts.result
        allUnits <- Tables.Units.result

        additionContactEmails <- SupplierContacts.filter(_.supplierId === supplier.id).map(_.email).result;

        // supplier info
        supplierName = supplier.name getOrElse "Vendor"
        supplierContactName = supplier.contactName getOrElse ""
        supplierEmail = supplier.email getOrElse ""
        specialInstructionsText = supplier.instructions getOrElse ""
        specialInstructions = if (specialInstructionsText == "") "" else s"<b>Special Instructions:</b>&nbsp;$specialInstructionsText"
        // user info
        usersName = modelUtils.getUsername(userRow)
        userEmail = userRow.email
        shippingPhone = userRow.phoneNumber.getOrElse("")
        // business info
        businessName = r.account.businessName.getOrElse("")
        usersNameWithBusiness = if (businessName == "") usersName else s"$usersName at $businessName"
        shippingAddress = r.account.shippingAddress.getOrElse("").replace("\n", "<br>")
        companyLogo = r.account.companyLogo match {
          case Some(logoUrl) => s"""
            <img style="max-width: 215px; max-height: 85px; position: absolute; right: 0; top: 0;" src="https://s3-$s3Region.amazonaws.com/$bucket/$logoUrl" alt="$businessName">
          """
          case None => ""
        }
        accountOwnerEmail <- r.accountQuery.owner.map(_.email).result.headOption.map(_.flatten)

        orderDate = java.time.LocalDate.now

        // product info organized for easy access and processing
        unitsById = allUnits.map(u => (u.id: UnitId) -> u).toMap
        productsById = rawProducts.map(p => (p.id: ProductId) -> p).toMap
        sourcesById = sources.map(s => (s.id: ProductSourceId) -> s).toMap
        orderItems = reqBody.items.sortBy(x => (productsById(x.product).name).toLowerCase)
        conversionRows = rawProductMeasures.groupBy(_.product)

        lineItems = orderItems.map( item => {
          val conversions = new RecipeUtils.Conversions(
            conversionRows(item.product), unitsById, item.product
          )
          val numberFormat = new DecimalFormat("0.##")

          val source = sourcesById(item.selectedSource)
          val packageName = conversions.abbrPackageName(source)
          val productName = productsById(item.product).name
          val sku = source.sku getOrElse ""

              // markup should match the preview in SendModal.jsx
              s"""<tr>
                <td style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 30px; padding-top: 10px; padding-bottom: 10px;">$productName</td>
                <td style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">
                  ${numberFormat.format(item.numPackages)}
                </td>
                <td style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">$packageName</td>
                <td style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">$sku</td>
              </tr>\r\n"""
        })

        orderContent = lineItems.mkString

        emailBody = s"""
                    <div style="font-family: Arial, sans-serif; font-size: 12pt;">
                      <table>
                        <tr>
                          <td style="position: relative;">
                            <h1 style="font-size: 16pt;">Purchase Order</h1>
                            $companyLogo
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>Order Placed By:</b> $usersName<br>
                            <b>Order Date:</b> $orderDate<br>
                            <b>Vendor:</b> $supplierName<br>
                            <table>
                              <tbody>
                                <tr>
                                  <td style="vertical-align: text-top;"><b>Attn:</b></td>
                                  <td>${if (supplierContactName.trim.nonEmpty) s"$supplierContactName<br>" else ""}$supplierEmail</td>
                                </tr>
                                <tr>
                                  <td style="vertical-align: text-top;"><b>Cc:</b></td>
                                  <td>${additionContactEmails.flatten.fold("")(_ + ", <br>" + _)}</td>
                                </tr>
                              </tbody>
                            </table>
                            <br>
                            <b>Deliver To:</b> <br>
                            $businessName<br>
                            $shippingAddress<br>
                            <b>Tel:</b>&nbsp;$shippingPhone<br>
                            <br>
                            $specialInstructions
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <table cellspacing="0" cellpadding="0" border="0">
                            <thead>
                              <tr>
                                <th width="220" scope="col" style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 30px; padding-top: 10px; padding-bottom: 10px;">Item&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th width="80" scope="col" style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">Quantity&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th width="160" scope="col" style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">Unit&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th width="150" scope="col" style="text-align: left; border-bottom: 1px solid #ececec; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">SKU&nbsp;&nbsp;&nbsp;&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>
                              $orderContent
                            </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr><td><p></p><p></p></td></tr>
                        <tr>
                          <td>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                              <tr>
                                <td align="right" style="text-align: right">
                                  <div style="display: inline; color: #999999; text-decoration: none; font-size: 10pt; position: relative; bottom: 3px;">Sent by&nbsp;</div>
                                  <a href="https://www.parsleycooks.com?utm_medium=email&utm_source=transactional" title="Parsley Software">
                                    <img style="width: 80px; height: 26px;" width="80" height="26" src="https://app.parsleycooks.com/assets/images/logo_new.png" alt="Parsley Software">
                                  </a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </div>"""

        from = new Email(userEmail.get, usersNameWithBusiness) // error out loudly if we can't get any valid email
        to = new Email(supplierEmail, supplierName)
        subject = s"Purchase Order - $supplierName"

        ccList = accountOwnerEmail.toSeq ++ userEmail.toSeq ++ additionContactEmails.flatten
        _ <-  emailUtils.send(to, from, subject, emailBody, cc=ccList.distinct.map(e => new Email(e))).toDBIO

        purchaseOrderId <- PurchaseOrderDBActions.createPurchase(
          id,
          r.accountId,
          r.authedUserId,
          reqBody,
          true,
          false,
        )
      } yield Ok(Json.obj("id" -> purchaseOrderId))
    }
  }

  // only using the writes, but might as use the handy helper
  implicit private val sourcedIngredientFormat = FancyFormat[ProductSourcesRow](productSourcesFormat,
    mockIdFields=Seq("supplier"),
    defaultFields=Map("tombstone" -> JsBoolean(false))
  )

  def getSourcedIngredients(supplierId: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>

    db.runInTransaction {
      val ingredients = r.accountQuery.products.filter(_.ingredient)
      for {
        sources <- r.accountQuery.suppliers
            .filter(_.id === supplierId)
            .sourcedProducts.filter(!_.tombstone)
            .result
      } yield Ok(Json.toJson(sources))
    }
  }


  def importReceivablesPricing() = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        importedSuppliers <- Try(r.body.as[SupplierImportedSources]).toDBIO
        _ <- DBActions.importReceivablesPricingBySupplier(importedSuppliers.id, importedSuppliers.sources , r.accountQuery.suppliers)
      } yield {
        Ok(Json.toJson(importedSuppliers.id))
      }
    }
  }

  def importReceipt() = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        is <- Try(r.body.as[SupplierImportedSources]).toDBIO
        importedSourcesId <- DBActions.importReceivablesPricingBySupplier(is.id, is.sources, r.accountQuery.suppliers)
        productSources <- r.accountQuery.suppliers.filter(_.id === is.id).sourcedProducts.filter(_.id inSet importedSourcesId).result
        orderItems = productSources.map(ps => SendOrderItem(ps.product, 1, ps.id, ps.cost, None))
        orderRequest = SendOrderRequest(List(), orderItems.toList)

        purchaseOrderId <- PurchaseOrderDBActions.createPurchase(is.id, r.accountId, r.authedUserId, orderRequest, false, true)
        purchaseOrderQuery = r.accountQuery.purchaseOrders.filter(_.id === purchaseOrderId)
        purchaseOrder <- purchaseOrderQuery.result.head

        originalTxnQuery = r.accountQuery.transactions.filter(_.id === purchaseOrder.xact)
        originalTxn <- originalTxnQuery.result.head
        originalTxnDeltas <- originalTxnQuery.deltas.result
        deltas = originalTxnDeltas.map(del => TransactionDeltaEntity(del.copy(cashFlow = del.cashFlow.map(_ * -1)), Seq()))

        receivedTransaction = TransactionAPIEntity(originalTxn.copy(isFinalized = true), deltas)
        confirmedTxn = TransactionDBActions.cleanupForDBInsertion(receivedTransaction, r.accountId, Some(-1))
        (transactionId, _) <- TransactionDBActions.createTransactionFromAPIEntity(confirmedTxn, true)
        _ <- originalTxnQuery.map(_.finalizedxact).update(Some(transactionId))
        _ <- purchaseOrderQuery.map(_.receivedBy).update(Some(r.authedUserId))
      } yield {
        Created(Json.obj("id" -> purchaseOrderId))
      }
    }
  }


  def addImportedSources(supplierId: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      val supplier = r.accountQuery.suppliers.filter(_.id === supplierId)
      for {
        importedSources <- Try(r.body.as[Seq[ProductSourcesRow]]).toDBIO
        _ <- DBIO.sequence(importedSources.map(s => PriceHistoryDBActions.addBasePrice(s.product)))
        // Delete
        _ <- supplier.sourcedProducts.filter(_.id inSet importedSources.map(_.id)).delete
        // Add
        _ <- ProductSources ++= importedSources.map { s => s.copy(supplier=Some(supplierId)) }
        // Price history
        _ <- DBIO.sequence(importedSources.map(s => PriceHistoryDBActions.addPriceHistoryFromProductSource(s)))
      } yield Ok(JsString("updated"))
    }
  }
}
