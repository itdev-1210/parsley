package controllers.api

import util.MiscellaneousImplicits._
import controllers.Env
import controllers.api.RecipeUtils._
import models.JsonConversions._
import models.QueryExtensions._
import models._
import util.AsyncUtils._
import util.Exceptions._
import util.MiscellaneousImplicits._
import util.OptionGetPf

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json._
import play.api.libs.json.Json.toJsObject
import play.api.mvc.{ControllerComponents, Result}
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import java.time.Instant
import javax.inject.Inject

import scala.concurrent.ExecutionContext
import scala.util.Try

class IngredientUsage @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val productsController: controllers.api.Products,
  val inventoriesController: controllers.api.Inventories,
) extends ApiController {

  import dbConfig.profile.api._
  import productsController.DBActions.{getProductCategories}
  import inventoriesController.DBActions.{getCurrentInventory}

  type BillOfMaterials = Map[ProductId, Quantity]

  def fetchConsumerInfo(
    id: Int,
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    productionInfo: Map[ProductId, ProductionInfo],
  ) : DBIO[Option[IngredientConsumerAPIEntity]] = {
    val ingredient = account.products.filter(_.id === id)
    val maybeRootProductionInfo = productionInfo.get(id)
    if (maybeRootProductionInfo.isEmpty) {
      return DBIO.successful(None)
    }
    val rootProductionInfo = maybeRootProductionInfo.get

    val mainInfo = rootProductionInfo.product
    val preppedBy = rootProductionInfo.productionMethod
        .left.toOption // "recipe"
        .flatMap(_.simplePrep)

    for {
      simplePreps <- ingredient.simplePreps.map(prep => prep.name -> prep.outputProduct).result

      isPrepOf = preppedBy.map(_.inputProduct)

      (ingredientCategories, recipeCategories, inventoryCategories) <- getProductCategories(id, account.products)

      primaryConversion = rootProductionInfo.conversions.maybePrimaryConversion
      referenceQuantity = Quantity(1,
        primaryConversion.map(_.measure),
        primaryConversion.map(_.preferredUnit))

      bom <- generateProductionOrder(Map(id -> referenceQuantity), productionInfo, false, true).toDBIO

      ingredientQuantities = bom.filterValues(!_.isPreparedFromRecipe)
          .map { case (ingdt, ingdtProductionInfo) =>
            ingdt -> ingdtProductionInfo.required // required is Quantity, not boolean
          }
      nutritionServingSize = rootProductionInfo.productionMethod match {
        case Left(recipe) => if (recipe.simplePrep.isDefined) {
          Some(referenceQuantity)
        } else {
          Some(recipe.outputSize)
        }
        case _ => {
          mainInfo.nutrientServingAmount.map(_.toDouble).map(
            Quantity(_, mainInfo.nutrientServingMeasure, mainInfo.nutrientServingUnit)
          )
        }
      }
      nutritionInfo <- calculateNutritionalInfo(
        id, productionInfo,
        multiplier = {
          implicit val conversions = rootProductionInfo.conversions
          nutritionServingSize.map(_ / referenceQuantity)
        },
        ingredientQuantities
      )(account, ec)
    } yield {
      // TODO: Provide warnings when there are uncosted ingredients nested inside
      val (costedComponents, uncostedComponents) = bom.values
          .filter(!_.isPreparedFromRecipe)
          .partition(_.cashUsed.isDefined)
      val componentCosts = costedComponents.map(_.cashUsed).collect(OptionGetPf)

      val unitCost = if (costedComponents.isEmpty)
        None
      else
        Some(componentCosts.sum)

      val preferredSource = productionInfo(id).productionMethod.right.toOption.flatten
      val price = productionInfo(id).productionMethod.left.toOption.flatMap(_.price)
      val itemNumber = productionInfo(id).productionMethod.left.toOption.flatMap(_.itemNumber)

      Some(IngredientConsumerAPIEntity(
        mainInfo.copy(
          // want to make sure we send up the serving size we used to calculate
          // nutritional info, not the arbitrary "serving size" that people
          // can enter for recipes for view-only purposes
          nutrientServingAmount = nutritionServingSize.map(_.amount),
          nutrientServingMeasure = nutritionServingSize.flatMap(_.measure),
          nutrientServingUnit = nutritionServingSize.flatMap(_.unit)
        ),
        rootProductionInfo.conversions.dbRepresentation,

        ingredientCategories,
        recipeCategories,
        inventoryCategories,

        preferredSource.map(_.id),
        inHouse = productionInfo(id).productionMethod.isLeft,
        simplePreps.toMap, isPrepOf,
        referenceQuantity.measure, referenceQuantity.unit,
        uncostedComponents.map(_.productId).toSeq,
        unitCost, price,
        nutritionInfo,
        dbNutrientServingAmount = mainInfo.nutrientServingAmount.map(_.toDouble),
        itemNumber
      ))
    }
  }

  def fetchMultipleConsumerInfo(
      ids: Seq[Int], modifiedAfterDate: Option[String],
  )(
      implicit account: AllColumnsQuery[Tables.Accounts, AccountsRow]
  ): DBIO[Map[String, IngredientConsumerAPIEntity]] = {
    for {
      toFetchIds <- modifiedAfterDate match {
        case Some(date) => account.products
            .filter(p => p.id.inSet(ids) && p.lastModified >= Instant.parse(date))
            .map(_.id).result
        case None => DBIO.successful(ids)
      }
      productionInfo <- deepFetchProductionInfo(toFetchIds)
      results <- DBIO.sequence(toFetchIds.map(id => fetchConsumerInfo(id, account, productionInfo)))
    } yield results.flatten.map(i => i.main.id.toString -> i).toMap
  }

  import JsonConversions.ingredientConsumerAPIFormats.ingredientConsumerAPIWrite

  def getConsumerInfo(id: Int) = SecuredApiAction.async { r =>
    db.runInTransaction {
      implicit val ownerQuery = r.accountQuery
      for {
        productionInfo <- deepFetchProductionInfo(Set(id))
        result <- fetchConsumerInfo(id, ownerQuery, productionInfo).flatMap {
          case None => DBIO.failed(HttpCodeException(NotFound, "no such ingredient"))
          case Some(ingredientConsumer) => DBIO.successful(ingredientConsumer)
        }
      } yield Ok(toJsObject(result))
    }
  }

  def getMultipleConsumerInfo(ids: String, modifiedAfterDate: Option[String]) = SecuredApiAction.async { r =>
    db.runInTransaction {
      val idsSeq = ids.split(",").map(_.toInt).toSeq
      for {
        result <- fetchMultipleConsumerInfo(idsSeq, modifiedAfterDate)(r.accountQuery)
      } yield Ok(toJsObject(result))
    }
  }

  def getActiveIngredientPars(
    userIngredientPars: AllColumnsQuery[models.Tables.IngredientPars, IngredientParsRow]
  ): DBIO[BillOfMaterials] = userIngredientPars.filter(
    p => p.amount.getOrElse(0.0) > 0.0
  ).result.map { pars =>
    // NOTE: assumes only one par per product for this user!!
    pars.map(p => (p, p.amount)).collect {
      case (p, Some(amount)) => p.product -> Quantity(amount, p.measure, p.unit)
    }.toMap
  }

  def getLatestInventoryBOM(
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    allowedProductIds: Iterable[ProductId]
  ) : DBIO[Map[ProductId, Quantity]] = for {
    latestInventoryId <- account.inventories.sortBy(_.time.desc).map(_.id).result.headOption
    itemsFromLatestInventory <- account.inventoryItems.filter(i =>
      i.inventory === latestInventoryId && i.product.inSet(allowedProductIds) && i.amount.getOrElse(0.0) > 0.0
    ).result
  } yield itemsFromLatestInventory.map(p => (p, p.amount)).collect {
    case (p, Some(amount)) => p.product -> Quantity(amount, p.measure, p.unit)
  }.toMap

  def getCurrentInventoryBOM(
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    allowedProductIds: Iterable[ProductId]
  ) : DBIO[Map[ProductId, Quantity]] = for {
    currentInventory <- getCurrentInventory(account, None, Some(allowedProductIds))
  } yield currentInventory.map(i => i.product -> i.quantity).toMap

  // fetch production info, aggregate quantities, add par levels, subtract latest inventory
  def processBOM(
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    rootRequirements: Map[ProductId, Seq[Quantity]],
    addParLevels: Boolean,
    subtractLatestInventory: Boolean,
    subtractCurrentInventory: Boolean
  ) : DBIO[(Map[ProductId, ProductionInfo], BillOfMaterials)] = for {
    parBOM <- if (addParLevels) {
      getActiveIngredientPars(account.ingredientPars)
    } else {
      DBIO.successful(Map[ProductId, Quantity]())
    }
    productIds = rootRequirements.keys ++ parBOM.keys
    productionInfo <- deepFetchProductionInfo(productIds)(account, ec)

    subtractLatestInventoryBOM <- if (subtractLatestInventory) {
      getLatestInventoryBOM(account, productionInfo.keys)
    } else {
      DBIO.successful(Map[ProductId, Quantity]())
    }
    subtractCurrentInventoryBOM <- if (subtractCurrentInventory) {
      getCurrentInventoryBOM(account, productionInfo.keys)
    } else {
      DBIO.successful(Map[ProductId, Quantity]())
    }
  } yield {
    implicit val allConversions = productionInfo.mapValues(_.conversions)
    val rootBOM = rootRequirements.map { case (productId, quantities) =>
      implicit val conversions = allConversions(productId)
      productId -> quantities.reduce(_ + _)
    }

    val processedBOM = rootBOM + parBOM - subtractLatestInventoryBOM - subtractCurrentInventoryBOM
    (productionInfo, processedBOM)
  }

  case class ProductionOrderRequestItem(product: ProductId, quantity: Quantity)
  case class ProductionOrderRequest(
      orders: Seq[ProductionOrderRequestItem],
      batched: Boolean,
      includePar: Boolean,
      subtractLatestInventory: Boolean,
      subtractCurrentInventory: Boolean
  )
  implicit val productionOrderRequestItemFormat = Json.format[ProductionOrderRequestItem]
  implicit val productionOrderRequestFormat = Json.format[ProductionOrderRequest]

  def getProductionOrder() = SecuredApiAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        ProductionOrderRequest(orders, batched, includePar, subtractLatestInventory, subtractCurrentInventory) <- Try(r.body.as[ProductionOrderRequest]).toDBIO

        orderRequirements = orders.map(item => item.product -> Seq(item.quantity)).toMap
        (productionInfo, processedBOM) <- processBOM(r.accountQuery, orderRequirements, includePar, subtractLatestInventory, subtractCurrentInventory)
        res <- returnProductionOrder(processedBOM, productionInfo, batched).toDBIO
      } yield res
    }
  }


  import Quantity.apiFormat
  case class ShoppingListEntry(
      product: ProductId, name: String,
      totalRequired: Quantity, totalPurchased: Quantity,
      source: Option[ProductSourcesRow]
  )
  import JsonConversions.productsAPIFormats.sourceAPIFormat
  private implicit val shoppingListProductSourceFormat = FancyFormat(productSourcesFormat,
    mockFields = Map("tombstone" -> JsBoolean(false)),
  )
  implicit val shoppingListFormat = Json.format[ShoppingListEntry]

  case class RecipeListEntry(
      product: ProductId, name: String,
      isSimplePrep: Boolean,
      isAdvancePrep: Boolean,
      totalRequired: Quantity, totalProduced: Quantity,
      billOfMaterials: Map[String, Quantity] // BillOfMaterials type, but with JSON-friendly keys
  )
  implicit val recipeListFormat = Json.format[RecipeListEntry]

  case class ProductionOrderAPIEntity(
      totalCost: BigDecimal,
      shoppingList: Seq[ShoppingListEntry],
      recipeList: Seq[RecipeListEntry]
  )
  implicit val productionOrderFormat = Json.format[ProductionOrderAPIEntity]

  def returnProductionOrder(
      rootBOM: BillOfMaterials,
      productionInfo: Map[ProductId, ProductionInfo],
      withBatching: Boolean
  ): Try[Result] = generateProductionOrder(rootBOM, productionInfo, withBatching = withBatching)
      .map { bom =>
        val (recipeList, shoppingList) = bom.values.partition(_.isPreparedFromRecipe)
        Ok(toJsObject(ProductionOrderAPIEntity(
          totalCost = shoppingList.map(_.cashUsed.getOrElse(BigDecimal(0))).sum,
          shoppingList = shoppingList.map { bomEntry =>
            ShoppingListEntry(bomEntry.productId, bomEntry.name, bomEntry.required, bomEntry.procured, bomEntry.source)
          }.toSeq,
          recipeList = recipeList.map { bomEntry =>
            RecipeListEntry(
              bomEntry.productId, bomEntry.name,
              bomEntry.isSimplePrep,
              bomEntry.isAdvancePrep,
              bomEntry.required, bomEntry.procured,
              bomEntry.ingredientsUsed.getOrElse(Map()).map { case (productId, quantity) =>
                (productId.toString, quantity)
              }
            )
          }.toSeq
        )))
      }

  case class BulkProductionOrderRequest(
      orders: Seq[Int],
      batched: Boolean,
      includePar: Boolean,
      subtractLatestInventory: Boolean,
      subtractCurrentInventory: Boolean,
  )
  implicit val bulkProductionOrderRequestFormat = Json.format[BulkProductionOrderRequest]

  def bulkProductionOrder() = SecuredAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        BulkProductionOrderRequest(orders, batched, includePar, subtractLatestInventory, subtractCurrentInventory) <- Try(r.body.as[BulkProductionOrderRequest]).toDBIO

        rawOrderItems <- r.accountQuery.orders
            .filter(_.id.inSet(orders))
            .transaction.deltas.filter(_.causedBy.isEmpty)
            .result
        orderRequirements = rawOrderItems
            .groupBy(_.product)
            .collect { case (Some(productId), deltas) =>
              (productId: ProductId) -> deltas.map(d => Quantity(d.amount, d.measure, d.unit))
            }

        (productionInfo, processedBOM) <- processBOM(r.accountQuery, orderRequirements, includePar, subtractLatestInventory, subtractCurrentInventory)

        res <- returnProductionOrder(processedBOM, productionInfo, batched).toDBIO
      } yield res
    }
  }
}
