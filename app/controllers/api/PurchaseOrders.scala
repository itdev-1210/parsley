package controllers.api

import controllers.Env
import models.JsonConversions._
import models.PurchaseOrdersRow
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions._
import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json.{JsArray, JsNull, JsNumber, JsString, JsValue, Json, JsBoolean}
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._
import com.mohiva.play.silhouette.api.Silhouette
import javax.inject.Inject
import java.time.{Instant, LocalDateTime, ZoneOffset}
import java.time.temporal.ChronoUnit
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.TimeZone

import slick.jdbc.{GetResult, SetParameter}

import scala.concurrent.ExecutionContext
import scala.util.Try

class PurchaseOrders @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val transactionsController: Transactions,
  val ingredientPriceHistoryController: IngredientPriceHistory,
) extends ApiController {

  import dbConfig.profile.api._
  import transactionsController.{ DBActions => TransactionDBActions }
  import ingredientPriceHistoryController.{ DBActions => PriceHistoryDBActions }

  def listPurchaseOrders = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val purchaseOrdersQuery = r.accountQuery.purchaseOrders
      for {
        purchaseOrders <- purchaseOrdersQuery.result
        orders <- purchaseOrdersQuery.orders.result
        userIdToNamesList <- DBIO.sequence((purchaseOrders.flatMap(p => p.receivedBy) ++: purchaseOrders.map(_.createdBy)).toSet.map { relatedUserId: Int =>
          for {
            userNames <- Users.filter(_.id === relatedUserId).map(u => (u.firstName, u.lastName)).result.head
          } yield (relatedUserId -> userNames)
        }.toList)
        originalTransactions <- Transactions.filter(_.id inSet purchaseOrders.map(_.xact)).result
        confirmedTransactions <- Transactions.filter(_.id inSet originalTransactions.flatMap(_.finalizedxact)).map(ct => (ct.id, ct.time)).result
        originalToConfirmedTxnMap = originalTransactions.map(ot => ot.id -> confirmedTransactions.filter(ct => ct._1 == ot.id)).toMap
        // map purchase order to finalized time
        userIdToNamesMap = userIdToNamesList.toMap
        purchaseOrderToReDr = purchaseOrders.map(po =>
          (po.id: Int) -> originalTransactions
              .find(ot => ot.id == po.xact).forall(_.isFinalized)
        ).toMap
        purchaseOrderToTxnMap = purchaseOrders.map(po =>
          (po.id: Int) -> {
            if (purchaseOrderToReDr.get(po.id).getOrElse(false))
              originalTransactions
              .find(ot => ot.id == po.xact)
              .map(ot => ot.time).headOption
            else
              originalTransactions
              .find(ot => ot.id == po.xact)
              .flatMap { ot => confirmedTransactions
                .filter(ct => ot.finalizedxact.contains(ct._1))
                .map(ct => ct._2)
                .headOption
              }
          }
        ).toMap

      } yield {

        val ordersByPurchaseOrder = orders.groupBy(_.purchaseOrder)
        val entries = purchaseOrders.map { po => {
            Json.obj("id" -> po.id,
              "xact" -> po.xact, "supplier" -> po.supplier, "emailedAt" -> po.emailedAt,
              "printedAt" -> po.printedAt, "orders" -> ordersByPurchaseOrder.getOrElse(po.id, Seq()).map(_.customerOrder),
              "receivedAt" -> purchaseOrderToTxnMap.get(po.id),
              "receiverFirstName" -> po.receivedBy.flatMap(receiverId =>
                userIdToNamesMap.get(receiverId).flatMap(_._1.map(_.capitalize))
              ),
              "receiverLastName" -> po.receivedBy.flatMap(receiverId =>
                userIdToNamesMap.get(receiverId).flatMap(_._2.map(_.capitalize))
              ),
              "receivedDirectly" -> purchaseOrderToReDr.get(po.id),
              "creatorFirstName" -> userIdToNamesMap.get(po.createdBy).flatMap(_._1.map(_.capitalize)),
              "creatorLastName" -> userIdToNamesMap.get(po.createdBy).flatMap(_._2.map(_.capitalize)),
              "importedAt" -> po.importedAt,
              "isImported" -> po.imported,
            )
          }
        }
        Ok(JsArray(entries))
      }
    }
  }

 def getPurchaseHistoryReport(period: String, purchaseType: String, purchaseValue: Option[Int] = None) =  UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
   db.runInTransaction {
     val now = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC)
     val endDate = now.toInstant(ZoneOffset.UTC)
     val (formatter, startDate, periodLabel) = period match  {
       case "month" =>
         (DateTimeFormatter.ofPattern("MMM/yy").withZone(ZoneOffset.UTC),
             now.minus(12, ChronoUnit.MONTHS).toInstant(ZoneOffset.UTC),
             period)
       case "week" =>
         (DateTimeFormatter.ofPattern("'week' w/YY").withZone(ZoneOffset.UTC),
             now.minus(12, ChronoUnit.WEEKS).toInstant(ZoneOffset.UTC),
             period)
       case "week-to-date" =>
         (DateTimeFormatter.ofPattern("E").withZone(ZoneOffset.UTC),
             now.minus(now.getDayOfWeek.getValue - 1, ChronoUnit.DAYS).toInstant(ZoneOffset.UTC),
             "day")
       case "month-to-date" =>
         (DateTimeFormatter.ofPattern("d/MMM").withZone(ZoneOffset.UTC),
             now.minus(now.getDayOfMonth - 1, ChronoUnit.DAYS).toInstant(ZoneOffset.UTC),
             "day")
       case "year-to-date" =>
         (DateTimeFormatter.ofPattern("w/YY").withZone(ZoneOffset.UTC),
             now.minus(now.getDayOfYear - 1, ChronoUnit.DAYS).toInstant(ZoneOffset.UTC),
             "week")
       case _ =>
         (DateTimeFormatter.ofPattern("d/MMM").withZone(ZoneOffset.UTC),
             now.minus(12, ChronoUnit.DAYS).toInstant(ZoneOffset.UTC),
             "day")
     }

     val defaultJoin =
       (r.accountQuery.transactions
           join r.accountQuery.transactions on (_.finalizedxact === _.id)
           join TransactionDeltas on (_._2.id === _.xact)
           join r.accountQuery.purchaseOrders on (_._1._1.id === _.xact))
           .filter(_._1._1._1.xactType === TransactionType.PurchaseOrder)
           .filter(_._1._1._2.time.between(startDate, now.toInstant(ZoneOffset.UTC)))

     val dynXactsQuery = (purchaseType, purchaseValue) match {
       case ("Supplier", Some(supplierId)) =>
         defaultJoin.filter(_._2.supplier === supplierId).map{
           case (((_, finalizedXact), xactsDeltas), _) => (finalizedXact, xactsDeltas)
         }
       case ("Ingredient", Some(productId)) =>
         defaultJoin.filter(_._1._2.product === productId).map{
           case (((_, finalizedXact), xactsDeltas), _) => (finalizedXact, xactsDeltas)
         }
       case ("Category", Some(categoryId)) =>
         (defaultJoin join ProductCategories on (_._1._2.product === _.product))
           .filter(_._2.category === categoryId).map{
           case ((((_, finalizedXact), xactsDeltas), _), _) => (finalizedXact, xactsDeltas)
         }
       case _ => defaultJoin.map{
         case (((_, finalizedXact), xactsDeltas), _) => (finalizedXact, xactsDeltas)
       }
     }

     implicit val setInstantParameter = SetParameter[Instant] {
       case (instant, params) => {
         val ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
         val timestamp = Timestamp.valueOf(ldt)
         params.setTimestamp(timestamp)
       }
     }
     implicit val getInstantResult = GetResult( r => r.nextTimestamp().toInstant)

     for {
       dateRange <- sql"""
           SELECT generate_series(date_trunc($periodLabel, $startDate::timestamp),
           date_trunc($periodLabel, $endDate::timestamp), ${'1' + periodLabel}::interval) AS date
       """.as[Instant]
       dateCount <- dynXactsQuery
           .groupBy(_._1.time.trunc(periodLabel))
             .map { case (date, xacts) =>
               (date, xacts.map(_._2.cashFlow).sum.abs)
             }.result
     } yield  {
       val map = dateCount.toMap
       val (labels, values) = dateRange.map { date =>
         (formatter.format(date), map.getOrElse(date, Some(BigDecimal(0))))
       }.unzip
       Ok(Json.obj(
         "labels" -> Json.toJson(labels),
         "values" -> Json.toJson(values)))
     }
   }
 }

 def getPurchaseOrderDetails(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      import purchaseOrdersAPIFormats.purchaseOrdersAPIFormat
      for {
        apiEntity <- DBActions.fetchPurchaseOrder(id, r.accountQuery)
        receiverNames <- Users.filter(_.id === apiEntity.main.receivedBy).map(u => (u.firstName, u.lastName)).result.headOption
        creatorNames <- if (apiEntity.main.receivedBy.contains(apiEntity.main.createdBy)) {
          DBIO.successful(receiverNames)
        } else {
          Users.filter(_.id === apiEntity.main.createdBy)
              .map(u => (u.firstName, u.lastName))
              .result.headOption
        }
        originalTransactions <- Transactions.filter(_.id === apiEntity.main.xact).result
        receivedDirectly = originalTransactions.forall(_.isFinalized)
      } yield Ok(
        Json.toJsObject(apiEntity)
        + ("receiverFirstName" -> receiverNames.flatMap(_._1).map(fName => JsString(fName.capitalize)).getOrElse(JsNull))
        + ("receiverLastName" -> receiverNames.flatMap(_._2).map(lName => JsString(lName.capitalize)).getOrElse(JsNull))
        + ("creatorFirstName" -> creatorNames.flatMap(_._1).map(fName => JsString(fName.capitalize)).getOrElse(JsNull))
        + ("creatorLastName" -> creatorNames.flatMap(_._2).map(lName => JsString(lName.capitalize)).getOrElse(JsNull))
        + ("receivedDirectly" -> JsBoolean(receivedDirectly))
      )
    }
  }

  def purchaseOrderFromJson(
    json: JsValue, ownerId: Int, createdBy: Int, existingPurchaseOrderId: Option[Int] = None
  ): Try[PurchaseOrderAPIEntity] = {
    import purchaseOrdersAPIFormats.purchaseOrdersAPIFormat

    val purchaseOrderId = existingPurchaseOrderId.getOrElse(-1)
    for {
      rawEntity <- Try(json.as[PurchaseOrderAPIEntity])
    } yield {

      val cleanedTransaction = TransactionDBActions.cleanupForDBInsertion(
        rawEntity.transaction, ownerId
      )

      val cleanedMain = rawEntity.main.copy(id=purchaseOrderId, owner=Option(ownerId), createdBy=createdBy)

      PurchaseOrderAPIEntity(cleanedMain, cleanedTransaction, rawEntity.orders)
    }
  }

  def createPurchaseOrder(supplier: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        reqBody <- Try(r.body.as[SendOrderRequest]).toDBIO

        _ <- if (reqBody.receivedDirectly) {
          val productSourcesToUpdate = reqBody.items
            .map(item => {
              val productSourceId = item.selectedSource
              productSourceId -> item.unitCost
            })
            .toMap
          for {
            _ <- DBIO.sequence(productSourcesToUpdate.map(kv =>
              r.accountQuery.suppliers.sourcedProducts.filter(_.id === kv._1).map(_.cost).update(kv._2)
            ))
            productSources <- r.accountQuery.suppliers.sourcedProducts.filter(_.id inSet productSourcesToUpdate.keys).result
            _ <- DBIO.sequence(productSources.map(ps =>
              PriceHistoryDBActions.addPriceHistoryFromProductSource(ps))
            )
          } yield Unit
        } else DBIO.successful(Unit)

        purchaseOrderId <- DBActions.createPurchase(supplier, r.accountId, r.authedUserId, reqBody, false, false)

      } yield {
        Created(Json.obj("id" -> purchaseOrderId))
      }
    }
  }

  def receivePurchaseOrder(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    import receivePurchaseOrderAPIFormats.{receivePurchaseOrderAPIReads, toTransactionApiEntity}
    db.runInTransaction {
      for {
        ReceivePurchaseOrderAPIEntity(supplier, receivedTransaction) <- Try(r.body.as[ReceivePurchaseOrderAPIEntity]).toDBIO
        purchaseOrderQuery = r.accountQuery.purchaseOrders.filter(_.id === id)
        mayBePurchaseOrder <- purchaseOrderQuery.result.headOption
        _ <- failIfNone(mayBePurchaseOrder,
          HttpCodeException(BadRequest, ReadableError("_error" -> "No such order").toJson)
        ).toDBIO

        purchaseOrder = mayBePurchaseOrder.get
        originalTxnQuery = r.accountQuery.transactions.filter(_.id === purchaseOrder.xact)
        originalTxn <- originalTxnQuery.result.head // must exist
        _ <- failIf(
          originalTxn.finalizedxact.nonEmpty,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Purchase Order is already received").toJson)
        ).toDBIO

        cleanedTxn = TransactionDBActions.cleanupForDBInsertion(receivedTransaction, r.accountId)
        confirmedTxn = cleanedTxn.copy(main = cleanedTxn.main.copy(isFinalized = true))
        (transactionId, deltasId) <- TransactionDBActions.createTransactionFromAPIEntity(confirmedTxn, true)

        productSourcesToUpdate = confirmedTxn.deltas
          .filter(d => d.main.cashFlow.nonEmpty && d.main.productSource.nonEmpty && d.main.amount > 0)
          .map(d => {
            val productSourceId = d.main.productSource.get
            productSourceId -> Some(receivedTransaction.deltas.find(_.row.product == d.main.product).get.unitCost)
          })
          .toMap

        _ <- DBIO.sequence(productSourcesToUpdate.map(kv =>
          r.accountQuery.suppliers.sourcedProducts.filter(_.id === kv._1).map(_.cost).update(kv._2)
        ))
        _ <- originalTxnQuery.map(_.finalizedxact).update(Some(transactionId))
        _ <- purchaseOrderQuery.map(_.receivedBy).update(Some(r.authedUserId))

        productSources <- r.accountQuery.suppliers.sourcedProducts.filter(_.id inSet productSourcesToUpdate.keys).result
        _ <- DBIO.sequence(productSources.map(ps =>
          PriceHistoryDBActions.addPriceHistoryFromProductSource(ps))
        )
      } yield {
        Ok(JsNumber(id))
      }
    }
  }

  object DBActions {


    def fetchPurchaseOrder(purchaseOrderId: Int, accountQuery: AllColumnsQuery[Tables.Accounts, AccountsRow]): DBIO[PurchaseOrderAPIEntity] = {
      val purchaseOrders = accountQuery.purchaseOrders.filter(_.id === purchaseOrderId)
      for {
        mayBeInfo <- purchaseOrders.result.headOption
        mainInfo <- mayBeInfo match {
          case Some(info) => DBIO.successful(info)
          case None => DBIO.failed(HttpCodeException(NotFound, "no such purchase order"))
        }

        xact <- TransactionDBActions.fetchTransaction(mainInfo.xact, accountQuery)
            // foreign key constraint should guarantee success
            .map(_.get)
        confirmedXact <- xact.main.finalizedxact match {
          case Some(fxact) => TransactionDBActions.fetchTransaction(fxact, accountQuery)
          case None => DBIO.successful(None)
        }
        cleanedXact = confirmedXact.map(cx => cx.copy(deltas = cx.deltas.map(d => d.copy(main = d.main.copy(cashFlow = d.main.cashFlow.map(v => -1 * v))) )))
        customerOrderIds <- PurchaseOrderCustomerOrders.filter(_.purchaseOrder === purchaseOrderId).map(_.customerOrder).result
      } yield PurchaseOrderAPIEntity(mainInfo, xact, customerOrderIds, cleanedXact)
    }

    def createPurchase(
      supplierId: Int, ownerId: Int, creatorId: Int, orderRequest: SendOrderRequest, isEmailed: Boolean, isImported: Boolean
    ): DBIO[Int] = {
      val accountQuery = Accounts.filter(_.id === ownerId)
      val supplierSourcesQuery = accountQuery.suppliers.filter(_.id === supplierId).sourcedProducts
      for {
        sources <- supplierSourcesQuery.result
        sourcesById = sources.map(s => (s.id: RecipeUtils.ProductSourceId) -> s).toMap
        rawPurchaseOrderMain = if (orderRequest.receivedDirectly)
          PurchaseOrdersRow(-1, Some(ownerId), -1, supplierId, receivedBy = Some(creatorId), createdBy = creatorId)
        else
          PurchaseOrdersRow(-1, Some(ownerId), -1, supplierId, createdBy = creatorId)
        purchaseOrderMain = if (isEmailed) {
          rawPurchaseOrderMain.copy(emailedAt = Some(Instant.now()))
        } else if (isImported) {
          rawPurchaseOrderMain.copy(importedAt = Some(Instant.now()), imported = true)
        } else {
          rawPurchaseOrderMain.copy(printedAt = Some(Instant.now()))
        }
        txnEntity = TransactionAPIEntity(
          TransactionsRow(id = 0, owner = ownerId, time = Instant.now(), isFinalized = orderRequest.receivedDirectly, xactType = TransactionType.PurchaseOrder),
          orderRequest.items.map { orderItem =>
            val source = sourcesById(orderItem.selectedSource)
            val effectiveSize = RecipeUtils.effectivePackageSize(source)
            val main = TransactionDeltasRow(-1, -1, ownerId, Some(orderItem.product),
              effectiveSize.amount * orderItem.numPackages,
              effectiveSize.measure,
              effectiveSize.unit,
              productSource = Some(source.id),
              cashFlow = orderItem.cashFlow,
            )
            TransactionDeltaEntity(
              main = main,
              Seq.empty
            )
          },
        )
        purchaseOrderEntity = PurchaseOrderAPIEntity(purchaseOrderMain, TransactionDBActions.cleanupForDBInsertion(txnEntity, ownerId), orderRequest.orders)
        purchaseOrderId <- DBActions.createPurchaseOrderFromAPIEntity(
          purchaseOrderEntity,
          accountQuery.purchaseOrders
        )
      } yield purchaseOrderId
    }
    /**
    * @return The ID of the newly-created menu
    */
    def createPurchaseOrderFromAPIEntity(
      apiEntity: PurchaseOrderAPIEntity,
      purchaseOrdersQuery: AllColumnsQuery[Tables.PurchaseOrders, PurchaseOrdersRow],
    ): DBIO[Int] = {
      val PurchaseOrderAPIEntity(main, transaction, orders, _) = apiEntity
      // search for purchases made to this supplier for any of given orders
      val prevPurchaseOrderIdQuery = purchaseOrdersQuery.filter(_.supplier === main.supplier).orders
        .filter(_.customerOrder inSet orders)
        .map(_.purchaseOrder)
      for {
        // clean previous purchase data
        prevPurchaseOrderId <- prevPurchaseOrderIdQuery.result

        _ <- if (prevPurchaseOrderId.nonEmpty) {
          deletePurchaseOrder(purchaseOrdersQuery.filter(_.id inSet prevPurchaseOrderId))
        } else {
          DBIO.successful(0)
        }

        (transactionId, _) <- TransactionDBActions.createTransactionFromAPIEntity(transaction, true, !transaction.main.isFinalized)
        purchaseOrderId <- (PurchaseOrders returning PurchaseOrders.map(_.id)) += main.copy(
          xact=transactionId
        )
        _ <- PurchaseOrderCustomerOrders ++= orders.map( o =>
          PurchaseOrderCustomerOrdersRow(customerOrder=o, purchaseOrder=purchaseOrderId))

      } yield purchaseOrderId
    }

    def deletePurchaseOrder(purchaseOrderQuery: AllColumnsQuery[Tables.PurchaseOrders, PurchaseOrdersRow]): DBIO[Int] = {

      val purchaseOrderIdQuery = purchaseOrderQuery.map(_.id)
      val purchaseOrderXactIdQuery = purchaseOrderQuery.map(_.xact)
      for {

        purchaseOrderXactIds <- purchaseOrderXactIdQuery.result

        _ <- TransactionDeltas.filter(_.xact in purchaseOrderXactIdQuery).delete
        _ <- PurchaseOrderCustomerOrders.filter(_.purchaseOrder in purchaseOrderIdQuery).delete
        numPurchaseOrderDeleted <- purchaseOrderQuery.delete
        _ <- Transactions.filter(_.id inSet purchaseOrderXactIds).delete
      } yield numPurchaseOrderDeleted
    }
  }

}
