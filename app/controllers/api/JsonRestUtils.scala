package controllers.api

import util.Exceptions._

import play.api.Logger
import play.api.libs.json._
import play.api.mvc.Result
import play.api.mvc.Results._

import com.newrelic.api.agent.NewRelic

import scala.collection.JavaConverters._

trait JsonRestUtils
    extends DefaultReads with DefaultWrites
    with GeneratedReads with GeneratedWrites {

  implicit class statusExtension(status: Status) {
    def parsleyFormattedError(message: String) =
      status(Json.obj("error" -> message))
  }

  /**
    * Should be the LAST in the recovery chain, since it wraps other exceptions in a specially-formatted
    * 500 response
    */
  val handleAPIError: PartialFunction[Throwable, Result] = {
    case httpError: HttpCodeException => httpError.status(httpError.body)
    case jsonParseError: JsResultException =>
      BadRequest(Json.obj("error" -> JsError.toJson(jsonParseError.errors)))
    case sqlException: java.sql.SQLException => {
      NewRelic.noticeError(sqlException.getNextException, Map("controller" -> getClass.toString).asJava)
      Logger.error(s"sqlException in APIEndpoint $getClass: ", sqlException)
      Logger.error(s"getNextException: ", sqlException.getNextException)
      InternalServerError.parsleyFormattedError(sqlException.getMessage)
    }
    case otherError => {
      NewRelic.noticeError(otherError, Map("controller" -> getClass.toString).asJava)
      Logger.error(s"error in API endpoint $getClass: ", otherError)
      InternalServerError.parsleyFormattedError(otherError.getMessage)
    }
  }

  case class ReadableError(fieldErrors: (String, String)*) {
    val asMap = fieldErrors.toMap.mapValues(JsString)

    def toJson: JsObject = Json.obj("readableErrors" -> JsObject(asMap))
  }
}

object JsonRestUtils extends JsonRestUtils
