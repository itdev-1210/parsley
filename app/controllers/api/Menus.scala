package controllers.api

import controllers.Env
import models.{AccountPermissionLevel, AccountsRow}
import models.JsonConversions.{MenuAPIEntity, MenuSectionAPIEntity, _}
import models.QueryExtensions._
import models.Tables._
import util.AsyncUtils._
import util.Exceptions._

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

import scala.concurrent.ExecutionContext
import scala.util.Try

class Menus @Inject()(
    override val controllerComponents: ControllerComponents,
    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider
) extends ApiController {

  import dbConfig.profile.api._

  def listMenus = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      for {
        menus <- r.accountQuery.menus.filter(_.userVisible).map(m => (m.id, m.name)).result
      } yield {
        val entries = menus.map { case (id, name) => Json.obj("id" -> id, "name" -> name) }
        Ok(JsArray(entries))
      }
    }
  }

  /**
    * the methods in this object handle matters *after* the JSON has been parsed. For the use of
    * external modifiers of the menu DB objects (namely, orders)
    */
  object DBActions {
    /**
      *
      * @param id
      * @param account
      * @return The Some(apiEntity) if a menu exists under that ID and user, otherwise None
      */
    def fetchMenuAPIEntity(
        id: Int,
        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow]
    ): DBIO[Option[MenuAPIEntity]] = {
      val menu = account.menus.filter(_.id === id)
      for {
        mainInfo <- menu.result.head.recoverWith { case _: NoSuchElementException =>
          DBIO.failed(HttpCodeException(NotFound, "no such product"))
        }
        sections <- menu.sections.sortBy(_.displayOrder).result
        // interleaves items from different sections
        items <- menu.sections.items.sortBy(_.displayOrder).result
      } yield {
        // groupBy preserves ordering within group, so the previous weird sorting by only the within-section
        // display order works itself out
        val itemsBySection = items.groupBy(_.section)
        val sectionAPIEntities = sections.map(s =>
          MenuSectionAPIEntity(s, itemsBySection.getOrElse(s.id, Nil))
        )
        Some(MenuAPIEntity(mainInfo, sectionAPIEntities))
      }
    }.recover { case e: NoSuchElementException => None }

    /**
      * Sets up the menu  with the appropriate ownerId and internal ID references
      *
      * @param rawEntity
      * @param ownerId
      * @param maybeId
      * @return
      */
    def cleanupForDBInsertion(
        rawEntity: MenuAPIEntity, ownerId: Int, maybeId: Option[Int]=None, userVisible: Boolean=true
    ): MenuAPIEntity = {
      // cleanup for insertion is a bit of a royal pain, since owner ID is included at every level for in-DB
      // business-logic verification. but we have to drill down anyway to set displayOrder
      val cleanedSections = rawEntity.sections.zipWithIndex.map { case (section, sectionDisplayOrder) =>
        val cleanedItems = section.items.zipWithIndex.map { case (item, itemDisplayOrder) =>
          item.copy(displayOrder = itemDisplayOrder, owner = ownerId)
        }

        MenuSectionAPIEntity(
          section.main.copy(owner = ownerId, displayOrder = sectionDisplayOrder),
          cleanedItems
        )
      }
      MenuAPIEntity(
        rawEntity.main.copy(owner = ownerId,
          id = maybeId.getOrElse(-1),
          userVisible=userVisible),
        cleanedSections)

    }

    // it is the caller's responsibility to ensure ownership in sections and of menu specified by menuId
    // is set up correctly. Also displayOrder
    def setupMenuSections(sections: Seq[MenuSectionAPIEntity], menuId: Int) = {
      val menu = Menus.filter(_.id === menuId)
      for {
      // then! insert everything from scratch
        sectionIds <- (
          MenuSections returning MenuSections.map(s => (s.displayOrder -> s.id))
        ) ++= sections.map(_.main.copy(menu=menuId))
        sectionIdsMap = Map(sectionIds: _*) // map from order to ID
        itemsWithSectionAndMenuIds = sections.flatMap { case MenuSectionAPIEntity(s, items) =>
          items.map(_.copy(section=sectionIdsMap(s.displayOrder), menu=menuId))
        }
        _ <- MenuItems ++= itemsWithSectionAndMenuIds
      } yield Unit
    }

    /**
      *
      * @param apiEntity
      * @return The ID of the newly-created menu
      */
    def createMenuFromAPIEntity(
        apiEntity: MenuAPIEntity
    ): DBIO[Int] = {
      val MenuAPIEntity(mainInfo, sections) = apiEntity
      for {
        menuId <- (Menus returning Menus.map(_.id)) += mainInfo
        _ <- setupMenuSections(sections, menuId)
      } yield menuId
    }

    /**
      * @return true if a menu exists under that ID and user, otherwise false. iff DBIO is successful
      *         and return value is true, then the menu was modified
      */
    def modifyMenuFromAPIEntity(
        apiEntity: MenuAPIEntity,
        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
        menuId: Int
    ): DBIO[Boolean] = {
      val MenuAPIEntity(mainInfo, sections) = apiEntity
      val menu = account.menus.filter(_.id === menuId)
      for {
        numModified <- menu.update(mainInfo.copy(id=menuId)) if numModified == 1

        // first! blow everything away. menu itmes should only be referenced using DEFERRED foreign key
        // constraints relating to product ID and menu ID
        _ <- menu.sections.items.delete
        _ <- menu.sections.delete
        _ <- setupMenuSections(sections, menuId)
      } yield true
    }.recover { case e: NoSuchElementException => false }

    /**
      * @return true if menu found and deleted, false if not found, failed DBIO if other error
      */
    def deleteMenu(
        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
        menuId: Int
    ): DBIO[Boolean] = {
      val menu = account.menus.filter(_.id === menuId)

      val orderUsagesQuery = account.orders.filter(_.menu === menuId)

      for {
        orderUsagesQueryResult <- orderUsagesQuery.result

        _ <- failIf(
          orderUsagesQueryResult.nonEmpty,
          new HttpCodeException(BadRequest, ReadableError(
            "_error" -> Messages("api.deletemenu.used")
          ).toJson)
        ).toDBIO

        _ <- menu.sections.items.delete
        _ <- menu.sections.delete
        numDeleted <- menu.delete
        _ <- failIf(
          numDeleted > 1,
          new RuntimeException("more than one menu deleted in DBActions.deleteMenu")
        ).toDBIO
      } yield numDeleted == 1
    }
  }

  def getMenuDetails(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    import menusAPIFormats.menuAPIFormat
    db.runInTransaction {
      for {
        maybeEntity <- DBActions.fetchMenuAPIEntity(id, r.accountQuery)
        apiEntity <- maybeEntity match {
          case Some(entity) => DBIO.successful(entity)
          case None => DBIO.failed(HttpCodeException(NotFound, "no such menu"))
        }
      } yield Ok(Json.toJsObject(apiEntity))
    }
  }

  def menuFromJson(
      json: JsValue, ownerId: Int, maybeId: Option[Int] = None
  ): Try[MenuAPIEntity] = {
    import menusAPIFormats.menuAPIFormat
    for {
      rawEntity <- Try(json.as[MenuAPIEntity])
    } yield DBActions.cleanupForDBInsertion(rawEntity, ownerId, maybeId)
  }

  def createMenu = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        apiEntity <- menuFromJson(r.body, r.accountId).toDBIO
        menuExistsWithName <- r.accountQuery.menus
          .filter(_.name.toLowerCase === apiEntity.main.name.trim().toLowerCase).result.headOption
        _ <- failIf(
          menuExistsWithName.nonEmpty,
          new HttpCodeException(BadRequest, ReadableError(
            "_error" -> "Menu already exists with this name"
          ).toJson)
        ).toDBIO
        menuId <- DBActions.createMenuFromAPIEntity(apiEntity)
      } yield Created(Json.obj("id" -> menuId))
    }
  }

  def lookupName = SecuredApiAction.async(parse.json) { r =>
    val name = (r.body \ "name").as[String]

    db.run {
      for {
        menuId <- r.accountQuery.menus
          .filter(_.name.toLowerCase === name.trim().toLowerCase())
          .map(_.id).result.headOption
      } yield {
        val jsonId = menuId.map(JsNumber(_)).getOrElse(JsNull)
        Ok(Json.obj("id" -> jsonId))
      }
    }
  }

  def modifyMenu(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        apiEntity <- menuFromJson(r.body, r.accountId, Some(id)).toDBIO
        menuFound <- DBActions.modifyMenuFromAPIEntity(apiEntity, r.accountQuery, id)
      } yield {
        if (menuFound) Ok(JsString("success"))
        else NotFound.parsleyFormattedError("no such menu")
      }
    }
  }

  def deleteMenu(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val menu = r.accountQuery.menus.filter(_.id === id)
      for {
        found <- DBActions.deleteMenu(r.accountQuery, id)
      } yield {
        if (found) Ok(JsString("success"))
        else NotFound.parsleyFormattedError("no such menu")
      }
    }
  }
}
