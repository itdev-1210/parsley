package controllers.api

import controllers.Env
import controllers.api.RecipeUtils._
import models._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import util.AsyncUtils._

import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.Json.toJsObject
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette
import _root_.util.Exceptions.HttpCodeException

import javax.inject.Inject
import java.time.{Instant, LocalDate}
import java.time.format.DateTimeFormatter

import scala.util.Try

class Inventories @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val productsController: controllers.api.Products,
  val transactionsController: Transactions,
) extends ApiController {

  import dbConfig.profile.api._
  import inventoryAPIFormats._
  import productsController.DBActions.getProductsFullName
  import transactionsController.{ DBActions => TransactionDBActions }

  object DBActions {
    def allowedOnInventory(p: Tables.Products) = p.lastRemovedFromInventory.isEmpty ||
        p.lastAddedToRecipe.exists(p.lastRemovedFromInventory <= _)

    def getAdditionalItemsQueries(
      userProducts: AllColumnsQuery[models.Tables.Products, ProductsRow],
      filterFunc: Tables.Products => Rep[Option[Boolean]] = allowedOnInventory
    ) = {
      val fromSuppliers = (userProducts join ProductSources on (_.id === _.product))
        .map { case (ingredients, _) => ingredients }

      val userRecipes = userProducts.filter(!_.tombstone).recipe
      val fromRecipes = (userProducts join userRecipes.steps.ingredientUsage on (_.id === _.ingredient))
        .map { case (ingredients, _) => ingredients }

      val ingredientsQuery = (fromSuppliers ++ fromRecipes).distinct.filter(filterFunc)

      for {
        // Filter out non-advance-prep sub-recipes & simple preps
        // Advance-prep sub-recipes are included since they also use `ingredientToNewInventoryItem`
        // Advance simple preps are filtered out as they use `advancedPrepToNewInventoryItem`
        subRecipeIds <- ingredientsQuery.recipe.filter(!_.advancePrep).map(_.product).result
        allPrepIds <- userProducts.simplePreps.map(_.outputProduct).result
        filterOutIds = (subRecipeIds ++ allPrepIds).distinct
        filteredIngredients = ingredientsQuery.filter(!_.id.inSet(filterOutIds))

        rawIngredientsFromPreps = ingredientsQuery.preppedBy.inputProducts.filter(filterFunc)

        // Advance preps in recipes
        advancePrepsQuery = (
          userProducts.simplePreps.filter(_.advancePrep)
              join userProducts.recipe.steps.ingredientUsage
              on (_.outputProduct === _.ingredient)
              join userProducts
              on (_._1.outputProduct === _.id)
        ).filter {
          case (_, p) => filterFunc(p)
        }.map(_._1._1).distinct
      } yield (
        (filteredIngredients ++ rawIngredientsFromPreps).distinct,
        advancePrepsQuery,
        filterOutIds
      )
    }

    def getIngredientsNameTagMap(
      userProducts: AllColumnsQuery[models.Tables.Products, ProductsRow],
      productIds: Seq[Int]
    ) = for {
      result <- DBIO.sequence(productIds.map { id =>
        val product = userProducts.filter(_.id === id)
        for {
          name <- getProductsFullName(id, userProducts)
          tags <- product.categories.map(_.category).result
          originalIngredientsTags <- product.preppedBy.inputProducts.categories.map(_.category).result
        } yield id -> (name, (tags ++ originalIngredientsTags).distinct)
      })
    } yield result.toMap

    def fetchInventoryDetails(
      account: AllColumnsQuery[Tables.Accounts, AccountsRow],
      id: Int
    ) : DBIO[InventoryAPIEntity] = for {
      inventory <- account.inventories.filter(_.id === id).result.headOption

      result <- inventory match {
        case Some(i) => for {
          items <- account.inventoryItems.filter(_.inventory === i.id).result
        } yield InventoryAPIEntity(
          i.id,
          i.date,
          i.name,
          i.time,
          items
        )
        case None => DBIO.failed(HttpCodeException(NotFound, "no such inventory"))
      }
    } yield result

    def getCurrentInventory(
      account: AllColumnsQuery[Tables.Accounts, AccountsRow],
      todayLocalTime: Option[String] = None,
      allowedProductIds: Option[Iterable[ProductId]] = None
    ) : DBIO[Seq[CurrentInventoryItem]] = {
      // There are cases where we want to ignore today's inventory,
      // like when calculating shrinkage, we use local values instead of the pending ones in DB
      val inventoriesQuery = todayLocalTime match {
        case Some(localTime) => {
          val todayDate = LocalDate.parse(localTime, DateTimeFormatter.BASIC_ISO_DATE)
          account.inventories.filter(_.date < todayDate)
        }
        case _ => account.inventories
      }

      // All ingredients that have been inventoried with a positive amount at least once and not removed
      var allInventoriedIngredientsQuery = (
        account.inventories.items
        join account.products
        on (_.product === _.id)
      )
      allowedProductIds.foreach { ids =>
        allInventoriedIngredientsQuery = allInventoriedIngredientsQuery.filter { case (i, _) => i.product.inSet(ids) }
      }

      for {
        allInventoriedIngredients <- allInventoriedIngredientsQuery
          .filter { case (i, p) => i.amount.getOrElse(0.0) > 0.0 && DBActions.allowedOnInventory(p) }
          .map(_._1.product).distinct.result

        // Commented code below are the complex logic we choose not to support (yet)
        // See PARS-2233/2276 for more information

        // All ingredients that have been ordered at least once
        // allOrderedIngredients <- account.transactions.filter(t =>
        //   t.xactType === TransactionType.CustomerOrder && t.isFinalized
        // ).deltas.filter(d => d.causedBy.isDefined && d.product.isDefined).map(_.product).distinct.result

        // All ingredients that have been received at least once
        // allReceivedIngredients <- account.transactions.filter(t =>
        //   t.xactType === TransactionType.PurchaseOrder && t.isFinalized
        // ).deltas.filter(_.product.isDefined).map(_.product).distinct.result

        // Ingredients that have been inventoried, ordered or received at least once
        allProductIds = (
          allInventoriedIngredients //++ allOrderedIngredients.flatten ++ allReceivedIngredients.flatten
        )//.distinct

        // Ingredients that have never been inventoried, ordered or received
        // (additionalFromSuppliersAndRecipesQuery, advancePrepsQuery, _) <- DBActions.getAdditionalItemsQueries(account.products)
        // additionalFromSuppliersAndRecipesIds <- additionalFromSuppliersAndRecipesQuery.filter(!_.id.inSet(allProductIds)).map(_.id).result
        // advancePrepsIds <- advancePrepsQuery.filter(!_.inputProduct.inSet(allProductIds)).map(_.inputProduct).result
        // allEmptyProductIds = (additionalFromSuppliersAndRecipesIds ++ advancePrepsIds).distinct

        // Util Maps
        toFetchProductIds = allProductIds// ++ allEmptyProductIds
        productsQuery = account.products.filter(_.id.inSet(toFetchProductIds))
        productsMeasures <- productsQuery.measures.result
        productsMeasuresMap = productsMeasures.groupBy(_.product)
        allUnits <- Units.map(u => (u.id, u)).result
        allUnitsMap = allUnits.toMap

        // sourcing info (for packaging)
        // emptyProductsQuery = account.products.filter(_.id.inSet(allEmptyProductIds))
        // emptyProductsLowestPricePreferredCandidates <- (emptyProductsQuery join ProductInfos on (_.id === _.product))
        //       .filter { case (product, info) => info.lowestPricePreferred }
        //       .map { case (product, info) => product }
        //       .sources.result
        // emptyProductsExplicitlyPreferredSources <- (emptyProductsQuery joinLeft ProductInfos on (_.id === _.product))
        //       .filter { case (product, maybeInfo) => !maybeInfo.isDefined || !maybeInfo.map(_.lowestPricePreferred) }
        //       .map { case (product, maybeInfo) => product }
        //       .explicitlyPreferredSource.map(s => (s.product, s)).result

        // emptyProductsExplicitlyPreferredSourceMap = emptyProductsExplicitlyPreferredSources.toMap
        // emptyProductsLowestPricePreferredSourceMap = emptyProductsLowestPricePreferredCandidates
        //     .groupBy(_.product)
        //     .map { case (productId, sources) =>
        //       implicit val conversions = new Conversions(
        //         productsMeasuresMap(productId), allUnitsMap, productId
        //       )
        //       productId -> getLowestCostSource(sources)
        //     }.collect { case (productId, Some(preferredSource)) =>
        //       (productId -> preferredSource)
        //     }.toMap
        // emptyProductsPreferredSourceMap = emptyProductsExplicitlyPreferredSourceMap ++ emptyProductsLowestPricePreferredSourceMap

        simplePreps <- account.products.preppedBy.map(p => (p.outputProduct, p)).result
        prepIdToPrepMap = simplePreps.toMap

        productsNames <- productsQuery.filter(_.id.inSet(toFetchProductIds)).map(p => (p.id, p.name)).result
        productsNamesMap = productsNames.toMap

        now = Instant.now

        items <- DBIO.sequence(allProductIds.map(productId =>
          for {
            // The last inventory item of this ingredient
            inInventory <- inventoriesQuery.items.filter(_.product === productId).sortBy(_.id.desc).result.headOption
            lastInventoryTime <- inInventory match {
              case None => DBIO.successful(None)
              case Some(item) => account.inventories.filter(_.id === item.inventory).map(_.time).result.headOption
            }

            // transactions since the product was last inventoried. assuming the product is not a final
            // salable product, since our transaction deltas for customer orders would indicate large positive
            // deltas of those products
            transacted <- account.transactions
                .filter(t => t.isFinalized && t.time > lastInventoryTime && t.time < now && t.includeInCalculatedInventory)
                .deltas
                .sortBy(_.xact.desc)
                .filter(d => d.product === productId).result

          } yield {
            // Do all calculations in pure unit
            // Revert back to a package (if any) after that.
            implicit val conversions = new Conversions(
              productsMeasuresMap(productId), allUnitsMap, productId
            )

            var item = CurrentInventoryItem(
              productId,
              productsNamesMap.get(productId),
              prepIdToPrepMap.get(productId) match {
                case Some(prep) => productsNamesMap.get(prep.inputProduct)
                case _ => None
              },
              productsMeasuresMap(productId),
              Quantity(0.0, None, None),
              packaged = false,
              None,
              None,
              1.0,
              1.0
            )

            // Use the latest packaging. See PARS-2408
            transacted.foreach(ing => {
              if (ing.unit.isDefined) {
                val superPackageSize = ing.superPackageSize.getOrElse(1.0)
                val packageSize = ing.packageSize.getOrElse(1.0)
                val q = Quantity(ing.amount, ing.measure, ing.unit)
                if (item.quantity.unit.isEmpty) {
                  // transactions can have a different package unit from the quantity, but inventory
                  // items can't, so convert the unit
                  val quantityInPackageUnits = if (ing.sourceUnit.isDefined) {
                    // TODO, DANGER: Assuming the conversion from quantity to package unit is still valid
                    // should be fetching, using transaction_measures for the conversion
                    conversions.convert(ing.sourceUnit, q).get
                  } else {
                    q
                  }
                  item = item.copy(
                    quantity = quantityInPackageUnits,
                    packaged = ing.packaged.getOrElse(false),
                    packageName = ing.packageName,
                    subPackageName = ing.subPackageName,
                    packageSize = packageSize,
                    superPackageSize = superPackageSize
                  )
                } else {
                  item = item.copy(quantity = item.quantity + q)
                }
              }
            })

            inInventory.foreach { inventoryItem =>
              if (inventoryItem.unit.isDefined) {
                val q = Quantity(
                  inventoryItem.amount.getOrElse(0.0),
                  inventoryItem.measure,
                  inventoryItem.unit
                )

                if (item.quantity.unit.isEmpty) {
                  item = item.copy(
                    quantity = q,
                    packaged = inventoryItem.packaged,
                    packageName = inventoryItem.packageName,
                    subPackageName = inventoryItem.subPackageName,
                    packageSize = inventoryItem.packageSize,
                    superPackageSize = inventoryItem.superPackageSize
                  )
                } else {
                  item = item.copy(quantity = item.quantity + q)
                }
              }
            }

            // Replace advance preps with their original ingredient if the quantity is negative
            if (item.quantity.amount < 0.0 && prepIdToPrepMap.get(item.product).isDefined) {
              val prep = prepIdToPrepMap(item.product)
              item = item.copy(
                product = prep.inputProduct,
                name = productsNamesMap.get(prep.inputProduct),
                originalIngredientName = None,
                quantity = item.quantity.copy(
                  amount = item.quantity.amount * (1.0 / prep.yieldFraction)
                )
              )
            }

            item
          }
        ))
      } yield {
        // val emptyItems = allEmptyProductIds.map { productId =>

        //   implicit val conversions = new Conversions(
        //     productsMeasuresMap(productId), allUnitsMap, productId
        //   )

        //   val preferredSource = emptyProductsPreferredSourceMap.get(productId)
        //   CurrentInventoryItem(
        //     productId,
        //     productsNamesMap.get(productId),
        //     prepIdToPrepMap.get(productId) match {
        //       case Some(prep) => productsNamesMap.get(prep.inputProduct)
        //       case _ => None
        //     },
        //     productsMeasuresMap(productId),
        //     Quantity(0.0, preferredSource.map(_.measure), preferredSource.map(_.unit)),
        //     packaged = preferredSource.exists(_.packaged),
        //     packageName = preferredSource.flatMap(_.packageName),
        //     subPackageName = preferredSource.flatMap(_.subPackageName),
        //     packageSize = preferredSource.map(_.packageSize).getOrElse(1.0),
        //     superPackageSize = preferredSource.map(_.superPackageSize).getOrElse(1).toDouble,
        //   )
        // }
        items// ++ emptyItems
      }
    }
  }

  def listInventories = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      for {
        inventories <- r.accountQuery.inventories.sortBy(_.id.desc).result
      } yield Ok(JsArray(inventories.map(i =>
        Json.toJsObject(InventoryAPIEntity(i.id, i.date, i.name, i.time, Seq()))
      )))
    }
  }

  def getLastTakenTime = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.run {
      for {
        lastTakenTime <- r.accountQuery.inventories.sortBy(_.time.desc).map(_.time).result.headOption
      } yield Ok(Json.toJson(lastTakenTime))
    }
  }

  def getInventoryDetails(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      for {
        result <- DBActions.fetchInventoryDetails(r.accountQuery, id)
      } yield Ok(toJsObject(result))
    }
  }

  def getPreviousInventoryDetails(today: String) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val todayLocalDate = LocalDate.parse(today, DateTimeFormatter.BASIC_ISO_DATE)

      for {
        latestId <- r.accountQuery.inventories
                      .filter(_.date < todayLocalDate)
                      .sortBy(_.id.desc)
                      .take(1)
                      .map(_.id)
                      .result.headOption
        result <- latestId match {
          case Some(id) => DBActions.fetchInventoryDetails(r.accountQuery, id)
          case None => DBIO.failed(HttpCodeException(NotFound, "no such inventory"))
        }
      } yield Ok(toJsObject(result))
    }
  }

  case class TodayInventoryItems(
    items: Seq[NewInventoryItemAPIEntity],
    mergedPreviousInventory: Boolean
  )
  implicit val todayInventoryItemsFormat = Json.writes[TodayInventoryItems]

  def getTodayInventoryItems(today: String) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    val todayLocalDate = LocalDate.parse(today, DateTimeFormatter.BASIC_ISO_DATE)
    val latestInventoryQuery = r.accountQuery.inventories.sortBy(_.id.desc).take(1)

    val products = r.accountQuery.products.filter(!_.tombstone)

    db.runInTransaction {
      for {
        // These latest inventory fields could be null
        latestInventoryId <- latestInventoryQuery.map(_.id).result.headOption
        latestInventoryDate <- latestInventoryQuery.map(_.date).result.headOption

        todayMergedPreviousInventory <- r.accountQuery.inventories.filter(_.date === todayLocalDate).map(_.mergedPreviousInventory).result.headOption

        // Items from the latest inventory
        itemsFromLatestInventory <- (
          latestInventoryQuery.items.filter(_.inventory === latestInventoryId)
              join products
              on (_.product === _.id)
        ).filter {
          case (_, product) => !product.tombstone
        }.map(_._1).result

        productIdsFromLatestInventory = itemsFromLatestInventory.map(_.product)

        // Additional ingredients from suppliers & recipes
        (additionalFromSuppliersAndRecipesQuery, advancePrepsQuery, _) <- DBActions.getAdditionalItemsQueries(products)
        additionalFromSuppliersAndRecipes <- additionalFromSuppliersAndRecipesQuery.filter(
          !_.id.inSet(productIdsFromLatestInventory)
        ).result
        additionalFromSuppliersAndRecipesTags <- DBActions.getIngredientsNameTagMap(products, additionalFromSuppliersAndRecipes.map(_.id))

        // Advanced preps in recipes
        advancedPreps <- advancePrepsQuery.result
        inputProductIdsFromAdvancedPreps = advancedPreps.map(_.inputProduct)    // Advanced preps use their original ingredient's tags
        outputProductIdsFromAdvancedPreps = advancedPreps.map(_.outputProduct)  // And use their own name

        // Get products' name and tags
        productNameTagMap <- DBActions.getIngredientsNameTagMap(
          products,
          (productIdsFromLatestInventory ++ inputProductIdsFromAdvancedPreps ++ outputProductIdsFromAdvancedPreps).distinct
        )
      } yield {
        val fromLatestInventory = itemsFromLatestInventory.map(i => inventoryItemToNewInventoryItem(
          latestInventoryDate == Some(todayLocalDate),
          i,
          productNameTagMap(i.product)._1,
          productNameTagMap(i.product)._2,
        ))
        val fromSuppliersAndRecipes = additionalFromSuppliersAndRecipes.map(
          i => ingredientToNewInventoryItem(i, additionalFromSuppliersAndRecipesTags(i.id)._2)
        )
        val fromAdvancedPreps = advancedPreps.filter(p => !productIdsFromLatestInventory.contains(p.outputProduct)).map(p =>
          advancedPrepToNewInventoryItem(p, productNameTagMap(p.outputProduct)._1, productNameTagMap(p.inputProduct)._2)
        )
        Ok(toJsObject(TodayInventoryItems(
          fromLatestInventory ++ fromSuppliersAndRecipes ++ fromAdvancedPreps,
          todayMergedPreviousInventory.getOrElse(false)
        )))
      }
    }
  }

  def getCurrentInventory(
    todayLocalTime: Option[String]
  ) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      for {
        currentInventory <- DBActions.getCurrentInventory(r.accountQuery, todayLocalTime)
      } yield Ok(Json.toJsObject(CurrentInventory(currentInventory)))
    }
  }

  def getTodayInventoryId(
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    userId: Int,
    todayLocalTime: String,
    mergedPreviousInventory: Boolean,
  ): DBIO[Int] = {
    val todayDate = LocalDate.parse(todayLocalTime, DateTimeFormatter.BASIC_ISO_DATE)
    val todayInventoryQuery = account.inventories.filter(_.date === todayDate)
    for {
      _ <- todayInventoryQuery.map(_.mergedPreviousInventory).update(mergedPreviousInventory)
      todayInventory <- todayInventoryQuery.result.headOption
      inventoryId <- todayInventory match {
        case Some(inventory) => DBIO.successful(inventory.id)
        case None => {
          Inventories returning Inventories.map(_.id)
        } += InventoriesRow(-1, userId, Instant.now(), None, todayDate, mergedPreviousInventory)
      }
    } yield inventoryId
  }

  case class SaveInventoryRequest(
      items: Seq[InventoryItemsRow],
      removedProductIds: Seq[Int],
      today: String,
      mergedPreviousInventory: Boolean
  )
  implicit val saveInventoryReqFormat = Json.format[SaveInventoryRequest]
  def saveInventory = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    val userId = r.accountId

    db.runInTransaction {
      for {
        request <- Try(r.body.as[SaveInventoryRequest]).toDBIO
        todayInventoryId <- getTodayInventoryId(r.accountQuery, userId, request.today, request.mergedPreviousInventory)

        // Delete all inventory_items
        changedProductIds = request.items.map(i => i.product)
        _ <- r.accountQuery.inventoryItems.filter(i =>
          i.inventory === todayInventoryId && i.product.inSet(changedProductIds ++ request.removedProductIds)
        ).delete

        // Update removed ingredients' last_removed_from_inventory timestamp
        _ <- r.accountQuery.products.filter(
          _.id.inSet(request.removedProductIds)
        )
        .map(_.lastRemovedFromInventory)
        .update(Some(Instant.now()))

        // Re-insert the changed inventory items
        _ <- InventoryItems ++= request.items.map(i => i.copy(
          inventory=todayInventoryId,
          owner=userId
        ))
      } yield Ok(JsString("success"))
    }
  }

  def deleteInventory(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val inventory = r.accountQuery.inventories.filter(_.id === id)
      for {
        _ <- inventory.items.delete
        numDeleted <- inventory.delete
      } yield {
        if (numDeleted == 1) Ok(JsString("success"))
        else NotFound.parsleyFormattedError("no such inventory")
      }
    }
  }
}
