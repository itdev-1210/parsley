package controllers.api

import controllers._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

import scala.util.Try

class Nutrients @Inject()(
    override val controllerComponents: ControllerComponents,
    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider
) extends ApiController {

  import models.JsonConversions.nutrientsAPIFormats.nutrientAPIFormat
  import dbConfig.profile.api._

  def getNutrients() = SecuredApiAction.async { r =>
    val nutrientsQuery = if (r.account.isAdmin) {
      // inactive are only visible to admin, mostly for performance (USDA gives
      // us A LOT of nutrients)
      //
      // Has the handy side effect of not requiring us to do this filtering
      // client-side
      Nutrients
    } else {
      Nutrients.filter(_.active)
    }
    db.runInTransaction {
      nutrientsQuery.result.map { nutrients =>
        Ok(Json.toJson(nutrients))
      }
    }
  }

  def saveNutrients() = AdminOnlyAction.async(parse.json) { r =>

    db.runInTransaction {
      for {
        newNutrients <- Try(r.body.as[Seq[NutrientsRow]]).toDBIO
        nutrientIds = newNutrients.map(_.id).filter(_ >= 0)
        // nutrients to delete are those that exist, but aren't in the put data
        _ <- Nutrients.filter(!_.id.inSet(nutrientIds)).delete
        // update the ones that do appear - using upsert because its API is neater
        _ <- DBIO.seq(newNutrients.filter(_.id >= 0).map(Nutrients.insertOrUpdate): _*)
        // finally, insert the new ones
        _ <- Nutrients ++= newNutrients.filter(_.id < 0)
      } yield Ok(JsString("updated"))
    }
  }
}