package controllers.api

import java.time.Instant

import controllers._
import models.JsonConversions._
import models.Tables._
import models._
import play.api.db.slick.DatabaseConfigProvider
import models.QueryExtensions._
import play.api.mvc.ControllerComponents
import com.mohiva.play.silhouette.api.Silhouette
import controllers.api.RecipeUtils.{Conversions, convertPerQuantityValue, getCostingSize}
import javax.inject.Inject
import models.JsonConversions.suppliersAPIFormats.SupplierImportedSource

/**
  * Not really a controller, in that it doesn't actually handle any HTTP reqs
  * directly. Just container for DBActions object, to match structure of other
  * controllers that export DBActions for external use
  */
class IngredientPriceHistory @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider
) extends ApiController {

  import dbConfig.profile.api._

  object DBActions {

    def addPriceHistoryFromProductSource(source: ProductSourcesRow, date: Option[Instant] = None): DBIO[Unit] = {
      val productId = source.product
      for {
        lowestPricePreferred <- ProductInfos
            .filter(_.product === productId).map(_.lowestPricePreferred).result.head
        _ <- if (lowestPricePreferred) {
          addPriceHistoryWithLowestPricePreferred(productId, date)
        } else {
          addPriceHistoryWithPreferredSource(source.id, productId, date)
        }
      } yield Unit
    }


    def addPriceHistoryFromImportReceivableSupplier(supplierId: Int, productId: Int, importedSources: Seq[SupplierImportedSource]): DBIO[Unit] = {
      val lastPriceByDate = importedSources.head
      val oldPricesByDate = importedSources.tail
      for {
        _ <- addPriceHistoryFromProductSource(lastPriceByDate.source, lastPriceByDate.date)
        _ <- DBIO.sequence(oldPricesByDate.map(is => addNewPriceHistory(ingredientPriceHistoryRowFromProductSourcesRow(is.source, is.date))))
      } yield Unit
    }


    def addPriceHistoryFromAPIEntity(purchaseInfo: PurchaseInfoAPIEntity, productId: Int): DBIO[Unit] = {
      val mPreferredSource = purchaseInfo.preferredSource
      for {
        lowestPricePreferred <- ProductInfos
            .filter(_.product === productId).map(_.lowestPricePreferred).result.head
        _ <- (lowestPricePreferred, mPreferredSource) match {
          case (true, _) =>
            addPriceHistoryWithLowestPricePreferred(productId)
          case (false, Some(sp)) =>
            addPriceHistoryWithPreferredSource(sp.id, productId)
          case _ => DBIO.successful(Unit)
        }
      } yield Unit
    }

    def addBasePrice(productId: Int) : DBIO[Unit] = {
      def addCurrent(id: Int): DBIO[Unit] = for {
        maybeCurrentSource <- getCurrentProductSource(id)
        lastModified <- Products.filter(_.id === productId).map(_.lastModified).result.headOption
        measures <- measuresQuery(id).result
        _ <- maybeCurrentSource match {
          case Some(current) => addNewPriceHistory(ingredientPriceHistoryRowFromProductSourceRow(current, lastModified),
            Some(measuresToPriceHistoryMeasures(measures)))
          case None => DBIO.successful(Unit)
        }
      } yield Unit

      for {
        priced <- IngredientPriceHistory.filter(_.product === productId).exists.result
        _ <- if (priced) DBIO.successful(Unit)
        else addCurrent(productId)
      } yield Unit
    }


    private def addNewPriceHistory(
      price: IngredientPriceHistoryRow,
      measures: Option[Seq[IngredientPriceHistoryMeasuresRow]] = None
    ): DBIO[Unit] = {

      def addPriceHistory(price: IngredientPriceHistoryRow, measures: scala.Seq[IngredientPriceHistoryMeasuresRow]) = for {
        priceId <- (IngredientPriceHistory returning IngredientPriceHistory.map(_.id)) += price
        _ <- IngredientPriceHistoryMeasures ++= measures.map(_.copy(id = 0, ingredientPrice = priceId))
      } yield Unit

      val lastPriceHistorySourceQuery = IngredientPriceHistory
          .filter(ph => ph.product === price.product)
          .sortBy(_.createAt desc)

      for {
        lastPriceHistory <- lastPriceHistorySourceQuery.result.headOption
        priceHistoryMeasures <-
            if (measures.isDefined)
              DBIO.successful(measures.get)
            else
              lastPriceHistorySourceQuery.filter(_.id === lastPriceHistory.map(_.id)).measures.result
        _ <- if (lastPriceHistory.isEmpty || lastPriceHistory.exists(!equalsPricesHistory(_, price)))
          addPriceHistory(price, priceHistoryMeasures)
        else DBIO.successful(Unit)
      } yield Unit
    }


    private def ingredientPriceHistoryRowFromProductSourceRow(
      ps: ProductSourcesRow, createdAt: Option[Instant] = None
    ): IngredientPriceHistoryRow = {
      IngredientPriceHistoryRow(
        -1,
        ps.product,
        ps.measure,
        ps.unit,
        ps.cost,
        ps.packaged,
        ps.packageSize.toFloat,
        ps.superPackageSize.toFloat,
        ps.pricePer,
        ps.pricingMeasure,
        ps.pricingUnit,
        ChangeType.PriceChange,
        createdAt.getOrElse(Instant.now)
      )
    }


    private def ingredientPriceHistoryRowFromTransactionDeltasRow(
      td: TransactionDeltasRow, createdAt: Option[Instant] = None
    ): IngredientPriceHistoryRow = {
      IngredientPriceHistoryRow(
        -1,
        td.product.get,
        td.measure.get,
        td.unit.get,
        td.cashFlow.map(_.abs),
        td.packaged.get,
        td.packageSize.get.toFloat,
        td.superPackageSize.get.toFloat,
        td.pricePer.get,
        td.pricingMeasure,
        td.pricingUnit,
        ChangeType.Receiving,
        createdAt.getOrElse(Instant.now)
      )
    }


    private def equalsPricesHistory(
      current: IngredientPriceHistoryRow, newPrice: IngredientPriceHistoryRow
    ): Boolean = {
      current.measure == newPrice.measure &&
      current.unit == newPrice.unit &&
      current.cost == newPrice.cost &&
      current.packaged == newPrice.packaged &&
      current.packageSize == newPrice.packageSize &&
      current.superPackageSize == newPrice.superPackageSize &&
      current.pricePer == newPrice.pricePer &&
      current.pricingMeasure == newPrice.pricingMeasure &&
      current.pricingUnit == newPrice.pricingUnit
    }


    def ingredientPriceHistoryRowFromProductSourcesRow(
     ps: ProductSourcesRow, createdAt: Option[Instant] = None
    ): IngredientPriceHistoryRow = {
      IngredientPriceHistoryRow(
        -1,
        ps.product,
        ps.measure,
        ps.unit,
        ps.cost,
        ps.packaged,
        ps.packageSize.toFloat,
        ps.superPackageSize.toFloat,
        ps.pricePer,
        ps.pricingMeasure,
        ps.pricingUnit,
        ChangeType.PriceChange,
        createdAt.getOrElse(Instant.now)
      )
    }


    def measuresToPriceHistoryMeasures(
      measures: Seq[ProductMeasuresRow]
    ): Seq[IngredientPriceHistoryMeasuresRow] = {
      measures.map(m =>
        IngredientPriceHistoryMeasuresRow(
          -1,
          -1,
          m.measure,
          m.preferredUnit,
          m.conversionMeasure,
          m.conversionUnit,
          m.conversion,
          m.amount
        )
      )
    }


    private def measuresQuery(productId: Int) = {
      ProductMeasures.filter(_.product === productId)
    }

    private def addPriceHistoryWithLowestPricePreferred(productId: Int, date: Option[Instant] = None): DBIO[Unit] = for {
      maybeLowestSource <- getLowestSource(productId)
      measures <- measuresQuery(productId).result
      phMeasures = measuresToPriceHistoryMeasures(measures)
      _ <- maybeLowestSource match {
        case Some(lowestSource) =>
          addNewPriceHistory(ingredientPriceHistoryRowFromProductSourcesRow(lowestSource, date), Some(phMeasures))
        case None =>
          DBIO.successful(Unit)
      }
    } yield Unit


    private def addPriceHistoryWithPreferredSource(sourcesId: Int, productId: Int, date: Option[Instant] = None): DBIO[Unit] = {
      val preferredSourceQuery = Products.filter(_.id === productId).map(_.preferredSource)

      for {
        preferredSourceId <- preferredSourceQuery.result.head
        preferredSource <- ProductSources.filter(_.id === preferredSourceId).result.head
        measures <- measuresQuery(productId).result
        priceHistoryMeasures = measuresToPriceHistoryMeasures(measures)
        _ <- if (!preferredSourceId.contains(sourcesId)) DBIO.successful(Unit)
        else addNewPriceHistory(ingredientPriceHistoryRowFromProductSourceRow(preferredSource, date), Some(priceHistoryMeasures))
      } yield Unit
    }


    private def getLowestSource(productId: Int): DBIO[Option[ProductSourcesRow]] = {
      val productQuery = Products.filter(_.id === productId)
      for {
        measures <- productQuery.measures.result
        allUnits <- Units.map(u => (u.id, u)).result
        allUnitsMap = allUnits.toMap
        sources <- productQuery.sources.result
      } yield {
        if (sources.isEmpty) None
        else {
          val conversions = new Conversions(
            measures, allUnitsMap, productId
          )
          val sourceCostList = sources.map(s => (s, getCostingSize(s)))
          val headSource = sourceCostList.head
          val minCostSource = sourceCostList.map {
            case (source, quantity) =>
              val cost = convertPerQuantityValue(
                source.cost.get, quantity.amount, quantity.unit.get,
                headSource._2.amount, headSource._2.unit.get, conversions
              )
              (source, cost)
          }.minBy(_._2)
          Some(minCostSource._1)
        }
      }
    }


    def getCurrentProductSource(productId: Int): DBIO[Option[ProductSourcesRow]] = {
      def getCurrent(
        preferredId: Option[Int], sources: Seq[ProductSourcesRow], lowestPricePreferred: Boolean
      ):DBIO[Option[ProductSourcesRow]] = {
        (preferredId, sources, lowestPricePreferred) match {
          case (_, Nil, _) => DBIO.successful(None)
          case (_, _, true) => getLowestSource(productId)
          case (Some(pId), seq, _) => DBIO.successful(seq.find(ps => ps.id == pId))
          case _ => DBIO.successful(None)
        }
      }

      val productQuery = Products.filter(_.id === productId)
      for {
        product <- productQuery.result.headOption
        sources <- productQuery.sources.result
        lowestPricePreferred <- ProductInfos
            .filter(_.product === productId).map(_.lowestPricePreferred).result.head
        current <- getCurrent(product.flatMap(_.preferredSource), sources, lowestPricePreferred)
      } yield current
    }

    def isLowestSourceSupplierById(productId: Int, supplierId: Int) = for {
      maybeLowestSource <- getLowestSource(productId)
    } yield {
      maybeLowestSource.flatMap(_.supplier).map(_.equals(supplierId)).getOrElse(false)
    }
  }

}