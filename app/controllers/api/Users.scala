package controllers.api

import controllers.Env
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.{EmailTokenReference, EmailTokenVerifier, EmailUtils, FileUtils}
import util.Exceptions._

import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.Messages
import play.api.libs.json.Json.toJsObject
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasherRegistry, PasswordInfo}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.sendgrid.Email
import fly.play.s3.{PUBLIC_READ, BucketFile}
import com.sksamuel.scrimage.Image

import javax.inject.Inject
import java.nio.file.{Files, Paths}
import java.time.{LocalDate, LocalDateTime, ZoneOffset}
import java.time.format.DateTimeFormatter

import scala.concurrent.Future
import scala.util.Try

class Users @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  credentialsProvider: CredentialsProvider,
  passwordHasherRegistry: PasswordHasherRegistry,
  override val dbConfigProvider: DatabaseConfigProvider,
  val config: Configuration,
  val emailTokenVerifier: EmailTokenVerifier,
  val emailUtils: EmailUtils,
  val fileUtils: FileUtils
) extends ApiController {

  import dbConfig.profile.api._

  private def baseUserFormat = Json.format[UsersRow]
  implicit private def userFacingUserWrites = FancyFormat(
    baseUserFormat,
    mockIdFields = Seq("id"),
    omittedFields = Seq("hasher", "hashedPassword", "salt"),
  )

  private def baseAccountFormat = Json.format[AccountsRow]
  implicit private def userFacingAccountWrites = FancyFormat(baseAccountFormat,
    mockIdFields = Seq("id"), // split from omittedFields for documentation purposes
    omittedFields = Seq(
      "stripeEmail", "stripeCustomerId", // internal billing use
      "multiLocationUpstream", "upstreamRelativeName", // only give user binary isMultiLocationDownstream

    )
  )

  implicit private def userFacingUserAccountWrites = OWrites[UserAccountsRow] { userAccount =>
    Json.obj(
      "accountId" -> userAccount.owner,
      "isReadOnly" -> JsBoolean(userAccount.permissionLevel == AccountPermissionLevel.RecipeReadOnly),
      "permissionLevel" -> userAccount.permissionLevel,
    )
  }

  implicit private def userFacingLoginWrites = OWrites[UserLoginsRow] { login =>
    Json.obj(
      "providerId" -> login.providerId,
      "providerKey" -> login.providerKey,
      "email" -> login.email
    )
  }

  implicit private def measureSystemsTypeFormat: Format[MeasureSystemsType.Value] = FancyFormat.enum(MeasureSystemsType)
  implicit private def recipePrintFormatTypeFormat: Format[RecipePrintFormatType.Value] = FancyFormat.enum(RecipePrintFormatType)
  implicit private def subtractInventoryTypeFormat: Format[SubtractInventoryType.Value] = FancyFormat.enum(SubtractInventoryType)

  val assetsImageRegex = "^/assets/images/".r
  def getAccounts = StdlibAction.async { r =>

    val canStillLoginAccounts = (Accounts join UserLogins on (_.accountOwner === _.userId)).map(_._1).distinct
    val q = canStillLoginAccounts join Users on (_.accountOwner === _.id)

    db.run(q.result).map{ usersWithInfo =>
      Ok(JsArray(usersWithInfo.map{
        case (account, user) => toJsObject(user) ++ toJsObject(account) +
          ("id" -> JsNumber(account.id)) +
          ("subscriptionLevel" -> Json.toJson(account.subscriptionLevel)) +
          ("phoneNumber" -> Json.toJson(user.phoneNumber)) +
          ("company" -> Json.toJson(account.businessName))
      }))
    }
  }

  sealed private case class AdminFacingUserDesc(
      user: UsersRow,
      userAccounts: Seq[UserAccountsRow],
  )
  implicit private val adminFacingUserDescWrites = OWrites[AdminFacingUserDesc] { desc =>
    Json.toJsObject(desc.user) + (
        "userAccounts" -> Json.toJson(desc.userAccounts)
    ) + (
        "id" -> JsNumber(desc.user.id)
    )

  }
  def getUsers = AdminOnlyAction.async { r =>
    val canStillLoginUsers = (Users join UserLogins on (_.id === _.userId)).map(_._1).distinct

    db.runInTransaction {
      for {
        users <- canStillLoginUsers.result
        rawUserAccounts <- (canStillLoginUsers join UserAccounts on (_.id === _.userId)).map(_._2).result
      } yield {
        val userAccounts = rawUserAccounts.groupBy(_.userId)
        val userDescs = users.map(u => AdminFacingUserDesc(u, userAccounts.getOrElse(u.id, Seq())))

        Ok(Json.toJson(userDescs))
      }
    }
  }

  def writeAccountWithUserAccount(userAccount: UserAccountsRow, account: AccountsRow) =
    toJsObject(account) ++ toJsObject(userAccount) + (
        "isAccountOwner" -> JsBoolean(account.accountOwner.contains(userAccount.userId))
        )

  def writeAccountsList(accounts: Seq[AccountsRow], userAccounts: Seq[UserAccountsRow]) = {
    val joinedPairs = accounts.map { a =>
      (a, userAccounts.find(_.owner == a.id))
    }.collect {
      case (a, Some(ua)) => (ua, a)
    }

    JsArray(joinedPairs.map(
      (writeAccountWithUserAccount _).tupled
    ))
  }

  def checkLoggedIn = UserAwareApiAction { implicit r =>
    Ok(JsBoolean(r.identity.isDefined))
  }

  def getUserInfo = UserLoggedInApiAction.async { implicit r =>
    val user = r.identity.dbRow
    val userQuery = r.authedUserQuery
    val maybeCurrentAccount = getAccountFromSession(r.identity)
    val accountQuery = maybeCurrentAccount match {
      case Some((userAccount, account)) => Accounts.filter(_.id === account.id)
      case None => Accounts.filter(_ => LiteralColumn(false))
    }


    val s3Region = config.getOptional[String]("s3.region").get
    val bucket = config.getOptional[String]("s3.bucket").get

    db.run {
      for {
        lastSubscription <- accountQuery.lastSubscription.result.headOption
        currentSubscription <- accountQuery.currentSubscription.result.headOption

        printPreference <- userQuery.printPreferences.result.head // must exist
        logins <- userQuery.userLogins.result
      } yield Ok(toJsObject(user) ++ maybeCurrentAccount.map((writeAccountWithUserAccount _).tupled).getOrElse(JsObject.empty)
          + ("allAccounts" -> writeAccountsList(r.identity.accounts, r.identity.userAccounts))
          + ("logins" -> Json.toJson(logins))
          + ("newSubscriber" -> JsBoolean(lastSubscription.isEmpty))
          + ("logoUrl" ->  JsString(s"https://s3-$s3Region.amazonaws.com/$bucket/"))

          + ("isMultiLocationDownstream" -> JsBoolean(maybeCurrentAccount.flatMap(_._2.multiLocationUpstream).isDefined))
          + ("plan" -> Json.toJson(currentSubscription.map(_.stripePlanId)))
          + ("previousSubscriptionLevel" -> Json.toJson(lastSubscription.map(_.subscriptionLevel)))

          + ("isSharedUser" -> JsBoolean(!maybeCurrentAccount.flatMap(_._2.accountOwner).contains(r.authedUserId)))
          + ("shouldShowOnboarding" -> JsBoolean(
              config.getOptional[String]("parsley.onboardingActiveDate")
                  .map(LocalDate.parse(_, DateTimeFormatter.ISO_LOCAL_DATE))
                  .toIterable.zip(maybeCurrentAccount.toIterable).headOption
                  .exists { case (onboardingActiveDate, (userAccount, account)) =>
                    onboardingActiveDate.isBefore(
                      LocalDateTime.ofInstant(userAccount.joinedAt, ZoneOffset.UTC).toLocalDate
                    )
                  }
            ))
          + ("printPreference" -> toJsObject(printPreference))
      )
    }
  }

  private sealed case class OwnerOrUpstreamUser(id: Int, name: String, email: Option[String])
  implicit private val ownerOrUpstreamWrites = Json.writes[OwnerOrUpstreamUser]

  private def getUpstreamInfo(accountQuery: AllColumnsQuery[Tables.Accounts, AccountsRow]) = for {
    (id, maybeBusinessName) <- accountQuery.map(a => (a.id, a.businessName)).result.head
    email <- accountQuery.owner.map(_.email).result.headOption.map(_.flatten)
    name = maybeBusinessName.getOrElse(email.get)
  } yield OwnerOrUpstreamUser(id, name, email)

  def getOwnerInfo = SecuredApiAction.async { r =>
    if (!r.isAccountOwner) {
      db.runInTransaction(
        getUpstreamInfo(r.accountQuery).map(Json.toJson(_)).map(Ok(_))
      )
    } else {
      Future.successful(Ok(Json.obj()))
    }
  }

  def getMultiLocationUpstreamInfo = SecuredApiAction.async { r =>
    db.runInTransaction(for {
      hasUpstream <- r.accountQuery.map(_.multiLocationUpstream.isDefined).result.head
      info <- if (hasUpstream) {
        getUpstreamInfo(r.accountQuery.upstream).map(Some(_))
      } else {
        DBIO.successful(None)
      }
    } yield Ok(Json.toJson(info)))
  }

  def getOnboardingInfo = SecuredApiAction.async { r =>
    import onboardingApiFormats.onboardingFormat
    db.run {
      for {
        visitedBoxes <- r.authedUserQuery.visitedBoxes.result
        pages <- OnboardingPages.result
        steps <- OnboardingSteps.result
      } yield Ok(
        Json.toJson(OnboardingApiEntity(
          visitedBoxes.map(vb => vb.copy(page = vb.page.trim)),
          pages.map(p => p.copy(key = p.key.trim)),
          steps.map(s => s.copy(page = s.page.trim))
        ))
      )
    }
  }

  def resetOnboardingInfo = SecuredApiAction.async { r =>
    db.runInTransaction {
      for {
        _ <- r.authedUserQuery.visitedBoxes.delete
      } yield Ok("reset")
    }
  }

  def setStepVisited = SecuredApiAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        OnboardingVisited(pageKey, step) <- Try(r.body.as[OnboardingVisited]).toDBIO
        pages <- OnboardingPages.result
        _ <- failIfNone(
          pages.map(_.key.trim).find(_ == pageKey),
          HttpCodeException(BadRequest, ReadableError("_error" -> "Could not set as visited. Invalid page key").toJson)
        ).toDBIO

        maxStepsInPage <- OnboardingSteps.filter(_.page.trim === pageKey).map(_.index).max.result
        _ <- failIf(
          maxStepsInPage.isEmpty || (step > maxStepsInPage.get),
          HttpCodeException(BadRequest, ReadableError("_error" -> "Could not set as visited. Step exceeds number of steps in page").toJson)
        ).toDBIO

        isOnboardingOnProgress <- r.authedUserQuery.visitedBoxes.filter(_.page.trim === pageKey).exists.result
        _ <- if (isOnboardingOnProgress) {
          r.authedUserQuery.visitedBoxes.filter(_.page.trim === pageKey).map(_.nextStep).update((step + 1).toShort)
        } else {
          UserOnboardingBoxes += UserOnboardingBoxesRow(r.authedUserId, pageKey, (step + 1).toShort)
        }
      } yield Ok(JsString("success"))
    }
  }

  def updateOnboardingInfo = AdminOnlyAction.async(parse.json) { r =>
    import onboardingApiFormats.onboardingFormat
    db.runInTransaction {
      for {
        OnboardingApiEntity(_, _, steps) <- Try(r.body.as[OnboardingApiEntity]).toDBIO
        pages <- OnboardingPages.result

        // validation
        _ <- failIf(
          steps.exists(s => !pages.map(_.key.trim).contains(s.page)),
          HttpCodeException(BadRequest, ReadableError("_error" -> "Could not map step to invalid page").toJson)
        ).toDBIO
        _ <- failIf(
          steps.groupBy(s => s.page).exists {
            case (page, pageSteps) => pageSteps.map(_.index).sorted != pageSteps.indices
          },
          HttpCodeException(BadRequest, ReadableError("_error" -> "Invalid step index").toJson)
        ).toDBIO

        // update steps
        stepIds <- DBIO.sequence(steps.map { step =>
          for {
            needsUpdate <- OnboardingSteps.filter(s => s.id === step.id).exists.result
            stepId <- if (needsUpdate) {
              OnboardingSteps
                  .filter(s => s.id === step.id)
                  .update(step)
                  .andThen(DBIO.successful(step.id))
            } else {
              OnboardingSteps returning OnboardingSteps.map(_.id) +=
                  step.copy(id = -1)
            }
          }  yield stepId
        })

        // clear remaining steps
        stepsToDelete = OnboardingSteps.filterNot(s => s.id inSet stepIds)
        nestedStepImagesToDelete <- stepsToDelete.map(s => (s.leftImage, s.rightImage)).result
        stepImagesToDelete = nestedStepImagesToDelete.flatMap { case (leftImage, rightImage) =>
          leftImage.toSeq ++ rightImage.toSeq
        }
        _ <- Future.sequence(
          stepImagesToDelete
              .filter(assetsImageRegex.findFirstMatchIn(_).nonEmpty)
              .map(fileUtils.removeFile)
        ).toDBIO
        _ <- OnboardingSteps.filterNot(s => s.id inSet stepIds).delete
      } yield Ok(JsString("success"))
    }
  }

  def setOnboardingImage(id: Int, isLeft: Boolean) = AdminOnlyAction.async(parse.multipartFormData) { r =>
    val onboardingStepQuery = OnboardingSteps.filter(_.id === id)
    val imageColumnQuery = if (isLeft) { onboardingStepQuery.map(_.leftImage) } else { onboardingStepQuery.map(_.rightImage) }

    db.runInTransaction {
      for {
        oldFilename <- imageColumnQuery.result.head
        _ <- oldFilename match {
          // file loading from assets
          case Some(_oldFilename) if assetsImageRegex.findFirstMatchIn(_oldFilename).isEmpty => fileUtils.removeFile(_oldFilename).toDBIO
          case _ => DBIO.successful(Unit)
        }

        newFilename <- r.body.file("photo") match {
          case Some(photo) =>
            val uuidName = java.util.UUID.randomUUID.toString
            for {
              filename <- fileUtils.uploadFile(BucketFile(
                uuidName,
                photo.contentType.getOrElse("text/plain"),
                content = Image(Files.readAllBytes(Paths.get(photo.ref.path.toUri))).bytes,
                acl = Some(PUBLIC_READ)
              )).map(_ => uuidName).toDBIO
            } yield Some(uuidName)
          case None => DBIO.successful(None)
        }

        _ <- if (isLeft) onboardingStepQuery.map(_.leftImage).update(newFilename) else onboardingStepQuery.map(_.rightImage).update(newFilename)
      } yield Ok(JsString("updated"))
    }
  }

  def rawChangePassword(newPassword: String, user: AllColumnsQuery[models.Tables.Users, UsersRow]) = {
    val PasswordInfo(hasher, password, salt) = passwordHasherRegistry.current.hash(newPassword)
    user.map(u => (u.hasher, u.hashedPassword, u.salt))
        .update((Some(hasher), Some(password), salt))
  }

  private case class UpdateCredentialsRequest(
      currentPassword: String,
      newPassword: String
  )
  implicit private val updateCredentialsReqReads = Json.reads[UpdateCredentialsRequest]
  def updateCredentials(
      user: AllColumnsQuery[models.Tables.Users, UsersRow],
      updateReq: UpdateCredentialsRequest
  ) = for {
    maybeCurrentLogin <- user.userLogins.filter(_.providerId === credentialsProvider.id)
        .result.headOption
    currentLogin <- maybeCurrentLogin match {
      case Some(currentLogin) => DBIO.successful(currentLogin)
      case None => DBIO.failed(HttpCodeException(BadRequest,
        ReadableError(
          "_error" -> Messages("auth.updatepassword.nopasswordauth")
        ).toJson
      ))}

    _ <- credentialsProvider.authenticate(
      Credentials(currentLogin.providerKey, updateReq.currentPassword)
    ).toDBIO.recoverWith { case _ => DBIO.failed(HttpCodeException(Unauthorized,
      ReadableError(
        "currentPassword" -> Messages("auth.updatepassword.currentpassword.incorrect")
      ).toJson
    ))}

    _ <- rawChangePassword(updateReq.newPassword, user)
  } yield Unit

  private case class AddCredentialsRequest(
      password: String
  )
  implicit private val addCredentialsReqReads = Json.reads[AddCredentialsRequest]
  def addCredentials(
      userId: Int,
      user: AllColumnsQuery[models.Tables.Users, UsersRow],
      addReq: AddCredentialsRequest
  ) = for {
    maybeCurrentLogin <- user.userLogins.filter(_.providerId === credentialsProvider.id)
        .result.headOption
    _ <- failIf(maybeCurrentLogin.isDefined,
      HttpCodeException(BadRequest,
        ReadableError(
          "_error" -> Messages("auth.addpassword.userhaspasswordauth")
        ).toJson
      )).toDBIO

    maybeEmail <- user.map(_.email).result.head
    email <- failIfNone(maybeEmail,
      // unlikely case; requires a social login provider that doesn't vie us an e-mail
      HttpCodeException(BadRequest, "email/password login requires an email to be associated with the account")
    ).toDBIO

    loginInfo <- credentialsProvider.loginInfo(Credentials(email, addReq.password)).toDBIO
    maybeUsernameHolder <- UserLogins.byLoginInfo(loginInfo).result.headOption
    _ <- failIf(maybeUsernameHolder.isDefined,
      HttpCodeException(BadRequest,
        ReadableError(
          "username" -> Messages("auth.addpassword.username.exists")
        ).toJson
      )).toDBIO


    PasswordInfo(hasher, password, salt) = passwordHasherRegistry.current.hash(addReq.password)

    _ <- user.map(u => (u.hasher, u.hashedPassword, u.salt))
        .update((Some(hasher), Some(password), salt))
    _ <- UserLogins += UserLoginsRow(
      loginInfo.providerID,
      loginInfo.providerKey,
      userId
    )
  } yield Unit


  case class RecipeOptions(
      hideAllRecipes: Boolean,
      newRecipeThresholdDays: Int
  )

  implicit private val recipeOptionsReads = Json.reads[RecipeOptions]

  private case class UpdatePrintPreferenceRequest(
    orderIncludePreps: Boolean,
    orderIncludeRecipes: Boolean,
    orderIncludeSubRecipes: Boolean,
    printSubRecipePreferences: Boolean,
    breakPageOnRecipePrint: Boolean,
    recipePrintFormatPreference: RecipePrintFormatType.Value,
    includePhotoInRecipePrint: Boolean,
    includeNutritionInMultipleRecipePrint: Boolean,
    includeCostBreakdownInMultipleRecipePrint: Boolean,
  )
  implicit private val updatePrintPrefReads = Json.reads[UpdatePrintPreferenceRequest]

  // whitelist of allowed updates
  private case class UserUpdateRequest(
      // individual contact info
      firstName: Option[String],
      lastName: Option[String],
      fullName: Option[String],

      // business shipping info
      businessName: Option[String],
      ccEmails: Seq[String],
      phoneNumber: Option[String],
      shippingAddress: Option[String],

      // weight default settings
      weightSystem: Option[MeasureSystemsType.Value],
      volumeSystem: Option[MeasureSystemsType.Value],
      purchaseWeightSystem: Option[MeasureSystemsType.Value],
      purchaseVolumeSystem: Option[MeasureSystemsType.Value],

      // more print preferences
      printPreference: UpdatePrintPreferenceRequest,
      // login info
      credentialUpdates: Option[UpdateCredentialsRequest],
      newCredentials: Option[AddCredentialsRequest],

      sourcePriceChangeWarningFraction: Double,
      recipeOptions: Option[RecipeOptions]
  )
  implicit private val userUpdateReqReads = Json.reads[UserUpdateRequest]



  def updateUserInfo = SecuredApiAction.async(parse.json) { r =>
    val userRow = r.identity.dbRow
    val user = r.authedUserQuery
    val account = r.accountQuery

    db.runInTransaction {
      for {
        updateReq <- Try(r.body.as[UserUpdateRequest]).toDBIO

        _ <- user.map(u => (
            u.firstName, u.lastName, u.fullName,
            u.phoneNumber,
            u.sourcePriceChangeWarningFraction,
        )).update((
            updateReq.firstName, updateReq.lastName, updateReq.fullName,
            updateReq.phoneNumber,
            updateReq.sourcePriceChangeWarningFraction,
        ))
        _ <- account.map(a => (
            a.businessName, a.ccEmails, a.shippingAddress,
          )).update((
            updateReq.businessName, updateReq.ccEmails.toList,
            updateReq.shippingAddress,
          ))
        _ <- updateReq.recipeOptions.map { case RecipeOptions(hideAllRecipes, newRecipeThresholdDays) =>
          user.map(info => (info.hideAllRecipes, info.newRecipeThresholdDays))
              .update((hideAllRecipes, newRecipeThresholdDays))
        }.getOrElse(DBIO.successful(Unit))

        _ <- user.printPreferences.map(p => (
            p.orderIncludePreps,
            p.orderIncludeRecipes,
            p.orderIncludeSubRecipes,
            p.printSubRecipePreferences,
            p.breakPageOnRecipePrint,
            p.recipePrintFormatPreference,
            p.includePhotoInRecipePrint,
            p.includeNutritionInMultipleRecipePrint,
            p.includeCostBreakdownInMultipleRecipePrint
          )).update((
            updateReq.printPreference.orderIncludePreps,
            updateReq.printPreference.orderIncludeRecipes,
            updateReq.printPreference.orderIncludeSubRecipes,
            updateReq.printPreference.printSubRecipePreferences,
            updateReq.printPreference.breakPageOnRecipePrint,
            updateReq.printPreference.recipePrintFormatPreference,
            updateReq.printPreference.includePhotoInRecipePrint,
            updateReq.printPreference.includeNutritionInMultipleRecipePrint,
            updateReq.printPreference.includeCostBreakdownInMultipleRecipePrint
          ))

        _ <- updateReq.weightSystem match {
          case Some(weight) => user.map(info => info.weightSystem).update(weight)
          case None => DBIO.successful(Unit)
        }

        _ <- updateReq.volumeSystem match {
          case Some(volume) => user.map(info => info.volumeSystem).update(volume)
          case None => DBIO.successful(Unit)
        }

        _ <- updateReq.purchaseWeightSystem match {
          case Some(pWeight) => user.map(info => info.purchaseWeightSystem).update(pWeight)
          case None => DBIO.successful(Unit)
        }

        _ <- updateReq.purchaseVolumeSystem match {
          case Some(pVolume) => user.map(info => info.purchaseVolumeSystem).update(pVolume)
          case None => DBIO.successful(Unit)
        }

        _ <- (updateReq.credentialUpdates, updateReq.newCredentials) match {
          case (Some(updateCredentialsReq), None) => updateCredentials(
            user,
            updateCredentialsReq
          )
          case (None, Some(addCredentialsReq)) => addCredentials(
            userRow.id,
            user,
            addCredentialsReq
          )
          case (None, None) => DBIO.successful(Unit)
          case (Some(_), Some(_)) => DBIO.failed(HttpCodeException(BadRequest,
            "cannot both add credentials and update existing ones in the same request"
          ))
        }
      } yield Ok(JsString("updated"))
    }
  }

  private case class SubtractInventoryUpdateRequest(
    subtractInventoryPreference: SubtractInventoryType.Value
  )
  implicit private val subtractInventoryUpdateReqReads = Json.reads[SubtractInventoryUpdateRequest]

  def updateUserSubtractInventory = SecuredApiAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        SubtractInventoryUpdateRequest(subtractInventoryPreference) <- Try(r.body.as[SubtractInventoryUpdateRequest]).toDBIO
        _ <- r.authedUserQuery.map(_.subtractInventoryPreference).update(subtractInventoryPreference)
      } yield Ok(JsString("updated"))
    }
  }

  implicit private val logoMaxSize = 240
  def updateLogo = SecuredApiAction.async(parse.multipartFormData) { r =>
    val accountId = r.accountId
    val account = r.accountQuery

    db.runInTransaction {
      for {
        oldFilename <- account.map(info => info.companyLogo).result.head
        _ <- oldFilename match {
          case Some(oFilename) => fileUtils.removeFile(oFilename).toDBIO
          case None => DBIO.successful(Unit)
        }

        newFilename <- r.body.file("companyLogo") match {
          case Some(logo) => for {
            filename <- fileUtils.uploadUserImage(accountId, logo, logoMaxSize, logoMaxSize).toDBIO
          } yield Some(filename)
          case None => DBIO.successful(None)
        }
        _ <- account.map(info => info.companyLogo).update(newFilename)
      } yield Ok(JsString("updated"))
    }
  }

  case class PasswordResetRequest(
      token: EmailTokenReference,
      newPassword: String
  )
  import EmailTokenReference.jsonFormat
  implicit val passwordChangeRequestFormat = Json.format[PasswordResetRequest]

  def resetPassword = UserAwareApiAction.async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        _ <- failIf(r.identity.isDefined, HttpCodeException(BadRequest,
          ReadableError("_error" -> Messages("auth.resetpassword.alreadyauthed")).toJson
        )).toDBIO
        changeRequest <- Try(r.request.body.as[PasswordResetRequest]).toDBIO

        token <- emailTokenVerifier.getValidToken(changeRequest.token)
        _ <- failIf(token.tokenType != EmailTokenType.PasswordReset, HttpCodeException(Unauthorized,
          ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson
        )).toDBIO

        _ <- rawChangePassword(changeRequest.newPassword,
          Accounts.filter(_.id === token.owner).owner
        )
        _ <- EmailTokens.filter(_.id === token.id).map(_.used).update(true)
      } yield Ok(JsString("password changed"))
    }
  }

  def sendForgotPasswordEmail = UserAwareApiAction.async(parse.json) { implicit r =>
    val applicationUrl= config.getOptional[String]("applicationUrl").get
    db.runInTransaction {
      for {
        _ <- failIf(r.identity.isDefined, HttpCodeException(BadRequest,
          ReadableError("_error" -> Messages("auth.resetpassword.alreadyauthed")).toJson
        )).toDBIO
        rawEmailAddress <- Try(r.request.body.as[String]).toDBIO
        emailAddress = rawEmailAddress.toLowerCase
        userQuery = Users.filter(_.email === emailAddress)

        maybeUser <- userQuery.result.headOption
        maybeAccount <- userQuery.account.result.headOption
        loginInfos <- userQuery.userLogins.result
        maybeLoginInfo = loginInfos.find(_.providerId == CredentialsProvider.ID)

        contentString <- (maybeLoginInfo, maybeAccount) match {
          case (Some(info), Some(account)) => {
            emailTokenVerifier.createToken(EmailTokenType.PasswordReset, account.id).map {
              case EmailTokenReference(tokenID, tokenSecret) => s"""
                <p>Dear ${maybeUser.flatMap(_.firstName).getOrElse("Subscriber")},</p>
                <p></p>
                <p>
                  Someone requested a password reset for your account at
                  <a href="https://www.parsleycooks.com/">Parsley</a>.

                  If this was you, please click
                  <a href="$applicationUrl/auth/passwordreset?tokenID=$tokenID&tokenSecret=$tokenSecret">here</a>
                </p>
                <p></p>

                <p>
                  If you did not request this e-mail, please let us know at
                  service@parsleycooks.com.
                </p>
                <p></p>

                <p>Thank you!</p>
                <p></p>
                <p>The Parsley Team</p>
              """
            }
          }
          case (None, _) if loginInfos.nonEmpty => DBIO.successful(s"""
            <p>Dear ${maybeUser.flatMap(_.firstName).getOrElse("Subscriber")},</p>
            <p></p>
            <p>
              Someone requested a password reset for your account at
              <a href="https://www.parsleycooks.com/">Parsley</a>.
              However, your account uses a third-party login service.

              If this was you, you can log in at
              <a href="$applicationUrl">$applicationUrl</a>
              with your ${loginInfos.head.providerId} account
              ${loginInfos.head.email.getOrElse("")}.

            </p>
            <p></p>

            <p>
              If you did not request this e-mail, please let us know at
              service@parsleycooks.com.
            </p>
            <p></p>

            <p>Thank you!</p>
            <p></p>
            <p>The Parsley Team</p>
          """)
          case _ => DBIO.successful(s"""
            <p>Dear ${maybeUser.flatMap(_.firstName).getOrElse("Subscriber")},</p>
            <p></p>
            <p>
              Someone requested a password reset for your account at
              <a href="https://www.parsleycooks.com/">Parsley</a>. However, there
              is no account registered for this e-mail address.


              If this was you, you can create an account at
              <a href="$applicationUrl">$applicationUrl</a>
            </p>
            <p></p>

            <p>
              If you did not request this e-mail, please let us know at
              service@parsleycooks.com.
            </p>
            <p></p>

            <p>Thank you!</p>
            <p></p>
            <p>The Parsley Team</p>
          """)
        }
        _ <- emailUtils.send(
          new Email(emailAddress.toLowerCase, "Parsley User"),
          new Email("service@parsleycooks.com", "Service at Parsley Software"),
          "Password Reset Request", contentString
        ).toDBIO
      } yield Ok(JsString("email sent"))
    }
  }

  def getOnboardingWidgetsInfo = SecuredApiAction.async { r =>
    import onboardingWidgetApiFormats.{onboardingWidgetFormat}
    db.run {
      for {
        visitedWidgets <- r.authedUserQuery.visitedWidgets.result
        widgets <- OnboardingWidgets.result
      } yield Ok(
        Json.toJson(OnboardingWidgetsApiEntity(
          visitedWidgets.map(_.widget),
          widgets
        ))
      )
    }
  }

  def updateOnboardingWidgetInfo = AdminOnlyAction.async(parse.json) { r =>
    import onboardingWidgetApiFormats.widgetFormat
    db.runInTransaction {
      for {
        OnboardingWidgetsRow(id, _, _, customMessage) <- Try(r.body.as[OnboardingWidgetsRow]).toDBIO
        widgets <- OnboardingWidgets.result

        // validation
        _ <- failIf(
          !widgets.exists(_.id == id),
          HttpCodeException(BadRequest, ReadableError("_error" -> "Could not update invalid widget").toJson)
        ).toDBIO

        _ <- OnboardingWidgets
              .filter(q => q.id === id)
              .map(_.customMessage)
              .update(customMessage)

      } yield Ok(JsString("success"))
    }
  }

  def setWidgetVisited = SecuredApiAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        OnboardingWidgetsVisited(widgetId) <- Try(r.body.as[OnboardingWidgetsVisited]).toDBIO
        widgets <- OnboardingWidgets.result
        _ <- failIfNone(
          widgets.map(_.id).find(_ == widgetId),
          HttpCodeException(BadRequest, ReadableError("_error" -> "Could not set as visited. Invalid widget key").toJson)
        ).toDBIO
        _ <- UserOnboardingWidgets += UserOnboardingWidgetsRow(r.authedUserId, widgetId)

      } yield Ok(JsString("success"))
    }
  }
}
