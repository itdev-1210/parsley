package controllers.api

import controllers.AuthUtils
import models.AccountPermissionLevel

import play.api.db.slick.HasDatabaseConfigProvider
import play.api.http.{ContentTypes, FileMimeTypes, HeaderNames, HttpProtocol}
import play.api.i18n.{I18nSupport, Langs, MessagesApi, MessagesImpl}
import play.api.libs.json.JsString
import play.api.mvc._

import com.mohiva.play.silhouette.api.actions.DefaultSecuredErrorHandler

import scala.concurrent.{ExecutionContext, Future}

trait ApiController
    // importing the stuff ControllerHelpers does, but NOT Results and Status - want
    // those types to be the same across libraries
    extends HttpProtocol with HeaderNames with ContentTypes
    with RequestExtractors with Rendering with RequestImplicits

    // and then the extras
    with I18nSupport with HasDatabaseConfigProvider[models.Tables.profile.type]
    with AuthUtils with JsonRestUtils {

  // replicating BaseControllerHelpers, without extending BaseController
  val controllerComponents: ControllerComponents
  def parse: PlayBodyParsers = controllerComponents.parsers
  implicit def ec: ExecutionContext = controllerComponents.executionContext
  implicit val messagesApi: MessagesApi = controllerComponents.messagesApi
  implicit val supportedLangs: Langs = controllerComponents.langs
  implicit val fileMimeTypes: FileMimeTypes = controllerComponents.fileMimeTypes
  val actionBuilder = controllerComponents.actionBuilder

  implicit val messagesProvider = MessagesImpl(supportedLangs.availables.head, messagesApi)

  val errorHandler = new DefaultSecuredErrorHandler(messagesApi) {
    override def onNotAuthenticated(implicit request: RequestHeader) = {
      Future.successful(Unauthorized(JsString("unauthed")))
    }

    override def onNotAuthorized(implicit request: RequestHeader) = {
      Future.successful(Forbidden(JsString("forbidden")))
    }
  }

  class ActionErrorHandler[R[_]] extends ActionFunction[R, R] {
    final def invokeBlock[A](request: R[A], block: R[A] => Future[Result]): Future[Result] = {
      block(request).recover(handleAPIError)(executionContext)
    }

    val executionContext = ec
  }

  def ApiAction = actionBuilder andThen new ActionErrorHandler[Request]
  val UserAwareApiAction = UserAwareAction andThen new ActionErrorHandler[UserAwareRequest]
  val UserLoggedInApiAction = UserLoggedInAction(errorHandler) andThen new ActionErrorHandler[UserLoggedInRequest]
  val SecuredApiAction = SecuredAction andThen new ActionErrorHandler[SecuredRequest]

  val AdminOnlyAction = SecuredApiAction andThen accountFilter(_.isAdmin)
  val StdlibAction = SecuredApiAction andThen accountFilter(a => a.isAdmin || a.isStdlib)

  def UserRestrictedAction(allowedPermLevels: AccountPermissionLevel.Value*) = SecuredApiAction andThen userPermFilter(allowedPermLevels.toSet)
}
