package controllers.api

import controllers.Env
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions.HttpCodeException
import util.{AccountProfile, AccountSetup, EmailTokenReference, EmailTokenVerifier, EmailUtils}

import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.google.inject.Inject
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.sendgrid.Email

import java.time.Instant

import scala.concurrent.Future
import scala.util.Try

class Sharing @Inject() (
    val emailUtils: EmailUtils,
    val config: Configuration,
    val emailTokenVerifier: EmailTokenVerifier,
    val synController: controllers.api.Sync,
    credentialsProvider: CredentialsProvider,
    passwordHasherRegistry: PasswordHasherRegistry,
    override val controllerComponents: ControllerComponents,
    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider,
    val accountSetup: AccountSetup,
  ) extends ApiController {

  import dbConfig.profile.api._
  import synController.DBActions.syncLocation

  case class ShareAccountRequest(
    email: String,
    level: AccountPermissionLevel.Value,
  )

  def shareAccount = UserRestrictedAction().async(parse.json) { implicit r =>

      db.runInTransaction {
        for {
          ShareAccountRequest(email, permissionLevel) <- Future.fromTry(Try(r.body.as(Json.reads[ShareAccountRequest]))).toDBIO
          emailAddress = email.toLowerCase

          _ <- purgeInvites(emailAddress)
          EmailTokenReference(tokenID, tokenSecret) <- emailTokenVerifier.createToken(EmailTokenType.UserSharedInvite, r.accountId)
          _ <- UserSharedInvites += UserSharedInvitesRow(0, tokenID, emailAddress, r.accountId, permissionLevel)

          _ <- sendInviteEmail(
            emailAddress,
            tokenID, tokenSecret,
            permissionLevel
          ).toDBIO
        } yield Ok(JsString("Invite Sent"))
      }
  }

  case class UserSharedInviteApiEntity(
    id: Int,
    inviteeEmail: String,
    inviteeLevel: AccountPermissionLevel.Value,
  )
  private implicit val userSharedInviteApiEntityFormats = Json.format[UserSharedInviteApiEntity]
  def pendingList() = UserRestrictedAction().async { implicit r =>
    db.run {
      for {
        userSharedInvites <- r.accountQuery.userInvites.filter { inv =>
          inv.emailToken in EmailTokens.filter(t => t.owner === r.accountId && t.used === false && t.tokenType === EmailTokenType.UserSharedInvite).map(_.id)
        }.result
      } yield Ok(Json.toJson(userSharedInvites
        .groupBy(_.inviteeEmail)
        .collect { case (inv, values) =>
          inv -> values.maxBy(_.emailToken) // only get latest token
        }
        .values
        .map(inv =>
        Json.toJsObject(UserSharedInviteApiEntity(inv.id, inv.inviteeEmail, inv.inviteLevel))
      )))
    }
  }

  case class UpdatePendingInvitation(id: Int, inviteeLevel: AccountPermissionLevel.Value, resendEmail: Boolean = false)
  private implicit val updatePendingInvitationFormat = Json.format[UpdatePendingInvitation]
  def updatePending() = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        UpdatePendingInvitation(id, newInviteLevel, resendEmail) <- Try(r.body.as[UpdatePendingInvitation]).toDBIO
        sharedInviteQuery = r.accountQuery.userInvites.filter(_.id === id)
        pendingTokenQuery = EmailTokens.filter(_.id in sharedInviteQuery.map(_.emailToken)).filter(_.used === false)
        mayBePendingToken <- pendingTokenQuery.map(_.id).result.headOption
        _ <- failIf(
          mayBePendingToken.isEmpty,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Not Pending Invitation").toJson),
        ).toDBIO

        pendingTokenID = mayBePendingToken.get
        _ <- if (!resendEmail) sharedInviteQuery.map(_.inviteLevel).update(newInviteLevel)
          else emailTokenVerifier.createToken(EmailTokenType.UserSharedInvite, r.accountId).flatMap {
            case EmailTokenReference(tokenID, tokenSecret) =>
              sharedInviteQuery
                .map(inv => (inv.emailToken, inv.inviteLevel))
                .update((tokenID, newInviteLevel))
                .andThen(EmailTokens.filter(_.id === pendingTokenID).delete)
                .andThen(sharedInviteQuery.map(_.inviteeEmail).result.head)
                .map(inviteeEmail => sendInviteEmail(
                  inviteeEmail,
                  tokenID, tokenSecret,
                  newInviteLevel,
                ).toDBIO)
          }
      } yield Ok(JsString("updated"))
    }
  }

  def deletePending(id: Int) = UserRestrictedAction().async { implicit r =>
    val sharedInviteQuery = r.accountQuery.userInvites.filter(_.id === id)
    db.runInTransaction {
      for {
        mayBePendingInvite <- EmailTokens.filter(_.id in sharedInviteQuery.map(_.emailToken)).filter(t => t.used === false && t.tokenType === EmailTokenType.UserSharedInvite).map(_.id).result.headOption
        _ <- failIf(
          mayBePendingInvite.isEmpty,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Not Pending Invitation").toJson),
        ).toDBIO
        inviteeEmail <- sharedInviteQuery.map(_.inviteeEmail).result.head // must exist
        // find invites sent to same email address and also delete all of them
        _ <- purgeInvites(inviteeEmail)
      } yield Ok(JsString("deleted"))
    }
  }

  case class UpdateUserPermission(
    userId: Int,
    permission: AccountPermissionLevel.Value,
  )
  private implicit val userPermissionReads = Json.reads[UpdateUserPermission]
  case class UpdateSharing(
    permissions: Seq[UpdateUserPermission],
  )

  def updateSharing() = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        UpdateUserPermission(userId, newPermission) <- Try(r.body.as(Json.reads[UpdateUserPermission])).toDBIO
        found <- r.accountQuery.userAccounts.filter(_.userId === userId)
            .map(_.permissionLevel).update(newPermission)
            .map(numUpdated => numUpdated > 0)
        _ <- failIf(
          !found,
          HttpCodeException(BadRequest, ReadableError("_error" -> "User not sharing account").toJson)
        ).toDBIO
      } yield Ok(JsString("updated"))
    }
  }

  def updateSharingBulk() = UserRestrictedAction().async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        UpdateSharing(newPermissions) <- Future.fromTry(Try(r.body.as(Json.reads[UpdateSharing]))).toDBIO
        sharedAccountUserIds <- r.accountQuery.userAccounts.filter(_.userId.inSet(newPermissions.map(_.userId))).result
        _ <- failIf(
          sharedAccountUserIds.size != newPermissions.size,
          HttpCodeException(BadRequest, ReadableError("_error" -> "User not sharing account").toJson)
        ).toDBIO

        _ <- DBIO.sequence(newPermissions.map(newPerm =>
          r.accountQuery.userAccounts.filter(_.userId === newPerm.userId)
              .map(_.permissionLevel).update(newPerm.permission)
        ))
      } yield Ok(JsString("updated"))
    }
  }

  case class LeaveAccountRequest(ownerId: Int)

  def leaveAccount() = SecuredApiAction.async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        LeaveAccountRequest(ownerId) <- Try(r.body.as(Json.reads[LeaveAccountRequest])).toDBIO

        _ <- failIf(
          r.isAccountOwner,
          HttpCodeException(BadRequest, ReadableError("_error" -> "Account owner cannot leave account").toJson)
        ).toDBIO

        _ <- failIf(
          r.accountId != ownerId,
          HttpCodeException(BadRequest, ReadableError("_error" -> s"User is not member of requested account").toJson)
        ).toDBIO

        _ <- UserAccounts
          .filter(t => t.userId === r.authedUserId && t.owner === ownerId)
          .delete
      } yield Ok(JsString("success"))
    }
  }

  def removeSharedUser(userId: Int) = UserRestrictedAction().async(parse.json) { implicit r =>
    val userToRemoveQuery = r.accountQuery.userAccounts.filter(sa =>
      sa.userId === userId && sa.isActive === true
    )
    db.runInTransaction {
      for {
        isShared <- userToRemoveQuery.exists.result

        _ <- failIf(
          !isShared,
          HttpCodeException(BadRequest, ReadableError("_error" -> "No shared user found to remove").toJson)
        ).toDBIO

        _ <- purgeSharedUser(userToRemoveQuery)
      } yield Ok(JsString("removed"))
    }
  }

  private def sendInviteEmail(
      toEmailAddress: String,
      tokenID: Int,
      tokenSecret: String,
      level: AccountPermissionLevel.Value
    )(implicit r: SecuredRequest[_]): Future[_] = {
      val applicationUrl= config.getOptional[String]("applicationUrl").get
      val msg = if (level == AccountPermissionLevel.RecipeReadOnly)
        "has added you as a user to their Parsley recipe display account"
      else "has added you as a user to their Parsley account"

      val contentString = s"""
        <p></p>
        <p>
          ${r.identity.dbRow.firstName.map(_.capitalize).getOrElse("Someone")} ${msg}.
          Click <a href="$applicationUrl/auth/confirm_invite/$tokenID/$tokenSecret">here</a> to confirm.
        </p>
        <p></p>
        <p></p>
        <p>The Parsley Team</p>
      """
      emailUtils.send(
        new Email(toEmailAddress, "Parsley User"),
        new Email("service@parsleycooks.com", "Service at Parsley Software"),
        s"Invitation to Join ${r.identity.dbRow.firstName.map(_.capitalize + "'s ").getOrElse("")}Parsley Account", contentString
      )
  }

  private def purgeInvites(inviteeEmail: String)(implicit r: SecuredRequest[_]): DBIO[Unit] = {
    val matchingInviteQuery = r.accountQuery.userInvites.filter(_.inviteeEmail === inviteeEmail)
    for {
      emailTokenIds <- matchingInviteQuery.map(_.emailToken).result
      _ <- matchingInviteQuery.delete
      _ <- EmailTokens.filter(_.id inSet emailTokenIds).delete
    } yield ()
  }

  // remove shared user logins and stdlibs data
  private def purgeSharedUser(userToRemoveQuery: AllColumnsQuery[UserAccounts, UserAccountsRow]) = {
    for {
      userHasOtherAccounts <- (userToRemoveQuery.user.userAccounts.size > 1).result
      _ <- if (userHasOtherAccounts) {
        DBIO.successful(Unit)
      } else for {
        // only remove user login and disable sharing
        _ <- userToRemoveQuery.user.userLogins.delete
        _ <- userToRemoveQuery.user.map(_.email).update(None)
      } yield ()
      _ <- userToRemoveQuery.map(_.isActive).update(false)
    } yield ()
  }

  private sealed case class SubordinateUserDescription(
      id: Int,
      email: Option[String],

      // for shared users only
      firstName: Option[String] = None,
      lastName: Option[String] = None,
      sharedAccount: Option[Int] = None,
      owner: Option[Int] = None,
      joinedAt: Option[Instant] = None,
      isActive: Boolean = true,
      isReadOnly: Boolean = false, // fallback for frontend
      permissionLevel: Option[AccountPermissionLevel.Value] = None,

      // for downstreams only
      upstreamRelativeName: Option[String] = None,
      downstreamActivated: Option[Boolean] = None,
  )
  private implicit val subordinateUserWrites = Json.writes[SubordinateUserDescription]


  // fields that only apply to users with access to account, not multi-location downstreams
  private sealed case class SharedUserDescription(
      sharedAccount: Int,
  )
  private implicit val sharedUserWrites = Json.writes[SharedUserDescription]
  import invitesAPIFormats._

  def sharedUsers() = SecuredApiAction.async { r =>

    val sharedAccountQuery = (
      r.accountQuery.userAccounts.filter(ua => ua.userId =!= r.account.accountOwner && ua.isActive)
      join Users on (_.userId === _.id)
    )
    db.run { sharedAccountQuery.result }.map { sharedAccWithDetail =>
      sharedAccWithDetail.map { case (sharedAcc, userDetail) =>
        SubordinateUserDescription(
          userDetail.id,
          userDetail.email,
          userDetail.firstName,
          userDetail.lastName,
          sharedAccount = Some(sharedAcc.userId), // should equal userDetail.id, no?
          joinedAt = Some(sharedAcc.joinedAt),
          isActive = sharedAcc.isActive,
          isReadOnly = sharedAcc.permissionLevel == AccountPermissionLevel.RecipeReadOnly,
          permissionLevel = Some(sharedAcc.permissionLevel),
        )
      }
    }.map(descriptions => Ok(Json.toJson(descriptions)))
  }

  def multiLocationDownstreams() = SecuredApiAction.async { r =>
    db.runInTransaction {
      for {
        downstreamAccounts <- r.accountQuery.downstreams.result
        downstreamEmailsFromOwner <- (r.accountQuery.downstreams join Users on (_.accountOwner === _.id)).map {  case (downstream, downstreamOwner) =>
            downstream.id -> downstreamOwner.email
        }.result
        downstreamEmailsFromInvite <- UpstreamInvites.filter(_.inviter === r.accountId)
            .map(i => (i.invitee, i.inviteeEmail))
            .result
      } yield {
        val fromOwnerEmailMap = downstreamEmailsFromOwner.collect {
          case (id, Some(email)) => id -> email
        }.toMap
        val emailMap = downstreamEmailsFromInvite.toMap ++ fromOwnerEmailMap // want the latter to take precedence, where it exists

        Ok(Json.toJson(downstreamAccounts.map(
          a => SubordinateUserDescription(
            a.id, emailMap.get(a.id),
            upstreamRelativeName = a.upstreamRelativeName,
            downstreamActivated = Some(a.accountOwner.isDefined),
          )
        )))
      }
    }
  }
}
