package controllers.api

import controllers.Env
import models._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import util.AsyncUtils._

import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette
import _root_.util.Exceptions.HttpCodeException

import javax.inject.Inject
import java.time.{Instant}
import scala.util.Try

class IngredientPars @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val inventoriesController: controllers.api.Inventories,
) extends ApiController {

  import dbConfig.profile.api._
  import inventoryAPIFormats._
  import inventoriesController.DBActions._

  // Quite similar to `getTodayInventoryItems`
  // In that we combine ingredients from suppliers & recipes with existing pars
  // This is simpler as fetching existing pars is more straightforward than finding inventory items from the latest inventory
  // Most API objects and formats are shared
  def getIngredientPars = SecuredApiAction.async { r =>
    db.runInTransaction {
      val products = r.accountQuery.products.filter(!_.tombstone)
      def allowedOnPars(p: Tables.Products) = p.lastRemovedFromPars.isEmpty ||
            p.lastAddedToRecipe.exists(p.lastRemovedFromPars <= _)

      for {
        (additionalFromSuppliersAndRecipesQuery, advancePrepsQuery, filterOutIds) <- getAdditionalItemsQueries(products, allowedOnPars)

        existingPars <- r.accountQuery.ingredientPars.filter(!_.product.inSet(filterOutIds)).result
        existingProductIds = existingPars.map(_.product)

        additionalFromSuppliersAndRecipes <- additionalFromSuppliersAndRecipesQuery.filter(
          !_.id.inSet(existingProductIds)
        ).result
        productIdsFromSuppliersAndRecipes = additionalFromSuppliersAndRecipes.map(_.id)

        // Advanced preps in recipes
        advancedPreps <- advancePrepsQuery.result
        productIdsFromAdvancedPreps = advancedPreps.map(_.inputProduct)

        // Get products' name and tags
        productNameTagMap <- getIngredientsNameTagMap(
          products,
          (existingProductIds ++ productIdsFromSuppliersAndRecipes ++ productIdsFromAdvancedPreps).distinct
        )
      } yield {
        val fromExistingPars = existingPars.map(i => ingredientParToNewInventoryItem(
          i,
          productNameTagMap(i.product)._1,
          productNameTagMap(i.product)._2,
        ))
        val fromSuppliersAndRecipes = additionalFromSuppliersAndRecipes.map(
          i => ingredientToNewInventoryItem(i, productNameTagMap(i.id)._2)
        )
        val fromAdvancedPreps = advancedPreps.filter(p => !existingProductIds.contains(p.outputProduct)).map(p =>
          advancedPrepToNewInventoryItem(p, productNameTagMap(p.inputProduct)._1, productNameTagMap(p.inputProduct)._2)
        )
        Ok(JsArray((fromExistingPars ++ fromSuppliersAndRecipes ++ fromAdvancedPreps).map(Json.toJsObject(_))))
      }
    }
  }

  implicit lazy val ingredientParAPIFormat = FancyFormat[IngredientParsRow](ingredientParFormat,
    mockIdFields=Seq("id", "owner"))

  case class SaveIngredientParsRequest(
    items: Seq[IngredientParsRow],
    removedProductIds: Seq[Int],
  )
  implicit val saveInventoryReqFormat = Json.format[SaveIngredientParsRequest]

  def saveIngredientPars = UserRestrictedAction(AccountPermissionLevel.Shared).async(parse.json) { r =>
    val userId = r.accountId

    db.runInTransaction {
      for {
        request <- Try(r.body.as[SaveIngredientParsRequest]).toDBIO

        // Delete existing ingredient pars
        changedProductIds = request.items.map(i => i.product)
        _ <- r.accountQuery.ingredientPars.filter(i =>
          i.product.inSet(changedProductIds ++ request.removedProductIds)
        ).delete

        // Update removed ingredients' last_removed_from_pars timestamp
        _ <- r.accountQuery.products.filter(
          _.id.inSet(request.removedProductIds)
        )
        .map(_.lastRemovedFromPars)
        .update(Some(Instant.now()))

        // Re-insert the changed inventory items
        _ <- IngredientPars ++= request.items.map(i => i.copy(
          owner=userId
        ))
      } yield Ok(JsString("success"))
    }
  }

  def ingredientParsExists = SecuredApiAction.async { r =>
    db.run {
      for {
        res <- r.accountQuery.ingredientPars.exists.result
      } yield Ok(JsBoolean(res))
    }
  }
}
