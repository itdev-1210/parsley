package controllers.api

import controllers._
import models._
import models.QueryExtensions._
import models.Tables._
import util.AsyncUtils._
import util.Exceptions.HttpCodeException
import util.{ProductSync, SupplierSync}

import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

import scala.util.Try

class Sync @Inject()(
    override val controllerComponents: ControllerComponents,
    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider,
    val productsController: controllers.api.Products,
    val productSync: ProductSync,
    val supplierSync: SupplierSync,
) extends ApiController {
  import dbConfig.profile.api._

  object DBActions {
    def syncAccount(
      authedAccount: AllColumnsQuery[Tables.Accounts, AccountsRow],
      upstreamId: Int,
      downstreamId: Int,
      products: Seq[Int],
      suppliers: Seq[Int]
    ) : DBIO[Unit] = {
      val upstreamSuppliersQuery = authedAccount.suppliers.filter(_.id inSet suppliers)
      val upstreamProductsQuery = authedAccount.products.filter(_.id inSet products) ++
          supplierSync.directProductDependencies(upstreamSuppliersQuery)
      for {
        _ <- productSync.syncMultiLocation(downstreamId, upstreamId, upstreamProductsQuery)
        _ <- supplierSync.sync(
          downstreamId,
          (_: Tables.Suppliers).syncUpstreamOwner.getOrElse(-1) === upstreamId,
          upstreamSuppliersQuery
        )
      } yield Unit
    }

    def syncLocation(
      authedAccount: AllColumnsQuery[Tables.Accounts, AccountsRow],
      upstreamId: Int,
      downstreamId: Int,
      recipeCategories: Seq[Int], // id in the `categories` table
      suppliers: Seq[Int]
    ) : DBIO[Unit] = for {
      _ <- authedAccount.locationsSyncedRecipes.filter(_.locationId === downstreamId).delete
      _ <- LocationsSyncedRecipes ++= recipeCategories.map(rc => LocationsSyncedRecipesRow(upstreamId, downstreamId, "", rc))

      _ <- authedAccount.locationsSyncedSuppliers.filter(_.locationId === downstreamId).delete
      _ <- LocationsSyncedSuppliers ++= suppliers.map(s => LocationsSyncedSuppliersRow(upstreamId, downstreamId, s))

      products <- authedAccount.recipeCategories
          .filter(_.id.inSet(recipeCategories))
          .productCategories.products
          .map(_.id).distinct.result
      _ <- syncAccount(authedAccount, upstreamId, downstreamId, products, suppliers)
    } yield Unit
  }

  private def syncAccounts(accountIds: Seq[Int]) =
    DBIO.sequence(accountIds.map(productSync.syncStdlib))

  def globalStdlibSync() = StdlibAction.async {
    db.runInTransaction {
      for {
        accountIds <- Accounts.filter(!_.isStdlib).map(_.id).result
        _ <- syncAccounts(accountIds)
      } yield Ok(Json.obj("totalUsers" -> JsNumber(accountIds.size)))
    }
  }

  def partialStdlibSync() = StdlibAction.async(parse.json) { r =>
    db.runInTransaction {
      for {
        requestedAccountIds <- Try(r.body.as[Seq[Int]]).toDBIO
        accountIds <- Accounts.filter(a => (a.id inSet requestedAccountIds) && !a.isStdlib)
                .map(_.id).result
        _ <- syncAccounts(accountIds)
      } yield Ok(Json.obj("totalAccounts" -> JsNumber(accountIds.size)))
    }
  }
}
