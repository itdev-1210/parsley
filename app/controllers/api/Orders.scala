package controllers.api

import controllers.Env
import controllers.api.RecipeUtils._
import models.JsonConversions._
import models.QueryExtensions._
import models.AccountPermissionLevel
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions._
import util.OptionGetPf

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json.{JsArray, JsString, JsValue, Json}
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

import scala.concurrent.ExecutionContext
import scala.util.Try

class Orders @Inject()(
  override val controllerComponents: ControllerComponents,
  override val silhouette: Silhouette[Env],
  override val dbConfigProvider: DatabaseConfigProvider,
  val menusController: Menus,
  val transactionsController: Transactions,
  val purchaseOrders: PurchaseOrders,
) extends ApiController {

  import dbConfig.profile.api._
  import menusController.{ DBActions => MenuDBActions }
  import transactionsController.{ DBActions => TransactionDBActions }

  def listOrders = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val ordersQuery = r.accountQuery.orders
      for {
        orders <- ordersQuery.map(o => (o.id, o.name, o.xact)).result
        transactions <- ordersQuery.transaction.map(xact => (xact.id -> (xact.time, xact.includeInCalculatedInventory))).result
      } yield {
        val xactMap = transactions.toMap
        val entries = orders.map {
          case (id, name, xactId) =>
            val xact = xactMap(xactId)
            Json.obj("id" -> id, "name" -> name, "date" -> xact._1, "subtractFromCurrentInventory" -> xact._2)
        }
        Ok(JsArray(entries))
      }
    }
  }

  def getOrderDetails(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val order = r.accountQuery.orders.filter(_.id === id)
      for {
        maybeInfo <- order.result.headOption
        mainInfo <- maybeInfo match {
          case Some(info) => DBIO.successful(info)
          case None => DBIO.failed(HttpCodeException(NotFound, "no such order"))
        }

        xact <- TransactionDBActions.fetchTransaction(mainInfo.xact, r.accountQuery)
            // foreign key constraint should guarantee success
            .map(_.get)

        (menuId, menuVisible) <- order.menu.map(m => (m.id, m.userVisible)).result.head
        menuAPIVal <- {
          if (menuVisible) DBIO.successful(Right(menuId))
          else MenuDBActions.fetchMenuAPIEntity(menuId, r.accountQuery)
              .map(_.get) // is a serious 503 if foreign key constraint violated
              .map(Left(_))
        }
      } yield {
        import ordersAPIFormats.ordersAPIFormat
        Ok(Json.toJsObject(OrderAPIEntity(mainInfo, xact, menuAPIVal)))
      }
    }
  }

  def orderFromJson(
      json: JsValue, ownerId: Int, maybeId: Option[Int] = None
  ): Try[OrderAPIEntity] = {
    import ordersAPIFormats.ordersAPIFormat

    val dbId = maybeId.getOrElse(-1)

    for {
      rawEntity <- Try(json.as[OrderAPIEntity])
    } yield {
      val cleanedMenu = rawEntity.usedMenu.left.map(
        MenuDBActions.cleanupForDBInsertion(_, ownerId, userVisible=false))
      val cleanedMainInfo = rawEntity.main.copy(id=dbId, owner=ownerId)
      OrderAPIEntity(cleanedMainInfo, rawEntity.transaction, cleanedMenu)
    }
  }

  def createCustomerOrdersTransaction(
    account: AllColumnsQuery[Tables.Accounts, AccountsRow],
    ownerId: Int,
    transaction: TransactionAPIEntity
  ) : DBIO[Int] = {
    val cleanedTransaction = TransactionDBActions.cleanupForDBInsertion(transaction, ownerId)
    for {
      // Create the transaction first to get recipe deltas' id
      // Which will be inserted into their ingredient deltas' `caused_by`
      (transactionId, transactionDeltaIds) <- TransactionDBActions.createTransactionFromAPIEntity(cleanedTransaction)

      // Create ingredient usage deltas, see PARS-2155
      recipeIds = transaction.deltas.map(_.main.product).collect(OptionGetPf)
      productionInfo <- deepFetchProductionInfo(recipeIds)(account, ec)
      _ <- DBIO.sequence(transactionDeltaIds.zip(transaction.deltas).map {
        case (deltaId, delta) => {
          val recipeId = delta.main.product.get
          for {
            bom <- generateProductionOrder(Map(recipeId -> Quantity(delta.main.amount, delta.main.measure, delta.main.unit)), productionInfo).toDBIO
            preps <- account.products.filter(_.id.inSet(bom.keys)).preppedBy.filter(_.advancePrep).map(p => (p.outputProduct, p.inputProduct)).result
            (advancePrepProductIds, originalIngredientProductIds) = preps.toSet.unzip
            ingredients = bom.values.filter(p =>
              p.required.amount > 0 &&
              (p.ingredientsUsed.isEmpty || advancePrepProductIds.contains(p.productId)) && // Filter out recipes but not advance preps
              !originalIngredientProductIds.contains(p.productId) // Filter out original ingredients of advance preps
            )
            _ <- TransactionDeltas ++= ingredients.map(i =>
              TransactionDeltasRow(
                -1, transactionId, ownerId, Some(i.productId),
                -i.required.amount, i.required.measure, i.required.unit,
                causedBy = Some(deltaId)
              )
            )
          } yield Unit
        }
      })
    } yield transactionId
  }

  def createOrder = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      for {
        OrderAPIEntity(mainInfo, transaction, menuData) <- orderFromJson(r.body, r.accountId).toDBIO
        menuId <- menuData match {
          // create the new menu *in the same transaction*
          case Left(inlineMenu) => MenuDBActions.createMenuFromAPIEntity(inlineMenu)
          // just check that the menu exists - prefer to give BadRequest instead of InternalServerError
          case Right(id) => Menus.filter(_.id === id).size.result
              .flatMap { numFound =>
                if (numFound == 1) DBIO.successful(id)
                else DBIO.failed(HttpCodeException(BadRequest, s"reference to nonexistent menu"))
              }
        }
        transactionId <- createCustomerOrdersTransaction(r.accountQuery, r.accountId, transaction)
        orderId <- (Orders returning Orders.map(_.id)) += mainInfo.copy(
          menu=menuId, xact=transactionId
        )
      } yield Created(Json.obj("id" -> orderId))
    }
  }

  def modifyOrder(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async(parse.json) { r =>
    db.runInTransaction {
      val order = r.accountQuery.orders.filter(_.id === id)
      for {
        OrderAPIEntity(mainInfo, transaction, menuData) <- orderFromJson(r.body, r.accountId, Some(id)).toDBIO

        orderFound <- order.exists.result
        _ <- failIf(!orderFound, HttpCodeException(NotFound, "no such order")).toDBIO

        (oldMenuWasUserVisible, oldMenuId) <- order.menu.map(m => (m.userVisible, m.id)).result.head
        menuId <- (menuData, oldMenuWasUserVisible) match {
          // modify existing implicit menu
          case (Left(inlineMenu), false) => {
            // no error checking, because we've verified above that the menu already exists
            MenuDBActions.modifyMenuFromAPIEntity(inlineMenu, r.accountQuery, oldMenuId)
                .andThen(DBIO.successful(oldMenuId))
          }
          // were previously referencing a named menu, but now using an implicit one
          case (Left(inlineMenu), true) => {
            MenuDBActions.createMenuFromAPIEntity(inlineMenu)
          }
          // switching from one named menu to another (or same one) - orders row update will handle change
          case (Right(menuId), true) => DBIO.successful(menuId)
          // switching from implicit to named menu
          case (Right(menuId), false) => for {
            // here handle deleting old one, let orders row update handle association with new one
            _ <- order.menu.sections.items.delete
            _ <- order.menu.sections.delete
            _ <- order.menu.delete
          } yield menuId
        }
        // this check should be irrelevant in implicit-menu case since we've just created/modified it, but
        // easier to just always check
        numMenusFound <- Menus.filter(_.id === menuId).size.result
        _ <- failIf(numMenusFound != 1,
          HttpCodeException(BadRequest, s"reference to nonexistent menu")
        ).toDBIO

        // drop and re-create the associated transaction
        // NOTE: this may need to change as we keep info about the relationships
        // between transactions!
        origTransactionId <- order.map(_.xact).result.head
        _ <- TransactionDBActions.deleteTransaction(r.accountQuery, origTransactionId)
        newTransactionId <- createCustomerOrdersTransaction(r.accountQuery, r.accountId, transaction)

        _ <- order.update(mainInfo.copy(menu = menuId, xact = newTransactionId))
      } yield Ok(JsString("success"))
    }
  }

  def deleteOrder(id: Int) = UserRestrictedAction(AccountPermissionLevel.Shared, AccountPermissionLevel.Operations).async { r =>
    db.runInTransaction {
      val order = r.accountQuery.orders.filter(_.id === id)
      for {
        // protect from deletion of other dependent data
        orderExists <- order.exists.result
        _ <- failIf(!orderExists, HttpCodeException(BadRequest, "No such order")).toDBIO

        purchaseOrderIds <- PurchaseOrderCustomerOrders.filter(_.customerOrder in order.map(_.id)).map(_.purchaseOrder).result
        hasOtherCustOrderFlagPerPurchaseOrder <- DBIO.sequence(purchaseOrderIds.map { purchaseOrderId =>
          for {
            hasOtherCustomerOrder <- PurchaseOrderCustomerOrders.filter(mapping => mapping.purchaseOrder === purchaseOrderId && mapping.customerOrder =!= id).exists.result
          } yield (purchaseOrderId -> hasOtherCustomerOrder)
        })
        _ <- DBIO.sequence(hasOtherCustOrderFlagPerPurchaseOrder.map { case (purchaseOrderId, hasOtherCustomerOrder) =>
          if (!hasOtherCustomerOrder)
            purchaseOrders.DBActions.deletePurchaseOrder(r.accountQuery.purchaseOrders.filter(_.id === purchaseOrderId))
          else
            PurchaseOrderCustomerOrders.filter(_.customerOrder in order.map(_.id)).delete.map(numDeleted => purchaseOrderId)
        })

        menuToDelete <- order.menu.filter(!_.userVisible).map(_.id).result.headOption
        xactToDelete <- order.map(_.xact).result.head

        numDeleted <- order.delete
        _ <- TransactionDBActions.deleteTransaction(r.accountQuery, xactToDelete)

        _ <- menuToDelete.map(MenuDBActions.deleteMenu(r.accountQuery, _))
            .getOrElse(DBIO.successful(false))
      } yield {
        if (numDeleted == 1) Ok(JsString("success"))
        else NotFound.parsleyFormattedError("no such order")
      }
    }
  }
}
