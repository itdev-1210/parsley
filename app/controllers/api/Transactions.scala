package controllers.api

import controllers._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions.HttpCodeException
import util.OptionGetPf

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import javax.inject.Inject

/**
  * Not really a controller, in that it doesn't actually handle any HTTP reqs
  * directly. Just container for DBActions object, to match structure of other
  * controllers that export DBActions for external use
  */
class Transactions @Inject()(
    override val controllerComponents: ControllerComponents,
    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider
) extends ApiController {

  import dbConfig.profile.api._

  object DBActions {
    /**
      *
      * @param id
      * @param account
      * @return The Some(apiEntity) if a transaction exists under that ID and user, otherwise None
      */
    def fetchTransaction(
        id: Int, account: AllColumnsQuery[models.Tables.Accounts, AccountsRow]
    ): DBIO[Option[TransactionAPIEntity]] = {

      val xact = account.transactions.filter(_.id === id)
      for {
        mainInfo <- xact.result.head
        deltas <- xact.deltas.result
        transactionMeasures <- xact.deltas.measures.result
      } yield {
        Some(TransactionAPIEntity(mainInfo, deltas.map { delta =>
          TransactionDeltaEntity(
            main = delta,
            measures = transactionMeasures.filter(_.transactionDelta == delta.id)
          )
        }))
      }
    }.recover { case e: NoSuchElementException => None }

    /**
      * Sets up the transaction with the appropriate ownerId and internal ID references
      *
      * NOTE: Result Transaction will always have empty measure list because we are
      * ignoring transaction measures when reading from Json.
      * Populate measures row if product_source is not null when inserting to db.
      * @param rawEntity
      * @param ownerId
      * @param maybeId
      * @return
      */
    def cleanupForDBInsertion(
        rawEntity: TransactionAPIEntity, ownerId: Int, maybeId: Option[Int] = None
    ): TransactionAPIEntity = {
      // cleanup for insertion is a bit of a royal pain, since owner ID is
      // included at every level for in-DB business-logic verification.

      var cleanedDeltas = maybeId match {
        case Some(xactId) =>
          rawEntity.deltas.map(d => d.copy(main = d.main.copy(xact = xactId)))
        case None => rawEntity.deltas
      }
      cleanedDeltas = cleanedDeltas.map(d => d.copy(main = d.main.copy(owner=ownerId), measures = Seq.empty))
      cleanedDeltas = cleanedDeltas.filter(delta =>
          delta.main.amount != 0 || delta.main.cashFlow.nonEmpty
      )

      TransactionAPIEntity(
        rawEntity.main.copy(owner = ownerId, id = maybeId.getOrElse(-1)),
        cleanedDeltas,
      )
    }

    /**
      * Transactions have a weird property - they can refer to non-measured products.
      *.
      * This means that we have a case (null measure in a delta) that isn't covered
      * by postgres key checking, and so to have a bare minimum of safety against
      * browser-side bugs introducing bad data we need to check this here
      *
      * @param account
      * @param apiEntity
      * @return
      */
    def validateTransactionReferences(
        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
        apiEntity: TransactionAPIEntity,
    ): DBIO[Unit] = {
      val unmeasuredProducts = apiEntity.deltas.filter(_.main.measure.isEmpty) // include deltas with no units specified...
          .map(_.main.product).collect(OptionGetPf) // don't include no-product rows

      for {
        hasBadProds <- account.products.measures.filter(
          _.product inSet unmeasuredProducts
        ).exists.result
        _ <- failIf(hasBadProds,
          new HttpCodeException(BadRequest, "attempted to use measured products in unmeasured transaction_delta")
        ).toDBIO
      } yield Unit
    }

    /**
      *
      * @param apiEntity
      * @param copyPackageInfo Copies Product Source Info for future reference. Use for finalized transactions
      * @return (The ID of the newly-created transaction, The IDs of the newly-created transaction deltas)
      */
    def createTransactionFromAPIEntity(
      apiEntity: TransactionAPIEntity,
      copyPackageInfo: Boolean = false,
      copyCashInfo: Boolean = false,
    ): DBIO[(Int, Seq[Int])] = {
      val TransactionAPIEntity(mainInfo, deltas) = apiEntity
      for {
        transactionId <- (Transactions returning Transactions.map(_.id)) += mainInfo
        // TODO: Handle the case of a (predicted, finalized) pair of transactions
        productSources <- if (!copyPackageInfo) DBIO.successful(Seq.empty) else ProductSources.filter(_.id inSet deltas.flatMap(_.main.productSource)).result
        deltaWithPackageInfo = deltas.map(_.main.copy(xact = transactionId)).map(deltaMain => {
          val productSource = deltaMain.productSource.flatMap(productSourceId => productSources.find(_.id == productSourceId))
          productSource.map(ps => deltaMain.copy(
            sourceMeasure = Some(ps.measure),
            sourceUnit = Some(ps.unit),
            packaged = Some(ps.packaged),
            pricePer = Some(ps.pricePer),
            pricingMeasure = ps.pricingMeasure,
            pricingUnit = ps.pricingUnit,
            packageName = ps.packageName,
            subPackageName = ps.subPackageName,
            packageSize = Some(ps.packageSize),
            superPackageSize = Some(ps.superPackageSize),
            cashFlow = if (copyCashInfo) ps.cost else deltaMain.cashFlow
          )).getOrElse(deltaMain)
        })
        deltaWithProductIdSeq <- (TransactionDeltas returning TransactionDeltas.map(td => (td.id, td.product))) ++= deltaWithPackageInfo
        productMeasures <- if (!copyPackageInfo) DBIO.successful(Seq.empty) else ProductMeasures.filter(_.product inSet deltaWithProductIdSeq.flatMap(_._2)).result
        transactionMeasures = for {
          (deltaId, productId) <- deltaWithProductIdSeq.collect {
            case (deltaId, Some(productId)) => (deltaId, productId)
          }
        } yield productMeasures.filter(_.product == productId).map(pm => TransactionMeasuresRow(
          transactionDelta = deltaId,
          amount = pm.amount,
          measure = pm.measure,
          preferredUnit = pm.preferredUnit,
          conversionMeasure = pm.conversionMeasure,
          conversionUnit = pm.conversionUnit,
          conversion = pm.conversion,
          customName = pm.customName,
        ))
        _ <- TransactionMeasures ++= transactionMeasures.flatten
      } yield (transactionId, deltaWithProductIdSeq.map(_._1))
    }

    /**
      * Not (yet?) needed; but if it is needed, here's what the signature should
      * look like, to match other DBActions for other entities
      * @return true if a transaction exists under that ID and user, otherwise false. iff DBIO is successful
      *         and return value is true, then the transaction was modified
      */
//    def modifyTransactionFromAPIEntity(
//        apiEntity: TransactionAPIEntity,
//        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
//        transactionId: Int
//    ): DBIO[Boolean] = ???

    /**
      * @return true if transaction found and deleted, false if not found, failed DBIO if other error
      */
    def deleteTransaction(
        account: AllColumnsQuery[models.Tables.Accounts, AccountsRow],
        transactionId: Int
    ): DBIO[Boolean] = {
      val transaction = account.transactions.filter(_.id === transactionId)

      val orderUsagesQuery = account.orders.filter(_.xact === transactionId)

      for {
        _ <- transaction.deltas.measures.delete
        _ <- transaction.deltas.delete
        numDeleted <- transaction.delete
        _ <- failIf(
          numDeleted > 1,
          new RuntimeException("more than one menu deleted in DBActions.deleteMenu")
        ).toDBIO
      } yield numDeleted == 1
    }
  }
}