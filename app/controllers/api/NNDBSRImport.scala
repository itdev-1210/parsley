package controllers.api

import controllers._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions.HttpCodeException

import play.api.db.slick.DatabaseConfigProvider
import play.api.i18n.MessagesApi
import play.api.libs.json._
import play.api.mvc.ControllerComponents
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.Silhouette

import java.time.Instant
import javax.inject.Inject

import scala.util.Try

class NNDBSRImport @Inject()(
    override val controllerComponents: ControllerComponents,

    override val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider,
    val productsController: controllers.api.Products,
) extends ApiController {
  import dbConfig.profile.api._

  private case class ChangeCounts(inserted: Int, updated: Int) {
    def +(other: ChangeCounts) = ChangeCounts(
      inserted + other.inserted,
      updated + other.updated,
    )
  }

  private final val WEIGHT_MEASURE = 2
  private final val GRAM_UNIT = 2

  private object ChangeCounts {
    val zero = ChangeCounts(0, 0)
  }

  case class NNDBSRNutrientDefition(
    description: String,
    unit: String
  )
  implicit val nndbsrNutrientDefitionFormat = Json.format[NNDBSRNutrientDefition]

  private def updateNutrient(
      nndbsrId: String,
      importInfo: NNDBSRNutrientDefition,
  ): DBIO[ChangeCounts] = {

    val existingNutrientQuery = Nutrients.filter(_.nndbsrId === nndbsrId)

    for {
      maybeNutrient <- existingNutrientQuery.result.headOption
      (numInserted, numUpdated) <- maybeNutrient match {
        case None => {
          (Nutrients ++= Seq(NutrientsRow(
            -1,
            nndbsrId,
            importInfo.description,
            Some(importInfo.description), // parsley_name -- need confirmation
            importInfo.unit,
            active = true // active -- need confirmation
          ))) andThen DBIO.successful((1, 0))
        }
        case Some(existingNutrient) => { // need confimation
          val shouldUpdate = existingNutrient.nndbsrName != importInfo.description || existingNutrient.unit != importInfo.unit
          if (shouldUpdate) {
            existingNutrientQuery.update(existingNutrient.copy(
              nndbsrName = importInfo.description,
              unit = importInfo.unit
            )) andThen DBIO.successful((0, 1))
          } else DBIO.successful((0, 0))
        }
      }
    } yield ChangeCounts(numInserted, numUpdated)
  }

  case class NNDBSRIngredientNutrient(
    code: String,
    value: Double
  )
  implicit val nndbsrIngredientNutrient = Json.format[NNDBSRIngredientNutrient]

  case class NNDBSRMeasure(
      preferred_unit: Int,
      conversion_unit: Int,
      nndb_seq: Int,
      nndb_amount: Double,
      nndb_msre_desc: String,
      nndb_gm_wgt: Double
  )
  implicit val nndbsrMeasureFormat = Json.format[NNDBSRMeasure]

  case class NNDBSRIngredient(
      name: String,
      last_tweaked: Instant,
      measures: Map[String, NNDBSRMeasure],
      nutrients: Seq[NNDBSRIngredientNutrient]
  ) {
    def parsleyIngredient(
      nndbsrId: String,
      gramMeasureRow: ProductMeasuresRow,
      nutrientRows: Seq[ProductNutrientsRow]
    ): ProductAPIEntity = {
      val derivedMeasures: Seq[ProductMeasuresRow] = measures.map { case (measureIdString, nndbsrMeasure) =>
        ProductMeasuresRow(-1, -1,
          // TODO: unsafe. will crash on non-integer-parseable measure id, but this is semi-trusted input
          amount = 1,
          measure = measureIdString.toInt,
          preferredUnit = nndbsrMeasure.preferred_unit,
          conversionMeasure = gramMeasureRow.measure,
          conversionUnit = gramMeasureRow.preferredUnit,
          conversion = nndbsrMeasure.nndb_gm_wgt / nndbsrMeasure.nndb_amount
        )
      }.toSeq
      val allMeasures = gramMeasureRow +: derivedMeasures

      val mainInfo = ProductsRow(-1, owner = None, name = name, ingredient = true,
        salable = false, nndbsrLastImport = Some(last_tweaked), nndbsrId = Some(nndbsrId),
        nutrientServingAmount = Some(100), nutrientServingMeasure = Some(2), nutrientServingUnit = Some(2), // default serving size = 100g
        lastModified = Instant.now()
      )
      val additionalInfo = ProductInfosRow(-1) // using defaults

      ProductAPIEntity(mainInfo, additionalInfo, allMeasures,
        purchaseInfo = None, upstreamInfo = None, recipe = None, simplePreps = Map(),
        nutrientInfo = nutrientRows, allergens = None, characteristics = None
      )
    }
  }

  implicit val nndbsrIngredientRead = Reads {
    case o: JsObject => for {
      toImport <- (o \ "import").validate[Boolean]
    } yield {
      if (toImport) o - "import"
      else JsNull
    }
    case _ => JsError(__, "JsObject required")
  } andThen Reads.optionWithNull(Json.format[NNDBSRIngredient])

  private def updateAllDerivedIngredients(nndbsrId: String,
      importInfo: NNDBSRIngredient,
      nutrientIdMap: Map[String, Int],
  ): DBIO[ChangeCounts] = {
    import productsController.DBActions._

    val gramConversionRow = ProductMeasuresRow(-1, -1,
      WEIGHT_MEASURE, GRAM_UNIT,
      WEIGHT_MEASURE, GRAM_UNIT,
      1
    )
    val nutrients = importInfo.nutrients.map(n =>
      // product id is correctly set later
      ProductNutrientsRow(-1, nutrientIdMap(n.code), n.value)
    )
    val importInfoInParsleyFormat = importInfo.parsleyIngredient(nndbsrId, gramConversionRow, nutrients)

    for {
      dependentProducts <- Products.filter(p => p.owner.isEmpty && p.nndbsrId === nndbsrId)
              .map(_.id).result
      individualUpdateResults <- if (dependentProducts.isEmpty) {
        createProductFromAPIEntity(importInfoInParsleyFormat) andThen
            DBIO.successful(Seq(ChangeCounts(1, 0)))
      } else {
        DBIO.sequence(dependentProducts.map(
          updateSingleIngredient(_, importInfoInParsleyFormat, importInfo.last_tweaked)
        ))
      }
    } yield individualUpdateResults.fold(ChangeCounts.zero)(_ + _)
  }

  private def updateSingleIngredient(productId: Int,
      importedProduct: ProductAPIEntity,
      last_tweaked_time: Instant,
  ): DBIO[ChangeCounts] = {
    import productsController.DBActions._

    for {
      maybeProduct <- fetchProductAPIEntity(productId, Products.filter(_.owner.isEmpty))
      existingProduct <- failIfNone(maybeProduct,
        HttpCodeException(BadRequest, "product found in query stage but then not found in update stage"),
      ).toDBIO
      counts <- {
        // an imported ingredient without a timestamp should be clobbered
        val shouldUpdate = existingProduct.product.nndbsrLastImport
            .forall(_.isBefore(last_tweaked_time))

        if (shouldUpdate) {

          var (mergedProduct, isExpansion, isCorrection) = existingProduct.merge(importedProduct)

          var p = mergedProduct.product

          if (isExpansion || isCorrection) {
            p = p.copy(lastModified = Instant.now)
          }
          // last_tweaked instead of now so that if the nndbsr dump is messed
          // with after export but before import, the next import will pick up
          // the change
          p = p.copy(nndbsrLastImport = Some(last_tweaked_time))

          if (p != mergedProduct.product) {
            mergedProduct = mergedProduct.copy(product = p)
          }

          modifyProductFromAPIEntity(
            mergedProduct,
            Products.filter(_.owner.isEmpty), existingProduct.product.id
          ) andThen DBIO.successful(ChangeCounts(0, 1))
        } else DBIO.successful(ChangeCounts(0, 0))
      }
    } yield counts
  }

  def importIngredients() = StdlibAction.async(
    parse.json(maxLength = 1024 * 1024 * 200) // max length 200MB
  ) { r =>

    db.runInTransaction {
      for {

        // Import nutrients
        nutrientDefinitions <- Try((r.body \ "nutrientDefinitions").as[Map[String, NNDBSRNutrientDefition]]).toDBIO
        nutrientUpdateCounts <- DBIO.sequence(nutrientDefinitions.map { case (nndbsrId, importInfo) =>
          updateNutrient(nndbsrId, importInfo)
        })

        // Used to map from `nndbsrId` to NutrientRows' id
        // We can skip this if we use straight `nndbsrId` as the id of the `nutrients` table?
        nutrients <- Nutrients.result

        // Import ingredients
        ingredients <- Try((r.body \ "ingredients").as[Map[String, Option[NNDBSRIngredient]]]).toDBIO
        toImport = ingredients.collect {
          case (id, Some(importableIngredient)) => (id, importableIngredient)
        }
        ingredientUpdateCounts <- {
          val nutrientIdMap = nutrients.map(n => (n.nndbsrId -> n.id)).toMap
          DBIO.sequence(toImport.map { case (nndbsrId, importInfo) =>
            updateAllDerivedIngredients(nndbsrId, importInfo, nutrientIdMap)
          })
        }
      } yield {
        val ChangeCounts(nutrientInserted, nutrientUpdated) = nutrientUpdateCounts.fold(ChangeCounts.zero)( _ + _)
        val ChangeCounts(ingredientInserted, ingredientUpdated) = ingredientUpdateCounts.fold(ChangeCounts.zero)( _ + _)

        Ok(Json.obj(
          "nutrientInserted" -> nutrientInserted,
          "nutrientUpdated" -> nutrientUpdated,
          "ingredientInserted" -> ingredientInserted,
          "ingredientUpdated" -> ingredientUpdated
        ))
      }
    }
  }
}
