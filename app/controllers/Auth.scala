package controllers

import controllers.api.{ApiController, JsonRestUtils}
import models.JsonConversions.FancyFormat
import models.QueryExtensions._
import models.Tables._
import models._
import models.JsonConversions.invitesAPIFormats._
import util.AsyncUtils._
import util._
import util.Exceptions._

import play.api.db.slick._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json._
import play.api.{Configuration, Logger}
import play.api.http.HttpVerbs
import play.api.mvc._
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.actions.DefaultSecuredErrorHandler
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasherRegistry, PasswordInfo}
import com.mohiva.play.silhouette.impl.exceptions._
import com.mohiva.play.silhouette.impl.providers._

import java.net.URL
import java.time.Instant
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class Auth @Inject()(
    override val controllerComponents: ControllerComponents,
    val silhouette: Silhouette[Env],

    val config: Configuration,

    userIdService: UserIdService,
    val productSync: ProductSync,
    accountSetup: AccountSetup,

    socialProviderRegistry: SocialProviderRegistry,
    credentialsProvider: CredentialsProvider,
    passwordHasherRegistry: PasswordHasherRegistry,
    override val dbConfigProvider: DatabaseConfigProvider,

    val loginView: views.html.Login,
    val emailTokenVerifier: EmailTokenVerifier,
) extends ApiController with
    JsonRestUtils with HasDatabaseConfigProvider[models.Tables.profile.type]
{

  import dbConfig.profile.api._
  import silhouette.env

  val applicationUrl = new URL(config.get[String]("applicationUrl"))

  type StandardProvider = SocialProvider with CommonSocialProfileBuilder
  case class UserState(
    redirect: Option[String],
    phonenumber: Option[String],
    company: Option[String]
  ) extends SocialStateItem
  implicit val redirectUrlFormat = Json.format[UserState]

  def socialAuth(provider: String, redirect: Option[String], phonenumber : Option[String], company: Option[String]) = actionBuilder.async { implicit request =>
    (socialProviderRegistry.get[SocialStateProvider with CommonSocialProfileBuilder](provider) match {
      case Some(p) =>
        for {
          authResult <- p.authenticate(UserState(redirect, phonenumber, company))
          result <- authResult match {
            case Left(authRedirect) => Future.successful(authRedirect)
            case Right(finishedAuth) => p.retrieveProfile(finishedAuth.authInfo)
                .flatMap(commonProfile => {
                  finishSocialAuth(
                    ParsleyUserProfile(
                      commonProfile,
                      phonenumber,
                    ),
                    AccountProfile(
                      company=finishedAuth.userState.company,
                    ),
                    finishedAuth.userState.redirect
                  )
                })
          }
        } yield result
      case None => Future.failed(new ProviderException(
        s"social auth provider $provider is unknown or does not provide basic user information"
      ))
    }).recover {
      case e: AccessDeniedException => Redirect(routes.Application.main(path=""))
      case e: ProviderException =>
        Logger.error("something is wrong in the state of controllers.Auth", e)
        Redirect(routes.Application.main(path=""))
    }
  }

  def embedAccountSelection(baseResult: Result, accountId: Int)(
      implicit request: RequestHeader
  ) = baseResult.withSession(Session(
    Map(AuthUtils.CURRENT_ACCOUNT -> accountId.toString)
  ))

  def embedNewAuthenticator(loginInfo: LoginInfo, baseResult: Result, initialAccount: Option[Int] = None)(
      implicit request: RequestHeader
  ): Future[Result] = for {
    authenticator <- env.authenticatorService.create(loginInfo)
    value <- env.authenticatorService.init(authenticator)
    authedResult <- env.authenticatorService.embed(value, baseResult)
  } yield initialAccount match {
    case Some(accountId) => embedAccountSelection(authedResult, accountId)
    case None => authedResult.withNewSession
  }

  private def safeRedirect(rawRedirect: Option[String]): Call = rawRedirect
      .filter { urlString =>
        val url = new URL(applicationUrl, urlString)
        // don't allow (probably-malicious) attempts to redirect to third party
        // after login completes
        url.getProtocol == applicationUrl.getProtocol &&
            url.getAuthority == applicationUrl.getAuthority
      }
      .map(Call(HttpVerbs.GET, _))
      .getOrElse(routes.Application.main(""))

  def finishSocialAuth(userProfile: ParsleyUserProfile, accountProfile: AccountProfile, redirect: Option[String])
      (implicit request: RequestHeader) = db.runInTransaction {
    for {
      existingUser <- userIdService.retrieveDBIO(userProfile.common.loginInfo)
      u <- existingUser
          .map(DBIO.successful)
          .getOrElse(userIdService.create(userProfile))

      availableAccounts <- if (u.accounts.isEmpty) {
        for {
              newAccount <- accountSetup.create(Some(u.id), accountProfile)
              _ <- productSync.initStdlib(newAccount.id)
        } yield Seq(newAccount)
      } else DBIO.successful(u.accounts)
      _ <- Users.filter(_.id === u.id).userAccounts.account
          .map(_.lastLogin).update(Some(Instant.now))
      initialAccount = if (availableAccounts.length == 1) availableAccounts.headOption else None

      redirectPath = initialAccount match {
        case Some(a) => {
          if (a.acceptsTerms && a.subscriptionLevel.isDefined) safeRedirect(redirect)
          else routes.Plan.main()
        }
        case _ => routes.Auth.selectAccountPage(redirect)
      }
      result <- embedNewAuthenticator(userProfile.common.loginInfo, Redirect(redirectPath), initialAccount.map(_.id)).toDBIO
    } yield result
  }

  val signOutErrorHandler = new DefaultSecuredErrorHandler(messagesApi) {

    override def onNotAuthenticated(implicit request: RequestHeader) = {
      Future.successful(Redirect(routes.Application.main(path=""))) // User has already been logged out
    }

    override def onNotAuthorized(implicit request: RequestHeader) = {
      Future.successful(Redirect(routes.Application.main(path=""))) // User has already been logged out
    }
  }


  def signOut = UserLoggedInAction(signOutErrorHandler).async { implicit request =>
    val result = Redirect(routes.Auth.loginPage(None)).withNewSession
    env.eventBus.publish(LogoutEvent(request.identity, request))
    env.authenticatorService.discard(request.authenticator, result)
  }

  def authView(redirect: Option[String], path: String = "") = UserAwareAction { implicit r =>
    r.identity match {
      case None => Ok(loginView())
      case Some(i) if getAccountFromSession(i).isEmpty => Redirect(routes.Auth.selectAccountPage(redirect))
      case _ => Redirect(safeRedirect(redirect))
    }
  }

  private sealed case class CheckUpstreamInviteResponse(
      mainInfo: UpstreamInvitesRow,
      inviterName: Option[String],
      email: String,
      businessName: Option[String],
  )
  private implicit val checkUpstreamInviteResponseFormat = FancyFormat(Json.format[CheckUpstreamInviteResponse],
    hoistedFields = Seq("mainInfo"),
  )
  def checkUpstreamInvite(id: String, secret: String) = UserAwareApiAction.async { implicit request =>
    db.runInTransaction {
      for {
        invitationId <- Try(id.toInt).toDBIO
        emailToken <- emailTokenVerifier.getValidToken(EmailTokenReference(invitationId, secret))
        _ <- failIf(
          emailToken.tokenType != EmailTokenType.UpstreamInvite,
          HttpCodeException(Unauthorized, ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson)
        ).toDBIO

        maybeInvitation <- UpstreamInvites.filter(_.emailToken === invitationId).result.headOption
        invitation <- failIfNone(maybeInvitation,
          HttpCodeException(InternalServerError, s"no upstream invite for token $invitationId")
        ).toDBIO

        inviterQuery = Accounts.filter(_.id === invitation.inviter)
        inviterAccount <- inviterQuery.result.head // must exist
        inviterUser  <- inviterQuery.owner.result.head // must exist

        maybeInvitee <- Accounts.filter(_.id === invitation.invitee).result.headOption
        invitee <- failIfNone(maybeInvitee,
          HttpCodeException(InternalServerError, s"upstream invite referenced nonexistent user (?) ${invitation.invitee}")
        ).toDBIO
      } yield Ok(Json.toJson(CheckUpstreamInviteResponse(
        invitation,
        inviterName = inviterAccount.businessName.map(_.capitalize.trim).orElse(inviterUser.email),
        email = invitation.inviteeEmail,
        businessName = invitee.upstreamRelativeName,
      )))
    }
  }

  private sealed case class CheckUserInviteResponse(
      mainInfo: UserSharedInvitesRow,
      inviterName: Option[String],
      firstName: Option[String],
      lastName: Option[String],
  )
  private implicit val checkUserInviteResponseFormat = FancyFormat(Json.format[CheckUserInviteResponse],
    hoistedFields = Seq("mainInfo"),
  )
  def checkUserInvite(id: String, secret: String) = UserAwareApiAction.async { implicit request =>
    db.runInTransaction {
      for {
        invitationId <- Try(id.toInt).toDBIO
        emailToken <- emailTokenVerifier.getValidToken(EmailTokenReference(invitationId, secret))
        _ <- failIf(emailToken.tokenType != EmailTokenType.UserSharedInvite, HttpCodeException(Unauthorized,
          ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson
        )).toDBIO
        invitation <- UserSharedInvites.filter(_.emailToken === invitationId).result.head // must exist

        inviterQuery = Accounts.filter(_.id === invitation.inviter)
        inviterAccount <- inviterQuery.result.head // must exist
        inviterUser  <- inviterQuery.owner.result.head // must exist
        inviterName = inviterAccount.businessName.map(_.capitalize.trim).orElse(inviterUser.email.map(_ + "'s account"))

        // for removed user but returning again
        inviteeDetail <- Users.filter(u => u.email === invitation.inviteeEmail).result.headOption
      } yield Ok(Json.toJson(CheckUserInviteResponse(
        invitation,
        inviterName = inviterName,
        firstName = inviteeDetail.flatMap(_.firstName),
        lastName = inviteeDetail.flatMap(_.lastName),
      )))
    }
  }

  def finishUpstreamInvitation(accountOwner: Int, accountProfile: AccountProfile, token: EmailTokensRow): DBIO[AccountsRow] = for {
    mayBeUpstreamInviter <- UpstreamInvites.filter(_.emailToken === token.id).map(_.invitee).result.headOption
    inviteeAccountId <- failIfNone(mayBeUpstreamInviter,
      HttpCodeException(InternalServerError, s"downstream invitee for email token ${token.id} does not exist (?)")
    ).toDBIO

    _ <- accountSetup.update(inviteeAccountId, accountProfile)
    inviteeAccountQuery = Accounts.filter(_.id === inviteeAccountId)
    inviteeAccount <- inviteeAccountQuery.result.head // must exist

    _ <- UserAccounts += UserAccountsRow(0,
      owner = inviteeAccountId, userId = accountOwner,
      joinedAt = Instant.now(), isActive = true,
      permissionLevel = AccountPermissionLevel.Shared,
    )

    _ <- inviteeAccountQuery.map(_.accountOwner).update(Some(accountOwner))

    _ <- productSync.initStdlib(inviteeAccount.id)

  } yield inviteeAccount

  sealed case class RequiredUserCreationInfo(
      firstName: String, lastName: String, phonenumber: String,
  )
  implicit val userCreationFormat = Json.reads[RequiredUserCreationInfo]
  sealed case class RequiredAccountCreationInfo(
      businessName: Option[String],
  )
  implicit val accountCreationFormat = Json.reads[RequiredAccountCreationInfo]
  sealed case class PasswordSetupInfo(
      email: String, password: String,
  )
  implicit val passwordSetupFormat = Json.reads[PasswordSetupInfo]


  case class CompleteUpstreamInviteRequest(
      userInfo: Option[RequiredUserCreationInfo],
      accountInfo: RequiredAccountCreationInfo,
      passwordSetupInfo: Option[PasswordSetupInfo],
      token: EmailTokenReference,
  )

  def completeUpstreamInvite = UserAwareApiAction.async(parse.json) { implicit request =>
    db.runInTransaction {
      for {
        reqBody <- Try(request.body.as(Json.reads[CompleteUpstreamInviteRequest])).toDBIO
        emailToken <- emailTokenVerifier.getValidToken(EmailTokenReference(reqBody.token.id, reqBody.token.secret))
        _ <- failIf(emailToken.tokenType != EmailTokenType.UpstreamInvite, HttpCodeException(Unauthorized,
          ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson
        )).toDBIO

        (downstreamAccountOwner, loginInfo) <- getOrCreateAccountForInvitation(request, reqBody.userInfo, reqBody.passwordSetupInfo)
        downstreamAccount <- finishUpstreamInvitation(
          downstreamAccountOwner,
          AccountProfile(reqBody.accountInfo.businessName),
          emailToken,
        )

        result <- embedNewAuthenticator(loginInfo, Ok(JsString("Upstream Joined")), Some(downstreamAccount.id)).toDBIO
        _ <- EmailTokens.filter(_.id === reqBody.token.id).map(_.used).update(true)
      } yield result
    }
  }

  def finishUserInvitation(userId: Int, token: EmailTokensRow): DBIO[AccountsRow] = for {
    invitation <- UserSharedInvites.filter(_.emailToken === token.id).result.head // must exist
    inviter <- Accounts.filter(_.id === invitation.inviter).result.head // must exist

    userAccountQuery = UserAccounts.filter(acc => acc.userId === userId && acc.owner === inviter.id)
    hasJoinedInviterAlready <- userAccountQuery.exists.result
    _ <- if (hasJoinedInviterAlready) { // user was removed and then re-added
      userAccountQuery.map(_.isActive).update(true)
    } else {
      UserAccounts += UserAccountsRow(
        0, inviter.id, userId,
        isActive = true, joinedAt = Instant.now(), permissionLevel = invitation.inviteLevel
      )
    }
  } yield inviter

  case class CompleteUserInviteRequest(
      userInfo: Option[RequiredUserCreationInfo],
      passwordSetupInfo: Option[PasswordSetupInfo],
      token: EmailTokenReference,
  )
  def completeInvite = UserAwareApiAction.async(parse.json) { implicit request =>
    db.runInTransaction {
      for {
        reqBody <- Try(request.body.as(Json.reads[CompleteUserInviteRequest])).toDBIO
        emailToken <- emailTokenVerifier.getValidToken(EmailTokenReference(reqBody.token.id, reqBody.token.secret))
        _ <- failIf(emailToken.tokenType != EmailTokenType.UserSharedInvite, HttpCodeException(Unauthorized,
          ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson
        )).toDBIO

        (userId, loginInfo) <- getOrCreateAccountForInvitation(request, reqBody.userInfo, reqBody.passwordSetupInfo)
        inviterAccount <- finishUserInvitation(userId, emailToken)

        result <- embedNewAuthenticator(loginInfo, Ok(JsString("user joined")), Some(inviterAccount.id)).toDBIO
        _ <- EmailTokens.filter(_.id === reqBody.token.id).map(_.used).update(true)
      } yield result
    }
  }

  private def getOrCreateAccountForInvitation(
      request: UserAwareRequest[_],
      maybeUserInfo: Option[RequiredUserCreationInfo], maybePasswordSetupInfo: Option[PasswordSetupInfo],
  ) = (request.identity, request.authenticator, maybeUserInfo, maybePasswordSetupInfo) match {

    case (Some(identity), Some(authenticator), _, _) => DBIO.successful(
      (identity.id, authenticator.loginInfo)
    )

    case (_, _, Some(userInfo), Some(passwordSetupInfo)) => {
      val emailAddress = passwordSetupInfo.email.toLowerCase
      for {
        loginInfo <- credentialsProvider.loginInfo(Credentials(emailAddress, passwordSetupInfo.password)).toDBIO

        inviteeLoginExists <- userIdService.retrieveDBIO(loginInfo).map(_.isDefined)
        userWithEmailAddressExists <- Users.filter(_.email === emailAddress).exists.result
        _ <- failIf(inviteeLoginExists || userWithEmailAddressExists,
          HttpCodeException(
            BadRequest,
            ReadableError("_error" -> s"A Parsley account with your email already exists. Please cancel the account, or sign up with a different e-mail address.").toJson
          )
        ).toDBIO

        user <- userIdService.create(
          ParsleyUserProfile(
            CommonSocialProfile(
              loginInfo,
              email = Some(emailAddress),
              firstName = Some(userInfo.firstName), lastName = Some(userInfo.lastName),
            ),
            Some(userInfo.phonenumber),
          ),
          Some(passwordHasherRegistry.current.hash(passwordSetupInfo.password)),
        )
      } yield (user.id, loginInfo)
    }

    case _ => DBIO.failed(HttpCodeException(BadRequest, "user not logged in and no user creation info supplied"))
  }

  // separate methods for the benefit of reverse routing
  def loginPage(redirect: Option[String]) = authView(redirect)
  def signUpPage(redirect: Option[String]) = authView(redirect)
  def selectAccountPage(redirect: Option[String]) = UserLoggedInAction { implicit r =>
    Ok(loginView())
  }
  def completeInvitationPage(id: String, secret: String) = Action(implicit r => Ok(loginView()))

  def passwordResetPage(tokenID: String, tokenSecret: String) = UserAwareApiAction.async { implicit r =>
    val result = Ok(loginView())

    r.identity.foreach(LogoutEvent(_, r))
    r.authenticator.map { auth =>
      env.authenticatorService.discard(auth, result)
    }.getOrElse(Future.successful(result))
  }

  case class UserLoginRequest(
    email: String,
    password: String
  )
  def passwordAuth() = ApiAction.async(parse.json) { implicit r =>
    {
      for {
        UserLoginRequest(rawEmail, password) <- Future.fromTry(Try(r.body.as(Json.reads[UserLoginRequest])))
        email = rawEmail.toLowerCase


        credentials = Credentials(email, password)

        loginInfo <- credentialsProvider.authenticate(credentials)
        result <- embedNewAuthenticator(loginInfo, Ok(JsString("logged in")))
      } yield result
    }.recover {
      case e: InvalidPasswordException =>
        BadRequest(ReadableError("password" -> Messages("auth.passwordauth.badpassword")).toJson)
      case e: IdentityNotFoundException =>
        // email from above is out of scope, but if we got this error we can be assured that the json parsed
        // correctly
        val email = r.body.as(Json.reads[UserLoginRequest]).email.toLowerCase
        BadRequest(ReadableError("email" -> Messages("auth.passwordauth.bademail", email)).toJson)
    }
  }

  case class UserCreationRequest(
    firstName: Option[String],
    lastName: Option[String],
    company: Option[String],
    phonenumber: Option[String],
    email: String,
    password: String
  )
  def passwordUserCreation() = ApiAction.async(parse.json) { implicit r =>
    db.runInTransaction {
      for {
        UserCreationRequest(firstName, lastName, company, phonenumber, rawEmail, password) <-
          Try(r.body.as(Json.reads[UserCreationRequest])).toDBIO
        email = rawEmail.toLowerCase

        passwordInfo = passwordHasherRegistry.current.hash(password)

        loginInfo <- credentialsProvider.loginInfo(Credentials(email, password)).toDBIO

        emailAsUsernameExists <- UserLogins.byLoginInfo(loginInfo).exists.result
        _ <- failIf(emailAsUsernameExists,
          HttpCodeException(BadRequest,
            ReadableError("email" -> Messages("auth.usercreation.email.exists", email)).toJson)
        ).toDBIO

        profile = ParsleyUserProfile(
          CommonSocialProfile(
            loginInfo,
            email = Some(email),
            firstName = firstName, lastName = lastName,
          ),
          phoneNumber = phonenumber,
        )
        mayBeReturningUser <- Users.filter(_.email === email).result.headOption
        userId <- mayBeReturningUser.map { returningUser =>
          userIdService.update(returningUser.id, profile, Some(passwordInfo))
              .map(_ => returningUser.id)
        }.getOrElse {
          userIdService.create(profile, Some(passwordInfo)).map(_.id)
        }

        initialAvailableAccounts <- Users.filter(_.id === userId).userAccounts.account.result
        availableAccounts <- if (initialAvailableAccounts.isEmpty) {
          for {
            newAccount <- accountSetup.create(Some(userId), AccountProfile(company=company))
            _ <- productSync.initStdlib(newAccount.id)
          } yield Seq(newAccount)
        } else DBIO.successful(initialAvailableAccounts)
        initialAccount = if (availableAccounts.length == 1) availableAccounts.headOption else None

        result <- embedNewAuthenticator(loginInfo, Ok(JsString("user created")), initialAccount.map(_.id)).toDBIO
      } yield result
    }
  }

  def selectAccount(requestedAccount: Int) = UserLoggedInApiAction { implicit r =>
    if (r.identity.accounts.exists(_.id == requestedAccount)) {
      embedAccountSelection(Ok(JsNumber(requestedAccount)), requestedAccount)
    } else {
      Forbidden(JsString(s"user ${r.identity.dbRow.id} is not a member of account $requestedAccount"))
    }
  }
}
