package controllers

import models.Tables._
import models._
import util.Exceptions.HttpCodeException

import play.api.db.slick.HasDatabaseConfigProvider
import play.api.mvc._
import play.api.mvc.Results._

import com.mohiva.play.silhouette.api.{Silhouette, Env => BaseEnv}
import com.mohiva.play.silhouette.api.actions.{SecuredErrorHandler, SecuredActionBuilder => SilhouetteSecuredActionBuilder, SecuredRequest => SilhouetteSecuredRequest, UserAwareRequest => SilhouetteUserAwareRequest}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

// Auth stuff
trait Env extends BaseEnv {
  type I = UserModel
  type A = CookieAuthenticator
}

object AuthUtils {
  final val CURRENT_ACCOUNT = "current_account"
}

trait AuthUtils {
  self: HasDatabaseConfigProvider[models.Tables.profile.type] =>

  import dbConfig.profile.api._

  val silhouette: Silhouette[Env]
  implicit def ec: ExecutionContext

  val UserLoggedInAction = silhouette.SecuredAction
  type UserLoggedInRequest[A] = SilhouetteSecuredRequest[Env, A]

  class SecuredRequest[A](
      identity: Env#I, authenticator: Env#A, request: Request[A],
      val userAccount: UserAccountsRow, val account: AccountsRow,
  ) extends UserLoggedInRequest[A](identity, authenticator, request) {

    def this(
        baseRequest: UserLoggedInRequest[A],
        userAccount: UserAccountsRow, account: AccountsRow,
    ) = this(baseRequest.identity, baseRequest.authenticator, baseRequest.request, userAccount, account)

    def isAccountOwner: Boolean = account.accountOwner.contains(identity.dbRow.id)

    def accountId = account.id
    def accountQuery = Accounts.filter(_.id === accountId)

    // for filling in database rows. NOT for use in querying, because SQL thinks
    // NULL is not equal to NULL
    def productWriteableOwnerId: Option[Int] =
      if (account.isStdlib) None
      else Some(accountId)
  }

  def getAccountFromSession(u: UserModel)(implicit r: RequestHeader): Option[(UserAccountsRow, AccountsRow)] = {
    val rawAccountId = r.session.get(AuthUtils.CURRENT_ACCOUNT)
    var maybeAccountId = Try(rawAccountId.map(_.toInt)) match {
      case Success(id) => id
      case Failure(_) => throw new HttpCodeException(BadRequest, "invalid account ID in " + AuthUtils.CURRENT_ACCOUNT)
    }

    if (u.userAccounts.length == 1) {
      maybeAccountId = maybeAccountId.orElse(u.userAccounts.headOption.map(_.owner))
    }

    for {
      accountId <- maybeAccountId
      userAccount <- u.userAccounts.find(_.owner == accountId)
      account <- u.accounts.find(_.id == accountId)
    } yield (userAccount, account)
  }

  private val accountSelectedChecker = new ActionFunction[UserLoggedInRequest, SecuredRequest] {
    override def invokeBlock[A](request: UserLoggedInRequest[A], block: SecuredRequest[A] => Future[Result]) = {
      val maybeSecuredRequest = getAccountFromSession(request.identity)(request).map { case (userAccount, account) =>
        new SecuredRequest(request, userAccount, account)
      }

      maybeSecuredRequest match {
        case Some(securedRequest) => block(securedRequest)
        case None => Future.successful(Unauthorized("select_account"))
      }
    }

    override protected val executionContext = ec
  }

  def SecuredAction(errorHandler: SecuredErrorHandler) = UserLoggedInAction(errorHandler) andThen accountSelectedChecker
  def SecuredAction = UserLoggedInAction andThen accountSelectedChecker

  val UserAwareAction = silhouette.UserAwareAction
  type UserAwareRequest[A] = SilhouetteUserAwareRequest[Env, A]


  implicit class UserLoggedInRequesetParsleyDBExtensions[A](
      r: UserLoggedInRequest[A]
  ) {
    def authedUserId = r.identity.dbRow.id
    def authedUserQuery = Users.filter(_.id === authedUserId)
  }

  def accountFilter(f: AccountsRow => Boolean) = new ActionFilter[SecuredRequest] {
    override def filter[A](request: SecuredRequest[A]) = Future.successful {
      if (f(request.account)) None
      else Some(Forbidden("For Parsley internal use only"))
    }

    override val executionContext = ec
  }

  def userPermFilter(allowedPermLevels: Set[AccountPermissionLevel.Value]) = new ActionFilter[SecuredRequest] {
    override def filter[A](request: SecuredRequest[A]) = Future.successful {
      if (request.isAccountOwner) None // owner
      else if (allowedPermLevels.contains(request.userAccount.permissionLevel)) None
      else Some(Forbidden("not allowed"))
    }

    override val executionContext = ec
  }
}
