package controllers

import models._
import models.Tables._
import models.QueryExtensions._
import util.AsyncUtils._

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc._

import com.google.inject.Inject
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.actions.DefaultSecuredErrorHandler

import java.time.Instant

import scala.concurrent._

class Application @Inject() (
    override val controllerComponents: ControllerComponents,
    val silhouette: Silhouette[Env],
    override val dbConfigProvider: DatabaseConfigProvider,

    val plansController: Plan,
    val applicationView: views.html.Application,
) extends BaseController with HasDatabaseConfigProvider[models.Tables.profile.type] with AuthUtils {

  implicit def ec = defaultExecutionContext

  def loginTest = SecuredAction { r: SecuredRequest[AnyContent] =>
    val li = r.authenticator.loginInfo
    Ok(Json.toJson(li)(Json.writes[LoginInfo]))
  }

  // path is ignored; it's consumed by the browser-side parts of the application
  def main(path: String) = UserAwareAction.async { implicit request: plansController.UserAwareRequest[AnyContent] =>
    request.identity match {
      case None => Future.successful(Redirect(routes.Auth.signUpPage(Some(request.uri))))
      case Some(i) => {
        getAccountFromSession(i) match {
          case None => Future.successful(Redirect(routes.Auth.signUpPage(Some(request.uri)))) // TODO: routes.Auth.selectAccount
          case Some((userAccount, account)) => {
            val securedRequest = new plansController.SecuredRequest(
              // request.identity.isDefined == request.authenticator.isDefined
              i, request.authenticator.get, request.request,
              userAccount, account
            )
            plansController.subscriptionCheckEndpoint(subscribedResponse = Ok(applicationView()))(securedRequest)
          }
        }
      }
    }
  }
}
