package util

import AsyncUtils._
import controllers.api.JsonRestUtils.ReadableError
import util.Exceptions.HttpCodeException

import play.api.Configuration

import com.sendgrid._
import play.api.Logger
import play.api.http.Status
import play.api.mvc.Results._

import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future, blocking}

class EmailUtils @Inject() (
    implicit val ec: ExecutionContext,
    val config: Configuration
)
{
  val sendGridKey = config.getOptional[String]("sendgrid.key")

  def send(
      // core params
      to: Email, from: Email, subject: String, content: String,
      // optional stuff
      cc: Seq[Email] = List.empty,
      bcc: Seq[Email] = List.empty
  ): Future[_] = {
    val mail = new Mail()
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.addContent(new Content("text/html", content))

    val personalization = new Personalization()
    personalization.addTo(to)
    cc.filter(contact => contact.getEmail() != to.getEmail()).foreach(personalization.addCc)
    bcc.filter(contact => contact.getEmail() != to.getEmail()).foreach(personalization.addBcc)

    mail.addPersonalization(personalization)

    sendGridKey match {
      case Some(key) => {
        val sg = new SendGrid(key)
        val request = new Request()
        request.setMethod(Method.POST)
        request.setEndpoint("mail/send")
        request.setBody(mail.build())
        for {
          response <- Future(blocking(sg.api(request)))

          _ <- failIf(
            !Status.isSuccessful(response.getStatusCode),
            HttpCodeException(InternalServerError, "we encountered an error sending your e-mail")
          ).toFuture
        } yield Unit
      }
      case None => {
        Future(Logger.debug(mail.buildPretty))
      }
    }
  }
}
