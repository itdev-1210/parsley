package util

import models.QueryExtensions._
import models.Tables._

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO

import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class ParsleyPasswordInfoDAO @Inject()(
    implicit val ec: ExecutionContext,
    override val dbConfigProvider: DatabaseConfigProvider
) extends
    DelegableAuthInfoDAO[PasswordInfo] with
    HasDatabaseConfigProvider[models.Tables.profile.type]
{
  import dbConfig.profile.api._

  def passwordInfoQuery(loginInfo: LoginInfo) = UserLogins.byLoginInfo(loginInfo).user
      .map(u => (u.hasher, u.hashedPassword, u.salt))

  override def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = {
    db.run {
      passwordInfoQuery(loginInfo).result.headOption
    }.map { maybeResult =>
      for {
        (maybeHasher, maybePassword, maybeSalt) <- maybeResult
        hasher <- maybeHasher
        password <- maybePassword
      } yield PasswordInfo(hasher, password, maybeSalt)
    }
  }

  override def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    db.run {
      passwordInfoQuery(loginInfo).update(
        (Some(authInfo.hasher), Some(authInfo.password), authInfo.salt)
      )
    }.map(_ => authInfo)
  }

  // The following functions aren't used anywhere in the Silhouette code, and their semantics are very
  // ill-defined. Leaving them as ??? instead of a dummy implementation because I want a loud failure
  // if these do turn out to be necessary
  override def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = ???
  override def remove(loginInfo: LoginInfo): Future[Unit] = ???
  override def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = ???
}
