package util

import models._

import javax.inject.Inject

class ModelUtils @Inject() () {

  def getUsername(userRow: UsersRow) = userRow.fullName getOrElse ((userRow.firstName getOrElse "") + " " + (userRow.lastName getOrElse ""))

}