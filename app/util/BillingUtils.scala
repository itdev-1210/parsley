package util

import play.api.Configuration

import com.stripe.Stripe

import javax.inject.Inject

// add DI dependency on this to make sure that Stripe config is initialized
// before using Stripe API methods
class BillingUtils @Inject() (
    val config: Configuration,
)
{
  Stripe.apiKey = config.get[String]("stripe.secretKey")
  Stripe.apiVersion = "2018-09-06"
}
