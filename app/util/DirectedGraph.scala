package util

/**
  *
  * @tparam A - the type that mixes this in. e.g. class Node extends DirectedGraph[Node].
  */
trait DirectedGraph[A] {
  type GraphNode = A

  def edgesFrom(n: GraphNode): Iterable[GraphNode]

  /**
    * does not construct a new graph; just traverses
    *
    * @param starters the set of nodes from which to start traversal. for the traversal to include the
    *                 whole graph, must be a superset of the set of nodes with no incoming edges; this could
    *                 be the whole graph, if that's easy to do
    * @param preVisit to be executed before visiting children. returning false will cause early return
    * @param postVisit to be executed after visiting children. returning false will cause early return
    * @param allowNonForest the value to return if the same node is reached twice during traversal
    * @return true iff every preVisit and postVisit call returned true
    */
  def depthFirstSearch(starters: Iterable[GraphNode],
      preVisit: (GraphNode => Boolean) = (_ => true),
      postVisit: (GraphNode => Boolean) = (_ => true),
      allowNonForest: Boolean=true
  ): Boolean = {
    var visited = Set[GraphNode]()

    def visit(current: GraphNode): Boolean = {
      // even if cycles allowed, don't call {pre,post}Visit twice for same node
      if (visited.contains(current)) return allowNonForest
      visited += current

      if (!preVisit(current)) return false
      if (!this.edgesFrom(current).forall(visit)) return false
      postVisit(current)
    }
    starters.forall(visit)
  }

  /**
    * once you know this and that the number of edges is one less than the number of nodes, you know you
    * have a tree. yay!
    *
    * @param root the node "downstream" of which to check for multiple paths
    */
  def noMultipath(root: GraphNode) = this.depthFirstSearch(Some(root), allowNonForest=false)

  /**
    *
    * @return the nodes in topological sort order, or None if that is impossible (i.e. this is not a DAG)
    */
  def topologicalSort(starters: Iterable[GraphNode]): Option[Iterable[GraphNode]] = {
    // using the DFS topoSort algorithm as shown at https://en.wikipedia.org/wiki/Topological_sorting on
    // 1 March 2016
    var currentlyVisiting = Set[GraphNode]()
    var output = List[GraphNode]()

    def enterSubgraph(n: GraphNode) = {
      if (currentlyVisiting(n)) false // not a DAG! reached n through n
      else {
        currentlyVisiting += n
        true
      }
    }
    def finalizeSubgraph(n: GraphNode) = {
      currentlyVisiting -= n
      output = n :: output
      true
    }

    if (this.depthFirstSearch(starters, preVisit=enterSubgraph, postVisit=finalizeSubgraph, allowNonForest=true))
      Some(output)
    else
      None
  }
}

object DirectedGraph {
  def apply[A](getEdgesFrom: A => Iterable[A]) = new DirectedGraph[A] {
    def edgesFrom(n: A) = getEdgesFrom(n)
  }
}
