package util

import play.api.Configuration

object ParsleyEnvironment {
  // values should be kept in sync with those in assets/app/common/utils/constants.jsx
  val DEV = "dev"
  val PROD = "prod"
  val TEST = "test"
  val STAGING = "staging"

  def getFrom(config: Configuration): String = config.getAndValidate[String](
    "parsley.env", Set(PROD, STAGING, TEST, DEV)
  )
}
