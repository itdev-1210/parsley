package util

import slick.dbio._
import util.AsyncUtils._
import util.Exceptions.{HttpCodeException}

import play.api.{Configuration}
import play.api.mvc.{MultipartFormData}
import play.api.mvc.Results.{BadRequest}
import play.api.libs.Files.{TemporaryFile}
import play.api.libs.ws.{WSClient}
import play.api.libs.json.{JsString}
import play.api.Logger

import fly.play.s3.{S3, PUBLIC_READ, BucketFile, S3Exception}
import com.sksamuel.scrimage.Image

import javax.inject.Inject
import java.nio.file.{Files, Paths}
import scala.concurrent.{ExecutionContext, Future}

class FileUtils @Inject()(
  implicit val ec: ExecutionContext,
  val config: Configuration,
  val ws: WSClient,
) {

  implicit val s3 = S3.fromConfiguration(ws, config)
  implicit val bucket = s3.getBucket(config.get[String]("s3.bucket"))

  def uploadFile(file : BucketFile) : Future[Unit] = {
    bucket.add(file).recoverWith { case e: S3Exception =>
      Logger.error("error putting image to S3", e)
      Future.failed(HttpCodeException(BadRequest, JsString("Error: " +
          e.message)))
    }
  }

  def removeFile(filename : String) : Future[Unit] = {
    bucket.remove(filename).recoverWith { case e: S3Exception =>
      Logger.error("error removing image from S3 -- " ++ filename, e)
      // It's generally fine to fail here, the file upload actions
      // that often happen together with this are more important
      Future.successful(Unit)
    }
  }

  def uploadUserImage(
    userId: Int,
    file: MultipartFormData.FilePart[TemporaryFile],
    maxWidth: Int = 500,
    maxHeight: Int = 500,
  ) : Future[String] = {

    val uuid = java.util.UUID.randomUUID.toString
    val filename = s"$userId/$uuid"

    // get the image
    val byteArray = Files.readAllBytes(Paths.get(file.ref.path.toUri))
    var image = Image(byteArray)

    // resize with max width and max height
    val ratio = image.width.toDouble / image.height

    if (image.width > maxWidth) image = image.scaleTo(maxWidth, (maxWidth.toDouble / ratio).toInt)
    if (image.height > maxHeight) image = image.scaleTo((maxHeight.toDouble * ratio).toInt, maxHeight)

    uploadFile(BucketFile(filename,
      file.contentType.getOrElse("text/plain"),
      content = image.bytes,
      acl = Some(PUBLIC_READ)
    )).map(_ => filename)
  }
}
