package util

import models.QueryExtensions._
import models.SuppliersRow
import models.Tables._

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import java.time.Instant
import javax.inject.Inject

class SupplierSync @Inject() (
    override val dbConfigProvider: DatabaseConfigProvider,
    implicit val ec: scala.concurrent.ExecutionContext,
    val productSync: ProductSync,
    val suppliersController: controllers.api.Suppliers,
) extends HasDatabaseConfigProvider[models.Tables.profile.type] {

  import dbConfig.profile.api._

  private def copySuppliers(downstreamAccountId: Int, upstreamSuppliers: AllColumnsQuery[Suppliers, SuppliersRow]) = {
    val downstreamSuppliers = Suppliers.filter(_.owner === downstreamAccountId)
    for {
      copiedInfo <- (Suppliers.map { s => (
          // insertion format
          s.owner, s.syncUpstreamOwner, s.syncUpstream,
          s.name,
          s.comments, s.instructions,
          s.contactName, s.accountId, s.phone, s.email,
          s.lastSynced,
      )} returning Suppliers.map { s =>
        (s.id, s.syncUpstream)
      }).forceInsertQuery(upstreamSuppliers.map { s => (
          downstreamAccountId, Rep.Some(s.owner), Rep.Some(s.id),
          s.name,
          s.comments, s.instructions,
          s.contactName, s.accountId, s.phone, s.email,
          Rep.Some(Instant.now()),
      )})
      _ <- DBIO.sequence(copiedInfo.map { case (downstream, upstream) =>
          SupplierContacts.map { c => (
              c.supplierId,
              c.name, c.phone, c.email,
              c.comments, c.displayOrder,
          )}.forceInsertQuery(SupplierContacts.filter(_.supplierId === upstream).map { c => (
              downstream,
              c.name, c.phone, c.email,
              c.comments, c.displayOrder,
          )})
      })
      // post-copy cleanup - find any duplicate names we've inserted
      duplicateNames <- (
          downstreamSuppliers.filter(_.id.inSet(copiedInfo.map(_._1)))
              join
              downstreamSuppliers.filter(!_.id.inSet(copiedInfo.map(_._1)))
              on
              (_.name.toLowerCase === _.name.toLowerCase)
          ).map(_._1).map(newSupp => (newSupp.id, newSupp.name)).result
      _ <- DBIO.sequence(duplicateNames.map { case (id, name) =>
        downstreamSuppliers.filter(_.id === id).map(_.name)
            // TODO: allow customization of this message (also ProductSync)
            .update(name.map(_ + " (from Parsley supplier library)"))
      })
    } yield Unit
  }

  val overwrittenProductSourceFields = Set(
    "measure", "unit",
    "packaged", "package_name", "package_size", "super_package_size", "sub_package_name",
    "sku",
    "price_per", "pricing_unit",
    "supplier_ingredient_name",
  )
  /**
    * Update/create/unlink product_sources rows
    */
  private def updateSources(downstreamAccountId: Int, upstreamSuppliers: AllColumnsQuery[Suppliers, SuppliersRow]) = {
    for {
      // ugly, but need this to sub into plain sql queries
      upstreamSupplierIds <- upstreamSuppliers.map(_.id).result
      upstreamSuppliersSqlList = if (upstreamSupplierIds.isEmpty) {
        "NULL"
      } else {
        upstreamSupplierIds.mkString(",")
      }
      downstreamSuppliers = Suppliers.filter(_.owner === downstreamAccountId)

      // disconnect product sources from upstreams that have changed product -
      // act as if they've been deleted and new ones created (and then sync the
      // new ones)
      _ <- sqlu"""
        UPDATE product_sources downstream
        SET sync_upstream = NULL
        FROM product_sources upstream, products downstream_prod
        WHERE upstream.supplier IN (#$upstreamSuppliersSqlList)
          AND downstream.sync_upstream = upstream.id AND downstream_prod.id = downstream.product
          AND downstream_prod.sync_upstream != upstream.product
      """

      upstreamsResynced <- sql"""
       UPDATE product_sources downstream
       SET #${overwrittenProductSourceFields.map(f => s"$f = upstream.$f").mkString(",")}
       FROM product_sources upstream, suppliers upstream_supplier, suppliers downstream_supplier
       WHERE upstream.id = downstream.sync_upstream AND downstream.supplier = downstream_supplier.id
         AND upstream.supplier = upstream_supplier.id AND downstream_supplier.owner = $downstreamAccountId
       RETURNING upstream.id
      """.as[Int]

      copyEligibleSources = upstreamSuppliers.sourcedProducts.filter(!_.tombstone)
      downstreamProducts = Products.filter(_.owner === downstreamAccountId)
      _ <- ProductSources.map { s => (
          s.product, s.supplier,
          s.measure, s.unit,
          s.packaged, s.packageName, s.packageSize, s.superPackageSize, s.subPackageName,
          s.cost, s.pricePer, s.pricingMeasure, s.pricingUnit,
          s.supplierIngredientName,
          s.syncUpstream, s.syncUpstreamOwner,
          s.manufacturerName, s.manufacturerNumber,
      )}.forceInsertQuery {
        val ooo =
        (
            copyEligibleSources
                join copyEligibleSources.products on (_.product === _.id)
                join downstreamProducts on (_._2.id === _.syncUpstream) // _._2.id is upstream product id
                join downstreamSuppliers on (_._1._1.supplier === _.syncUpstream) // _._1._1.supplier is upstream source's supplier ID

        // only copy product_sources rows that *didn't* have a downstream
        ).filter { case (((upstreamSource, upstreamProduct), downstreamProduct), downstreamSupplier) =>
          !(upstreamSource.id inSet upstreamsResynced)

        // sortBy + distinctOn picks, for each source to copy, the downstream product created first (lowest ID)
        }.sortBy { case (((upstreamSource, upstreamProduct), downstreamProduct), downstreamSupplier) =>
          // distinctOn columns must be a prefix of sortBy columns, so add upstreamSource.id here
          (upstreamSource.id, downstreamProduct.id)
        }.distinctOn { case (((upstreamSource, upstreamProduct), downstreamProduct), downstreamSupplier) =>
          upstreamSource.id

        }

        ooo.map { case (((upstreamSource, upstreamProduct), downstreamProduct), downstreamSupplier) => (
          // compare to _ <- ProductSources.map { s => foo } line
          downstreamProduct.id, Rep.Some(downstreamSupplier.id),
          upstreamSource.measure, upstreamSource.unit,
          upstreamSource.packaged, upstreamSource.packageName, upstreamSource.packageSize, upstreamSource.superPackageSize, upstreamSource.subPackageName,
          upstreamSource.cost, upstreamSource.pricePer, upstreamSource.pricingMeasure, upstreamSource.pricingUnit,
          upstreamSource.supplierIngredientName,
          Rep.Some(upstreamSource.id), upstreamProduct.owner,
          upstreamSource.manufacturerName, upstreamSource.manufacturerNumber,
        )}
      }
    } yield Unit

  }

  private def updateSupplier(
      downstreamAccountId: Int, downstreamSupplierId: Int, upstreamSupplierId: Int,
  // the name is the only thing that we override (it's non-writeable by downstream)
  ): DBIO[_] = sqlu"""
    UPDATE suppliers downstream
    SET name = upstream.name,
      last_synced = (CASE WHEN downstream.name = upstream.name
          THEN downstream.last_synced
          ELSE now()
    END)
    FROM suppliers upstream
    WHERE downstream.id = ${downstreamSupplierId}
      AND upstream.id = ${upstreamSupplierId};
  """

  def directProductDependencies(upstreamSuppliers: AllColumnsQuery[Suppliers, SuppliersRow]) =
    upstreamSuppliers.sourcedProducts.filter(!_.tombstone).products

  def sync(
      downstreamAccountId: Int,
      supplierWasSynced: Suppliers => Rep[Boolean],
      upstreamSuppliers: AllColumnsQuery[Suppliers, SuppliersRow],
  ): DBIO[_] = {
    // TODO
    val downstreamSuppliers = Suppliers.filter(_.owner === downstreamAccountId)
    val toCopy = upstreamSuppliers.filter { upstreamSupp =>
      !downstreamSuppliers.filter(_.syncUpstream === upstreamSupp.id).exists
    }
    for {

      /*
       * main info (not sources)
       */
      // ordering here is important. want to assemble list of actions (upstreams
      // to sync and to create) based on initial DB state, so before any writes
      // have happened). Run toModify query up front...
      toModify <- {
        downstreamSuppliers join upstreamSuppliers on (_.syncUpstream === _.id)
      }.map { case (downstreamSupp, upstreamSupp) =>
        downstreamSupp.id -> upstreamSupp.id
      }.result

      // ...and run copy (which takes a Query) first
      _ <- copySuppliers(downstreamAccountId, toCopy)
      _ <- DBIO.sequence(toModify.map(s => updateSupplier(downstreamAccountId, s._1, s._2)))

      _ <- updateSources(downstreamAccountId, upstreamSuppliers)
    } yield Unit
  }

}
