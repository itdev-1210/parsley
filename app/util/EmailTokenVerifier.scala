package util

import controllers.api.JsonRestUtils.ReadableError
import models.Tables._
import models._
import util.AsyncUtils._
import util.Exceptions._

import akka.actor.ActorSystem
import akka.pattern.Patterns.after
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.i18n.{I18nSupport, Messages, MessagesApi, MessagesProvider}
import play.api.libs.crypto.CSRFTokenSigner
import play.api.libs.json.Json
import play.api.mvc.Results._

import org.apache.commons.codec.binary.Hex

import java.security.{SecureRandom, MessageDigest}
import java.time.{Duration, Instant}
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.{MILLISECONDS, FiniteDuration => ScalaDuration}

case class EmailTokenReference(id: Int, secret: String)
object EmailTokenReference {
  implicit val jsonFormat = Json.format[EmailTokenReference]
}

class EmailTokenVerifier @Inject()(
    override val dbConfigProvider: DatabaseConfigProvider,
    implicit val ec: ExecutionContext,
    override val messagesApi: MessagesApi,
    val actorSystem: ActorSystem,
    val tokenSigner: CSRFTokenSigner,
    implicit val messagesProvider: MessagesProvider
) extends HasDatabaseConfigProvider[models.Tables.profile.type] with I18nSupport {
  import dbConfig.profile.api._

  // randomization code is the same as DefaultCSRFTokenSigner, but without a bit
  // of hackiness they use for compatibility on old Windows versions
  private val random = new SecureRandom()
  private val TOKEN_LENGTH = 32

  private val RESET_TOKEN_VALIDITY = Duration.ofMinutes(20)
  private val SHARED_INVITE_TOKEN_VALIDITY = Duration.ofDays(14)
  // TODO: delete accounts that have not been set up as of one year
  private val SIGNUP_TOKEN_VALIDITY = Duration.ofDays(365)
  // Upstream invite sets up account details and could not invite again
  // so set duration of a year for now.
  // TODO: delete accounts that have upstream invite but no login activity
  // i.e. user.last_login is null
  private val UPSTREAM_INVITE_TOKEN_VALIDITY = Duration.ofDays(365)

  def createToken(tokenType: EmailTokenType.Value, owner: Int): DBIO[EmailTokenReference] = {
    val bytes = new Array[Byte](TOKEN_LENGTH)
    random.nextBytes(bytes)
    val secret = new String(Hex.encodeHex(bytes))
    val expiration = Instant.now.plus(
      tokenType match {
        case EmailTokenType.EmailConfirm => SIGNUP_TOKEN_VALIDITY
        case EmailTokenType.PasswordReset => RESET_TOKEN_VALIDITY
        case EmailTokenType.UserSharedInvite => SHARED_INVITE_TOKEN_VALIDITY
        case EmailTokenType.UpstreamInvite => UPSTREAM_INVITE_TOKEN_VALIDITY
      }
    )

    for {
      id <- (EmailTokens returning EmailTokens.map(_.id)) += EmailTokensRow(0,
        secret, used=false, expiration=expiration, owner, tokenType
      )
    } yield EmailTokenReference(id, secret)
  }

  def getValidToken(reference: EmailTokenReference): DBIO[EmailTokensRow] = {
     for {
       maybeToken <- EmailTokens.filter(t =>
         t.id === reference.id
       ).result.headOption
       token <- maybeToken match {
         case Some(t) => DBIO.successful(t)
         case None => DBIO.failed(HttpCodeException(Unauthorized,
           ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson
         ))
       }
       correctSecret = MessageDigest.isEqual(reference.secret.getBytes, token.secret.getBytes)
       _ <- if (correctSecret) DBIO.successful(Unit) else after(
         // nothing complicated for brute-force prevention; just make 'em wait
         new ScalaDuration(200, MILLISECONDS), actorSystem.scheduler, ec,

         Future.failed[Unit](HttpCodeException(Unauthorized,
           // whether a token has an incorrect ID or secret can still be inferred
           // by timing; I'm sharing the error messages here solely for UX
           ReadableError("_error" -> Messages("emailtokens.nosuchtoken")).toJson))
       ).toDBIO
       _ <- failIf(token.used, HttpCodeException(Unauthorized,
         ReadableError("_error" -> Messages("emailtokens.alreadyused")).toJson
       )).toDBIO
       _ <- failIf(Instant.now.isAfter(token.expiration), HttpCodeException(Unauthorized,
         ReadableError("_error" -> Messages("emailtokens.expired")).toJson
       )).toDBIO
     } yield token
  }

}
