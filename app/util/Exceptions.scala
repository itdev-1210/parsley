package util

import play.api.libs.json.{JsValue, Json}
import play.api.mvc.Results._

// Annoying to have to import Results members using self, but since that's
// how controllers get access to Status type, we have to as well. Ugh.
object Exceptions {

  class HttpCodeException(val status: Status, val body: JsValue) extends
      Exception(s"returning ${status.header.status}, body ${body.toString()}") {

    def this(status: Status, message: String) = this(status, Json.obj("error" -> message))
  }

  object HttpCodeException {
    def apply(status: Status, body: JsValue) = new HttpCodeException(status, body)

    def apply(status: Status, message: String) = new HttpCodeException(status, message)

    def unapply(t: HttpCodeException) = Some((t.status, t.body))
  }
}
