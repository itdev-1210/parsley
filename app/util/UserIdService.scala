package util

import models.QueryExtensions._
import models.Tables._
import models.Tables.profile.api._
import models.MeasureSystemsType
import models._
import util.AsyncUtils.DBExtensions

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import com.google.inject.{ImplementedBy, Inject}
import com.mohiva.play.silhouette.api.services.IdentityService
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.api.{Logger, LoginInfo}
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile

import java.time.Instant

import scala.concurrent.{ExecutionContext, Future}

case class ParsleyUserProfile(
    common: CommonSocialProfile,
    phoneNumber: Option[String],
)

// We can't attach the Identity trait to UsersRow without monkeying with slick-codegen, so we user UserModel
// here purely for interfacing with Silhouette APIs
@ImplementedBy(classOf[DBUserIdService])
trait UserIdService extends IdentityService[UserModel] {

  def retrieveDBIO(loginInfo: LoginInfo): DBIO[Option[UserModel]]
  def create(
      profile: ParsleyUserProfile,
      maybePasswordInfo: Option[PasswordInfo] = None,
      sharedLevel: Option[AccountPermissionLevel.Value] = None,
  ): DBIO[UserModel]

  /*
   * One case that needs whole profile update is when a user is removed by owner
   * then only login details are removed but profile details are kept for further use.
   * So, in case of those users returning by signing up or accepting invitation
   * just update profile instead of calling create method
   */
  def update(
    id: Int, profile: ParsleyUserProfile,
    maybePasswordInfo: Option[PasswordInfo] = None,
    sharedLevel: Option[AccountPermissionLevel.Value] = None,
  ): DBIO[Unit]

  // TODO: Actually do de-duplication of social log-in users
  // def addSocialProfile(profile: ParsleyUserProfile): DBIO[UsersRow]
}

class DBUserIdService @Inject()(
    implicit val ec: ExecutionContext,
    protected val dbConfigProvider: DatabaseConfigProvider,
)
    extends UserIdService with Logger
    with HasDatabaseConfigProvider[models.Tables.profile.type] {

  override def retrieve(loginInfo: LoginInfo) = db.runInTransaction(retrieveDBIO(loginInfo))

  override def retrieveDBIO(loginInfo: LoginInfo): DBIO[Option[UserModel]] = for {
    maybeUser <- findUserByLogin(loginInfo).result.headOption
    maybeUserModel <- maybeUser match {
      case None => DBIO.successful(None)
      case Some(userRow) => for {
        userAccounts <- retrieveUserAccounts(userRow.id)
        accounts <- Accounts.filter(a => (a.id inSet userAccounts.map(_.owner)) && !a.fakeAccount).result
      } yield Some(new UserModel(userRow,
        userAccounts.filter(ua => accounts.map(_.id).contains(ua.owner)),
        accounts
      ))
    }
  } yield maybeUserModel

  // returns one row maximum due to unique (providerId, providerKey) constraints, but
  // Slick doesn't know that...
  def findUserByLogin(li: LoginInfo) =
    UserLogins.byLoginInfo(li).user

  def retrieveUserAccounts(userId: Int): DBIO[Seq[UserAccountsRow]] = UserAccounts
      .filter(t => t.userId === userId && t.isActive === true)
      .result

  override def create(
      profile: ParsleyUserProfile,
      maybePasswordInfo: Option[PasswordInfo] = None,
      permissionLevel: Option[AccountPermissionLevel.Value] = None,
  ): DBIO[UserModel] = {
    val commonProfile = profile.common;
    var user = UsersRow(0,
      email = commonProfile.email,
      firstName = commonProfile.firstName, lastName = commonProfile.lastName, fullName = commonProfile.fullName,
      phoneNumber = profile.phoneNumber,
      weightSystem = MeasureSystemsType.Imperial,
      volumeSystem = MeasureSystemsType.Imperial,
      purchaseWeightSystem = MeasureSystemsType.Imperial,
      purchaseVolumeSystem = MeasureSystemsType.Imperial,
      subtractInventoryPreference = SubtractInventoryType.SubtractNone,
    )
    maybePasswordInfo.foreach { case PasswordInfo(hasher, password, salt) =>
      user = user.copy(hasher=Some(hasher), hashedPassword=Some(password), salt=salt)
    }
    for {
      insertedUser <- Users returning Users += user
      _ <- UserLogins += UserLoginsRow(
        commonProfile.loginInfo.providerID,
        commonProfile.loginInfo.providerKey,
        insertedUser.id,
        commonProfile.firstName,
        commonProfile.lastName,
        commonProfile.fullName
      )
      _ <- UserPrintPreferences += UserPrintPreferencesRow(
        userId = insertedUser.id,
        recipePrintFormatPreference = RecipePrintFormatType.StepByStep,
      )
    } yield new UserModel(insertedUser, Seq(), Seq())
  }

  override def update(
    id: Int, profile: ParsleyUserProfile,
    maybePasswordInfo: Option[PasswordInfo] = None,
    permissionLevel: Option[AccountPermissionLevel.Value] = None,
  ): DBIO[Unit] = {
    val ParsleyUserProfile(
      CommonSocialProfile(
        loginInfo, firstName, lastName, fullName, email, _
      ),
      phoneNumber,
    ) = profile
    val (hasher, hashedPassword, salt) = maybePasswordInfo
      .map(p => (Some(p.hasher), Some(p.password), p.salt))
      .getOrElse((None, None, None))
    val user = Users.filter(_.id === id)
    for {
      _ <- user.map(u =>
        (u.email, u.firstName, u.lastName, u.fullName, u.phoneNumber, u.hasher, u.hashedPassword, u.salt)
      ).update(
        (email, firstName, lastName, fullName, phoneNumber, hasher, hashedPassword, salt)
      )
      _ <- permissionLevel match {
        // TODO: Assuming only one account for now
        case Some(l) => user.userAccounts.map(_.permissionLevel).update(l)
        case None => DBIO.successful(None)
      }
      _ <- UserLogins += UserLoginsRow(loginInfo.providerID, loginInfo.providerKey, id, firstName, lastName, fullName)
    } yield Unit
  }
}
