package util

import java.time.Instant
import javax.inject.Inject
import models.Tables._
import models._

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import scala.concurrent.ExecutionContext

// things that are attached *not* to user profile
case class AccountProfile(
    company: Option[String] = None,
    multiLocationUpstream: Option[Int] = None,
    upstreamRelativeName: Option[String] = None,
)

class AccountSetup @Inject() (
    implicit val ec: ExecutionContext,
    override val dbConfigProvider: DatabaseConfigProvider,
    productSync: ProductSync,
) extends HasDatabaseConfigProvider[models.Tables.profile.type] {
  import dbConfig.profile.api._

  def create(
      accountOwner: Option[Int],
      accountProfile: AccountProfile,
  ): DBIO[AccountsRow] = {
    val AccountProfile(
      businessName,
      multiLocationUpstream, upstreamRelativeName,
    ) = accountProfile

    for {
      newAccount <- (Accounts returning Accounts) += AccountsRow(0,
        accountOwner = accountOwner,
        createdAt = Instant.now,
        businessName = businessName,
        ccEmails = List(),
        addons = List(),

        multiLocationUpstream = multiLocationUpstream,
        upstreamRelativeName = upstreamRelativeName,
      )
      _ <- accountOwner match {
        case Some(userId) => UserAccounts += UserAccountsRow(0,
          owner = newAccount.id, userId = userId,
          isActive = true, joinedAt = Instant.now(),
          permissionLevel = AccountPermissionLevel.Shared,
        )
        case None => DBIO.successful(Unit)
      }
    } yield newAccount
  }

  def update(
      accountId: Int,
      accountProfile: AccountProfile,
  ): DBIO[Unit] = {
    val AccountProfile(
      businessName,
      multiLocationUpstream, upstreamRelativeName,
    ) = accountProfile

    for {
      _ <- Accounts.filter(_.id === accountId)
          .map(_.businessName).update(businessName)

      // don't clear multi-location info
      _ <- if (multiLocationUpstream.isDefined && upstreamRelativeName.isDefined) {
        Accounts.filter(_.id === accountId).map(a =>
          (a.multiLocationUpstream, a.upstreamRelativeName)
        ).update(
          (multiLocationUpstream, upstreamRelativeName)
        )
      } else DBIO.successful(Unit)
    } yield Unit
  }
}
