package util

import models.Tables
import slick.dbio._

import com.parsleycooks.pgdriver.PgDriver

import scala.concurrent.{blocking, ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util._

object AsyncUtils {

  implicit class FutureExtensions[A](f: Future[A]) {
    def toDBIO: DBIO[A] = DBIO.from(f)
  }

  implicit class TryExtensions[A](t: Try[A]) {
    def toFuture: Future[A] = t match {
      case Success(s) => Future.successful(s)
      case Failure(e) => Future.failed(e)
    }

    def toDBIO: DBIO[A] = toFuture.toDBIO
  }

  def blockingOp[A](block: => A)(implicit ec: ExecutionContext): Future[A] =
    Future(blocking(block))

  def sequenceTrys[A](seq: TraversableOnce[Try[A]]): Try[Seq[A]] = {
    seq.foldLeft[Try[Seq[A]]](Success(Seq())) { (tryList, tryNext) =>
      for {
        list <- tryList
        next <- tryNext
      } yield list :+ next
    }
  }

  def failIf[Unit](test: Boolean, e: => Throwable) = {
    if (test) Failure(e)
    else Success(Unit)
  }

  def failIfNoMatch[A, B](
      value: A, test: PartialFunction[A, B], e: => Throwable
  ): Try[B] = {
    test.andThen(Success(_))
        .applyOrElse(value, (_: A) => Failure(e))
  }

  def failIfNone[T](
      maybeValue: Option[T], e: => Throwable
  ): Try[T] = failIfNoMatch[Option[T], T](maybeValue, { case Some(t) => t }, e)

  implicit class DBIOExtensions[+R, +S <: NoStream, -E <: Effect](
      action: DBIOAction[R, S, E]
  ) {

    /**
      * @see [[Future.recoverWith]]
      */
    def recoverWith[E2 <: E, R2 >: R](
        f: PartialFunction[Throwable, DBIOAction[R2, NoStream, E2]]
    )(implicit exec: ExecutionContext) = {
      action.asTry.flatMap({
        case Success(value) => DBIO.successful(value)
        case Failure(e) if f.isDefinedAt(e) => f(e)
        case Failure(e) => DBIO.failed(e)
      })
    }

    def recover[E2 <: Effect, R2 >: R](f: PartialFunction[Throwable, R2])(implicit exec: ExecutionContext) =
      recoverWith(f.andThen(DBIO.successful(_)))
  }

  implicit class DBExtensions[P <: PgDriver](
      db: P#Backend#Database
  ) {
    import Tables.profile.api._
    def runInTransaction[R](a: DBIOAction[R, NoStream, Effect.All]) = db.run(a.transactionally)
  }
}
