package util.modules


import controllers.Env
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.codingwell.scalaguice.ScalaModule
import util.{ParsleyPasswordInfoDAO, UserIdService}
import play.api.Configuration
import play.api.i18n._
import play.api.libs.ws.WSClient
import play.api.mvc.CookieHeaderEncoding
import com.google.inject.{Provides, Singleton}
import com.mohiva.play.silhouette.api.crypto.{AuthenticatorEncoder, Crypter, CrypterAuthenticatorEncoder, Signer}
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AuthenticatorService
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.{Environment, EventBus, Silhouette, SilhouetteProvider}
import com.mohiva.play.silhouette.crypto.{JcaCrypter, JcaCrypterSettings, JcaSigner, JcaSignerSettings}
import com.mohiva.play.silhouette.impl.authenticators._
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.providers.oauth2.{GoogleProvider, LinkedInProvider}
import com.mohiva.play.silhouette.impl.providers.state.CsrfStateSettings
import com.mohiva.play.silhouette.impl.util.{DefaultFingerprintGenerator, SecureRandomIDGenerator}
import com.mohiva.play.silhouette.password.BCryptPasswordHasher
import com.mohiva.play.silhouette.persistence.repositories.DelegableAuthInfoRepository
import com.typesafe.config.Config
import com.typesafe.config.ConfigException.BadValue
import net.ceedubs.ficus.readers.ValueReader

import scala.concurrent.ExecutionContext

class SilhouetteModule extends ScalaModule {
  import play.api.mvc.Cookie.SameSite
  implicit private val SameSiteReader = new ValueReader[SameSite] {
    override def read(config: Config, path: String) = {
      val value = config.getString(path)
      SameSite.parse(value).getOrElse {
        throw new BadValue(config.origin(), path, s"$value is not a valid value for SameSite option; allowed values: Lax, Strict")
      }
    }
  }

  override def configure(): Unit = {
    bind[Silhouette[Env]].to[SilhouetteProvider[Env]]
    bind[HTTPLayer].to[PlayHTTPLayer]
    bind[AuthenticatorEncoder].to[CrypterAuthenticatorEncoder]
    bind[Clock].toInstance(Clock())
    bind[FingerprintGenerator].toInstance(new DefaultFingerprintGenerator())
    bind[PasswordHasherRegistry].toInstance(PasswordHasherRegistry(
      // 10 is default, but making it explicit
      current = new BCryptPasswordHasher(10),
      // NOTE: when the line above is changed, the old value must be added to the line below
      deprecated = Seq()
    ))
  }

  @Provides
  def provideEnvironment(
      userIdService: UserIdService,
      authenticatorService: AuthenticatorService[CookieAuthenticator],
      eventBus: EventBus
  )(implicit ec: ExecutionContext): Environment[Env] = {
    Environment[Env](
      userIdService,
      authenticatorService,
      Seq(),
      eventBus)
  }

  @Provides @Singleton
  def provideIDGenerator(implicit ec: ExecutionContext): IDGenerator =
    new SecureRandomIDGenerator()

  @Provides
  def provideSigner(
      configuration: Configuration
  ): Signer = {
    val settings = configuration.underlying.as[JcaSignerSettings](s"silhouette.cookie.signer")
    if (settings.pepper.isEmpty) {
      throw new Exception("Pepper required for cookie signer!")
    }
    new JcaSigner(settings)
  }

  @Provides
  def provideCrypter(
      configuration: Configuration
  ): Crypter = {
    val settings = configuration.underlying.as[JcaCrypterSettings](s"silhouette.crypter")
    new JcaCrypter(settings)
  }


  @Provides
  def provideAuthenticatorService(
      config: Configuration,
      cookieSigner: Signer,
      encoding: CookieHeaderEncoding,
      authenticatorEncoder: AuthenticatorEncoder,
      fingerprintGenerator: FingerprintGenerator,
      idGenerator: IDGenerator,
      clock: Clock
  )(implicit ec: ExecutionContext): AuthenticatorService[CookieAuthenticator] = {
    val settings = config.underlying.as[CookieAuthenticatorSettings]("silhouette.authenticator")

    new CookieAuthenticatorService(
      settings, repository = None,
      cookieSigner, encoding, authenticatorEncoder,
      fingerprintGenerator, idGenerator, clock)
  }

  @Provides
  def provideSocialProviderRegistry(
      google: GoogleProvider, linkedin: LinkedInProvider
  ): SocialProviderRegistry = {
    SocialProviderRegistry(Seq(google, linkedin))
  }

  @Provides
  def provideGoogle(
      config: Configuration,
      stateProvider: SocialStateHandler,
      httpLayer: HTTPLayer
      ): GoogleProvider = {
    val settings = config.underlying.as[OAuth2Settings]("silhouette.google")
    new GoogleProvider(httpLayer, stateProvider, settings)
  }

  @Provides
  def provideLinkedIn(
      config: Configuration,
      stateProvider: SocialStateHandler,
      httpLayer: HTTPLayer
  ): LinkedInProvider = {
    val settings = config.underlying.as[OAuth2Settings]("silhouette.linkedin")
    new LinkedInProvider(httpLayer, stateProvider, settings)
  }

  @Provides
  def provideHTTPLayer(client: WSClient, ec: ExecutionContext) = new PlayHTTPLayer(client)(ec)

  @Provides
  def provideCsrfStateSettings(config: Configuration): CsrfStateSettings = {
    config.underlying.as[CsrfStateSettings]("silhouette.csrfState")
  }

  @Provides
  def provideSocialStateHandler(signer: Signer): SocialStateHandler =
    new DefaultSocialStateHandler(Set.empty, signer)

  @Provides
  def provideAuthInfoRepository(
      passwordsDAO: ParsleyPasswordInfoDAO
  )(implicit ec: ExecutionContext): AuthInfoRepository =
    new DelegableAuthInfoRepository(passwordsDAO)

  // ugh ugh ugh provides*Provider???? For serious?
  @Provides
  def provideCredentialsProvider(
      hasherRegistry: PasswordHasherRegistry,
      authInfoRepository: AuthInfoRepository
  )(implicit ec: ExecutionContext): CredentialsProvider = {
    new CredentialsProvider(authInfoRepository, hasherRegistry)
  }

  @Provides
  def provideMessages(langs: Langs, api: MessagesApi): MessagesProvider = {
    MessagesImpl(langs.availables.head, api)
  }

}
