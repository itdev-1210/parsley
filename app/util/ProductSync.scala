package util

import AsyncUtils._
import models.JsonConversions._
import models.QueryExtensions._
import models.Tables._
import models._
import util.Exceptions.HttpCodeException

import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.mvc.Results._

import javax.inject.Inject
import java.time.Instant

class ProductSync @Inject() (
    override val dbConfigProvider: DatabaseConfigProvider,
    implicit val ec: scala.concurrent.ExecutionContext,
    val productsController: controllers.api.Products,
) extends HasDatabaseConfigProvider[models.Tables.profile.type] {

  type ProductQuery = AllColumnsQuery[Products, ProductsRow]

  import dbConfig.profile.api._
  import productsController.DBActions.{fetchProductAPIEntity, modifyProductFromAPIEntity}

  def copyExtraInfo(downstreamProductId: Int, upstreamProductId: Int) = sqlu"""
    INSERT INTO product_infos AS downstream_product_info (
      product,
      lowest_price_preferred
    )
    SELECT
      $downstreamProductId,
      upstream_product_info.lowest_price_preferred
    FROM product_infos upstream_product_info
    WHERE upstream_product_info.product = $upstreamProductId
    ON CONFLICT (product) DO UPDATE SET lowest_price_preferred = EXCLUDED.lowest_price_preferred;
  """

  def copyMeasures(downstreamProductId: Int, upstreamProductId: Int) = sqlu"""
    INSERT INTO product_measures AS downstream_product_measures (
      product,
      measure,
      preferred_unit,
      conversion_measure,
      conversion_unit,
      conversion,
      custom_name
    )
    SELECT
      ${downstreamProductId},
      measure,
      preferred_unit,
      conversion_measure,
      conversion_unit,
      conversion,
      custom_name
    FROM product_measures upstream_product_measures
    WHERE upstream_product_measures.product = ${upstreamProductId}
    ON CONFLICT (product, measure) DO UPDATE SET custom_name=coalesce(
        downstream_product_measures.custom_name, EXCLUDED.custom_name
      )
     -- with stdlibCorrection implementation, will need to rethink this
  """

  def copyNutrients(downstreamProductId: Int, upstreamProductId: Int) = sqlu"""
    INSERT INTO product_nutrients AS downstream_product_nutrients (
      product,
      nutrient,
      amount
    )
    SELECT
      ${downstreamProductId},
      nutrient,
      amount
    FROM product_nutrients upstream_product_nutrients
    WHERE upstream_product_nutrients.product = ${upstreamProductId}
    ON CONFLICT (product, nutrient) DO NOTHING
  """

  def copyNutrientAllergens(downstreamProductId: Int, upstreamProductId: Int) = sqlu"""
    INSERT INTO product_allergens AS downstream_product_allergens (
      product,
      milk,
      eggs,
      fish,
      crustacean_shellfish,
      tree_nuts,
      wheat,
      peanuts,
      soybeans,
      molluscs,
      cereals_gluten,
      celery,
      mustard,
      sesame_seeds,
      sulphur_dioxide_sulphites,
      lupin
    )
    SELECT
      ${downstreamProductId},
      milk,
      eggs,
      fish,
      crustacean_shellfish,
      tree_nuts,
      wheat,
      peanuts,
      soybeans,
      molluscs,
      cereals_gluten,
      celery,
      mustard,
      sesame_seeds,
      sulphur_dioxide_sulphites,
      lupin
    FROM product_allergens upstream_product_allergens
    WHERE upstream_product_allergens.product = ${upstreamProductId}
    ON CONFLICT (product) DO NOTHING
  """

  def copyNutrientCharacteristics(downstreamProductId: Int, upstreamProductId: Int) = sqlu"""
    INSERT INTO product_characteristics AS downstream_product_characteristics (
      product,
      added_sugar,
      meat,
      pork,
      corn,
      poultry,
      non_edible
    )
    SELECT
      ${downstreamProductId},
      added_sugar,
      meat,
      pork,
      corn,
      poultry,
      non_edible
    FROM product_characteristics upstream_product_characteristics
    WHERE upstream_product_characteristics.product = ${upstreamProductId}
    ON CONFLICT (product) DO NOTHING
  """

  def copyPreparations(downstreamUserId: Int, downstreamPrepProductId: Int, upstreamPrepProductId: Int) = {
    val upstreamPreparation = SimplePreparations.filter(_.outputProduct === upstreamPrepProductId);
    for {
      _ <- upstreamPreparation.result.headOption.flatMap {
        case None => DBIO.successful(Unit)
        case Some(prep) => for {
          maybeRawProduct <- Products.filter(p => p.owner === downstreamUserId && p.syncUpstream === prep.inputProduct).map(_.id).result.headOption
          downstreamRawProductId <- failIfNone(maybeRawProduct,
            // TODO: check for case where raw is coming from multi-location upstream, and use *that* instead
            HttpCodeException(InternalServerError, s"downstream user $downstreamUserId doesn't have raw product for prepped product $downstreamPrepProductId (upstream prepped orig: $upstreamPrepProductId, upstream raw orig: ${prep.inputProduct})"),
          ).toDBIO
          correspondingDownstreamPrepExists <- SimplePreparations.filter(p => p.inputProduct === downstreamRawProductId && p.name === prep.name).exists.result
          _ <- if (correspondingDownstreamPrepExists) {
            sqlu"""
              UPDATE simple_preparations
              SET
                advance_prep = ${prep.advancePrep},
                uses_parent_measures = ${prep.usesParentMeasures},
                yield_fraction = ${prep.yieldFraction},
                yield_measure = ${prep.yieldMeasure}
              WHERE input_product = ${downstreamRawProductId} AND name = ${prep.name}
            """
          } else {
            sqlu"""
              INSERT INTO simple_preparations (
                output_product,
                input_product,
                name,
                advance_prep,
                uses_parent_measures,
                yield_fraction,
                yield_measure
              )
              VALUES (
                ${downstreamPrepProductId},
                ${downstreamRawProductId},
                ${prep.name},
                ${prep.advancePrep},
                ${prep.usesParentMeasures},
                ${prep.yieldFraction},
                ${prep.yieldMeasure}
              )
              ON CONFLICT (output_product) DO NOTHING
            """
          }
        } yield DBIO.successful(Unit)
      }
    } yield()
  }

  /*
   * Should be a NO-OP when there is no recipe for the upstream product
   */
  def copyRecipes(
      downstreamUserId: Int, downstreamProductId: Int, upstreamProductId: Int,
      hardOverwrite: Boolean = false
  ) = {
    for {
      _ <- if (hardOverwrite) {
        val oldRecipe = Recipes.filter(_.product === downstreamProductId)
        // BUGGY: we end up with orphan photos in S3 in this way when the upstream
        // changed or deleted the photo and downstream is the only one that still uses it
        oldRecipe.steps.ingredientUsage.delete andThen oldRecipe.steps.delete andThen oldRecipe.delete
      } else DBIO.successful(Unit)
      oldOwner <- Products.filter(_.id === upstreamProductId).user.map(_.id).result.headOption

      numInserted <- sqlu"""
        INSERT INTO recipes (
          product,
          output_amount, output_measure, output_unit,
          quantum_amount, quantum_measure, quantum_unit,
          max_batch_amount, max_batch_measure, max_batch_unit,
          price, batch_size, batch_label, advance_prep,
          created_at, created_by
        )
        SELECT
          ${downstreamProductId},
          upstream.output_amount, upstream.output_measure, upstream.output_unit,
          upstream.quantum_amount, upstream.quantum_measure, upstream.quantum_unit,
          upstream.max_batch_amount, upstream.max_batch_measure, upstream.max_batch_unit,
          upstream.price, upstream.batch_size, batch_label, advance_prep,
          created_at, ${oldOwner}
        FROM recipes upstream
        WHERE upstream.product = ${upstreamProductId}
        ON CONFLICT DO NOTHING;
      """
      _ <- if (numInserted > 0) { // recipe is totally new - no modifications!
        for {
          _ <- sqlu"""
            INSERT INTO recipes_revision_history (product, owner, last_modified)
            SELECT
              ${downstreamProductId}, upstream.owner, upstream.last_modified
            FROM recipes_revision_history upstream
            WHERE upstream.product = ${upstreamProductId}
          """

          _ <- sqlu"""
            INSERT INTO recipe_steps (product, display_order, description, photo)
            SELECT
              ${downstreamProductId}, upstream.display_order, upstream.description,
              upstream.photo
            FROM recipe_steps upstream
            WHERE upstream.product = ${upstreamProductId}
          """
          stepIdMappings <- (RecipeSteps.filter(
            _.product === upstreamProductId
          ) join RecipeSteps.filter(
            _.product === downstreamProductId
          ) on (_.displayOrder === _.displayOrder))
              .map { case (s1, s2) => (s1.id, s2.id) }
              .result
          _ <- DBIO.sequence(stepIdMappings.map { case (upstreamStep, downstreamStep) =>
              // select clause picks, for each usage in upstream's step, the
              // downstream ingredient synced from it that was created first (has
              // the smallest ID).
              sqlu"""
                 INSERT INTO recipe_step_ingredients (
                   step, display_order,
                   ingredient, description,
                   amount, measure, unit
                 )
                 SELECT DISTINCT ON (upstream_usage.id)
                  ${downstreamStep}, upstream_usage.display_order,
                  downstream_ingredient.id, upstream_usage.description,
                  upstream_usage.amount, upstream_usage.measure, upstream_usage.unit
                 FROM recipe_step_ingredients upstream_usage join products downstream_ingredient
                  ON upstream_usage.ingredient = downstream_ingredient.sync_upstream
                 WHERE upstream_usage.step = ${upstreamStep} and downstream_ingredient.owner = ${downstreamUserId}
                 ORDER BY upstream_usage.id, downstream_ingredient.id;
              """
          })
        } yield Unit
      } else {
        DBIO.successful(Unit)
      }
    } yield Unit
  }

  def copyCategories(downstreamUserId: Int, upstreamProductId: Int) = for {
    upstreamCategories <- Products.filter(_.id === upstreamProductId).categories.categories.result
    _ <- DBIO.sequence(upstreamCategories.map(uc =>
      sqlu"""
        INSERT INTO categories (
          owner,
          name,
          category_type,
          sync_upstream
        )
        SELECT
          ${downstreamUserId},
          upstream.name,
          upstream.category_type,
          ${uc.id}
        FROM categories upstream
          LEFT JOIN categories downstream
          ON upstream.name = downstream.name
            AND downstream.owner = $downstreamUserId
            AND downstream.category_type = upstream.category_type
        WHERE upstream.id = ${uc.id} AND downstream.id IS NULL;
      """ // condition above says - only the ones that don't exist downstream
          andThen
      // if we can't do the copy above, we have to take an existing category
      // and make it our downstream
      // also handles the legacy case, where sync was set up before the move of
      // categories to a separate table
      sqlu"""
        UPDATE categories SET sync_upstream = ${uc.id}
        WHERE owner = ${downstreamUserId} AND name = ${uc.name}
          AND category_type = ${uc.categoryType.toString}::product_category_type;
      """
    ))
  } yield Unit

  def copyProductCategories(downstreamUserId: Int, downstreamProductId: Int, upstreamProductId: Int) = for {
    upstreamProductCategories <- Products.filter(_.id === upstreamProductId).categories.result
    _ <- DBIO.sequence(upstreamProductCategories.map(upc =>
      for {
        downstreamCategoryId <- Categories
            .filter(dc => dc.owner === downstreamUserId && dc.syncUpstream === upc.category)
            .map(_.id).result.head
        _ <- sqlu"""
          INSERT INTO product_categories (
            product,
            category
          )
          VALUES (
            ${downstreamProductId},
            ${downstreamCategoryId}
          )
          ON CONFLICT DO NOTHING;
        """
      } yield Unit
    ))
  } yield Unit

  def copyProducts(
      downstreamUserId: Int, upstreamProducts: AllColumnsQuery[Products, ProductsRow], sourceName: String
  ): DBIO[Seq[(Int, Int)]] = { // mappings from upstream to downstream product IDs
    val downstreamProducts = Products.filter(_.owner === downstreamUserId)

    for {
      copiedInfo <- (Products.map { p => (
          // insertion format
          p.owner, p.syncUpstreamOwner,
          p.name,
          p.ingredient, p.salable,
          p.nndbsrId, p.nndbsrLastImport, p.syncUpstream, p.lastSynced, p.lastModified,
          p.hidden,
          p.nutrientServingAmount, p.nutrientServingMeasure, p.nutrientServingUnit,
      )} returning Products.map { p =>
        (p.id, p.syncUpstream.get, p.hidden, p.ingredient)
      }).forceInsertQuery(upstreamProducts.map { p => (
          Rep.Some(downstreamUserId), p.owner,
          p.name,
          p.ingredient, p.salable,
          p.nndbsrId, p.nndbsrLastImport, Rep.Some(p.id), Rep.Some(Instant.now), p.lastModified,
          p.hidden,
          p.nutrientServingAmount, p.nutrientServingMeasure, p.nutrientServingUnit,
      )})
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyExtraInfo(downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyMeasures(downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyNutrients(downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.filter(p => p._3 && p._4).map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyPreparations(downstreamUserId, downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyNutrientAllergens(downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyNutrientCharacteristics(downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyRecipes(downstreamUserId, downstreamProductId, upstreamProductId)
      })
      _ <- DBIO.sequence(copiedInfo.map { case (downstreamProductId, upstreamProductId, _, _) =>
        copyCategories(downstreamUserId, upstreamProductId) andThen
        copyProductCategories(downstreamUserId, downstreamProductId, upstreamProductId)
      })

      // post-copy cleanup - find any duplicate names we've inserted
      duplicateNames <- (
          downstreamProducts.filter(_.id.inSet(copiedInfo.map(_._1)))
              join
              downstreamProducts.filter(!_.id.inSet(copiedInfo.map(_._1)))
              on
              (_.name.toLowerCase === _.name.toLowerCase)
          ).map(_._1).map(newProd => (newProd.id, newProd.name)).result
      _ <- DBIO.sequence(duplicateNames.map { case (id, name) =>
        downstreamProducts.filter(_.id === id).map(_.name)
            // TODO: allow customization of this message (also SupplierSync)
            .update(name ++ s" (from $sourceName)")
      })
    } yield copiedInfo.map { case (downstreamProductId, upstreamProductId, hidden, ingredient) =>
        upstreamProductId -> downstreamProductId
    }
  }

  def fixTags(
    productId: Int,
    tags: Seq[TagEntity],
  ) : DBIO[Seq[TagEntity]] = DBIO.sequence(tags.map(t =>
    for {
      downstreamTag <- Categories.filter(c => c.owner === productId && c.name === t.name).result.headOption
    } yield downstreamTag match {
      case Some(dt) => TagEntity.fromDB(dt).copy(upstreamId = Some(t.id))
      case None => TagEntity(-1, t.name, Some(t.id))
    }
  ))

  def expandProduct(
    downstreamUserId: Int,
    downstreamProductId: Int,
    upstreamProductId: Int,
    syncStdlibProducts: Boolean = false
  ) = for {
    mUpstreamProd <- fetchProductAPIEntity(upstreamProductId, Products)
    mDownstreamProd <- fetchProductAPIEntity(downstreamProductId, Products)
    _ <- (mUpstreamProd, mDownstreamProd) match {
      case (Some(upstreamProd), Some(downstreamProd)) => {
        val allowCorrection = !syncStdlibProducts
        val (mergedProd, _, _) = downstreamProd.merge(upstreamProd, allowCorrection)
        val downstreamProducts = Products.filter(_.owner === downstreamUserId)
        val preppedProductUpstreamIds = mergedProd.simplePreps.values.map(_.preparationInfo.outputProduct).toSet
        for {
          // ProductAPIEntity.merge() doesn't know how to translate product IDs
          // from upstream to downstream based on sync info. So need to do this
          // for the two places that reference other products - preps and recipes

          preppedProductUpDownPairs <- downstreamProducts.filter(_.syncUpstream inSet preppedProductUpstreamIds)
                  .map(p => (p.syncUpstream.get, p.id)).result
          downstreamProductForUpstream = preppedProductUpDownPairs.toMap
          // set preferred source if upstream has one
          fixedPurchaseInfo = (mergedProd.purchaseInfo, upstreamProd.purchaseInfo) match {
            case (Some(mProdPurchaseInfo), Some(PurchaseInfoAPIEntity(Some(uProdPreferredSource), _))) =>
              Some(mProdPurchaseInfo.copy(
                preferredSource = mProdPurchaseInfo.combinedSources.find(_.syncUpstream == Some(uProdPreferredSource.id)),
                otherSources = mProdPurchaseInfo.combinedSources.filter(_.syncUpstream != Some(uProdPreferredSource.id)),
              ))
            case _ => mergedProd.purchaseInfo
          }
          fixedPreps = mergedProd.simplePreps.mapValues { prep =>

            val newMainInfo = downstreamProductForUpstream.get(prep.preparationInfo.outputProduct) match {
              // either it's a preparation copied from upstream, in which case it has output products
              // that are owned by the upstream user and not the downstream...
              case Some(downstreamVersion) => prep.preparationInfo.copy(outputProduct = downstreamVersion)
              // or it's from the downstream and doesn't need any changes
              case None => prep.preparationInfo
            }

            prep.copy(preparationInfo = newMainInfo)
          }

          fixedRecipe <- if (allowCorrection) {
            // The standard `merge` method doesn't merge recipe steps
            val downstreamProductQuery = Products.filter(_.id === downstreamProductId)
            val allUpstreamRecipeIngredientsId = upstreamProd.recipe match {
              case None => Seq()
              case Some(r) => r.steps.flatMap(s => s.ingredients.map(_.ingredient))
            }
            for {
              // Delete, then re-add recipe steps & ingredients
              _ <- downstreamProductQuery.recipe.steps.ingredientUsage.delete
              _ <- downstreamProductQuery.recipe.steps.delete

              // upstream's product id -> downstream's product id
              productUpstreamDownstreamPair <- Products
                  .filter(p => p.owner === downstreamUserId && p.syncUpstream.inSet(allUpstreamRecipeIngredientsId))
                  .map(p => (p.syncUpstream.get, p.id)).result
            } yield {
              val productUpstreamDownstreamMap = productUpstreamDownstreamPair.toMap
              upstreamProd.recipe match {
                case None => None
                case Some(upstreamRecipe) => {
                  Some(upstreamRecipe.copy(
                    steps = upstreamRecipe.steps.map(s => s.copy(
                      step = s.step.copy(
                        id = -1
                      ),
                      ingredients = s.ingredients.map(i => i.copy(
                        id = -1,
                        step = -1,
                        ingredient = productUpstreamDownstreamMap(i.ingredient)
                      ))
                    )),
                    history = upstreamRecipe.history.map(h => h.copy(
                      id = -1,
                      product = downstreamProductId
                    ))
                  ))
                }
              }
            }
          } else {
            // we don't have any well-defined way to "expand" a recipe
            DBIO.successful(mergedProd.recipe)
          }

          _ <- RecipesRevisionHistory.filter(_.product === downstreamProductId).filterNot(_.owner === downstreamUserId).delete

          fixedIngredientTags <- fixTags(downstreamUserId, upstreamProd.ingredientTags)
          fixedRecipeTags <- fixTags(downstreamUserId, upstreamProd.recipeTags)
          fixedInventoryTags <- fixTags(downstreamUserId, upstreamProd.inventoryTags)

          _ <- modifyProductFromAPIEntity(
            mergedProd.copy(
              purchaseInfo = fixedPurchaseInfo,
              simplePreps = fixedPreps,
              recipe = fixedRecipe,
              ingredientTags = fixedIngredientTags,
              recipeTags = fixedRecipeTags,
              inventoryTags = fixedInventoryTags
            ),
            Products,
            downstreamProductId
          )
          modificationTime <- Products.filter(_.id === upstreamProductId).map(_.lastModified).result.head
          _ <- Products.filter(_.id === downstreamProductId).map(_.lastModified).update(modificationTime)
          _ <- Products.filter(_.id === downstreamProductId).map(_.lastSynced).update(Some(Instant.now))
        } yield Unit
      }
      case _ => DBIO.successful(None)
    }
  } yield ()

  private class SyncPlusRequirementsTempTable(_tableTag: Tag) extends Table[Int](_tableTag, "product_sync_plus_dependencies") {
    def product = column[Int]("product", O.Unique)
    def * = product
  }
  private object syncPlusRequirements extends TableQuery(new SyncPlusRequirementsTempTable(_))
  /**
    *
    * @param rootRequirements
    * @return a @DBIO *containing a query*
    *         query will include rootRequirements
    */
  def collectDependencies(
      rootRequirements: AllColumnsQuery[Products, ProductsRow]
  ): DBIO[AllColumnsQuery[Products, ProductsRow]] = for {
    // DB xact is used in a single-threaded manner, so if we've gotten back to
    // here within the same xact it means we've finished the previous sync
    // the last table creation in the xact is taken care of by "on commit drop"
    _ <- sqlu"""
      DROP TABLE IF EXISTS product_sync_plus_dependencies;
    """
    _ <- sqlu"""
      CREATE TEMP TABLE product_sync_plus_dependencies (
        product INT UNIQUE
      ) ON COMMIT DROP;
    """
    _ <- syncPlusRequirements.forceInsertQuery(rootRequirements.map(_.id).distinct)
    _ <- collectDependenciesRecursive(rootRequirements)
  } yield {
    Products.filter(_.id in syncPlusRequirements)
  }

  // may require multiple passes to assemble all deps!
  private def collectDependenciesRecursive(
      toCheck: AllColumnsQuery[Products, ProductsRow],
  ): DBIO[Unit] = {
    def depIsNew(id: Rep[Int]) =
      !(id in syncPlusRequirements)

    val newRecipeDependencies = toCheck.recipe.steps.ingredientUsage
        .map(_.ingredient)
        .filter(depIsNew)
    val newPrepDependencies = toCheck.preppedBy
        .map(_.inputProduct)
        .filter(depIsNew)

    // when we copy a product, also want to copy all its preps, because:
    // a) from the user's perspective they're kind of the same product
    // b) when deciding on upstream/stdlib hijacks, we don't want to copy
    //    only the preps and not the raw from stdlib
    val newPrepReverseDeps = toCheck.simplePreps
        .map(_.outputProduct)
        .filter(depIsNew)

    for {
      newDependencies <- (
          newRecipeDependencies ++ newPrepDependencies ++ newPrepReverseDeps
      ).to[Set].result

      _ <- if (newDependencies.isEmpty) {
        DBIO.successful(Products.filter(_.id inSet newDependencies))
      } else {
        (syncPlusRequirements ++= newDependencies).andThen(
          collectDependenciesRecursive(Products.filter(_.id inSet newDependencies))
        )
      }
    } yield Unit
  }

  /**
    * fastpath for when it's 100% certain that there aren't any existing synced
    * copies
    *
    * @param downstreamUserId used to fill in new owner field
   *  @param sourceName used to add clarification in parens for duplicate products
    * @return mappings from upstream product IDs to downstream IDs
    */
  def init(
      downstreamUserId: Int, upstreamProducts: AllColumnsQuery[Products, ProductsRow],
      sourceName: String,
  ): DBIO[Seq[(Int, Int)]] = copyProducts(downstreamUserId, upstreamProducts, sourceName)

  /**
    *
    * @param downstreamUserId used to check for existing synced products,
    *                         and to fill in new owner field
    * @param productWasSynced whether the product came from *this* upstream source
    *                         (stdlib, specific upstream)
    * @param upstreamProducts *including* any dependencies
    * @return mappings from upstream product IDs to downstream IDs
    */
  private def sync(
      downstreamUserId: Int,
      productWasSynced: Products => Rep[Boolean],
      upstreamProducts: AllColumnsQuery[Products, ProductsRow],
      sourceName: String,
      syncStdlibProducts: Boolean = false
  ): DBIO[Seq[(Int, Int)]] = {
    val downstreamProducts = Accounts.filter(_.id === downstreamUserId).products
    for {

      // ordering here is important. want to assemble list of actions (upstreams
      // to sync and to create) based on initial DB state, so before any writes
      // have happened). Run toModify query up front...
      toExpand <- {
        downstreamProducts join upstreamProducts on (_.syncUpstream === _.id)
      }.filter { case (downstreamProd, upstreamProd) =>
        downstreamProd.lastSynced.isEmpty ||
            downstreamProd.lastSynced < upstreamProd.lastModified
      }.map {case (downstreamProd, upstreamProd) =>
        upstreamProd.id -> downstreamProd.id
      }.sortBy { case (_, downstreamId) =>
        // the one that shows up in the map is the *last* one; so we order this
        // map so that the *first* created of the possibly-multiple copies for one
        // upstream product will be *last* in the seq of k-v pairs
        downstreamId.desc
      }.result
      toCopy = (upstreamProducts joinLeft downstreamProducts on (_.id === _.syncUpstream))
          .filter { case (upstream, downstream) => !upstream.tombstone && downstream.isEmpty }
          .map { case (upstream, downstream) => upstream }
          .distinct

      // ...and run copy (which takes a Query) first
      copiedMap <- copyProducts(downstreamUserId, toCopy, sourceName)
      _ <- DBIO.sequence(toExpand.map { case (upstreamProd, downstreamProd) =>
        expandProduct(downstreamUserId, downstreamProd, upstreamProd, syncStdlibProducts)
      })
      _ <- if (syncStdlibProducts) {
        val toUnlink = downstreamProducts.filter(productWasSynced)
            .filter(ds => !ds.syncUpstream.in(upstreamProducts.map(_.id)))
            .syncUpstream
        // WARNING - will unsync ALL downstreams!
        productsController.DBActions.deleteOrUnlinkDownstreams(toUnlink)
      } else {
        DBIO.successful(Unit)
      }

      // TODO: corrections
    } yield copiedMap ++ toExpand
  }

  private val multiLocationDuplicateLabel = "group account"
  private def multiLocationStdlibProductsToHijack(downstreamUserId: Int, upstreamProducts: AllColumnsQuery[Products, ProductsRow]) = {
    val downstreamProducts = Products.filter(_.owner === downstreamUserId)
    ( // find products that come from the same stdlib original
        upstreamProducts join downstreamProducts on (_.syncUpstream === _.syncUpstream)
    ).map { case (upstreamProd, downstreamProd) =>
      downstreamProd.id -> (upstreamProd.id, upstreamProd.owner)
    }
  }

  def syncMultiLocation(downstreamUserId: Int, upstreamUserId: Int, upstreamProducts: AllColumnsQuery[Products, ProductsRow])= for {
    // if a product from downstream has the same stdlib original as a newly-synced
    // product from upstream, make them now take the multi-location upstream as
    // their sync source
    // TODO: Also update lastSynced timestamp
    upstreamAndDependencies <- collectDependencies(upstreamProducts)
    stdlibSyncsToHijack <- multiLocationStdlibProductsToHijack(downstreamUserId, upstreamAndDependencies).result
    _ <- DBIO.sequence(stdlibSyncsToHijack.map { case (downstream, (newUpstream, newUpstreamOwner)) =>
      Products.filter(_.id === downstream)
          .map(p => (p.syncUpstream, p.syncUpstreamOwner, p.lastSynced))
          .update((Some(newUpstream), newUpstreamOwner, None))
    })

    // and now do a normal sync
    _ <- sync(
      downstreamUserId,
      (_: Products).syncUpstreamOwner.getOrElse(-1) === upstreamUserId,
      upstreamAndDependencies, multiLocationDuplicateLabel
    )
  } yield Unit

  private val stdlibProducts = Products.filter(_.owner.isEmpty)
  private val stdlibDuplicateLabel = "Parsley ingredient library"

  /**
   * filters out products that should not be synced because they duplicate an upstream prod
   */
  private def stdlibProductsToSync(accountId: Int): DBIO[AllColumnsQuery[Products, ProductsRow]] = {
    val userQuery = Accounts.filter(_.id === accountId)
    for {
      stdlibAndDependencies <- collectDependencies(stdlibProducts)
    } yield {
      val stdlibProductsFromUpstream = (
        userQuery.products join userQuery.upstream.products on (_.syncUpstream === _.id)
      ).filter { case (downstream, upstream) =>
        // stdlib-synced products have syncUpstreamOwner empty
        upstream.syncUpstream.isDefined && upstream.syncUpstreamOwner.isEmpty
      }.map { case (downstream, upstream) => upstream.syncUpstream }

      stdlibAndDependencies.filter(stdlibProd => !(Rep.Some(stdlibProd.id) in stdlibProductsFromUpstream))
    }
  }

  def syncStdlib(accountId: Int): DBIO[Unit] = for {
    toSync <- stdlibProductsToSync(accountId)
    _ <- sync(
      accountId,
      p => p.syncUpstream.isDefined && p.syncUpstreamOwner.isEmpty,
      toSync, stdlibDuplicateLabel, syncStdlibProducts = true
    )
  } yield Unit

  def initStdlib(accountId: Int): DBIO[Unit] = for {
    toSync <- stdlibProductsToSync(accountId)
    _ <- init(accountId, toSync, stdlibDuplicateLabel)
  } yield Unit
}
