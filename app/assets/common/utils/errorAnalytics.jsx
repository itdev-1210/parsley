// @flow

export function noticeError(message: string) {
  if (window.NREUM) // NREUM = New Relic End User Monitoring
    window.NREUM.noticeError(new Error(message));
  console.log(message);
}