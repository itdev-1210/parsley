// @flow

export const prettyErrorMsg = 'A mistake on our part!  Please report this error to service@parsleycooks.com';

// time formats
export const dateFormat = 'MMM D, YYYY';
export const dateLongFormat = 'MMMM D, YYYY';
export const dateWithWeekdayFormat = 'dddd, ' + dateLongFormat;
export const timeFormat = 'h:mm a';
export const dateTimeShortFormat = 'ddd, MMMM D, h:mma';

// order list
export const relevantPurchaseDuration = 10; // in minutes

// values should be kept in sync with util/ParsleyEnvironment.scala
export type Env = 'dev' | 'test' | 'staging' | 'prod'

export const PERMISSION_LEVELS = {
  'recipe-read-only': 'Recipe Read-Only',
  'operations': 'Operations',
  'shared': 'Shared',
};

export type PermissionLevel = $Keys<typeof PERMISSION_LEVELS>;
