/* @flow */

export type comparator = (a: mixed, b: mixed) => (1 | 0 | -1)

// for tiebreaking by normal '<' and '>' operators
// need to have these declarations because we need to be passed exactly one of these types, not a union type
declare function defaultComparator(a: number, b: number): (1 | 0 | -1)
declare function defaultComparator(a: string, b: string): (1 | 0 | -1)
declare function defaultComparator(a: bool, b: bool): (1 | 0 | -1)
export function defaultComparator(a, b) {
  if (a == b) {
    return 0;
  } else if (a < b) {
    return -1;
  } else {
    return 1;
  }
}

export function reverseComparator(orig: comparator): comparator {
  // $FlowFixMe flow doesn't understand that the enum is closed over negation
  return (a, b) => -orig(a, b);
}
