export function transformErrorValue(handler) {
  return (error) => Promise.reject(handler(error))
}

export function addSideEffect(sideEffectGenerator) {
  return value => {
    sideEffectGenerator(value);
    return value;
  }
}