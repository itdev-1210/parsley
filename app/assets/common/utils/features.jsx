// @flow

import type { Env } from 'common/utils/constants';

import type { Node } from 'react';

type SubscriptionLevelInfo = {| name: string |};
const levelDescs = {
  'business-with-nutr': {
    name: 'Business Plan with Nutritional Information',
  },
  'business': { // legacy
    name: 'Business Plan',
  },
  'chef-with-nutr': {
    name: 'Chef Plan with Nutritional Information',
  },
  'chef': {
    name: 'Chef Plan',
  },
};

export type SubscriptionLevel = $Keys<typeof levelDescs>;
// we make sure never to suggest a user "upgrade" (actually downgrade) to a subscription
// level earlier in this list than their current one
export const planPreferenceOrder: Array<SubscriptionLevel> = [
  'chef', 'chef-with-nutr',
  'business', 'business-with-nutr',
];

// doing this weird declare-then-export so we can both infer the keys from the
// literal, AND specify that the values are of a specific shape
export const SUBSCRIPTION_LEVELS: { [SubscriptionLevel]: SubscriptionLevelInfo } = levelDescs;

export function upgradeRequiredText(level: SubscriptionLevel): Node {
  return [
    ` upgrade to a ${SUBSCRIPTION_LEVELS[level].name}`,
  ];
}

/*
 * Buncha fields for upselling text. For their usage, see FeatureGating.jsx
 */
export type UpsellVerbiage = {|
  // a noun phrase e.g. "custom logos for printed pages"
  description: string,
  // a present-tense infinitive verb phrase, for use after the description has been
  // read, to describe the act of using the feature.
  infinitive: string,
|};

type FeatureInfo = $Shape<{
  // generally, anything that is disabled in prod should also be disabled in staging,
  // for testing consistency
  disabledEnvs?: Array<Env>,

  // 1. if undefined, feature enabled for all plans
  // 2. if defined, enabled only for listed plans
  // special case of 2: if defined and empty, feature enabled for no plans - "beta"
  // admin users have access to everything
  // NOTE: *first* one is the one used for up-selling text
  enabledSubscriptionLevels?: Array<SubscriptionLevel>,


  upsellVerbiage?: UpsellVerbiage,
}>

// map from feature names to feature enablement information
export const featureDescs = {
  DASHBOARD: {
  },

  MULTI_DASHBOARD: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },

  GRAY_LOGO: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'custom logos for printed pages',
      infinitive: 'add a logo',
    },
  },
  READONLY_USER: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },
  MULTI_PURCHASE: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'purchasing for multiple days or events',
      infinitive: 'purchase for these events',
    },
  },
  MULTI_USER: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'multiple users per account',
      infinitive: 'add users',
    },
  },
  MULTI_LOCATION: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'syncing recipes to another account',
      infinitive: 'sync between accounts',
    },
  },
  OPERATIONS_USER: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'allow Operations level access to organization',
      infinitive: 'share to user',
    },
  },
  NUTRITIONAL_INFO: {
    enabledSubscriptionLevels: ['chef-with-nutr', 'business-with-nutr'],
    upsellVerbiage: {
    description: 'nutrition facts for recipes',
      infinitive: 'include them',
    },
  },
  RECEIVING: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },
  INVENTORY: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },
  INVENTORY_SHRINKAGE: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },
  PAR_LEVELS: {
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
    upsellVerbiage: {
      description: 'par levels',
      infinitive: 'set and order to par levels',
    },
  },

  RECIPE_EXPORT: {
    upsellVerbiage: {
      description: 'CSV exports of recipes',
      infinitive: 'export your recipes',
    },
  },

  ONBOARDING_RESET: {
    disabledEnvs: ['staging', 'prod'],
  },

  POS: {
    upsellVerbiage: {
      description: 'Point-of-Sale integration',
      infinitive: 'import sales data from your Point-of-Sale system',
    },
    enabledSubscriptionLevels: ['business-with-nutr', 'business'],
  },

  INVOICE_IMPORT: {
    disabledEnvs: ['staging', 'prod'],
  },

  IMPORT_RECEIPTS: {
    disabledEnvs: ['staging', 'prod'],
  },
};

export type FeatureName = $Keys<typeof featureDescs>;

// doing this weird declare-then-export so we can both infer the keys from the
// literal, AND specify that the values are of a specific shape
export const FEATURES: { [FeatureName]: FeatureInfo } = featureDescs;

/**
 *
 * @returns {?Array<Plan>} returns null if not available in this env, otherwise
 * a (possibly empty) array of enabled plans. if empty, enabled only for admin/stdlib
 * accounts (ie beta access)
 * @private
 */
export function availableSubscriptionLevels(f: FeatureName): ?Array<SubscriptionLevel> {
  const info = featureDescs[f];

  const currentEnv: Env = window.parsley_env;
  if (info.disabledEnvs && info.disabledEnvs.includes(currentEnv)) {
    return null;
  }

  return info.enabledSubscriptionLevels || Object.getOwnPropertyNames(levelDescs);
}

// returns true only if the feature is enabled for some (non-admin) plans
// check for featureIsSupported FIRST - that will return true for admin and stdlib
// accounts in certain cases where featureIsAvailable returns false!
export function featureIsAvailable(f: FeatureName): boolean {
  // nullity of this has a different meaning from enabledSubscriptionLevels in FeatureInfo
  // this is designed for ease of code usage, while enabledSubscriptionLevels is designed
  // for ease of declaration and manual inspection
  const availableLevels = availableSubscriptionLevels(f);

  return Boolean(availableLevels && availableLevels.length !== 0);
}
