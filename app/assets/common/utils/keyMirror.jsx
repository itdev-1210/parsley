import _ from 'lodash';

export default function(vals) {
  return _.assign({}, ...vals.map(
      v => ({ [v]: v })
  ));
}
