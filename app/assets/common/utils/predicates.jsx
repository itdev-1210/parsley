import Immutable from 'immutable';

export const keyIn = (...keys) => {
  const keySet = Immutable.Set(keys);
  return (value, key) => keySet.has(key)
}