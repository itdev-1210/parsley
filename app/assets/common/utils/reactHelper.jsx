import React, { Component } from 'react';

export function changeNewLineToBreak(text) {
  return text ? text.split("\n").map((item, key) => <span key={key}>{item}<br/></span>) : null;
}
