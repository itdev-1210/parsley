// @flow
import type { Env } from './constants';

declare function gtag(command: string, ...any): void;

export function gaInit() {
  if (!window.gtag) {
    console.log('on dev server, inserting dummy Google Analytics code');
    window.gtag = function() { };
  }

  const currentEnv: Env = window.parsley_env;
  let appName = 'App';
  if (currentEnv !== 'prod') {
    appName += ` (${currentEnv})`;
  }

  gtag('set', {
    appName,
    appVersion: GIT_ID,
  });
}

export function newEvent(eventName: string, eventParams: Object) {
  gtag('event', eventName, eventParams);
}

export function newScreen(screenName: string) {
  newEvent('screen_view', { screenName });
}

/*
 * Copied body of capterra tag from https://www.capterra.com/vp/vendors/conversion_tracking
 */
export function reportCapterraConversion() {
/* eslint-disable */

  var capterra_vkey = '6a01df281880cca228b4eb4004b2ea84',
      capterra_vid = '2115216',
      capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');

  (function() {
    var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
    ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
    // $FlowFixMe
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
  })();
/* eslint-enable */
}
