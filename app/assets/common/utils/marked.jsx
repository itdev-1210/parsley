import originalMarked from 'marked';

var renderer = new originalMarked.Renderer();

renderer.link = (href, title, text) =>
    `<a href=${href} title=${title} target="_blank">${text}</a>`;

export default function (rawText, options) {
  return originalMarked(rawText, {
    renderer,
    sanitize: true,
    ...options
  });
}