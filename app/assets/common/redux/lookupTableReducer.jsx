// @flow

type Action<ActionType> = {
  type: ActionType,
  content: any,
}

type ReducerFunction<State, ActionTypes> = (State, Action<ActionTypes>) => State;

export default function lookupTableReducer<State, ActionTypes>(
    initialState: State, actionsMap: { [ActionTypes]: ReducerFunction<State, ActionTypes> },
): ReducerFunction<State, ActionTypes> {
  return (state=initialState, action) => {
    const reduceFn = actionsMap[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
  };
}