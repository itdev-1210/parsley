// @flow

// type annotation explicitly disallows null, should catch bad use of keyMirror?
export const ERROR_TYPES: Array<string> = [];
