// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

export const actionTypes = keyMirror([
  'STRIPE_INFO_RECEIVED', 'STRIPE_INFO_REQERROR',
]);

type InfoMembers =
    | 'products'
    | 'coupons'
    | 'publishableKey'
    | 'summary'

type StripePlan = {
  id: string,
  active: boolean,
  price: number,
  trialPeriodDays: ?number,
}
type StripeProduct = {
  id: string,
  subscriptionLevel: string,
  contactUs: boolean,
  active: boolean,
  name: string,
  description: ?string,
  plans: Array<StripePlan>,
}
type ProductsResponse = Array<StripeProduct>

export const actionCreators = {
  received(name: InfoMembers, info: ProductsResponse | $FlowTODO) {
    return {
      type: actionTypes.STRIPE_INFO_RECEIVED,
      content: { name, info },
    };
  },

  failure(error: Error) {
    return {
      type: actionTypes.STRIPE_INFO_REQERROR,
      content: { error },
    };
  },
};

const actionsMap = {
  [actionTypes.STRIPE_INFO_RECEIVED]: (state, action) => state.set(action.content.name, Immutable.fromJS(action.content.info)),
};

let initialState = Immutable.Map();

export default lookupTableReducer(initialState, actionsMap);
