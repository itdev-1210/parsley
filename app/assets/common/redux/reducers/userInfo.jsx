// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

export const actionTypes = keyMirror([
  'USER_RECEIVED', 'USER_REQERROR',
  'USER_SAVE_ERROR', 'USER_SAVED',
  'LOGIN_CHECK', 'LOGIN_CHECK_ERROR',
]);
export const actionCreators = {
  received(info: $FlowTODO) {
    return {
      type: actionTypes.USER_RECEIVED,
      content: info,
    };
  },

  failure(error: Error) {
    return {
      type: actionTypes.USER_REQERROR,
      content: {
        error,
      },
    };
  },

  saved() {
    return {
      type: actionTypes.USER_SAVED,
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.USER_SAVE_ERROR,
      content: {
        error: error,
      }
    };
  },

  loginChecked(loggedIn: boolean) {
    return {
      type: actionTypes.LOGIN_CHECK,
      content: loggedIn,
    };
  },

  loginCheckError(error: Error) {
    return {
      type: actionTypes.LOGIN_CHECK_ERROR,
      content: {
        error,
      },
    };
  },
};
// doing mutation here to prevent cyclic dependency
ERROR_TYPES.push(
    actionTypes.USER_REQERROR,
    actionTypes.USER_SAVE_ERROR,
    actionTypes.LOGIN_CHECK_ERROR,
);

let initialState = null;

const actionsMap = {
  // $FlowFixMe: fromJS() weirdness
  [actionTypes.USER_RECEIVED]: (state, action) => (Immutable.fromJS(action.content): Immutable.Map<string, $FlowTODO>)
      .set('loggedIn', true),
  [actionTypes.LOGIN_CHECK]: (state, action) => (state || Immutable.Map()).set('loggedIn', action.content),
};

export default lookupTableReducer(initialState, actionsMap);
