// @flow
import React, { Component, type Node } from 'react';

import { Row, Col } from 'react-bootstrap';
import Radio from 'common/ui-components/Radio';

type Props = {
  planSlug: string,
  planName: string,
  planPrice: number,

  children: Node,
  selectable: boolean,
} & $FlowTODO; // redux-form props
export default class PlanOption extends Component<Props> {

  render() {

    const { planSlug, planName, planPrice, children, selectable, ...selectedPlan } = this.props;

    return <div className="plan-option panel panel-default" onClick={selectable ? () => selectedPlan.onChange(planSlug) : undefined}>
      <div className="panel-heading">
        <Row>
          <Col md={1}>
            { selectable ? <Radio
              checked={selectedPlan.value === planSlug}
              onChange={selectedPlan.onChange}
              name="planRadios"
              id={'planRadios' + planSlug}
              value={planSlug} /> : null }
          </Col>
          <Col md={5}><h2 className="panel-title">{planName}</h2></Col>
          <Col md={6}><h4>
            { selectable ?
              <span className="label label-default pull-right">{planPrice > 0 ? '$' + planPrice + '/month' : 'Free'}</span> :
              <span className="label label-default pull-right">In beta: please contact us <br />at beta@parsleycooks.com</span> }
          </h4></Col>
        </Row>
      </div>
      <div className="panel-body">
        {children}
      </div>
    </div>;
  }

}
