// @flow
import React, { Component } from "react";
import type { Component as ComponentType } from 'react';
import { withRouter } from 'react-router';


type Props = {
  baseElemClass: ComponentType<*>,
  path: string,
  router: any, // react-router router
  currentLocation: any, // react-router location
}

@withRouter
export default class RoutableLink extends Component<Props> {

  render () {
    const {
        router,
        path,
        // using capital letter to prevent JSX parser from treating it as a literal string
        baseElemClass: BaseElemClass,
        ...rest
    } = this.props;
    const onClick = (event, eventKey) => {
      router.push(path);
      event.preventDefault();
    };

    // $FlowFixMe flow doesn't like use of a component class value
    return <BaseElemClass
      {...rest}
      active={router.isActive(path)}
      href={path}
      onClick={onClick}
    />;
  }
}
