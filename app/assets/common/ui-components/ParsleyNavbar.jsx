// @flow
import React, { Component, type Node } from 'react';

import { Navbar }
  from 'react-bootstrap';

export default class ParsleyNavbar extends Component<{ children?: Node }> {
  render () {
    const brandImage = serverRoutes.controllers.Assets.versioned(
        'images/logo_new.png',
    ).url;

    const {
      children,
    } = this.props;

    return <header>
      <div className="build-id">{GIT_ID}</div>
      <Navbar fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <img src={brandImage}/>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          {children}
        </Navbar.Collapse>
      </Navbar>
    </header>;
  }
}
