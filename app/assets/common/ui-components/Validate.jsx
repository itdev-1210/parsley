// @flow
import React, { Component } from 'react';
import type { Node } from 'react';
import _ from 'lodash';
import { InputGroup, Overlay, Tooltip } from 'react-bootstrap';
import classnames from 'classnames'

export type ReduxFormProps = {
  // from redux-form
  valid: boolean,
  touched: boolean,
  error: ?string,
}

type Props = ReduxFormProps & {
  name: string,
  className?: string,
  inModal?: boolean,

  children: Node,

  // for use if there's not enough space to visually show the error message nicely without a tooltip
  compact?: boolean,
}

// specifically built to take the redux form field props
export default class Validate extends Component<Props> {
  render() {
    let {
        name,
        valid, touched,

        compact = false,

        error,
        inModal,
        className,
        children, ...rest
    } = this.props;

    let errorDisplay = null;
    if (touched && !valid && error) {
      const tooltipClassName = classnames('error-tooltip', inModal ? 'modal-validate' : null);
      if (compact) {
        errorDisplay = <Overlay
            placement="top"
            show
            target={this}
        >
          <Tooltip id={name + '-error'} className={tooltipClassName}>{error}</Tooltip>
        </Overlay>;
      } else {
        errorDisplay = <Overlay
            placement="bottom"
            show
            target={this}
        >
          <Tooltip id={name + '-error'} className={tooltipClassName}>{error}</Tooltip>
        </Overlay>;
      }
    }

    className = classnames(className,
        !touched ? null
            : valid ? 'has-success'
            : 'has-error',
    );

    const directPassthrough = _.pick(rest, ['onMouseOver', 'onMouseOut']);

    return <InputGroup
        className={className} {...directPassthrough}
      >
        { children }
        { errorDisplay }
      </InputGroup>;
  }
}
