import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
chai.use(chaiEnzyme());

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import LoadingSpinner from './LoadingSpinner';

describe('<LoadingSpinner/>', function () {
  it('should contain a text element', function () {
    const wrapper = shallow(<LoadingSpinner />);
    expect(wrapper).to.have.exactly(1).descendants('h4');
  });

  it('should show loading text', function () {
    const wrapper = shallow(<LoadingSpinner />);
    expect(wrapper).to.contain('Loading...');
  });
});
