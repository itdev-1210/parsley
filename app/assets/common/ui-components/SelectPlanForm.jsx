// @flow

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';

import { Grid, Row, Col, Button } from 'react-bootstrap';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import PlanOption from './PlanOption';

function defaultPlanId(plans) {
  if (!plans) return null;
  const defaultPlan = plans.first();
  return defaultPlan && defaultPlan.get('id');
}

@reduxForm(
  {
    form: 'selectPlan',
    fields: ['plan'],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {

    const { userInfo, stripeProducts } = ownProps;

    let activeStripePlans = null;
    if (stripeProducts) {
      // if a user is on an product that we're not offering to new customers
      // let them stay on it
      const activeStripeProducts = stripeProducts.filter(prod =>
          prod.has('subscriptionLevel') &&
          (prod.get('active') || prod.get('subscriptionLevel') === userInfo.get('subscriptionLevel'))
      );

      const userCurrentPlan = userInfo.get('plan');

      activeStripePlans = activeStripeProducts.map(prod => {
        const allPlans = prod.get('plans');
        const preferredPlan =
            // if a user is on an old payment plan (e.g. different price) for the same
            // product, let/make them stay on it
            allPlans.find(plan => plan.get('id') === userCurrentPlan) ||
            allPlans.find(plan => plan.get('active'));

        return prod.delete('plans').merge(preferredPlan);
      // drop products that didn't have any eligible plans
      }).filter(plan => !plan.contains('price'));
    }

    return {
      initialValues: {
        'plan': state.getIn(['userInfo', 'plan']) || defaultPlanId(activeStripePlans),
      },
      stripePlans: activeStripePlans,
      userInfo,
      mainInfoLoaded: Boolean(userInfo) && Boolean(activeStripePlans),
    };
  },
)
export default class SelectPlanForm extends Component<$FlowTODO> {

  render() {

    const selectedPlan = this.props.fields.plan;
    const {
      handleSubmit, submitting,
      stripePlans, inModal, onSelect,
      mainInfoLoaded,
    } = this.props;

    if (!mainInfoLoaded) {
      return <Grid fluid><LoadingSpinner /></Grid>;
    }

    const colAttrs = !inModal ? { xs: 12, md: 6 } : { xs: 12 };
    return <Grid fluid>
      <Row>
        { !inModal ? <Col xs={12} md={2} /> : null }
        <Col {...colAttrs}>
          <Button
            className="pull-right select-plan-top-button"
            disabled={submitting}
            onClick={handleSubmit(onSelect)}
            >
            {inModal && submitting ? 'Confirming' : inModal ? 'Confirm' : 'Continue'}
          </Button>
          <h2>Select a Plan</h2>

          { stripePlans
            .map((stripePlan) =>
              <PlanOption
                {...selectedPlan}
                key={stripePlan.get('id')}
                selectable={!stripePlan.get('contact_us')}
                planName={stripePlan.get('name')}
                planSlug={stripePlan.get('id')}
                planPrice={stripePlan.get('price') / 100.0}>
                { /* TODO: sanitize HTML (https://www.npmjs.com/package/sanitize-html ?) */ }
                <div dangerouslySetInnerHTML={({ __html: stripePlan.get('description') })} />
              </PlanOption>) }

          <Button
            className="pull-right"
            disabled={submitting}
            onClick={handleSubmit(onSelect)}
            >
            {inModal && submitting ? 'Confirming' : inModal ? 'Confirm' : 'Continue'}
          </Button>
          <br /><br /><br />
        </Col>
        { !inModal ? <Col xs={12} md={4} /> : null }
      </Row>
    </Grid>;
  }
}
