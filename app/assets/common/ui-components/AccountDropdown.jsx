// @flow
import React, { Component, type Node } from 'react';
import Immutable from 'immutable';
import { MenuItem, NavDropdown } from 'react-bootstrap';

type Props = {
  userInfo: ?Immutable.Map<string, $FlowTODO>,
  children?: Node,
};

export default class AccountDropdown extends Component<Props> {
  render() {
    const { userInfo, children } = this.props;

    let dropdownTitle;
    if (!userInfo) {
      dropdownTitle = 'loading';
    } else {
      const userName = userInfo.get('longName');
      const businessName = userInfo.get('businessName');
      dropdownTitle = <div className="nav-dropdown-title">
        <div className="nav-dropdown-username">{userName}</div><br/>
        <div className="nav-dropdown-business-name">{businessName || 'Unnamed Account'}</div>
      </div>;
    }

    let switchAccountButton;
    // $FlowFixMe: only with records can we assert member existence/value
    if (userInfo && userInfo.get('allAccounts').size > 1) {
      switchAccountButton = [
        <MenuItem key="switch-account" id="switch-account-button"
                  href={global.serverRoutes.controllers.Auth.selectAccountPage().url}
        >
          Switch Account
        </MenuItem>,
      ];
    }

    return <NavDropdown title={dropdownTitle}
                        className="account-dropdown">
      {switchAccountButton}
      {children}
      <MenuItem key="sign-out" id="sign-out-button"
                href={global.serverRoutes.controllers.Auth.signOut().url}
      >
        Sign Out
      </MenuItem>
    </NavDropdown>;
  }
}
