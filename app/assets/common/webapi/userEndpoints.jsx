// @flow
import { transformErrorValue, addSideEffect } from 'common/utils/promises';

import { actionCreators } from '../redux/reducers/userInfo';
import { get, put, postMultipart, getApiErrorMsg, rawAPIFetch, } from './utils';
import type { RequestContent } from './utils';
import { Collection } from 'immutable';
import { jsObjectFetch } from 'common/webapi/utils';

export function getUserInfo() {
  const { reduxStore } = window;
  return get('/me')
      .then(
          user => {
            const fixedInfo = {
              ...user,
              shortName: user.firstName,
              longName: user.fullName || [user.firstName, user.lastName].join(' '),
            };
            reduxStore.dispatch(actionCreators.received(fixedInfo));
            return Promise.resolve(fixedInfo);
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.failure(error))))
      );
}

export function saveUserInfo(newInfo: Collection<*, *>) {
  const { reduxStore } = window;
  return put('/me', newInfo)
      .then(
          success => reduxStore.dispatch(actionCreators.saved()),
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.saveError(error))
          ))
      );
}

export type SubtractInventoryPreference =
    | 'subtract-none'
    | 'subtract-current-inventory'
    | 'subtract-latest-inventory';
export function updateSubtractInventoryPreference(subtractInventoryPreference: SubtractInventoryPreference) {
  const { reduxStore } = window;
  return put('/me/subtractInventory', { subtractInventoryPreference })
    .then(
      success => reduxStore.dispatch(actionCreators.saved()),
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(actionCreators.saveError(error)),
      )),
    );
}

export function saveLogo(newLogo: FormData) {
  const { reduxStore } = window;
  return postMultipart('/logo', newLogo)
      .then(
          success => reduxStore.dispatch(actionCreators.saved()),
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.saveError(error))
          ))
      );
}

export function reactivatePlan(reactivateReq: RequestContent): Promise<void> {
  return jsObjectFetch('/plan/reactivate', 'POST', { content: reactivateReq })
    .then(
      success => Promise.resolve(),
      err => Promise.reject(getApiErrorMsg(err))
    );
}

export function signOut() {
  return rawAPIFetch('/auth/logout', 'GET');
}

export function checkLoggedIn() {
  const { reduxStore } = window;
  return get('/me/logged_in_check')
      .then(
          isLoggedIn => {
            reduxStore.dispatch(actionCreators.loginChecked(isLoggedIn));
            return isLoggedIn;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.saveError(error)),
          ))
      );
}

export function selectAccount(accountId: number) {
  return rawAPIFetch(`/auth/select_account/${accountId}`, 'POST');
}
