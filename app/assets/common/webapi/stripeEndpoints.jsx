// @flow
import { transformErrorValue, addSideEffect } from 'common/utils/promises';
import { post } from 'common/webapi/utils';

import { actionCreators } from '../redux/reducers/stripeInfo';
import { get } from './utils';

export function getStripeProducts() {
  const { reduxStore } = window;
  return get('/stripe/products')
      .then(
          plans => {
            reduxStore.dispatch(actionCreators.received('products', plans));
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.failure(error)))),
      );
}

export function getStripeCoupons() {
  const { reduxStore } = window;
  return get('/stripe/coupons')
      .then(
          coupons => {
            reduxStore.dispatch(actionCreators.received('coupons', coupons.data));
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.failure(error)))),
      );
}

export function getStripePublishableKey() {
  const { reduxStore } = window;
  return get('/stripe/key')
      .then(
          publishableKey => {
            reduxStore.dispatch(actionCreators.received('publishableKey', publishableKey.data));
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(actionCreators.failure(error)))),
      );
}

export function getStripeSummary() {
  const { reduxStore } = window;
  return get('/stripe/summary')
    .then(
        summary => {
          reduxStore.dispatch(actionCreators.received('summary', summary));
        },
        transformErrorValue(addSideEffect(error =>
            reduxStore.dispatch(actionCreators.failure(error)))),
      );
}

export function getStripeProRationPreview(plan: string, proRationDate: number) {
  return get('/stripe/previewProRation/' + plan + '/' + proRationDate);
}

export function subscribe(plan: string, otherInfo: Object) {
  return post('/plan/subscribe', {
    ...otherInfo, plan,
  });
}

export function changePlan(plan: string, proRationDate: number) {
  return post('/plan/changePlan', {
    plan, proRationDate,
  });
}

export function unsubscribe() {
  return post('/plan/unsubscribe');
}

export function uncancel() {
  return post('/plan/uncancel');
}
