/* @flow */
import type { Collection } from 'immutable';
import { actionCreators as NotifyActions } from '../../app/reducers/notification';
import reduxStore from '../../app/store';
import { prettyErrorMsg } from '../utils/constants';
import { noticeError } from 'common/utils/errorAnalytics';

// fetch module output not captured; it's a global-namespace polyfill, (returns undefined)
// which this module needs available at load-time because of top-level (not in function)
// use of Headers()
export const apiRoot = '/api';

type ErrorBody = { error: string, readableErrors: mixed } | string;

export class BadResponseError extends Error {
  reduxFormErrors: mixed;

  constructor(resp: Response, body: ErrorBody) {
    // message will not be available in constructor, so store in variable for multiple uses
    const message = `${resp.status}:${resp.statusText}: ${
      (typeof body === 'string') ? body : JSON.stringify(
          body.error || body.readableErrors || body,
          undefined, 4,
      )
    }`;
    super(message);

    if (body.readableErrors) {
      this.reduxFormErrors = body.readableErrors;
    } else if (resp.status === 401) {
      // If the user has logged out (in another tab?) or his/her session has expired, reload to get redirected correctly.
      return location.reload();
    } else {
      noticeError('unfriendly API error ' + message);

      this.reduxFormErrors = { _error:
        'An error occurred, and we don\'t have a readable error message for you. ' +
        'Contact us at service@parsleycooks.com to let us know of the problem.'
      };
    }
  }
}

function checkValidJson(fetchResponse): Promise<any> {
  if (!/^application\/json/.test(fetchResponse.headers.get('content-type') || '')) {
    // we expect even error responses to contain valid JSON
    return fetchResponse.text().then(
        body => Promise.reject(new BadResponseError(fetchResponse, `plain string "${body}"`))
    );
  }

  return fetchResponse.json().then(
      json => fetchResponse.ok ?
          Promise.resolve(json) :
          Promise.reject(new BadResponseError(fetchResponse, json))
  );
}

const COMMON_FETCH_OPTIONS = {
  credentials: 'same-origin',
};

const COMMON_FETCH_HEADERS = new Headers({
  'Accept': 'application/json',
});

/**
 * "raw" means this doesn't add on the "/api" root to the url, and it doesn't convert
 * JSON for you
 */
export function rawAPIFetch(url: string, method: fetch.MethodType, content: ?(string | Blob)): Promise<any> {
  let headers = new Headers(COMMON_FETCH_HEADERS);
  if (content !== undefined) {
    headers.set('Content-Type', 'application/json');
  }

  let request = new Request(url,
      {
        method,
        body: content,
        headers,
        ...COMMON_FETCH_OPTIONS,
      });

  return fetch(request)
      .then(
          checkValidJson,
          // fetch() only rejects if the browser doesn't even try - perms or offline
          e => {
            const errorMsg = 'Cannot contact the server. You may be offline. Please check your internet connection';
            reduxStore.dispatch(NotifyActions.notify('', 'error', errorMsg));
            return Promise.reject(new Error(errorMsg));
          }
      );
}

export type RequestContent =
    | Collection<*, *>
    | Collection.Indexed<*>
    | { [string]: any }
    | Array<*>;

// All API fetch functions take URLs relative to apiRoot
export function jsObjectFetch(
    url: string, method: fetch.MethodType,
    opts: {
      content?: RequestContent,
      searchParams?: { [string]: string },
    } = {},
) {
  const {
    content,
    searchParams = {},
  } = opts;

  const structuredUrl = new URL(apiRoot + url, window.location.origin);
  for (const name of Object.getOwnPropertyNames(searchParams)) {
    structuredUrl.searchParams.append(name, searchParams[name]);
  }

  return rawAPIFetch(structuredUrl.toString(), method, JSON.stringify(content));
}

// returns Promise[object], where object comes from JSON of response
export function get(url: string, searchParams?: { [string]: string }) {
  return jsObjectFetch(url, 'GET', { searchParams });
}

// returns Promise[Integer], where the integer is the ID of the newly-
// created object (which all POST API endpoints should return)
export function post(url: string, content?: RequestContent): Promise<number> {
  return jsObjectFetch(url, 'POST', { content })
      .then(res => res.id);
}

// POST multipart form data
export function postMultipart(url: string, content: FormData): Promise<string> {
  return fetch(apiRoot + url, {
            method: 'POST',
            body: content,
            ...COMMON_FETCH_OPTIONS
      }).then(
          checkValidJson,
          // fetch() only rejects if the browser doesn't even try - perms or offline
          e => {
            const errorMsg = 'Cannot contact the server. You may be offline. Please check your internet connection';
            reduxStore.dispatch(NotifyActions.notify('', 'error', errorMsg));
            return Promise.reject(new Error(errorMsg));
          }
      );
}

// returns Promise[something]; all we care about is success/failure
export function put(url: string, content: RequestContent): Promise<null> {
  return jsObjectFetch(url, 'PUT', { content })
      .then(res => null);
}

// returns Promise[something]; all we care about is success/failure
// delete is unfortunately a reserved keyword
export function del(url: string): Promise<null> {
  return jsObjectFetch(url, 'DELETE')
      .then(res => null);
}

export function getApiErrorMsg(err: any) { // $FlowTODO type the errors a Promise can throw
  return err.reduxFormErrors ? err.reduxFormErrors._error: prettyErrorMsg;
}