import { types } from '../actions';
import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  useSignUpForm: true // as opposed to logging in with an existing account
});

export default function(oldState, action) {
  if (typeof(oldState) === 'undefined') {
    return initialState;
  }

  switch (action.type) {
    case types.USE_SIGN_UP_FORM: return oldState.set('useSignUpForm', true);
      break;
    case types.USE_LOG_IN_FORM: return oldState.set('useSignUpForm', false);
      break;
    default: return oldState;
  }
}
