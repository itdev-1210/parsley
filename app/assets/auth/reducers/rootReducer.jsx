import { combineReducers } from 'redux-immutablejs';
import { reducer as form } from 'redux-form';
import uiState from './uiState';
import pendingInvitation from './pendingInvitation';
import userInfo from 'common/redux/reducers/userInfo';

export default combineReducers({
  form,
  uiState,
  userInfo,
  pendingInvitation,
});