import Immutable from 'immutable';

import { types } from '../actions';

const initialState = Immutable.fromJS({
  error: null,
  invitation: Immutable.Map(),
  upstreamInvitation: Immutable.Map(),
});

export default function(oldState, action) {
  if (typeof(oldState) === 'undefined') {
    return initialState;
  }
  switch (action.type) {
    case types.FETCH_INVITATION_SUCCESS:
      return oldState
        .set('error', null)
        .set('invitation', Immutable.fromJS(action.data));
    case types.FETCH_INVITATION_ERROR:
      return oldState
        .set('invitation', null)
        .set('error', action.data);
    case types.ACCEPT_INVITATION_ERROR:
      return oldState
        .set('error', action.data);
    case types.FETCH_UPSTREAM_INVITATION_SUCCESS:
      return oldState
        .set('error', null)
        .set('upstreamInvitation', Immutable.fromJS(action.data));
    case types.FETCH_UPSTREAM_INVITATION_ERROR:
      return oldState
        .set('upstreamInvitation', null)
        .set('error', action.data);
    case types.ACCEPT_UPSTREAM_INVITATION_ERROR:
      return oldState
        .set('error', action.data);
    default: return oldState;
  }
  
}