// @flow

import React from 'react';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import _ from 'lodash';

import { Button, ButtonToolbar, Col, Grid, Nav, PageHeader, Row, } from 'react-bootstrap';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import { checkUpstreamInvitation, completeUpstreamInvitation, } from '../utils/invitationEndpoints';

import isEmail from 'validator/lib/isEmail';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';
import { checkLoggedIn, getUserInfo } from 'common/webapi/userEndpoints';
import AuthInput from './AuthInput';
import AccountDropdown from 'common/ui-components/AccountDropdown';

const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

type FormValues = {
  userInfo: {
    firstName: string,
    lastName: string,
    phonenumber: string,
  },
  accountInfo: {
    businessName: string,
  },
  passwordSetupInfo: {
    email: string,
    password: string,
    confirmPassword: string,
  },
  useSocialAccount: boolean,
}
function validate(loggedIn: boolean, values: $Shape<FormValues>) {
  const errors = {};

  errors.accountInfo = {};
  // Phone number is required in both standard and signup-through-a-social-provider accounts
  if (!values.userInfo.phonenumber) {
    errors.userInfo.phonenumber = 'Required';
  } else try {
    if (!phoneUtil.isValidNumber(phoneUtil.parse(values.userInfo.phonenumber, 'US'))) {
      errors.accountInfo.phonenumber = 'Invalid phone number';
    }
  } catch (e) {
    errors.userInfo.phonenumber = 'Invalid phone number';
  }


  if (!loggedIn) {
    errors.userInfo = {};
    errors.passwordSetupInfo = {};

    if (!values.userInfo.firstName) {
      errors.userInfo.firstName = 'Required';
    }
    if (!values.userInfo.lastName) {
      errors.userInfo.lastName = 'Required';
    }

    if (!values.passwordSetupInfo.email) {
      errors.passwordSetupInfo.email = 'Required';
    } else if (!isEmail(values.passwordSetupInfo.email)) {
      errors.passwordSetupInfo.email = 'Invalid email address';
    }

    if (!values.passwordSetupInfo.password) {
      errors.passwordSetupInfo.password = 'Required';
    }

    if (!values.passwordSetupInfo.confirmPassword) {
      errors.passwordSetupInfo.confirmPassword = 'Required (measure twice, cut once!)';
    } else if (values.passwordSetupInfo.password !== values.passwordSetupInfo.confirmPassword) {
      errors.passwordSetupInfo.confirmPassword = 'Password does not match';
    }
  }

  return errors;
}

type Props = $FlowTODO;

function initialValueFromState(state, field) {
  return state.getIn(['userInfo', field]) || state.getIn(['pendingInvitation', field]);
}

const inputColumnWidths = {
  xs: 12,
};

const nameInputColumnWidths = {
  xs: 12,
  md: 6,
};

@withRouter
@reduxForm(
  {
    form: 'confirm-upstream-invitation',
    fields: [
      'userInfo.firstName', 'userInfo.lastName', 'userInfo.phonenumber',
      'accountInfo.businessName',
      'passwordSetupInfo.email', 'passwordSetupInfo.password', 'passwordSetupInfo.confirmPassword',
      'token.id', 'token.secret',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
  },
  state => {
    const upstreamInvitation = state.getIn(['pendingInvitation', 'upstreamInvitation']);
    return {
      validate: _.partial(validate, Boolean(state.getIn(['userInfo', 'loggedIn']))),

      invitation: upstreamInvitation,
      invitationError: state.getIn(['pendingInvitation', 'error']),
      userInfo: state.get('userInfo'),
      initialValues: {
        accountInfo: {
          businessName: upstreamInvitation.get('businessName'),
        },
        userInfo: {
          firstName: initialValueFromState(state, 'firstName'),
          lastName: initialValueFromState(state, 'lastName'),
          phonenumber: initialValueFromState(state, 'phonenumber'),
        },
        passwordSetupInfo: {
          email: upstreamInvitation.get('inviteeEmail'),
        },
      },
    };
  },
)
export default class ConfirmUpstreamInvitation extends React.Component<Props> {

  async componentWillMount() {
    const { id, secret } = this.props.params;
    checkUpstreamInvitation(id, secret);
    if (await checkLoggedIn()) {
      getUserInfo();
    }
  }

  submit(values: FormValues) {
    const { userInfo } = this.props;
    const { id, secret } = this.props.params;
    const processedValues = {
      ...values,
      token: {
        id: Number(id),
        secret,
      },
    };
    delete processedValues.passwordSetupInfo.confirmPassword;
    if (userInfo.get('loggedIn')) {
      delete processedValues.passwordSetupInfo;
      delete processedValues.userInfo;
    }
    return completeUpstreamInvitation(processedValues, () => {
      console.log('Upstream Invitation Accepted');
      window.location = '/';
    });
  }

  render() {
    const { userInfo, handleSubmit, submitting, invitation, invitationError, error: globalError } = this.props;
    const {
      userInfo: {
        firstName, lastName,
        phonenumber,
      },
      accountInfo: {
        businessName,
      },
      passwordSetupInfo: {
        email, password, confirmPassword,
      },
    } = this.props.fields;

    let loading = false;
    if (!invitation || !userInfo) {
      loading = true;
    } else if (userInfo.get('loggedIn') && !userInfo.get('logoUrl')) { // got loginCheck response, still waiting on getUserInfo
      loading = true;
    }

    let formContent;
    if (loading) {
      formContent = <LoadingSpinner />;
    } else if (userInfo.get('loggedIn')) {
      formContent = <Row>
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Company Name" bsSize="large" type="text" {...businessName} />
      </Row>;
    } else {
      // TODO: add "already have an account"? link
      formContent = <Row>
        <AuthInput wrapperComponent={Col} wrapperProps={nameInputColumnWidths}
                   placeholder="First Name" bsSize="large" type="text" {...firstName} />
        <AuthInput wrapperComponent={Col} wrapperProps={nameInputColumnWidths}
                   placeholder="Last Name" bsSize="large" type="text" {...lastName} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Phone Number" bsSize="large" type="text" {...phonenumber} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Company Name" bsSize="large" type="text" {...businessName} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Email Address" bsSize="large" type="text" {...email} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Password" bsSize="large" type="password" {...password} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Confirm Password" bsSize="large" type="password" {...confirmPassword} />
      </Row>;
    }

    return <Grid componentClass="form" fluid>
      <ParsleyNavbar>
        {!loading && userInfo.get('loggedIn') ?
            <Nav navbar pullRight>
              <AccountDropdown userInfo={userInfo}/>
            </Nav> :
            null
        }
      </ParsleyNavbar>
      <div className="auth-container">
        <PageHeader>
          Create a Location for {(invitation && invitation.get('inviterName')) || 'Unnamed Account'}
        </PageHeader>
        {invitationError || globalError ?
          <Row className="alert alert-danger"><Col>{invitationError || globalError}</Col></Row>
          : null
        }
        <Row>
          <Col xs={12} style={{ textAlign: 'center', paddingBottom: '20px' }}>
            {formContent}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <ButtonToolbar type="radio" className="pull-right">
              <Button
                type="button" bsStyle="default"
                onClick={() => window.location = '/'}
                disabled={submitting}
                >
                Cancel
              </Button>
              <Button
                type="submit" bsStyle="primary"
                onClick={handleSubmit(this.submit.bind(this))}
                disabled={submitting || invitationError || loading}
                >
                {submitting ? 'Accepting' : 'Accept'}
              </Button>
            </ButtonToolbar>
          </Col>
        </Row>
      </div>
    </Grid>;
  }
}