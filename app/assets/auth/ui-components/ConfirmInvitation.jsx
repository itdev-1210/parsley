// @flow
import React from 'react';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import _ from 'lodash';

import isEmail from 'validator/lib/isEmail';

import {
  Grid, PageHeader, FormControl,
  Row, Col, Button, ButtonToolbar, Nav,
} from 'react-bootstrap';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';

import {
  checkInvitation, completeInvitation,
} from '../utils/invitationEndpoints';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';
import { checkLoggedIn, getUserInfo } from 'common/webapi/userEndpoints';
import AuthInput from './AuthInput';
import AccountDropdown from 'common/ui-components/AccountDropdown';

type FormValues = {
  userInfo: {
    firstName: string,
    lastName: string,
  },
  passwordSetupInfo: {
    email: string,
    password: string,
    confirmPassword: string,
  },
  useSocialAccount: boolean,
}
function validate(loggedIn: boolean, values: $Shape<FormValues>) {
  let errors = {};

  if (!loggedIn) {
    errors.userInfo = {};
    errors.passwordSetupInfo = {};

    if (!values.userInfo.firstName) {
      errors.userInfo.firstName = 'Required';
    }
    if (!values.userInfo.lastName) {
      errors.userInfo.lastName = 'Required';
    }

    if (!values.passwordSetupInfo.email) {
      errors.passwordSetupInfo.email = 'Required';
    } else if (!isEmail(values.passwordSetupInfo.email)) {
      errors.passwordSetupInfo.email = 'Invalid email address';
    }

    if (!values.passwordSetupInfo.password) {
      errors.passwordSetupInfo.password = 'Required';
    }

    if (!values.passwordSetupInfo.confirmPassword) {
      errors.passwordSetupInfo.confirmPassword = 'Required (measure twice, cut once!)';
    } else if (values.passwordSetupInfo.password !== values.passwordSetupInfo.confirmPassword) {
      errors.passwordSetupInfo.confirmPassword = 'Password does not match';
    }
  }

  return errors;
}

type Props = $FlowTODO;

function initialValueFromState(state, field) {
  return state.getIn(['userInfo', field]) || state.getIn(['pendingInvitation', field]);
}

const inputColumnWidths = {
  xs: 12,
};

const nameInputColumnWidths = {
  xs: 12,
  md: 6,
};

@withRouter
@reduxForm(
  {
    form: 'confirm-invitation',
    fields: [
      'userInfo.firstName', 'userInfo.lastName', 'userInfo.phonenumber',
      'passwordSetupInfo.email', 'passwordSetupInfo.password', 'passwordSetupInfo.confirmPassword',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  state => ({
    validate: _.partial(validate, Boolean(state.getIn(['userInfo', 'loggedIn']))),

    invitation: state.getIn(['pendingInvitation', 'invitation']),
    invitationError: state.getIn(['pendingInvitation', 'error']),
    userInfo: state.get('userInfo'),
    initialValues: {
      userInfo: {
        firstName: initialValueFromState(state, 'lastName'),
        lastName: state.getIn(['pendingInvitation', 'invitation', 'lastName']),
        phonenumber: initialValueFromState(state, 'phonenumber'),
      },
      passwordSetupInfo: {
        email: initialValueFromState(state, 'inviteeEmail'),
      },
    },
  }),
)
export default class ConfirmInvitation extends React.Component<Props> {

  async componentWillMount() {

    const { id, secret } = this.props.params;
    checkInvitation(id, secret);
    if (await checkLoggedIn()) {
      getUserInfo();
    }
  }

  submit(values: FormValues) {
    const { userInfo } = this.props;
    const { id, secret } = this.props.params;
    const valuesToSubmit = {
      ...values,
      token: {
        id: Number(id),
        secret,
      },
    };
    delete valuesToSubmit.passwordSetupInfo.confirmPassword;
    if (userInfo.get('loggedIn')) {
      delete valuesToSubmit.passwordSetupInfo;
      delete valuesToSubmit.userInfo;
    }

    return completeInvitation(valuesToSubmit, () => {
      console.log('Invitation accepted');
      window.location = '/';
    });
  }
  render() {
    const { userInfo, handleSubmit, submitting, invitation, invitationError, error: globalError } = this.props;
    const {
      userInfo: {
        firstName, lastName, phonenumber,
      },
      passwordSetupInfo: {
        email, password, confirmPassword,
      },
    } = this.props.fields;

    let loading = false;
    if (!invitation || !userInfo) {
      loading = true;
    } else if (userInfo.get('loggedIn') && !userInfo.get('logoUrl')) { // got loginCheck response, still waiting on getUserInfo
      loading = true;
    }

    let formContent;
    if (loading) {
      formContent = <LoadingSpinner />;
    } else if (userInfo.get('loggedIn')) {
      formContent = null; // no information required; just a yes-no response
    } else {
      // TODO: add "already have an account"? link
      formContent = <Row>
        <AuthInput wrapperComponent={Col} wrapperProps={nameInputColumnWidths}
                   placeholder="First Name" bsSize="large" type="text" {...firstName} />
        <AuthInput wrapperComponent={Col} wrapperProps={nameInputColumnWidths}
                   placeholder="Last Name" bsSize="large" type="text" {...lastName} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Phone Number" bsSize="large" type="text" {...phonenumber} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Email Address" bsSize="large" type="text" {...email} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Password" bsSize="large" type="password" {...password} />
        <AuthInput wrapperComponent={Col} wrapperProps={inputColumnWidths}
                   placeholder="Confirm Password" bsSize="large" type="password" {...confirmPassword} />
      </Row>;
    }

    return <Grid componentClass="form" fluid>
      <ParsleyNavbar>
        {!loading && userInfo.get('loggedIn') ?
            <Nav navbar pullRight>
              <AccountDropdown userInfo={userInfo}/>
            </Nav> :
            null
        }
      </ParsleyNavbar>
      <div className="auth-container">
        <PageHeader>
          Confirm Invitation to {(invitation && invitation.get('inviterName')) || 'Unnamed Account'}
        </PageHeader>
        {
          invitationError || globalError ?
            <Row className="alert alert-danger"><Col>{invitationError || globalError}</Col></Row>
            : null
        }
        <Row>
          <Col xs={12} style={{ textAlign: 'center', paddingBottom: '20px' }}>
            {formContent}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <ButtonToolbar type="radio" className="pull-right">
              <Button
                type="button" bsStyle="default"
                onClick={() => window.location = '/'}
                disabled={submitting}
                >
                Cancel
              </Button>
              <Button
                type="submit" bsStyle="primary"
                onClick={handleSubmit(this.submit.bind(this))}
                disabled={submitting || invitationError || loading}
                >
                {submitting ? 'Accepting' : 'Accept'}
              </Button>
            </ButtonToolbar>
          </Col>
        </Row>
      </div>
    </Grid>;
  }
}