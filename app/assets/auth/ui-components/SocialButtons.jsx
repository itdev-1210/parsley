// @flow
import React, { Component } from 'react';

const socialProviders = {
  google: '/auth/social/google',
  linkedin: '/auth/social/linkedin',
};

type Props = {
  redirect: ?URL,
  submit?: (SyntheticEvent<*>, string) => void,
}

export default class SocialButtons extends Component<Props> {

  render() {
    const { redirect, submit } = this.props;

    function handleClick(provider: string, evt) {

      let url = socialProviders[provider];
      if (redirect) url += `?redirect=${redirect.toString()}`;

      if (typeof submit === 'function') {
        submit(evt, url);
      } else {
        location.href = url;
      }
    }

    return <div className="social-auth-buttons">
      using your <span onClick={evt => handleClick('google', evt)}>Google</span> or <span onClick={evt => handleClick('linkedin', evt)}>LinkedIn</span> account
    </div>;
  }
}