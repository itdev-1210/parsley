// @flow
import { connect } from 'react-redux';
import React, { Component } from 'react';
import _ from 'lodash';
import { Grid, ListGroup, ListGroupItem, Nav, PageHeader } from 'react-bootstrap';

import AccountDropdown from 'common/ui-components/AccountDropdown';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';
import marked from 'common/utils/marked';
import { getUserInfo } from 'common/webapi/userEndpoints';
import { getRedirectUrl } from '../utils/redirection';
import { selectAccountAndRedirect, type Account } from '../utils/selectAccount';
import { withRouter } from 'react-router';
import { PERMISSION_LEVELS } from 'common/utils/constants';

type Props = $FlowTODO;
@connect(state => ({
  userInfo: state.get('userInfo'),
}))
@withRouter
export default class SelectAccountPage extends Component<Props> {
  render() {
    const {
      userInfo,
      error: globalError,
      location,
    } = this.props;

    const globalErrorDisplay = globalError
        ? <div dangerouslySetInnerHTML={{ __html: marked(globalError) }} />
        : null;

    return <Grid fluid>
      <ParsleyNavbar>
        <Nav navbar pullRight>
          <AccountDropdown userInfo={userInfo} />
        </Nav>
      </ParsleyNavbar>
      <div className="auth-container">
        <PageHeader>Select Account</PageHeader>

        {globalErrorDisplay}

        <AccountList
            accounts={userInfo ? userInfo.get('allAccounts').toJS() : []}
            redirect={getRedirectUrl(location.query)}
        />
      </div>
    </Grid>;
  }

  componentWillMount() {
    getUserInfo();
  }
}

type AccountListProps = {
  accounts: Array<Account>,
  redirect: ?URL,
};
function AccountList(props: AccountListProps) {
  const { accounts, redirect } = props;

  let listItems = _.sortBy(accounts, a => a.accountId).map(a =>
      <ListGroupItem
          header={a.businessName || 'Unnamed Account'} key={a.accountId}
          onClick={() => selectAccountAndRedirect(a, redirect)}
      >
        {a.isAccountOwner ? 'Account Owner' : `${PERMISSION_LEVELS[a.permissionLevel]} User`}
      </ListGroupItem>,
  );

  // TODO: Allow user to add more accounts?
  return <ListGroup>{listItems}</ListGroup>;
}
