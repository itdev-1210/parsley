import React, { Component } from 'react';
import { Link, withRouter } from 'react-router';
import ReactPropTypes from 'prop-types';
import TextBetweenTwoLines from './TextBetweenTwoLines';
import { Button, Grid, PageHeader } from 'react-bootstrap';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';

@withRouter
export default class RootForm extends Component {
  static propTypes = {
    location: ReactPropTypes.object.isRequired,
    router: ReactPropTypes.object.isRequired,
  };

  render() {
    const {
      router, location,
    } = this.props;

    const isUserInviteConfirm = router.isActive('/auth/confirmuserinvite');
    const isSignupVariant = router.isActive('/auth/signup') || isUserInviteConfirm;
    // copied over from submitPasswordAuth.jsx

    let { redirect } = location.query;
    redirect = redirect && new URL(redirect, window.location);
    if (redirect && (redirect.origin !== window.location.origin)) {
      redirect = null;
    }

    const headerText = isSignupVariant
        ? isUserInviteConfirm ? 'Accept Invitation' : 'Start Your 14 Day Free Trial'
        : 'Log In';
    const footerButton = isSignupVariant
        // Copy over query params (esp. redirect)
        ? <Button
          style={{ width: '50%' }}
          componentClass={Link} bsStyle="primary"
          to={{ pathname: '/auth/login', query: location.query }}>
          Already have an account?
        </Button>
        : <Button
          style={{ width: '50%' }}
          componentClass={Link} bsStyle="primary"
          to={{ pathname: '/auth/signup', query: location.query }}>
          {'Don\'t Have an Account?'}
        </Button>;

    return (
      <Grid fluid>
        <ParsleyNavbar/>
        <div className="auth-container">
          <PageHeader>{headerText}</PageHeader>
          {this.props.children}
          <TextBetweenTwoLines label={isUserInviteConfirm ? 'OR' : isSignupVariant ? '' : 'OR'}/>
          {footerButton}
        </div>
      </Grid>
    );
  }
}
