// @flow
import React, { Component, type ComponentType } from 'react';
import Validate, { type ReduxFormProps } from 'common/ui-components/Validate';
import { FormControl } from 'react-bootstrap';

type Props = ReduxFormProps & {
  placeholder?: string,
  style?: Object,
  type?: string,
  wrapperComponent: ComponentType<{ style?: Object }>,
  wrapperProps: Object,
}
export default class AuthInput extends Component<Props> {
  static defaultProps = {
    placeholder: '',
    style: {},
    type: 'text',
    wrapperComponent: 'div',
    wrapperProps: {},
  };

  render() {
    const { style, type, placeholder, wrapperComponent: Wrapper, wrapperProps, ...reduxFormProps } = this.props;
    return <Wrapper style={style} {...wrapperProps} >
      <Validate className="auth-input" {...reduxFormProps}>
        <FormControl bsSize="large" placeholder={placeholder} type={type} {...reduxFormProps}/>
      </Validate>
    </Wrapper>;
  }
}