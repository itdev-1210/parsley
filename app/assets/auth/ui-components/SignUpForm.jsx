// @flow

import { newEvent, newScreen } from 'common/utils/analytics';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import _ from 'lodash';
import { reduxForm } from 'redux-form';
import isEmail from 'validator/lib/isEmail';
import { Button, ButtonToolbar, Form } from 'react-bootstrap';
import AuthInput from './AuthInput';
import { submitPasswordAuth } from '../utils/submitPasswordAuth';
import SocialButtons from './SocialButtons';
import TextBetweenTwoLines from './TextBetweenTwoLines';
import type { ReactRouterProps } from 'common/utils/routing';
import { getRedirectUrl } from '../utils/redirection';

// $FlowFixMe 'require' isn't supported by flow
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

type FormValues = {
  useSocialAccount: boolean,
  firstName: string,
  lastName: string,
  company: ?string,
  phonenumber: string,
  email: string,
  password: string,
  passwordConfirm: string,
}
export function validate(values: $Shape<FormValues>) {

  const errors = {};

  // Phone number is required in both standard and signup-through-a-social-provider accounts
  if (!values.phonenumber) {
    errors.phonenumber = 'Required';
  } else try {
    if (!phoneUtil.isValidNumber(phoneUtil.parse(values.phonenumber, 'US'))) {
      errors.phonenumber = 'Invalid phone number';
    }
  } catch (e) {
    errors.phonenumber = 'Invalid phone number';
  }

  // Further validations are only applied to standard accounts
  if (values.useSocialAccount) return errors;

  if (!values.firstName) {
    errors.firstName = 'Required';
  }
  if (!values.lastName) {
    errors.lastName = 'Required';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!isEmail(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  if (!values.passwordConfirm) {
    errors.passwordConfirm = 'Required (measure twice, cut once!)';
  } else if (values.passwordConfirm !== values.password) {
    errors.passwordConfirm = 'Password does not match';
  }

  return errors;
}

function submitStandardAccount(redirect, router, values) {
  const processedValues = _.omitBy(values, _.isEmpty); // don't send in empty-string first/last name
  return submitPasswordAuth('/auth/signup', processedValues, redirect, router);
}

function submitSocialAccount(url, values) {
  const structuredUrl = new URL(url, window.location.href);
  structuredUrl.searchParams.append('phonenumber', values.phonenumber);
  if (values.company)
    structuredUrl.searchParams.append('company', values.company);
  window.location = structuredUrl;
}

type Props = {
  fields: $FlowTODO,
  submitting: boolean,
  handleSubmit: Function => (SyntheticEvent<*> => void),
}

@reduxForm({
  form: 'login',
  fields: ['firstName', 'lastName', 'company', 'phonenumber', 'password', 'passwordConfirm', 'email', 'useSocialAccount'],
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  validate,
})
@withRouter
export default class SignUpForm extends Component<ReactRouterProps & Props> {
  static propTypes = {};
  componentWillMount() {
    newScreen('signup');
  }

  render() {

    const {
      fields: { firstName, lastName, company, phonenumber, password, passwordConfirm, email, useSocialAccount },
      submitting, handleSubmit, location, router,
    } = this.props;

    const redirect = getRedirectUrl(location.query);

    function createStandardAccount(evt) {
      evt.preventDefault();
      useSocialAccount.onChange(false);
      evt.persist();
      newEvent('sign_up', { method: 'password' });
      setTimeout(() => handleSubmit(submitStandardAccount.bind(this, redirect, router))(evt), 1);
    }

    function createSocialProvidedAccount(evt, url) {
      useSocialAccount.onChange(true);
      evt.persist();
      newEvent('sign_up', { method: 'social' });
      setTimeout(() => handleSubmit(submitSocialAccount.bind(this, url))(evt), 1);
    }

    return <Form submit={handleSubmit}>
      <AuthInput placeholder="First name" autoComplete="off" style={{ width: 'calc(50% - 6px)', display: 'inline-block', marginRight: '10px' }}
          {...firstName} />
      <AuthInput placeholder="Last name" autoComplete="off" style={{ width: 'calc(50% - 6px)', display: 'inline-block' }}
          {...lastName} />
      <AuthInput autoComplete="off" placeholder="Company name"
          {...company} />
      <AuthInput autoComplete="off" placeholder="Phone number"
          {...phonenumber} />
      <TextBetweenTwoLines label="AND"/>
      <AuthInput type="email" autoComplete="off" placeholder="Email address"
          {...email} />
      <AuthInput type="password" placeholder="Password"
          {...password} />
      <AuthInput type="password" placeholder="Confirm password"
          {...passwordConfirm} />
      <ButtonToolbar type="checkbox">
        <Button
          type="submit"
          style={{ width: '50%', marginTop: '20px' }}
          bsStyle="primary" disabled={submitting}
          onClick={createStandardAccount}>
          {submitting ? 'Creating Account' : 'Create Account'}
        </Button>
      </ButtonToolbar>
      <TextBetweenTwoLines label="OR"/>
      <SocialButtons submit={createSocialProvidedAccount} redirect={redirect}/>
    </Form>;
  }
}
