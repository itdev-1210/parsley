// @flow
import { newScreen } from 'common/utils/analytics';
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router';
import { reduxForm } from 'redux-form';
import { Button, ButtonToolbar } from 'react-bootstrap';
import AuthInput from './AuthInput';
import { submitPasswordAuth } from '../utils/submitPasswordAuth';
import SocialButtons from './SocialButtons';
import TextBetweenTwoLines from './TextBetweenTwoLines';
import { getRedirectUrl } from '../utils/redirection';
import type { ReactRouterProps } from 'common/utils/routing';

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Required';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  return errors;
}

type Props = {
  fields: $FlowTODO,
  submitting: boolean,
  handleSubmit: Function => (SyntheticEvent<*> => void),
}

@reduxForm({
  form: 'login',
  fields: ['email', 'password'],
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  validate,
})
@withRouter
export default class LogInForm extends Component<ReactRouterProps & Props> {
  componentWillMount() {
    newScreen('login');
  }

  render() {

    const {
      fields: { email, password },
      submitting, handleSubmit,
      location, router,
    } = this.props;

    const redirect = getRedirectUrl(location.query);

    function submit(values) {
      return submitPasswordAuth(
          '/auth/login', values, redirect, router,
      );
    }

    return <form>
      <AuthInput placeholder="Email address" autoComplete="off" {...email} />
      <AuthInput placeholder="Password" type="password" {...password} />
      <ButtonToolbar type="radio" style={{ marginTop: '35px' }}>
        <Button
          style={{ width: 'calc(50% - 6px)', marginLeft: '6px' }}
          type="submit" className="green-btn indent"
          bsStyle="primary" disabled={submitting}
          onClick={handleSubmit(submit)}>
          {submitting ? 'Signing In' : 'Sign In'}
        </Button>
      </ButtonToolbar>
      <Link className="btn forgot-password" to="/auth/forgotpassword">
        forgot your password?
      </Link>
      <TextBetweenTwoLines label="OR"/>
      <SocialButtons redirect={redirect}/>
    </form>;
  }
}
