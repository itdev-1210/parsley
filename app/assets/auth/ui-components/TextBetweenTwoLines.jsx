import React from 'react';

export default function TextBetweenTwoLines(props) {
  return <div className="tb2l">
    <div className="tb2l-line"/>
    <div className="tb2l-text">{props.label}</div>
    <div className="tb2l-line"/>
  </div>;
}