// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import marked from 'common/utils/marked';

import { reduxForm } from 'redux-form';

import resetPassword from '../utils/resetPassword';

import { Button, Grid, PageHeader } from 'react-bootstrap';
import AuthInput from './AuthInput';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';

function validate(values) {
  let errors = {};

  if (!values.password) {
    errors.password = 'Required';
  }

  if (!values.passwordConfirm) {
    errors.passwordConfirm = 'Required';
  } else if (values.passwordConfirm !== values.password) {
    errors.passwordConfirm = 'Password does not match';
  }

  return errors;
}

function submit(values) {
  let apiEntity = _.pick(values, ['token']);
  apiEntity.newPassword = values.password;
  return resetPassword(apiEntity);
}

@reduxForm(
    {
      form: 'passwordreset',
      fields: ['password', 'passwordConfirm', 'token.id', 'token.secret'],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      onSubmit: submit,
      validate,
    },
    (state, ownProps) => ({
      // not actually using state, but want ownProps for router stuff
      initialValues: {
        token: {
          id: parseInt(ownProps.location.query.tokenID),
          secret: ownProps.location.query.tokenSecret,
        },
      },

    }),
)
export default class PasswordResetForm extends Component<*> {
  render() {

    const {
      fields: {
        password, passwordConfirm, tokenID, tokenSecret,
      },
      error: globalError, submitting,
      handleSubmit,
    } = this.props;

    const globalErrorDisplay = globalError
        ? <div dangerouslySetInnerHTML={{ __html: marked(globalError) }} />
        : null;

    return <Grid componentClass="form" fluid>
      <ParsleyNavbar/>
      <div className="auth-container">
        <PageHeader>Reset Password</PageHeader>

        {globalErrorDisplay}

        <AuthInput type="password" placeholder="Password"
                   {...password} />
        <AuthInput type="password" placeholder="Confirm Password"
                   {...passwordConfirm} />

        <Button onClick={handleSubmit} type="submit"
                bsStyle="primary" disabled={submitting}>
          {submitting ? 'Resetting' : 'Reset Password'}
        </Button>
      </div>
    </Grid>;
  }

}
