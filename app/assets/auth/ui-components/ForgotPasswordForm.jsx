// @flow
import React, { Component } from 'react';
import marked from 'common/utils/marked';
import { reduxForm } from 'redux-form';
import isEmail from 'validator/lib/isEmail';
import forgotPassword from '../utils/forgotPassword';
import { Button, Grid, PageHeader } from 'react-bootstrap';
import AuthInput from './AuthInput';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (!isEmail(values.email)) {
    errors.email = 'Invalid email address';
  }

  return errors;
}

@reduxForm(
    {
      form: 'forgotpassword',
      fields: ['email'],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      onSubmitSuccess: component => component.setState({ submitted: true }),
      validate,
    },
)
export default class ForgotPasswordForm extends Component<$FlowTODO, { submitted: boolean }> {
  state = { submitted: false };

  render() {
    const {
      fields: { email },
      error: globalError, submitting,
      handleSubmit,
    } = this.props;

    const { submitted } = this.state;

    const submit = (values) => forgotPassword(values.email).then(() => this);

    const globalErrorDisplay = globalError
        ? <div dangerouslySetInnerHTML={{ html: marked(globalError) }} />
        : null;

    const getFormBody = () => {
      if (!submitted) {
        return (
          <div>
            <p>Enter your e-mail address to receive a password reset e-mail</p>

            <AuthInput type="email" autoComplete="off" placeholder="E-mail"
                       {...email} />
            <Button type="submit" bsStyle="primary" disabled={submitting || submitted}
                onClick={handleSubmit(submit)} style={{ marginTop: '20px' }}
            >
              {submitting ? 'Resetting' : 'Reset Password'}
            </Button>
          </div>
        );
      } else {
        return <p>Password reset email sent; please check your inbox</p>;
      }
    };

    return (
      <Grid componentClass="form" fluid onSubmit={() => handleSubmit(submit)}>
        <ParsleyNavbar/>
        <div className="auth-container">
          <PageHeader>Reset Password</PageHeader>
          {globalErrorDisplay}
          {getFormBody()}
        </div>
      </Grid>
    );
  }
}
