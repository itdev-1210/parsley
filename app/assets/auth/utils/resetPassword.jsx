import { rawAPIFetch } from 'common/webapi/utils';

// If successful, redirects to plan selection page
export default function resetPassword(body) {
  return rawAPIFetch(
      '/auth/passwordreset', 'POST', JSON.stringify(body),
  ).then(
      _ => Promise.resolve(window.location = '/auth/login'),
      error => Promise.reject(error.reduxFormErrors), // should be a BadResponseError
  );
}
