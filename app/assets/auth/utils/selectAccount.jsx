// @flow

import { selectAccount } from 'common/webapi/userEndpoints';
import { PERMISSION_LEVELS } from 'common/utils/constants';
import type { SubscriptionLevel } from 'common/utils/features';

export type Account = {
  accountId: number,
  acceptsTerms: boolean,
  subscriptionLevel: ?SubscriptionLevel,

  isAccountOwner: boolean,
  permissionLevel: $Keys<typeof PERMISSION_LEVELS>,

  businessName: ?string,
  email: ?string,
}

export function selectAccountAndRedirect(account: Account, redirect: ?URL) {
  const sanitizedRedirect = redirect || new URL('/', window.location);

  return selectAccount(account.accountId)
      .then(
          _ => {
            let nextRedirect;
            if (account.acceptsTerms && account.subscriptionLevel) {
              nextRedirect = sanitizedRedirect;
            } else {
              nextRedirect = '/plan';
            }

            return Promise.resolve(window.location = nextRedirect);
          },
          error => Promise.reject(error.reduxFormErrors), // should be a BadResponseError
      );

}
