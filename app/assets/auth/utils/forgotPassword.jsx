import { rawAPIFetch } from 'common/webapi/utils';

export default function resetPassword(body) {
  return rawAPIFetch(
      '/auth/forgotpassword', 'POST', JSON.stringify(body),
  ).then(
      _ => Promise.resolve(null),

      error => Promise.reject(error.reduxFormErrors), // should be a BadResponseError
  );
}
