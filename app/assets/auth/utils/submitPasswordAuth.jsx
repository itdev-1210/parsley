// @flow
import { get, rawAPIFetch } from 'common/webapi/utils';
import type { RequestContent } from 'common/webapi/utils';
import { embedRedirect } from './redirection';

// If successful, redirects to plan selection page
export function submitPasswordAuth(endpoint: string, body: RequestContent, redirect: ?URL, router: $FlowTODO) {
  // attach all the fancy URL-parsing attributes - doesn't work on IE
  // copied to RootForm.jsx
  const sanitizedRedirect = redirect || new URL('/', window.location);
  const isAcceptingInvite = sanitizedRedirect.pathname.startsWith('/auth/confirm_invite/');

  return rawAPIFetch(
      endpoint, 'POST', JSON.stringify(body),
  ).then(
      _ => get('/me'),
  ).then(
      userInfo => {
        let nextRedirect;
        if (isAcceptingInvite || userInfo.allAccounts.length <= 1) {
          if (isAcceptingInvite || (userInfo.acceptsTerms && userInfo.plan)) {
            nextRedirect = sanitizedRedirect;
          } else {
            nextRedirect = '/plan';
          }

          return Promise.resolve(window.location = nextRedirect);
        } else {
          let selectAccountUrl = new URL('/auth/select_account', window.location);
          if (sanitizedRedirect) {
            selectAccountUrl = embedRedirect(selectAccountUrl, sanitizedRedirect);
          }
          router.push(selectAccountUrl);
        }
      },
      error => Promise.reject(error.reduxFormErrors), // should be a BadResponseError
  );
}
