import { rawAPIFetch, getApiErrorMsg } from 'common/webapi/utils';

import {
  fetchInvitationError,
  fetchInvitationSuccess,
  fetchUpstreamInvitationSuccess,
} from '../actions';
import reduxStore from '../store';

window.reduxStore = reduxStore;

export function checkInvitation(id, secret) {
  return rawAPIFetch(`/auth/check_invite/${id}/${secret}`, 'GET').then(
    resp => {
      reduxStore.dispatch(fetchInvitationSuccess(resp));
      return Promise.resolve(resp);
    },
    err => {
      const errMsg = err.reduxFormErrors ? err.reduxFormErrors._error : err.message;
      reduxStore.dispatch(fetchInvitationError(errMsg));
      return Promise.reject(err);
    },
  );
}

export function completeInvitation(body, onSuccess) {
  return rawAPIFetch('/auth/complete_invite', 'POST', JSON.stringify(body)).then(
    resp => onSuccess(resp),
    err => {
      const apiError = err.reduxFormErrors ? err.reduxFormErrors : { _error: err.message };
      return Promise.reject(apiError);
    },
  );
}

export function checkUpstreamInvitation(id, secret) {
  return rawAPIFetch(`/auth/check_upstream_invite/${id}/${secret}`, 'GET').then(
    resp => {
      reduxStore.dispatch(fetchUpstreamInvitationSuccess(resp));
      return Promise.resolve(resp);
    },
    err => {
      const errMsg = getApiErrorMsg(err);
      reduxStore.dispatch(fetchInvitationError(errMsg));
      return Promise.reject(err);
    },
  );
}

export function completeUpstreamInvitation(body, onSuccess) {
  return rawAPIFetch('/auth/complete_upstream_invite', 'POST', JSON.stringify(body)).then(
    resp => onSuccess(resp),
    err => Promise.reject(getApiErrorMsg(err)),
  );
}