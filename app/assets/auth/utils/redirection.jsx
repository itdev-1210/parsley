// @flow
export function getRedirectUrl(locationQuery: { redirect: ?string }): ?URL {
  let { redirect } = locationQuery;
  let redirectUrl;
  if (redirect) {
    redirectUrl = new URL(redirect, window.location);
  }
  if (redirectUrl && (redirectUrl.origin !== window.location.origin)) {
    redirectUrl = null;
  }
  return redirectUrl;
}

export function embedRedirect(url: URL, redirect: URL | string): URL {
  url.searchParams.append('redirect', redirect.toString());
  return url;
}
