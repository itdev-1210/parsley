import _ from 'lodash';

import keyMirror from 'common/utils/keyMirror';

export const types = keyMirror([
    'USE_SIGN_UP_FORM', 'USE_LOGIN_FORM',
    'FETCH_INVITATION_ERROR',
    'FETCH_INVITATION_SUCCESS',
    'ACCEPT_INVITATION_ERROR',
    'FETCH_UPSTREAM_INVITATION_ERROR',
    'FETCH_UPSTREAM_INVITATION_SUCCESS',
    'ACCEPT_UPSTREAM_INVITATION_ERROR',
]);

// super super short, because redux-form carries most of the load
export const useSignUpForm = () => ({
  type: types.USE_SIGN_UP_FORM
});

export const useLogInForm = () => ({
  type: types.USE_LOG_IN_FORM
});

export const fetchInvitationError = (err) => ({
  type: types.FETCH_INVITATION_ERROR,
  data: err,
});
export const fetchInvitationSuccess = (invitation) => ({
  type: types.FETCH_INVITATION_SUCCESS,
  data: invitation,
});

export const fetchUpstreamInvitationError = (err) => ({
  type: types.FETCH_UPSTREAM_INVITATION_ERROR,
  data: err,
});

export const fetchUpstreamInvitationSuccess = (invitation) => ({
  type: types.FETCH_UPSTREAM_INVITATION_SUCCESS,
  data: invitation,
})