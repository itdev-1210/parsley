import React from 'react';
import ReactDOM from 'react-dom';

import { Route, Router, useRouterHistory } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import useQueries from 'history/lib/useQueries';

import { Provider } from 'react-redux';

import store from './store';
import RootForm from './ui-components/RootForm';

import SignUpForm from './ui-components/SignUpForm';
import LogInForm from './ui-components/LogInForm';
import ConfirmInvitation from './ui-components/ConfirmInvitation';
import ConfirmUpstreamInvitation from './ui-components/ConfirmUpstreamInvitation';

import ForgotPasswordForm from './ui-components/ForgotPasswordForm';
import PasswordResetForm from './ui-components/PasswordResetForm';

import { gaInit } from 'common/utils/analytics'; // automatically sets up polyfill

/*
 * polyfills
 */
import '@babel/polyfill'; // automatically sets up polyfill
import fetch from 'whatwg-fetch'; // automatically sets up polyfill
import SelectAccountPage from './ui-components/SelectAccountPage';

gaInit();

ReactDOM.render(
    <Provider store={store}>
      <Router history={useRouterHistory(useQueries(createBrowserHistory))()}>
        <Route path="/auth/passwordreset" component={PasswordResetForm} />
        <Route path="/auth/forgotpassword" component={ForgotPasswordForm} />
        <Route path="/auth/select_account" component={SelectAccountPage} />
        <Route path="/auth/confirm_invite/:id/:secret" component={ConfirmInvitation} />
        <Route path="/auth/confirm_upstream_invite/:id/:secret" component={ConfirmUpstreamInvitation} />
        <Route path="/auth" component={RootForm}>
          <Route path="login" component={LogInForm} />
          <Route path="signup" component={SignUpForm} />
        </Route>
      </Router>
    </Provider>,
    document.querySelector('#login-container'),
);
