import React from 'react';
import ReactDOM from 'react-dom';

import { Route, Router, browserHistory } from 'react-router';

import { Provider } from 'react-redux';

import store from './store';
import PlanFrame from './ui-components/PlanFrame';
import Plan from './ui-components/Plan';
import ActivatePlan from './ui-components/ActivatePlan';

import { gaInit } from 'common/utils/analytics'; // automatically sets up polyfill

/*
 * polyfills
 */
import '@babel/polyfill'; // automatically sets up polyfill
import fetch from 'whatwg-fetch'; // automatically sets up polyfill

export const upstreamActivationPath = '/plan/multiLocation';
export const isUpstreamActivation = pathname => Boolean(pathname.match(upstreamActivationPath));

gaInit();

ReactDOM.render(
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="" component={PlanFrame}>
          <Route path="/plan/activate" component={ActivatePlan} />
          <Route path={upstreamActivationPath} component={Plan}/>
          <Route path="/plan" component={Plan}/>
        </Route>
      </Router>
    </Provider>,
    document.querySelector('#login-container'),
);
