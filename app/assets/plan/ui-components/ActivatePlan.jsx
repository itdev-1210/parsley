// @flow
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { reduxForm } from 'redux-form';

import type { List, Map } from 'immutable';

import _ from 'lodash';

import { getUserInfo, reactivatePlan } from 'common/webapi/userEndpoints';
import { getStripeProducts, getStripePublishableKey, getStripeSummary, } from 'common/webapi/stripeEndpoints';

import { Button, Col, FormControl, Grid, Row, } from 'react-bootstrap';
import StripeCheckout, { type Token as StripeToken } from 'react-stripe-checkout';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';

import marked from 'common/utils/marked';
import type { ReactRouterProps } from 'common/utils/routing';
import { noticeError } from 'common/utils/errorAnalytics';

// $FlowFixMe 'require' isn't supported by flow
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();


type FormValues = {
  firstName: string,
  lastName: string,
  businessName?: string,
  shippingPhoneNumber: string,
}

function validate(values: $Shape<FormValues>) {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = 'Required';
  } else if (!_.isString(values.firstName)) {
    errors.firstName = 'Invalid Name';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  } else if (!_.isString(values.lastName)) {
    errors.lastName = 'Invalid Name';
  }

  if (!values.shippingPhoneNumber) {
    errors.shippingPhoneNumber = 'Required';
  } else try {
    if (!phoneUtil.isValidNumber(phoneUtil.parse(values.shippingPhoneNumber, 'US'))) {
      errors.shippingPhoneNumber = 'Invalid phone number';
    }
  } catch (e) {
    errors.shippingPhoneNumber = 'Invalid phone number';
  }

  return errors;
}

type ReduxProps = {
  userInfo?: Map<string, $FlowTODO>,
  stripeProducts?: List<$FlowTODO>,
  stripeSummary?: Map<string, $FlowTODO>,
  stripePublishableKey?: string,
}

type PassedProps = {
  inModal: boolean,
  onSelect: FormValues => void,
}

type ReduxFormProps = {
  fields: any,
  values: FormValues,
  invalid: boolean,
};

type Props = ReduxProps & PassedProps & ReactRouterProps & ReduxFormProps

type State = {
  error: string,
  reactivating: boolean,
};

@withRouter
@reduxForm(
  {
    form: 'activate-plan',
    fields: [
      'firstName', 'lastName',
      'businessName', 'shippingPhoneNumber',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
  },
  (state, ownProps) => {
    const userInfo = state.get('userInfo');
    const stripeProducts = state.getIn(['stripeInfo', 'products']);
    const stripeSummary = state.getIn(['stripeInfo', 'summary']);
    const stripePublishableKey = state.getIn(['stripeInfo', 'publishableKey']);

    return {
      userInfo,
      stripeProducts,
      stripeSummary,
      stripePublishableKey,
      initialValues: {
        firstName: userInfo && userInfo.get('firstName'),
        lastName: userInfo && userInfo.get('lastName'),
        businessName: userInfo && userInfo.get('businessName'),
        shippingPhoneNumber: userInfo && userInfo.get('shippingPhoneNumber'),
      },
    };
  }
)
export default class ActivatePlan extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      error: '',
      reactivating: false,
    };
  }

  componentWillMount() {
    getUserInfo().then(u => {
      if (!u.previousSubscriptionLevel) {
        noticeError('ActivatePlan loaded with new customer');
      }
    });
    getStripeProducts();
    getStripeSummary();
    getStripePublishableKey();
  }

  subscribe(token: StripeToken) {
    const { values } = this.props;
    console.log('token');
    this.setState({ reactivating: true });
    return reactivatePlan({
      ...values,
      token: token.id,
    }).then(
      resp => {
        this.setState({ reactivating: false });
        window.location = '/';
      },
      err => {
        this.setState({ reactivating: false, error: err });
      }
    );
  }

  render() {

    const {
      userInfo, stripeSummary, stripePublishableKey, stripeProducts,
      invalid,
    } = this.props;
    const {
      firstName, lastName,
      businessName, shippingPhoneNumber,
    } = this.props.fields;

    if (!(userInfo && stripeProducts && stripeSummary && stripePublishableKey)) {
      return <Grid fluid><LoadingSpinner /></Grid>;
    }

    return <Grid fluid className="reactivate-plan">
      <h3>Reactivate Account</h3>

      {this.state.error ? <Row className="alert alert-danger">
        <Col dangerouslySetInnerHTML={{ __html: marked(this.state.error) }} />
      </Row> : null}
      <Row>
        <Col xs={12} md={6} className="mb-1">
          <Validate {...firstName}>
            <FormControl placeholder="First Name" bsSize="large" {...firstName} />
          </Validate>
        </Col>
        <Col xs={12} md={6} className="mb-1">
          <Validate {...lastName}>
            <FormControl placeholder="Last Name" bsSize="large" {...lastName} />
          </Validate>
        </Col>
      </Row>
      <Row>
        <Col xs={12} className="mb-1">
          <Validate {...businessName}>
            <FormControl placeholder="Company Name" bsSize="large" {...businessName} />
          </Validate>
        </Col>
      </Row>
      <Row>
        <Col xs={12} className="mb-1">
          <Validate {...shippingPhoneNumber}>
            <FormControl placeholder="Phone Number" bsSize="large" {...shippingPhoneNumber} />
          </Validate>
        </Col>
      </Row>
      <Row>
        <Col xs={12} className="mb-1" style={{ fontSize: '19px' }}>
          Plan:&nbsp;
          {stripeProducts.find(p =>
              p.get('subscriptionLevel') === userInfo.get('previousSubscriptionLevel'),
          ).get('name')}
        </Col>
      </Row>
      <Row>
        <Col xs={12} className="mb-1">
          {
            invalid ? <Button disabled>Confirm</Button>
                : <StripeCheckout
                    name="Parsley"
                    description="Activate Subscription"
                    panelLabel="Pay"
                    amount={stripeSummary.get('accountBalance')}
                    currency="USD"
                    token={t => {
                      this.subscribe(t);
                    }} // gotta return void for StripeCheckout's sake
                    email={userInfo.get('email')}
                    allowRememberMe={false}
                    stripeKey={stripePublishableKey}
                >
                  {this.state.reactivating ? <Button disabled>Activating</Button> : <Button>Confirm</Button>}
                </StripeCheckout>
          }
        </Col>
      </Row>
    </Grid>;
  }
}