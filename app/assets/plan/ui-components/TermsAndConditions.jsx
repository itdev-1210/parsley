import React, { Component } from "react";

export default class TermsAndConditions extends Component {

  render() {

    return <div style={{"overflowY": "scroll", "height": "400px"}}>


      <p>Terms of Use Revised, 22 June, 2017</p>
      <p>Thank you for visiting our website! These terms of use (the
        "Agreement") govern access to and use of the website (the "Website") of
        Parsley Software, Inc. (hereinafter, &ldquo;Company&rdquo;; "we" or
        "our"), the services offered from time to time ("Services") provided
        through the Website and through any applications and software provided
        to you to access our services, including mobile applications, updates or
        patches, utilities and tools or APIs (the "Software" and, collectively
        with the Website, the "Platform") by you ("Customer" or "you"), so
        please carefully read them before using the Platform and Services.</p>
      <p>These Terms, together with our Privacy Policy (&ldquo;Privacy
        Policy&rdquo;) as set forth from time to time at our Website and which
        are hereby incorporated into this Agreement by reference, establish the
        legal terms and conditions of the agreement between us governing your
        use and our provision of the Platform and Services.</p>
      <p>By using the Platform and Services you agree to be bound by this
        Agreement. If you are using the Platform and Services on behalf of an
        organization, you are agreeing to this Agreement for that organization
        and representing that you have the capacity and authority to bind that
        organization to this Agreement. In that case, "you" and "your" will
        refer to that organization.</p>
      <p>You may use the Platform and Services only in compliance with this
        Agreement. You may use the Platform and Services only if you have the
        power to form a contract with the Company and are not barred from doing
        so under any applicable constitutions, laws, ordinances, principles of
        common law, codes, regulations, statutes or treaties and all applicable
        orders, rulings, instructions, requirements, directives or requests of
        any courts, regulators or other governmental authorities (&ldquo;
        Law&rdquo;). You may not use the Services if you are under 18 years of
        age. By agreeing to this Agreement, you are representing to us that you
        are over 18. You may not use the Services unless you are a resident of
        the United States of America and access the Platform in the United
        States of America. Without limiting the foregoing, the Services are not
        available to any person resident in the European Union or any person
        accessing it under the jurisdiction of the European Union.</p>
      <p>1. Definitions. Capitalized terms in this Agreement have the meanings
        assigned to them in Section 14 or elsewhere in these this Agreement,
        unless the context otherwise requires, which meaning will be equally
        applicable to both the singular and plural forms of such terms. In this
        Agreement, unless a clear contrary intention appears (a) &rdquo;
        Section&rdquo; refers to sections of this Agreement; (b) &rdquo;
        including&rdquo; (and with correlative meaning &ldquo;include&rdquo;)
        means including without limiting the generality of any description
        preceding such term, and (c) any provision for Company&rsquo;s consent
        or approval allows Company to grant or withhold its consent or approval
        in its sole and absolute discretion.</p>
      <p>2. Services Provided by Company.</p>
      <p>2.1 Services. Subject to the terms of this Agreement, Company may
        provide certain recipe, ingredient and inventory services to Customer
        through the Platform.</p>
      <p>2.2 Updates. Company reserves the right to change or upgrade any
        equipment or software that Company uses to provide the Services without
        notice to Customer. Company may install security patches, updates,
        upgrades and service packs with respect to the Platform (&ldquo;
        Updates&rdquo;) as Company determine in its sole discretion, and Company
        reserves the right, but not the obligation, to roll back any Updates.
        Updates may change system behavior and functionality and as such may
        negatively affect the Services used by Customer. Company will not be
        responsible or liable for service disruption or changes in functionality
        or performance due to Updates. Company will not be responsible or liable
        for issues that may arise from incompatibilities between Customer's
        systems or software and any Update or hardware or software change or
        configuration, regardless of whether discretionary or requested.</p>
      <p>2.3 Access Credentials. Customer is responsible for safeguarding the
        username, password and other access credentials that Customer uses to
        access the Platform and Services (&ldquo;Access Credentials&rdquo;) and
        Customer agrees not to disclose Customer's Access Credentials to any
        third party. Customer is responsible for any activity using Customer's
        Access Credentials, whether or not Customer authorized that activity.
        Customer will immediately notify Company of any unauthorized use of
        Customer's account and of any actual or potential disclosure of
        Customer's Access Credentials. Customer acknowledges that if Customer
        wishes to protect Customer's transmission of data or files to Company,
        it is Customer's responsibility to use a secure encrypted connection to
        communicate with the Platform and Services. Company makes no
        representations or warranties as to the security of any such encrypted
        connection, however, and Customer agrees that Company will have no
        liability if it fails to protect Customer's transmission.</p>
      <p>3. Customer Obligations. Customer will use the Services for business
        purposes only. Customer&rsquo;s receipt of Services hereunder is at all
        times conditioned on Customer:</p>
      <p>(a) providing Company with all information reasonably necessary for
        Company to provide the Services;</p>
      <p>(b) complying with the Specifications at all times and using only the
        Access Credentials provided by Company;</p>
      <p>(c) otherwise performing Customer&rsquo;s obligations under this
        Agreement;</p>
      <p>(d) reviewing all transactions periodically, notifying Company promptly
        of suspected fraudulent or unauthorized activity under Customer&rsquo;s
        Access Credentials, cooperating with Company to investigate, remediate
        and prosecute any such security breach and reimbursing Company for
        remediation costs incurred in connection with any such security
        breach;</p>
      <p>(e) complying with all applicable Law in the use of the Platform and
        Services; and</p>
      <p>(f) cooperating with Company on technical matters as necessary to cause
        the parties&rsquo; respective servers to interoperate successfully such
        that transaction data are accurately recorded and processed and securely
        transmitted and stored.</p>
      <p>4. Suspension of Services. Company may in its sole discretion
        immediately suspend any Services and any right to use the Platform at
        any time in its discretion, including if:</p>
      <p>(a) Company believes that Customer has committed a material breach of
        this Agreement;</p>
      <p>(b) Company is obligated or believes itself obligated to suspend any
        Services to comply with an order, instruction, requirement, directive or
        request of any governmental body;</p>
      <p>(c) Company believes, in its reasonable opinion, that continuing to
        supply the Services to Customer may cause damage or harm to
        Company&rsquo;s relationship with any governmental body, business
        partner or other third party related to the Services;</p>
      <p>(d) applicable Law or a regulatory action or lawsuit prohibits, impairs
        or makes impractical the provision of the Services;</p>
      <p>(e) a third-party supplier on whose services the provision of Services
        is dependent suspends its provision of those services to Company;</p>
      <p>(f) the Services are being used in a manner that Company determines or
        has been notified may otherwise create liability or may be fraudulent or
        illegal;</p>
      <p>(g) Customer or any third party has accessed the Services in violation
        of Section 3, or there has otherwise been unauthorized use of
        Customer&rsquo;s Access Credentials.</p>
      <p>(h) Customer ceases to do business as an ongoing business concern,
        fails to meet its obligations as they come due or becomes subject to
        proceedings of bankruptcy, receivership, insolvency, liquidation or
        assignment for the benefit of creditors.</p>
      <p>5. Payment Terms.</p>
      <p>5.1 Fees. Some Services may require payment of a Fees. The Fees for use
        of these Services are set forth at www.parsleycooks.com. Company
        expressly reserves the right to change or modify its prices and fees at
        any time, and any changes or modifications will be effective immediately
        on posting without need for further notice to Customer.</p>
      <p>5.2 Payment. Customer agrees to pay any and all Fees at the time
        Customer orders the Services. Except for Fees payable on a
        per-transaction basis, all Fees are due in advance of the time period
        during which Services are provided. All invoices must be paid within 30
        days of the invoice date. Any invoice that is outstanding for more than
        30 days may result in the suspension or termination of Services. This
        may result in loss of data. Access to the account will not be restored
        until payment has been received. Any Fees not paid as and when due will
        incur late fees equal to 1.5% per month or the highest rate permitted by
        applicable Law. Customer will pay all</p>
      <p>costs and expenses incurred by Company in collecting any unpaid Fees,
        including court costs and fees, attorneys' fees and the commissions of
        collection agents.</p>
      <p>5.3 Payment Methods. Company accepts various forms of payment, as set
        forth on the Platform from time to time (each, a &ldquo;Payment
        Method&rdquo;). Customer must provide and verify at least one Payment
        Method to use the Services. Customer authorizes Company to store, and
        contract with a third-party to store, Payment Method information for
        future use as provided in this Agreement. To the extent permitted by
        applicable Law, Company may use certain third-party vendors and service
        providers to process payments and manage Customer's Payment Method
        information. By providing Payment Method information, Customer
        represents and warrants that (a) Customer is legally authorized to
        provide that information to Company, (b) Customer is legally authorized
        to perform payments using the Payment Method(s); and (c) that action
        does not violate the terms and conditions applicable to Customer's use
        of those Payment Method(s) or applicable Law. When Customer authorizes a
        payment using a Payment Method, Customer represents and warrants that
        there are sufficient funds or credit available to complete the payment
        using the designated Payment Method.</p>
      <p>5.4 Auto-Renewal. Unless otherwise provided, Customer agrees that until
        and unless Customer terminates the Services, the approved Payment Method
        will be billed on an automatically recurring basis to prevent any
        disruption to Services, using the Payment Method information provided to
        Company.</p>
      <p>5.5 Taxes. Listed Fees for the Services do not include any applicable
        sales, use, revenue, excise or other taxes imposed by any taxing
        authority. Any applicable taxes will be added to Company's invoice as a
        separate charge to be paid by Customer.</p>
      <p>5.6 No Refunds; Invoices Final. All Fees are non-refundable, even if
        the Services are suspended, terminated, or transferred before the end of
        any term for which Customer has paid. All invoices for Fees will be
        deemed as accepted and final unless Customer provides Company with a
        specific written description of any disagreement within 60 days after
        notice of the applicable invoice.</p>
      <p>5.7 Trial Subscriptions. For some Services, Company may offer a free
        trial subscription. If Customer accepts a free trial subscription,
        Company will begin to bill Customer for that Service when the free trial
        subscription expires, unless Customer cancels its subscription before
        that time. Customer is always responsible for any internet service
        provider, telephone, wireless and other connection fees that Customer
        may incur when using Services, even when Company offers a free trial
        subscription. Trial subscriptions are not transferrable.</p>
      <p>5.8 Fees Charged By Third-Party Sites and Vendors. Company may provide
        links to other websites. Some of these websites may charge separate
        fees, which are not included in any Fees that Customer may pay to
        Company. Any separate charges or obligations that Customer incurs in its
        dealings with third parties are Customer&rsquo;s responsibility.
        Customer is responsible for any internet service provider, telephone,
        wireless and other connection fees that Customer may incur when using
        Services.</p>
      <p>5.9 Credit Reports and Evaluation of Credit. Customer authorizes
        Company to obtain business and personal credit bureau reports in the
        name of the Customer at any time. Customer agrees to submit to Company
        current financial information in the name of the Customer at any time on
        request. Such information will be used for the purposes of evaluating or
        re-evaluating Customer's creditworthiness. Customer also authorizes
        Company to use such information and to share it with any Affiliate of
        Company in order to determine whether Customer is qualified for other
        products or services offered by any Affiliate of Company. Company may
        report its credit experience with Customer and Customer&rsquo;s payment
        history to third parties. Customer agrees that Company may release
        information about Customer or Customer's account to any Affiliate of
        Company.</p>
      <p>6. Intellectual Property Rights; Data.</p>
      <p>6.1 Platform. Company owns and will retain all right, title and
        interest in all Intellectual Property Rights embodied or fixed in, or
        otherwise pertaining to, the Platform. Subject to the terms and
        conditions of this Agreement, Company hereby grants to Customer a
        limited, non- exclusive, non-transferable license to use the Platform
        and the Specifications only during the Term and only to enable Company
        to provide Customer and its Users with the Services. Other than the
        foregoing grant of rights, Company does not grant, and Customer does not
        receive or possess, any right or interest in any of Company&rsquo;s
        Intellectual Property Rights, or any other type of right or interest,
        whether an economic, property or moral rights interest in the Platform.
        Customer acknowledges that Company may from time to time upgrade or
        otherwise change the Platform or the Specifications in its sole
        discretion. Company will use commercially reasonable efforts to notify
        Customer of any such changes that may affect the Services or the way in
        which Customer connects to the Platform. Customer is responsible for
        satisfying itself that it can successfully interface with the Platform
        under the Specifications, and Customer understands that Company may
        change those Specifications from time to time and that Customer may not
        be aware when changes have been made or are about to be made. Customer
        is prohibited from copying or otherwise reproducing or attempting to
        reproduce the Platform. Customer agrees not to modify, disassemble,
        decompile, reverse engineer, create derivative works of the
        Platform.</p>
      <p>6.2 Data. As between Customer and Company, Company agrees that Customer
        owns all right, title and interest, including all Intellectual Property
        Rights, in and to the Customer Content and Customer Data, and any
        changes, modifications or corrections to them. For purposes of this
        Agreement (a) “Customer Data” means any data of Customer processed or
        stored using the Platform or Services, and (b) “Customer Content” means
        any Content provided to Company by or on behalf of Customer as part of
        the Services. The Company may use the Customer Content and Customer Data
        in order to provide use of the Platform and Services. Company also may
        aggregate Customer Data and Customer Content with that of other
        customers for the sole purpose of analyzing and improving Company’s
        services to all customers, so long as Company does not in the process
        disclose to any third parties Customer Data or Customer Content in a
        manner that is personally identifiable to Customer.</p>
      <p>6.5 Reservation of Rights. Each party does not grant, and hereby
        expressly reserves onto itself, all rights not granted in this
        Agreement.</p>
      <p>6.6 Injunctive Relief. Customer agrees that any breach by Customer of
        this Section 6 is likely to cause irreparable injury for which Company
        would have no adequate remedy at law. Therefore, in the event of such a
        breach or threatened breach, Company will be entitled to seek injunctive
        relief, without limiting any other rights or remedies that may be
        available to it and Customer agrees to waive any requirement for the
        securing or posting of any bond in connection with any Company efforts
        to seek injunctive relief in accordance with this Section 6.6.</p>
      <p>6.7 Software, Utilities and Tools. Services may require or allow
        Customer to download Software from Company or its licensors onto
        Customer&rsquo;s smartphone, tablet, computer or other device. Company
        grants to Customer a non-exclusive, limited license to use Software
        solely for the purpose stated by Company at the time the Software is
        made available to Customer. If an end user license agreement is provided
        with the Software, Customer&rsquo;s use of the Software is subject to
        the terms of that license agreement. Customer may not sub-license, or
        charge others to use or access Software. Customer may not translate,
        reverse-engineer, reverse-compile or decompile, disassemble or make
        derivative works from Software. Customer may not modify Software or use
        it in any way not expressly authorized in writing by Company. Customer
        understands that Company&rsquo;s introduction of various technologies
        may not be consistent across all platforms and that the performance of
        Software and related Services may vary depending on Customer&rsquo;s
        computer and other equipment. From time to time, Company may provide
        Customer with updates or modifications to Software. Customer understand
        that certain updates and modifications may be required in order to
        continue use the Software and Services.</p>
      <p>7. Contributing UGC to Services.<br /> 7.1 Customer Responsibility.
        Company does not pre-screen all</p>
      <p>UGC and does not endorse or approve any UGC that Customer and other
        customers may contribute to Services. Customer is solely responsible for
        Customer&rsquo;s UGC and may be held liable for UGC that Customer posts.
        Customer bears the entire risk of the completeness, accuracy and/or
        usefulness of UGC found on Services.</p>
      <p>7.2 Intellectual Property Rights. Company respects the intellectual
        property rights of others. Customer must have the legal right to Upload
        UGC through the Services. Customer may not Upload any UGC on Services
        that infringes the Intellectual Property Rights or any other rights of a
        third party nor may you Upload UGC in violation of Law or this
        Agreement. You may Upload only UGC that you are permitted to Upload by
        the owner or by Law. Company may, without prior notice to you and in its
        discretion, remove UGC that may infringe the Intellectual Property
        Rights or other rights of a third party.</p>
      <p>7.3 Treatment of UGC. Company reserves the right (but has no obligation
        except as required by Law) to remove, block, edit, move or disable UGC
        for any reason, including when Company determines that UGC violates
        these terms. The decision to remove UGC at any time is in Company&rsquo;
        s discretion. To the maximum extent permitted by Law, Company does not
        assume</p>
      <p>any responsibility or liability for UGC or for removal of, UGC or any
        failure to or delay in removing, UGC or other content.</p>
      <p>7.4 Take Down Procedure. If any person believes that person&rsquo;s
        Intellectual Property Rights have been infringed by someone else on the
        Services, that person may contact Company by e- mailing the following
        information to service@parsleycooks.com :</p>
      <p>(a) a description of the Intellectual Property Rights and an
        explanation as to how they have been infringed;</p>
      <p>(b) a description of where the infringing material is located;</p>
      <p>(c) the complaining person&rsquo;s address, phone number and email
        address;</p>
      <p>(d) a statement by the complaining person, made under penalty of
        perjury, that (i) the complaining person has a good-faith belief that
        the disputed use of material in which that person owns Intellectual
        Property Rights is not authorized, and (ii) the information provided is
        accurate, correct, and that the complaining person is authorized to act
        on behalf of the owner of an exclusive right that is allegedly
        infringed; and</p>
      <p>(e) a physical or electronic signature of the person authorized to act
        on behalf of the owner of the exclusive right that has allegedly been
        infringed.</p>
      <p>7.5 UGC License Grant. If Customer uses the Services to transmit UGC to
        other customers, except as expressly communicated by Customer to the
        recipient in writing at the time of transmittal, then Customer grant
        those customers the right to use, copy, modify, display, perform, create
        derivative works from, and otherwise communicate and distribute the UGC
        transmitted on or through the relevant Services without further notice,
        attribution or compensation to Customer.</p>
      <p>8. Rules of Conduct.</p>
      <p>8.1 Rules. The Services may include discussion forums, bulletin boards,
        review services or other forums in which Customer or third parties may
        post reviews or other content, messages, materials or other items on the
        Services (&ldquo;Interactive Areas&rdquo;). If Company provides such
        Interactive Areas, Customer is solely responsible for Customer&rsquo;s
        use of such Interactive Areas and uses them at Customer&rsquo;s own
        risk. Customer Content submitted to any public area of the Services will
        be considered non-confidential. Customer agrees not to Upload any of the
        following:</p>
      <p>(a) any message, data, information, text, music, sound, photos,
        graphics, code or other Content that is unlawful, libelous, defamatory,
        obscene, pornographic, indecent, lewd, suggestive, harassing,
        threatening, invasive of privacy or publicity rights, abusive,
        inflammatory, fraudulent or otherwise objectionable;</p>
      <p>(b) Content that would constitute, encourage or provide instructions
        for a criminal offense, violate the rights of any party, or that would
        otherwise create liability or violate any Law;</p>
      <p>(c) Content that may infringe any Intellectual Property Rights or any
        other right of any person;</p>
      <p>(d) Content that impersonates any person or entity or otherwise
        misrepresents your affiliation with a person or entity;</p>
      <ol start="5">
        <li>
          <p>(e) &nbsp;unsolicited promotions, political campaigning,
            advertising or solicitations;</p>
        </li>
        <li>
          <p>(f) &nbsp;private information of any third person, including
            addresses, phone numbers, email</p>
        </li>
      </ol>
      <p>addresses and credit card numbers, unless that third person has
        expressly consented to that use;</p>
      <ol start="7">
        <li>
          <p>(g) &nbsp;viruses, corrupted data or other harmful, disruptive or
            destructive files;</p>
        </li>
        <li>
          <p>(h) &nbsp;Content that is unrelated to the topic of the Interactive
            Area(s) in which the Content</p>
        </li>
      </ol>
      <p>is posted;</p>
      <p>(i) commercial or other messages to any third person if those messages
        are not solicited, authorized or welcomed by the third person; or</p>
      <p>(j) Content that, in Company&rsquo;s discretion, is objectionable or
        which restricts or inhibits any other person from using or enjoying the
        Interactive Areas or the Services, or which may expose Company or its
        Affiliates or its customers to any harm or liability of any type.</p>
      <p>8.2 Company Has No Duty to Monitor. Unless otherwise specified, there
        is no requirement or expectation that Company will monitor or record any
        online activity on Services, including communications. However, Company
        reserves the right to access and/or record any online activity on
        Services and Customer gives Company Customer&rsquo;s express consent to
        access and record Customer&rsquo;s activities. Company has no liability
        for Customer&rsquo;s or any third party&rsquo;s violation of this
        Agreement, including this Section 8.</p>
      <p>8.3 Reporting Abuse. If Customer encounters another customer who is
        violating this Section 8, Customer will promptly report this activity to
        Company using the &ldquo;Help&rdquo; or &ldquo;Report Abuse&rdquo;
        functions in the relevant Service, if available, or contact Company
        customer support at service@parsleycooks.com.</p>
      <p>9. Representations, Warranties and Covenants.</p>
      <p>9.1 Authority. Customer represents, warrants and covenants to Company
        that it has full power and authority to enter into this Agreement, to
        carry out its obligations under this Agreement and to grant the rights
        and licenses granted by it to Company pursuant to this Agreement.</p>
      <p>9.2 Approval. Customer warrants to Company that it has and will have at
        all times during the Term all licenses, approvals, qualifications,
        permits or certificates required in respect of the delivery of all
        Customer Content, Customer Content and other information provided
        pursuant to this Agreement.</p>
      <p>9.3 Compliance with Law. Customer represents, warrants and covenants to
        Company that in connection with all actions under this Agreement, it
        will comply with all applicable Law, including all Law related to
        privacy.</p>
      <p>9.4 Conflicting Obligations. Customer represents, and warrants to
        Company that it has no outstanding agreement or obligation which is in
        conflict with any of the provisions of this Agreement, or which would
        preclude it from complying with the provisions hereof, and further
        agrees that during the Term it will not enter into any such conflicting
        agreement.</p>
      <p>9.5 Cooperation. Customer will provide Company with access to, and use
        of, all information, data, documentation and other materials reasonably
        necessary for Company to fulfill its obligations under this
        Agreement.</p>
      <p>9.6 Content Warranties. In respect of any reproduction, adaptation or
        copy of an artistic work, audiovisual work, motion picture, sound
        recording, musical work, other copyrightable content, file or other data
        (each, a &ldquo;Work&rdquo;), forming part of Customer Content received
        or delivered in connection with the provisions of the Services, Customer
        warrants to Company that Customer has the Intellectual Property Rights,
        permission or proper authority necessary to allow the Work to be used
        through the Platform in provision of the Services without infringing the
        Intellectual Property Rights or other rights of any third party; and, if
        applicable, Customer has paid or will pay any royalty, license fees and
        all other properly imposed fees associated with the Work to a third
        party having the relevant Intellectual Property Rights.</p>
      <p>10. Indemnification. Customer will defend, indemnify, save, and hold
        Company and its Affiliates, officers, employees, agents, suppliers or
        licensors harmless from any and all demands, liabilities, losses, costs,
        and claims, including reasonable attorneys&rsquo; fees, costs of
        investigation, and the amount of any settlements, asserted against
        Company and them that may arise or result from (a) Customer's and its
        Users' use of the Services or the Platform, (b) Customer's breach of any
        representation, warranty or covenant in this Agreement, (c) Customer's
        negligence, willful misconduct or violation of applicable Law, (d) any
        Customer Content, or (e) any disclose of personally identifiable
        information or other confidential information in violation of
        third-party rights or applicable Law except if solely attributable to a
        willful violation of applicable Law by Company.</p>
      <p>11. Limitations and Disclaimers.</p>
      <p>11.1 Limitation of Liability. EXCEPT AS SPECIFICALLY SET FORTH IN THIS
        AGREEMENT, UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN
        TORT, CONTRACT OR OTHERWISE, WILL EITHER PARTY BE LIABLE TO THE OTHER
        PARTY FOR ANY INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL
        DAMAGES OF ANY CHARACTER INCLUDING DAMAGES FOR LOSS OF GOODWILL, LOSS OF
        PROFITS, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, EVEN IF SUCH
        PARTY WILL HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH LOSS. EXCEPT
        FOR CUSTOMER'S OBLIGATIONS UNDER SECTIONS 3(d), 5, 10, 13.3 AND 13.10,
        IN NO EVENT WILL EITHER PARTY&rsquo;S AGGREGATE LIABILITY TO THE OTHER
        PARTY UNDER THIS AGREEMENT EXCEED THE LOWER OF (a) $1,000, AND (b) THE
        AGGREGATE AMOUNTS PAID OR PAYABLE</p>
      <p>BY WAY OF FEES FOR THE 6-MONTH PERIOD PRIOR TO THE EVENT GIVING RISE TO
        LIABILITY .</p>
      <p>11.2 Disclaimer of Warranties. ASIDE FROM THE WARRANTIES THAT COMPANY
        EXPRESSLY SETS FORTH IN WRITING IN THIS AGREEMENT, IT PROVIDES THE
        PLATFORM AND SERVICES &ldquo;AS-IS&rdquo; AND &ldquo;WITH ALL
        FAULTS.&rdquo; EXCEPT AS OTHERWISE SET FORTH HEREIN, TO THE MAXIMUM
        EXTENT PERMITTED BY LAW, COMPANY EXPRESSLY DISCLAIMS ALL WARRANTIES,
        EXPRESS OR IMPLIED, WITH RESPECT TO THE PLATFORM AND SERVICES AND
        CUSTOMER&rsquo;S USE THEREOF. CUSTOMER WAIVES ANY AND ALL WARRANTIES
        THAT MAY BE IMPLIED BY LAW, INCLUDING, ANY IMPLIED WARRANTY OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF
        THIRD PARTY RIGHTS OR ANY WARRANTY ARISING FROM A COURSE OF DEALING OR
        USAGE OF TRADE. NO WARRANTY IS MADE REGARDING THE RESULTS OF THE
        SERVICES OR PLATFORM, OR THAT USE OF THE SERVICES OR PLATFORM WILL BE
        UNINTERRUPTED OR ERROR-FREE, OR THAT ANY ERRORS OR DEFECTS IN THE
        SERVICES OR PLATFORM WILL BE CORRECTED, OR THAT THE SERVICES OR
        PLATFORM'S FUNCTIONALITY WILL MEET CUSTOMER'S REQUIREMENTS. WITHOUT
        LIMITING THE GENERALITY OF THE FOREGOING, COMPANY DOES NOT PROVIDE ANY
        LEGAL ADVICE AND DOES NOT WARRANT THAT USE OF THE SERVICES WILL COMPLY
        OR ASSURE CUSTOMER'S COMPLIANCE WITH ANY CUSTOMER OBLIGATIONS UNDER
        APPLICABLE LAW. CUSTOMER UNDERSTANDS AND AGREES THAT IT IS CUSTOMER'S
        SOLE RESPONSIBILITY TO DETERMINE HOW TO COMPLY WITH APPLICABLE LAW AND
        THE BENEFITS, IF ANY, OF THE SERVICES IN DOING SO. WITHOUT LIMITING THE
        GENERALITY OF THE FOREGOING, CUSTOMER UNDERSTANDS AND AGREES THAT
        NUTRITIONAL INFORMATION PROVIDED THROUGH THE PLATFORM MAY NOT BE
        APPROPRIATE OR SUFFICIENT FOR CUSTOMER&rsquo;S COMPLIANCE WITH LEGAL
        LABELLING OR NUTRITIONAL INFORMATION DISCLOSURE OBLIGATIONS. CUSTOMER IS
        RESPONSIBLE FOR AND MUST PROVIDE ALL HARDWARE, SOFTWARE, SERVICES AND
        OTHER COMPONENTS NECESSARY TO ACCESS AND USE THE SERVICES, OTHER THAN
        THE PLATFORM. COMPANY MAKES NO REPRESENTATIONS, WARRANTIES, OR
        ASSURANCES THAT CUSTOMER'S HARDWARE, SOFTWARE AND OTHER SERVICES AND
        SYSTEMS WILL BE COMPATIBLE WITH THE PLATFORM OR SERVICE. COMPANY WILL
        HAVE NO RESPONSIBILITY FOR ANY HARM TO CUSTOMER'S COMPUTER SYSTEM, LOSS
        OR CORRUPTION OF DATA, OR OTHER HARM THAT RESULTS FROM CUSTOMER'S ACCESS
        TO OR USE OF THE SERVICES OR PLATFORM. Some states do not allow the
        types of disclaimers in this Section 11.2, so they may not apply to
        you.</p>
      <p>11.3 Third-Party Services. Links from the Platform to external websites
        (including external sites that are framed by the Website) or inclusion
        of advertisements do not constitute an endorsement by Company of those
        sites or the content, products, advertising and other materials
        presented on those sites or of the products and services that are the
        subject of those advertisements, but are for Customer's reference and
        convenience. Customer accesses such sites or the products and services
        that are the subject of those advertisements at Customer's risk. It is
        Customer's responsibility to evaluate the content and usefulness of the
        information obtained from other sites.</p>
      <p>Company does not control those sites, and is not responsible for their
        content. Company provision of links to third-party sites does not mean
        that Company endorses any of the material on those sites, or has any
        association with their operators. Customer further acknowledges that use
        of any site controlled, owned or operated by third parties is governed
        by the terms and conditions of use for those sites, and not by this
        Agreement. Company expressly disclaims any liability derived from the
        use and/or viewing of links that may appear on the Platform. Customer
        agrees to hold Company harmless from any liability that may result from
        the use of links that may appear on the Platform.</p>
      <p>12. Term and Termination. The term of this Agreement (the "Term") will
        begin on the Effective Date and will continue until it is terminated
        pursuant to its terms. Either party may terminate this Agreement at any
        time. Company may terminate this Agreement by notice to Customer.
        Customer may terminate this Agreement by terminating Customer's account
        using the process provided on the Platform for terminating a customer
        account. The provisions of Sections 1, 2.2, 2.3, 3, 5 (as to any
        payments due with respect to the period before termination) and 6-14, as
        well as any other terms of this Agreement that expressly extend or by
        their nature should extend beyond termination or expiration of this
        Agreement, will survive and continue in full force and effect after any
        termination or expiration of this Agreement. Any license(s) granted to
        Customer under this Agreement will automatically terminate on
        termination or expiration of this Agreement.</p>
      <p>13. Miscellaneous.</p>
      <p>13.1 Assignment. Customer will not assign, transfer or delegate its
        rights or obligations under this Agreement to any third party without
        Company&rsquo;s prior written consent. For the purposes of this
        Agreement, any sale or transfer by Customer of all or substantially all
        of its stock or assets or by merger or otherwise by operation of law is
        considered an assignment, requiring Company&rsquo;s express written
        consent. Company may freely assign this Agreement or transfer any of its
        interest herein, including to any Company Affiliate, to a purchaser of
        all or substantially all of Company&rsquo;s assets, and to a successor
        in interest of Company as part of a corporate reorganization,
        consolidation or merger. This Agreement and each of the provisions
        hereof will inure to the benefit of and be binding on each party&rsquo;s
        successors, administrators and permitted assigns.</p>
      <p>13.2 Severability. If any provision of this Agreement is determined by
        any court of competent jurisdiction to be invalid or unenforceable, such
        provision will be interpreted to the maximum extent to which it is valid
        and enforceable, all as determined by such court in such action, and the
        remaining provisions of this Agreement will, nevertheless, continue in
        full force and effect without being impaired or invalidated in any
        way.</p>
      <p>13.3 Entire Agreement; No Reliance. This Agreement, including the
        Privacy Policy, constitutes the entire agreement and understanding
        between the parties with respect to its subject matter, and this
        Agreement merges and supersedes all prior agreements, discussions and
        writings with respect to its subject matter. Each party represents that
        it has not relied on any representations made by the other party or its
        representatives or on any descriptions, illustrations or specifications
        contained in any physical or digital text including websites, proposals,
        catalogues or other publicity material. Each party has relied only on
        the express terms of this Agreement, and not on any representations</p>
      <p>of the other party not set forth herein, nor on any other documents or
        materials of the other party not expressly made a part hereof.</p>
      <p>13.4 Force Majeure. Neither party will be held responsible for any
        delay or failure in performance of any part of this Agreement (with the
        exception of any obligation to make payments to the other party
        hereunder) to the extent such delay or failure is caused by fire, flood,
        explosion, war, terrorism, strike, embargo, governmental action or
        failure to act, the act of any civil or military authority, act of God,
        inability to secure material or transportation facilities, acts or
        omissions of carriers, power outages, computer failures, or by any other
        causes beyond its control whether or not similar to the foregoing.</p>
      <p>13.5 No Waiver. The waiver, modification, or failure to insist by a
        party on any of the provisions of this Agreement will not void, waive,
        nor modify any of the other provisions nor be construed as a waiver or
        relinquishment of such party&rsquo;s right to performance in the future
        of any such provision.</p>
      <p>13.6 Relationship of the Parties. The relationship of the parties under
        this Agreement is one of independent contractors, and no agency,
        partnership, employment, joint venture or similar relationship is
        created hereby. Except as specifically authorized, neither party will
        have any authority to assume or create obligations on the other party's
        behalf, and neither party will take any action that has the effect of
        creating the appearance of its having such authority.</p>
      <p>13.7 Notices. Any notice required or permitted under this Agreement
        will be given in writing by personal delivery, by USPS Priority Express
        Mail, by nationally recognized overnight delivery service (e.g. UPS), or
        e mail. Any notice will be deemed received on the earlier of the date of
        actual delivery or the date on which delivery is refused, regardless of
        whether the party has vacated the physical address or discontinued the
        e-mail address. The notice address and e-mail address for Customer will
        be the address and e-mail address on record with Company as modified by
        Customer through the Platform from time to time. The notice address for
        Company will be 2618 3rd st, Santa Monica, and the e-mail address for
        Company will be service@parsleycooks.com , as reflected in this
        Agreement as modified form time to time, pursuant to its terms.</p>
      <p>13.8 Third Party Beneficiaries. This Agreement does not and is not
        intended to confer any rights or remedies on any person or entity other
        than the parties hereto.</p>
      <p>13.9 Amendment. Company may revise this Agreement from time to time and
        the most current version will always be posted on the Website. If a
        revision, in our sole discretion, is material Company may, but have no
        obligation to, notify Customer, including by postings to relevant
        Company blogs, so please check those pages regularly. By continuing to
        access or use the Platform and Services after revisions become
        effective, Customer agrees to be bound by the revised terms. If Customer
        does not agree to the new terms, Customer must cease using the Platform
        and Services and terminate Customer's account using the process provided
        on the Platform for terminating a customer account.</p>
      <p>13.10 Governing Law; Arbitration. Any disputes between the parties
        arising out of or relating to the Agreement (&ldquo;Disputes&rdquo;)
        will be governed by California law regardless of Customer's location and
        notwithstanding of any conflicts of law principles. Except for Disputes
        relating to Intellectual</p>
      <p>Property Rights, any Disputes will be resolved exclusively by final and
        binding arbitration under the rules and auspices of the American
        Arbitration Association, to be held in Los Angeles County, California,
        in English, with a written decision stating legal reasoning issued by
        the arbitrator(s) at either party&rsquo;s request, and with arbitration
        costs and reasonable documented attorneys&rsquo; costs of both parties
        to be borne by the party that ultimately loses. Either party may obtain
        injunctive relief (preliminary or permanent) and orders to compel
        arbitration or enforce arbitral awards in any court of competent
        jurisdiction. If there is more than one Dispute between the parties, all
        such Disputes may be heard in a single arbitration under this Section
        13.10. Except to the extent required by applicable Law that cannot be
        waived or modified by this Agreement, Disputes under this Agreement may
        not be consolidated into a single arbitration proceeding with disputes
        between the Company and other persons, even if those disputes are
        governed by an arbitration proceeding similar or identical to this
        Section 13.10 and even those other persons are similarly situated and
        their disputes are similar or identical in the nature to a Dispute under
        this Agreement.</p>
      <p>14. Definitions. The following terms will be defined as set forth below
        for purposes of this Agreement:</p>
      <p>&ldquo;Access Credentials&rdquo; means the secure method by which
        Company provides Customer, generally through Customer&rsquo;s own
        computer systems, access to control the Platform.</p>
      <p>&ldquo;Affiliate&rdquo; means, in respect of a party, any company or
        other business entity controlled by, controlling, or sharing common
        control. For the purposes of this definition, &ldquo;control&rdquo; will
        mean the direct or indirect power to direct or cause the direction of
        the management and policies of a company or other business entity,
        whether through ownership of 50% or more of the voting interest, by
        contract, or otherwise.</p>
      <p>&ldquo;Content&rdquo; means software, technology, text, forum posts,
        chat posts, profiles, widgets, messages, links, emails, music, sound,
        graphics, pictures, video, code, and all audio visual or other material
        appearing on or emanating to and/or from Services, as well as the design
        and appearance of our applications and websites. Content includes
        UGC.</p>
      <p>&ldquo;Fees&rdquo; means the amount payable by Customer to Company for
        the Services.</p>
      <p>&ldquo;Intellectual Property Rights&rdquo; means all copyrights fixed
        in any medium now known or hereafter discovered, including copyrights in
        computer programs, pictorial works, audiovisual works, literary works,
        musical works, sound recordings, motion pictures and the like; patents,
        trademarks, trade names, trade secrets, design rights, design models,
        database rights and topography rights, all rights to bring an action for
        passing off, rights of privacy or publicity, and any other similar form
        of intellectual rights in intangible property or proprietary rights,
        statutory or otherwise, whether registered or not, and whether applied
        for or not, all rights to apply for protection in respect of any of the
        above rights and all other forms of protection of a similar nature or
        that relate to intangible property, ideas or expression, as they may
        exist anywhere in the world.</p>
      <p>"Specifications" means the documentation and specifications provided by
        Company to Customer from time to time (through the Website, the Software
        or otherwise) with respect to use and integration of the Service and
        Platform.</p>
      <p>&ldquo;Upload&rdquo; means to upload to the Platform, transmit,
        distribute, store, create or otherwise publish through the Service.</p>
      <p>&ldquo;UGC&rdquo; means Content contributed by customers to Services or
        created by customers through Services.</p>


      <p>Parsley Software, Inc. Customer Privacy Policy Last Modified: January
        24, 2017</p>
      <p>1. Introduction. This Privacy Policy describes how Parsley Software,
        Inc. (referred to as &ldquo;we&rdquo;, &ldquo;our&rdquo;, &ldquo;
        us&rdquo;) collects, uses, and stores your information. This privacy
        policy, together with our Terms of Use (&ldquo;Terms of Use&rdquo;)
        separately agreed to you and incorporating this policy, establishes the
        legal terms and conditions of the agreement between us governing the
        submission of the Information and our use of it in performing the
        Service. All capitalized terms used in this policy and not otherwise
        defined here will have the meanings assigned to the in the Terms of Use.
        By submitting the Information, you agree to be bound by this policy,
        User ("you") allow the Information to be used in the manner specified in
        this policy.</p>
      <p>2. Information We Collect. We collect information in two ways, directly
        from your input into the Platform, and automatically through the
        Service.</p>
      <p>2.1 Information Provided By You. We may collect and store any personal
        information you enter on our website or provide to us in some other
        manner. This includes identifying information, such as name, address,
        email address, and telephone number, and, if you transact business with
        us, financial information such as your payment method (valid credit card
        number, type, expiration date or other financial information). If you
        use our services to upload and manipulate information about your
        business, this will include that information, for example recipes,
        ingredient information, supplier information, purchase orders,
        deliveries and inventory levels. If you contact us for support, we will
        keep an internal record of what support was given. From time-to-time, we
        may provide you with the opportunity to participate in contests or
        surveys. If you choose to participate, we may request certain personal
        information from you. Participation in these contests or surveys is
        completely voluntary and you therefore have a choice whether or not to
        disclose the requested information. The requested information typically
        includes contact information (such as name and address), and demographic
        information (such as zip code).</p>
      <p>2.2 Information Collected Automatically.</p>
      <p>(a) We collect non-personally identifiable information automatically
        for the primary purpose of customizing the Platform and Service,
        understanding how they are used, and preventing misuse. For example,
        when you use access the Platform, we automatically record information
        from your device, its software, and your activity using the Platform.
        This may include, but is not limited to the date and time of your access
        to the Platform, your device&rsquo;s internet protocol (&ldquo;IP&rdquo;
        ) address, browser type, the web page visited before you came to our
        website, locale preferences, identification numbers associated with your
        devices, your mobile carrier, date and time stamps associated with
        transactions, system configuration information, metadata concerning
        files included in the Information submitted to us, and other
        interactions with the Platform.</p>
      <p>(b) Some devices allow applications to access real-time location-based
        information (for example, GPS or sensor data from your device that may
        provide information on nearby Wi-Fi access points and cell towers). In
        addition, some of the information we collect from you, for example IP
        address, can sometimes be used to approximate a device&rsquo;s location.
        Finally, some browsers share your location information (either the
        location registered with your account or gleaned from other geo-location
        methods) with sites you visit and we may obtain location information
        about you in this way. If you do not wish us to receive this
        information, please configure your browser not to share it.</p>
      <p>(c) We also use &ldquo;cookies&rdquo; to collect information and
        improve the Service. A cookie is a small data file that we transfer to
        your device. We may use &ldquo;persistent cookies&rdquo; to recognize
        you for future access to the Platform. We may use &ldquo;session ID
        cookies&rdquo; to enable certain features of the Platform, to better
        understand how you interact with the Platform and to monitor aggregate
        usage and web traffic routing on the Platform. You can instruct your
        browser, by changing its options, to stop accepting cookies or to prompt
        you before accepting a cookie from the websites you visit. If you do not
        accept cookies, however, you may not be able to use all aspects of the
        Platform or the Services.</p>
      <p>(d) We use Local Storage Objects (&ldquo;LSOs&rdquo;) such as HTML5 or
        Flash to store [content information and preferences]. Third parties with
        whom we partner to provide certain features on our site may use LSOs
        such as HTML 5 or Flash to collect and store information. Various
        browsers may offer their own management tools for removing HTML5
        LSOs.</p>
      <p>3. Use of your Information.</p>
      <p>3.1 In General. We may use information that we collect about you to (a)
        deliver the Services, and manage the Platform and our business, (b)
        manage your account and provide you with customer support, (c) perform
        research and analysis about your use of, or interest in, our products,
        services, or content, or products, services or content offered by
        others, (d) communicate with you by email, postal mail, telephone and/or
        mobile devices about products or services that may be of interest to you
        either from us or other third parties, (e) develop and display content
        and advertising tailored to your interests on our site and other sites,
        including providing our advertisements to you when you visit other
        sites, (f) perform ad tracking and website or mobile application
        analytics, (g) enforce or exercise any rights in our terms and
        conditions, and (h) perform functions as otherwise described to you at
        the time of collection.</p>
      <p>3.2 Payment Information. We use credit card and other personally
        identifiable information (such as PayPal email addresses) you submit to
        us on the Platform, and other information that we collect, as required,
        to process payments you make through the Platform through our payment
        processor intermediaries. We do not store credit card or other payment
        method information. Our third-party payment processors have the sole and
        complete responsibility for the storage of credit card and payment
        information. We may also share personally identifiable information with
        our payment processor intermediaries for risk management and fraud
        prevention.</p>
      <p>3.3 Third-Party Service Providers. In all circumstances, we may perform
        the functions described above directly or use a third party vendor to
        perform these functions on our behalf who will be obligated to use your
        personal information only to perform services for us and only in
        conformance with this privacy policy.</p>
      <p>4. Sharing or Disclosing your Information.</p>
      <p>4.1 Personal Information. We do not share your personal information
        with others except as indicated below or when we inform you and give you
        an opportunity to opt out of having your personal information
        shared.</p>
      <p>(a) Service Providers. We may share information, including personal
        information, with third parties that perform certain services on our
        behalf. These services may include fulfilling orders, providing customer
        service and marketing assistance, performing business and sales
        analysis, ad tracking and analytics, supporting our website
        functionality, and supporting surveys and other features offered through
        the Platform. We may also share your name, contact information and
        credit card information with our service providers who process credit
        card payments. These service providers may have access to personal
        information needed to perform their functions but are not permitted to
        share or use such information for any other purposes and are not
        permitted to use the information in violation of this privacy
        policy.</p>
      <p>(b) Third Parties.</p>
      <p>(i) When click-through on third-party offers featured on the Platform,
        we may share personal information with the businesses with which we
        partner to offer you the applicable products or services. When you elect
        to engage in a particular merchant's offer or program, you authorize us
        to provide your email address and other information to that third party.
        To opt-out of cookies that may be set by third party data partners,
        please go to http://www.aboutads.info/choices. We may share your
        information with a third party application or a third-party merchant
        services provider or other reseller with your consent, for example when
        you choose to access the Platform through such an application or are
        provided with access to the Platform through such a third-party merchant
        services firm.</p>
      <p>(ii) The Platform may include, from time to time, social media
        features, such as Facebook and Twitter buttons and widgets, such
        as &ldquo;share&rdquo; buttons or interactive mini-programs that run on
        our site. These features may collect your IP address, which page you are
        visiting on our site, and may set a cookie to enable the feature to
        function properly. Social media features and widgets are either hosted
        by a third party or hosted directly on the Platform.</p>
      <p>(iii) When you use the Services to disclose your information to other
        customers or other third parties (such as suppliers), we may share that
        information with those third parties.</p>
      <p>(iv) Your interactions with third parties through the Platform and
        Services are governed by the privacy policies, if any, of those third
        parties. We are not responsible for what those third parties do with
        your information. So you should make sure you trust those third parties
        and that they have privacy policies acceptable to you.</p>
      <p>4.2 Other Information. Automatically collected non-personally
        identifiable information may be aggregated and disclosed without
        restriction. We may share aggregated, non-personally identifiable
        information publicly. For example, we may share information publicly to
        show trends about the general use of the Service with the press, in our
        marketing materials or with our business partners. We may collate or
        connect non-personally identifiable information with your personal
        information, in which case the resulting personally identifiable
        information will be treated as personal information.</p>
      <p>4.3 Other Disclosures.</p>
      <p>(a) We may disclose to outside parties Information you submit and other
        information about you that we collect when we have a good faith belief
        that disclosure is reasonably necessary to (i) comply with applicable
        Law; (ii) protect the safety of any person from death or serious bodily
        injury; (iii) prevent fraud or abuse of us or our customers and users;
        (iv) to protect our property rights; or (v) enforce the Terms of Use,
        including investigation of potential violations. If we provide your
        information to a law enforcement agency as set forth above, we will
        remove encryption before providing it.</p>
      <p>(b) If we are involved in a merger, acquisition, or sale of all or a
        portion of our assets, your information may be transferred as part of
        that transaction, but we will notify you (for example, via email and/or
        a prominent notice on our website) of any change in control or use of
        your Information or if either become subject to a different privacy
        policy. We will also notify you of choices you may have regarding the
        information.</p>
      <p>5. Accessing Your Information; Your Choices.</p>
      <p>5.1 If you have an account with us, you have the ability to review and
        update your personal information online by logging into your account and
        editing your account profile. More information about how to contact us
        is provided below. If you have an account with us, you also may close
        your account at any time through the Platform. After you close your
        account, you will not be able to sign in to the Platform or access any
        of your personal information. If you close your account, we may still
        retain (a) any non-personally identifiable information, and (b) certain
        personal information associated with your account, if retention is
        reasonably necessary to comply with our legal obligations, meet
        regulatory requirements, resolve disputes, prevent fraud and abuse or
        enforce the Terms of Use. Information stored in routine backups may also
        be retained for the period those backups are retained in the ordinary
        course of business.</p>
      <p>5.2 You can choose not to provide us with certain information, but that
        may result in you being unable to use certain features of the Platform
        and Services because that information may be required in order for you
        to register as a customer, purchase Services, obtain customer support,
        or initiate other transactions.</p>
      <p>5.3 When you register on the Platform, you consent to receive email
        messages from us. You may modify this consent later by clicking on
        Unsubscribe at the bottom of the emails, and following the instructions
        to unsubscribe. It may take up to 10 days for us to process an opt-out
        request. We may send you transactional and relationship emails, such as
        service announcements, administrative notices, and surveys, without
        offering you the opportunity to opt out of receiving them. Please note
        that changing information in your account, or otherwise opting out of
        receipt of promotional email communications, will only affect future
        activities or communications from us . If we have already provided your
        information to a third party (such as a service provider) before you
        have changed your preferences or updated your information, you may have
        to change you preferences directly with that third party.</p>
      <p>5.4 If we develop mobile applications in the future, these applications
        may also deliver notifications to your phone or mobile device. You can
        disable these notifications by modifying the notifications settings
        associated with your account or by deleting the relevant
        application.</p>
      <p>5.5 Most web browsers are set to accept cookies by default. If you
        prefer, you can usually choose to set your browser to remove or reject
        browser cookies. Please note that if you choose to remove or reject
        cookies, this could affect the availability and functionality of the
        Platform and Services.</p>
      <p>6. Security. We use reasonable methods, consistent with industry
        practices, to protect the confidentiality of your information, including
        administrative, physical and technical methods. Information you submit
        to or receive from our Service is sent using an encrypted TLS (SSL)
        connection. No method of transmission over the internet is completely
        secure, therefore we cannot and do not guarantee the security of your
        information.</p>
      <p>7. Our Policy Toward Children. The Service is not directed or available
        to persons under 18. We do not knowingly collect information from
        children under 18. If a parent or guardian becomes aware that his or her
        child has submitted Information in violation of our policies and has
        provided us with information without their consent, he or she should
        contact us at service@parsleycooks.com. If we become aware that a child
        under 18 has provided us with Information, we will take steps to delete
        that Information from our records, subject to any retention reasonably
        necessary to comply with our legal obligations, meet regulatory
        requirements, resolve disputes, prevent fraud and abuse or enforce the
        Agreement.</p>
      <p>8. Changes to our Privacy Policy. We may make changes to this Privacy
        Policy from time to time. If this Privacy Policy is modified, we will
        update this page and the revision date.</p>
      <p>9. California. California Civil Code Section 1798.83, known as
        the &ldquo;Shine The Light&rdquo; law, permits our customers who are
        California residents to request and obtain from us a list of what
        personal information (if any) we disclosed to third parties for direct
        marketing purposes in the preceding calendar year and the names and
        addresses of those third parties. Requests may be made only once a year
        and are free of charge. Under Section 1798.83, we currently do not share
        any personal information with third parties for their direct marketing
        purposes.</p>
      <p>10. Contacting Us. To ask questions about this Privacy Policy, our
        privacy practices, your Information, or anything relating to the
        Service, including requests that we unsubscribe you from communications,
        you may email us at our user contact email:
        service@parsleycooks.com.</p>
      <p>&nbsp;</p>


    </div>;
  }

}
