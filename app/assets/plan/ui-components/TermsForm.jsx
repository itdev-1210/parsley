import React, { Component } from "react";
import { withRouter } from 'react-router';

import { post } from 'common/webapi/utils';

import { Grid, Row, Col, Button } from 'react-bootstrap';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import AcceptTermsModal from './AcceptTermsModal';

@withRouter
export default class TermsForm extends Component {

  onAccept = () => {
    // only usage of this endpoint; replace with accept_terms
    post('/plan/update', { acceptsTerms: true })
        .then(response => this.props.onAccept());
  };

  render() {
    return <Grid fluid>
      <Row>
        <Col xs={12} md={2}></Col>
        <Col xs={12} md={6}>
          <h2>Accept Terms and Conditions</h2>

          <AcceptTermsModal onAccept={this.onAccept} />
          <Button onClick={this.props.goBack}>Go back</Button>
          <br /><br /><br />
        </Col>
        <Col xs={12} md={4}></Col>
      </Row>
    </Grid>;
  }

}
