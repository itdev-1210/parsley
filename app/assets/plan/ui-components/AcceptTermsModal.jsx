import React, { Component } from "react";

import { Modal, Button } from 'react-bootstrap';

import TermsAndConditions from './TermsAndConditions';

export default class AcceptTermsModal extends Component {

  render() {

    const { onHide, onAccept } = this.props;
    
    return <Modal show onHide={onHide}>
      <Modal.Header>
        <Modal.Title>Terms and Conditions</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <TermsAndConditions />
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="success" onClick={() => onAccept()}>
          accept
        </Button>
      </Modal.Footer>
    </Modal>;
  }

}
