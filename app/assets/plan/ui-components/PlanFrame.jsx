// @flow
import React, { Component, type Node } from 'react';
import type { Map } from 'immutable';

import { connect } from 'react-redux';

import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';
import AccountDropdown from 'common/ui-components/AccountDropdown';
import { Nav } from 'react-bootstrap';

type Props = {
  children: Node,
  userInfo: Map<string, $FlowTODO>,
}

@connect(state => ({
  userInfo: state.get('userInfo'),
}))
export default class PlanFrame extends Component<Props> {
  render() {
    const { children, userInfo } = this.props;
    return <div>
      <ParsleyNavbar>
        <Nav navbar pullRight>
          <AccountDropdown
              userInfo={userInfo}
          />
        </Nav>
      </ParsleyNavbar>
      {children}
    </div>;
  }
}