// @flow

import { newScreen } from 'common/utils/analytics';

import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import SelectPlanForm from 'common/ui-components/SelectPlanForm';
import TermsForm from './TermsForm';
import PaymentForm from './PaymentForm';
import { isUpstreamActivation } from '../plan.jsx';

import { getUserInfo } from 'common/webapi/userEndpoints';
import { getStripeProducts } from 'common/webapi/stripeEndpoints';
import { availableSubscriptionLevels } from 'common/utils/features';
import type { ReactRouterProps } from 'common/utils/routing';
import { noticeError } from 'common/utils/errorAnalytics';

type Props = {
  userInfo: $FlowTODO,
  stripeProducts: $FlowTODO,
} & ReactRouterProps

type StepId = 'select' | 'terms' | 'payment';
type State = {
  step: StepId,
  selectedPlan: ?string,
}

@connect(
  state => ({
    userInfo: state.get('userInfo'),
    stripeProducts: state.getIn(['stripeInfo', 'products']),
  }),
)
@withRouter
export default class Plan extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      step: 'select',
      selectedPlan: null,
    };
  }

  savePlan = (values: { plan: string }) => this.setState({
    step: 'terms',
    selectedPlan: values.plan,
  });

  acceptTerms = () => this.setState({ step: 'payment' });

  render() {
    const { pathname } = this.props.location;
    const { step, selectedPlan } = this.state;
    const { userInfo, stripeProducts } = this.props;

    let allowedStripeProducts = stripeProducts;

    if (stripeProducts && isUpstreamActivation(pathname)) {
      allowedStripeProducts = stripeProducts.filter(
          prod => {
            const supportedLevels = availableSubscriptionLevels('MULTI_LOCATION');
            return supportedLevels && supportedLevels.includes(prod.get('subscriptionLevel'));
          },
      );
    }

    switch (step) {
      case 'select': return <SelectPlanForm
          stripeProducts={allowedStripeProducts}
          userInfo={userInfo}
          inModal={false}
          onSelect={this.savePlan.bind(this)}
      />;
      case 'terms': return <TermsForm
          onAccept={this.acceptTerms}
          goBack={() => this.setState({ step: 'select' })}
      />;
      case 'payment': return <PaymentForm
          selectedPlan={selectedPlan}
          goBack={() => this.setState({ step: 'select' })}
      />;
      default:
        (step: empty);
        return null;
    }
  }

  changeStep(nextStep: StepId) {
    this.setState({ step: nextStep });
    switch (nextStep) {
      case 'select': newScreen('subscribe_select_plan'); break;
      case 'terms': newScreen('subscribe_terms_and_conditions'); break;
      case 'payment': newScreen('subscribe_confirm'); break;
      default:
        (nextStep: empty);
        break;
    }
  }

  componentWillMount() {
    newScreen('subscribe_select_plan');
    getUserInfo();
    getStripeProducts();
  }
}
