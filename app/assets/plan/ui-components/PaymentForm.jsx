import { newEvent, reportCapterraConversion } from 'common/utils/analytics';
import { subscribe } from 'common/webapi/stripeEndpoints';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import { getUserInfo, getStripeProducts, getStripePublishableKey } from '../webapi/endpoints';

import {
  Grid, Row, Col, Button, PageHeader, ButtonToolbar,
} from 'react-bootstrap';
import StripeCheckout from 'react-stripe-checkout';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { isUpstreamActivation } from '../plan.jsx';

@withRouter
@connect(
    (state, ownProps) => {
      const stripeProducts = state.getIn(['stripeInfo', 'products']);
      let selectedPlan, selectedProduct;
      if (stripeProducts) {
        const isSelectedPlan = plan => plan.get('id') === ownProps.selectedPlan;
        selectedPlan = stripeProducts.flatMap(prod => prod.get('plans'))
            .find(isSelectedPlan);
        selectedProduct = stripeProducts.filter(
            prod => prod.get('plans').some(isSelectedPlan));
      }
      return {
        isUpstreamActivation: isUpstreamActivation(ownProps.location.pathname),
        selectedPlan,
        selectedProduct,
        stripePublishableKey: state.getIn(['stripeInfo', 'publishableKey']),
        userInfo: state.get('userInfo'),
        mainInfoLoaded: Boolean(state.get('userInfo')) &&
            Boolean(state.getIn(['stripeInfo', 'products'])) &&
            Boolean(state.getIn(['stripeInfo', 'publishableKey'])),
      };
    },
)
export default class PaymentForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      promoCodeMessage: null,
    };
  }

  componentWillMount() {
    getUserInfo();
    getStripeProducts();
    getStripePublishableKey();
  }

  subscribe = (token) => {

    let customerInfo = this.props.userInfo;

    // hopefully the following actions take long enough for this to complete
    reportCapterraConversion();

    if (token) {
      newEvent('add_payment_info', {});
      customerInfo = customerInfo.set('token', token.id);
    }

    newEvent('subscribe_completed', {
      level: this.props.selectedProduct && this.props.selectedProduct.get('name'),
    });

    return subscribe(this.props.selectedPlan.get('id'), customerInfo.toJS())
        .then(() => {
          // Redirect to main app
          window.location = '/recipes';
        });
  }

  selectedPlanPrice = () => {
    const plan = this.props.selectedPlan;
    if (plan) {
      return plan.get('price');
    }
    return null;
  }

  monthlyPayment() {
    return this.selectedPlanPrice();
  }

  trialPeriod() {
    const {
      selectedPlan, userInfo,
      isUpstreamActivation,
    } = this.props;

    // no free trial for multi location
    if (isUpstreamActivation) return 0;

    if (userInfo.get('newSubscriber') && selectedPlan) {
      return selectedPlan.get('trialPeriodDays') || 0;
    }
    return 0;
  }

  paymentRequired = () => {
    if (this.trialPeriod() > 0) return 0;
    else return this.monthlyPayment();
  }

  render() {

    const { selectedProduct, stripePublishableKey, isUpstreamActivation } = this.props;
    const trialPeriod = this.trialPeriod();

    if (!this.props.mainInfoLoaded) {
      return <Grid fluid><LoadingSpinner /></Grid>;
    }

    return <Grid fluid>
      <Row>
        <Col xs={12} md={2}/>
        <Col xs={12} md={6}>
          <PageHeader>Subscription Confirmation</PageHeader>
          <div className="block-text">
            <p>
              Selected Plan: {selectedProduct && selectedProduct.get('name')}
              &nbsp;&nbsp;
              ${this.monthlyPayment() / 100} / month {(this.paymentRequired() > 0) ? null : isUpstreamActivation ? null : `(first ${trialPeriod} days free)`}
            </p>
          </div>

          <ButtonToolbar style={{ margin: '20px 0px 75px', position: 'relative' }}>
            <div style={{ position: 'absolute', marginLeft: '195px' }}>
            <Button onClick={this.props.goBack} style={{ marginRight: '6px'}}>
              Go back
            </Button>
            { (this.paymentRequired() > 0) ?
                <StripeCheckout
                    name="Parsley"
                    description="Monthly Subscription"
                    panelLabel="Sign up"
                    amount={this.paymentRequired()}
                    currency="USD"
                    token={this.subscribe}
                    email={this.props.userInfo.get('email')}
                    ComponentClass="span"
                    allowRememberMe={false}
                    stripeKey={stripePublishableKey}>
                  <Button>Confirm</Button>
                </StripeCheckout>
                : <Button onClick={() => this.subscribe()}>CONFIRM</Button> }
            </div>
          </ButtonToolbar>
        </Col>
        <Col xs={12} md={4}/>
      </Row>
      <br /><br /><br />
    </Grid>;
  }
}
