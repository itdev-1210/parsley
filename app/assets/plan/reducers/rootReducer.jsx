import { combineReducers } from 'redux-immutablejs';
import { reducer as formReducer } from 'redux-form';
import userInfo from 'common/redux/reducers/userInfo';
import stripeInfo from 'common/redux/reducers/stripeInfo';

export default combineReducers({
  form: formReducer,
  userInfo,
  stripeInfo,
});
