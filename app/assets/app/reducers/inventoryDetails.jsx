// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'INVENTORY_DETAILS',
  'INVENTORY_DETAILS_ERROR',
  'INVENTORY_DROP_DETAILS',
]);

ERROR_TYPES.push(actionTypes.INVENTORY_DETAILS_ERROR);

let initialState = null;

const actionsMap = {
  [actionTypes.INVENTORY_DETAILS]: (state, action) => action.content.deets,
  [actionTypes.INVENTORY_DROP_DETAILS]: () => null,
};

type Inventory = $FlowTODO;

export const actionCreators = {
  details(deets: Inventory) {
    return {
      type: actionTypes.INVENTORY_DETAILS,
      content: { deets },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.INVENTORY_DETAILS_ERROR,
      content: { error },
    };
  },

  dropDetails() {
    return { type: actionTypes.INVENTORY_DROP_DETAILS };
  },
};

export default lookupTableReducer(initialState, actionsMap);
