// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'SUPPLIER_DETAILS',
  'SUPPLIER_DETAILS_ERROR',
  'SUPPLIER_DROP_DETAILS',
  'SUPPLIER_SAVED', // unused for now
  'SUPPLIER_SAVE_ERROR',
  'SUPPLIER_DELETED',
  'SUPPLIER_DELETE_ERROR',
]);
ERROR_TYPES.push(
    actionTypes.SUPPLIER_DETAILS_ERROR,
    actionTypes.SUPPLIER_SAVE_ERROR,
    actionTypes.SUPPLIER_DELETE_ERROR,
);

type Supplier = $FlowTODO

export const actionCreators = {

  details(id: number, deets: Supplier) {
    return {
      type: actionTypes.SUPPLIER_DETAILS,
      content: { id, deets },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.SUPPLIER_DETAILS_ERROR,
      content: { error },
    };
  },

  dropDetails() {
    return {
      type: actionTypes.SUPPLIER_DROP_DETAILS,
    };
  },

  saved(id: number) {
    return {
      type: actionTypes.SUPPLIER_SAVED,
      content: { id },
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.SUPPLIER_SAVE_ERROR,
      content: { error },
    };
  },

  deleted(id: number) {
    return {
      type: actionTypes.SUPPLIER_DELETED,
      content: { id },
    };
  },

  deleteError(error: Error) {
    return {
      type: actionTypes.SUPPLIER_DELETE_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.SUPPLIER_DETAILS]: (state, action) => action.content.deets,
  [actionTypes.SUPPLIER_DROP_DETAILS]: (state, action) => initialState,
  [actionTypes.SUPPLIER_DELETED]: (state, action) => initialState,
};

export default lookupTableReducer(initialState, actionsMap);
