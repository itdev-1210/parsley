// @flow
import Immutable from 'immutable';

import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'REVERSE_DEPENDENCIES',
  'DROP_REVERSE_DEPENDENCIES',
  'REVERSE_DEPENDENCIES_ERROR',
]);
ERROR_TYPES.push(actionTypes.REVERSE_DEPENDENCIES_ERROR);

export const actionCreators = {
  received(ingredientId: number, recipes: Array<*>) {
    return {
      type: actionTypes.REVERSE_DEPENDENCIES,
      content: { ingredientId, reverseDependencies: recipes },
    };
  },
  error(error: Error) {
    return {
      type: actionTypes.REVERSE_DEPENDENCIES_ERROR,
      content: { error },
    };
  },
  drop(ingredientId: number) {
    return {
      type: actionTypes.DROP_REVERSE_DEPENDENCIES,
      content: { ingredientId },
    };
  },
};

let initialState = Immutable.Map();

const actionsMap = {
  [actionTypes.REVERSE_DEPENDENCIES]: (state, action) =>
    // $FlowFixMe: no fromJS() typing
    state.set(action.content.ingredientId, Immutable.fromJS(action.content.reverseDependencies).sortBy(i => i.get('name').toLowerCase())),
  [actionTypes.DROP_REVERSE_DEPENDENCIES]: (state, action) => state.delete(action.content.ingredientId),
};

export default lookupTableReducer(initialState, actionsMap);
