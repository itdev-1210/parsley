// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

type Inventory = { id: number, date: string }; // $FlowTODO: use moments?

const actionTypes = keyMirror([
  'INVENTORY_LIST',
  'INVENTORY_LIST_ERROR',
]);

ERROR_TYPES.push(actionTypes.INVENTORY_LIST_ERROR);

let initialState = null;

const actionsMap = {
  [actionTypes.INVENTORY_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.inventories).sortBy(i => -i.get('date')),
};

export const actionCreators = {
  list(inventories: Array<Inventory>) {
    return {
      type: actionTypes.INVENTORY_LIST,
      content: { inventories },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.INVENTORY_LIST_ERROR,
      content: { error },
    };
  },
};

export default lookupTableReducer(initialState, actionsMap);
