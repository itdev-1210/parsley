// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'POS_CSV_DETAILS',
  'POS_CSV_ERROR',

]);
ERROR_TYPES.push(actionTypes.MENU_DETAILS_ERROR);

export const actionCreators = {
  csvDetails(headers: Array<string>, data: Array<*>) {
    return {
      type: actionTypes.POS_CSV_DETAILS,
      content: { headers, data },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.POS_CSV_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.POS_CSV_DETAILS]: (state, action) => Immutable.fromJS(action.content),
  [actionTypes.POS_CSV_ERROR]: (state, action) => null,
};

export default lookupTableReducer(initialState, actionsMap);
