// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'MENU_LIST',
  'MENU_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.MENU_LIST_ERROR);

export const actionCreators = {
  list(menus: SummaryList) {
    return {
      type: actionTypes.MENU_LIST,
      content: { menus },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.MENU_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.MENU_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.menus).sortBy(i => i.get("name").toLowerCase())
};

export default lookupTableReducer(initialState, actionsMap);
