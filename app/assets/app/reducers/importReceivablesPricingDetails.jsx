// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'IMPORT_RECEIVABLES_PRICING_DETAILS',
  'IMPORT_RECEIVABLES_PRICING_ERROR',
  'IMPORT_RECEIVABLES_SUPPLIERS_INFO',

]);
ERROR_TYPES.push(actionTypes.MENU_DETAILS_ERROR);

export const actionCreators = {
  csvDetails(headers: Array<string>, data: Array<*>) {
    return {
      type: actionTypes.IMPORT_RECEIVABLES_PRICING_DETAILS,
      content: { headers, data },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.IMPORT_RECEIVABLES_PRICING_ERROR,
      content: { error },
    };
  },

   multipleSuppliersInfo(deets: Array<*>) {
    return {
      type: actionTypes.IMPORT_RECEIVABLES_SUPPLIERS_INFO,
      content: { deets },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.IMPORT_RECEIVABLES_PRICING_DETAILS]: (state, action) => Immutable.fromJS(action.content),
  [actionTypes.IMPORT_RECEIVABLES_PRICING_ERROR]: (state, action) => null,
  [actionTypes.IMPORT_RECEIVABLES_SUPPLIERS_INFO]: (state, action) => state.merge(
      // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.deets).mapKeys(k => parseInt(k)),
  ),
};

export default lookupTableReducer(initialState, actionsMap);
