// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

import type { ProductInfo } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'PRODUCT_DETAILS',
  'PRODUCT_DETAILS_ERROR',
  'MULTIPLE_PRODUCT_DETAILS',
  'PRODUCT_DROP_DETAILS',

  'PRODUCT_SAVED',
  'PRODUCT_SAVE_ERROR',

  'PRODUCT_DELETED',
  'PRODUCT_DELETE_ERROR',

]);
ERROR_TYPES.push(
    actionTypes.PRODUCT_DETAILS_ERROR,
    actionTypes.PRODUCT_SAVE_ERROR,
    actionTypes.PRODUCT_DELETE_ERROR,
);

export const actionCreators = {
  details(id: number, deets: ProductInfo) {
    return {
      type: actionTypes.PRODUCT_DETAILS,
      content: { id, deets },
    };
  },

  multipleDetails(deets: { [number]: ProductInfo }) { // not really number keys, but parseable as such
    return {
      type: actionTypes.MULTIPLE_PRODUCT_DETAILS,
      content: { deets },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.PRODUCT_DETAILS_ERROR,
      content: { error },
    };
  },

  dropDetails(id: number) {
    return {
      type: actionTypes.PRODUCT_DROP_DETAILS,
      id,
    };
  },

  saved(id: number) {
    return {
      type: actionTypes.PRODUCT_SAVED,
      content: { id },
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.PRODUCT_SAVE_ERROR,
      content: { error },
    };
  },

  deleted(id: number) {
    return {
      type: actionTypes.PRODUCT_DELETED,
      content: { id },
    };
  },

  deleteError(error: Error) {
    return {
      type: actionTypes.PRODUCT_DELETE_ERROR,
      content: { error },
    };
  },
};

import lookupTableReducer from 'common/redux/lookupTableReducer';

let initialState = Immutable.Map();

const actionsMap = {
  [actionTypes.PRODUCT_DETAILS]: (state, action) => {
    let newState = state || Immutable.Map();
    newState = newState.set(parseInt(action.content.id), action.content.deets);
    return newState;
  },
  [actionTypes.MULTIPLE_PRODUCT_DETAILS]: (state, action) => state.merge(
    Immutable.Map(action.content.deets).mapKeys(k => parseInt(k)),
  ),
  [actionTypes.PRODUCT_DROP_DETAILS]: (state, action) => state.delete(parseInt(action.id)),
  [actionTypes.PRODUCT_DELETED]: (state, action) => state.delete(parseInt(action.content.id)),
};

export default lookupTableReducer(initialState, actionsMap);
