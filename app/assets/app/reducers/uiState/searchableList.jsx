// @flow
import Immutable from 'immutable';

import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
  'CHECK_ITEM',
  'UNCHECK_ITEM',
  'UNCHECK_ALL',
  'SET_CHECKED_ITEMS',
  'SET_SEARCH_TERM',
  'SET_SELECTED_TAGS',
  'ENTER_PAGE',
  'LEAVE_PAGE',
  'SHOW_DELETE_CONFIRMATION',
  'HIDE_DELETE_CONFIRMATION',
  'DELETE_ERROR_MESSAGE',
]);

type ListName = string;

type SelectedTag = {
  id: number,
  type: string,
};

export const actionCreators = {
  checkItem(listName: ListName, itemId: number) {
    return { type: [UIActionTypes.CHECK_ITEM], listName, content: { itemId } };
  },
  uncheckItem(listName: ListName, itemId: number) {
    return { type: [UIActionTypes.UNCHECK_ITEM], listName, content: { itemId } };
  },

  uncheckAll(listName: ListName) {
    return { type: [UIActionTypes.UNCHECK_ALL], listName };
  },

  setCheckedItems(listName: ListName, checkedItems: Immutable.Set<number>) {
    return { type: [UIActionTypes.SET_CHECKED_ITEMS], listName, content: { checkedItems } };
  },

  setSearchTerm(listName: ListName, searchTerm: string) {
    return { type: [UIActionTypes.SET_SEARCH_TERM], listName, content: { searchTerm } };
  },

  setSelectedTags(listName: ListName, selectedTags: Immutable.Set<SelectedTag>) {
    return { type: [UIActionTypes.SET_SELECTED_TAGS], listName, content: { selectedTags } };
  },

  enterPage(listName: ListName) {
    return { type: UIActionTypes.ENTER_PAGE, listName };
  },

  leavePage(listName: ListName) {
    return { type: UIActionTypes.LEAVE_PAGE, listName };
  },

  showDeleteConfirmation(listName: ListName, itemsToDelete: Immutable.List<number>) {
    return { type: UIActionTypes.SHOW_DELETE_CONFIRMATION, listName, items: itemsToDelete };
  },
  hideDeleteConfirmation(listName: ListName, itemsToDelete: Immutable.List<number>) {
    return { type: UIActionTypes.HIDE_DELETE_CONFIRMATION, listName };
  },
  deleteErrorMessage(listName: ListName, message: string) {
    return { type: UIActionTypes.DELETE_ERROR_MESSAGE, listName, message };
  },
};

const perPageInitialState = Immutable.fromJS({
  checkedItems: Immutable.Set(),
  searchTerm: '',
  selectedTags: Immutable.Set(),
});

const actionsMap = {
  [UIActionTypes.CHECK_ITEM]:
      (state, action) => state.has(action.listName)
          ? state.updateIn([action.listName, 'checkedItems'], s => s.add(action.content.itemId))
          : state,
  [UIActionTypes.UNCHECK_ITEM]:
      (state, action) => state.has(action.listName)
          ? state.updateIn([action.listName, 'checkedItems'], s => s.delete(action.content.itemId))
          : state,
  [UIActionTypes.UNCHECK_ALL]:
          (state, action) => state.setIn([action.listName, 'checkedItems'], Immutable.Set()),
  [UIActionTypes.SET_CHECKED_ITEMS]:
      (state, action) => state.has(action.listName)
          ? state.setIn([action.listName, 'checkedItems'], action.content.checkedItems)
          : state,
  [UIActionTypes.SET_SEARCH_TERM]:
      (state, action) => state.has(action.listName)
          ? state.setIn([action.listName, 'searchTerm'], action.content.searchTerm)
          : state,
  [UIActionTypes.SET_SELECTED_TAGS]:
      (state, action) => state.has(action.listName)
          ? state.setIn([action.listName, 'selectedTags'], action.content.selectedTags)
          : state,
  [UIActionTypes.SHOW_DELETE_CONFIRMATION]:
      (state, action) => state.has(action.listName)
          ? state.setIn([action.listName, 'deleteConfirmationItems'], action.items)
          : state,
  [UIActionTypes.HIDE_DELETE_CONFIRMATION]:
      (state, action) => state.has(action.listName)
          ? state.deleteIn([action.listName, 'deleteConfirmationItems'])
          : state,
  [UIActionTypes.DELETE_ERROR_MESSAGE]:
      (state, action) => state.has(action.listName)
          ? state.setIn([action.listName, 'deleteErrorMessage'], action.message)
          : state,
  [UIActionTypes.ENTER_PAGE]:
      (state, action) => state.set(action.listName, perPageInitialState),
  [UIActionTypes.LEAVE_PAGE]:
      (state, action) => state.delete(action.listName),
};

export default lookupTableReducer(Immutable.Map(), actionsMap);
