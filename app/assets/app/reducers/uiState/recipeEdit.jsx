// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';
import Immutable from 'immutable';
import { UIActionTypes as IngredientEditUIActionTypes } from './ingredientEdit';
import keyMirror from 'common/utils/keyMirror';
import type { NewUnitDescriptor } from '../../ui-components/modals/NewUnitModal';

const UIActionTypes = keyMirror([
  'RECIPE_EDIT_LEAVE',
  'RECIPE_EDIT_OPEN_INGREDIENT_COST_MODAL',
  'RECIPE_EDIT_CLOSE_INGREDIENT_COST_MODAL',
  'RECIPE_EDIT_OPEN_INGREDIENT_NUTRIENT_MODAL',
  'RECIPE_EDIT_CLOSE_INGREDIENT_NUTRIENT_MODAL',
  'RECIPE_EDIT_OPEN_PRINT_MODAL',
  'RECIPE_EDIT_CLOSE_PRINT_MODAL',
  'RECIPE_EDIT_OPEN_PRINT_NUTRITION_MODAL',
  'RECIPE_EDIT_CLOSE_PRINT_NUTRITION_MODAL',
  'RECIPE_EDIT_OPEN_RESIZE_MODAL',
  'RECIPE_EDIT_CLOSE_RESIZE_MODAL',
  'RECIPE_EDIT_OPEN_COST_MODAL',
  'RECIPE_EDIT_CLOSE_COST_MODAL',
  'RECIPE_EDIT_OPEN_NEW_INGREDIENT_MODAL',
  'RECIPE_EDIT_CLOSE_NEW_INGREDIENT_MODAL',
  'RECIPE_EDIT_OPEN_NEW_UNIT_MODAL',
  'RECIPE_EDIT_CLOSE_NEW_UNIT_MODAL',

]);

export const actionCreators = {
  openIngredientCostModal(ingredientId: number) {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_INGREDIENT_COST_MODAL],
      content: {
        ingredientId,
      },
    };
  },
  closeIngredientCostModal() {
    return { type: [UIActionTypes.RECIPE_EDIT_CLOSE_INGREDIENT_COST_MODAL] };
  },

  openIngredientNutrientModal(ingredientId: number, preppedIngredientIds: Array<number>) {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_INGREDIENT_NUTRIENT_MODAL],
      content: {
        ingredientId,
        preppedIngredientIds,
      },
    };
  },
  closeIngredientNutrientModal() {
    return { type: [UIActionTypes.RECIPE_EDIT_CLOSE_INGREDIENT_NUTRIENT_MODAL] };
  },

  openPrintModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_PRINT_MODAL],
    };
  },
  closePrintModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_PRINT_MODAL],
    };
  },

  openPrintNutritionModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_PRINT_NUTRITION_MODAL],
    };
  },
  closePrintNutritionModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_PRINT_NUTRITION_MODAL],
    };
  },

  openResizeModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_RESIZE_MODAL],
    };
  },
  closeResizeModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_RESIZE_MODAL],
    };
  },

  openCostModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_COST_MODAL],
    };
  },
  closeCostModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_COST_MODAL],
    };
  },

  openNewIngredientModal(newIngredient: string) {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_NEW_INGREDIENT_MODAL],
      content: {
        newIngredient,
      },
    };
  },
  closeNewIngredientModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_NEW_INGREDIENT_MODAL],
    };
  },

  leaveRecipeEdit() {
    return { type: [UIActionTypes.RECIPE_EDIT_LEAVE] };
  },

  openNewUnitModal(newUnit: NewUnitDescriptor) {
    return {
      type: [UIActionTypes.RECIPE_EDIT_OPEN_NEW_UNIT_MODAL],
      content: {
        newUnit
      }
    };
  },
  closeNewUnitModal() {
    return {
      type: [UIActionTypes.RECIPE_EDIT_CLOSE_NEW_UNIT_MODAL]
    };
  },
};

const initialState = Immutable.fromJS({
  showIngredientCostModal: false,
  showIngredientCostModalIsHidden: false,
  showIngredientNutrientModal: false,
  showPrintNutritionModal: false,
  showPrintModal: false,
  showResizeModal: false,
  showCostModal: false,
  showNewIngredientModal: false,
  showNewIngredientModalIsHidden: false,
  showNewUnitModal: false,
});

const actionsMap = {
  [UIActionTypes.RECIPE_EDIT_LEAVE](state, action) {
    return initialState;
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_INGREDIENT_COST_MODAL](state, action) {
    return state
        .set('showIngredientCostModal', action.content.ingredientId);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_INGREDIENT_COST_MODAL](state, action) {
    return state
        .set('showIngredientCostModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_INGREDIENT_NUTRIENT_MODAL](state, action) {
    return state
        // $FlowFixMe: no fromJS() typing
        .set('showIngredientNutrientModal', Immutable.fromJS(action.content).delete('type')); // ingredientId and preppedIngredientId
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_INGREDIENT_NUTRIENT_MODAL](state, action) {
    return state
        .set('showIngredientNutrientModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_PRINT_MODAL](state, action) {
    return state
        .set('showPrintModal', true);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_PRINT_MODAL](state, action) {
    return state
        .set('showPrintModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_PRINT_NUTRITION_MODAL](state, action) {
    return state
        .set('showPrintNutritionModal', true);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_PRINT_NUTRITION_MODAL](state, action) {
    return state
        .set('showPrintNutritionModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_RESIZE_MODAL](state, action) {
    return state
        .set('showResizeModal', true);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_RESIZE_MODAL](state, action) {
    return state
        .set('showResizeModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_COST_MODAL](state, action) {
    return state
        .set('showCostModal', true);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_COST_MODAL](state, action) {
    return state
        .set('showCostModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_NEW_INGREDIENT_MODAL](state, action) {
    return state
        .set('showNewIngredientModal', action.content.newIngredient);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_NEW_INGREDIENT_MODAL](state, action) {
    return state
        .set('showNewIngredientModal', false);
  },
  [UIActionTypes.RECIPE_EDIT_OPEN_NEW_UNIT_MODAL](state, action) {
    return state
        .set('showNewUnitModal', action.content.newUnit);
  },
  [UIActionTypes.RECIPE_EDIT_CLOSE_NEW_UNIT_MODAL](state) {
    return state
        .set('showNewUnitModal', false);
  },

  [IngredientEditUIActionTypes.INGREDIENT_EDIT_OPEN_NEW_SUPPLIER_MODAL](state) {
    return state
        .set('showNewIngredientModalIsHidden', true)
        .set('showIngredientCostModalIsHidden', true);
  },
  [IngredientEditUIActionTypes.INGREDIENT_EDIT_CLOSE_NEW_SUPPLIER_MODAL](state) {
    return state
        .set('showNewIngredientModalIsHidden', false)
        .set('showIngredientCostModalIsHidden', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
