// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
  'INGREDIENT_LIST_OPEN_ADD_TO_CATEGORY_MODAL',
  'INGREDIENT_LIST_CLOSE_ADD_TO_CATEGORY_MODAL',
  'INGREDIENT_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL',
  'INGREDIENT_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL'
]);

export const actionCreators = {
  openAddToCategoryModal(ingredients: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.INGREDIENT_LIST_OPEN_ADD_TO_CATEGORY_MODAL],
      content: { ingredients },
    };
  },
  closeAddToCategoryModal() {
    return {
      type: [UIActionTypes.INGREDIENT_LIST_CLOSE_ADD_TO_CATEGORY_MODAL],
    };
  },

  openRemoveFromCategoryModal(ingredients: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.INGREDIENT_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL],
      content: { ingredients },
    };
  },
  closeRemoveFromCategoryModal() {
    return {
      type: [UIActionTypes.INGREDIENT_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL],
    };
  }
};

const initialState = Immutable.fromJS({
  showAddToCategoryModal: false,
  showRemoveFromCategoryModal: false,
});

const actionsMap = {
  [UIActionTypes.INGREDIENT_LIST_OPEN_ADD_TO_CATEGORY_MODAL](state, action) {
    return state
      .set('showAddToCategoryModal', true)
      .set('addingToCategoryIngredients', action.content.ingredients);
  },
  [UIActionTypes.INGREDIENT_LIST_CLOSE_ADD_TO_CATEGORY_MODAL](state) {
    return state.set('showAddToCategoryModal', false);
  },

  [UIActionTypes.INGREDIENT_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL](state, action) {
    return state
      .set('showRemoveFromCategoryModal', true)
      .set('removingFromCategoryIngredients', action.content.ingredients);
  },
  [UIActionTypes.INGREDIENT_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL](state) {
    return state.set('showRemoveFromCategoryModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
