// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';
import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';
import type { NewIngredientDescriptor } from '../../ui-components/modals/NewIngredientModal';
import type { NewUnitDescriptor } from '../../ui-components/modals/NewUnitModal';
import type { CsvImportData } from '../../ui-components/SupplierImportIngredients';

const UIActionTypes = keyMirror([
  'SUPPLIER_EDIT_OPEN_NEW_INGREDIENT_MODAL',
  'SUPPLIER_EDIT_CLOSE_NEW_INGREDIENT_MODAL',
  'SUPPLIER_EDIT_OPEN_NEW_UNIT_MODAL',
  'SUPPLIER_EDIT_CLOSE_NEW_UNIT_MODAL',
  'SUPPLIER_EDIT_OPEN_IMPORT_INGREDIENTS_MODAL',
  'SUPPLIER_EDIT_CLOSE_IMPORT_INGREDIENTS_MODAL',
]);

export const actionCreators = {
  openNewIngredientModal(newIngredient: NewIngredientDescriptor) {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_OPEN_NEW_INGREDIENT_MODAL],
      content: {
        newIngredient,
      },
    };
  },
  closeNewIngredientModal() {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_CLOSE_NEW_INGREDIENT_MODAL],
    };
  },

  openNewUnitModal(newUnit: NewUnitDescriptor) {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_OPEN_NEW_UNIT_MODAL],
      content: {
        newUnit,
      },
    };
  },
  closeNewUnitModal() {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_CLOSE_NEW_UNIT_MODAL],
    };
  },

  openImportIngredientsModal(ingredients: Immutable.List<CsvImportData>, fileName: string) {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_OPEN_IMPORT_INGREDIENTS_MODAL],
      content: {
        ingredients,
        fileName,
      },
    };
  },
  closeImportIngredientsModal() {
    return {
      type: [UIActionTypes.SUPPLIER_EDIT_CLOSE_IMPORT_INGREDIENTS_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showNewIngredientModal: false,
  showNewUnitModal: false,
  showImportIngredientsModal: null,
});

const actionsMap = {
  [UIActionTypes.SUPPLIER_EDIT_OPEN_NEW_INGREDIENT_MODAL](state, action) {
    return state
        .set('showNewIngredientModal', action.content.newIngredient);
  },
  [UIActionTypes.SUPPLIER_EDIT_CLOSE_NEW_INGREDIENT_MODAL](state) {
    return state
        .set('showNewIngredientModal', false);
  },
  [UIActionTypes.SUPPLIER_EDIT_OPEN_NEW_UNIT_MODAL](state, action) {
    return state
        .set('showNewUnitModal', action.content.newUnit);
  },
  [UIActionTypes.SUPPLIER_EDIT_CLOSE_NEW_UNIT_MODAL](state) {
    return state
        .set('showNewUnitModal', false);
  },
  [UIActionTypes.SUPPLIER_EDIT_OPEN_IMPORT_INGREDIENTS_MODAL](state, action) {
    return state
        .set('showImportIngredientsModal', action.content.ingredients);
  },
  [UIActionTypes.SUPPLIER_EDIT_CLOSE_IMPORT_INGREDIENTS_MODAL](state) {
    return state
        .set('showImportIngredientsModal', null);
  },
};

export default lookupTableReducer(initialState, actionsMap);
