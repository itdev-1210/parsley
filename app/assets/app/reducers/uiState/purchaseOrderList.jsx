// @flow
// State: a set of selected supplier ids in <PurchaseOrderList/>
import Immutable from 'immutable';

import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
  'PURCHASE_ORDER_LIST_CHECK_ITEM',
  'PURCHASE_ORDER_LIST_UNCHECK_ITEM',
  'PURCHASE_ORDER_LIST_CLEAR',
  'PURCHASE_ORDER_LIST_OPEN_RECEIPTS_IMPORT_MODAL',
  'PURCHASE_ORDER_LIST_CLOSE_RECEIPTS_IMPORT_MODAL',
  'PURCHASE_ORDER_LIST_SHOW_RECEIVING',
  'PURCHASE_ORDER_LIST_HIDE_RECEIVING',
]);

export const actionCreators = {
  showReceiving() {
    return { type: UIActionTypes.PURCHASE_ORDER_LIST_SHOW_RECEIVING };
  },
  hideReceiving() {
    return { type: UIActionTypes.PURCHASE_ORDER_LIST_HIDE_RECEIVING };
  },
  checkItem(itemId: number) {
    return { type: [UIActionTypes.PURCHASE_ORDER_LIST_CHECK_ITEM], content: { itemId } };
  },
  uncheckItem(itemId: number) {
    return { type: [UIActionTypes.PURCHASE_ORDER_LIST_UNCHECK_ITEM], content: { itemId } };
  },
  clear() {
    return { type: [UIActionTypes.PURCHASE_ORDER_LIST_CLEAR] };
  },
  openReceiptsImportModal() {
    return {
      type: [UIActionTypes.PURCHASE_ORDER_LIST_OPEN_RECEIPTS_IMPORT_MODAL],
    };
  },
  closeReceiptsImportModal() {
    return {
      type: [UIActionTypes.PURCHASE_ORDER_LIST_CLOSE_RECEIPTS_IMPORT_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showImportReceiptsModal: false,
  showReceiving: false,
  selectedSuppliers: Immutable.Set(),
});

const actionsMap = {
  [UIActionTypes.PURCHASE_ORDER_LIST_SHOW_RECEIVING]:
    (state, action) => state.set('showReceiving', true),
  [UIActionTypes.PURCHASE_ORDER_LIST_HIDE_RECEIVING]:
    (state, action) => state.set('showReceiving', false),
  [UIActionTypes.PURCHASE_ORDER_LIST_CHECK_ITEM]:
    (state, action) => state.updateIn(['selectedSuppliers'], s => s.add(action.content.itemId)),
  [UIActionTypes.PURCHASE_ORDER_LIST_UNCHECK_ITEM]:
    (state, action) => state.updateIn(['selectedSuppliers'], s => s.delete(action.content.itemId)),
  [UIActionTypes.PURCHASE_ORDER_LIST_CLEAR]:
    (state, action) => state.setIn(['selectedSuppliers'], Immutable.Set()),
  [UIActionTypes.PURCHASE_ORDER_LIST_OPEN_RECEIPTS_IMPORT_MODAL]:
    (state, action) => state.set('showImportReceiptsModal', true),
  [UIActionTypes.PURCHASE_ORDER_LIST_CLOSE_RECEIPTS_IMPORT_MODAL]:
    (state, action) => state.set('showImportReceiptsModal', false),

};

export default lookupTableReducer(initialState, actionsMap);
