// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';

import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';
import type { BaseOrder } from '../productionOrder';

const UIActionTypes = keyMirror([
  "ORDER_EDIT_LEAVE",
  "ORDER_EDIT_OPEN_SHOPPING_LIST",
  "ORDER_EDIT_CLOSE_SHOPPING_LIST",
  "ORDER_EDIT_PRINT_PREP",
  "ORDER_EDIT_PRINT_PREP_COMPLETE", // complete meaning, it's finished
  "ORDER_EDIT_PRINT_SUB_RECIPES",
  "ORDER_EDIT_PRINT_SUB_RECIPES_COMPLETE",
  "ORDER_EDIT_PRINT_RECIPES",
  "ORDER_EDIT_PRINT_RECIPES_COMPLETE",
  "ORDER_EDIT_PRINT_MENU",
  "ORDER_EDIT_PRINT_MENU_COMPLETE",
  "ORDER_EDIT_OPEN_PRINT_OPTION_MODAL",
  "ORDER_EDIT_CLOSE_PRINT_OPTION_MODAL",
  "ORDER_EDIT_OPEN_PRINT_RECIPE_MODAL",
  "ORDER_EDIT_CLOSE_PRINT_RECIPE_MODAL",
  "ORDER_EDIT_OPEN_COST_MODAL",
  "ORDER_EDIT_CLOSE_COST_MODAL",
  "ORDER_EDIT_OPEN_SEND_MODAL",
  "ORDER_EDIT_CLOSE_SEND_MODAL",
  "ORDER_EDIT_EMAIL",
  "ORDER_EDIT_EMAIL_COMPLETE",
  "ORDER_EDIT_OPEN_LOAD_MENU_MODAL",
  "ORDER_EDIT_CLOSE_LOAD_MENU_MODAL",
  "ORDER_EDIT_OPEN_LOAD_MENU_CONFIRM",
  "ORDER_EDIT_CLOSE_LOAD_MENU_CONFIRM",
]);

export var actionCreators = {
  openShoppingListModal(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_SHOPPING_LIST],
      content: {
        baseOrder
      },
    }
  },
  closeShoppingListModal() {
    return { type: [UIActionTypes.ORDER_EDIT_CLOSE_SHOPPING_LIST] }
  },

  openCostModal(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_COST_MODAL],
      content: {
        baseOrder
      },
    }
  },
  closeCostModal() {
    return {
      type: [UIActionTypes.ORDER_EDIT_CLOSE_COST_MODAL],
    }
  },

  leaveOrderEdit() {
    return { type: [UIActionTypes.ORDER_EDIT_LEAVE] }
  },
  openPrintOptionModal(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_PRINT_OPTION_MODAL],
      content: {
        baseOrder,
      },
    };
  },
  closePrintOptionModal() {
    return { type: [UIActionTypes.ORDER_EDIT_CLOSE_PRINT_OPTION_MODAL] }
  },
  openPrintRecipeModal(selectedPrintOptions: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_PRINT_RECIPE_MODAL],
      content: {
        selectedPrintOptions,
      },
    };
  },
  closePrintRecipeModal() {
    return { type: [UIActionTypes.ORDER_EDIT_CLOSE_PRINT_RECIPE_MODAL] }
  },
  printPrep(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_PRINT_PREP],
      content: {
        baseOrder
      },
    }
  },
  printPrepComplete() {
    return { type: [UIActionTypes.ORDER_EDIT_PRINT_PREP_COMPLETE] }
  },
  printSubRecipes(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_PRINT_SUB_RECIPES],
      content: { baseOrder },
    }
  },
  printSubRecipesComplete() {
    return { type: [UIActionTypes.ORDER_EDIT_PRINT_SUB_RECIPES_COMPLETE] }
  },
  printRecipes(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_PRINT_RECIPES],
      content: {
        baseOrder
      },
    }
  },
  printRecipesComplete() {
    return { type: [UIActionTypes.ORDER_EDIT_PRINT_RECIPES_COMPLETE] }
  },
  printMenu() {
    return { type: [UIActionTypes.ORDER_EDIT_PRINT_MENU] }
  },
  completePrintMenu() {
    return { type: [UIActionTypes.ORDER_EDIT_PRINT_MENU_COMPLETE] }
  },
  openSendModal(supplierId: number, userEditedOrder: $FlowTODO) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_SEND_MODAL],
      content: { supplierId, userEditedOrder },
    }
  },
  closeSendModal() {
    return {
      type: [UIActionTypes.ORDER_EDIT_CLOSE_SEND_MODAL],
    }
  },
  email(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_EDIT_EMAIL],
      content: {
        baseOrder
      },
    } 
  },
  emailComplete() {
    return {
      type: [UIActionTypes.ORDER_EDIT_EMAIL_COMPLETE],
    }
  },
  openLoadMenuModal() {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_LOAD_MENU_MODAL],
    }
  },
  closeLoadMenuModal() {
    return {
      type: [UIActionTypes.ORDER_EDIT_CLOSE_LOAD_MENU_MODAL],
    }
  },
  openLoadMenuConfirm(selectedMenuId: number) {
    return {
      type: [UIActionTypes.ORDER_EDIT_OPEN_LOAD_MENU_CONFIRM],
      content: {
        selectedMenuId
      }
    }
  },
  closeLoadMenuConfirm() {
    return {
      type: [UIActionTypes.ORDER_EDIT_CLOSE_LOAD_MENU_CONFIRM],
    }
  },
};

const initialState = Immutable.fromJS({
  showShoppingList: false,
  // show_print_options, show_print_recipe_options
  printStep: '',
  selectedPrintOptions: Immutable.Set([]),
  printPrep: false,
  printMenu: false,
  printSubRecipes: false,
  printRecipes: false,
  showCostModal: false,
  showSendModal: false,
  sendingEmail: false,
  showLoadMenuModal: false,
  showLoadMenuConfirm: false,
});

const actionsMap = {
  [UIActionTypes.ORDER_EDIT_LEAVE](state, action) {
    return initialState;
  },
  [UIActionTypes.ORDER_EDIT_OPEN_SHOPPING_LIST](state, action) {
    return state
        .set("showShoppingList", true)
        .set("shoppingListOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_SHOPPING_LIST](state, action) {
    return state
        .set("showShoppingList", false)
        .delete("shoppingListOrder");
  },
  [UIActionTypes.ORDER_EDIT_PRINT_PREP](state, action) {
    return state
        .set("printPrep", true)
        .set("printRecipesOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_PRINT_PREP_COMPLETE](state, action) {
    return state
        .set("printPrep", false);
  },
  [UIActionTypes.ORDER_EDIT_PRINT_SUB_RECIPES](state, action) {
    return state
        .set("printSubRecipes", true)
        .set("printRecipesOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_PRINT_SUB_RECIPES_COMPLETE](state, action) {
    return state
        .set("printSubRecipes", false)
        .delete("printRecipesOrder");
  },
  [UIActionTypes.ORDER_EDIT_PRINT_RECIPES](state, action) {
    return state
        .set("printRecipes", true)
        .set("printRecipesOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_PRINT_RECIPES_COMPLETE](state, action) {
    return state
        .set("printRecipes", false)
        .delete("printRecipesOrder");
  },
  [UIActionTypes.ORDER_EDIT_PRINT_MENU](state, action) {
    return state.set("printMenu", true)
  },
  [UIActionTypes.ORDER_EDIT_PRINT_MENU_COMPLETE](state, action) {
    return state.set("printMenu", false)
  },
  [UIActionTypes.ORDER_EDIT_OPEN_PRINT_OPTION_MODAL](state, action) {
    return state
        .set('printStep', 'show_print_options')
        .set("printRecipesOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_PRINT_OPTION_MODAL](state, action) {
    return state
        .set('printStep', '')
        .set('printPrep', false)
        .set('selectedPrintOptions', Immutable.Set([]))
        .delete('printRecipesOrder');
  },
  [UIActionTypes.ORDER_EDIT_OPEN_PRINT_RECIPE_MODAL](state, action) {
    return state
        .set('printStep', 'show_print_recipe_options')
        .set('selectedPrintOptions', action.content.selectedPrintOptions);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_PRINT_RECIPE_MODAL](state, action) {
    return state
        .set('printStep', 'show_print_options')
        .set('printPrep', false)
        .set('selectedPrintOptions', Immutable.Set([]));
  },
  [UIActionTypes.ORDER_EDIT_OPEN_COST_MODAL](state, action) {
    return state
        .set("showCostModal", true)
        .set("baseOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_COST_MODAL](state, action) {
    return state
        .set("showCostModal", false)
        .delete("baseOrder");
  },
  [UIActionTypes.ORDER_EDIT_OPEN_SEND_MODAL](state, action) {
    return state
        .set("showSendModal", true)
        .set("sendSupplierId", action.content.supplierId)
        .set("userEditedOrder", action.content.userEditedOrder);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_SEND_MODAL](state, action) {
    return state
        .set("showSendModal", false)
        .delete("sendSupplierId")
        .delete("userEditedOrder");
  },
  [UIActionTypes.ORDER_EDIT_EMAIL](state, action) {
    return state
        .set("sendingEmail", true)
        .set("baseOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_EDIT_EMAIL_COMPLETE](state, action) {
    return state
        .set("sendingEmail", false)
        .delete("baseOrder");
  },
  [UIActionTypes.ORDER_EDIT_OPEN_LOAD_MENU_MODAL](state, action) {
    return state.set('showLoadMenuModal', true);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_LOAD_MENU_MODAL](state, action) {
    return state.set('showLoadMenuModal', false);
  },
  [UIActionTypes.ORDER_EDIT_OPEN_LOAD_MENU_CONFIRM](state, action) {
    return state
        .set('showLoadMenuModal', false)
        .set('showLoadMenuConfirm', true)
        .set('selectedMenuId', action.content.selectedMenuId);
  },
  [UIActionTypes.ORDER_EDIT_CLOSE_LOAD_MENU_CONFIRM](state, action) {
    return state
      .set('showLoadMenuConfirm', false)
      .delete('selectedMenuId');
  },
};

export default lookupTableReducer(initialState, actionsMap);
