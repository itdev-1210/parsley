// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
    'POS_IMPORT_OPEN_ADD_CODE_MODAL',
    'POS_IMPORT_CLOSE_ADD_CODE_MODAL',
    'POS_IMPORT_OPEN_EMPTY_CODE_MODAL',
    'POS_IMPORT_CLOSE_EMPTY_CODE_MODAL',
    'POS_IMPORT_OPEN_EMPTY_QUANTITY_MODAL',
    'POS_IMPORT_CLOSE_EMPTY_QUANTITY_MODAL',
    'POS_IMPORT_OPEN_DUPLICATE_ITEM_NUMBER_MODAL',
    'POS_IMPORT_CLOSE_DUPLICATE_ITEM_NUMBER_MODAL',
]);


export const actionCreators = {
  openAddCodeModal(recipesCode: any) {
    return {
      type: [UIActionTypes.POS_IMPORT_OPEN_ADD_CODE_MODAL],
      content: recipesCode,
    };
  },
  closeAddCodeModal() {
    return {
      type: [UIActionTypes.POS_IMPORT_CLOSE_ADD_CODE_MODAL],
    };
  },
  openEmptyCodeModal(emptyCodeRows: Array<*>) {
    return {
      type: [UIActionTypes.POS_IMPORT_OPEN_EMPTY_CODE_MODAL],
      content: emptyCodeRows,
    };
  },
  closeEmptyCodeModal() {
    return {
      type: [UIActionTypes.POS_IMPORT_CLOSE_EMPTY_CODE_MODAL],
    };
  },
  openEmptyQuantityModal(emptyCodeRows: Array<*>) {
    return {
      type: [UIActionTypes.POS_IMPORT_OPEN_EMPTY_QUANTITY_MODAL],
      content: emptyCodeRows,
    };
  },
  closeEmptyQuantityModal() {
    return {
      type: [UIActionTypes.POS_IMPORT_CLOSE_EMPTY_QUANTITY_MODAL],
    };
  },
  openDuplicateItemsNumberModal(recipesCode: any) {
    return {
      type: [UIActionTypes.POS_IMPORT_OPEN_DUPLICATE_ITEM_NUMBER_MODAL],
      content: recipesCode,
    };
  },
  closeDuplicateItemsNumberModal() {
    return {
      type: [UIActionTypes.POS_IMPORT_CLOSE_DUPLICATE_ITEM_NUMBER_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showAddCodeModal: false,
  showEmptyCodeModal: false,
  showEmptyQuantityModal: false,
  showDuplicateItemNumberModal: false,
});

const actionsMap = {
  [UIActionTypes.POS_IMPORT_OPEN_ADD_CODE_MODAL](state, action) {
    return state
        .set('showAddCodeModal', true)
        .set('recipesCode', action.content);
  },
  [UIActionTypes.POS_IMPORT_CLOSE_ADD_CODE_MODAL](state, action) {
    return state
        .set('showAddCodeModal', false)
        .delete('recipesCode');
  },
  [UIActionTypes.POS_IMPORT_OPEN_EMPTY_CODE_MODAL](state, action) {
    return state
        .set('showEmptyCodeModal', true)
        .set('emptyCodeRows', action.content);
  },
  [UIActionTypes.POS_IMPORT_CLOSE_EMPTY_CODE_MODAL](state, action) {
    return state
        .set('showEmptyCodeModal', false)
        .delete('emptyCodeRows');
  },
  [UIActionTypes.POS_IMPORT_OPEN_EMPTY_QUANTITY_MODAL](state, action) {
    return state
        .set('showEmptyQuantityModal', true)
        .set('emptyQuantityRows', action.content);
  },
  [UIActionTypes.POS_IMPORT_CLOSE_EMPTY_QUANTITY_MODAL](state, action) {
    return state
        .set('showEmptyQuantityModal', false)
        .delete('emptyQuantityRows');
  },
  [UIActionTypes.POS_IMPORT_OPEN_DUPLICATE_ITEM_NUMBER_MODAL](state, action) {
    return state
        .set('showDuplicateItemNumberModal', true)
        .set('duplicateItemNumbers', action.content);
  },
  [UIActionTypes.POS_IMPORT_CLOSE_DUPLICATE_ITEM_NUMBER_MODAL](state, action) {
    return state
        .set('showDuplicateItemNumberModal', false)
        .delete('duplicateItemNumbers');
  },
};

export default lookupTableReducer(initialState, actionsMap);
