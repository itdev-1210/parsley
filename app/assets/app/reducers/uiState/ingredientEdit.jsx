// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';
import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';

export const UIActionTypes = keyMirror([
  'INGREDIENT_EDIT_OPEN_NEW_SUPPLIER_MODAL',
  'INGREDIENT_EDIT_CLOSE_NEW_SUPPLIER_MODAL'
]);

export const actionCreators = {
  openNewSupplierModal(newSupplier: string) {
    return {
      type: [UIActionTypes.INGREDIENT_EDIT_OPEN_NEW_SUPPLIER_MODAL],
      content: {
        newSupplier
      }
    };
  },
  closeNewSupplierModal() {
    return {
      type: [UIActionTypes.INGREDIENT_EDIT_CLOSE_NEW_SUPPLIER_MODAL]
    };
  }
};

const initialState = Immutable.fromJS({
  showNewSupplierModal: false
});

const actionsMap = {
  [UIActionTypes.INGREDIENT_EDIT_OPEN_NEW_SUPPLIER_MODAL](state, action) {
    return state
        .set('showNewSupplierModal', action.content.newSupplier);
  },
  [UIActionTypes.INGREDIENT_EDIT_CLOSE_NEW_SUPPLIER_MODAL](state) {
    return state
        .set('showNewSupplierModal', false);
  }
};

export default lookupTableReducer(initialState, actionsMap);
