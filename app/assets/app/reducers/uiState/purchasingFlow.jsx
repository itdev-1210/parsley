// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
    'PURCHASING_FLOW_SELECT_SUPPLIERS',
    'PURCHASING_FLOW_SELECT_SUBTRACT_INVENTORY_OPTION',
    'PURCHASING_FLOW_REVIEW',
    'PURCHASING_FLOW_LEAVE',
    'PURCHASING_FLOW_ADD_SUPPLIER',
    'PURCHASING_FLOW_REMOVE_SUPPLIER',
]);

export const actionCreators = {
  startPickSuppliers() {
    return { type: [UIActionTypes.PURCHASING_FLOW_SELECT_SUPPLIERS] }
  },
  startPickSubtractInventoryOption() {
    return { type: [UIActionTypes.PURCHASING_FLOW_SELECT_SUBTRACT_INVENTORY_OPTION] }
  },
  startReview() {
    return {
      type: [UIActionTypes.PURCHASING_FLOW_REVIEW],
    }
  },
  leave() {
    return { type: [UIActionTypes.PURCHASING_FLOW_LEAVE] }
  },

  addSupplier(supplierId: number) {
    return {
      type: [UIActionTypes.PURCHASING_FLOW_ADD_SUPPLIER],
      content: {
        supplierId
      },
    }
  },

  removeSupplier(supplierId: number) {
    return {
      type: [UIActionTypes.PURCHASING_FLOW_REMOVE_SUPPLIER],
      content: {
        supplierId
      },
    }
  },
};

const initialState = Immutable.Map({
  phase: 'supplierSelect',
  selectedSuppliers: Immutable.Set(),
});

const actionsMap = {
  [UIActionTypes.PURCHASING_FLOW_SELECT_SUPPLIERS](state, action) {
    return state.set('phase', 'supplierSelect');
  },
  [UIActionTypes.PURCHASING_FLOW_SELECT_SUBTRACT_INVENTORY_OPTION](state, action) {
    return state.set('phase', 'subtractInventoryOptionSelect');
  },
  [UIActionTypes.PURCHASING_FLOW_REVIEW](state, action) {
    return state.set('phase', 'review');
  },
  [UIActionTypes.PURCHASING_FLOW_LEAVE](state, action) {
    return initialState;
  },

  [UIActionTypes.PURCHASING_FLOW_ADD_SUPPLIER](state, action) {
    return state.update('selectedSuppliers', s => s.add(action.content.supplierId));
  },
  [UIActionTypes.PURCHASING_FLOW_REMOVE_SUPPLIER](state, action) {
    return state.update('selectedSuppliers', s => s.delete(action.content.supplierId));
  },
};

export default lookupTableReducer(initialState, actionsMap);
