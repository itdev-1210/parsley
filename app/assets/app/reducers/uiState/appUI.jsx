/** @flow */
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

let initialState = Immutable.Map({ fileDraggingOver: false });

const actionTypes = keyMirror([
  'FILE_DRAG_ENTER',
  'FILE_DRAG_LEAVE',
]);

const actionsMap = {
  [actionTypes.FILE_DRAG_ENTER]: state => state.set('fileDraggingOver', true),
  [actionTypes.FILE_DRAG_LEAVE]: state => state.set('fileDraggingOver', false),
};

export default lookupTableReducer(initialState, actionsMap);

export const actionCreators = {
  fileDragEnter() {
    return { type: actionTypes.FILE_DRAG_ENTER };
  },

  fileDragLeave() {
    return { type: actionTypes.FILE_DRAG_LEAVE };
  },
};
