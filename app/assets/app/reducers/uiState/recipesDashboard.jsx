// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
  'RECIPE_DASHBOARD_OPEN_ADDED_CHANGED_DETAILS_MODAL',
  'RECIPE_DASHBOARD_CLOSE_ADDED_CHANGED_DETAILS_MODAL',
]);

export const actionCreators = {
  openRecipesAddedChangedDetailsModal(recipes: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.RECIPE_DASHBOARD_OPEN_ADDED_CHANGED_DETAILS_MODAL],
      content: { recipes },
    };
  },
  closeRecipesAddedChangedDetailsModal() {
    return {
      type: [UIActionTypes.RECIPE_DASHBOARD_CLOSE_ADDED_CHANGED_DETAILS_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showRecipesAddedChangedDetailsModal: false,
});

const actionsMap = {
  [UIActionTypes.RECIPE_DASHBOARD_OPEN_ADDED_CHANGED_DETAILS_MODAL](state, action) {
    return state
      .set('showRecipesAddedChangedDetailsModal', true);
  },
  [UIActionTypes.RECIPE_DASHBOARD_CLOSE_ADDED_CHANGED_DETAILS_MODAL](state) {
    return state.set('showRecipesAddedChangedDetailsModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
