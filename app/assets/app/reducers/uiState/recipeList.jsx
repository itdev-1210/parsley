// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
  'RECIPE_LIST_OPEN_ADD_TO_CATEGORY_MODAL',
  'RECIPE_LIST_CLOSE_ADD_TO_CATEGORY_MODAL',
  'RECIPE_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL',
  'RECIPE_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL',
  'RECIPE_LIST_OPEN_PRINT_MULTIPLE_RECIPE_INFO_MODAL',
  'RECIPE_LIST_CLOSE_PRINT_MULTIPLE_RECIPE_INFO_MODAL',
]);

export const actionCreators = {
  openAddToCategoryModal(recipes: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.RECIPE_LIST_OPEN_ADD_TO_CATEGORY_MODAL],
      content: { recipes },
    };
  },
  closeAddToCategoryModal() {
    return {
      type: [UIActionTypes.RECIPE_LIST_CLOSE_ADD_TO_CATEGORY_MODAL],
    };
  },

  openRemoveFromCategoryModal(recipes: Immutable.Set<number>) {
    return {
      type: [UIActionTypes.RECIPE_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL],
      content: { recipes },
    };
  },
  closeRemoveFromCategoryModal() {
    return {
      type: [UIActionTypes.RECIPE_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL],
    };
  },
  openPrintMultipleRecipeInfoModal(recipes: Array<number>) {
    return {
      type: [UIActionTypes.RECIPE_LIST_OPEN_PRINT_MULTIPLE_RECIPE_INFO_MODAL],
      content: { recipes },
    };
  },
  closePrintMultipleRecipeInfoModal() {
    return {
      type: [UIActionTypes.RECIPE_LIST_CLOSE_PRINT_MULTIPLE_RECIPE_INFO_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showAddToCategoryModal: false,
  showRemoveFromCategoryModal: false,
  showPrintMultipleRecipeInfoModal: false,
});

const actionsMap = {
  [UIActionTypes.RECIPE_LIST_OPEN_ADD_TO_CATEGORY_MODAL](state, action) {
    return state
      .set('showAddToCategoryModal', true)
      .set('addingToCategoryRecipes', action.content.recipes);
  },
  [UIActionTypes.RECIPE_LIST_CLOSE_ADD_TO_CATEGORY_MODAL](state) {
    return state.set('showAddToCategoryModal', false);
  },

  [UIActionTypes.RECIPE_LIST_OPEN_REMOVE_FROM_CATEGORY_MODAL](state, action) {
    return state
      .set('showRemoveFromCategoryModal', true)
      .set('removingFromCategoryRecipes', action.content.recipes);
  },
  [UIActionTypes.RECIPE_LIST_CLOSE_REMOVE_FROM_CATEGORY_MODAL](state) {
    return state.set('showRemoveFromCategoryModal', false);
  },
  [UIActionTypes.RECIPE_LIST_OPEN_PRINT_MULTIPLE_RECIPE_INFO_MODAL](state, action) {
    return state
      .set('showPrintMultipleRecipeInfoModal', true)
      .set('printingRecipes', Immutable.fromJS(action.content.recipes));
  },
  [UIActionTypes.RECIPE_LIST_CLOSE_PRINT_MULTIPLE_RECIPE_INFO_MODAL](state) {
    return state.set('showPrintMultipleRecipeInfoModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
