// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
    'IMPORT_RECEIVABLES_PRICING_OPEN_ADD_SUPPLIER_MODAL',
    'IMPORT_RECEIVABLES_PRICING_CLOSE_ADD_SUPPLIER_MODAL',
    'IMPORT_RECEIVABLES_PRICING_OPEN_NEW_SUPPLIER_MODAL',
    'IMPORT_RECEIVABLES_PRICING_CLOSE_NEW_SUPPLIER_MODAL',
]);

export const actionCreators = {
  openAddSupplierModal() {
    return {
      type: [UIActionTypes.IMPORT_RECEIVABLES_PRICING_OPEN_ADD_SUPPLIER_MODAL],
    };
  },
  closeAddSupplierModal() {
    return {
      type: [UIActionTypes.IMPORT_RECEIVABLES_PRICING_CLOSE_ADD_SUPPLIER_MODAL],
    };
  },
  openNewSupplierModal(newSupplier: string) {
    return {
      type: [UIActionTypes.IMPORT_RECEIVABLES_PRICING_OPEN_NEW_SUPPLIER_MODAL],
      content: {
        newSupplier,
      },
    };
  },
  closeNewSupplierModal() {
    return {
      type: [UIActionTypes.IMPORT_RECEIVABLES_PRICING_CLOSE_NEW_SUPPLIER_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showAddSupplier: false,
  showNewSupplierModal: false,
});

const actionsMap = {
  [UIActionTypes.IMPORT_RECEIVABLES_PRICING_OPEN_ADD_SUPPLIER_MODAL](state, action) {
    return state
        .set('showAddSupplier', true)
        .set('showNewSupplierModal', false);
  },
  [UIActionTypes.IMPORT_RECEIVABLES_PRICING_CLOSE_ADD_SUPPLIER_MODAL](state, action) {
    return state
        .set('showAddSupplier', false)
        .set('showNewSupplierModal', false);
  },
  [UIActionTypes.IMPORT_RECEIVABLES_PRICING_OPEN_NEW_SUPPLIER_MODAL](state, action) {
    return state
        .set('showNewSupplierModal', action.content.newSupplier);
  },
  [UIActionTypes.IMPORT_RECEIVABLES_PRICING_CLOSE_NEW_SUPPLIER_MODAL](state) {
    return state
        .set('showNewSupplierModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
