// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';

import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';

var UIActionTypes = keyMirror([
  "MENU_EDIT_PRINT_MENU",
  "MENU_EDIT_PRINT_MENU_COMPLETE",

]);

export const actionCreators = {
  printMenu() {
    return { type: [UIActionTypes.MENU_EDIT_PRINT_MENU] }
  },
  completePrintMenu() {
    return { type: [UIActionTypes.MENU_EDIT_PRINT_MENU_COMPLETE] }
  }
};

const initialState = Immutable.fromJS({
  printMenu: false
});

const actionsMap = {
  [UIActionTypes.MENU_EDIT_PRINT_MENU](state, action) {
    return state.set('printMenu', true)
  },
  [UIActionTypes.MENU_EDIT_PRINT_MENU_COMPLETE](state, action) {
    return state.set('printMenu', false)
  }
};

export default lookupTableReducer(initialState, actionsMap);
