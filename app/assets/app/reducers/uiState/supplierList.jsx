// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
    'SUPPLIER_LIST_OPEN_IMPORT_RECEIVABLES_PRICING_MODAL',
    'SUPPLIER_LIST_CLOSE_IMPORT_RECEIVABLES_PRICING_MODAL',
]);

export const actionCreators = {
  openImportReceivablesPricingModal() {
    return {
      type: [UIActionTypes.SUPPLIER_LIST_OPEN_IMPORT_RECEIVABLES_PRICING_MODAL],
    };
  },
  closeImportReceivablesPricingModal() {
    return {
      type: [UIActionTypes.SUPPLIER_LIST_CLOSE_IMPORT_RECEIVABLES_PRICING_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showImportReceivablesPricingModal: false,
});

const actionsMap = {
  [UIActionTypes.SUPPLIER_LIST_OPEN_IMPORT_RECEIVABLES_PRICING_MODAL](state, action) {
    return state
        .set('showImportReceivablesPricingModal', true);
  },
  [UIActionTypes.SUPPLIER_LIST_CLOSE_IMPORT_RECEIVABLES_PRICING_MODAL](state, action) {
    return state
        .set('showImportReceivablesPricingModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
