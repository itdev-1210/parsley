// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const UIActionTypes = keyMirror([
    'ORDER_LIST_BEGIN_PURCHASING',
    'ORDER_LIST_END_PURCHASING',
    'ORDER_LIST_SHOW_PURCHASING',
    'ORDER_LIST_HIDE_PURCHASING',
    'ORDER_LIST_OPEN_SEND_MODAL',
    'ORDER_LIST_CLOSE_SEND_MODAL',
    'ORDER_LIST_EMAIL',
    'ORDER_LIST_EMAIL_COMPLETE',
    'ORDER_LIST_OPEN_POS_IMPORT_MODAL',
    'ORDER_LIST_CLOSE_POS_IMPORT_MODAL',
]);

import type { BaseOrder } from '../productionOrder';

export const actionCreators = {
  beginPurchasing(orders: Immutable.Set<number>, includePar: boolean, fromPurchaseOrder: boolean) {
    return {
      type: UIActionTypes.ORDER_LIST_BEGIN_PURCHASING,
      content: {
        orders,
        includePar,
        fromPurchaseOrder,
      },
    }
  },
  endPurchasing() {
    return { type: UIActionTypes.ORDER_LIST_END_PURCHASING }
  },
  showPurchasing() {
    return { type: UIActionTypes.ORDER_LIST_SHOW_PURCHASING}
  },
  hidePurchasing() {
    return { type: UIActionTypes.ORDER_LIST_HIDE_PURCHASING }
  },
  // $FlowFixMe - see ShoppingList.getUserEditedOrder for structure
  openSendModal(supplierId: number, userEditedOrder) {
    return {
      type: [UIActionTypes.ORDER_LIST_OPEN_SEND_MODAL],
      content: { supplierId, userEditedOrder },
    }
  },
  closeSendModal() {
    return {
      type: [UIActionTypes.ORDER_LIST_CLOSE_SEND_MODAL],
    }
  },
  email(baseOrder: BaseOrder) {
    return {
      type: [UIActionTypes.ORDER_LIST_EMAIL],
      content: {
        baseOrder
      },
    }
  },
  emailComplete() {
    return {
      type: [UIActionTypes.ORDER_LIST_EMAIL_COMPLETE],
    }
  },
  openPosImportModal() {
    return {
      type: [UIActionTypes.ORDER_LIST_OPEN_POS_IMPORT_MODAL],
    };
  },
  closePosImportModal() {
    return {
      type: [UIActionTypes.ORDER_LIST_CLOSE_POS_IMPORT_MODAL],
    };
  },
};

const initialState = Immutable.fromJS({
  showPurchasing: false,
  showSendModal: false,
  sendingEmail: false,
  showPosImportModal: false,
});

const actionsMap = {
  [UIActionTypes.ORDER_LIST_BEGIN_PURCHASING](state, action) {
    return state
        .set('purchasingForOrders', action.content.orders)
        .set('includePar', action.content.includePar)
        .set('fromPurchaseOrder', action.content.fromPurchaseOrder)
        .set('showPurchasing', true);
  },
  [UIActionTypes.ORDER_LIST_END_PURCHASING](state, action) {
    return state
        .set('showPurchasing', false)
        .delete('purchasingForOrders');
  },
  [UIActionTypes.ORDER_LIST_SHOW_PURCHASING](state, action) {
    return state.set('showPurchasing', true)
  },
  [UIActionTypes.ORDER_LIST_HIDE_PURCHASING](state, action) {
    return state.set('showPurchasing', false)
  },
  [UIActionTypes.ORDER_LIST_OPEN_SEND_MODAL](state, action) {
    return state
        .set("showSendModal", true)
        .set("sendSupplierId", action.content.supplierId)
        .set("userEditedOrder", action.content.userEditedOrder);
  },
  [UIActionTypes.ORDER_LIST_CLOSE_SEND_MODAL](state, action) {
    return state
        .set("showSendModal", false)
        .delete("sendSupplierId")
        .delete("userEditedOrder");
  },
  [UIActionTypes.ORDER_LIST_EMAIL](state, action) {
    return state
        .set("sendingEmail", true)
        .set("baseOrder", action.content.baseOrder);
  },
  [UIActionTypes.ORDER_LIST_EMAIL_COMPLETE](state, action) {
    return state
        .set("sendingEmail", false)
        .delete("baseOrder");
  },
  [UIActionTypes.ORDER_LIST_OPEN_POS_IMPORT_MODAL](state, action) {
    return state
        .set('showPosImportModal', true);
  },
  [UIActionTypes.ORDER_LIST_CLOSE_POS_IMPORT_MODAL](state, action) {
    return state
        .set('showPosImportModal', false);
  },
};

export default lookupTableReducer(initialState, actionsMap);
