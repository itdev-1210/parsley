// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

type CurrentInventoryItem = $FlowTODO;

const actionTypes = keyMirror([
  'INVENTORY_CURRENT',
  'INVENTORY_CURRENT_ERROR',
]);

ERROR_TYPES.push(actionTypes.INVENTORY_CURRENT_ERROR);

let initialState = null;

const actionsMap = {
  [actionTypes.INVENTORY_CURRENT]: (state, action) =>
    // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.currentInventory).sortBy(i => {
      if (i.get('originalIngredientName')) {
        return i.get('originalIngredientName') + ' - ' + i.get('name');
      }
      return i.get('name');
    }),
};

export const actionCreators = {
  list(currentInventory: Array<CurrentInventoryItem>) {
    return {
      type: actionTypes.INVENTORY_CURRENT,
      content: { currentInventory },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.INVENTORY_CURRENT_ERROR,
      content: { error },
    };
  },
};

export default lookupTableReducer(initialState, actionsMap);
