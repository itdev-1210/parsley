// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'ORDER_DETAILS',
  'ORDER_DETAILS_ERROR',
  'ORDER_DROP_DETAILS',
  'ORDER_SAVED',
  'ORDER_SAVE_ERROR',
  'ORDER_DELETED',
  'ORDER_DELETE_ERROR',
]);
ERROR_TYPES.push(
    actionTypes.ORDER_DETAILS_ERROR,
    actionTypes.ORDER_SAVE_ERROR,
    actionTypes.ORDER_DELETE_ERROR,
);

type Order = $FlowTODO

export const actionCreators = {
  details(id: number, deets: Order) {
    return {
      type: actionTypes.ORDER_DETAILS,
      content: { id, deets },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.ORDER_DETAILS_ERROR,
      content: { error },
    };
  },

  dropDetails() {
    return {
      type: actionTypes.ORDER_DROP_DETAILS,
    };
  },

  saved(id: number) {
    return {
      type: actionTypes.ORDER_SAVED,
      content: { id },
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.ORDER_SAVE_ERROR,
      content: { error },
    };
  },

  deleted(id: number) {
    return {
      type: actionTypes.ORDER_DELETED,
      content: { id },
    };
  },

  deleteError(error: Error) {
    return {
      type: actionTypes.ORDER_DELETE_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.ORDER_DETAILS]: (state, action) => action.content.deets,
  [actionTypes.ORDER_DROP_DETAILS]: (state, action) => initialState,
  [actionTypes.ORDER_DELETED]: (state, action) => initialState,
};

export default lookupTableReducer(initialState, actionsMap);
