import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_ONBOARDING_SUCCESS',
  'FETCH_ONBOARDING_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_ONBOARDING_ERROR,
);

const initialState = null;

export const actionCreators = {
  info(onboardingInfo) {
    return {
      type: actionTypes.FETCH_ONBOARDING_SUCCESS,
      content: onboardingInfo
    }
  },
  inforError(error: Error) {
    return {
      type: actionTypes.FETCH_ONBOARDING_ERROR,
      content: error,
    }
  },
};

const actionsMap = {
  [actionTypes.FETCH_ONBOARDING_SUCCESS]: (state, action) => {
    return Immutable.fromJS(action.content);
  },
  [actionTypes.FETCH_ONBOARDING_ERROR]: (state, action) => null,
}

export default lookupTableReducer(initialState, actionsMap);
