import { combineReducers } from 'redux-immutablejs';
import { reducer as formReducer } from 'redux-form';

import userInfo from 'common/redux/reducers/userInfo';
import onboardingInfo from './onboardingInfo';
import onboardingWidgetsInfo from './onboardingWidgetsInfo';
import stripeInfo from 'common/redux/reducers/stripeInfo';
import mainInfoLoaded from './mainInfoLoaded';

import menus from './menus';
import orders from './orders';
import inventories from './inventories';
import inventoryCurrent from './inventoryCurrent';
import inventoryDetails from './inventoryDetails';
import inventoryItems from './inventoryItems';
import ingredientPars from './ingredientPars';
import tags from './tags';
import measures from './measures';
import nutrients from './nutrients';
import recipes from './recipes';
import totalRecipes from './totalRecipes';
import totalRecipesAddedChanged from './totalRecipesAddedChanged';
import recipesAddedChanged from './recipesAddedChanged';
import recentPriceChanges from './recentPriceChanges';
import menuItems from './menuItems';
import suppliers from './suppliers';
import ingredients from './ingredients';
import advancedIngredientPreps from './advancedIngredientPreps';
import purchaseOrders from './purchaseOrders';
import locations from './locations';
import detailedIngredients from './detailedIngredients';
import detailedSuppliers from '../reducers/detailedSuppliers';
import supplierDetails from './supplierDetails';
import supplierSourcedIngredients from './supplierSourcedIngredients';
import menuDetails from './menuDetails';
import orderDetails from './orderDetails';
import productDetails from './productDetails';
import purchaseOrderDetails from './purchaseOrderDetails';
import productionOrder from './productionOrder';
import indexesCache from './indexesCache';
import posSetup from './posSetup';
import posCsvDetails from './posCsvDetails';
import posImportPreferences from './posImportPreferences';
import importReceivablesPricingDetails from './importReceivablesPricingDetails';
import importReceiptsDetails from './importReceiptsDetails';

import appUI from './uiState/appUI';
import searchableList from './uiState/searchableList';
import orderList from './uiState/orderList';
import purchaseOrderList from './uiState/purchaseOrderList';
import recipeList from './uiState/recipeList';
import recipesDashboard from './uiState/recipesDashboard';
import ingredientList from './uiState/ingredientList';
import menuEdit from './uiState/menuEdit';
import orderEdit from './uiState/orderEdit';
import recipeEdit from './uiState/recipeEdit';
import supplierEdit from './uiState/supplierEdit';
import ingredientEdit from './uiState/ingredientEdit';
import reverseDependencies from './reverseDependencies';
import purchasingFlow from './uiState/purchasingFlow';
import posImport from './uiState/posImport';
import supplierList from './uiState/supplierList';
import importReceivablesPricing from './uiState/importReceivablesPricing';

import notification from './notification';

import userInvitation from './userInvitation';
import pendingShareInvites from './pendingShareInvites';
import upstreamInfo from './upstreamInfo';
import locationsSyncState from './locationsSyncState';

export default combineReducers({
  form: formReducer,

  // data reducers
  userInfo,
  onboardingInfo,
  onboardingWidgetsInfo,
  stripeInfo,
  mainInfoLoaded,

  ingredients,
  advancedIngredientPreps,
  menus,
  orders,
  inventories,
  inventoryCurrent,
  inventoryDetails,
  inventoryItems,
  ingredientPars,
  tags,
  measures,
  nutrients,
  recipes,
  totalRecipes,
  totalRecipesAddedChanged,
  recipesAddedChanged,
  recentPriceChanges,
  menuItems,
  suppliers,
  supplierDetails,
  menuDetails,
  orderDetails,
  productDetails,
  detailedIngredients,
  detailedSuppliers,
  productionOrder,
  purchaseOrders,
  locations,
  purchaseOrderDetails,
  reverseDependencies,

  supplierSourcedIngredients,
  indexesCache,
  posSetup,
  posCsvDetails,
  posImportPreferences,
  importReceivablesPricingDetails,
  importReceiptsDetails,


  // UI state reducers
  appUI,
  menuEdit,
  orderEdit,
  recipeEdit,
  supplierEdit,
  ingredientEdit,

  searchableList,
  orderList,
  purchaseOrderList,
  recipeList,
  recipesDashboard,
  ingredientList,

  purchasingFlow,
  posImport,

  notification,
  userInvitation,
  pendingShareInvites,
  upstreamInfo,
  locationsSyncState,
  supplierList,
  importReceivablesPricing,
});
