import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_POSSETUP_SUCCESS',
  'FETCH_POSSETUP_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_POSSETUP_ERROR,
);

const initialState = null;

export const actionCreators = {
  info(posInfo) {
    return {
      type: actionTypes.FETCH_POSSETUP_SUCCESS,
      content: posInfo,
    };
  },
  inforError(error: Error) {
    return {
      type: actionTypes.FETCH_POSSETUP_ERROR,
      content: error,
    };
  },
};

const actionsMap = {
  [actionTypes.FETCH_POSSETUP_SUCCESS]: (state, action) => {
    return Immutable.fromJS(action.content);
  },
  [actionTypes.FETCH_POSSETUP_ERROR]: (state, action) => null,
};

export default lookupTableReducer(initialState, actionsMap);
