// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'MENU_DETAILS',
  'MENU_DETAILS_ERROR',
  'MENU_DROP_DETAILS',
  'MENU_SAVED',
  'MENU_SAVE_ERROR',
  'MENU_DELETED',
  'MENU_DELETE_ERROR',

]);
ERROR_TYPES.push(actionTypes.MENU_DETAILS_ERROR, actionTypes.MENU_DELETE_ERROR);

type Menu = $FlowTODO;

export const actionCreators = {
  details(id: number, deets: Menu) {
    return {
      type: actionTypes.MENU_DETAILS,
      content: { id, deets },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.MENU_DETAILS_ERROR,
      content: { error },
    };
  },

  dropDetails() {
    return {
      type: actionTypes.MENU_DROP_DETAILS,
    };
  },

  saved(id: number) {
    return {
      type: actionTypes.MENU_SAVED,
      content: { id },
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.MENU_SAVE_ERROR,
      content: { error },
    };
  },

  deleted(id: number) {
    return {
      type: actionTypes.MENU_DELETED,
      content: { id },
    };
  },

  deleteError(error: Error) {
    return {
      type: actionTypes.MENU_DELETE_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.MENU_DETAILS]: (state, action) => ({
    id: action.content.id,
    ...action.content.deets,
  }),
  [actionTypes.MENU_DROP_DETAILS]: (state, action) => initialState,
  [actionTypes.MENU_DELETED]: (state, action) => initialState,
};

export default lookupTableReducer(initialState, actionsMap);
