// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'RECIPE_LIST',
  'RECIPE_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.RECIPE_LIST_ERROR);

export const actionCreators = {
  recipeList(recipes: SummaryList) {
    return {
      type: actionTypes.RECIPE_LIST,
      content: { recipes },
    };
  },

  recipeListError(error: Error) {
    return {
      type: actionTypes.RECIPE_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.RECIPE_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.recipes).sortBy(i => i.get("name").toLowerCase())
};

export default lookupTableReducer(initialState, actionsMap);
