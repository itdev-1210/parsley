/** @flow */
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const initialState = null;

const actionTypes = keyMirror([
  'NOTIFY', 'CLEAR_NOTIFICATION'
]);

const actionsMap = {
  [actionTypes.NOTIFY]: (state, action) => action.content,
  [actionTypes.CLEAR_NOTIFICATION]: () => initialState,
};

export default lookupTableReducer(initialState, actionsMap);

export const actionCreators = {
  notify(title: string, level: string, message: string, position: string = 'tr', autoDismiss: number = 6) {
    return {
      type: actionTypes.NOTIFY,
      content: {
        title,
        level,
        message,
        position,
        autoDismiss
      },
    };
  },

  clearNotification() {
    return {
      type: actionTypes.CLEAR_NOTIFICATION
    };
  }
};
