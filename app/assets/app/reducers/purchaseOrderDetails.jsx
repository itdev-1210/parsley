// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_PURCHASE_SUCCESS',
  'FETCH_PURCHASE_ERROR',
  'DROP_PURCHASE_DETAILS',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_PURCHASE_ERROR,
);

let initialState = null;

type PurchaseOrder = $FlowTODO

export const actionCreators = {
  details(purchaseDetails: PurchaseOrder) {
    return {
      type: actionTypes.FETCH_PURCHASE_SUCCESS,
      content: purchaseDetails
    }
  },
  detailsError(error: Error) {
    return {
      type: actionTypes.FETCH_PURCHASE_ERROR,
      content: error
    }
  },
  dropDetails() {
    return { type: actionTypes.DROP_PURCHASE_DETAILS }
  },
};

const actionsMap = {
  [actionTypes.FETCH_PURCHASE_SUCCESS]: (state, action) => Immutable.fromJS(action.content),
  [actionTypes.FETCH_PURCHASE_ERROR]: (state, action) => null,
  [actionTypes.DROP_PURCHASE_DETAILS]: (state, action) => null,
};

export default lookupTableReducer(initialState, actionsMap);
