// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { LocationList } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'LOCATION_LIST_FETCH_SUCCESS',
  'LOCATION_LIST_FETCH_ERROR',
]);
ERROR_TYPES.push(actionTypes.LOCATION_LIST_FETCH_ERROR);

export const actionCreators = {
  list(locations: LocationList) {
    return {
      type: actionTypes.LOCATION_LIST_FETCH_SUCCESS,
      content: { locations },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.LOCATION_LIST_FETCH_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.LOCATION_LIST_FETCH_SUCCESS]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.locations).sortBy(item => -(item.get('id') || 0))
};

export default lookupTableReducer(initialState, actionsMap);
