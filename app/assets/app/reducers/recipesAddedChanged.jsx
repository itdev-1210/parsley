// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';


type RecipesAddedChanged = {
  id: number,
  name: string,
  tombstone: boolean,
  createdAt?: string, // ISO-8601 date
  lastModified?: string, // ISO-8601 date
}

type RecipesAddedChangedList = {
  added: Array<RecipesAddedChanged>,
  changed: Array<RecipesAddedChanged>,
}

const actionTypes = keyMirror([
  'RECIPES_ADDED_CHANGED',
  'RECIPES_ADDED_CHANGED_ERROR',
]);
ERROR_TYPES.push(actionTypes._RECIPES_ADDED_CHANGED_ERROR);

export const actionCreators = {
  recipesAddedChanged(recipes: RecipesAddedChangedList) {
    return {
      type: actionTypes.RECIPES_ADDED_CHANGED,
      content: { recipes },
    };
  },

  recipesAddedChangedError(error: Error) {
    return {
      type: actionTypes.RECIPES_ADDED_CHANGED_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.RECIPES_ADDED_CHANGED]: (state, action) =>
      Immutable.fromJS(action.content.recipes),
};

export default lookupTableReducer(initialState, actionsMap);
