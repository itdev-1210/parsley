// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import moment from 'moment';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_PURCHASE_LIST_SUCCESS',
  'FETCH_PURCHASE_LIST_ERROR',
  'PURCHASE_DELETED',
  'PURCHASE_DELETE_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_PURCHASE_LIST_ERROR,
  actionTypes.PURCHASE_DELETE_ERROR,
);

type PurchaseOrder = $FlowTODO; // see JsonConversions.purchaseOrdersAPIFormats

const initialState = null;

export const actionCreators = {
  list(purchaseList: Array<PurchaseOrder>) {
    return {
      type: actionTypes.FETCH_PURCHASE_LIST_SUCCESS,
      content: purchaseList
    }
  },
  listError(error: Error) {
    return {
      type: actionTypes.FETCH_PURCHASE_LIST_ERROR,
      content: error
    }
  },
  purchaseDeleted(id: number) {
    return {
      type: actionTypes.PURCHASE_DELETED,
      content: { id }
    }
  },
};

const actionsMap = {
  [actionTypes.FETCH_PURCHASE_LIST_SUCCESS]: (state, action) => {
    return Immutable.fromJS(action.content)
    // $FlowFixMe: no fromJS() typing
    .sortBy(
      i => moment(i.get('emailedAt') ? i.get('emailedAt') : i.get('printedAt')).valueOf(),
      (a, b) => a < b  // latest purchase order first
    );
  },
  [actionTypes.FETCH_PURCHASE_LIST_ERROR]: (state, action) => null,
  [actionTypes.PURCHASE_DELETED]: (state, action) =>
    state ? state.filter(i => i.get('id') !== action.content.id): state,
}

export default lookupTableReducer(initialState, actionsMap);
