// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

export const actionTypes = keyMirror([
  'INGREDIENT_LIST',
  'INGREDIENT_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.INGREDIENT_LIST_ERROR);

export const actionCreators = {
  ingredientList(ingredients: SummaryList) {
    return {
      type: actionTypes.INGREDIENT_LIST,
      content: { ingredients },
    };
  },

  ingredientListError(error: Error) {
    return {
      type: actionTypes.INGREDIENT_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.INGREDIENT_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.ingredients).sortBy(i => i.get("name").toLowerCase())
};

export default lookupTableReducer(initialState, actionsMap);
