/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

export type Nutrients = Immutable.List<Immutable.Map<string, any>>
type JsNutrient = {
  id: number,
  nndbsrId: number,
  nndbsrName: string,
  parsleyName: ?string,
  unit: string,
  active: boolean,
};

let initialState = null;

const actionTypes = keyMirror([
  'NUTRIENTS', 'NUTRIENTS_ERROR',
]);
ERROR_TYPES.push(actionTypes.NUTRIENTS_ERROR);

const actionsMap = {
  [actionTypes.NUTRIENTS]: (state, action) => Immutable.fromJS(action.content),
  [actionTypes.NUTRIENTS_ERROR]: (state, action) => initialState,
};

export default lookupTableReducer(initialState, actionsMap);

export const actionCreators = {
  received(nutrients: Array<JsNutrient>) {
    return {
      type: actionTypes.NUTRIENTS,
      content: nutrients,
    };
  },

  error(error: Error) {
    return {
      type: actionTypes.NUTRIENTS_ERROR,
      content: {
        error,
      },
    };
  },
};
