// @flow
import Immutable from 'immutable';

import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

type IngredientDetails = $FlowTODO;

/*
 * Ingredient info for usage in recipes
 */
const actionTypes = keyMirror([
 'INGREDIENT_INFO',
 'INGREDIENT_INFO_ERROR',
 'INGREDIENT_MULTIPLE_INFO',
 'DROP_INGREDIENT_INFO',
]);
ERROR_TYPES.push(actionTypes.INGREDIENT_INFO_ERROR);

export const actionCreators = {
  info(id: number, deets: IngredientDetails) {
    return {
      type: actionTypes.INGREDIENT_INFO,
      content: { id, deets },
    };
  },
  multipleInfo(deets: Array<IngredientDetails>) {
    return {
      type: actionTypes.INGREDIENT_MULTIPLE_INFO,
      content: { deets },
    };
  },

  infoError(error: Error) {
    return {
      type: actionTypes.INGREDIENT_INFO_ERROR, error,
    };
  },

  dropInfo(id: number) {
    return {
      type: actionTypes.DROP_INGREDIENT_INFO,
      content: {
        id: parseInt(id),
      },
    };
  },
};

let initialState = Immutable.Map();

const actionsMap = {
  [actionTypes.INGREDIENT_INFO]: (state, action) => state.set(action.content.id, Immutable.fromJS(action.content.deets)),
  [actionTypes.INGREDIENT_MULTIPLE_INFO]: (state, action) => state.merge(
      // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.deets).mapKeys(k => parseInt(k)),
  ),
  [actionTypes.DROP_INGREDIENT_INFO]: (state, action) => state.delete(action.content.id),
};

export default lookupTableReducer(initialState, actionsMap);
