// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'ORDER_LIST',
  'ORDER_LIST_ERROR',
]);
ERROR_TYPES.push();

export const actionCreators = {
  list(orders: SummaryList) {
    return {
      type: actionTypes.ORDER_LIST,
      content: { orders },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.ORDER_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.ORDER_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.orders).sortBy(i => i.get("name").toLowerCase())
};

export default lookupTableReducer(initialState, actionsMap);
