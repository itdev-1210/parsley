// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

import type { Measure } from '../utils/unit-conversions';

const actionTypes = keyMirror([
  'MEASURE_LIST',
  'MEASURE_LIST_ERROR',

  // below are unused, but here for API consistency
  'MEASURE_SAVED',
  'MEASURE_SAVE_ERROR',
  'MEASURE_DELETED',
  'MEASURE_DELETE_ERROR',
]);
ERROR_TYPES.push(
    actionTypes.MEASURE_LIST_ERROR,
    actionTypes.MEASURE_SAVE_ERROR,
    actionTypes.MEASURE_DELETE_ERROR,
);

export const actionCreators = {
  list(measures: Array<Measure>) {
    return {
      type: actionTypes.MEASURE_LIST,
      content: { measures },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.MEASURE_LIST_ERROR,
      content: { error },
    };
  },

  // below are ignored by reducer, but here for API consistency
  saved(id: number) {
    return {
      type: actionTypes.MEASURE_SAVED,
      content: { id },
    };
  },

  saveError(error: Error) {
    return {
      type: actionTypes.MEASURE_SAVE_ERROR,
      content: { error },
    };
  },

  deleted(id: number) {
    return {
      type: actionTypes.MEASURE_DELETED,
      content: { id },
    };
  },

  deleteError(error: Error) {
    return {
      type: actionTypes.MEASURE_DELETE_ERROR,
      content: { error },
    };
  },
};

import lookupTableReducer from 'common/redux/lookupTableReducer';
import { byId } from '../utils/immutable-utils';

let initialState = null;

const actionsMap = {
  [actionTypes.MEASURE_LIST]: (state, action) => {
    // $FlowFixMe: no fromJS() typing
    const rawMeasures: Immutable.List<Measure> = Immutable.fromJS(action.content.measures);
    return Immutable.Map({
      rawMeasures: rawMeasures,
      measures: byId(rawMeasures).map(m => m.update("units",
          // this is the data source for UI controls listing all units, so sort them nicely
          // false (metric) sorts earlier
          units => byId(units).sort((u1, u2) => {
            const metricTest = u2.get("metric") - u1.get("metric"); // positive if u2 not metric and u1 is
            if (metricTest) { // 0 (falsy) means the two are equal
              return metricTest;
            } else {
              return u1.get("size") - u2.get("size"); // sort by size *ascending*
            }
          })
      )),
      units: byId(rawMeasures.flatMap(m =>
              m.get("units").map(
                  u => u.set("measure", m.delete("units"))
              )))
    });
  }
};

export default lookupTableReducer(initialState, actionsMap);
