// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

export const actionTypes = keyMirror([
  'MENU_ITEM_LIST',
  'MENU_ITEM_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.MENU_ITEM_LIST_ERROR);


export const actionCreators = {
  menuItemList(menuItems: SummaryList) {
    return {
      type: actionTypes.MENU_ITEM_LIST,
      content: { menuItems },
    };
  },

  menuItemListError(error: Error) {
    return {
      type: actionTypes.MENU_ITEM_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.MENU_ITEM_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.menuItems).sortBy(i => i.get("name").toLowerCase())
};

export default lookupTableReducer(initialState, actionsMap);
