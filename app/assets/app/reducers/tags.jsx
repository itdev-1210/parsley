/* @flow */
import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

let initialState = Immutable.Map();

const actionTypes = keyMirror([
  'TAG_LIST_INGREDIENTS', 'TAG_LIST_RECIPES', 'TAG_LIST_INVENTORIES', 'TAG_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.TAG_LIST_ERROR);

const actionsMap = {
  [actionTypes.TAG_LIST_INGREDIENTS]: (state, action) => state.set('ingredients', Immutable.fromJS(action.content.tags)),
  [actionTypes.TAG_LIST_RECIPES]: (state, action) => state.set('recipes', Immutable.fromJS(action.content.tags)),
  [actionTypes.TAG_LIST_INVENTORIES]: (state, action) => state.set('inventories', Immutable.fromJS(action.content.tags)),
};

export default lookupTableReducer(initialState, actionsMap);

export const actionCreators = {
  list(target: string, tags: Array<string>) {
    let type = '';
    if (target === 'ingredients') {
      type = actionTypes.TAG_LIST_INGREDIENTS;
    } else if (target === 'recipes') {
      type = actionTypes.TAG_LIST_RECIPES;
    } else if (target === 'inventories') {
      type = actionTypes.TAG_LIST_INVENTORIES;
    }
    return {
      type,
      content: { tags }
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.TAG_LIST_ERROR,
      content: { error }
    };
  },
};
