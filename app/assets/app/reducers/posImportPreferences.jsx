import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_POS_IMPORT_PREFERENCES_SUCCESS',
  'FETCH_POS_IMPORT_PREFERENCES_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_POS_IMPORT_PREFERENCES_ERROR,
);

const initialState = null;

export const actionCreators = {
  info(posImportInfo) {
    return {
      type: actionTypes.FETCH_POS_IMPORT_PREFERENCES_SUCCESS,
      content: posImportInfo,
    };
  },
  infoError(error: Error) {
    return {
      type: actionTypes.FETCH_POS_IMPORT_PREFERENCES_ERROR,
      content: error,
    };
  },
};

const actionsMap = {
  [actionTypes.FETCH_POS_IMPORT_PREFERENCES_SUCCESS]: (state, action) => {
    return Immutable.fromJS(action.content);
  },
  [actionTypes.FETCH_POS_IMPORT_PREFERENCES_ERROR]: (state, action) => null,
};

export default lookupTableReducer(initialState, actionsMap);
