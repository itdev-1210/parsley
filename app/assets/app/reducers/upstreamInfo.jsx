/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

export type OwnerOrUpstreamuser = {
  id: number,
  name: string,
  email: ?string
};

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_INFO_SUCCESS', 'FETCH_INFO_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_INFO_ERROR,
);

const initialState = Immutable.Map();

export const actionCreators = {
  infoReceived(info: OwnerOrUpstreamuser) {
    return {
      type: actionTypes.FETCH_INFO_SUCCESS,
      content: info
    }
  },

  infoError(error: Error) {
    return {
      type: actionTypes.FETCH_INFO_ERROR,
      content: error
    }
  },
};

const actionsMap = {
  [actionTypes.FETCH_INFO_SUCCESS]: (state, action) =>
    state.set('info', Immutable.fromJS(action.content)),
  [actionTypes.FETCH_INFO_ERROR]: (state, action) =>
    state.set('info', null),
};

export default lookupTableReducer(initialState, actionsMap);
