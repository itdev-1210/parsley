// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  'INGREDIENT_PARS_LIST',
  'INGREDIENT_PARS_LIST_ERROR',
  'INGREDIENT_PARS_ADDITION',
  'INGREDIENT_PARS_REMOVE',
  'INGREDIENT_PARS_ADD_TO_CATEGORY',
  'INGREDIENT_PARS_REMOVE_FROM_CATEGORY',
]);
ERROR_TYPES.push(actionTypes.INGREDIENT_PARS_LIST_ERROR);

let initialState = null;

const actionsMap = {
  [actionTypes.INGREDIENT_PARS_LIST]: (state, action) =>
    Immutable.fromJS(action.content.ingredientPars),
  [actionTypes.INGREDIENT_PARS_ADDITION]: (state, action) =>
    state.push(Immutable.fromJS(action.content.item)),
  [actionTypes.INGREDIENT_PARS_REMOVE]: (state, action) =>
    state.filter(i => i.get('product') !== action.content.productId),

  [actionTypes.INGREDIENT_PARS_ADD_TO_CATEGORY]: (state, action) => {
    const newTags = Immutable.Set(action.content.tags);
    let newState = state;
    action.content.productIds.forEach(productId => {
      const idx = newState.findIndex(i => i.get('product') === productId);
      newState = newState.updateIn([idx, 'tags'], Immutable.Set(), tags => tags.concat(newTags).toSet());
    });
    return newState;
  },

  [actionTypes.INGREDIENT_PARS_REMOVE_FROM_CATEGORY]: (state, action) => {
    const toRemoveTags = Immutable.Set(action.content.tags);
    let newState = state;
    action.content.productIds.forEach(productId => {
      const idx = newState.findIndex(i => i.get('product') === productId);
      newState = newState.updateIn([idx, 'tags'], Immutable.Set(), tags => tags.filter(t => !toRemoveTags.contains(t)));
    });
    return newState;
  },
};

type Par = $FlowTODO;

export const actionCreators = {
  list(ingredientPars: Array<Par>) {
    return {
      type: actionTypes.INGREDIENT_PARS_LIST,
      content: { ingredientPars },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.INGREDIENT_PARS_LIST_ERROR,
      content: { error },
    };
  },

  addIngredient(item: Par) {
    return {
      type: actionTypes.INGREDIENT_PARS_ADDITION,
      content: { item },
    };
  },

  removeIngredient(productId: number) {
    return {
      type: actionTypes.INGREDIENT_PARS_REMOVE,
      content: { productId },
    };
  },

  addIngredientToCategory(productIds: Array<number>, tags: Array<string>) {
    return {
      type: actionTypes.INGREDIENT_PARS_ADD_TO_CATEGORY,
      content: { productIds, tags },
    };
  },

  removeIngredientFromCategory(productIds: Array<number>, tags: Array<string>) {
    return {
      type: actionTypes.INGREDIENT_PARS_REMOVE_FROM_CATEGORY,
      content: { productIds, tags },
    };
  },
};

export default lookupTableReducer(initialState, actionsMap);
