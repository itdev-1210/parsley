/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

export type SharedAccount = {
  id: number,
  owner: number,
  sharedAccount: number,
  isActive: boolean,
  joinedAt: string,
  email: string,
}
export type SharedAccounts = Array<SharedAccount>;
export type ContributionInfo = {
  email: string,
  name: ?string,
};

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_SHARED_ACCOUNTS_SUCCESS', 'FETCH_SHARED_ACCOUNTS_ERROR',
  'FETCH_CONTRIBUTION_INFO_SUCCESS', 'FETCH_CONTRIBUTION_INFO_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_SHARED_ACCOUNTS_ERROR,
  actionTypes.FETCH_CONTRIBUTION_ERROR,
);

const initialState = Immutable.Map();

export const actionCreators = {
  received(sharedAccounts: SharedAccounts) {
    return {
      type: actionTypes.FETCH_SHARED_ACCOUNTS_SUCCESS,
      content: sharedAccounts
    }
  },

  error(error: Error) {
    return {
      type: actionTypes.FETCH_SHARED_ACCOUNTS_ERROR,
      content: error
    }
  },
  contributionInfoReceived(info: ContributionInfo) {
    return {
      type: actionTypes.FETCH_CONTRIBUTION_INFO_SUCCESS,
      content: info
    }
  },
  contributionInfoError(error: Error) {
    return {
      type: actionTypes.FETCH_CONTRIBUTION_INFO_ERROR,
      content: error
    }
  },
};

const actionsMap = {
  [actionTypes.FETCH_SHARED_ACCOUNTS_SUCCESS]: (state, action) =>
    state.set('sharedAccounts', Immutable.fromJS(action.content)),
  [actionTypes.FETCH_SHARED_ACCOUNTS_ERROR]: (state, action) =>
    state.set('sharedAccounts', null),
  [actionTypes.FETCH_CONTRIBUTION_INFO_SUCCESS]: (state, action) =>
    state.set('contributionInfo', Immutable.fromJS(action.content)),
  [actionTypes.FETCH_CONTRIBUTION_INFO_ERROR]: (state, action) =>
    state.set('contributionInfo', null),
};

export default lookupTableReducer(initialState, actionsMap);
