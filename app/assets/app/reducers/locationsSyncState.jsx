/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'FETCH_LOCATION_SYNC_STATUS_SUCCESS',
  'FETCH_LOCATION_SYNC_STATUS_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_LOCATION_SYNC_STATUS_ERROR,
);

export type LocationSyncState = {
  recipes: Array<number>,
  suppliers: Array<number>,
};

// location id -> { recipes, suppliers }
const initialState = Immutable.Map();

export const actionCreators = {
  received(locationId: string, syncState: LocationSyncState) {
    return {
      type: actionTypes.FETCH_LOCATION_SYNC_STATUS_SUCCESS,
      content: {
        locationId,
        syncState,
      },
    };
  },

  error(error: Error) {
    return {
      type: actionTypes.FETCH_LOCATION_SYNC_STATUS_ERROR,
      content: error,
    };
  },
};

const actionsMap = {
  [actionTypes.FETCH_LOCATION_SYNC_STATUS_SUCCESS]: (state, action) => state.set(action.content.locationId, Immutable.fromJS(action.content.syncState)),
  [actionTypes.FETCH_LOCATION_SYNC_STATUS_ERROR]: state => state,
};

export default lookupTableReducer(initialState, actionsMap);
