// @flow
import lookupTableReducer from 'common/redux/lookupTableReducer';
import Immutable from 'immutable';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror(['SET_MAIN_INFO_LOADED']);

let initialState = Immutable.Map();

const actionsMap = {
  [actionTypes.SET_MAIN_INFO_LOADED]: (state, action) => state.set(action.content.form, action.content.mainInfoLoaded),
};

export const actionCreators = {
  setMainInfoLoaded(form: string, mainInfoLoaded: boolean) {
    return {
      type: actionTypes.SET_MAIN_INFO_LOADED,
      content: { form, mainInfoLoaded },
    };
  },
};

export default lookupTableReducer(initialState, actionsMap);
