// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

export type recentPriceChange = {
  productId: number,
  percentage: number,
  monthlySpend: number,
};

const actionTypes = keyMirror([
  'RECENT_PRICE_CHANGE',
  'RECENT_PRICE_CHANGE_ERROR',
]);
ERROR_TYPES.push(actionTypes.RECENT_PRICE_CHANGE_ERROR);

export const actionCreators = {
  recentPriceChanges(recentChanges: Array<recentPriceChange>) {
    return {
      type: actionTypes.RECENT_PRICE_CHANGE,
      content: { recentChanges },
    };
  },

  recentPriceChangesError(error: Error) {
    return {
      type: actionTypes.RECENT_PRICE_CHANGE_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.RECENT_PRICE_CHANGE]: (state, action) =>
      Immutable.fromJS(action.content.recentChanges),
};

export default lookupTableReducer(initialState, actionsMap);
