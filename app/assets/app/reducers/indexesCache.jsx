// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';
import { actionTypes as IngredientActionTypes } from './ingredients';
import { actionTypes as MenuItemsActionTypes } from './menuItems';
import { actionTypes as SupplierActionTypes } from './suppliers';

const actionTypes = keyMirror([
  'SEARCH_INDEX_INGREDIENTS',
]);

export type IndexType =
    | 'PURCHASE_PRODUCTS' // purchasable by user from suppliers
    | 'IMPORT_PURCHASE_PRODUCTS' // same as above, but special case with different index opts
    | 'INVENTORY_ITEMS' // usable in pars, inventory
    | 'INGREDIENTS' // usable in recipes
    | 'MENU_ITEMS' // usable in orders/menus
    | 'SUPPLIERS';

export const indexTypes: { [t: IndexType]: IndexType } = keyMirror([
  'PURCHASE_PRODUCTS', // purchasable by user from suppliers
  'IMPORT_PURCHASE_PRODUCTS', // same as above, but special case with different index opts
  'INVENTORY_ITEMS', // usable in pars, inventory
  'INGREDIENTS', // usable in recipes
  'MENU_ITEMS', // usable in orders/menus
  'SUPPLIERS',
]);


const actionTemplate = (content: any, key: IndexType) => ({
  type: actionTypes.SEARCH_INDEX_INGREDIENTS,
  content,
  key,
});

export const actionCreators = {
  addIndexToCache(indexType: IndexType, content: any) {
    return actionTemplate(content, indexType);
  },
};

type State = Immutable.Map<IndexType, any>

function indexResetter(...types: Array<IndexType>) {
  return (state: State, action) => {
    let newState = state;
    for (const t of types) {
      newState = newState.delete(t);
    }
    return newState;
  };
}

let initialState: State = Immutable.Map();

const actionsMap = {
  [actionTypes.SEARCH_INDEX_INGREDIENTS]: (state, action) =>
    state.set(action.key, action.content),
  [IngredientActionTypes.INGREDIENT_LIST]: indexResetter(
      indexTypes.INGREDIENTS, indexTypes.INVENTORY_ITEMS, indexTypes.PURCHASE_PRODUCTS,
  ),
  [MenuItemsActionTypes.MENU_ITEM_LIST]: indexResetter(indexTypes.MENU_ITEMS),
  [SupplierActionTypes.SUPPLIER_LIST]: indexResetter(indexTypes.SUPPLIERS),
};

export default lookupTableReducer(initialState, actionsMap);
