// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

export type TotalRecipesAddedChanged = {
  added: number,
  changed: number,
};

const actionTypes = keyMirror([
  'TOTAL_RECIPES_ADDED_CHANGED',
  'TOTAL_RECIPES_ADDED_CHANGED_ERROR',
]);
ERROR_TYPES.push(actionTypes.TOTAL_RECIPES_ERROR);

export const actionCreators = {
  totalRecipesAddedChanged(totals: TotalRecipesAddedChanged) {
    return {
      type: actionTypes.TOTAL_RECIPES_ADDED_CHANGED,
      content: { totals },
    };
  },

  totalRecipesAddedChangedError(error: Error) {
    return {
      type: actionTypes.TOTAL_RECIPES_ADDED_CHANGED_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.TOTAL_RECIPES_ADDED_CHANGED]: (state, action) =>
      Immutable.fromJS(action.content.totals),
};

export default lookupTableReducer(initialState, actionsMap);
