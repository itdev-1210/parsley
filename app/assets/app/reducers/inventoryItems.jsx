// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'INVENTORY_ITEMS_LIST',
  'INVENTORY_ITEMS_LIST_ERROR',
  'INVENTORY_ITEMS_ADDITION',
  'INVENTORY_ITEMS_REMOVE',
  'INVENTORY_ITEMS_ADD_TO_CATEGORY',
  'INVENTORY_ITEMS_REMOVE_FROM_CATEGORY',
]);

ERROR_TYPES.push(actionTypes.INVENTORY_ITEMS_LIST_ERROR);

let initialState = null;

const actionsMap = {
  [actionTypes.INVENTORY_ITEMS_LIST]: (state, action) =>
    Immutable.fromJS(action.content.inventoryItems),
  [actionTypes.INVENTORY_ITEMS_ADDITION]: (state, action) =>
    state.push(Immutable.fromJS(action.content.item)),
  [actionTypes.INVENTORY_ITEMS_REMOVE]: (state, action) =>
    state.filter(i => i.get('product') !== action.content.productId),

  [actionTypes.INVENTORY_ITEMS_ADD_TO_CATEGORY]: (state, action) => {
    const newTags = Immutable.Set(action.content.tags);
    let newState = state;
    action.content.productIds.forEach(productId => {
      const idx = newState.findIndex(i => i.get('product') === productId);
      newState = newState.updateIn([idx, 'tags'], Immutable.Set(), tags => tags.concat(newTags).toSet());
    });
    return newState;
  },

  [actionTypes.INVENTORY_ITEMS_REMOVE_FROM_CATEGORY]: (state, action) => {
    const toRemoveTags = Immutable.Set(action.content.tags);
    let newState = state;
    action.content.productIds.forEach(productId => {
      const idx = newState.findIndex(i => i.get('product') === productId);
      newState = newState.updateIn([idx, 'tags'], Immutable.Set(), tags => tags.filter(t => !toRemoveTags.contains(t)));
    });
    return newState;
  },
};

type InventoryItem = $FlowTODO;

export const actionCreators = {
  list(inventoryItems: Array<InventoryItem>) {
    return {
      type: actionTypes.INVENTORY_ITEMS_LIST,
      content: { inventoryItems },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.INVENTORY_ITEMS_LIST_ERROR,
      content: { error },
    };
  },

  addIngredient(item: InventoryItem) {
    return {
      type: actionTypes.INVENTORY_ITEMS_ADDITION,
      content: { item },
    };
  },

  removeIngredient(productId: number) {
    return {
      type: actionTypes.INVENTORY_ITEMS_REMOVE,
      content: { productId },
    };
  },

  addIngredientToCategory(productIds: Array<number>, tags: Array<string>) {
    return {
      type: actionTypes.INVENTORY_ITEMS_ADD_TO_CATEGORY,
      content: { productIds, tags },
    };
  },

  removeIngredientFromCategory(productIds: Array<number>, tags: Array<string>) {
    return {
      type: actionTypes.INVENTORY_ITEMS_REMOVE_FROM_CATEGORY,
      content: { productIds, tags },
    };
  },
};

export default lookupTableReducer(initialState, actionsMap);
