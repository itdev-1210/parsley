// @flow
import Immutable from 'immutable';

import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

/*
 * Ingredient info for usage in recipes
 */
const actionTypes = keyMirror([
 'SUPPLIER_MULTIPLE_INFO',
 'SUPPLIER_MULTIPLE_INFO_ERROR',
]);
ERROR_TYPES.push(actionTypes.SUPPLIER_MULTIPLE_INFO_ERROR);

export const actionCreators = {
  multipleInfo(deets: Array<*>) {
    return {
      type: actionTypes.SUPPLIER_MULTIPLE_INFO,
      content: { deets },
    };
  },

  infoError(error: Error) {
    return {
      type: actionTypes.SUPPLIER_MULTIPLE_INFO_ERROR, error,
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.SUPPLIER_MULTIPLE_INFO]: (state, action) =>
    // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.deets).mapKeys(k => parseInt(k)),
  [actionTypes.SUPPLIER_MULTIPLE_INFO_ERROR]: (state, action) => state.delete(action.content.id),
};

export default lookupTableReducer(initialState, actionsMap);
