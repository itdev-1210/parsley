// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

export const actionTypes = keyMirror([
  'ADVANCED_PREPS_LIST',
  'ADVANCED_PREPS_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.ADVANCED_PREPS_LIST_ERROR);

export const actionCreators = {
  advancedPrepsList(preps: SummaryList) {
    return {
      type: actionTypes.ADVANCED_PREPS_LIST,
      content: { preps },
    };
  },

  advancedPrepsListError(error: Error) {
    return {
      type: actionTypes.ADVANCED_PREPS_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = Immutable.List();

const actionsMap = {
  [actionTypes.ADVANCED_PREPS_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
    Immutable.fromJS(action.content.preps).sortBy(i => i.get('name').toLowerCase()),
};

export default lookupTableReducer(initialState, actionsMap);
