// @flow
import Immutable from 'immutable';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import keyMirror from 'common/utils/keyMirror';

import type { ProductSource } from '../ui-components/utils/PropTypes';

const actionTypes = keyMirror([
  "SUPPLIER_SOURCED_INGREDIENTS",
  "SUPPLIER_DROP_SOURCED_INGREDIENTS",
  "SUPPLIER_SOURCED_INGREDIENTS_ERROR",
]);

ERROR_TYPES.push(actionTypes.SUPPLIER_SOURCED_INGREDIENTS_ERROR);

export const actionCreators = {
  received(id: number, sources: Array<ProductSource>) {
    return {
      type: actionTypes.SUPPLIER_SOURCED_INGREDIENTS,
      content: {
        id, sources,
      },
    }
  },
  drop(id: number) {
    return {
      type: actionTypes.SUPPLIER_DROP_SOURCED_INGREDIENTS,
      content: { id }
    }
  },

  failure(error: Error) {
    return {
      type: actionTypes.SUPPLIER_SOURCED_INGREDIENTS_ERROR,
      content: {
        error,
      },
    }
  },
};

let initialState = Immutable.Map();

const actionsMap = {
  [actionTypes.SUPPLIER_SOURCED_INGREDIENTS]: (state, action) =>
      state.set(action.content.id, Immutable.fromJS(action.content.sources)),
  [actionTypes.SUPPLIER_DROP_SOURCED_INGREDIENTS]: (state, action) =>
      state.delete(action.content.id),
};

export default lookupTableReducer(initialState, actionsMap);