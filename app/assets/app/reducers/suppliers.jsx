// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

import type { SummaryList } from '../ui-components/utils/PropTypes';

export const actionTypes = keyMirror([
  'SUPPLIER_LIST',
  'SUPPLIER_LIST_ERROR',
]);
ERROR_TYPES.push(actionTypes.SUPPLIER_LIST_ERROR);

export const actionCreators = {
  list(suppliers: SummaryList) {
    return {
      type: actionTypes.SUPPLIER_LIST,
      content: { suppliers },
    };
  },

  listError(error: Error) {
    return {
      type: actionTypes.SUPPLIER_LIST_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.SUPPLIER_LIST]: (state, action) =>
      // $FlowFixMe: no fromJS() typing
      Immutable.fromJS(action.content.suppliers).sortBy(i => i.get("name").toLowerCase()),
};

export default lookupTableReducer(initialState, actionsMap);
