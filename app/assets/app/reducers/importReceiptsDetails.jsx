// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

const actionTypes = keyMirror([
  'IMPORT_RECEIPTS_DETAILS',
  'IMPORT_RECEIPTS_ERROR',
]);
ERROR_TYPES.push(actionTypes.IMPORT_RECEIPTS_ERROR);

export const actionCreators = {
  csvDetails(headers: Array<string>, data: Array<*>) {
    return {
      type: actionTypes.IMPORT_RECEIPTS_DETAILS,
      content: { headers, data },
    };
  },

  detailsError(error: Error) {
    return {
      type: actionTypes.IMPORT_RECEIPTS_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.IMPORT_RECEIPTS_DETAILS]: (state, action) => Immutable.fromJS(action.content),
  [actionTypes.IMPORT_RECEIPTS_ERROR]: (state, action) => null,
};

export default lookupTableReducer(initialState, actionsMap);
