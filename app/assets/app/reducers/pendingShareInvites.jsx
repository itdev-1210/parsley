/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

export type PendingShareInvite = {
  id: string,
  inviteeEmail: string,
  inviteeLevel: string,
}

const actionTypes = keyMirror([
  'FETCH_PENDING_SHARE_INVITES_SUCCESS', 'FETCH_PENDING_SHARE_INVITES_ERROR',
]);

ERROR_TYPES.push(
  actionTypes.FETCH_PENDING_SHARE_INVITES_SUCCESS,
  actionTypes.FETCH_PENDING_SHARE_INVITES_ERROR,
  actionTypes.DROP_FETCHED_PENDING_SHARE_INVITES,
  actionTypes.PENDING_SHARE_INVITE_DELETED,
  actionTypes.PENDING_SHARE_INVITE_DELETE_ERROR,
);

const initialState = Immutable.Map();

export const actionCreators = {
  received(pendingShareInvites: PendingShareInvite) {
    return {
      type: actionTypes.FETCH_PENDING_SHARE_INVITES_SUCCESS,
      content: pendingShareInvites,
    }
  },

  error(error: Error) {
    return {
      type: actionTypes.FETCH_PENDING_SHARE_INVITES_ERROR,
      content: { error },
    }
  },
}

const actionsMap = {
  [actionTypes.FETCH_PENDING_SHARE_INVITES_SUCCESS]: (state, action) =>
    state.set('pendingShareInvites', Immutable.fromJS(action.content)),
  [actionTypes.FETCH_PENDING_SHARE_INVITES_ERROR]: (state, action) =>
    state.set('pendingShareInvites', null),
};

export default lookupTableReducer(initialState, actionsMap);
