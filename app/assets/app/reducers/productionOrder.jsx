/** @flow */
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';

import keyMirror from 'common/utils/keyMirror';

export type BaseOrder =
    | Immutable.List<Immutable.Map<string, any>> // { product: <id>, quantity: <quantity> }
    | Immutable.List<number>; // order IDs

let initialState = null;

const actionTypes = keyMirror([
  'PRODUCTION_ORDER', 'PRODUCTION_ORDER_ERROR', 'DROP_PRODUCTION_ORDER'
]);
const actionsMap = {
  [actionTypes.PRODUCTION_ORDER]: (state, action) => {
    // could just pass action.content straight to Immutable.fromJS, but want to explicitly enumerate fields
    // storing the baseOrder because it may be non-trivial to determine whether to throw out or keep this
    // info otherwise
    var { baseOrder, batched, productionOrder } = action.content;
    return Immutable.fromJS({ baseOrder, batched, productionOrder })
  },
  [actionTypes.PRODUCTION_ORDER_ERROR]: (state, action) => initialState,
  [actionTypes.DROP_PRODUCTION_ORDER]: (state, action) => initialState,
};

// purchasing, prep lists, and the like. used in multiple UI screens

ERROR_TYPES.push(actionTypes.PRODUCTION_ORDER_ERROR);

export default lookupTableReducer(initialState, actionsMap);

// utility for callers
export function productionOrderFromState(state: Immutable.Map<string, $FlowTODO>, baseOrder: BaseOrder, batched: boolean) {
  if (
      !Immutable.is(state.getIn(['productionOrder', 'baseOrder']), baseOrder)
      || state.getIn(['productionOrder', 'batched']) !== batched
  ) {
    return null;
  }

  return state.getIn(['productionOrder', 'productionOrder']);
}

export const actionCreators = {
  received(baseOrder: BaseOrder, batched: boolean, productionOrderDetails: any) {
    return {
      type: actionTypes.PRODUCTION_ORDER,
      content: { baseOrder, batched, productionOrder: productionOrderDetails }
    };
  },

  error(error: Error) {
    return {
      type: actionTypes.PRODUCTION_ORDER_ERROR,
      content: {
        error,
      },
    };
  },

  dropDetails() {
    return {
      type: actionTypes.DROP_PRODUCTION_ORDER
    }
  },
};
