// @flow
import Immutable from 'immutable';
import { ERROR_TYPES } from 'common/redux/actions/Types';
import lookupTableReducer from 'common/redux/lookupTableReducer';
import keyMirror from 'common/utils/keyMirror';

export type TotalRecipes = {
  total: number,
  subRecipes: number,
  topLevel: number,
};

const actionTypes = keyMirror([
  'TOTAL_RECIPES',
  'TOTAL_RECIPES_ERROR',
]);
ERROR_TYPES.push(actionTypes.TOTAL_RECIPES_ERROR);

export const actionCreators = {
  totalRecipes(totals: TotalRecipes) {
    return {
      type: actionTypes.TOTAL_RECIPES,
      content: { totals },
    };
  },

  totalRecipesError(error: Error) {
    return {
      type: actionTypes.TOTAL_RECIPES_ERROR,
      content: { error },
    };
  },
};

let initialState = null;

const actionsMap = {
  [actionTypes.TOTAL_RECIPES]: (state, action) =>
      Immutable.fromJS(action.content.totals),
};

export default lookupTableReducer(initialState, actionsMap);
