/* @flow */
import { transformErrorValue, addSideEffect } from 'common/utils/promises';
import Immutable from 'immutable';
import _ from 'lodash';

import { actionCreators as SupplierListActions } from '../reducers/suppliers';
import { actionCreators as SupplierDetailsActions } from '../reducers/supplierDetails';
import { actionCreators as SuppliersActions } from '../reducers/detailedSuppliers';
import {
  actionCreators as supplierSourcedIngredientActionCreators,
} from '../reducers/supplierSourcedIngredients';

import { actionCreators as MeasureActions } from '../reducers/measures';
import { actionCreators as ProductDetailsActions } from '../reducers/productDetails';
import { actionCreators as IngredientsActions } from '../reducers/ingredients';
import { actionCreators as AdvancedIngredientPrepsActions } from '../reducers/advancedIngredientPreps';
import { actionCreators as MenuItemsActions } from '../reducers/menuItems';
import { actionCreators as RecipesActions } from '../reducers/recipes';
import { actionCreators as TotalRecipesActions } from '../reducers/totalRecipes';
import { actionCreators as totalRecipesAddedChangedActions } from '../reducers/totalRecipesAddedChanged';
import { actionCreators as recipesAddedChangedActions } from '../reducers/recipesAddedChanged';
import { actionCreators as ReverseDependencyActions } from '../reducers/reverseDependencies';
import { actionCreators as IngredientActions } from '../reducers/detailedIngredients';
import { actionCreators as MenuListActions } from '../reducers/menus';
import { actionCreators as MenuDetailsActions } from '../reducers/menuDetails';
import { actionCreators as OrderListActions } from '../reducers/orders';
import { actionCreators as OrderDetailsActions } from '../reducers/orderDetails';
import { actionCreators as UpstreamActions } from '../reducers/upstreamInfo';
import { actionCreators as LocationSyncStatusActions } from '../reducers/locationsSyncState';
import { actionCreators as LocationActions } from '../reducers/locations';
import { actionCreators as OnboardingActions } from '../reducers/onboardingInfo';
import { actionCreators as OnboardingWidgetsActions } from '../reducers/onboardingWidgetsInfo';
import { actionCreators as UserInvitationActions } from '../reducers/userInvitation';
import { actionCreators as PendingShareInvites } from '../reducers/pendingShareInvites';
import { actionCreators as ProductionAnalyticsActions } from '../reducers/productionOrder';
import { actionCreators as PurchaseActions } from '../reducers/purchaseOrders';
import { actionCreators as PurchaseDetailsActions } from '../reducers/purchaseOrderDetails';
import type { BaseOrder } from '../reducers/productionOrder';
import { actionCreators as NutrientActions } from '../reducers/nutrients';
import { actionCreators as TagActions } from '../reducers/tags';
import { actionCreators as InventoryActions } from '../reducers/inventories';
import { actionCreators as InventoryCurrentActions } from '../reducers/inventoryCurrent';
import { actionCreators as InventoryDetailsActions } from '../reducers/inventoryDetails';
import { actionCreators as InventoryItemsActions } from '../reducers/inventoryItems';
import { actionCreators as IngredientParsActions } from '../reducers/ingredientPars';
import { actionCreators as RecentPriceChangesActions } from '../reducers/recentPriceChanges';
import { actionCreators as PosSetupActions } from '../reducers/posSetup';
import { actionCreators as PosImportActions } from '../reducers/posImportPreferences';
import { actionCreators as ImportReceivablesActions } from '../reducers/importReceivablesPricingDetails';
import { get, post, put, del, jsObjectFetch, postMultipart } from 'common/webapi/utils';
import type { RequestContent } from 'common/webapi/utils';
import moment from 'moment';

import reduxStore from '../store';
import type {
  ProductSource,
  AccountListInfo,
  UserListInfo,
  GraphData,
  PurchaseHistoryPayload,
} from '../ui-components/utils/PropTypes';
import { noticeError } from 'common/utils/errorAnalytics';
window.reduxStore = reduxStore;

type ErrorAction = {|
  type: string,
  content: {|
    error: Error,
  |}
|}
type BaseAction = {|
  type: string,
  content: mixed,
|}
type ErrorActionCreator = (err: Error) => ErrorAction;
type ListActions = {
  list: (items: Array<{id: number, name: string, tags: ?Array<string>}>) => BaseAction,
  listError: ErrorActionCreator,
}

type CrudActions = {
  deleted: (id: number) => BaseAction,
  deleteError: ErrorActionCreator,
  saved: (id: number) => BaseAction,
  saveError: ErrorActionCreator,
}

type DetailsActions = {
  details: (id: number, deets: mixed) => BaseAction,
  detailsError: ErrorActionCreator,
}
type PurchaseActionMode = 'print' | 'email';

export * from 'common/webapi/userEndpoints';
export * from 'common/webapi/stripeEndpoints';

function getList<Item>(uri, actions: ?ListActions): Promise<Array<Item>> {
  return get(uri)
    .then(
      items => {
        if (actions) {
          // clear the index
          reduxStore.dispatch(actions.list(items));
        }
        return items;
      },
      transformErrorValue(addSideEffect(error =>
          actions && reduxStore.dispatch(actions.listError(error)))),
    );
}

function getDetails(uri, actions: DetailsActions, id: number) {
  id = parseInt(id);
  return get(uri + '/' + id.toString())
    .then(
      deets => {
        if (actions) {
          reduxStore.dispatch(actions.details(id, deets));
        }
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
          reduxStore.dispatch(actions.detailsError(error)))),
    );
}

export function getAccountsList(): Promise<Array<AccountListInfo>> {
  return getList('/accounts', null);
}

export function getUsersList(): Promise<Array<UserListInfo>> {
  return getList('/users', null);
}

type SaveTarget = 'new' | number

export function save(uri: string, actions: CrudActions, id: SaveTarget, deets: RequestContent) {
  const req = typeof(id) === 'string' && id === 'new' ?
    post(uri, deets) :
    // $FlowFixMe if id was 'new', then we wouldn't have hit this case. couldn't manage to get type refinement right
    put(uri + '/' + id.toString(), deets).then(() => (id: number));

  return req.then(
    savedId => {
      reduxStore.dispatch(actions.saved(savedId));
      return savedId;
    },
    transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(actions.saveError(error)))),
  );
}

export function deleteItem(uri: string, actions: CrudActions, id: number) {
  return del(uri + '/' + id.toString())
    .then(
      success => reduxStore.dispatch(actions.deleted(id)),
      transformErrorValue(addSideEffect(error =>
          reduxStore.dispatch(actions.deleteError(error)))),
    );
}

export function getSupplierList() {
  return getList('/suppliers', SupplierListActions);
}

export function getSupplierDetails(id: number) {
  return getDetails('/suppliers', SupplierDetailsActions, id);
}

export function lookupSupplierName(name: string) {
  if (!name || !name.length) {
    throw 'Name is not valid.';
  }
  return post('/suppliers/name', { name });
}

/**
 * @param id ID of the supplier
 */
export function getSupplierSourcedIngredients(id: number): Promise<Array<ProductSource>> {
  return get(`/suppliers/${id}/sourcedIngredients`)
      .then(
          sourcedIngredients => {
            reduxStore.dispatch(supplierSourcedIngredientActionCreators.received(id, sourcedIngredients));
            return sourcedIngredients;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(supplierSourcedIngredientActionCreators.failure(error)))),
      );
}

// returns a promise of the supplier's ID after the call (either newly-
// assigned for arg id == 'new', or from the argument otherwise)
export function saveSupplier(id: SaveTarget, deets: RequestContent) {
  return save('/suppliers', SupplierDetailsActions, id, deets);
}

export function deleteSupplier(id: number) {
  return deleteItem('/suppliers', SupplierDetailsActions, id);
}
export function getMeasures() {
  return getList('/measures', MeasureActions);
}

export function deleteMeasure(id: number) {
  return deleteItem('/measures', MeasureActions, id);
}

export function saveMeasure(id: SaveTarget, deets: RequestContent) {
  return save('/measures', MeasureActions, id, deets);
}

export function getNutrients() {
  return get('/nutrients')
      .then(
          nutrients => {
            reduxStore.dispatch(NutrientActions.received(nutrients));
            return nutrients;
          },
      );
}

export function saveNutrients(deets: RequestContent) {
  return put('/nutrients', deets);
}

export function getIngredientList() {
  return get('/ingredients')
      .then(
          products => {
            reduxStore.dispatch(IngredientsActions.ingredientList(products));
            return products;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(IngredientsActions.ingredientListError(error)))),
      );
}

export function getAdvancedIngredientPreps() {
  return get('/ingredients/advancedPreps')
      .then(
          preps => {
            reduxStore.dispatch(AdvancedIngredientPrepsActions.advancedPrepsList(preps));
            return preps;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(AdvancedIngredientPrepsActions.advancedPrepsListError(error)))),
      );
}

export function getIngredientParsExists() {
  return get('/ingredients/pars/exists');
}

export function getIngredientPars() {
  return get('/ingredients/pars')
    .then(
      par => {
        reduxStore.dispatch(IngredientParsActions.list(par));
        return par;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(IngredientParsActions.listError(error)))),
    );
}

export function saveIngredientPars(items, removedProductIds) {
  return post('/ingredients/pars', {
    items,
    removedProductIds,
  });
}

export function getRecipeList() {
  return get('/recipes')
      .then(
          products => {
            reduxStore.dispatch(RecipesActions.recipeList(products));
            return products;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(RecipesActions.recipeListError(error)))),
      );
}

export function getTotalRecipes() {
  return get('/totalRecipes')
      .then(
          totals => {
            reduxStore.dispatch(TotalRecipesActions.totalRecipes(totals));
            return totals;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(TotalRecipesActions.totalRecipesError(error)))),
      );
}

export function getTotalRecipesAddedChanged() {
  return get('/totalRecipesAddedAndChanged')
      .then(
          totals => {
            reduxStore.dispatch(totalRecipesAddedChangedActions.totalRecipesAddedChanged(totals));
            return totals;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(totalRecipesAddedChangedActions.totalRecipesAddedChangedError(error)))),
      );
}

export function getRecipesAddedChanged() {
  return get('/recipesAddedAndChanged')
      .then(
        recipes => {
            reduxStore.dispatch(recipesAddedChangedActions.recipesAddedChanged(recipes));
            return recipes;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(recipesAddedChangedActions.recipesAddedChangedError(error)))),
      );
}

export function getReverseDependencies(id: number) {
  return get(`/product/${id.toString()}/reverseDependencies`)
      .then(
          recipes => {
            reduxStore.dispatch(ReverseDependencyActions.received(id, recipes));
            return recipes;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(ReverseDependencyActions.error(error)))),
      );
}

export function getMenuItemsList() {
  return get('/menu_items')
      .then(
          products => {
            reduxStore.dispatch(MenuItemsActions.menuItemList(products));
            return products;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(MenuItemsActions.menuItemListError(error)))),
      );
}

export function getProductDetails(id: number) {
  return getDetails('/products', ProductDetailsActions, id);
}

// returns a promise of the product's ID after the call (either newly-
// assigned for arg id == 'new', or from the argument otherwise)
export function saveProduct(id: SaveTarget, deets: RequestContent) {
  if (!deets.name) {
    noticeError('tried to save a nameless product (probably prep)');
    return Promise.reject('tried to save a nameless product (probably prep)');
  }
  return save('/products', ProductDetailsActions, id, deets);
}

export function deleteProduct(id: number) {
  return deleteItem('/products', ProductDetailsActions, id);
}

export function getMenuList() {
  return getList('/menus', MenuListActions);
}

export function getMenuDetails(id: number) {
  return getDetails('/menus', MenuDetailsActions, id);
}

export function saveMenu(id: SaveTarget, deets: RequestContent) {
  return save('/menus', MenuDetailsActions, id, deets);
}

export function deleteMenu(id: number) {
  return deleteItem('/menus', MenuDetailsActions, id);
}

export function getOrderList() {
  return getList('/orders', OrderListActions);
}

export function getOrderDetails(id: number) {
  return getDetails('/orders', OrderDetailsActions, id);
}

export function getInventoryList() {
  return get('/inventories')
    .then(
      items => {
        reduxStore.dispatch(InventoryActions.list(items));
        return items;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(InventoryActions.listError(error)))),
    );
}

export function getInventoryLastTakenTime() {
  return get('/inventories/lastTakenTime');
}

export function getInventoryDetails(id: number) {
  return get('/inventories/' + id.toString())
    .then(
      deets => {
        reduxStore.dispatch(InventoryDetailsActions.details(deets));
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(InventoryDetailsActions.detailsError(error)))),
    );
}

export function getPreviousInventoryDetails() {
  const today = moment().format('YYYYMMDD');
  return get('/inventories/previous/' + today);
}

export function deleteInventory(id: number) {
  return del('/inventories/' + id.toString());
}

export function getNewInventoryItemList() {
  const today = moment().format('YYYYMMDD');
  return get('/inventories/take/' + today)
    .then(
      data => {
        reduxStore.dispatch(InventoryItemsActions.list(data.items));
        return data;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(InventoryItemsActions.listError(error)))),
    );
}

export function saveInventory(items, removedProductIds, mergedPreviousInventory) {
  return post('/inventories', {
    items,
    removedProductIds,
    today: moment().format('YYYYMMDD'),
    mergedPreviousInventory,
  });
}

export function getCurrentInventory(ignoreToday) {
  return get('/inventories/current', ignoreToday && { today: moment().format('YYYYMMDD') })
    .then(data => {
      reduxStore.dispatch(InventoryCurrentActions.list(data.items));
      return data;
    },
    transformErrorValue(addSideEffect(error =>
      reduxStore.dispatch(InventoryCurrentActions.listError(error)))),
    );
}

export type OrderJSON = {
  name: string,
  description: ?string,
  menu: number | Object, // TODO: menu type
  transaction: Object, // TODO: transaction type
}
export function saveOrder(id: SaveTarget, deets: RequestContent) {
  return save('/orders', OrderDetailsActions, id, deets);
}

export function deleteOrder(id: number) {
  return deleteItem('/orders', OrderDetailsActions, id);
}

export function getPurchaseList() {
  return get('/purchase_orders')
    .then(
      purchaseList => {
        reduxStore.dispatch(PurchaseActions.list(purchaseList));
        return purchaseList;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(PurchaseActions.listError(error)),
      )),
    );
}

export function getPurchaseDetails(id: number) {
  return get('/purchase_orders/' + id)
    .then(
      deets => {
        reduxStore.dispatch(PurchaseDetailsActions.details(deets));
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(PurchaseDetailsActions.detailsError(error)),
      )),
    );
}

export function createPurchase(supplierId: number, orders: Array<number>, deets: RequestContent, purchaseActionMode: PurchaseActionMode, receivedDirectly: boolean = false) {
  // $FlowFixMe: no fromJS() typing
  let orderInfo: Immutable.Map<string, *> = Immutable.fromJS(deets);
  orderInfo = orderInfo.set('supplierId', orderInfo.getIn([0, 'supplierId']));
  orderInfo = orderInfo.map(i => i.flatten(true)
    .delete('index').delete('name').delete('purchasePackageName').delete('userAdded').delete('supplierId'),
    );
  const reqBody = { orders, items: orderInfo.toJS(), receivedDirectly };

  if (purchaseActionMode === 'print' || receivedDirectly) {
    return savePurchase(supplierId, reqBody);
  } else if (purchaseActionMode === 'email') {
    return sendOrderToSupplier(supplierId, reqBody);
  } else {
    console.error(`Unknown purchase action mode: ${purchaseActionMode}`);
    return Promise.reject();
  }
}

function sendOrderToSupplier(id: number, reqBody: RequestContent) {
  return post(`/suppliers/${id}/send`, reqBody);
}

function savePurchase(id: number, reqBody: RequestContent) {
  return post(`/purchase_orders/${id}`, reqBody);
}

export function receivePurchase(id: number, deets: RequestContent) {
  return post('/receive_purchase/' + id, deets);
}

// for consumers of ingredients only, not editing
export function getProductUsageInfo(id: number) {
  id = parseInt(id);
  if (!Number.isInteger(id)) {
    throw 'Product ID is not valid.';
  }
  return get(`/products/${id.toString()}/usage_info`)
      .then(
          deets => {
            reduxStore.dispatch(IngredientActions.info(id, deets));
            return deets;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(IngredientActions.infoError(error)))),
      );
}

function fetchProductsUsageInfo(ids: Array<number>, additionalParams: Object) {
  return get('/products/multiple_usage_info/' + ids.join(','), additionalParams)
    .then(
        data => {
          reduxStore.dispatch(IngredientActions.multipleInfo(data));
          return data;
        },
        transformErrorValue(addSideEffect(error =>
            reduxStore.dispatch(IngredientActions.infoError(error)))),
    );
}

function fetchMultipleProductDetails(ids: Array<number>, additionalParams: Object) {
  return get('/multipleProducts/' + ids.join(','), additionalParams)
    .then(data => {
      reduxStore.dispatch(ProductDetailsActions.multipleDetails(data));
      return data;
    },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(ProductDetailsActions.detailsError(error)))),
    );
}

function createBatchFetchFunction(
    fetchFunc: (Array<number>, ?Object) => Promise<*>,
    mergerFunc?: (?any, ?any) => any, // when objects have mismatching keys, called with undefined
) {
  return function (
    ids: Array<number> | Immutable.List<number> | Immutable.Set<number>,
    batchSize = 10,
    additionalParams = {},
  ) {
    if (!Array.isArray(ids)) {
      ids = ids.valueSeq().toArray();
    }

    const idChunks = _.chunk(ids, batchSize);
    return Promise.all(idChunks.map(chunk => fetchFunc(chunk, additionalParams)))
      .then(resultChunks =>
        _.reduce(resultChunks, (m, obj) => _.mergeWith(m, obj, mergerFunc), {}),
      );
  };
}

// $FlowFixMe: Don't know how to make the *requirement* for a second arg optional
export const getMultipleProductsUsageInfo = createBatchFetchFunction(fetchProductsUsageInfo);
// $FlowFixMe: Don't know how to make the *requirement* for a second arg optional
export const getMultipleProductsDetails = createBatchFetchFunction(fetchMultipleProductDetails);

function syncStdlibUserBatch(users: Array<number>) {
  // using jsObjectFetch() because post() requires response in format { id: number }
  return jsObjectFetch('/partial_stdlib_sync', 'POST', { content: users });
}
export const syncStdlibUsers = createBatchFetchFunction(
    syncStdlibUserBatch,
    (a, b) => a && b && a + b,
);

export function lookupProductName(name: string) {
  if (!name || !name.length) {
    throw 'Name is not valid.';
  }
  return post('/products/name', { name });
}

export function lookupMenuName(name: string) {
  if (!name || !name.length) {
    throw 'Name is not valid.';
  }
  return post('/menus/name', { name });
}

export function getProductionOrder(
    baseOrder: BaseOrder,
    batched: boolean = false,
    includePar: boolean = false,
    subtractLatestInventory: boolean = false,
    subtractCurrentInventory: boolean = false,
) {
  let baseUrl;
  if (typeof(baseOrder.first()) === 'number') {
    // list of orders
    baseUrl = '/bulk_production_order';
  } else {
    // list of { product, quantity } entries
    baseUrl = '/production_order';
  }

  // TODO: let users of store distinguish batched and unbatched cases
  return jsObjectFetch(`${baseUrl}`, 'POST', { content: { orders: baseOrder.toJS(), batched, includePar, subtractLatestInventory, subtractCurrentInventory } })
      .then(
          addSideEffect(productionOrder =>
              reduxStore.dispatch(ProductionAnalyticsActions.received(baseOrder, batched, productionOrder))),
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(ProductionAnalyticsActions.error(error)))),
      );
}

export type TagType = 'recipes' | 'ingredients' | 'inventories';
export function getTagsList(tagType: TagType) {
  return get(`/tags/${tagType}`)
    .then(
      items => {
        reduxStore.dispatch(TagActions.list(tagType, items));
        return items;
      },
      transformErrorValue(addSideEffect(error =>
          TagActions && reduxStore.dispatch(TagActions.listError(error)))),
    );
}

export function saveCategories(tagType: TagType, tags) {
  return post(`/tags/${tagType}`, tags);
}

export function addToCategory(tagType: TagType, targetIds, tags) {
  return post(`/addToCategory/${tagType}`, {
    targetIds, tags,
  });
}

export function removeFromCategory(tagType: TagType, targetIds, tags) {
  return post(`/removeFromCategory/${tagType}`, {
    targetIds, tags,
  });
}

export function getOwnerInfo() {
  return get('/owner')
    .then(
      deets => {
        reduxStore.dispatch(UserInvitationActions.contributionInfoReceived(deets));
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
          reduxStore.dispatch(UserInvitationActions.contributionInfoError(error)),
      )),
    );
}

export function getUpstreamInfo() {
  return get('/users/upstreams')
    .then(
      deets => {
        reduxStore.dispatch(UpstreamActions.infoReceived(deets));
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(UpstreamActions.infoError(error)),
      )),
    );
}

export function createLocation(deets) {
  return post('/locations', deets);
}

export function getLocationList() {
  return getList('/users/downstreams', LocationActions);
}

export function lookupLocation(deets: {name: string}) {
  return post('/locations/lookup', deets);
}

export function updateLocation(id: number, deets) {
  return put('/locations/' + id, deets);
}

export function removeLocation(id: number) {
  return del('/locations/' + id);
}

export function leaveAccount(id) {
  return post('/user/leave', { ownerId: id });
}

export function removeSharedUser(id) {
  return post('/user/remove/' + id, {});
}

export function getLocationsSyncState(locationId) {
  return get('/locations/sync_state/' + locationId)
    .then(
      syncState => {
        reduxStore.dispatch(LocationSyncStatusActions.received(locationId, syncState));
        return syncState;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(LocationSyncStatusActions.error(error)),
      )),
    );
}

export function getCanSyncAllLocations() {
  return get('/locations/can_sync_all');
}

export function syncAllLocations() {
  return post('/locations/sync_all');
}

export function addAllergensCharacteristicsToIngredients(targetIds, allergens, characteristics) {
  return post('/products/addAllergensCharacteristics', {
    targetIds, allergens, characteristics,
  });
}

export function removeAllergensCharacteristicsFromIngredients(targetIds, allergens, characteristics) {
  return post('/products/removeAllergensCharacteristics', {
    targetIds, allergens, characteristics,
  });
}

export function getSharedAccounts() {
  return get('/user/shared')
    .then(
      deets => {
        reduxStore.dispatch(UserInvitationActions.received(deets));
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(UserInvitationActions.error(error)),
      )),
    );
}

export function getPendingSharedInvites() {
  return get('/user/share/pending')
    .then(
      deets => {
        reduxStore.dispatch(PendingShareInvites.received(deets))
        return deets;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(PendingShareInvites.error(error)),
      )),
    );
}

export function updatePendingShareInvite(deets) {
  return post('/user/share/pending', deets);
}

export function removePendingShareInvite(id) {
  return del('/user/share/pending/' + id);
}

export function updateSharedAccount(deets) {
  return post('/user/share/update', deets);
}

export function updateBulkSharedAccounts(deets) {
  return post('/user/share/updateBulk', deets).then(getSharedAccounts);
}

export function sendUserInvite(deets) {
  return put('/user/share', deets);
}

export function saveRecipePhoto(id, newPhoto: FormData) {
  return postMultipart(`/products/${id}/photo`, newPhoto).then();
}

export function saveProductSources(id, deets) {
  return post(`/products/${id}/sources`, deets);
}

export function saveRecipeStepPhoto(productId, step, newPhoto: FormData) {
  return postMultipart(`/products/${productId}/steps/${step}/photo`, newPhoto).then();
}

export function getOnboardingInfo() {
  return get('/onboarding')
    .then(
      onboardingInfo => {
        reduxStore.dispatch(OnboardingActions.info(onboardingInfo));
        return onboardingInfo;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(OnboardingActions.infoError(error))
      ))
    );
}

export function resetOnboarding() {
  return post('/onboarding/reset');
}

export function updateOnboardingInfo(deets) {
  return post('/onboarding', deets);
}

export function saveOnboardingImage(id, isLeftImage, newImage: FormData) {
  return postMultipart(`/onboarding/${id}/photo/${isLeftImage}`, newImage).then();
}

export function setStepVisited(pageKey, step) {
  return post('/onboarding/visited', {pageKey, step});
}

export function getPurchaseHistoryGraphData(params: PurchaseHistoryPayload): Promise<GraphData> {
  return get('/history/purchase_orders', params);
}

export function getIngredientPriceHistory(ingredientId: number): Promise<Array<*>> {
  return get(`/products/${ingredientId}/priceHistory`);
}

export function getRecentPriceChanges(date: string) {
  return get(`/products/info/priceChanges?from=${date}`)
      .then(
          recentChanges => {
            reduxStore.dispatch(RecentPriceChangesActions.recentPriceChanges(recentChanges));
            return recentChanges;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(RecentPriceChangesActions.recentPriceChangesError(error)))),
      );
}

export function getPosSetup() {
  return get('/posSetup')
      .then(
          posSetupInfo => {
            reduxStore.dispatch(PosSetupActions.info(posSetupInfo));
            return posSetupInfo;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(PosSetupActions.inforError(error)))),
      );
}

export function getPosImportPreferences() {
  return get('/posImportPreference')
      .then(
          posImportInfo => {
            reduxStore.dispatch(PosImportActions.info(posImportInfo));
            return posImportInfo;
          },
          transformErrorValue(addSideEffect(error =>
              reduxStore.dispatch(PosImportActions.infoError(error)))),
      );
}

export function updatePosImportPreferences(deets) {
  return post('/posImportPreference', deets);
}

export function updatePosSetup(deets) {
  return post('/posSetup', deets);
}

export function getOnboardingWidgetInfo() {
  return get('/onboarding/widgets')
    .then(
      onboardingWidgetsInfo => {
        reduxStore.dispatch(OnboardingWidgetsActions.info(onboardingWidgetsInfo));
        return onboardingWidgetsInfo;
      },
      transformErrorValue(addSideEffect(error =>
        reduxStore.dispatch(OnboardingWidgetsActions.infoError(error)),
      )),
    );
}

export function updateOnboardingWidgetInfo(deets) {
  return put('/onboarding/widgets', deets);
}
export function setOnBoardingWidgetVisited(widgetId) {
  return post('/onboarding/widgets/visited', { widgetId });
}

export function updateItemNumber(id, itemNumber) {
  return put(`/products/${id}/itemNumber`, { itemNumber });
}

export function addImportedSourcesBySupplier(supplierId, importedSources) {
  return post(`/suppliers/${supplierId}/importedSources `, importedSources);
}

function fetchSuppliersInfo(ids: Array<number>) {
  return get('/suppliers/multiple_supplier_info/' + ids.join(','))
    .then(
        data => {
          reduxStore.dispatch(SuppliersActions.multipleInfo(data));
          return data;
        },
        transformErrorValue(addSideEffect(error =>
            reduxStore.dispatch(SuppliersActions.infoError(error)))),
    );
}

export const getMultipleSuppliersDetails = createBatchFetchFunction(fetchSuppliersInfo);

export function importReceivablesPricing(deets) {
  return post('/suppliers/importReceivables', deets);
}

export function importReceipts(deets) {
  return post('/suppliers/importReceipt', deets);
}