import { compose, createStore } from 'redux';
import Immutable from 'immutable';
import rootReducer from './reducers/rootReducer'

var enhancers = [];
if (process.env.NODE_ENV !== 'production' && window.devToolsExtension) {
  enhancers.push(window.devToolsExtension());
}

const initialState = Immutable.Map();

export default createStore(rootReducer, initialState, compose(...enhancers));
