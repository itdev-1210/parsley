// @flow
import base64 from 'base64-js';

// Rotate images by parsing exif data
// Original code: https://gist.github.com/runeb/c11f864cd7ead969a5f0
// TODO: get async/await working, $FlowTODO

const rotation = {
  // string keys will work when numbers are used, because javascript is weird
  '1': 0,
  '3': 180,
  '6': 90,
  '8': 270,
};

export async function readFileAsArrayBUffer(file: Blob): ArrayBuffer {
  const fileReader = new FileReader();
  await new Promise((resolve, reject) => {
    fileReader.onloadend = resolve;
    fileReader.readAsArrayBuffer(file);
  });

  // $FlowFixMe - readAsArrayBuffer ensures correct type of this value
  return fileReader.result;
}

export function arrayBufferToBase64(buffer: ArrayBuffer) {
  let binary = '';
  const bytes = new Uint8Array(buffer);
  for (let i = 0; i < bytes.byteLength; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

// We need this to resize & rotate user-uploaded photos in the correct orientation
// Eg: When users take screenshots via mobile phones then upload as recipe photos
export async function uploadImage(
  file: Blob,
  maxWidth: number = 500,
  maxHeight: number = 500,
): Promise<{ photoUrl: string, processedFile: Blob }> {

  // We resize the image first, to potentially reduce rotation time
  const resizedImage = await resizeImage(file, maxWidth, maxHeight);
  const resizedResult = await readFileAsArrayBUffer(resizedImage);
  const base64img = 'data:' + file.type + ';base64,' + arrayBufferToBase64(resizedResult);

  // Resizing the image effectively erases its exif data, so we need read from the original file here
  // $FlowFixMe
  const scanFile = await readFileAsArrayBUffer(file);
  const scanner = new DataView(scanFile);

  let idx = 0, rotateIdx = 1; // Non-rotated is the default
  if (scanFile.byteLength < 2 || scanner.getUint16(idx) !== 0xFFD8) {
    // Not a JPEG
    return { photoUrl: base64img, processedFile: resizedImage };
  }

  idx += 2;
  let maxBytes = scanner.byteLength;
  let littleEndian = false;
  while (idx < maxBytes - 2) {
    const uint16 = scanner.getUint16(idx, littleEndian);
    idx += 2;
    switch (uint16) {
      case 0xFFE1: // Start of EXIF
        const endianNess = scanner.getUint16(idx + 8);
        // II (0x4949) Indicates Intel format - Little Endian
        // MM (0x4D4D) Indicates Motorola format - Big Endian
        if (endianNess === 0x4949) {
          littleEndian = true;
        }
        const exifLength = scanner.getUint16(idx, littleEndian);
        maxBytes = exifLength - idx;
        idx += 2;
        break;
      case 0x0112: // Orientation tag
        // Read the rotateIdx, its 6 bytes further out
        // See page 102 at the following URL
        // http://www.kodak.com/global/plugins/acrobat/en/service/digCam/exifStandard2.pdf
        rotateIdx = scanner.getUint16(idx + 6, littleEndian);
        maxBytes = 0; // Stop scanning
        break;
      default: break;
    }
  }

  // Don't have to rotate
  if (rotateIdx === 1) {
    return { photoUrl: base64img, processedFile: resizedImage };
  }

  // Rotate the file
  const image = new Image();
  await new Promise((resolve, reject) => {
    image.onload = resolve;
    image.src = base64img;
  });

  const canvas = document.createElement('canvas');
  if (rotateIdx === 3) {
    canvas.width = image.width;
    canvas.height = image.height;
  } else {
    canvas.width = image.height;
    canvas.height = image.width;
  }

  const context = canvas.getContext('2d');
  context.translate(canvas.width / 2, canvas.height / 2);
  context.rotate(rotation[rotateIdx] * Math.PI / 180);
  context.drawImage(image, -image.width / 2, -image.height / 2);

  let rotatedFile = new Blob(); // initializing to satisfy typechecker
  await new Promise((resolve, reject) => {
    canvas.toBlob(b => {
      rotatedFile = b;
      resolve();
    });
  });
  const rotatedResult = await readFileAsArrayBUffer(rotatedFile);

    const rotatedBase64img = 'data:' + rotatedFile.type + ';base64,' + arrayBufferToBase64(rotatedResult);

    return {
      processedFile: rotatedFile,
      photoUrl: rotatedBase64img,
    };
}

export async function resizeImage(
  file: Blob,
  maxWidth: number = 500,
  maxHeight: number = 500,
): Promise<Blob> {

  // $FlowFixMe
  const result = await readFileAsArrayBUffer(file);

  const imageURL = 'data:' + file.type + ';base64,' + arrayBufferToBase64(result);

  const image = new Image();
  await new Promise((resolve, reject) => {
    image.onload = resolve;
    image.src = imageURL;
  });

  let width = image.width, height = image.height;
  const ratio = width / height;
  if (width > maxWidth) {
    width = maxWidth;
    height = maxWidth / ratio;
  }
  if (height > maxWidth) {
    height = maxHeight;
    width = maxHeight * ratio;
  }

  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  canvas.getContext('2d').drawImage(image, 0, 0, width, height);

  let blob = new Blob();
  await new Promise((resolve, reject) => {
    canvas.toBlob(b => {
      blob = b;
      resolve();
    });
  })
  return blob;
}

export function csvQuote(value: string) {
  return `"${value}"`;
}

export function downloadCSVDataAsFile(fileName: string, csvData: string) {
  const encoded = base64.fromByteArray(
    (new TextEncoder()).encode(csvData),
  );

  const a = document.createElement('a');
  a.download = fileName;
  a.type = 'text/csv';
  a.href = `data:text/csv;base64,${encoded}`;
  if (document.body) document.body.appendChild(a);
  a.click();
  if (document.body) document.body.removeChild(a);
}
