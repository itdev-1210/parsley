import _ from 'lodash';
import { deepMap } from './general-algorithms';

/*
Takes object mapping from field names to validator functions, which return truthy error messages if there
is an error otherwise any false-y value

Only good for field-by-field validation; validation rules that require looking at multiple fields need to be
implemented separately.
*/
export function checkFields(values, validators) {
  let errors = {};
  for (let name in validators) {
    let err = validators[name](values[name]);

    if (err) {
      errors[name] = err;
    }
  }
  return errors;
}

// the default for plain functions
function isSimpleValueValidator(validator) {
  return !isArrayValidator(validator) && !isObjectValidator(validator);
}

function isArrayValidator(validator) {
  return validator.arrayValidator;
}

function setArrayValidator(validator) {
  validator.arrayValidator = true;
  return validator;
}

function isObjectValidator(validator) {
  return validator.objectValidator;
}

function setObjectValidator(validator) {
  validator.objectValidator = true;
  return validator;
}

/*
 * returns a validator function
 */
export function deepArrayField(singleValueValidator) {
  return setArrayValidator(values => values.map(
      // optional so that completely-blank items will be dropped
      // this works because of the call to deepOmit() in CrudEditor.submit()
      optional(singleValueValidator)
  ));
}

export function deepObjectField(deepValidators) {
  // this means the validator can be a single function that looks at the whole value!
  return setObjectValidator(_(checkFields)
      .partialRight(deepValidators)
      .unary()
      .value());
}

/*
 * Recursive!
 */
export function isBlank(val) {
  if (Array.isArray(val)) {
    return val.length == 0;
  } else if (val instanceof Blob) {
    return val.size === 0;
  } else if (typeof(val) == typeof({})) {
    return _.every(val, isBlank);
  } else {
    return !_.trim(val);
  }
}

export function optional(nextValidator=_.constant(null)) {
  if (isObjectValidator(nextValidator)) {
    return setObjectValidator(val => isBlank(val) ? deepMap(val, _.constant(null)) : nextValidator(val))
  } else if (isArrayValidator(nextValidator)) {
    // array fields will be empty fields anyway if the user doesn't enter anything, so they'll always work
    return nextValidator;
  } else {
    return val => isBlank(val) ? null : nextValidator(val);
  }
}

export function required(nextValidator=_.constant(null)) {
  if (isSimpleValueValidator(nextValidator)) {
    return val => isBlank(val) ? "Required" : nextValidator(val);
  } else {
    // if object: presumably, *one* of the fields in the whole thing is required
    // if array: length-0 is presumed valid
    return nextValidator;
  }
}

export function number(value) {
  if (isNaN(value)) {
    return "Must be a number";
  }
  return null;
}

export function pricePerValidation(value) {
  if (!['super-package', 'package', 'unit'].includes(value)) {
    return 'Invalid value';
  }
  return null;
}

// for fields where the user enters a numerical literal, not ones where our custom controls select an ID
export function positive(value) {
  return number(value) || (
      value <= 0 ?  "Must be positive" : null);
}

export function nonnegative(value) {
  return number(value) || (
          value < 0 ?  "Cannot be negative" : null);
}

export const quantity = deepObjectField({
  amount: required(positive),
  measure: required(),
  unit: required(),
});

export const portions = deepObjectField({
  amount: required(positive),
});

/*
 don't modify this in users; use spread operator to attach to general validators
*/

export const commonSourceValidators = {
  id: required(),
  measure: required(),
  unit: required(),
  cost: required(nonnegative),
  packaged: optional(),
  packageName: optional(),
  packageSize: required(positive),
  subPackageName: optional(),
  superPackageSize: required(positive),
  sku: optional(),
  pricePer: required(pricePerValidation),
  pricingMeasure: optional(nonnegative),
  pricingUnit: optional(nonnegative),
  supplierIngredientName: optional(),
};

/*
 doesn't include recipe validation, since it's currently not possible to make an entire sub-tree optional
 */
export const commonProductValidators = {
  name: required(),
  description: optional(),
  ingredient: optional(),
  salable: optional(),
  tags: optional(),
  measures: deepArrayField(deepObjectField({
    amount: required(),
    measure: required(),
    preferredUnit: required(),
    conversionMeasure: required(),
    conversionUnit: required(),
    conversion: required(positive)
  })),
  sources: deepArrayField(deepObjectField({
    ...commonSourceValidators,
    preferred: optional(),
    supplier: required(),
  })),
  simplePreps: deepArrayField(deepObjectField({
    outputProduct: optional(positive),
    name: required(),
    advancePrep: required(),
    usesParentMeasures: required(),
    yieldFraction: required(positive),
    yieldMeasure: required(positive)
  }))
  // TODO: preps - no duplicate names
};
