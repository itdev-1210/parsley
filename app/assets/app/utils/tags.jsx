// @flow

export type TagType = 'ingredients' | 'recipes' | 'inventories'

type TagTypeInfo = {
  singularName: string,
  pluralName: string,
};

export const allTagTypes: { [TagType]: TagTypeInfo } = {
  'ingredients': {
    singularName: 'GL Category',
    pluralName: 'GL Categories',
  },
  'recipes': {
    singularName: 'Recipe Category',
    pluralName: 'Recipe Categories',
  },
  'inventories': {
    singularName: 'Ingredient Location',
    pluralName: 'Ingredient Locations',
  },
};