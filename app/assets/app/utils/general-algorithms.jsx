import _ from 'lodash';
import moment from 'moment';

export function deepMap(val, mapper) {
  let deeperMapper = _.chain(deepMap).partialRight(mapper).unary().value();

  if (Array.isArray(val)) {
    return _.map(val, deeperMapper);
  } else if (typeof(val) == typeof({})) {
    return _.mapValues(val, deeperMapper);
  } else {
    return mapper(val);
  }
}

// drops elements from arrays if they match predicate, drops values from objects if they match predicate,
// and APPLIES PREDICATE TO ARRAYS AND OBJECTS
export function deepOmit(val, iteratee) {
  let deeperOmitter = _.chain(deepOmit)
      .partialRight(iteratee)
      .unary()
      .value();

  let predicate = _.iteratee(iteratee); // because I want to be able to not worry about whether I'm using lodash

  if (Array.isArray(val)) {
    return val
        .map(deeperOmitter)
        .filter(_.negate(predicate));
  } else if (val instanceof Blob) {
    return val;
  } else if (val instanceof Object) {
    return _.chain(val)
        .mapValues(deeperOmitter)
        .omitBy(predicate)
        .value();
  } else {
    return val;
  }
}

export function textMatch(value, query) {
  return value.toLowerCase().includes(query.toLowerCase());
}

export const startOfDay = date => moment(date).local().startOf('day');

export const isBeforeToday = date => moment(date).isBefore(startOfDay());

export const isOlderThan = (date, duration) =>
  date.isBefore(moment().subtract(duration));

export const sanitizePrice = (price) => {
  const regexValidation = /\D?(\d+(\.\d+)?)\D*/g;
  const match = regexValidation.exec(price);
  return match && match[1] ? match[1] : price;
};