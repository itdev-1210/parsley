// @flow
import store from '../store';

import { availableSubscriptionLevels, featureDescs } from 'common/utils/features';
import type { FeatureName } from 'common/utils/features';

export function featureIsSupported(f: FeatureName): boolean {
  const info = featureDescs[f];

  if (!info) { // we've removed the feature flags
    return true;
  }

  const availableLevels = availableSubscriptionLevels(f);
  if (!availableLevels) return false;

  const reduxState = store.getState();
  let userSubscriptionLevel = reduxState.getIn(['userInfo', 'subscriptionLevel']);
  if (!userSubscriptionLevel) { // would be a weird case, where subscription redirect hasn't gone through yet
    return false;
  }
  const isAdmin = reduxState.getIn(['userInfo', 'isAdmin']);
  const isStdlib = reduxState.getIn(['userInfo', 'isStdlib']);

  if (
      !isAdmin && !isStdlib &&
      !availableLevels.includes(userSubscriptionLevel) // NOT enabled for this plan
  ) {
    return false;
  }

  return true;
}
