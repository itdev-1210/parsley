// @flow
import Immutable from 'immutable';
import _ from 'lodash';

import CrudEditor from '../ui-components/pages/helpers/CrudEditor';
import type { ReduxFormProps as ValidationProps } from 'common/ui-components/Validate';
import {
  nutrition_allergens, nutrition_characteristics, nutrient_fields,
} from '../ui-components/NutritionDisplay';
import { deepOmit } from './general-algorithms';
import { isBlank } from '../utils/validation-utils';

import type { OrderJSON } from '../webapi/endpoints';
import moment from 'moment';
import { rescaleUnits } from './unit-conversions';
import { getLowestCostSourceId } from './packaging';
import type { Conversions } from './unit-conversions';

type ReduxFormSyntheticEvent<V> = {
  target: {
    value: ?V,
  }
}
export type FormEvent<V> = ?V | ReduxFormSyntheticEvent<V>;
export type FormField<V> = ValidationProps & {
  onChange: (FormEvent<V>) => void,
  onBlur: (FormEvent<V>) => void,
  onFocus: (FormEvent<V>) => void,
  initialValue: ?V,
  value: ?V,
  name: string,
}

export const quantityFormFields = [
  'amount', 'measure', 'unit',
];

const conversionFormFields = [
  'conversion',
  'conversionMeasure',
  'conversionUnit',
  'amount',
  'measure',
  'preferredUnit',
  'customName',
];

const recipeNutrientsFields = [
  'recipe.recipeNutrients.packaged',
  'recipe.recipeNutrients.portionAmount',
  'recipe.recipeNutrients.portionMeasure',
  'recipe.recipeNutrients.portionUnit',
  'recipe.recipeNutrients.portionsPerPackage',
  'recipe.recipeNutrients.servingsPerPackage',
  'recipe.recipeNutrients.servingName',
  'recipe.recipeNutrients.servingAmount',
  'recipe.recipeNutrients.servingMeasure',
  'recipe.recipeNutrients.servingUnit',
];

const nutrientServingFields = [
  'nutrientServingAmount',
  'nutrientServingMeasure',
  'nutrientServingUnit',
];

const nutrientInfoFields = [
  'nutrient',
  'amount',
];

export const allergenFields = [
  'milk',
  'eggs',
  'fish',
  'crustaceanShellfish',
  'treeNuts',
  'wheat',
  'peanuts',
  'soybeans',
  'molluscs',
  'cerealsGluten',
  'celery',
  'mustard',
  'sesameSeeds',
  'sulphurDioxideSulphites',
  'lupin',
];

export const characteristicFields = [
  'addedSugar',
  'meat',
  'pork',
  'corn',
  'poultry',
  'ingredients',
  'nonEdible',
];

export const commonSourceFormFields = [
  'id',
  'measure',
  'unit',
  'cost',
  'packaged',
  'packageName',
  'packageSize',
  'subPackageName',
  'superPackageSize',
  'sku',
  'pricePer',
  'pricingMeasure',
  'pricingUnit',
  'supplierIngredientName',
  'syncUpstream',
  'syncUpstreamOwner',
  'manufacturerName',
  'manufacturerNumber',
];

export const supplierFormFields = [
  'name',
  'accountId',
  'contactName',
  'phone',
  'email',
  'comments',
  'instructions',
  'contacts[].name',
  'contacts[].phone',
  'contacts[].email',
  'contacts[].comments',
  'cost',
  'id',
  'syncUpstream',
  'syncUpstreamOwner',
  ...commonSourceFormFields.map(f => `sources[].${f}`),
  'sources[].product',
];

export const productFormFields = [
  'name',
  'description',
  'photo',
  'ingredientTags',
  'recipeTags',
  'inventoryTags',
  'ingredient',
  'salable',
  'info.lowestPricePreferred',

  ...conversionFormFields.map(f => `measures[].${f}`),

  ...commonSourceFormFields.map(f => `sources[].${f}`),
  'sources[].preferred',
  'sources[].supplier',

  'recipe.price',

  ...quantityFormFields.map(f => `recipe.maxBatch.${f}`),
  ...quantityFormFields.map(f => `recipe.recipeSize.${f}`),
  // if we want to let the user decide an arbitrary quantum, this should be
  // uncommented, and quantum added to most code that works on maxBatch and recipeSize
  // ...quantityFormFields.map(f => `recipe.quantum.${f}`),
  // but for now, it's just boolean
  'recipe.isQuantized',
  'recipe.advancePrep',

  'recipe.batchSize',
  'recipe.batchLabel',

  'recipe.createdAt',
  'recipe.itemNumber',

  'recipe.steps[].description',
  'recipe.steps[].photo',
  'recipe.steps[].ingredients[].ingredient',
  'recipe.steps[].ingredients[].description',
  ...quantityFormFields.map(f => `recipe.steps[].ingredients[].quantity.${f}`),

  'recipe.history[].id',
  'recipe.history[].product',
  'recipe.history[].ownerName',
  'recipe.history[].lastModified',

  'simplePreps[].outputProduct',
  'simplePreps[].name',
  'simplePreps[].advancePrep',
  'simplePreps[].usesParentMeasures',
  'simplePreps[].yieldFraction',
  'simplePreps[].yieldMeasure',
  ...conversionFormFields.map(f => `simplePreps[].measures[].${f}`),

  ...recipeNutrientsFields,
  ...nutrientServingFields,
  ...nutrientInfoFields.map(f => `nutrientInfo[].${f}`),
  ...allergenFields.map(f => `allergens.${f}`),
  ...characteristicFields.map(f => `characteristics.${f}`),

  // not for modification in form; just for preservation of fields received from backend
  'nndbsrId',
  'nndbsrLastImport',
  'syncUpstream',
  'syncUpstreamOwner',
  'lastModified',
  'ownerName',
];

export const posImportFields = [
  'itemCodeHeader',
  'dateSoldHeader',
  'quantitySoldHeader',
  'averagePriceHeader',
  'customDate',
  'nameHeader',
];

export const importReceivablesPricingFields = [
  'supplierNameHeader',
  'dateHeader',
  'customDate',
  'ingredientNameHeader',
  'priceHeader',
  'skuHeader',
  'readerSkuHeader',
  'purchasingPackageHeader',
  'pricingUnitHeader',
];

export const recipePrintFormatOptions = Immutable.fromJS([
  { value: 'step-by-step', label: 'Step by Step' },
  { value: 'ingredients-first', label: 'Ingredients First' },
]);

export function getNNDBSRIdToNutrientIdMap(allNutrients: $FlowTODO) {
  let nndbsrIdToNutrientIdMap = {};
  if (allNutrients) {
    allNutrients.forEach(n => {
      nndbsrIdToNutrientIdMap[n.get('nndbsrId')] = n.get('id');
    });
  }
  return nndbsrIdToNutrientIdMap;
}

export function getEmptyNutrientInfo(allNutrients: $FlowTODO) {
  const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);
  let nutrientInfo = Immutable.List();
  _.each(nutrient_fields, nndbsrId => {
    const nutrientId = nndbsrIdToNutrientIdMap[nndbsrId];
    if (nutrientId != null) {
      nutrientInfo = nutrientInfo.push(Immutable.Map({ nutrient: nutrientId, amount: 0 }));
    }
  });
  return nutrientInfo;
}

export function convertProductFormValuesToJSON(deets: Object): Object {

  // hacky: do not omit blank fields in allergens (if values has allergens)
  const allergens = deepOmit(deets.allergens, x => x == null);
  deets = deepOmit(deets, isBlank);
  if (_.size(allergens) > 0) deets.allergens = allergens;

  // $FlowFixMe: fromJS() has no type support
  let formatted: Immutable.Map<string, $FlowTODO> = Immutable.fromJS(deets);

  const isPreferred = s => s.get('preferred');
  const sources = formatted.get('sources');
  if (sources && !sources.isEmpty()) {
    formatted = formatted.set('purchaseInfo', Immutable.Map(
        [
          ['preferredSource', sources.find(isPreferred)],
          ['otherSources', sources.filter(_.negate(isPreferred)).valueSeq()],
        ],
    ));
  }

  for (let quantityName of ['recipeSize', 'maxBatch']) {
    // redux-form sets the value to undefined instead of deleting it when field is empty, so hasIn
    // doesn't work here
    if (formatted.getIn(['recipe', quantityName, 'amount']) === undefined) {
      formatted = formatted.deleteIn(['recipe', quantityName]);
    }
  }

  if (formatted.getIn(['recipe', 'isQuantized'])) {
    formatted = formatted.setIn(
        ['recipe', 'quantum'],
        formatted.getIn(['recipe', 'recipeSize']),
    );
  }
  formatted = formatted.deleteIn(['recipe', 'isQuantized']);

  formatted = formatted.update('simplePreps', preps => preps && Immutable.Map( // constructor takes iterable of k/v pairs
      preps.map(p => [p.get('name'), p.delete('name')]),
  ));

  // updating all ["recipe", "steps", stepNum, "ingredients", usageNum]
  if (formatted.hasIn(['recipe', 'steps'])) {
    formatted = formatted.updateIn(['recipe', 'steps'], steps => steps.map(s =>
      s.update('ingredients', (ingredientUsages = Immutable.List()) => ingredientUsages
          .map(usage => {
            const quantity = usage.get('quantity');

            if (quantity && quantity.get('amount') && quantity.get('measure') && quantity.get('unit')) {
              // hoist 'quantity' fields up a level
              return usage.merge(usage.get('quantity')).delete('quantity');
            } else {
              // normalize - if any of them are blank we've treated that in the UI as a null quantity
              return usage.delete('quantity');
            }
          })),
    ));
  }

  formatted = formatted.update('nutrientInfo', Immutable.List(), // notSetValue
      allNutrients => allNutrients.map((n: Immutable.Map<string, *>) =>
          n.get('amount') ? n : n.set('amount', 0), // treat blank as zero
      ),
  );

  // Remove uploaded/removed photos
  // String-value photos are existing photos, just keep them as is
  if (typeof formatted.get('photo') !== 'string') {
    formatted = formatted.delete('photo');
  }
  if (formatted.hasIn(['recipe', 'steps'])) {
    // $FlowFixMe: only with records can we assert member existence/value
    formatted = formatted.updateIn(['recipe', 'steps'], steps => steps.map(s =>
      // $FlowFixMe: only with records can we assert member existence/value
      typeof s.get('photo') === 'string' ? s : s.delete('photo'),
    ));
  }

  if (!formatted.get('ingredientTags')) formatted = formatted.set('ingredientTags', []);
  if (!formatted.get('recipeTags')) formatted = formatted.set('recipeTags', []);
  if (!formatted.get('inventoryTags')) formatted = formatted.set('inventoryTags', []);

  if (formatted.get('ingredient') && formatted.hasIn(['recipe', 'itemNumber'])) {
    // $FlowFixMe: only with records can we assert member existence/value
    formatted = formatted.deleteIn(['recipe', 'itemNumber']);
  }

  return formatted.toJS();
}

export function convertProductJSONToFormValues(productDetails: Object, params: Object): ?Immutable.Map<string, $FlowTODO> {
  if (!productDetails || !params.allNutrients) {
    return null;
  }

  // $FlowFixMe: no fromJS() flow support
  let deets: Immutable.Map<string, $FlowTODO> = Immutable.fromJS(productDetails);
  if (CrudEditor.isCopying(params) && deets.get('syncUpstreamOwner')) {
    // want to leave stdlib links intact, but not multi-location ones (so user
    // can get write access to the copies)
    deets = deets.set('syncUpstream', null).set('syncUpstreamOwner', null);
  }

  const purchaseInfo = deets.get('purchaseInfo');

  let sources = Immutable.List();
  if (!CrudEditor.isCopying(params) && purchaseInfo) {
    const primarySupplier = purchaseInfo.getIn(['preferredSource', 'supplier']);
    const preferredSource = purchaseInfo.get('preferredSource');
    sources = purchaseInfo.get('otherSources')
        .sortBy(source => source.get('supplier') == primarySupplier
            // preferred source's supplier comes first
            ? -1
            // sorts by ID, which not super user-friendly, but *groups* by supplier, which is user-friendly
            : source.get('supplier'),
        );
    if (preferredSource) {
      sources = sources.unshift(
          purchaseInfo.get('preferredSource').set('preferred', true),
      );
    } else if (deets.getIn(['info', 'lowestPricePreferred'])) {
      const lowestCostSourceId = getLowestCostSourceId(
        sources,
        deets.get('measures', Immutable.List()),
      );
      sources = sources.map(s =>
        s.get('id') !== lowestCostSourceId ? s : s.set('preferred', true)
      );
    }
  }

  // $FlowFixMe: only with records can we assert member existence/value
  deets = deets.set('sources', sources).delete('purchaseInfo');

  deets = deets.update('simplePreps', preps =>
      preps.map((p, name) => {
        p = p.set('name', name);
        if (CrudEditor.isCopying(params)) {
          p = p.delete('outputProduct');
        }
        return p;
      }).valueSeq().sortBy(p => p.get('name').toLowerCase()),
  );

  // for user convenience/communication, make sure the primary measure is on top
  // $FlowFixMe: only with records can we assert member existence/value
  deets = deets.update('measures', (measures: Conversions) => {
    if (measures.isEmpty()) return measures;

    const primaryMeasureId = measures
        .find(c => c.get('conversionMeasure') == c.get('measure'))
        .get('measure');

    if (measures.first().get('measure') != primaryMeasureId) {
      const primaryIndex = measures.findIndex(c => c.get('measure') == primaryMeasureId);
      return measures
          .delete(primaryIndex)
          .unshift(measures.get(primaryIndex));
    }
    return measures;
  });

  if (deets.hasIn(['recipe', 'steps'])) {
    // $FlowFixMe: only with records can we assert member existence/value
    deets = deets.updateIn(['recipe', 'steps'], steps => steps.map(s =>
        s.update('ingredients', ingredientUsages => ingredientUsages.map(usage => {
          const quantity = _.pick(usage.toJS(), ['amount', 'unit', 'measure']);
          return usage.delete('amount').delete('unit').delete('measure')
              .set('quantity', Immutable.fromJS(quantity));
        }))));
  }

  // we don't support arbitrary quanta; just those that are the same as recipeSize
  if (deets.has('recipe')) {
    // $FlowFixMe: only with records can we assert member existence/value
    deets = deets.setIn(['recipe', 'isQuantized'], deets.hasIn(['recipe', 'quantum']));
    deets = deets.deleteIn(['recipe', 'quantum']);

    if (!deets.hasIn(['recipe', 'batchLabel'])) {
      deets = deets.setIn(['recipe', 'batchLabel'], 'batches');
    }
  }


  // Fill in default allergen fields if there's no existing data
  nutrition_allergens.forEach(allergen => {
    const fieldName = allergen.fieldName || allergen.name;
    if (!deets.hasIn(['allergens', fieldName])) {
      deets = deets.setIn(['allergens', fieldName], allergen.additionalInfo ? null : false);
    }
  });

  // Fill in default characteristic fields if there's no exsited data
  nutrition_characteristics.forEach(characteristic => {
    const fieldName = characteristic.fieldName || characteristic.name;
    if (!deets.hasIn(['characteristics', fieldName])) {
      const defaultValue = fieldName === 'ingredients' ? '' : false;
      deets = deets.setIn(['characteristics', fieldName], defaultValue);
    }
  });

  if (params.allNutrients) {

    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(params.allNutrients);
    // $FlowFixMe: only with records can we assert member existence/value
    let nutrientInfo: Immutable.List<$FlowTODO> = deets.get('nutrientInfo');

    _.each(nutrient_fields, nndbsrId => {
      const nutrientId = nndbsrIdToNutrientIdMap[nndbsrId];
      const existedNutrient = nutrientInfo.find(n => n && n.get('nutrient') === nutrientId);
      if (nutrientId != null && !existedNutrient) {
        nutrientInfo = nutrientInfo.push(Immutable.Map({ nutrient: nutrientId, amount: 0 }));
      }
    });

    deets = deets.set('nutrientInfo', nutrientInfo);
  }

  // Default values for nutrient serving size
  if (deets.has('recipe')) {
    // Sub-recipe: Default to recipe size
    if (deets.get('ingredient')) {
      if (!deets.get('nutrientServingAmount')) {
        // $FlowFixMe: only with records can we assert member existence/value
        deets = deets.set('nutrientServingAmount', deets.getIn(['recipe', 'recipeSize', 'amount']));
      }
      if (!deets.get('nutrientServingUnit')) {
        // $FlowFixMe: only with records can we assert member existence/value
        deets = deets.set('nutrientServingMeasure', deets.getIn(['recipe', 'recipeSize', 'measure']));
        // $FlowFixMe: only with records can we assert member existence/value
        deets = deets.set('nutrientServingUnit', deets.getIn(['recipe', 'recipeSize', 'unit']));
      }
    // Normal Recipe: Default to 1 portion
    } else if (!deets.get('nutrientServingAmount')) {
      deets = deets.set('nutrientServingAmount', 1);
    }
  }

  return deets;
}

export function convertSupplierJSONToFormValues(
    supplierDetails: Object, params: Object,
): Immutable.Map<string, $FlowTODO> {
  const ingredientIdToNameMap = Immutable.Map(
      (params.allIngredients || [])
          .map(i => [i.get('id'), i.get('name')]),
  );

  // $FlowFixMe: no fromJS flow support
  let deets: Immutable.Map<string, $FlowTODO> = Immutable.fromJS(supplierDetails);
  deets = deets.update('sources', sources => sources.sort((a, b) => {
    const [aName, bName] = [a, b].map(s => ingredientIdToNameMap.get(s.get('product')));
    if (!bName) {
      return -1;
    } else if (!aName) {
      return 1;
    } else {
      return aName.localeCompare(bName);
    }
  }));
  return deets;
}

// changes a deep object field that does not contain any object or array fields into an plain object of values
export function shallowToObject(fields: {[string]: FormField<any>}): {[string]: any} {
  return _.mapValues(fields, 'value');
}

// sometimes you get an event, sometimes you just get the value. so actually the title of this function lies
export function valueFromEvent<V>(ev: FormEvent<V>): V {
  return ev && ev.target && ev.target.value
      ? ev.target.value
      // $FlowFixMe flow somehow thinks a ReduxFormSyntheticEvent will get past conditional
      : ev;
}
export function convertOrderFormValuesToJSON(deets: Object): OrderJSON {
  // existing realization have 2 different saveObject with different structure.

  let {
    name, description, coversCount,
    sections, items, usedMenu, subtractFromInventory,
  } = deets;
  const saveOrder = { name, description, coversCount, subtractFromInventory };

  usedMenu = { // orders are always custom
    name: 'Custom Order',
    sections,
  };
  // copy items from sections
  items = _.flatMap(sections || [],
      i => i.items || [],
  );

  let transaction = {
    isFinalized: true, // for now, no support for forecast/final distinction
    // have to be in utc mode to prevent timezone conversion when the date is parsed in backend
    // see PARS-2374 & PARS-2362
    time: moment.utc(deets.date).format(),
    includeInCalculatedInventory: subtractFromInventory,
    deltas: undefined,
  };
  transaction.deltas = items
      .filter(d => d.orderedQuantity && d.orderedQuantity.amount)
      // The API requires product IDs and ordered amount to be integers, not strings,
      // and form controls tend to set these to strings
      .map(i => _.update(i, ['orderedQuantity', 'amount'], parseInt))
      // rename orderedQuantity to quantity
      .map(i => ({ ..._.omit(i, 'orderedQuantity'), quantity: i.orderedQuantity }));

  return deepOmit({ ...saveOrder, transaction, usedMenu, items }, isBlank);
}

export function convertMenuJSONToFormValues(menu: Object, params: Object) {
  if (!menu) {
    return null;
  }

  // $FlowFixMe: no fromJS flow support
  return Immutable.fromJS(menu).delete('id');
}

export function convertOrderJSONToFormValues(order: Object, params: Object) {
  if (!order) {
    return null;
  }
  const formValues = _.pick(order, 'name', 'description', 'usedMenu', 'coversCount');
  formValues.subtractFromInventory = order.transaction.includeInCalculatedInventory;
  if (!CrudEditor.isCopying(params)) {
    // otherwise you won't be able to edit the new order if the original was in the past
    formValues.date = moment(order.transaction.time).local().format();
  }

  let customMenu;
  if (order.usedMenu && typeof order.usedMenu === 'object') {
    customMenu = order.usedMenu;
    formValues.usedMenu = 'custom_menu';
  }

  if (customMenu) {
    formValues.sections = _.cloneDeep(customMenu.sections);
    formValues.sections.forEach(section => {
          section.items.forEach(item => {
            const matchingDelta = order.transaction.deltas.find(d => d.product === item.product);
            if (matchingDelta) {
              item.orderedQuantity = { ...matchingDelta.quantity };
              item.cashFlow = matchingDelta.cashFlow;
            } else {
              item.orderedQuantity = { amount: 0 };
            }
          });
        },
    );
  }

  return Immutable.fromJS(formValues);
}

export const onboardingStepFields = [
  'id',
  'page',
  'index',
  'placement',
  'targetSelector',
  'name',
  'title',
  'description',
  'leftImage',
  'rightImage',
  'nextButtonText',
  'backButtonText',
  'bodyWidth',
  'titleBgColor',
  'descBgColor',
  'borderColor',
  'nextBgColor',
  'closeBtnColor',
];

export const onboardingBulkEditFields = [
  'visitedBoxes[].userId',
  'visitedBoxes[].page',
  'visitedBoxes[].nextStep',

  'pages[].key',
  'pages[].name',

  ...onboardingStepFields.map(f => `steps[].${f}`),
];

export function convertFormValuesToOnboardingJson(onboardingInfo: Immutable.Map<*, *>, params: Object) {

  if (params.id !== -1) {

    // $FlowFixMe: only with records can we assert member existence/value
    const onboardingSteps: Immutable.List<$FlowTODO> = onboardingInfo.get('steps');
    const updatedStep = onboardingSteps.find(s => s.get('id') === params.id);
    // step's page is updated
    if (updatedStep.get('page') !== params.page) {
      // rearrange step index of other steps in original page group
      return onboardingInfo.set('steps', onboardingSteps.map(s => {
        if (s.get('id') === params.id) {
          return Immutable.fromJS(params);
        } else if (s.get('page') === updatedStep.get('page') && s.get('index') > updatedStep.get('index')) {
          return s.set('index', s.get('index') - 1);
        }

        return s;
      }));
    }

    return onboardingInfo.set('steps', onboardingSteps.map(s => s.get('id') === params.id ? Immutable.fromJS(params) : s));
  }

  return onboardingInfo.update('steps', steps => steps.push(Immutable.fromJS({ ...params })));
}

export function convertPurchaseOrderFormValuesToJSON(deets: Object, orderType: string) {
  let purchaseOrder = {};
  if (deets.size === 0) {
    return purchaseOrder;
  }
  purchaseOrder.supplier = deets.get('supplierId');

  if (orderType === 'print') {
    purchaseOrder.printedAt = moment().format();
  } else if (orderType === 'email') {
    purchaseOrder.emailedAt = moment().format();
  } else {
    return console.error('cannot convert purchaseOrder with orderType: ', orderType);
  }

  purchaseOrder.transaction = {
    isFinalized: false,
    time: moment().format(),
    xactType: 'purchase-order',
    deltas: deets.toJS().deltas.map(item => ({
      product: item.product.value,
      quantity: {
        amount: item.totalPurchased.amount.value,
        measure: item.totalPurchased.measure.value,
        unit: item.totalPurchased.unit.value,
      },
      productSource: item.selectedSource.value,
    })),
  };

  purchaseOrder.orders = deets.get('orders').toJS();

  return purchaseOrder;
}

// not *quite* accurate -
export function convertShoppingListJSONToFormValues(shoppingList: Immutable.Map<string, $FlowTODO>) {

  return shoppingList.map(item => item
      .filter((value, key) => ['product', 'totalPurchased'].includes(key))
      .set('selectedSource', item.getIn(['source', 'id']))
      .set('userAdded', false)
      .set('supplierId', item.getIn(['source', 'supplier'], 'unknown'))
      .update('totalPurchased', q => q.get('unit') ? Immutable.fromJS(rescaleUnits(q.toJS())) : q)
      // can be negative when subtracting inventory; still want to preserve plusIndeterminate flag
      .update('totalPurchased', q => q.get('amount') < 0 ? q.set('amount', 0).delete('unit').delete('measure') : q),
  ).toJS();
}
