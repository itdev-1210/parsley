// @flow
import Immutable from 'immutable';
import _ from 'lodash';

class ImmutableCache {
  base = Immutable.Map().asMutable();;

  clear = this.base.clear.bind(this.base);
  delete = this.base.delete.bind(this.base);
  get = this.base.get.bind(this.base);
  set = this.base.set.bind(this.base);
  has = this.base.has.bind(this.base);
}

// gives us a map that follows ES Map() API, but can have Immutable collections as keys
_.memoize.Cache = ImmutableCache;

export function memoize<Arg, Result>(baseFun: Arg => Result): Arg => Result {
  return _.memoize(baseFun, Immutable.fromJS);
}
