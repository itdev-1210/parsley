// @flow
import _ from 'lodash';
import Immutable from 'immutable';
import formatNum from 'format-num';

import store from '../store';

type Conversion = Immutable.Map<string, $FlowTODO> // placeholder, but should at least get us nominal typechecking
export type Conversions = Immutable.List<Conversion>

export type MeasureSystem = 'metric' | 'imperial';

export type Measure = $FlowTODO
export type Measures = Immutable.Map<number, Measure> // placeholder - not used here because we just go straight to the store
export type Unit = $FlowTODO

export type Quantity = {
  amount: number,
  unit: number,
  measure: number,
};

/** ******************************************************
 * the functions in this file should only be called after a getMeasures() API call
 * has succeeded
 ********************************************************/

function getAllUnits(): Immutable.List<Unit> {
  return store.getState().getIn(['measures', 'units']);
}

function getUnit(unitId) {
  return store.getState().getIn(['measures', 'units', unitId]);
}

// TODO: find a better way to deal with rounding error. see commit message

// copied from http://blog.magnetiq.com/post/497605344/rounding-to-a-certain-significant-figures-in
function sigFigs(n: number, sig: number) {
  const mult = Math.pow(10,
      sig - Math.floor(Math.log(n) / Math.LN10) - 1);
  return Math.round(n * mult) / mult;
}

// round up to a specified number of digits after the decimal point, and preserve positive values at minimum value
export function purchasingRound(n: number, figs: number, quantum: number) {
  if (n < quantum) {
    return quantum; // Don't round down to zero
  }

  return Number(formatNum(n, { maxFraction: figs }).replace(/,/g, ''));
}

export function conversionRound(conv: number) {
  return sigFigs(conv, 5);
}

export function quantityRound(quantity: number) { // misleading name WRT type of param and return
  return sigFigs(quantity, 3);
}

export function roundToNearest(value: number, nearest: number) {
  const inv = 1.0 / nearest;
  return Math.round(value * inv) / inv;
}

/*
* Get measure id from unitId
*/
export function getMeasure(unitId: number): number {
  return getUnit(unitId).getIn(['measure', 'id']);
}
// truncates but, unlike toFixed, doesn't leave garbage at the end of the number
export function truncated(amount: number, fractionalDigits: number) {
  const roundingFactor = Math.pow(10, fractionalDigits);
  return Math.round(amount * roundingFactor) / roundingFactor;
}

export function currencyRound(currency: number) {
  return truncated(currency, 2);
}

/**
 * DO NOT USE DIRECTLY - this does unintuitive things in the case where no valid primary measure can be found
 * @param conversions
 * @returns {null}
 */
function getPrimaryMeasureConversion(conversions): ?Conversion {
  return conversions.find(
      c => c.get('measure') && c.get('conversionMeasure') == c.get('measure'),
  ) || Immutable.Map();
}

export function getPrimaryMeasureId(conversions: Conversions): ?number {
  const primaryMeasureConversion = getPrimaryMeasureConversion(conversions);
  return primaryMeasureConversion && primaryMeasureConversion.get('measure');
}

export function getPrimaryUnitId(conversions: Conversions) {
  const primaryMeasureConversion = getPrimaryMeasureConversion(conversions);
  return primaryMeasureConversion && primaryMeasureConversion.get('preferredUnit');
}

/*
 * Return unit in terms of primary measures smallest unit
 * if primary == volume will return unitId in milliliters
 *
 * should match RecipeUtils.scala:Conversions.getSizeInPrimary
 */
function getSizeInPrimary(unitId, conversions: Conversions) {
  const unit = getUnit(unitId);
  const thisConversion = conversions.find(
      c => c.get('measure') == unit.getIn(['measure', 'id']),
  );

  if (!thisConversion) {
    return 0;
  }

  const primaryConversion = conversions.find(
    c => c.get('measure') == c.get('conversionMeasure'),
  );

  const preferredUnitSize = getUnit(thisConversion.get('preferredUnit')).get('size');

  let conversionUnitSize;
  // measure definition is not in terms of primary
  if (thisConversion.get('conversionMeasure') != primaryConversion.get('measure')) {

    // test for two-node cycle in dependency graph
    // if all is well, nextConversion.measure = thisConversion.getConversion => thisConversion.measure != nextConversion.conversionMeasure
    const nextConversion = conversions.find(
      c => c.get('measure') == thisConversion.get('conversionMeasure'),
    );

    if (nextConversion.get('conversionMeasure') == thisConversion.get('measure')) {
      console.error(`circular measure dependency - ${nextConversion.get('measure') || ''} and ${thisConversion.get('measure') || ''} are defined in terms of each other`);
      console.error('full conversions:', thisConversion.toJS(), conversions.toJS());
      return 0;
    }
    conversionUnitSize = getSizeInPrimary(thisConversion.get('conversionUnit'), conversions);
  } else {
    conversionUnitSize = getUnit(thisConversion.get('conversionUnit')).get('size');
  }

  return unit.get('size') / preferredUnitSize * conversionUnitSize /
      // $FlowFixMe: only with records can we assert member existence/value
      thisConversion.get('amount') *
      // $FlowFixMe: only with records can we assert member existence/value
      thisConversion.get('conversion');
}

export function convert(
    sourceAmount: number, sourceUnitId: number,
    destUnitId: number,
    conversions: Conversions,
): number {
  const sourceInPrimary = getSizeInPrimary(sourceUnitId, conversions);
  const destInPrimary = getSizeInPrimary(destUnitId, conversions);
  if (sourceInPrimary <= 0 || destInPrimary <= 0) {
    return 0;
  }
  return sourceAmount * sourceInPrimary / destInPrimary;
}

export function divideQuantities(numerator: Quantity, denominator: Quantity, conversions: Conversions) {
  if (numerator.amount == 0) { // handles indeterminate-quantity case
    return 0;
  }
  const [numeratorScalar, denominatorScalar] = [numerator, denominator].map(quantity =>
    quantity.amount * getSizeInPrimary(quantity.unit, conversions),
  );
  return numeratorScalar / denominatorScalar;
}

/*
 * takes a value per (amount, unit) tuple (e.g. cost per
 * 2kg bag, or calories per 8 oz. serving) into a different (amount, unit) tuple
 * TODO: Modify to use Quantity type for source and destination
 *
 * Should match with RecipeUtils.scala:convertPerQuantityValue
 */
export function convertPerQuantityValue(val: number,
                                    sourceAmount: number, sourceUnitId: number,
                                    destAmount: number, destUnitId: number,
                                    conversions: Conversions) {
  const [sourceUnitSize, destUnitSize] = [sourceUnitId, destUnitId].map(u =>
      getSizeInPrimary(u, conversions));

  const [sourcePackageSize, destPackageSize] = _.zipWith(
      [sourceAmount, destAmount],
      [sourceUnitSize, destUnitSize],
      (q, s) => q * s);

  return val / sourcePackageSize * destPackageSize;
}

export function unitDisplayName(unitId: number, conversions: ?Conversions, abbr: boolean = true) {
  const unit = getUnit(unitId);
  const measure = unit.get('measure');
  // in the abbr == true case, we want to get the name anyway.
  // In the abbr == false case, this || is a no-op
  const defaultName = unit.get(abbr ? 'abbr' : 'name') || unit.get('name');
  const isMeasureBaseUnit = unit.get('size') == 1; // only replace name for "each", not "dozen" or other things
  const thisMeasureConversion = conversions && conversions.find(c => c.get('measure') == measure.get('id'));

  let customName = measure.get('allowsCustomNames') && isMeasureBaseUnit
      ? thisMeasureConversion && thisMeasureConversion.get('customName')
      : null;
  // hacky - custom names are usually not suited to being put after their quantity without space in between
  // HTML is nice to us and will collapse multiple spaces into one
  // otherwise, callers would have to know whether the string they got back was a custom name - even worse
  customName = customName && ' ' + customName;

  return customName || defaultName;
}

export function unitAltNames(unitId: number, conversions: ?Conversions, displayIsAbbr: boolean = true) {
  const maybeCustomAltName = unitDisplayName(unitId, conversions, !displayIsAbbr);
  const unit = getUnit(unitId);
  const defaultAltName = unit.get(displayIsAbbr ? 'name' : 'abbr') || unit.get('name');
  if (defaultAltName !== maybeCustomAltName) {
    // if it's custom, also include the default as an alt
    return [defaultAltName, maybeCustomAltName];
  } else {
    return maybeCustomAltName; // it's the default
  }
}

export function unitDisplayPrecision(unitId: number) {
  if (unitId) {
    const unit = getUnit(unitId);
    return unit.get('precision');
  }
  return 2;
}

export function unitDisplayQuantum(unitId: number): number {
  if (unitId) {
    const unit = getUnit(unitId);
    return unit.get('quantum');
  }
  return 1;
}

/*
 * Picks the most appropriately-scaled human-readable unit for a quantity,
 * potentially converting to a different unit
 */
export function rescaleUnits(originalQuantity: Quantity, isMetric?: boolean, commonPurchaseUnit?: boolean): Quantity {
  const newQuantity = {
      ...originalQuantity,
  };

  const originalUnit = getUnit(originalQuantity.unit);
  const metric = typeof isMetric === 'boolean' ? isMetric : originalUnit.get('metric');

  const scalableUnits = getAllUnits().filter((u) =>
        u.getIn(['measure', 'id']) === originalUnit.getIn(['measure', 'id'])
        && u.get('metric') === metric,
      );

  let availableUnits;
  if (typeof commonPurchaseUnit === 'boolean') {
    // NB: this will do weird things in the case where there's more than one commonPurchaseUnit
    // for a (measure, isMetric) tuple. if we get into that situation, revisit this.
    availableUnits = scalableUnits.filter(u => u.get('commonPurchaseUnit') === commonPurchaseUnit);
  } else {
    availableUnits = scalableUnits.filter((u) => {
      const amountNewUnit = originalQuantity.amount * originalUnit.get('size') / u.get('size');
      const upperLimit = u.get('scaleUpLimit') || amountNewUnit;
      const lowerLimit = u.get('scaleDownLimit') || -1;
      return (amountNewUnit <= upperLimit)
        && (amountNewUnit >= lowerLimit);
    });
  }

  let newUnit;
  if (
      availableUnits.isEmpty() ||
      availableUnits.map(u => u.get('id')).includes(originalUnit.get('id'))
  ) {
    newUnit = originalUnit;
  } else {
    newUnit = availableUnits.sortBy(u => u.get('size')).first();
  }
  newQuantity.unit = newUnit.get('id');

  newQuantity.amount = originalQuantity.amount * originalUnit.get('size') / newUnit.get('size');
  return newQuantity;
}

/*
 * Picks the most appropriately-scaled unit in the given measure for showing e.g. cost per unit, potentially
 * converting to a different unit
 */
type PerQuantityValue = Quantity & { val: number }
export function pickPerQuantityValueDisplayUnit(
    { measure: displayMeasure, unit: preferredUnit }: any,
    perQuantityValues: Array<PerQuantityValue>, conversions: Conversions) {

  const validUnits = displayMeasure.get('units') // NB the sorting
      .filter((unit) => unit.get('metric') == preferredUnit.get('metric'))
      .sortBy(unit => unit.get('size'));

  const unitIsBigEnough = candidateUnit => perQuantityValues.every(({ val, amount, unit: u }) =>
    convertPerQuantityValue(val, amount, u, 1, candidateUnit.get('id'), conversions) > 0.10,
  );

  if (unitIsBigEnough(preferredUnit)) return preferredUnit;

  // we'd like units with at least two significant figures after the truncation to two fractional digits
  const bigEnoughUnits = validUnits.filter(unitIsBigEnough);
  if (bigEnoughUnits.isEmpty()) {
    return preferredUnit; // but if we can't get it, we'll take the biggest we can
  } else {
    // of the accurate enough units, pick the smallest one to minimize trailing zeros
    return bigEnoughUnits.first();
  }
}

// Get the common purchase unit for a (measure, isMetric)
export function getCommonPurchaseUnits(measure: number, isMetric: boolean): Immutable.Seq.Indexed<Unit> {
  return getAllUnits().filter(u =>
    u.getIn(['measure', 'id']) === measure &&
    u.get('metric') === isMetric &&
    u.get('commonPurchaseUnit'),
  ).valueSeq();
}
