// @flow
import Immutable from 'immutable';
import _ from 'lodash';

import type { Quantity, Conversions } from './unit-conversions';
import type { BaseOrder } from '../reducers/productionOrder';

import { convert, divideQuantities } from './unit-conversions';
import { quantityFormFields } from './form-utils';
import type { ProductInfo } from '../ui-components/utils/PropTypes';

type RecipeIngredient = {
  ingredient: number,
  quantity: Quantity,
};

export const getRecipeBaseOrder = (recipe: ProductInfo, requiredQuantity: Quantity, isConvertedFromRecipeEditForm: boolean = false) => {
  const _recipe = _(recipe);
  const recipeSize = _recipe.get(['recipe', 'recipeSize']);
  const recipeIngredients = _recipe.get(['recipe', 'steps']).flatMap(step =>
      step.ingredients.map(ing =>
          ({
            ingredient: ing.ingredient,
            quantity: isConvertedFromRecipeEditForm ? ing.quantity : _.pick(ing, quantityFormFields),
          }),
      ),
  ).filter(ing => ing.quantity.amount); // truthiness is what we want - not "", 0, or null
  // $FlowFixMe: no fromJS() typing
  const conversions: Conversions = Immutable.fromJS(_recipe.get('measures'));

  return recipeToBaseOrder(
      recipeSize,
      requiredQuantity,
      recipeIngredients,
      isConvertedFromRecipeEditForm
          ? (_recipe.get(['isQuantized']) ? _.recipe.get(['recipe', 'recipeSize']) : null)
          : _recipe.get(['recipe', 'quantum']),
      conversions,
  );
};

export function getRecipeProductionQuantity(
    requiredSize: Quantity, quantum: ?Quantity,
    conversions: Conversions,
): Quantity {
  if (quantum) {
    let requiredToQuantumRatio = requiredSize.unit
        ? divideQuantities(requiredSize, quantum, conversions)
        : requiredSize.amount / quantum.amount;
    // since recipe can be produced in quantum so adjust amount to multiple of recipe size
    const numQuanta = Math.ceil(requiredToQuantumRatio);
    const productionQuantity = {
      ...quantum,
      amount: quantum.amount * numQuanta,
    };

    return productionQuantity.unit ?
        {
          amount: convert( // keep quantity in the unit the user specified
              productionQuantity.amount, productionQuantity.unit,
              requiredSize.unit,
              conversions,
          ),
          unit: requiredSize.unit,
          measure: requiredSize.measure,
        } : productionQuantity;
  } else {
    return requiredSize;
  }
}

export const recipeToBaseOrder = (
    recipeSize: Quantity,
    requiredSize: Quantity,
    recipeIngredients: Array<RecipeIngredient>,
    quantum: ?Quantity,
    conversions: Conversions,
): BaseOrder => {
  const correctedSize = getRecipeProductionQuantity(requiredSize, quantum, conversions);

  const ingMultiplier = correctedSize.unit
      ? divideQuantities(correctedSize, recipeSize, conversions)
      : correctedSize.amount / recipeSize.amount;

  return reduceRecipeIngredients(recipeIngredients, ingMultiplier);
};

export const reduceRecipeIngredients = (
    recipeIngredients: Array<RecipeIngredient>,
    ingMultiplier: number,
// $FlowFixMe: no fromJS() typing
): BaseOrder => Immutable.fromJS(recipeIngredients.map(prevIng => ({
  product: prevIng.ingredient,
  quantity: {
    ...prevIng.quantity,
    amount: prevIng.quantity.amount * ingMultiplier,
  },
})));