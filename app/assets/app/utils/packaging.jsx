// @flow
import _ from 'lodash';
import type { List, Map } from 'immutable';
import Immutable from 'immutable';
import type { SelectOption } from '../ui-components/Select';

import {
  convertPerQuantityValue,
  getCommonPurchaseUnits,
  getPrimaryMeasureId, getPrimaryUnitId,
  pickPerQuantityValueDisplayUnit,
  unitDisplayName,
} from './unit-conversions';
import type {
  Conversions,
  Measures,
  MeasureSystem,
  Quantity,
  Unit,
} from './unit-conversions';
import type { ProductSource } from '../ui-components/utils/PropTypes';

type PackageDesc = {
  displayName: string,
  alternateName?: string,
  plural?: string,
  canBeSubPackage?: boolean,
  canBeSuperPackage?: boolean,
}
const packageDescDefaults = {
  canBeSubPackage: true,
  canBeSuperPackage: false,
};
// mapping from internal (in-DB) name to name to use in selects
export const builtinPackages: { [packageId: string]: PackageDesc } = {
  bag: {
    ...packageDescDefaults,
    displayName: 'bag',
  },
  bottle: {
    ...packageDescDefaults,
    displayName: 'btl',
    alternateName: 'bottle',
  },
  case: {
    ...packageDescDefaults,
    displayName: 'case',
    alternateName: 'cs',
    canBeSuperPackage: true,
    canBeSubPackage: false,
  },
  halfCase: {
    ...packageDescDefaults,
    displayName: 'half-cs',
    alternateName: 'half case',
    plural: 'half-cses',
    canBeSuperPackage: true,
    canBeSubPackage: false,
  },
  flat: {
    ...packageDescDefaults,
    displayName: 'flat',
    canBeSuperPackage: true,
    canBeSubPackage: false,
  },
  jar: {
    ...packageDescDefaults,
    displayName: 'jar',
  },
  can: {
    ...packageDescDefaults,
    displayName: 'can',
  },
  package: {
    ...packageDescDefaults,
    displayName: 'pkg',
    alternateName: 'package',
    canBeSuperPackage: true,
  },
  wheel: {
    ...packageDescDefaults,
    displayName: 'wheel',
  },
  box: {
    ...packageDescDefaults,
    displayName: 'box',
    plural: 'boxes',
    canBeSuperPackage: true,
  },
};

export const nestedPackages = Object.getOwnPropertyNames(builtinPackages)
    .filter(pack => builtinPackages[pack].canBeSubPackage)
    .map<SelectOption>(pack => {
      const packageInfo = builtinPackages[pack];
      return {
        value: pack,
        label: `${packageInfo.plural || (packageInfo.displayName + 's')} of`,
        altName: packageInfo.alternateName,
      };
    });

export const newSourceTemplate: $Shape<ProductSource> = {
  packaged: false,
  packageSize: 1,
  subPackageName: '',
  superPackageSize: 1,
  pricePer: 'unit',
};

// should match with RecipeUtils.scala:getCostingSize
export function costingSize(sourceInfo: any): Quantity {
  // $FlowFixMe - scary error case
  if (!sourceInfo) return null;

  const {
    pricePer, superPackageSize, packageSize, measure, unit, pricingMeasure, pricingUnit,
  } = sourceInfo.toJS();
  switch (pricePer) {
    case 'super-package':
      return {
        amount: packageSize * superPackageSize,
        measure, unit,
      };
    case 'package':
      return {
        amount: packageSize,
        measure, unit,
      };
    case 'unit':
      return {
        amount: 1,
        measure: pricingMeasure ? pricingMeasure : measure,
        unit: pricingUnit ? pricingUnit : unit,
      };
    default:
      // $FlowFixMe bad pricePer value
      return null;
  }
}

export function packageInfoFromTxn(txnDel: any): ?ProductSource {
  if (!txnDel) return null;
  if (!txnDel.get('sourceMeasure')) return null;

  return txnDel.filter((v, k) =>
    [
      'packaged', 'pricePer',
      'pricingMeasure', 'pricingUnit',
      'packageName', 'subPackageName',
      'packageSize', 'superPackageSize',
    ].includes(k),
  )
  .set('measure', txnDel.get('sourceMeasure'))
  .set('unit', txnDel.get('sourceUnit'));
}

// should match with RecipeUtils#effectivePackageSize
export function effectivePackageSize(sourceInfo: any): Quantity {
  // $FlowFixMe - scary error case
  if (!sourceInfo) return null;

  return {
    amount: sourceInfo.get('packageSize') * sourceInfo.get('superPackageSize'),
    unit: sourceInfo.get('unit'),
    measure: sourceInfo.get('measure'),
  };
}

/** takes "sources", which may be actual source objects or synthetic ones for in-house
 * they contain fields: preferred, unit, measure, packageSize, superPackageSize, cost
 */
export function pickComparisonUnit(
    sources: List<any>,
    conversions: Conversions,
    measures: Measures,
    userWeightPref?: string,
    userVolumePref?: string,
    commonPurchaseUnit?: boolean,

): ?Unit {

  const preferredSource = sources.find(s => s.get('preferred'));

  const comparisonMeasureId = (preferredSource && preferredSource.get('measure'))
      || getPrimaryMeasureId(conversions);
  if (!comparisonMeasureId) {
    return undefined;
  }

  const comparisonMeasure = measures.get(
      comparisonMeasureId,
  );
  if (!comparisonMeasure) return undefined;

  let preferredUnit;
  if (commonPurchaseUnit && userVolumePref && userWeightPref) {
    let measure = comparisonMeasure.get('id');

    let userPurchaseSystem = 'metric';
    if (measure === 2) userPurchaseSystem = userWeightPref;
    if (measure === 3) userPurchaseSystem = userVolumePref;
    preferredUnit = getCommonPurchaseUnits(measure, userPurchaseSystem === 'metric').first();
  } else {
    preferredUnit = comparisonMeasure.getIn([
        'units',
        preferredSource ? preferredSource.get('unit') : getPrimaryUnitId(conversions),
    ]);
  }

  if (!preferredUnit) return undefined;

  const completedSources = sources.toArray().filter(source =>
      typeof source.get('unit') == 'number' && source.get('superPackageSize') && source.get('packageSize') && !isNaN(source.get('cost')),
  ).map(source => ({
    ...effectivePackageSize(source),
    val: source.get('cost'),
  }));

  return pickPerQuantityValueDisplayUnit(
      { measure: comparisonMeasure, unit: preferredUnit },
      completedSources,
      conversions,
  );
}

/** takes "sources", of the same format as pickComparisonUnits, but with morewhich may be actual source objects or synthetic ones for in-house
 * they contain fields: preferred, unit, measure, packageSize, superPackageSize, cost
 */
export function pickAllComparisonUnits(
    sources: Array<ProductSource>,
    productConversions: Map<number, Conversions>,
    measures: Measures,
    userWeightPref?: string,
    userVolumePref?: string,
    commonPurchaseUnit?: boolean,
): { [number]: Unit } {

  // Calculate comparisonUnit for each product by
  // first group all sources of a same ingredient
  const sourcesByProducts = _.groupBy(sources, 'product');

  return _.mapValues(sourcesByProducts, (productSources, id) => {
    const productId = parseInt(id);
    const conversions = productConversions.get(productId);
    if (!conversions) return undefined; // still loading

    // set the one with the largest id as the preferred
    const sortedSources = _.sortBy(productSources,
            // sort newly-created sources last
            s => s.id < 0 ? Infinity : s.id,
    );
    sortedSources[0].preferred = 1;
    return pickComparisonUnit(
        // $FlowFixMe: no flow typing for fromJS()
        Immutable.fromJS(sortedSources),
        conversions, measures, userWeightPref, userVolumePref, commonPurchaseUnit);
  });
}

export function receivingUnitName(
    source: Immutable.Map<string, *>, // $FlowTODO: immutable in shape of ProductSource
    conversions: Conversions, shortened: boolean = true,
) {

  function packageToDisplayName(key: string) {
    return builtinPackages[key] ? builtinPackages[key].displayName : key;
  }

  switch (source.get('pricePer')) {
    case 'unit':
      return unitDisplayName(
          // $FlowFixMe: only with records can we assert member existence/value
          source.get('pricingUnit'),
          conversions,
      );
    case 'package':
      const packageAbbrName = packageToDisplayName(
          // $FlowFixMe: only with records can we assert member existence/value
        source.get('subPackageName') || source.get('packageName'),
      );
      if (shortened) return packageAbbrName;
      const packageUnitName = unitDisplayName(
          // $FlowFixMe: only with records can we assert member existence/value
          source.get('unit'),
          conversions,
      );
      // $FlowFixMe: only with records can we assert member existence/value
      return `${source.get('packageSize')}${packageUnitName} ${packageAbbrName}`;
    case 'super-package':
      // $FlowFixMe: only with records can we assert member existence/value
      const superPackageAbbrName = packageToDisplayName(source.get('packageName'));
      if (shortened) return superPackageAbbrName;
      // $FlowFixMe: only with records can we assert member existence/value
      const superPackageUnitName = unitDisplayName(source.get('unit'), conversions);
      // $FlowFixMe: only with records can we assert member existence/value
      const superPackage = `${source.get('superPackageSize')}\u00d7${source.get('packageSize')}${superPackageUnitName}`;
      // $FlowFixMe: only with records can we assert member existence/value
      return `${superPackageAbbrName} of ${superPackage} ${packageToDisplayName(source.get('subPackageName'))}`;
    default: return null;
  }
}

// For use in TakeInventory (inventory/pars)
// For a packaging, the user should be able to denominate quantities in one of the following:
// - The super package (if there is one)
// - The package
// - If a common purchase unit is defined for a (measure, measure-system), that unit
export function expandSourceOptions(
    source: Immutable.Map<string, *>, // $FlowTODO: Immutable ProductSource
    existingSources: Immutable.List<Immutable.Map<string, *>>,
    conversions: Conversions,
    purchaseWeightSystem: MeasureSystem, purchaseVolumeSystem: MeasureSystem,
): Array<$Shape<ProductSource>> {
  const expandedSources = [];
  const costingQuantity = costingSize(source);

  // case of 2 bottles of -> 2 bottles of
  if (source.get('subPackageName')) {
    const existed = existingSources.some(s =>
        s.get('packageName') === source.get('subPackageName')
        && !s.get('subPackageName')
        && s.get('unit') === source.get('unit')
        && s.get('supplier') === source.get('supplier'),
    );

    if (!existed) {
      expandedSources.push({
        cost: convertPerQuantityValue(
            // $FlowFixMe: only with records can we assert member existence/value
            source.get('cost'),
            costingQuantity.amount, costingQuantity.unit,
            // $FlowFixMe: only with records can we assert member existence/value
            source.get('packageSize'), source.get('unit'),
            conversions,
        ),
        measure: source.get('measure'),
        packageName: source.get('subPackageName'),
        packageSize: source.get('packageSize'),
        packaged: true,
        pricePer: 'package',
        superPackageSize: 1,
        supplier: source.get('supplier'),
        unit: source.get('unit'),
      });
    }
  }

  // $FlowFixMe: only with records can we assert member existence/value
  const measure: number = source.get('measure');
  function addAdditionalPureUnit(unit) {
    const existed = existingSources.some(s =>
        !s.get('packageName')
        && !s.get('subPackageName')
        && s.get('unit') === unit
        && s.get('supplier') === source.get('supplier'),
    );

    if (!existed) {
      expandedSources.push({
        cost: convertPerQuantityValue(
            // $FlowFixMe: only with records can we assert member existence/value
            source.get('cost'),
            costingQuantity.amount, costingQuantity.unit,
            1, unit,
            conversions,
        ),
        measure,
        packageSize: 1,
        packaged: false,
        pricePer: 'unit',
        pricingMeasure: measure,
        pricingUnit: unit,
        superPackageSize: 1,
        supplier: source.get('supplier'),
        unit,
      });
    }
  }

  let userPurchaseSystem = 'metric';
  if (measure === 2) userPurchaseSystem = purchaseWeightSystem;
  if (measure === 3) userPurchaseSystem = purchaseVolumeSystem;
  const commonPurchaseUnits = getCommonPurchaseUnits(measure, userPurchaseSystem === 'metric');
  commonPurchaseUnits.forEach(unit => addAdditionalPureUnit(unit.get('id')));

  return expandedSources;
}

/**
 * find id of source having lowest cost option
 *
 * converts all sources to effective size,
 * then convert every cost to first effective size
 * and then returns id of source having minimum cost
 */
export function getLowestCostSourceId(
  sources: List<Immutable.Map<string, *>>,
  conversions: Conversions,
): ?number {

  if (sources.isEmpty()) return null;

  // $FlowFixMe: no flow typing for fromJS()
  const sourceCostList: List<Immutable.Map<string, *>> = sources.map(s => Immutable.fromJS({
    id: s.get('id'),
    cost: s.get('cost'),
    packageSize: costingSize(s),
  }));

  const headSource = sourceCostList.first();

  // $FlowFixMe: no flow typing for fromJS()
  const minCostSource: Immutable.Map<string, *> = sourceCostList.map(s => Immutable.fromJS({
    id: s.get('id'),
    cost: convertPerQuantityValue(
      // $FlowFixMe: only with records can we assert member existence/value
      s.get('cost'),
      // $FlowFixMe: only with records can we assert member existence/value
      s.getIn(['packageSize', 'amount']), s.getIn(['packageSize', 'unit']),
      // $FlowFixMe: only with records can we assert member existence/value
      headSource.getIn(['packageSize', 'amount']), headSource.getIn(['packageSize', 'unit']),
      conversions,
    ),
  }))
  // $FlowFixMe: no flow typing for fromJS()
  .sortBy(s => s.get('cost'))
  .first();

  return minCostSource && minCostSource.get('id');
}