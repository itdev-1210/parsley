// @flow
import Immutable from 'immutable';
import _ from 'lodash';

import lunr from 'lunr';
import type { Index, PipelineFunction, Query, Token, Tokenizer } from 'lunr';
import * as SearchActions from '../actions/SearchActions';
import type { IndexType } from '../reducers/indexesCache';
import { actionCreators as SearchIndexesActions } from '../reducers/indexesCache';
import store from '../store';

/**
 * From lunr.js:
 *
 * A function for splitting a string into tokens ready to be inserted into
 * the search index. Uses `separator` (defaults to lunr.tokenizer.separator) to split strings, change
 * the value of this property to change how strings are split into tokens.
 *
 * This tokenizer will convert its parameter to a string by calling `toString` and
 * then will split this string on the regular expression in `separator`.
 * Arrays will have their elements converted to strings and wrapped in a lunr.Token.
 *
 * Optional metadata can be passed to the tokenizer, this metadata will be cloned and
 * added as metadata to every token that is created from the object to be tokenized.
 *
 * @static
 * @param {?(string|object|object[])} obj - The object to convert into tokens
 * @param {?object} metadata - Optional metadata to associate with every token
 * @returns {lunr.Token[]}
 * @see {@link lunr.Pipeline}
 */
function makeTokenizer(separator: RegExp = lunr.tokenizer.separator) {
  return function (obj, metadata) {
    if (obj == null || obj == undefined) {
      return [];
    }

    if (Array.isArray(obj)) {
      return obj.map((t) => new lunr.Token(
            lunr.utils.asString(t).toLowerCase(),
            lunr.utils.clone(metadata),
        ));
    }

    let str = obj.toString().trim().toLowerCase(),
        len = str.length,
        tokens = [];

    for (let sliceEnd = 0, sliceStart = 0; sliceEnd <= len; sliceEnd++) {
      let char = str.charAt(sliceEnd),
          sliceLength = sliceEnd - sliceStart;

      if (char.match(separator) || sliceEnd == len) {

        if (sliceLength > 0) {
          let tokenMetadata = lunr.utils.clone(metadata) || {};
          tokenMetadata['position'] = [sliceStart, sliceLength];
          tokenMetadata['index'] = tokens.length;

          tokens.push(
              new lunr.Token(
                  str.slice(sliceStart, sliceEnd),
                  tokenMetadata,
              ),
          );
        }

        sliceStart = sliceEnd + 1;
      }

    }

    return tokens;
  };
}

const wildcardTermOptions = Immutable.Map({
  wildcard: lunr.Query.wildcard.TRAILING,
});

const stemmedTermOptions = Immutable.Map({
});

/*
 * custom additions to the search and indexing pipelines pipeline functions
 */
const normalizeCase = function (token) {
  return token.update(() => token.toString().toLowerCase());
};

// strips leading tokens that contain leading digits. e.g. in "1103E 444", drop "1103E"
export function stripLeadingDigitToken(token: Token<*>, index: number, tokens: Array<Token<*>>) {
  const leadingDigitsRegex = /^\d+\D+/;
  if (index != 0 || leadingDigitsRegex.exec(token.toString()) === null)
    return token.update(() => token.toString().toLowerCase());
  else {
    return undefined;
  }
};

// takes one token and returns two - stemmed and unstemmed versions - so search
// queries can match either of the two
const optionalStemmer = function(token, i, tokens) {
  return [
    token,
    lunr.stemmer(token.clone()),
  ];
};

// Register the pipeline functions so the index can be serialised
lunr.Pipeline.registerFunction(normalizeCase, 'normaliseCase');
lunr.Pipeline.registerFunction(stripLeadingDigitToken, 'stripLeadingDigitToken');
lunr.Pipeline.registerFunction(optionalStemmer, 'optionalStemmer');

function parsleySearchPipeline(preprocessingSteps: Array<PipelineFunction<*>>): lunr.Pipeline {
  const p = new lunr.Pipeline();
  for (const step of preprocessingSteps) {
    p.add(step);
  }
  p.add(
      normalizeCase,
      optionalStemmer,
  );

  return p;
}

type ResultBooster = $FlowTODO;
type TokenBoostablePred = $FlowTODO;
export type SearchOpts = {
  resultBoost?: ResultBooster,
  isTokenBoostable?: TokenBoostablePred,
  tokenSeparator?: RegExp,
  preprocessingSteps?: Array<PipelineFunction<*>>,
}
export class SearchLunr {

  lunr: Index;

  ref: *;
  keys: *;
  list: *;

  resultBoost: ResultBooster;
  isTokenBoostable: TokenBoostablePred;
  tokenizer: Tokenizer<*, *>;
  preprocessingSteps: Array<PipelineFunction<*>>;


  constructor(list: Array<Object>, keys: Array<string>, ref: string, opts: SearchOpts) {
    const {
      resultBoost = () => 0,
      isTokenBoostable = () => true,
      tokenSeparator = lunr.tokenizer.separator,
      preprocessingSteps = [],
    } = opts;
    this.keys = keys;
    this.ref = ref;
    this.list = list.reduce((acc, item) => {
      acc[item[ref]] = item;
      return acc;
      }, {},
    );
    this.resultBoost = resultBoost;
    this.isTokenBoostable = isTokenBoostable;
    this.tokenizer = makeTokenizer(tokenSeparator);
    this.preprocessingSteps = preprocessingSteps;
  }

  queryFunction = (queryString: string) => (qb: Query) => {
    const tokens = this.tokenizer(queryString);
    let boostsLeft = 2;
    tokens.forEach(tok => {
      let termOptions = {};

      if (boostsLeft > 0 && this.isTokenBoostable(tok.str)) {
        // boost early tokens, but only if our heuristics say they're probably
        // information-rich
        termOptions.boost = 2;
        boostsLeft--;
      }

      // match *either* by trailing-wildcard rules *or* by stemming rules. if matches
      // by *both*, this will be equivalent to a 2x boost, but I don't care enough
      // to jump through hoops to avoid that
      qb.term(tok, {
        ...wildcardTermOptions.toJS(),
        ...termOptions,
      });
      qb.term(tok, {
        ...stemmedTermOptions.toJS(),
        ...termOptions,
        // undocumented behavior: fuzzy matching turns off wildcard search -_-
        editDistance: Math.min(3, tok.str.length / 2),
      });
    });
  };

  searchWithQueryFn(queryString: string, relevanceCutoff?: number) {
    let results = this.lunr.query(this.queryFunction(queryString)).map(result => {
      let resultQuery = this.list[result.ref];
      // On MenuEdit in the isCreatingNew case, we sometimes end up with !resultQuery (???)
      if (resultQuery) {
        resultQuery.score = result.score + this.resultBoost(resultQuery);
      }

      return resultQuery;
    }).filter(_.identity);
    results = _.sortBy(results, r => -r.score);
    if (relevanceCutoff)
      results = results.filter(r => r.score > relevanceCutoff);
    return results;
  }

  initIndex() {
    const {
      list, keys, ref,
      tokenizer, preprocessingSteps,
    } = this; // because 'this' will be rebound below

    this.lunr = lunr(function () {
      // from here down, "this" refers to the lunr.Builder
      this.ref(ref);
      const pipeline = parsleySearchPipeline(preprocessingSteps);
      this.pipeline = this.searchPipeline = pipeline;
      this.tokenizer = tokenizer;
      keys.forEach(function (key) {
        this.field(key);
      }, this);
      for (const r in list) {
        this.add(list[r]);
      };
    });
  }
}

export class SearchLunrStore extends SearchLunr {
  constructor(
      list: Array<Object>, keys: Array<string>, ref: string,
      indexName: IndexType,
      opts: SearchOpts,
  ) {
    super(list, keys, ref, opts);
    const index = store.getState().getIn(['indexesCache', indexName]);
    if (index)
      this.lunr = lunr.Index.load(index);
    else {
      super.initIndex();
      store.dispatch(SearchIndexesActions.addIndexToCache(indexName, this.lunr.toJSON()));
    }
  }
}

export class SearchLunrBase extends SearchLunr {
  constructor(list: Array<Object>, keys: Array<string>, ref: string, opts: SearchOpts) {
    super(list, keys, ref, opts);
    super.initIndex();
  }
}

/*
 * keys is which attributes of list items to search on
 * resultBoost (optional) is a function taking an item, returning a number,
 *    usually in the (-1, 1) range. A positive number moves the item up in
 *    search results, a negative number moves it down.
 */
export function getSearchObject(
    list: Array<Object>, keys: Array<string>, opts: SearchOpts,
    ref: string = 'id', index?: IndexType): SearchLunr {
  let search;
  try {
    if (index)
      search = new SearchLunrStore(list, keys, ref, index, opts);
    else
      search = new SearchLunrBase(list, keys, ref, opts);
  } catch (err) {
    store.dispatch(SearchActions.searchListError(err));
  }
  // $FlowFixMe: only undefined in error case
  return search;
}

/*
 * assorted utilities
 */

export function ingredientListBoost(item: { isUsed: boolean }) {
  return item.isUsed ? 0.5 : 0;
}

export const ingredientListSearchOpts = {
  resultBoost: ingredientListBoost,
};