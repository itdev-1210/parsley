// @flow
import React from 'react';
import type { ComponentType } from 'react';

type FieldArrayProps<Props> = {|
  component: ComponentType<Props>,
  // array of fields - to be replaced with field path
  // by callers when we flip the switch on redux-form 7
  fields: Array<any>,
  props: Props,
|};

export function FieldArray<C>(props: FieldArrayProps<C>) {
  const {
    component: UnderlyingComponent,
    fields,
    props: passthrough,
  } = props;

  return <UnderlyingComponent
    fields={fields}
    {...passthrough}
  />;
}