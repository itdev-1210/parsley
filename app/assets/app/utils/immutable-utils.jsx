import { List, OrderedMap } from 'immutable';

/*
 * will work on non-set collections, but it'll be waaaaay slow.
 * O(a.size() + b.size()) for sets; O(a.size() * b.size()) for others
 */
export function setEquals(a, b) {
  return a.isSubset(b) && b.isSubset(a);
}

/*
 * Operates on Immutables
 *
 * Takes a List of Maps with an "id" attribute, and returns
 * an Map from id to the original Maps with id omitted
 */
export function byId(rawItems) {
  var entries = rawItems.map(m =>
      List.of(m.get("id"), m));
  return OrderedMap(entries);
}
