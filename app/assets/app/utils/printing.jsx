
import _ from "lodash";

export const photoUrl = (photoSrc, logoUrl) => {

  if (photoSrc instanceof Blob) {
    return URL.createObjectURL(photoSrc);
  } else {
    return logoUrl + photoSrc;
  }
}

export const printWindowJSSnippet =
    "var isMobileSafari = function () {\n" +
      "\tvar ua = window.navigator.userAgent;\n" +
      "\tvar iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);\n" +
      "\tvar webkit = !!ua.match(/WebKit/i);\n" +
      "\treturn iOS && webkit && !ua.match(/CriOS/i);\n" +
    "};\n" +
    "window.onload = function() { window.print(); setTimeout(function() { if(!isMobileSafari()) { window.close() } }, 1500); }";

// We are following some hacky way to print
// so that images are loaded
// before opening print window.
export const printContent = (contentToPrint, imageUrlList) => new Promise((resolve, reject) => {

  document.getElementById('section-to-print').innerHTML = contentToPrint;

  if (_.isEmpty(imageUrlList)) { resolve(window.print()) }

  let counter = 0;
  imageUrlList.forEach((imageUrl, idx) => {

    const imageToLoad = new Image();
    imageToLoad.onload = () => {

      if (imageUrl.className) {
        _.each(document.getElementsByClassName(imageUrl.className), elem => elem.src = imageUrl.url);
      } else {
        document.getElementById(imageUrl.id).src = imageUrl.url; 
      }
      counter++;
      if (counter === imageUrlList.length) { // last image
        resolve(window.print());
      }
    }
    imageToLoad.src = imageUrl.url;
  });
})