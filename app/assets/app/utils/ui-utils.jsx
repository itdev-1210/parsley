export function scrollToTopErrorTooltip() {
  const tooltips = document.getElementsByClassName('error-tooltip');
  if (tooltips[0]) {
    tooltips[0].scrollIntoView();
  }
}
