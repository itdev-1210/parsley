// @flow
import _ from 'lodash';
import React from 'react';
import type { ComponentType, Element } from 'react';

import createBrowserHistory from 'history/lib/createBrowserHistory';
import useBeforeUnload from 'history/lib/useBeforeUnload';

import { useScroll } from 'react-router-scroll';
import { Redirect, Route, Router, applyRouterMiddleware, useRouterHistory } from 'react-router';

import type { FeatureName } from 'common/utils/features';

import App from './ui-components/pages/App';

import Dashboard from './ui-components/pages/Dashboard';

import SupplierList from './ui-components/pages/SupplierList';
import SupplierEdit from './ui-components/pages/SupplierEdit';

import MeasureList from './ui-components/pages/MeasureList';
import MeasureEdit from './ui-components/pages/MeasureEdit';

import NutrientEdit from './ui-components/pages/NutrientEdit';

import IngredientList from './ui-components/pages/IngredientList';
import IngredientEdit from './ui-components/pages/IngredientEdit';

import RecipeList from './ui-components/pages/RecipeList';
import RecipeEdit from './ui-components/pages/RecipeEdit';

import MenuList from './ui-components/pages/MenuList';
import MenuEdit from './ui-components/pages/MenuEdit';

import OrderList from './ui-components/pages/OrderList';
import OrderEdit from './ui-components/pages/OrderEdit';

import InventoryList from './ui-components/pages/InventoryList';
import InventoryDetails from './ui-components/pages/InventoryDetails';

import PurchaseOrderList from './ui-components/pages/PurchaseOrderList';
import PurchaseOrderEdit from './ui-components/pages/PurchaseOrderEdit';

import StdlibManager from './ui-components/pages/StdlibManager';
import AdminDashboard from './ui-components/pages/AdminDashboard';
import UserList from './ui-components/pages/UserList';
import OnboardingList from './ui-components/pages/OnboardingList';
import OnboardingEdit from './ui-components/pages/OnboardingEdit';

import OnboardingWidgetsList from './ui-components/pages/OnboardingWidgetsList';
import OnboardingWidgetsEdit from './ui-components/pages/OnboardingWidgetsEdit';

import LocationList from './ui-components/pages/LocationList';
import LocationEdit from './ui-components/pages/LocationEdit';

import AccountSettings from './ui-components/pages/AccountSettings';
import { IngredientCategoryEditor, RecipeCategoryEditor, InventoryCategoryEditor, EditCategories } from './ui-components/pages/CategoryEditor';
import MeasureSettings from './ui-components/pages/MeasureSettings';
import UserManager from './ui-components/pages/UserManager';
import PosSetup from './ui-components/pages/PosSetup';
import PosImport from './ui-components/pages/PosImport';
import ImportReceipts from './ui-components/pages/ImportReceipts';
import ImportReceivables from './ui-components/pages/ImportReceivables';
import { featureIsSupported } from './utils/features';

export const app_root = '';

type AccessFlags = $Shape<{
  adminOnly: boolean,
  stdlibOnly: boolean,
  stdlibAllowed: boolean,
  readOnlyAllowed: boolean,
  featureName: FeatureName,
}>;

class NavSection {
  rootUrl: string;
  name: string;
  component: ComponentType<any>;
  subRoutes: Array<Element<any>>;
  accessFlags: AccessFlags;


  // JSX interprets element classes starting with lowercase as strings (i.e.
  // native DOM element types), not as React classes, hence DefaultComponent
  constructor(
      rootUrl: string, name: string,
      DefaultComponent: ComponentType<any>, subRoutes: ?Array<Element<any>>,
      accessFlags: AccessFlags,
  ) {
    this.rootUrl = rootUrl;
    this.name = name;
    this.subRoutes = subRoutes || [];
    this.accessFlags = accessFlags || {};

    if (DefaultComponent === undefined) {
      DefaultComponent = () => <p>Currently at {this.name}</p>;
    }

    this.component = (props) => (
      props.children || <DefaultComponent {...props} />
    );
  }

  getRoute(user, defaultRoute) {

    const notAllowed = (user.isReadOnly && !this.accessFlags.readOnlyAllowed) ||
                       (user.isStdlib && !this.accessFlags.stdlibAllowed);

    if (notAllowed) {
      return <Redirect from={this.rootUrl} to={defaultRoute}/>;
    }

    return (
        <Route key={this.name} path={this.rootUrl} component={this.component}>
          {this.subRoutes}
        </Route>);
  }
}

export const sections: Array<NavSection> = [
  ['dashboard', 'Dashboard', Dashboard,
    [],
    { featureName: 'DASHBOARD' },
  ],
  ['orders', 'Plans & Purchasing', OrderList,
    [
      <Route key="edit"
             path=":id(/from/:origId)"
             component={OrderEdit} />,
    ],
  ],
  ['purchase_orders', 'Receiving', PurchaseOrderList,
    [
      <Route key="edit"
             path=":id"
             component={PurchaseOrderEdit} />,
    ],
    { featureName: 'RECEIVING' },
  ],
  ['inventories', 'Inventory', InventoryList,
    [
      <Route key="edit"
             path=":id"
             component={InventoryDetails} />,
    ],
    { featureName: 'INVENTORY' },
  ],
  ['menus', 'Menus', MenuList,
      [
      <Route key="edit"
             path=":id(/from/:origId)"
             component={MenuEdit} />,
      ],
  ],
  ['recipes', 'Recipes', RecipeList,
      [
          /*
           * see ingredients route for details on :origId
           */
        <Route key="edit"
               path=":id(/from/:origId)"
               component={RecipeEdit} />,
      ],
      { readOnlyAllowed: true },
  ],
  ['ingredients', 'Ingredients', IngredientList,
    [
        /*
         :origId is only used when params.id == 'new'. I don't know how to enforce
         that in the router config, so IngredientEdit will deal with it.
        */
        <Route key="edit"
               path=":id(/from/:origId)"
               component={IngredientEdit} />,
    ],
    { stdlibAllowed: true },
  ],
  ['suppliers', 'Suppliers', SupplierList,
    [
      <Route key="edit" path=":id" component={SupplierEdit} />,
    ],
  ],
  ['admin', 'Admin Tools', AdminDashboard,
    [
      <Route key="measures" path="measures" component={MeasureList} />,
      <Route key="measureEdit" path="measures/:id" component={MeasureEdit} />,
      <Route key="nutrients" path="nutrients" component={NutrientEdit} />,
      <Route key="user" path="users" component={UserList} />,
      <Route key="onboardings" path="onboardings" component={OnboardingList} />,
      <Route key="onboardingEdit" path="onboardings/:id" component={OnboardingEdit} />,
      <Route key="onboardingsWidgets" path="onboardingsWidgets" component={OnboardingWidgetsList} />,
      <Route key="onboardingWidgetsEdit" path="onboardingsWidgets/:id" component={OnboardingWidgetsEdit} />,
    ],
    { adminOnly: true },
  ],
  [
    'stdlib', 'Manage Standard Lib', StdlibManager, [], { stdlibOnly: true, stdlibAllowed: true },
  ],
].map(args => new NavSection(...args));

export const history = useRouterHistory(useBeforeUnload(createBrowserHistory))();

// https://github.com/rafrex/react-router-hash-link/tree/react-router-v2/3
export function hashLinkScroll(waitTime: number = 0) {
  const { hash } = window.location;
  if (hash !== '') {
    // Push onto callback queue so it runs after the DOM is updated,
    // this is required when navigating from a different page so that
    // the element is rendered on the page before trying to getElementById.
    setTimeout(() => {
      const id = hash.replace('#', '');
      const element = document.getElementById(id);
      if (element) element.scrollIntoView();
    }, waitTime);
  }
}

export const router = (user: $FlowTODO) => {

  let defaultRoute;
  if (user.isStdlib) {
    defaultRoute = app_root + '/ingredients';
  } else {
    defaultRoute = app_root + '/recipes';
  }

  return <Router history={history} render={applyRouterMiddleware(useScroll())} onUpdate={hashLinkScroll}>
    <Redirect from={app_root} to={defaultRoute} />
    <Redirect from="/" to={defaultRoute} />
    <Route path={app_root} component={App}>
      {sections.map((s) => s.getRoute(user, defaultRoute))}
      <Route path="account" component={AccountSettings} />
      {!user.isReadOnly ?
        [<Route key="measure-settings" path="measure-settings" component={MeasureSettings} />,
        <Route key="manage-users" path="manage-users" component={UserManager} />,
        <Route key="edit_categories" path="edit_categories" component={EditCategories} />,
        <Route key="ingredient_categories" path="ingredient_categories" component={IngredientCategoryEditor} />,
        <Route key="inventory_categories" path="inventory_categories" component={InventoryCategoryEditor} />,
        <Route key="recipe_categories" path="recipe_categories" component={RecipeCategoryEditor} />,
        <Route key="manage-locations" path="manage-locations" component={LocationList} />,
        <Route key="edit-location" path="manage-locations/:id" component={LocationEdit} />,
        <Route key="pos-setup" path="pos-setup" component={PosSetup} />,
        <Route key="pos-import" path="pos-import" component={PosImport} />,
        <Route key="import-receivables-pricing" path="import-receivables-pricing" component={ImportReceivables} />,
        <Route key="receipts-import" path="receipts-import" component={ImportReceipts} />,
        ]
        : null}
    </Route>
  </Router>;
};

// debugging
window.parsleyRouter = router;
