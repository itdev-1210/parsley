// @flow
import React, { Component, type ElementConfig, type ElementRef, type Node, type Ref } from 'react';
import classNames from 'classnames';
import { getSearchObject, type SearchLunr, type SearchOpts } from '../utils/search';
import _ from 'lodash';

import { components } from 'react-select';
import ReactSelect from 'react-select/src/Select';
import AsyncSelect from 'react-select/src/Async';
import AsyncCreatableSelect from 'react-select/src/AsyncCreatable';

import type { IndexType } from '../reducers/indexesCache';
import type { CreatableProps } from 'react-select/src/Creatable';

// react-select's OptionType is vague to the point of uselessness
export type SelectValue = number | string;
export type SelectOption = {
  value: SelectValue,
  label: string,
  disabled?: boolean,
};
type SelectStateManagerProps = {// things that stateManager handles, but doesn't type correctly
  inputValue: string,
  onInputChange: Function,
  onMenuClose: Function,
  onMenuOpen: Function,
}
type Props = {
  // we do different things with onChange, onBlur, etc.
  ...$Exact<$Diff<ElementConfig<typeof ReactSelect>, SelectStateManagerProps>>,
  onChange: SelectValue => void,
  onBlur?: SelectValue => void,
  options: Array<SelectOption>,
  value: ?SelectValue,

  // search props
  extraKeys?: Array<string>,
  searchIndexName?: IndexType,
  searchObject?: SearchLunr,
  searchOpts: SearchOpts,

  // assorted behavioral flags
  clearable?: boolean,
  creatable: boolean,
  disabled?: boolean,
  grabFocus: boolean,
  selectOnBlur: boolean,
  allowBlankNewOptions: boolean,

  handlerOnFocus: () => void,
  handlerOnBlur: () => void,
  createNew?: SelectOption => void,
  additionalLabel?: (string | number) => Node,
  promptTextCreator: $ElementType<CreatableProps, 'formatCreateLabel'>,

  inputRef?: Ref<typeof AsyncSelect | typeof AsyncCreatableSelect>,
}

export default class Select extends Component<Props> {
  static defaultProps = {
    creatable: false,
    selectOnBlur: true,
    grabFocus: false,
    searchOpts: {},
    allowBlankNewOptions: true,
    handlerOnFocus: () => {},
    handlerOnBlur: () => {},
    promptTextCreator: () => 'New',

    // things react-select should have defaults for (or mark as optional) but doesn't
    hideSelectedOptions: false,
  };

  search: ?SearchLunr;
  selectComponent: ?ElementRef<typeof AsyncSelect>;

  constructor(props: Props, context: any) {
    super(props, context);
    this.setupSearch(props);
  }

  getInnerSelect(): ?ElementRef<typeof ReactSelect> {
    // react-select components are composed of an atribtrary number of nestd HOCs
    // however, we can always get the next nesting layer with the '.select' attr,
    // meaning we can get all the way down to the root one iteratively

    if (!this.selectComponent) {
      return undefined;
    }

    let currentTry = this.selectComponent;
    while (currentTry.select) {
      currentTry = currentTry.select;
    }

    // $FlowFixMe: flow doesn't get that the recursion above only terminates with the right type of select
    return currentTry;
  }

  setupSearch(props: Props) {
    if (props.searchObject) {
      this.search = props.searchObject;
    } else {
      const {
        options,
        extraKeys,
        searchOpts,
        searchIndexName,
      } = props;
      let keys = ['label'];
      if (extraKeys) {
        keys.push(...extraKeys);
      }
      this.search = getSearchObject(options, keys, searchOpts, 'value', searchIndexName); // create search object
    }
  }


  componentDidMount() {
    if (this.props.grabFocus && this.selectComponent) {
      this.selectComponent.focus();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.searchObject) {
      this.search = this.props.searchObject;
    } else if (_.differenceBy(this.props.options, prevProps.options, 'label').length > 0) {
      this.setupSearch(this.props);
    }
  }

  componentWillUnmount() {
    // remove this reference for the GC to collect the object when it's not referenced elsewhere
    // this prevents a memory leak, see PARS-1954 & 1974
    this.search = null;
    delete this.search;
  }

  render () {
    const {
        creatable,
        inputRef, // ref for ReactSelect component; it implements focus only, but that's good enough
        onChange,
        createNew,
        onBlur,
        selectOnBlur,
        additionalLabel,
        value,
        disabled,
        clearable,
        options,
        allowBlankNewOptions,
        promptTextCreator,
        handlerOnFocus,
        handlerOnBlur,
        ...sealedPassthrough
    } = this.props;
    let passthrough = {};
    Object.assign(passthrough, sealedPassthrough); // jumping through a hoop to unseal the passthrough object

    let selectOptions = options;

    passthrough.onBlur = event => {
      const innerSelect = this.getInnerSelect();
      let newValue = value; // event has value of inputValue, not selected option
      if (selectOnBlur && innerSelect && innerSelect.props.menuIsOpen) {
        // hacky proxy for "has the user explicitly selected an option" -
        // if the user has been operating on pure mouse, expect them to have clicked
        if (innerSelect.props.inputValue && innerSelect.state.focusedOption) {
          newValue = innerSelect.state.focusedOption.value;
        }
      }

      if (onBlur) {
        // react-select calls onBlur with a dummy input element, whose value is AFAICT always ''
        // redux-form's onBlur updates fields with event.target.value
        // the result is left as an exercise to the reader

        // if called with a raw value instead of an event, redux-form uses it
        // hopefully if we have an onBlur, this is a controlled element with a props.value
        // $FlowFixMe not getting the called-with-value alternative
        onBlur(newValue);
      }

    };

    if (onChange) {
      // react-select calls onChange with the full { value, label } object, not just the value
      passthrough.onChange = (opt, eventAction) => {
        if (this.props.isMulti) {
          // $FlowFixMe: opt is actually an array of opts, and semantics are complicated enough we'll just let the caller handle it
          onChange(opt, eventAction);
        } else if (opt && createNew && eventAction && eventAction.action === 'create-option') {
          // $FlowFixMe: we know this is a single value, because !isMulti
          return createNew(opt);
        } else {
          // single-select, and we selected an existing value
          if (this.search) {
            // $FlowFixMe: react-select types this as... something?
            selectOptions = this.search.searchWithQueryFn(value);
          }
          // $FlowFixMe: we know this is a single value, because !isMulti
          return onChange(opt && opt.value, eventAction);
        }
      };
    }

    if (additionalLabel) {
      passthrough.components = {
        Option: (props) => {
          const { data, innerProps } = props;
          return (
            <components.Option {...props}>
              <div
                role="option"
                id={`${innerProps.id}-option-${data.value}`}
                style={{ position: 'relative' }}
                title={data.title}>
                  <div className={data.className}>{data.label}</div>
                  {data.label !== data.value ? additionalLabel(data.value) : null}
              </div>
            </components.Option>
          );
        }, // REFACTOR
      };
    }

    const baseRef = selectComponent => {
      this.selectComponent = selectComponent;
      // $FlowFixMe: we actually only accept function version of ref
      if (inputRef) inputRef(selectComponent);
    };

    // tabSelectsValue combined with our custom call to selectFocusedOption()
    // would end up selecting the first in the list, not the focused one, if we
    // blurred by hitting tab
    passthrough.tabSelectsValue = false;
    let clearValue;
    if (passthrough.isMulti && Array.isArray(value)) {
      clearValue = value.map(val => (
        {
          label: val,
          value: val,
        }
      ));
    } else
      clearValue = selectOptions.find(option => option.value === value);

    const customComponents = {
      SingleValue: (props) => {
        const { data } = props;
        return (
          <components.SingleValue {...props}>
            { /* $FlowFixMe: react-select actuallyy will accept any Node, not just strings */ }
            <div className={data.className}>{data.label}</div>
          </components.SingleValue>
        );
      },
      DropdownIndicator: (props) =>
        components.DropdownIndicator &&
        <components.DropdownIndicator {...props}>
          <span className="select-dropdown-indicator"/>
        </components.DropdownIndicator>,
      ClearIndicator: (props) =>
       components.ClearIndicator &&
       <components.ClearIndicator {...props}>
         <span className="select-clear-indicator" title="Clear value">x</span>
       </components.ClearIndicator>,
       MenuList: (props) => {
        return (
          <React.Fragment>
            {
              (creatable && !passthrough.isMulti) &&
              <div
                className="select-creatable-option"
                onClick={() => props.selectProps.onChange({ label: '', value: '' }, { action: 'create-option' })}>
                  {promptTextCreator('')}
              </div>
            }
            <components.MenuList {...props}>
              {props.children}
            </components.MenuList>
          </React.Fragment>
        );
      },
    };

    const commonProps = {
      isDisabled: disabled,
      isClearable: clearable,
      className: classNames('Select', passthrough.className),
      classNamePrefix: 'Select',
      loadOptions: (inputValue) =>
        Promise.resolve(this.search ? this.search.searchWithQueryFn(inputValue) : options),
      defaultOptions: options,
      value: clearValue,
      isOptionDisabled: (option) => option.disabled,
      components: { ...customComponents, ...passthrough.components },
      tabSelectsValue: true,
      minMenuHeight: 300,
      /**
       * Redux-form's onFocus and onBlur events are conflicting with default in react-select.
       * As a fix we're not passing those events to react-select but instead redefining as
       * handlerOnFocus and handlerOnBlur in case we want to respond to these events
       */
      onFocus: handlerOnFocus,
      onBlur: handlerOnBlur,
    };

    if (creatable) {
      passthrough.ref = creatableSelect => baseRef(creatableSelect && creatableSelect.select);

      /**
       * Async components are used because the filterOption props was modified and
       * now their params contains all options and returns a boolean to filter out items
       */
      return <AsyncCreatableSelect
          key="async-creatable-react-select"
          {...passthrough}
          {...commonProps}
          isValidNewOption={(inputValue, selectValue, selectOptions) => {
            if (!passthrough.isMulti) return false;
            const isNotDuplicated = !selectOptions
                 .map(option => option.label.toLowerCase())
                 .includes(inputValue.toLowerCase());
              const nonEmpty = inputValue !== '';
              return (nonEmpty || allowBlankNewOptions) && isNotDuplicated;
          }}
          createOptionPosition="first"
          formatCreateLabel={promptTextCreator}
          shouldKeyDownEventCreateNewOption={({ keyCode }) => [
            9, // TAB
            13, // ENTER
          ].includes(keyCode)}
      />;
    }

    /**
     * Async components are used because the filterOption props was modified and
     * now their params contains all options and returns a boolean to filter out items
     */
    // $FlowFixMe: AsyncSelect props don't recognize defaultProps of inner Select
    return <AsyncSelect
      key="async-react-select"
      ref={baseRef}
      {...passthrough}
      {...commonProps}
    />;
  }
}
