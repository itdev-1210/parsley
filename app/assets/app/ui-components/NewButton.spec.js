import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import sinonChai from 'sinon-chai';
chai.use(chaiEnzyme());
chai.use(sinonChai);

import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { spy } from 'sinon';

import NewButton from './NewButton';

describe('<NewButton/>', function() {

  it('should show text indicating a new item', function() {
    const wrapper = mount(<NewButton noun="Item" />);
    expect(wrapper).to.contain.text('+ New Item');
  });

  it('should have a href value of the new page', function() {
    const wrapper = mount(<NewButton noun="Item" newPage="/#/myNewPage" />);
    expect(wrapper).to.have.attr('href', '/#/myNewPage');
  });

  describe('on click', function() {
    it('should preferentially run createItem function', function() {
      const createItemFn = spy();
      const wrapper = mount(<NewButton noun="Item" createItem={createItemFn} />);
      wrapper.find('a').simulate('click');
      expect(createItemFn).to.have.been.calledOnce;
    });

    it.skip('should run navigateToNew if createItem is not available', function() {
      const context = { router: [] };
      const wrapper = mount(<NewButton noun="Item" />, context);
      wrapper.find('a').simulate('click');
      expect(NewButton.prototype.navigateToNew).to.have.been.calledOnce;
    });
  });

});
