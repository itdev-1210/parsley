import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import ReactPropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import Immutable from 'immutable';
import _ from 'lodash';
import moment from 'moment';

import ImmutablePropTypes from 'react-immutable-proptypes';
import * as ParsleyPropTypes from './utils/PropTypes';

import {
  getUserInfo,
  getSupplierList, getMeasures,
  getIngredientList,
  getProductUsageInfo,
  getMultipleProductsUsageInfo,
  getSupplierSourcedIngredients,
  createPurchase,
  getOrderList,
  getPurchaseList,
} from '../webapi/endpoints';
import { actionCreators as UIStateActionsList } from '../reducers/uiState/orderList';
import { actionCreators as UIStateActionsEdit } from '../reducers/uiState/orderEdit';
import { productionOrderFromState } from '../reducers/productionOrder';

import {
  convertShoppingListJSONToFormValues, shallowToObject,
  valueFromEvent, convertPurchaseOrderFormValuesToJSON,
} from '../utils/form-utils';
import { convert, convertPerQuantityValue, unitDisplayName, divideQuantities, purchasingRound, rescaleUnits } from '../utils/unit-conversions';

import { Link } from 'react-router';
import {
  Table, Button, InputGroup,
  Modal, Row, Col, ListGroup, ListGroupItem,
} from 'react-bootstrap';
import { withRouter } from 'react-router';

import Validate from 'common/ui-components/Validate';
import { checkFields, deepArrayField, deepObjectField, isBlank, nonnegative, required } from '../utils/validation-utils';
import { defaultComparator } from 'common/utils/sorting';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Select from './Select';
import AddButton from './AddButton';
import PrintButton from './PrintButton';
import CurrencyDisplay from './CurrencyDisplay';
import PackagedAmountInput, { IndeterminateQuantityWarning } from './PackagedAmountInput';
import { printContent } from '../utils/printing';
import { effectivePackageSize } from '../utils/packaging';
import { FieldArray } from '../utils/redux-upgrade-shims';
import { featureIsSupported } from '../utils/features';
import { dateFormat, dateWithWeekdayFormat } from 'common/utils/constants';

function validate(values) {

  const validators = {
    items: deepArrayField(deepObjectField({
      product: prod => isBlank(prod) ? 'Please select an item' : null,
      totalPurchased: deepObjectField({
        amount: required(nonnegative),
      }),
    })),
  };

  return checkFields(values, validators);
}


@withRouter
@reduxForm(
    {
      form: 'shopping-list',
      fields: [
        // not for user modification - just used for cross-referencing into the full data structure
        'items[].product',

        'items[].totalPurchased.amount',
        'items[].totalPurchased.measure',
        'items[].totalPurchased.unit',
        'items[].selectedSource',
        // other fields for future user editing can be found in PropTypes.jsx:shoppingListEntry

        // hidden, non-modifiable fields - necessary to maintain proper UI state for newly-added items
        'items[].userAdded',
        'items[].supplierId', // this one also just makes things easier in the common case

      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => {
      const productionOrder = productionOrderFromState(state, ownProps.baseOrder, true);
      const detailedIngredients = state.get('detailedIngredients');

      let fullShoppingListItems = productionOrder
        && productionOrder.get('shoppingList')
          .groupBy(item => item.get('product'))
          // groupBy doesn't assume that the key is unique, so need to assert this
          .map(itemList => itemList.first());
      return {
        allSuppliers: state.get('suppliers'),
        fullShoppingListItems,
        initialValues: {
          items: productionOrder && state.get('measures')
            && (convertShoppingListJSONToFormValues(
            fullShoppingListItems.toList(),
          ) || []).filter(item =>
              _.get(item, ['totalPurchased', 'amount'], 0) > 0 ||
              _.get(item, ['totalPurchased', 'plusIndeterminate']),
          ),
        },
        purchaseWeightSystem: state.getIn(['userInfo', 'purchaseWeightSystem']),
        purchaseVolumeSystem: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        purchaseOrders: state.get('purchaseOrders'),
        detailedIngredients,
        units: state.getIn(['measures', 'units']),
        supplierSourcedIngredients: state.get('supplierSourcedIngredients'),
        companyLogoUrl: state.getIn(['userInfo', 'companyLogo']) ?
              state.getIn(['userInfo', 'logoUrl']) + state.getIn(['userInfo', 'companyLogo'])
              : null,
      };
    },
    (dispatch, ownProps) => ({
      uiStateActionsList: bindActionCreators(UIStateActionsList, dispatch),
      uiStateActionsEdit: bindActionCreators(UIStateActionsEdit, dispatch),
    }),
)
export default class ShoppingListPublic extends Component {
  render() {
    const { fields, ...rest } = this.props;
    return <FieldArray component={ShoppingList}
      fields={fields.items}
      props={{ ...rest }}
    />;
  }
}

class ShoppingList extends Component {
  static propTypes = {
    // from caller
    baseOrder: ImmutablePropTypes.listOf(
        ImmutablePropTypes.contains({
          product: ReactPropTypes.number.isRequired,
          quantity: ParsleyPropTypes.quantity.isRequired,
        }),
    ).isRequired,
    orderId: ReactPropTypes.number,

    supplierSourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource),
    page: ReactPropTypes.string.isRequired,

    shoppingEmailStatus: ImmutablePropTypes.map,
    // NOT for direct use - if it's missing, it's calculated from fullShoppingListItems by getDisplayedSuppliers()
    displayedSuppliers: ImmutablePropTypes.setOf(ReactPropTypes.number),
    // TODO: find some way to represent this
    // startingInventory: ImmutablePropTypes.listOf(
    //   ImmutablePropTypes.contains({
    //     product: ReactPropTypes.number.isRequired,
    //     quantity: ParsleyPropTypes.quantity.isRequired
    //   })
    // ),

    // from redux store
    allSuppliers: ParsleyPropTypes.summaryList,
    fullShoppingListItems: ImmutablePropTypes.listOf(ParsleyPropTypes.shoppingListEntry),
    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
    fromPurchaseOrder: ReactPropTypes.bool,
  };

  static defaultProps = {
    shoppingEmailStatus: Immutable.Map(),
    fromPurchaseOrder: true,
  };

  static usedIngredientsList(shoppingList) {
    if (shoppingList) {
      return shoppingList.map(entry => entry.get('product'));
    } else {
      return Immutable.Set();
    }
  }

  static getDisplayedSuppliers(props) {
    let { fullShoppingListItems, displayedSuppliers } = props;

    displayedSuppliers = displayedSuppliers
        || (fullShoppingListItems && fullShoppingListItems.map(
            i => i.getIn(['source', 'supplier'], 'unknown'),
        ).toSet());
    return displayedSuppliers;
  }

  constructor(props) {
    super(props);
    this.state = {
      confirmSupplierId: null,
      confirmAction: '',
    };
  }

  componentWillMount() {
    getUserInfo();
    getSupplierList();
    getMeasures();
    getIngredientList(); // Get ingredients' name

    if (this.props.fullShoppingListItems)
      getMultipleProductsUsageInfo(ShoppingList.usedIngredientsList(this.props.fullShoppingListItems));

    let displayedSuppliers = ShoppingList.getDisplayedSuppliers(this.props);
    if (displayedSuppliers) {
      for (let supplierId of displayedSuppliers.filter(Number.isInteger)) {
        getSupplierSourcedIngredients(supplierId);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.fullShoppingListItems &&
        !Immutable.is(this.props.fullShoppingListItems, nextProps.fullShoppingListItems)
    ) {
      getMultipleProductsUsageInfo(ShoppingList.usedIngredientsList(nextProps.fullShoppingListItems)
          .toSet()
          .subtract(
              ShoppingList.usedIngredientsList(this.props.fullShoppingListItems),
          ));
    }

    let displayedSuppliers = ShoppingList.getDisplayedSuppliers(nextProps);
    if (displayedSuppliers && !Immutable.is(displayedSuppliers, ShoppingList.getDisplayedSuppliers(this.props))) {
      for (let supplierId of displayedSuppliers.filter(Number.isInteger)) {
        getSupplierSourcedIngredients(supplierId);
      }
    }
  }

  render() {
    const {
      fullShoppingListItems, allSuppliers, fields, supplierSourcedIngredients, shoppingEmailStatus, companyLogoUrl,
      baseOrder, purchaseOrders, fromPurchaseOrder,
      close, send,
      page, orderId, detailedIngredients,
      uiStateActionsList, uiStateActionsEdit,
      purchaseWeightSystem, purchaseVolumeSystem,
    } = this.props;
    if (!(fullShoppingListItems && allSuppliers && purchaseOrders) || (fullShoppingListItems.size > 0 && fields.length === 0)) {
      return <LoadingSpinner />;
    }
    const displayedSuppliers = ShoppingList.getDisplayedSuppliers(this.props);

    const getSupplierName = id => Number.isInteger(id)
        ? allSuppliers.find(entry => entry.get('id') == id).get('name')
        : 'Unknown Source';

    const indexedItems = Immutable.List(fields.map((field, index) => ({ index, ...field })));
    let [...sortedSupplierIds] = indexedItems
      .map(item => item.supplierId.value)
      .filter(supplierId => displayedSuppliers.has(supplierId))
      .toSet() // make unique suppler id
      .sortBy(supplierId => Number.isInteger(supplierId) ? getSupplierName(supplierId).toLocaleLowerCase() : '');

    if (!fromPurchaseOrder) {
      sortedSupplierIds = displayedSuppliers.sortBy(supplierId => Number.isInteger(supplierId) ? getSupplierName(supplierId) : '');
    }

    const printShoppingList = (selectedSupplierId) => {

      const filteredSupplierIds = sortedSupplierIds.filter(s => {
        const validItems = indexedItems.filter(item => item.supplierId.value === s && item.product.value);
        if (validItems.size === 0) return false;
        if (selectedSupplierId && s !== selectedSupplierId) return false;
        return true;
      });

      filteredSupplierIds.forEach(supplierToOrder => {
        const items = indexedItems.filter(item => item.supplierId.value === supplierToOrder);
        const userEditedOrder = getUserEditedOrder(items, supplierSourcedIngredients.get(supplierToOrder), detailedIngredients);
        let orders = [];
        if (this.props.page === 'edit') {
          if (orderId) orders = [orderId];
        } else {
          orders = this.props.baseOrder.toJS();
        }
        if (supplierToOrder !== 'unknown') {
          createPurchase(supplierToOrder, orders, userEditedOrder, 'print');
        }
      });

      const content = ReactDOMServer.renderToString(
        <html>
        <head />
        <body>
          <div className="print-title" style={companyLogoUrl ? { position: 'relative' } : {}}>Purchase Order for {moment().format(dateFormat)}</div>
          { companyLogoUrl ?
            <div className="purchase-gray-label">
              <img id="company-logo" />
            </div>
            : null
          }
          <div className={this.props.companyLogoUrl ? 'shopping-list pt-5' : 'shopping-list'}>
            <div className="logo">
              <img id="powered-by-logo" />
            </div>
            { filteredSupplierIds
                .filter(supplierId => {
                  if (selectedSupplierId) {
                    return supplierId === selectedSupplierId;
                  } else { // if none given, print all
                    return true;
                  }
                })
                .map(supplierId => {
                  const items = indexedItems.filter(item => item.supplierId.value === supplierId);
                  return <PrintedShoppingListPage
                    supplierId={supplierId} supplierName={getSupplierName(supplierId)}
                    key={String(supplierId)} items={items}
                    sourcedIngredients={supplierSourcedIngredients.get(supplierId)}
                    detailedIngredients={this.props.detailedIngredients}
                    units={this.props.units}
                    fullShoppingListItems={fullShoppingListItems}
                    purchaseWeightSystem={purchaseWeightSystem}
                    purchaseVolumeSystem={purchaseVolumeSystem}
                    fromPurchaseOrder={fromPurchaseOrder}
                  />;
                })
            }
          </div>
        </body>
        </html>,
      );

      const imageUrlListWithId = [{ url: '/assets/images/poweredby.png', id: 'powered-by-logo' }];

      if (companyLogoUrl) {
        imageUrlListWithId.push({ url: companyLogoUrl, id: 'company-logo' });
      }

      return printContent(content, imageUrlListWithId);
    };

    let sections = sortedSupplierIds.map(supplierId => {
      const items = indexedItems.filter(item => item.supplierId.value === supplierId);
      return <ShoppingListSection
        fromPurchaseOrder={fromPurchaseOrder}
        supplierId={supplierId}
        shouldScroll={supplierId == this.props.scrollToSupplier}
        supplierName={getSupplierName(supplierId)}
        key={String(supplierId)} items={items}
        fullShoppingListItems={fullShoppingListItems}
        removeItem={index => fields.removeField(index)}
        addItem={() => fields.addField({ supplierId, userAdded: true })}

        sourcedIngredients={supplierSourcedIngredients.get(supplierId)}
        purchaseWeightSystem={purchaseWeightSystem}
        purchaseVolumeSystem={purchaseVolumeSystem}

        shoppingAction={(purchaseMethod) => {
          const checkOrders = page === 'edit' ? Immutable.fromJS([orderId]) : baseOrder;
          // confirm for supplier which contains order whose purchase order is already created
          const previousPurchaseOrder = purchaseOrders
            .filter(po => po.get('supplier') === supplierId)
            .filter(po => po.get('orders').some(orderId => checkOrders.includes(orderId)));
          if (featureIsSupported('RECEIVING') && !previousPurchaseOrder.isEmpty()) {
            if (purchaseMethod === 'email') {
              this.setState({ confirmSupplierId: supplierId, confirmAction: 'email', prevPurchaseOrder: previousPurchaseOrder.first() });
            } else if (purchaseMethod === 'print') {
              this.setState({ confirmSupplierId: supplierId, confirmAction: 'print', prevPurchaseOrder: previousPurchaseOrder.first() });
            }
          } else if (purchaseMethod === 'email') {
              const userEditedOrder = getUserEditedOrder(items, supplierSourcedIngredients.get(supplierId), detailedIngredients);
              close();
              send(supplierId, userEditedOrder);
            } else if (purchaseMethod === 'print') {
              printShoppingList(supplierId);
            }
        }}


        detailedIngredients={this.props.detailedIngredients}
        units={this.props.units}
        page={this.props.page}
        isEmailSent={shoppingEmailStatus.get(`${supplierId}`)}
      />;
    });

    let printAllButton = <PrintAllButton printShoppingList={printShoppingList} disabled={indexedItems.size === 0}/>;

    if (fromPurchaseOrder && indexedItems.size < 1) {
      sections = <p>There are no ingredients needed for this order.</p>;
      printAllButton = null;
    }

    return <div>
      {printAllButton}
      <div className="shopping-list">
        <div className="shopping-list-inside-wrapper">
          {sections}
        </div>
      </div>
      {printAllButton}
      {
        typeof this.state.confirmSupplierId === 'number' ?
          <ConfirmOverridePurchase
            confirmAction={this.state.confirmAction}
            prevPurchaseOrder={this.state.prevPurchaseOrder}
            hide={() => {
              this.setState({
                confirmSupplierId: null,
                confirmAction: '',
              });
            }}
            confirm={() => {
              const confirmSuppId = this.state.confirmSupplierId;
              if (this.state.confirmAction === 'print') {
                printShoppingList(confirmSuppId);
                getPurchaseList();
              } else if (this.state.confirmAction === 'email') {
                const userEditedOrder = getUserEditedOrder(
                  indexedItems.filter(f => f.supplierId.value === confirmSuppId),
                  supplierSourcedIngredients.get(confirmSuppId),
                  detailedIngredients,
                );
                if (this.props.page === 'edit') {
                  uiStateActionsEdit.closeShoppingListModal();
                  uiStateActionsEdit.openSendModal(this.state.confirmSupplierId, userEditedOrder);
                } else if (this.props.page === 'list') {
                  uiStateActionsList.hidePurchasing();
                  uiStateActionsList.openSendModal(this.state.confirmSupplierId, userEditedOrder);
                }
              }
              // close confirm modal
              this.setState({
                confirmSupplierId: null,
                confirmAction: '',
              });
            }}
          />
          : null
      }
      <div className="clearfix" />
    </div>;
  }
}

export function assembleItemInfo(
    fields,
    fullShoppingListItems,
    detailedIngredients,
    thisIngredientSources,
) {
  const purchaseQuantity = shallowToObject(fields.totalPurchased);
  if (!fields.product.value) {
    return { loading: false };
  }
  const detailedProductInfo = detailedIngredients.get(fields.product.value);

  if (!detailedProductInfo) {
    return {
      loading: true,
    };
  }

  const conversions = detailedProductInfo.get('measures');
  let returnValue = {
    // placeholders if we don't have sourcing information or conversions available
    purchasePackage: Immutable.fromJS(
        purchaseQuantity,
    ).set('amount', 1),
    conversions,
    name: detailedProductInfo.get('name'),
  };

  const source = thisIngredientSources && thisIngredientSources.find(s => s.get('id') == fields.selectedSource.value);
  if (source) {
    // The unit cost of the whole package
    returnValue.unitCost = (fields.unitCost && fields.unitCost.value) || source.get('cost');
    if (source.get('pricePer') === 'package') returnValue.unitCost *= source.get('superPackageSize');
    else if (source.get('pricePer') === 'unit') returnValue.unitCost *= source.get('superPackageSize') * source.get('packageSize');
    returnValue.purchasePackageName = PackagedAmountInput.fullPackageName(source, conversions);
    returnValue.sku = source.get('sku');
    const packageSize = effectivePackageSize(source);

    returnValue.purchasePackage = Immutable.Map(packageSize);

    if (source.get('supplierIngredientName'))
      returnValue.name = source.get('supplierIngredientName');

    // purchase quantities are internally represented in the primary unit, but we'd like to express them in the shopping unit
    if (purchaseQuantity.amount && purchaseQuantity.unit) {
      returnValue.totalCost = convertPerQuantityValue(returnValue.unitCost,
          packageSize.amount, packageSize.unit,
          purchaseQuantity.amount, purchaseQuantity.unit,
          conversions,
      );
    }
    returnValue.sourced = true;
  } else {
    returnValue.sourced = false;
  }

  // if the user adds an ingredient to the shopping list, we won't have any
  // totalRequired value
  let fullItem = fullShoppingListItems && fullShoppingListItems.get(fields.product.value);

  // ignore fullItem info if it's referring to same product from different supplier
  if (fullItem) {
    if (fields.supplierId.value === 'unknown' && !fullItem.has('source')) {
      // this counts as a correct supplierId value
    } else if (fullItem.getIn(['source', 'supplier']) !== fields.supplierId.value) {
      fullItem = undefined;
    }
  }

  if (fullItem) {
    let requiredQuantity = fullItem.get('totalRequired');
    if (requiredQuantity.get('amount') < 0) { // possible when there's an indeterminate requirement
      requiredQuantity = requiredQuantity.set('amount', 0);
    }
    returnValue.requiredPackages = divideQuantities(
        requiredQuantity.toJS(),
        returnValue.purchasePackage.toJS(),
        conversions,
    );
    // we care about this part of the requirement regardless of if there's sourcing info
    returnValue.indeterminateRequirement = requiredQuantity.get('plusIndeterminate');
  }

  return returnValue;
}

export function getUserEditedOrder(items, sourcedIngredients, detailedIngredients) {

  const orderItems = items.map(field => {

    const thisIngredientSources = sourcedIngredients &&
        sourcedIngredients.filter(s => s.get('product') === field.product.value);
    const {
        name,
        loading,
        conversions,
        purchasePackage,
        purchasePackageName, // only defined if sourced, to deal with packaged special-case
        requiredPackages,
        unitCost,
        totalCost,
        sku,
    } = assembleItemInfo(field,
        null, // don't care about required amount, so fullShoppingListItems unnecessary
        detailedIngredients,
        thisIngredientSources,
    );

    if (loading) {
      return null;
    }

    const numPackages = field.totalPurchased.amount.value && purchasePackage ?
        divideQuantities(shallowToObject(field.totalPurchased), purchasePackage.toJS(),
        conversions)
        : 0;
    const itemInfo = shallowToObject(field);

    return { display: { name, numPackages, purchasePackageName, sku }, itemInfo };

  });
  return orderItems.toJS();
}

class PrintedShoppingListPage extends Component {
  static propTypes = {
    supplierId: ReactPropTypes.number,
    supplierName: ReactPropTypes.string.isRequired,
    items: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired,
    sourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,
    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    purchaseWeightSystem: ReactPropTypes.string,
    purchaseVolumeSystem: ReactPropTypes.string,
  };

  render() {

    const {
        supplierId,
        supplierName,
        items,
        sourcedIngredients, detailedIngredients,
        fullShoppingListItems,
        purchaseWeightSystem, purchaseVolumeSystem,
        fromPurchaseOrder,
    } = this.props;

    const colAttrs = ShoppingListSection.colAttrs; // can't CSS-select for cells in a given column, so hack

    return <section className="shopping-list-page no-page-break-inside">
      <Table>
        <thead>
          <tr><th scope="column" colSpan="3" style={{ width: '100%' }}>{supplierName}</th></tr>
        </thead>
      </Table>
      <Table>
        <thead>
          <tr>
            <th scope="column" {...colAttrs.name}>Ingredient</th>
            {fromPurchaseOrder ? <th scope="column" {...colAttrs.requirement}>Needed</th> : null}
            <th scope="column" {...colAttrs.quantity}>Purchase</th>
            <th scope="column" {...colAttrs.sku}>SKU</th>
          </tr>
        </thead>
        <tbody>{items
          .map(field => {
            const thisIngredientSources = sourcedIngredients &&
              sourcedIngredients.filter(s => s.get('product') == field.product.value);
            const item = assembleItemInfo(field,
              fullShoppingListItems,
              detailedIngredients,
              thisIngredientSources,
            );
            item.field = field;
            return item;
          })
          .filter(({ field: { totalPurchased } }) => supplierId === 'unknown' || totalPurchased.amount.value > 0)
          .sort((a, b) => defaultComparator(a.name.toLowerCase(), b.name.toLowerCase()))
          .map(item => {
            const {
              name,
              sku,
              loading,
              requiredPackages,
              conversions,
              purchasePackage,
              purchasePackageName, // only defined if sourced, to deal with packaged special-case
              field,
            } = item;

          if (loading) {
            return <tr />;
          }

          let requiredAmount, requirementInfo;
          const totalPurchased = shallowToObject(field.totalPurchased);

          // Unknown source -- convert unit accordingly to `purchaseWeightSystem` && `purchaseVolumeSystem`
          if (supplierId === 'unknown') {
            // using totalPurchased as proxy for requirement amount, since two are equal in this case
            if (totalPurchased.amount) {
              const userPurchaseSystem = totalPurchased.measure === 2 ? purchaseWeightSystem : purchaseVolumeSystem;
              const rescaledUnit = rescaleUnits(totalPurchased, userPurchaseSystem === 'metric', true);
              requiredAmount = rescaledUnit.amount;
              requirementInfo = unitDisplayName(rescaledUnit.unit, conversions);
            } else {
              requiredAmount = 0;
              requirementInfo = '';
            }
          } else {
            requiredAmount = requiredPackages;
            const numPackages = divideQuantities(totalPurchased, purchasePackage.toJS(), conversions);
            requirementInfo = numPackages > 0 ?
              purchasePackageName ?
                `${purchasingRound(numPackages, 2)}&nbsp;&nbsp;&nbsp;&nbsp;${purchasePackageName}`
                : `${purchasingRound(numPackages, 2)} ${unitDisplayName(purchasePackage.get('unit'), conversions)}`
              : '';
          }

          return <tr className="separator-line">
            <td {...colAttrs.name}>{name}</td>
            {fromPurchaseOrder ? <td {...colAttrs.requirement}>{purchasingRound(requiredAmount, 2).toFixed(2)}</td> : null}
            <td
              {...colAttrs.quantity}
              dangerouslySetInnerHTML={{ __html: requirementInfo }} />
            <td {...colAttrs.sku}>{sku}</td>
          </tr>;
        })}</tbody>
      </Table>
      <br /><br />
    </section>;
  }
}

@withRouter
@connect(
    (state, ownProps) => ({
      ingredientsNameMap: Immutable.Map((state.get('ingredients') || Immutable.List()).map(i => [i.get('id'), i.get('name')])),
    }),
    (dispatch, ownProps) => ({
      uiStateActionsList: bindActionCreators(UIStateActionsList, dispatch),
      uiStateActionsEdit: bindActionCreators(UIStateActionsEdit, dispatch),
    }),
)
class ShoppingListSection extends Component {
  static propTypes = {
    supplierId: ReactPropTypes.number,
    supplierName: ReactPropTypes.string,
    supplierEmail: ReactPropTypes.string,
    shoppingAction: ReactPropTypes.func,
    items: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired,
    fullShoppingListItems: ImmutablePropTypes.listOf(ParsleyPropTypes.shoppingListEntry).isRequired,
    removeItem: ReactPropTypes.func.isRequired,
    addItem: ReactPropTypes.func.isRequired,

    sourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,

    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
    page: ReactPropTypes.string.isRequired,
    fromPurchaseOrder: ReactPropTypes.bool,
  };

  componentDidMount () {
    const { shouldScroll, supplierId, items, addItem, units } = this.props;
    if (shouldScroll) {
      const nextLocation = {
        pathname: this.props.location.pathname,
        hash: '#shopping-list-section-' + supplierId,
      };
      this.props.router.replace(nextLocation);
    }
    if (units && items.size === 0) {
      addItem();
    }
  }

  componentWillUnmount() {
    if (this.props.shouldScroll) {
      const nextLocation = {
        pathname: this.props.location.pathname,
        hash: '',
      };
      this.props.router.replace(nextLocation);
    }
  }

  componentDidUpdate(prevProps) {
    const { units, items, addItem } = this.props;
    if (!prevProps.unit && units && items.size === 0) {
      addItem();
    }
  }

  render() {
    const {
        supplierName, supplierId,
        shoppingAction,
        items, fullShoppingListItems, detailedIngredients, sourcedIngredients,
        removeItem, addItem,
        units, purchaseWeightSystem, purchaseVolumeSystem,
        isEmailSent,
        ingredientsNameMap,
        fromPurchaseOrder,
    } = this.props;

    const supplierLink = Number.isInteger(supplierId)
        ? <Link to={`/suppliers/${supplierId}`} target="_blank">{supplierName}</Link>
        : supplierName;
    const orderedItems = Immutable.Set(items.map(i => i.product.value).filter(_.identity));
    const unorderedItems = sourcedIngredients ?
        sourcedIngredients
            .map(s => {
              let label = s.get('supplierIngredientName') || ingredientsNameMap.get(s.get('product'));
              const conversions = detailedIngredients.getIn([s.get('id'), 'measures']);
              let ingredientsPackage = PackagedAmountInput.fullPackageName(s, conversions);
              if (ingredientsPackage) {
                label += ` (${ingredientsPackage})`;
              }
              return {
                value: s.get('id'),
                label,
              };
            }).toSet() // accept no duplicates
            .filter(product => !orderedItems.includes(product.value)) :
        Immutable.Set();
    const colAttrs = ShoppingListSection.colAttrs; // can't CSS-select for cells in a given column, so hack

    const itemsNotValid = !items.every(item => item.product.valid)
                       || !items.every(item => item.totalPurchased.amount.valid)
                       || !items.every(item => item.totalPurchased.unit.valid)
                       || !items.every(item => item.totalPurchased.measure.valid);

    let sectionCost = 0; // accumulator

    const safeToLowerCase = (input) => input && typeof input === 'string' ? input.toLowerCase() : input;

    return <Table className="shopping-list-section" id={'shopping-list-section-' + supplierId}>

      <colgroup>
        <col {...colAttrs.name}/>
        <col {...colAttrs.requirement}/>
        <col {...colAttrs.quantity}/>
        <col {...colAttrs.unitCost}/>
        <col {...colAttrs.totalCost}/>
        <col {...colAttrs.deleteLink}/>
      </colgroup>
      <thead>
      <tr>
        <th scope="colgroup" {...colAttrs.name}>
          <h4 style={{ marginBottom: '8px' }}>{supplierLink}&nbsp;&nbsp;</h4>
          { Number.isInteger(supplierId) ?
          <Button onClick={() => {
 shoppingAction('email');
}} disabled={isEmailSent || (fromPurchaseOrder && itemsNotValid) || items.size === 0}>
            <i className="glyphicon glyphicon-envelope" />&nbsp;{isEmailSent ? 'Sent' : 'Email'}
          </Button>
          : null } &nbsp;
          <PrintButton onClick={() => {
 shoppingAction('print');
}} disabled={(fromPurchaseOrder && itemsNotValid) || items.size === 0} />
        </th>
        {fromPurchaseOrder ?
          <th scope="column" {...colAttrs.requirement} style={{ paddingTop: '30px' }}>
            Needed
          </th>
        : null}
        <th scope="column" {...colAttrs.quantity} style={{ paddingTop: '30px' }}>
          {supplierId !== 'unknown' ? 'Purchase' : null}
        </th>
        <th scope="column" {...colAttrs.unitCost} style={{ paddingTop: '30px' }}>
          {supplierId !== 'unknown' ? 'Unit Cost' : null}
        </th>
        <th scope="column" {...colAttrs.totalCost} style={{ paddingTop: '30px' }}>
          {supplierId !== 'unknown' ? 'Total' : null}
        </th>
        <th scope="column" {...colAttrs.deleteLink} style={{ paddingTop: '30px' }} />
      </tr></thead>
      <tbody>
        { !units ? <LoadingSpinner /> :
            items.map(field => {
              const thisIngredientSources = sourcedIngredients &&
                  sourcedIngredients.filter(s => s.get('product') == field.product.value);
              const itemInfo = assembleItemInfo(field, fullShoppingListItems, detailedIngredients, thisIngredientSources);
              return { ...field, itemInfo };
            })
            .sort(
              (fieldA, fieldB) =>
                  defaultComparator(fieldA.userAdded.value, fieldB.userAdded.value) // user-added should go *last*
                  || fieldA.userAdded.value ? defaultComparator(fieldA.index, fieldB.index) // sort user-added by insertion order
                      // and order-added by name
                      : defaultComparator(safeToLowerCase(fieldA.itemInfo.name),
                          safeToLowerCase(fieldB.itemInfo.name)),
            )
            .map(field => {
              const thisIngredientSources = sourcedIngredients &&
                  sourcedIngredients.filter(s => s.get('product') == field.product.value);
              const {
                  name,
                  loading,
                  conversions,
                  purchasePackage,
                  requiredPackages,
                  indeterminateRequirement,
                  unitCost,
                  totalCost,
                  sourced,
              } = field.itemInfo;

              let itemNameDisplay, quantityDisplay, unitCostDisplay, totalCostDisplay, requirementDisplay;
              if (loading) {
                // each a separate declaration because I think React would react (heh) badly to the same
                // instantiated component being used multiple times
                itemNameDisplay = <LoadingSpinner />;
                unitCostDisplay = <LoadingSpinner />;
                totalCostDisplay = <LoadingSpinner />;
                quantityDisplay = <LoadingSpinner />;
                requirementDisplay = <LoadingSpinner />;
              } else {
                if (field.userAdded.value) {
                  const options = unorderedItems.filter(i => i.label).sortBy(i => i.label).toArray();
                  itemNameDisplay = <Validate {...field.product}><Select
                      placeholder="Select the ingredients to purchase"
                      options={options} {...field.selectedSource}
                      onChange={ev => {
                        const newSourceId = valueFromEvent(ev);
                        let newProductId;
                        const newIngredientSource = sourcedIngredients && sourcedIngredients.filter(s => s.get('id') === newSourceId);
                        if (newIngredientSource && !newIngredientSource.isEmpty()) {
                          newProductId = newIngredientSource.first().get('product');
                           getProductUsageInfo(newProductId);
                          field.selectedSource.onChange(newIngredientSource.first().get('id'));
                        } else {
                          field.selectedSource.onChange(null);
                        }
                        _.forOwn(field.totalPurchased, subfield => subfield.onChange(null));
                        field.product.onChange(newProductId);
                      }}
                  /></Validate>;
                } else {
                  itemNameDisplay = <Link
                      target="_blank"
                      to={`/ingredients/${field.product.value}`}>
                    {name}
                  </Link>;
                }

                if (unitCost != undefined) {
                  unitCostDisplay = <CurrencyDisplay value={unitCost}/>;
                }
                if (totalCost != undefined) {
                  totalCostDisplay = <CurrencyDisplay value={totalCost}/>;
                  // yes, side-effects in map() are bad. so sue me
                  sectionCost += totalCost;
                }

                if (sourced) {
                  const requiredPackagesDisplay = requiredPackages ?
                      purchasingRound(requiredPackages, 2).toFixed(2) :
                      null;
                  const requiredPackagesIndeterminateWarning = indeterminateRequirement ?
                      <IndeterminateQuantityWarning key="indeterminate"/> :
                      null;
                  requirementDisplay = [
                    requiredPackagesDisplay,
                    requiredPackagesIndeterminateWarning,
                  ];
                }

                if (field.product.value) {
                  quantityDisplay = <PackagedAmountInput
                    sources={
                      fromPurchaseOrder ?
                      thisIngredientSources
                      : thisIngredientSources.filter(s => s.get('id') === field.selectedSource.value)
                    }
                    quantityFields={field.totalPurchased}
                    selectedSource={field.selectedSource}
                    units={units}
                    packageSize={purchasePackage}
                    conversions={conversions}
                    indeterminateRequirement={indeterminateRequirement}
                    purchaseWeightSystem={purchaseWeightSystem}
                    purchaseVolumeSystem={purchaseVolumeSystem}
                  />;
                } else {
                  quantityDisplay = <div/>;
                }
              }

              return <tr key={field.product.value} className={field.userAdded.value ? 'user-added-shopping-list-item' : null}>
                <th scope="row" {...colAttrs.name}>{itemNameDisplay}</th>
                {supplierId !== 'unknown' && fromPurchaseOrder ? <td {...colAttrs.requirement}>{requirementDisplay}</td> : null}
                <td {...colAttrs.quantity} {...supplierId !== 'unknown' ? {} : { colSpan: '2' }}>{quantityDisplay}</td>
                <td {...colAttrs.unitCost}>{unitCostDisplay}</td>
                <td {...colAttrs.totalCost}>{totalCostDisplay}</td>
                <td {...colAttrs.deleteLink}>
                  <span className="delete-link" onClick={() => removeItem(field.index)}>remove</span>
                </td>
              </tr>;
            })
        }
      </tbody>
      { Number.isInteger(supplierId)
          ? <tfoot>
              <tr>
                <td {...colAttrs.name}><AddButton onClick={addItem} /></td>
                <td colSpan="3"/>
                <td {...colAttrs.totalCost}>
                  <h4>
                    Grand Total: <CurrencyDisplay value={sectionCost}/>
                  </h4>
                </td>
              </tr>
            </tfoot>
          : null
      }
      </Table>;
  }

  static colAttrs = {
    name: {
      className: 'shopping-list-name',
    },
    quantity: {
      className: 'shopping-list-quantity',
    },
    requirement: {
      className: 'shopping-list-requirement',
    },
    unitCost: {
      className: 'shopping-list-unit-cost',
    },
    totalCost: {
      className: 'shopping-list-total-cost',
    },
    deleteLink: {
      className: 'shopping-list-delete-link',
    },
    sku: {
      className: 'shopping-list-sku',
    },
  };
}

class PrintAllButton extends Component {

  static propTypes = {
    printShoppingList: ReactPropTypes.func.isRequired,
  };

  render() {
    const { printShoppingList, ...passthrough } = this.props;

    return <PrintButton
      onClick={() => printShoppingList()}
      className="pull-right"
      noun="Print All"
      {...passthrough}
    />;
  }
}

@connect(
  (state, ownProps) => ({
    suppliers: state.get('suppliers'),
    orders: state.get('orders'),
  }),
)
export class ConfirmOverridePurchase extends Component {

  render() {
    const {
      hide, confirm, confirmAction,
      prevPurchaseOrder,
      orders, suppliers,
    } = this.props;

    const confirmSupplier = suppliers && suppliers.filter(s => s.get('id') === prevPurchaseOrder.get('supplier')).first();
    const prevPurchaseOrderTime = moment(!prevPurchaseOrder.get('printedAt') ? prevPurchaseOrder.get('emailedAt') : prevPurchaseOrder.get('printedAt')).format(dateWithWeekdayFormat + ', h:mm A');
    const confirmOrders = orders && orders.filter(o => prevPurchaseOrder.get('orders').contains(o.get('id')));

    return <Modal show onHide={hide} className="modal-base">
      <Modal.Header closeButton>
        <Modal.Title>Confirm Purchase</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {
          !(confirmOrders && confirmSupplier)
            ? <LoadingSpinner />
            : <div>
            <Row><Col xs={12}>
              A purchase order/s has already been {!prevPurchaseOrder.get('printedAt') ? 'sent' : 'printed'} to {confirmSupplier.get('name')} for {prevPurchaseOrderTime}.
            </Col></Row>
            <Row><Col xs={12}>Do you want to {confirmAction === 'email' ? 'resend' : 'reprint'}?</Col></Row>
            <Row>
              <Col xs={12}>
                <ListGroup>
                  {confirmOrders.map(order => <ListGroupItem>{order.get('name')}</ListGroupItem>)}
                </ListGroup>
              </Col>
            </Row>
          </div>
        }
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={hide}>cancel</Button>
        <Button bsStyle="danger" onClick={confirm}>Confirm</Button>
      </Modal.Footer>
    </Modal>;
  }

  componentWillMount() {
    if (this.props.page === 'edit') {
      getOrderList();
    }
  }
}
