// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { app_root, sections } from '../navigation';
import AppAccountDropdown from './AppAccountDropdown';
import ParsleyNavbar from 'common/ui-components/ParsleyNavbar';
import RoutableLink from 'common/ui-components/RoutableLink';

import { Nav, NavItem }
  from 'react-bootstrap';

import { getUserInfo } from '../webapi/endpoints';
import { featureIsSupported } from '../utils/features';

@connect(
    state => ({
      user: state.get('userInfo'),
    }),
)
export default class AppNavbar extends Component<*> {
  render () {

    const {
        user,
        currentLocation,
    } = this.props;

    return <ParsleyNavbar>
      <Nav navbar>
        {sections.filter(s => {
          if (!user) return false; // hide everything until we know


          const accessFlags = s.accessFlags;
          if (accessFlags.adminOnly && !(user && user.get('isAdmin'))) return false;
          if (accessFlags.stdlibOnly && !(user && user.get('isStdlib'))) return false;

          if (!accessFlags.stdlibAllowed && !(user && !user.get('isStdlib'))) return false;
          if (!accessFlags.readOnlyAllowed && !(user && !user.get('isReadOnly'))) return false;

          if (accessFlags.featureName && !featureIsSupported(accessFlags.featureName)) return false;

          return true;
        }).map((section) =>
            // $FlowFixMe: can't simulate extra props passed in by decorators
            <RoutableLink
                baseElemClass={NavItem}
                key={section.rootUrl}
                path={[app_root, section.rootUrl].join('/')}
                currentLocation={currentLocation}
            >
              {section.name}
            </RoutableLink>,
        )}
      </Nav>
      <Nav navbar pullRight>
        { /* $FlowFixMe: can't simulate extra props passed in by decorators */}
        <AppAccountDropdown
            userInfo={user}
        />
      </Nav>
    </ParsleyNavbar>;
  }

  componentWillMount () {
    getUserInfo();
  }
}
