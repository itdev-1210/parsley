// @flow
import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  categories: Array<string>,
  activeCategories: Array<string>,
  onClick: Function, // react-router location
}

export default function SideBar({ currentLocation, categories, activeCategories, onClick }: Props) {
  return <div>
    {
      categories.map(name => {
        const isActive = activeCategories.includes(name);
        return <Button
            key={name}
            bsStyle="info"
            className="tagFilter"
            active={isActive}
            onClick={() => onClick(name, isActive)}>
            {name}
        </Button>;
      })
    }
  </div>;
}