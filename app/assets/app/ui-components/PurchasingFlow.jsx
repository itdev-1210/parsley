// @flow

import React, { Component } from 'react';
import Immutable from 'immutable';
import moment from 'moment';

import { Button, ListGroup, ListGroupItem, Modal } from 'react-bootstrap';

import { actionCreators as ProductionOrderActions, productionOrderFromState } from '../reducers/productionOrder';
import { actionCreators as UIStateActions } from '../reducers/uiState/purchasingFlow';
import { actionCreators as OrderListStateActions } from '../reducers/uiState/orderList';

import { getUserInfo, getIngredientPars, getProductionOrder, getSupplierList, getPurchaseList, getInventoryLastTakenTime } from '../webapi/endpoints';
import { updateSubtractInventoryPreference } from 'common/webapi/userEndpoints';

import Checkbox from './Checkbox';
import Radio from 'common/ui-components/Radio';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import ShoppingList from './ShoppingList';

import {
  isBeforeToday,
} from '../utils/general-algorithms';
import { featureIsSupported } from '../utils/features';
import type { SubtractInventoryPreference } from '../webapi/endpoints';

type PurchasingFlowProps = {
  purchaseEmailStatus: Immutable.Map<string, boolean>,
  uiState: Immutable.Map<string, any>,
  orders: Immutable.List<Immutable.Map<string, any>>,
  orderIdList: Immutable.List<number>,
  pastOrders: Immutable.List<Immutable.Map<string, any>>,
  purchaseOrders: Immutable.List<Object>,
  hide: () => void,
  productionOrderActions: typeof ProductionOrderActions,
  uiStateActions: typeof UIStateActions,
  display: boolean,
  includePar: boolean,
  fromPurchaseOrder: boolean,

  // api-backed information
  suppliers: Immutable.List<any>,
  usedSuppliers: Immutable.Set<number>,
  subtractInventoryPreference: SubtractInventoryPreference,
};

type PurchasingFlowState = {
  subtractingInventory: SubtractInventoryPreference,
  inventoryLastTakenTime: ?string,
}

@connect(
    (state, ownProps) => {
      const orderIdList = ownProps.orders.map(o => o.get('id'));
      const suppliers = state.getIn(['suppliers']);
      const ingredientPars = state.get('ingredientPars') ? state.get('ingredientPars').filter(p => p.get('amount') > 0) : Immutable.List();
      const productionOrder = productionOrderFromState(state, orderIdList, true);
      let usedSuppliers = null;

      if (!ownProps.fromPurchaseOrder) {
        // Purchase Direct: list all suppliers
        usedSuppliers = suppliers.map(s => s.get('id')).toSet();
      } else {
        if (productionOrder) {
          usedSuppliers = productionOrder
            .get('shoppingList')
            .map(i => i.getIn(['source', 'supplier'], 'unknown'))
            .toSet();
        }
        if (ownProps.includePar && ingredientPars.size) {
          usedSuppliers = (usedSuppliers || Immutable.Set()).concat(ingredientPars.map(p => p.get('supplier') || 'unknown')).toSet();
        }
      }

      return {
        uiState: state.getIn(['purchasingFlow']),
        suppliers,
        orderIdList,
        pastOrders: ownProps.orders.filter(o => isBeforeToday(o.get('date'))),
        usedSuppliers,
        subtractInventoryPreference: state.getIn(['userInfo', 'subtractInventoryPreference']),
      };
    },
    dispatch => ({
      uiStateActions: bindActionCreators(UIStateActions, dispatch),
      productionOrderActions: bindActionCreators(ProductionOrderActions, dispatch),
    }),

)
export default class PurchasingFlow extends Component<PurchasingFlowProps, PurchasingFlowState> {

  constructor(props: PurchasingFlowProps) {
    super(props);
    this.state = {
      inventoryLastTakenTime: null,
      subtractingInventory: props.subtractInventoryPreference,
    };

    // $FlowFixMe - this.x.bind(this) is type-weird
    this.checkAllSuppliers = this.checkAllSuppliers.bind(this);
    // $FlowFixMe
    this.unCheckAllSuppliers = this.unCheckAllSuppliers.bind(this);
    // $FlowFixMe
    this.checkSupplier = this.checkSupplier.bind(this);
    // $FlowFixMe
    this.unCheckSupplier = this.unCheckSupplier.bind(this);
    // $FlowFixMe
    this.subtractInventoryPreferenceOnChange = this.subtractInventoryPreferenceOnChange.bind(this);
  }

  checkAllSuppliers() {
    const { usedSuppliers, uiStateActions } = this.props;
    usedSuppliers.forEach(id => uiStateActions.addSupplier(id));
  }

  unCheckAllSuppliers() {
    const { usedSuppliers, uiStateActions } = this.props;
    usedSuppliers.forEach(id => uiStateActions.removeSupplier(id));
  }

  checkSupplier(id: number) {
    const { uiStateActions } = this.props;
    uiStateActions.addSupplier(id);
  }

  unCheckSupplier(id: number) {
    const { uiStateActions } = this.props;
    uiStateActions.removeSupplier(id);
  }

  subtractInventoryPreferenceOnChange(subtractInventoryPreference: SubtractInventoryPreference) {
    updateSubtractInventoryPreference(subtractInventoryPreference);

    let subtractLatestInventory = false, subtractCurrentInventory = false;
    if (subtractInventoryPreference === 'subtract-current-inventory') {
      subtractCurrentInventory = true;
    } else if (subtractInventoryPreference === 'subtract-latest-inventory') {
      subtractLatestInventory = true;
    }
    this.setState({
      subtractingInventory: subtractInventoryPreference,
    });
    getProductionOrder(this.props.orderIdList, true, this.props.includePar, subtractLatestInventory, subtractCurrentInventory);
  }

  render() {
    const {
      orders, orderIdList, pastOrders,
      hide: baseHide, uiState,
      suppliers, usedSuppliers,
      uiStateActions,
      purchaseOrders,
      purchaseEmailStatus, display, includePar, fromPurchaseOrder,
    } = this.props;

    function hide() {
      uiStateActions.leave();
      baseHide();
    }

    const hasSubtractInventoryModal = featureIsSupported('INVENTORY') && this.state.inventoryLastTakenTime && fromPurchaseOrder;

    let body, footerButtons;
    // $FlowFixMe: only with records can we assert member existence/value
    const selectedSuppliers: Immutable.Set<number> = uiState.get('selectedSuppliers');

    const closeModalLink = <Button
      style={{ float: 'right', color: 'gray', marginRight: '8px' }}
      onClick={hide}
      bsStyle="link">
      close
    </Button>;
    footerButtons = [closeModalLink];

    switch (uiState.get('phase') || 'supplierSelect') {
      case 'supplierSelect':
        if (!pastOrders.isEmpty()) {
          body = (pastOrders.size === orderIdList.size) ? <p>Cannot purchase for past orders</p>
            : <p>Cannot purchase for past orders, please uncheck the Past orders and try again</p>;
          break;
        } else {
          let topContinueButtonLabel, topContinueButtonAction;
          if (hasSubtractInventoryModal) {
            topContinueButtonLabel = 'Continue';
            topContinueButtonAction = uiStateActions.startPickSubtractInventoryOption;
          } else {
            topContinueButtonLabel = 'View Orders';
            topContinueButtonAction = uiStateActions.startReview;
          }
          const topContinueButton = <Button
                                      disabled={selectedSuppliers.isEmpty()}
                                      onClick={topContinueButtonAction}
                                      style={{ float: 'right' }}
                                    >
                                      {topContinueButtonLabel}
                                    </Button>;
          const bottomContinueButton = React.cloneElement(topContinueButton);
          body = <div>
            {topContinueButton}
            <SupplierSelect
                suppliers={suppliers} usedSuppliers={usedSuppliers}
                selectedSuppliers={selectedSuppliers}
                checkSupplier={this.checkSupplier}
                unCheckSupplier={this.unCheckSupplier}
                checkAllSuppliers={this.checkAllSuppliers}
                unCheckAllSuppliers={this.unCheckAllSuppliers}
            />
          </div>;
          footerButtons.unshift(bottomContinueButton);
        }
        break;
      case 'subtractInventoryOptionSelect':
        body = <div>
          <SubtractInventoryOptionSelect
              inventoryLastTakenTime={this.state.inventoryLastTakenTime}
              subtractInventoryPreference={this.state.subtractingInventory}
              subtractInventoryPreferenceOnChange={this.subtractInventoryPreferenceOnChange}
          />
        </div>;
        footerButtons = [
          <Button
            key="button-main"
            disabled={selectedSuppliers.isEmpty()}
            onClick={uiStateActions.startReview}
            style={{ float: 'right' }}
          >
            {fromPurchaseOrder ? 'View Orders' : 'Continue'}
          </Button>,
          <Button key="button-back" style={{ float: 'left' }} onClick={uiStateActions.startPickSuppliers}>
            back
          </Button>,
        ];
        break;
      // case 'inventoryCheck': // no inventory functionality as of yet
      case 'review':
        body = <OrderReview selectedSuppliers={selectedSuppliers}
                            orders={orderIdList} orderEmailStatus={purchaseEmailStatus}
                            purchaseOrders={purchaseOrders} includePar={includePar} fromPurchaseOrder={fromPurchaseOrder}
        />;
        footerButtons = [
          <Button key="button-done" onClick={hide}>done</Button>,
          <Button key="button-back" style={{ float: 'left' }}
                  onClick={hasSubtractInventoryModal ? uiStateActions.startPickSubtractInventoryOption : uiStateActions.startPickSuppliers}
          >
            back
          </Button>,
        ];
        break;
      default: break;
    }

    let modalTitle = 'Purchase ';
    if (!fromPurchaseOrder) {
      modalTitle += 'Direct';
    } else if (includePar) {
      modalTitle += 'to Par';
      if (orders.size > 0) modalTitle += ' plus Forecast';
    } else {
      modalTitle += 'to Forecast';
    }

    let titleSuffix = null;
    if (uiState.get('phase') === 'review') {
      const { subtractingInventory } = this.state;
      if (subtractingInventory === 'subtract-latest-inventory') {
        titleSuffix = '(Inventory Subtracted)';
      } else if (this.state.subtractingInventory === 'subtract-current-inventory') {
        titleSuffix = '(Expected Inventory Subtracted)';
      }
    }
    return <Modal show onHide={hide} style={(!display) ? { display: 'none' } : null}
                  className={uiState.get('phase') === 'review' ? 'medium-modal wider-modal' : 'small-modal'}>
      <Modal.Header closeButton>
        <Modal.Title>
          {modalTitle} {titleSuffix}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ marginLeft: '11px', marginRight: '11px' }}>{body}</Modal.Body>
      <Modal.Footer style={{ marginRight: '5%' }}>{footerButtons}</Modal.Footer>
    </Modal>;
  }

  handleProps(oldProps: $Shape<PurchasingFlowProps>, newProps: PurchasingFlowProps) {
    if (!Immutable.is(oldProps.orderIdList, newProps.orderIdList) || oldProps.includePar !== newProps.includePar) {
      const { subtractingInventory } = this.state;
      getProductionOrder(newProps.orderIdList, true, newProps.includePar,
          subtractingInventory === 'subtract-latest-inventory',
          subtractingInventory === 'subtract-current-inventory',
      );
    }
    if (!oldProps.subtractInventoryPreference && newProps.subtractInventoryPreference) {
      this.setState({ subtractingInventory: newProps.subtractInventoryPreference });
    }
  }

  componentWillMount() {

    const { pastOrders, uiStateActions, includePar } = this.props;

    // no need to fetch any data, just show message to uncheck past orders
    if (!includePar && !pastOrders.isEmpty()) return;

    getUserInfo();

    if (includePar) getIngredientPars();

    const blankProps = {
      uiState: Immutable.Map(),
      orders: Immutable.List(),
      hide: () => undefined,
    };
    this.handleProps(blankProps, this.props);

    getSupplierList();
    uiStateActions.startPickSuppliers();

    getInventoryLastTakenTime().then(lastTakenTime => {
      this.setState({ inventoryLastTakenTime: lastTakenTime });
    });
  }

  componentWillReceiveProps(nextProps: PurchasingFlowProps) {
    this.handleProps(this.props, nextProps);
  }

  componentWillUnmount() {
    const {
      productionOrderActions, uiStateActions,
    } = this.props;
    productionOrderActions.dropDetails();
    uiStateActions.leave();
    getPurchaseList();
  }
}

type SupplierSelectProps = {
  checkSupplier: (supplierId: number) => any,
  unCheckSupplier: (supplierId: number) => any,
  checkAllSuppliers: () => any,
  unCheckAllSuppliers: () => any,
  suppliers: Immutable.List<any>,
  usedSuppliers: Immutable.Set<number>,
  selectedSuppliers: Immutable.Set<number>,
}
export class SupplierSelect extends Component<SupplierSelectProps> {

  render() {
    const {
      suppliers, selectedSuppliers, usedSuppliers,
      checkAllSuppliers, unCheckAllSuppliers,
      checkSupplier, unCheckSupplier,
    } = this.props;

    if (!suppliers || !usedSuppliers) return <LoadingSpinner />;
    if (usedSuppliers.size < 1) return <p>There are no ingredients needed for this order.</p>;
    const getSupplierName = id => Number.isInteger(id)
        ? suppliers.find(entry => (entry.get('id') === id)).get('name')
        : 'Unknown Supplier';

    return <div id="purchasing-content">
      <h4>
        Select Suppliers
      </h4>
      <h5>
        <Checkbox
            inline
            checked={selectedSuppliers.equals(usedSuppliers)}
            indeterminate={selectedSuppliers.size > 0 && selectedSuppliers.size < usedSuppliers.size && selectedSuppliers.isSubset(usedSuppliers)}
            onChange={ev => ev.target.checked ? checkAllSuppliers() : unCheckAllSuppliers()}
        />
        Select All
      </h5>
      <ListGroup className="green-barred-list-group" id="purchasing-suppliers">
        {usedSuppliers.sortBy(id =>
            Number.isInteger(id) ? getSupplierName(id).toLocaleLowerCase() : '',
        ).map(id => {
          const isSelected = selectedSuppliers.contains(id);
          return <ListGroupItem key={id} style={(isSelected) ? { backgroundColor: '#bae08e' } : null}>
            <Checkbox inline checked={isSelected}
                      onChange={ev => ev.target.checked ? checkSupplier(id) : unCheckSupplier(id)}
            /> {Number.isInteger(id) ? getSupplierName(id) : <em>{getSupplierName(id)}</em> /* on same line to add space in markup */}
          </ListGroupItem>;
        })}
      </ListGroup>
    </div>;
  }
}

type SubtractInventoryOptionSelectProps = {
  inventoryLastTakenTime: ?string,
  subtractInventoryPreference: SubtractInventoryPreference,
  subtractInventoryPreferenceOnChange: SubtractInventoryPreference => any,
}
class SubtractInventoryOptionSelect extends Component<SubtractInventoryOptionSelectProps> {

  render() {
    const {
      inventoryLastTakenTime, subtractInventoryPreference,
      subtractInventoryPreferenceOnChange,
    } = this.props;

    return <div id="purchasing-content">
      <Radio
        checked={subtractInventoryPreference === 'subtract-current-inventory'}
        onChange={() => subtractInventoryPreferenceOnChange('subtract-current-inventory')}
      >
        Subtract Current Expected Inventory
      </Radio>
      <Radio
        style={{ padding: '15px 0px' }}
        checked={subtractInventoryPreference === 'subtract-latest-inventory'}
        onChange={() => subtractInventoryPreferenceOnChange('subtract-latest-inventory')}
      >
        Subtract Latest Physical Inventory
        (Taken {moment(inventoryLastTakenTime).fromNow()})
      </Radio>
      <Radio
        checked={subtractInventoryPreference === 'subtract-none'}
        onChange={() => subtractInventoryPreferenceOnChange('subtract-none')}
      >
        {'Don\'t Subtract Inventory'}
      </Radio>
    </div>;
  }
}

type OrderReviewProps = {
  selectedSuppliers: Immutable.Set<number>,
  orders: Immutable.List<number>,
  orderEmailStatus: Immutable.Map<string, boolean>,
  purchaseOrders: Immutable.List<Object>,
}

@connect(
  () => {},
  dispatch => ({
    orderListActions: bindActionCreators(OrderListStateActions, dispatch),
  }),
)
class OrderReview extends Component<OrderReviewProps> {

  render() {
    const {
      selectedSuppliers, orders, orderEmailStatus,
      // $FlowFixMe: react-redux!!!
      purchaseOrders, includePar, orderListActions, fromPurchaseOrder,
    } = this.props;

    return <ShoppingList
      page={'list'}
      baseOrder={orders}
      purchaseOrders={purchaseOrders}
      fromPurchaseOrder={fromPurchaseOrder}
      includePar={includePar}
      displayedSuppliers={selectedSuppliers}
      shoppingEmailStatus={orderEmailStatus}
      close={() => orderListActions.hidePurchasing()}
      send={(supplierId, userEditedOrder) => orderListActions.openSendModal(supplierId, userEditedOrder)}
    />;

  }
}
