import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import { history } from '../../navigation';

import { Row, Col, Button } from 'react-bootstrap';
import Radio from 'common/ui-components/Radio';
import Onboarding from '../Onboarding';
import PercentInput from '../PercentInput';

import Checkbox from '../Checkbox';

import { saveUserInfo, getUserInfo, getOnboardingInfo } from '../../webapi/endpoints.jsx';

import Validate from 'common/ui-components/Validate';
import { checkFields, required } from '../../utils/validation-utils';
import { MaskedNumberInput } from '../MaskedInput';

function validate(values) {
  return checkFields(values, {
    weightSystem: required(),
    volumeSystem: required(),
    purchaseWeightSystem: required(),
    purchaseVolumeSystem: required(),
  });
}

@withRouter
@reduxForm(
    {
      form: 'measure-settings',
      fields: [
        'weightSystem',
        'volumeSystem',
        'purchaseWeightSystem',
        'purchaseVolumeSystem',
        'hideAllRecipes',
        'sourcePriceChangeWarningFraction',
        'newRecipeThresholdDays',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      onSubmitSuccess: () => history.push('/measure-settings'),
    },
    (state, origProps) => ({
      initialValues: {
        weightSystem: state.getIn(['userInfo', 'weightSystem']),
        volumeSystem: state.getIn(['userInfo', 'volumeSystem']),
        purchaseWeightSystem: state.getIn(['userInfo', 'purchaseWeightSystem']),
        purchaseVolumeSystem: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        hideAllRecipes: state.getIn(['userInfo', 'hideAllRecipes']),
        sourcePriceChangeWarningFraction: state.getIn(['userInfo', 'sourcePriceChangeWarningFraction']),
        newRecipeThresholdDays: state.getIn(['userInfo', 'newRecipeThresholdDays']),
      },
      userInfo: state.get('userInfo'),
    }),
)
export default class MeasureSettings extends Component {
  constructor() {
    super();
    this.state = { saving: false };
  }
  saveData = (data) => {
    this.setState({ saving: true });
    const userUpdate = this.props.userInfo
                         .set('weightSystem', data.weightSystem)
                         .set('volumeSystem', data.volumeSystem)
                         .set('purchaseWeightSystem', data.purchaseWeightSystem)
                         .set('purchaseVolumeSystem', data.purchaseVolumeSystem)
                         .set('hideAllRecipes', data.hideAllRecipes)
                         .set('sourcePriceChangeWarningFraction', data.sourcePriceChangeWarningFraction)
                         .set('recipeOptions', { hideAllRecipes : data.hideAllRecipes, 
                          newRecipeThresholdDays: data.newRecipeThresholdDays })
    return saveUserInfo(userUpdate)
      .then(getUserInfo)
      .then(succ => {
        this.clearRouteLeaveHook();
        history.goBack();
      });
  }

  weightChangeM = () => {
 this.props.fields.weightSystem.onChange('metric');
}
  weightChangeI = () => {
 this.props.fields.weightSystem.onChange('imperial');
}
  volumeChangeM = () => {
 this.props.fields.volumeSystem.onChange('metric');
}
  volumeChangeI = () => {
 this.props.fields.volumeSystem.onChange('imperial');
}

  purchaseWeightChangeM = () => {
 this.props.fields.purchaseWeightSystem.onChange('metric');
}
  purchaseWeightChangeI = () => {
 this.props.fields.purchaseWeightSystem.onChange('imperial');
}
  purchaseVolumeChangeM = () => {
 this.props.fields.purchaseVolumeSystem.onChange('metric');
}
  purchaseVolumeChangeI = () => {
 this.props.fields.purchaseVolumeSystem.onChange('imperial');
}

  render() {
    const {
      handleSubmit,
    } = this.props;
    const {
     weightSystem,
     volumeSystem,
     purchaseWeightSystem,
     purchaseVolumeSystem,
     hideAllRecipes,
     sourcePriceChangeWarningFraction,
     newRecipeThresholdDays,
    } = this.props.fields;
    return (
      <Col xs={6} xsOffset={4}>
        <h3>Settings</h3>
        <hr className="green-hr"/>

        <Row className="mt-1">
          <h4>Measurement Preferences</h4>
          <Row className="mt-2" componentClass={Validate} {...weightSystem}>
            <Col xs={11} xsOffset={1}>
               <hr className="green-hr"/>
              <h4>Default unit of weight for recipes</h4>
              <Row>
                <Col md={1} xs={2}>
                <Radio
                  checked={weightSystem.value == 'imperial'}
                  name={'weight'}
                  onChange={this.weightChangeI}/>
                </Col>
                <Col xs={10} md={11}>Imperial (ounce, pound)</Col>
              </Row>
              <Row className="mt-1">
                <Col md={1} xs={2}>
                <Radio
                  checked={weightSystem.value == 'metric'}
                  name={'weight'}
                  onChange={this.weightChangeM}/>
                </Col>
                <Col xs={10} md={11}>Metric (gram, kilogram)</Col>
              </Row>
            </Col>
          </Row>

          <Row className="mt-1" componentClass={Validate} {...volumeSystem}>
            <Col xs={11} xsOffset={1}>
              <hr className="green-hr"/>
              <h4>Default unit of volume for recipes </h4>
              <Row>
                <Col md={1} xs={2}>
                <Radio
                  checked={volumeSystem.value == 'imperial'}
                  name={'volume'}
                  onChange={this.volumeChangeI}/>
                </Col>
                <Col xs={10} md={11}>Imperial (fluid ounce, pint, quart, gallon...)</Col>
              </Row>
              <Row className="mt-1">
                <Col md={1} xs={2}>
                <Radio
                  checked={volumeSystem.value == 'metric'}
                  name={'volume'}
                  onChange={this.volumeChangeM}/>
                </Col>
                <Col xs={10} md={11}>Metric (milliliter, liter)</Col>
              </Row>
            </Col>
          </Row>

          <Row className="mt-1" componentClass={Validate} {...purchaseWeightSystem}>
            <Col xs={11} xsOffset={1}>
              <hr className="green-hr"/>
              <h4>Default unit of weight for purchasing</h4>
              <Row>
                <Col md={1} xs={2}>
                <Radio
                  checked={purchaseWeightSystem.value == 'imperial'}
                  name={'pWeight'}
                  onChange={this.purchaseWeightChangeI}/>
                </Col>
                <Col xs={10} md={11}>Imperial (pound, ounce)</Col>
              </Row>
              <Row className="mt-1">
                <Col md={1} xs={2}>
                <Radio
                  checked={purchaseWeightSystem.value == 'metric'}
                  name={'pWeight'}
                  onChange={this.purchaseWeightChangeM}/>
                </Col>
                <Col xs={10} md={11}>Metric (gram, kilogram)</Col>
              </Row>
            </Col>
          </Row>

          <Row className="mt-1" componentClass={Validate} {...purchaseVolumeSystem}>
            <Col xs={11} xsOffset={1}>
              <hr className="green-hr"/>
              <h4>Default unit of volume for purchasing</h4>
              <Row>
                <Col md={1} xs={2}>
                <Radio
                  checked={purchaseVolumeSystem.value == 'imperial'}
                  name={'pVolume'}
                  onChange={this.purchaseVolumeChangeI}/>
                </Col>
                <Col xs={10} md={11}>Imperial (fluid ounce, pint, quart, gallon)</Col>
              </Row>
              <Row className="mt-1">
                <Col md={1} xs={2}>
                <Radio
                  checked={purchaseVolumeSystem.value == 'metric'}
                  name={'pVolume'}
                  onChange={this.purchaseVolumeChangeM}/>
                </Col>
                <Col xs={10} md={11}>Metric (milliliter, liter)</Col>
              </Row>
            </Col>
          </Row>

        </Row>
         <Row className="mt-1">
            <h4>Recipes Preferences</h4>
            <Row className="mt-1" >
              <Col xs={11} xsOffset={1}>
                <hr className="green-hr"/>
                <Checkbox {...hideAllRecipes}>
                    Don't display recipes when no category is selected
                </Checkbox>
              </Col>
            </Row>
            <Row className="mt-1" >
              <Col xs={11} xsOffset={1}>
                <hr className="green-hr"/>
                <h4>Mark recipes as new or updated if changed in the last:</h4>
                <Row>
                  <Col xs={1} xsOffset={1}>
                    <MaskedNumberInput
                      min={0}
                      normalizeInitial
                      precision={0} 
                      {...newRecipeThresholdDays}
                    />
                  </Col>
                  <Col xs={8} md={8}>
                    <h4 style={{margin: '5px 0'}}>days</h4>
                  </Col>
                </Row>
              </Col>
            </Row>
         </Row>

         <Row className="mt-1">
            <h4>Import Ingredients Preferences</h4>
            <Row className="mt-2" >
              <Col xs={11} xsOffset={1}>
                <hr className="green-hr"/>
                <h4>Threshold percentage of original price</h4>
                <Row>
                  <Col xs={1} xsOffset={1}>
                    <PercentInput
                      {...sourcePriceChangeWarningFraction}
                    />
                  </Col>
                  <Col xs={10} md={10}>%</Col>
                </Row>
              </Col>
            </Row>
         </Row>

        <Button type="submit"
              bsStyle="primary"
              className={(this.state.saving ? 'btn-info' : 'green-btn') + ' mt-1'}
              onClick={handleSubmit(this.saveData)}>
          Save
        </Button>
        <hr className="green-hr mt-2"/>
        <Onboarding pageKey="measure_settings" />
      </Col>
    );
  }

  componentWillMount () {
    const { router, route } = this.props;
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
    getOnboardingInfo();
  }
}


