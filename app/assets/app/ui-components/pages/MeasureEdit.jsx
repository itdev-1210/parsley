import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import ReactPropTypes from 'prop-types'
import { withRouter } from 'react-router';

import { fromJS } from 'immutable';

import { Row, Col, FormControl, Button } from 'react-bootstrap';
import { MaskedNumberInput } from '../MaskedInput';

import { history } from '../../navigation';

import CrudEditor from './helpers/CrudEditor';
import AddButton from '../AddButton';
import TitleInput from '../TitleInput';
import Checkbox from '../Checkbox';

import Validate from 'common/ui-components/Validate';

import { getMeasures, deleteMeasure, saveMeasure }
    from '../../webapi/endpoints.jsx';

import {
    checkFields, required, optional,
    deepArrayField, deepObjectField,
    positive, nonnegative, isBlank,
} from '../../utils/validation-utils';

import { deepOmit } from '../../utils/general-algorithms';
import { FieldArray } from '../../utils/redux-upgrade-shims';

const emptyMeasure = fromJS({
  'units': [{ 'size': 1, 'metric': true }],
});

function validate(values) {
  return checkFields(values, {
    name: required(),
    units: deepArrayField(deepObjectField({
      id: optional(), // hidden field, for pre-existing units
      name: required(),
      size: required(positive),
      abbr: optional(),
      scaleDownLimit: optional(),
      scaleUpLimit: optional(),
      precision: required(nonnegative),
      quantum: optional(nonnegative),
      metric: required(),
      commonPurchaseUnit: required(),
    })),
  });
}

@withRouter
@reduxForm(
    {
      form: 'measure-edit',
      fields: [
        'name',
        'allowsCustomNames',
        'units[].id',
        'units[].name',
        'units[].size',
        'units[].abbr',
        'units[].scaleDownLimit',
        'units[].scaleUpLimit',
        'units[].precision',
        'units[].quantum',
        'units[].metric',
        'units[].commonPurchaseUnit',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      onSubmitSuccess: () => history.push('/admin/measures'),
    },
    (state, origProps) => ({
      initialValues: origProps.params.id !== 'new' && state.hasIn(['measures', 'rawMeasures']) ?
          state.getIn(['measures', 'rawMeasures'])
              .find(m => m.get('id') === parseInt(origProps.params.id))
              .delete('id')
              .toJS() :
          emptyMeasure.toJS(),
      mainInfoLoaded: Boolean(state.getIn(['measures', 'measures', parseInt(origProps.params.id)])),
    })
)
export default class MeasureEdit extends CrudEditor {
  listUrl = '/admin/measures';
  noun = 'measure';

  deleteItem() {
    deleteMeasure(this.props.params.id);
  }
  handleSave(id, deets) {
    deets = deepOmit(deets, isBlank);
    for (let u of deets.units) {
      if (typeof (u.metric) != typeof (true)) {
        u.metric = (u.metric === 'true');
      }
    }
    return saveMeasure(id, deets);
  }

  renderBody() {
    const {
      name, allowsCustomNames, units,
    } = this.props.fields;
    return (
        <div>
          <Row><Col xs={12} md={6}>
            <TitleInput noun="Measure" {...name}
                        inputRef={input => this.initialInput = input}
            />
            <Checkbox {...allowsCustomNames}>Allows Custom names</Checkbox>
          </Col></Row>
          <FieldArray
            component={Units}
            fields={units}
          />
        </div>);
  }

  componentWillMount() {
    super.componentWillMount();
    if (this.sourceId() !== null)
      getMeasures();
  }
}

class Units extends Component {

  addUnit() {
    this.props.fields.addField({ metric: true });
  }
  removeUnit(i) {
    this.props.fields.removeField(i);
  }

  render() {

    return (<div>
          <h4>Units</h4>
          {this.props.fields.map((unit, index) => (
            <UnitRow key={index} isPrimary={index === 0}
                     fields={unit}
                     removeUnit={this.removeUnit.bind(this, index)}
            />
          ))}
          <AddButton onClick={this.addUnit.bind(this)} noun="unit" />
    </div>);
  }
}

class UnitRow extends Component {

  static propTypes = {
    fields: ReactPropTypes.object.isRequired,
    removeUnit: ReactPropTypes.func.isRequired,
    isPrimary: ReactPropTypes.bool.isRequired,
  };

  render() {
    // TODO: Allow changing of base unit for measure
    // TODO: More robust detection of base unit for input disabling

    let {
        fields: {
          name, size, abbr, metric, commonPurchaseUnit,
          scaleDownLimit, scaleUpLimit, precision, quantum,
        },
        isPrimary,
    } = this.props;

    return (
        <Row>
          <Col xs={12} md={2}>
            <Validate {...name}>
              <FormControl type="text" placeholder="Name" {...name} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...size}>
              <MaskedNumberInput placeholder="Size" {...size} disabled={isPrimary} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...abbr}>
              <FormControl type="text" placeholder="Abbr. (optional)" {...abbr} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...scaleDownLimit}>
              <MaskedNumberInput placeholder="Scale down limit" {...scaleDownLimit} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...scaleUpLimit}>
              <MaskedNumberInput placeholder="Scale up limit" {...scaleUpLimit} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...precision}>
              <MaskedNumberInput placeholder="Precision" {...precision}
                value={precision.value || '0'} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...quantum}>
              <MaskedNumberInput placeholder="Round to increment" {...quantum} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...metric}>
              <FormControl componentClass="select" {...metric}>
                <option value>Metric</option>
                <option value={false}>Imperial</option>
              </FormControl>
            </Validate>
          </Col>
        <Col xs={12} md={2}>
          <Checkbox {...commonPurchaseUnit}>
            Common Purchase
          </Checkbox>
        </Col>
          <Col xs={12} md={1}>
            <Button bsStyle="warning" disabled={isPrimary} onClick={this.props.removeUnit}>
              delete
            </Button>
          </Col>
        </Row>);
  }
}
