import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import ReceivingFlow from '../ReceivingFlow';

import {
  Button, Grid, Row, Col,
  ListGroup, ListGroupItem,
} from 'react-bootstrap';

import moment from 'moment';

import { history } from '../../navigation';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Onboarding from '../Onboarding';
import UploadCsvModal from '../modals/UploadCsvModal';

import { supplierIdToName } from './SupplierList';

import {
  getOrderList,
  getSupplierList,
  getPurchaseList,
  getOnboardingInfo,
} from '../../webapi/endpoints';
import { dateWithWeekdayFormat, timeFormat, dateTimeShortFormat } from 'common/utils/constants';

import { actionCreators as PurchaseOrderListActions } from '../../reducers/uiState/purchaseOrderList';
import { actionCreators as ImportReceiptsStateActions } from '../../reducers/importReceiptsDetails';

const getPurchasedTime = po => moment(po.get("emailedAt") ? po.get("emailedAt") : po.get("printedAt"));

@connect(
  state => ({
    orders: state.get('orders'),
    suppliers: state.get('suppliers'),
    purchaseOrders: state.get('purchaseOrders') && state.get('purchaseOrders').sort((a, b) => {
      if (getPurchasedTime(a).isBefore(getPurchasedTime(b))) {
        return 1;
      } else return -1;
    }),
    uiState: state.get('purchaseOrderList'),
    selectedSuppliers: state.getIn(['purchaseOrderList', 'selectedSuppliers']),
  }),
  dispatch => ({
    purchaseOrderListActions: bindActionCreators(PurchaseOrderListActions, dispatch),
    importReceiptsActions: bindActionCreators(ImportReceiptsStateActions, dispatch),
  }),
)
export default class PurchaseOrderList extends Component {

	render() {
    const {
      location, uiState, importReceiptsActions, selectedSuppliers,
      orders, suppliers, purchaseOrders,
      purchaseOrderListActions,
    } = this.props;

    const leftSpacerOptions = { sm: 2, xs: 12 };
    const mainBodyOptions = { sm: 10, xs: 12 };

    if (!(orders && suppliers && purchaseOrders)) {
      return <Row>
        <Col {...leftSpacerOptions}/>
        <Col {...mainBodyOptions}><LoadingSpinner/></Col>
      </Row>;
    }

      return <Grid fluid style={{ position: 'relative' }}>
      <FeatureGuardedButton
        style={
          /* zIndex needs to be high enough for the button to be clickable
            and low enough to be below popup modals */
          { position: 'absolute', right: '395px', top: '12px', zIndex: 1020 }
        }
        onClick={purchaseOrderListActions.openReceiptsImportModal}
        feature="IMPORT_RECEIPTS"
      >
        Import Receipts
      </FeatureGuardedButton>
      <FeatureGuardedButton
        style={
          { position: 'absolute', right: '268px', top: '12px', zIndex: 1020 }
        }
        onClick={purchaseOrderListActions.showReceiving}
        feature="RECEIVE_DIRECT"
      >
        Receive Direct
      </FeatureGuardedButton>
      {uiState.get('showReceiving')
        ? <ReceivingFlow
          hide={purchaseOrderListActions.hideReceiving}
          display={uiState.get('showReceiving')}
          />
        : null
      }
      <div>
      <Row>

        <Col {...leftSpacerOptions} />
        <Col {...mainBodyOptions}>
          <Row>
            <Col className="title-block" md={3} sm={6} xs={6}>
              <h3 className="list-title" style={{marginBottom: "38px"}}>Receiving</h3>
            </Col>
            <div>
            </div>
          </Row>
        </Col>
      </Row>
      <Row style={{paddingTop: "48px"}}>
        <Col {...leftSpacerOptions} style={{paddingTop: "8px"}}>
          <div className='leftcol' key='tags-toggles'>
            <h4>Suppliers</h4>
            {
              suppliers.filter(
                s => !s.get('tombstone'),
              ).map(s => {
                const isSupplierActive = selectedSuppliers.includes(s.get('id'));
                return <Button
                  key={s.get('name')}
                  bsStyle='info'
                  className='tagFilter'
                  active={isSupplierActive}
                  onClick={() => {
                    if (isSupplierActive) {
                      purchaseOrderListActions.uncheckItem(s.get('id'));
                    } else {
                      purchaseOrderListActions.checkItem(s.get('id'));
                    }
                  }}>
                  {s.get('name')}
                </Button>
              })
            }
          </div>
        </Col>
        <Col {...mainBodyOptions}>
          <div className='purchase-list-body'>
            <div className='purchase-list' style={{marginTop: "0px"}}>
              <ListGroup style={{width: "77%"}}>
                {
                  // TODO: fix link color
                  purchaseOrders
                    .filter(po => selectedSuppliers.isEmpty() || selectedSuppliers.includes(po.get('supplier')))
                    .map(po => {
                      const isReceived = po.get('receivedAt') || po.get('receivedDirectly');
                      const isImported = po.get('isImported');
                      const isReceivedDirectly = po.get('receivedDirectly');
                      let supplierName = `${supplierIdToName(po.get('supplier'), suppliers)}`;

                      let orderDetail;
                      if (po.get('emailedAt')) {
                        orderDetail = ` - Order Emailed on ${moment(po.get('emailedAt')).format(dateTimeShortFormat)}`;
                      } else if (po.get('printedAt')) {
                        orderDetail = ` - Order Printed on ${moment(po.get('printedAt')).format(dateTimeShortFormat)}`;
                      } else {
                        orderDetail = `Received on ${moment(po.get('importedAt')).format(dateTimeShortFormat)}`;
                      }

                      orderDetail += ` by ${po.get('creatorFirstName')} ${po.get('creatorLastName')}`;
                      if (isReceivedDirectly)
                        orderDetail = '';
                      return <ListGroupItem key={po.get('id')} className='purchase-list-item' style={isReceived ? {marginBottom: '14px'} : null}>
                        <div className='container-fluid'>
                          <Link id={po.get('id')} to={{pathname: location.pathname + '/' + po.get('id')}}>
                            {
                              isImported ?
                              <Row style={{ marginBottom: '3px' }}>
                                <Col xs="12">
                                  <span>
                                    <b>{supplierName} - </b>
                                    <span className="purchase-list-item-received" style={{ marginLeft: '4px', position: 'absolute' }}>
                                      <span className="list-item-check"/>
                                      <span style={{ marginLeft: '14px' }}>{orderDetail}</span>
                                    </span>
                                  </span>
                                </Col>
                              </Row>
                              :
                              <React.Fragment>
                                <Row style={{ marginBottom: '3px' }}>
                                  <Col xs="12">
                                    {
                                      isReceived
                                        ? <div>
                                            <b>{supplierName}</b>
                                            <span>{orderDetail}</span>
                                          </div>
                                        : <Link id={po.get('id')} className="list-item-main-link" to={{pathname: location.pathname + '/' + po.get('id')}}>
                                            <b>{supplierName}</b>
                                            <span>{orderDetail}</span>
                                          </Link>
                                    }
                                  </Col>
                                </Row>
                                {isReceived && <Row>
                                  <Col xs={12}>
                                    <span className="purchase-list-item-received">
                                      <span className="list-item-check"/>
                                      Received on {moment(po.get('receivedAt')).format(dateWithWeekdayFormat)} at {`${moment(po.get('receivedAt')).format(timeFormat)} `}
                                      by {`${po.get('receiverFirstName')} ${po.get('receiverLastName')}`}
                                    </span>
                                  </Col>
                                </Row>}
                              </React.Fragment>
                            }
                          </Link>
                        </div>
                      </ListGroupItem>
                    })
                }
              </ListGroup>
            </div>
          </div>
        </Col>
      </Row>
      </div>
      <Onboarding pageKey="receiving_list" />
      {
        uiState.get('showImportReceiptsModal') ?
          <UploadCsvModal
            title="Import POS Sales Data"
            onHide={purchaseOrderListActions.closeReceiptsImportModal}
            onUpload={(headers, data) => {
              importReceiptsActions.csvDetails(headers, data);
              purchaseOrderListActions.closeReceiptsImportModal();
              history.push('/receipts-import');
            }}
          />
      : null }
    </Grid>
	}

  componentWillMount() {
    getOrderList();
    getSupplierList();
    getPurchaseList();
    getOnboardingInfo();
  }

  componentWillUnmount() {
    const { location: currentLocation, router: { location: nextLocation }, purchaseOrderListActions } = this.props;
    if (!nextLocation.pathname.match(currentLocation.pathname)) {
      purchaseOrderListActions.clear();
    }
  }
}
