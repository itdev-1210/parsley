import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import moment from 'moment';
import Onboarding from '../Onboarding';

import SearchableList, { ScrollableLink, searchableListLoadingSpinner } from './helpers/SearchableList';
import { getInventoryList, getOnboardingInfo } from '../../webapi/endpoints';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import { dateWithWeekdayFormat } from 'common/utils/constants';

@connect(
  state => ({
    inventories: state.get('inventories'),
  }),
)
export default class InventoryList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      ingredientParsExists: false,
    };
    this.takeInventory = this.takeInventory.bind(this);
    this.currentInventory = this.currentInventory.bind(this);
  }

  takeInventory() {
    this.props.router.push(this.props.location.pathname + '/take');
  }

  currentInventory() {
    this.props.router.push(this.props.location.pathname + '/current');
  }

  renderSectionItem(item) {
    return <div style={{ fontSize: '19px', paddingBottom: '12px' }}>
      <ScrollableLink
        id={`list-item-inventory-${item.get('id')}`}
        item={item}
        shouldScroll={() => false}
        className="list-item-main-link"
        to={{
          pathname: 'inventories/' + item.get('id').toString(),
        }}>
        {moment(item.get('date')).format(dateWithWeekdayFormat)}
      </ScrollableLink></div>;
  }

  render() {
    const {
      inventories, location,
    } = this.props;

    if (!inventories) {
      return searchableListLoadingSpinner;
    }

    let takeInventoryLabel = 'Take Inventory';
    const today = moment().format('YYYY-MM-DD');
    if (!inventories.isEmpty() && inventories.first().get('date') === today) {
      takeInventoryLabel = 'Continue Taking Inventory';
    }

    const newButtons = [
      <Button key="take-inventory" id="search-new-btn-inventory" style={{ margin: '12px 0px' }} onClick={this.takeInventory}>
        {takeInventoryLabel}
      </Button>,
    ];

    return <Grid fluid style={{ position: 'relative' }}>
      <Onboarding pageKey="inventory_list" />
      { inventories.isEmpty() ? null
      : <FeatureGuardedButton feature="INVENTORY_SHRINKAGE" id="current-calculated-inventory-btn" onClick={this.currentInventory}>
          Current Expected Inventory
        </FeatureGuardedButton>
      }
      <SearchableList
        noun="Inventory"
        title="Inventory"
        searchKey="inventories"
        reduxStatePath="inventories"
        baseUrl={location.pathname}
        newButton={newButtons}
        shouldScroll={item => location.state && item.get('id') === location.state.prevObjId}
        renderSectionItem={this.renderSectionItem}
        groupReverseSort
      />
    </Grid>;
  }

  componentWillMount() {
    getInventoryList();
    getOnboardingInfo();
  }
}
