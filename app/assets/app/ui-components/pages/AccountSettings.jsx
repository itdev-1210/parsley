import { newEvent } from 'common/utils/analytics';
import {
  uncancel,
  unsubscribe
} from 'common/webapi/stripeEndpoints';
import _ from 'lodash';

import React, { Component } from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { history, hashLinkScroll } from '../../navigation';
import moment from 'moment';

import { featureIsSupported } from '../../utils/features';
import { checkFields, required } from '../../utils/validation-utils';
import {
  getUserInfo,
  saveUserInfo,
  saveLogo,
  getStripePublishableKey,
  getStripeSummary,
  getStripeProducts,
  getOnboardingInfo, resetOnboarding,
} from '../../webapi/endpoints';

import { post } from 'common/webapi/utils.jsx';

import {
  Button,
  ControlLabel, Form, FormControl, FormGroup, InputGroup,
  ListGroup, ListGroupItem,
  Grid, Col,
} from 'react-bootstrap';
import StripeCheckout from 'react-stripe-checkout';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';
import ConfirmedButton from '../ConfirmedButton';
import Section from '../Section';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import AddButton from '../AddButton';
import TextWithHelp from '../TextWithHelp';
import Validate from 'common/ui-components/Validate';
import { SwitchPlan, ConfirmPlan } from '../modals/SwitchSubscription';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { dateFormat } from 'common/utils/constants';
import { noticeError } from 'common/utils/errorAnalytics';

const SOCIAL_PROVIDER_NAMES = {
  linkedin: 'LinkedIn',
  google: 'Google',
};

@connect(
    state => ({
      userInfo: state.get('userInfo'),
    })
)
export default class AccountSettings extends Component {

  constructor(props) {
    super(props);
    this.state = {
      step: 'main',
      selectedPlan: null,
    }
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.userInfo && this.props.userInfo) {
      hashLinkScroll(1500);
    }
  }

  render() {
    const {
      userInfo,
      router, route
    } = this.props;

    const showMainEditor = () => this.setState({step: 'main', selectedPlan: null});

    if (!userInfo) {
      return <Grid fluid><LoadingSpinner /></Grid>;
    }
    const passwordLogin = this.props.userInfo.get('logins').find(
        login => login.get('providerId') === 'credentials'
    );
    const socialLogins = this.props.userInfo.get('logins').filter(
        login => login.get('providerId') !== 'credentials'
    );

    let loginSettingSections = [];

    const displaySocialLogin = socialLogins.first();
    const socialLoginSection = displaySocialLogin ?
        <ListGroupItem header="Social Login" key="social">
          { SOCIAL_PROVIDER_NAMES[displaySocialLogin.get('providerId')] } account: { displaySocialLogin.get('email') }
        </ListGroupItem> :
        null;

    loginSettingSections.push(socialLoginSection);

    const echoedUserInfo = this.props.userInfo.delete('logins').delete('id');
    const commonPasswordFormProps = {
      echoedUserInfo,
      key: 'password',
    };
    const passwordLoginSection = <ListGroupItem header="Password Login" key="password">
      {passwordLogin ?
          <PasswordUpdateForm loginInfo={passwordLogin} {...commonPasswordFormProps}/> :
          <PasswordAdditionForm {...commonPasswordFormProps}/>}
    </ListGroupItem>;

    if (passwordLogin) {
      loginSettingSections.unshift(passwordLoginSection);
    } else {
      loginSettingSections.push(passwordLoginSection);
    }

    let stepContent;
    switch(this.state.step) {
      case 'show-plans':
        stepContent = <SwitchPlan
          userInfo={userInfo}
          hide={showMainEditor}
          showConfirmPlan={(selectedPlan, proRationPreview) => this.setState({ step: 'confirm-plan', selectedPlan, proRationPreview })}
        />;
        break;
      case 'confirm-plan':
        stepContent = <ConfirmPlan
            hide={showMainEditor}
            selectedPlan={this.state.selectedPlan}
            proRationPreview={this.state.proRationPreview}
        />;
        break;
      default:
        stepContent = null;
        break;
    }
    return <div className="account-settings">
      <h3>Your Account</h3>
      <UserInfoEditor router={router} route={route}/>
      {!userInfo.get('isReadOnly') && !userInfo.get('isSharedUser') ?
        <PaymentEditor onChangeSubscriptionClick={() => this.setState({step: 'show-plans', selectedPlan: null})} />
      : null}
      <Section title="Login Methods">
        <ListGroup>
          {loginSettingSections}
        </ListGroup>

        {stepContent}
      </Section>
      <FeatureGuardedButton feature="ONBOARDING_RESET" onClick={resetOnboarding}>
        Reset Onboarding Progress
      </FeatureGuardedButton>
      <Onboarding pageKey="account_settings" />
    </div>;
  }

  componentWillMount() {
    getOnboardingInfo();
  }
}

function validateUpdatePassword(values) {
  let errors = {};

  errors = checkFields(values,
      {
        currentPassword: required(),
        newPassword: required(),
        confirmPassword: val => values.confirmPassword ? null :
            'Required (measure twice, cut once!)',
      }
  );

  if (!errors.confirmPassword && values.confirmPassword !== values.newPassword) {
    errors.confirmPassword = 'Password does not match';
  }

  return errors;
}

@reduxForm(
    {
      form: 'updatePassword',
      fields: ['currentPassword', 'newPassword', 'confirmPassword'],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: validateUpdatePassword,
      onSubmitSuccess: () => history.goBack(),
    },
    state => ({
      userEmail: state.getIn(['userInfo', 'email']),
    })
)
class PasswordUpdateForm extends Component {
  save(values) {
    const updateCredentialsRequest = Immutable.fromJS(values).delete('confirmPassword');
    return saveUserInfo(this.props.echoedUserInfo
        .set('credentialUpdates', updateCredentialsRequest)
        .toJS()
    ).then(() => {
      this.props.resetForm();
    }).catch(
        e => Promise.reject(e.reduxFormErrors || { _error: e.message })
    ).then(getUserInfo);
  }

  render() {
    const {
        fields: {
          currentPassword, newPassword, confirmPassword,
        },
        handleSubmit,
        submitting,
        loginInfo,
    } = this.props;

    // TODO
    return <Form horizontal>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Login E-mail</Col>
        <Col xs={8} componentClass={FormControl.Static}>
          <strong>{loginInfo.get('providerKey')}</strong>
        </Col>
      </FormGroup>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Current Password</Col>
        <Col xs={8} componentClass={Validate} {...currentPassword}>
          <FormControl type="password" {...currentPassword} />
        </Col>
      </FormGroup>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>New Password</Col>
        <Col xs={8} componentClass={Validate} {...newPassword}>
          <FormControl type="password" {...newPassword} />
        </Col>
      </FormGroup>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Confirm Password</Col>
        <Col xs={8} componentClass={Validate} {...confirmPassword}>
          <FormControl type="password" {...confirmPassword} />
        </Col>
      </FormGroup>
      <Button disabled={submitting} type="submit"
              onClick={handleSubmit(this.save.bind(this))}
              bsStyle="primary"
              className="green-btn">
        Change Password
      </Button>
    </Form>;
  }
}

function validateAddPassword(values) {
  let errors = {};

  errors = checkFields(values,
      {
        password: required(),
        confirmPassword: val => values.confirmPassword ? null :
            'Required (measure twice, cut once!)',
      }
  );

  if (!errors.confirmPassword && values.confirmPassword !== values.password) {
    errors.confirmPassword = 'Password does not match';
  }

  return errors;
}

@reduxForm(
    {
      form: 'addPassword',
      fields: ['password', 'confirmPassword'],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: validateAddPassword,
      onSubmitSuccess: () => history.goBack(),
    },
    state => ({
      userEmail: state.getIn(['userInfo', 'email']),
    })
)
class PasswordAdditionForm extends Component {
  save(values) {
    const addCredentialsRequest = Immutable.fromJS(values).delete('confirmPassword');
    return saveUserInfo(this.props.echoedUserInfo
        .set('newCredentials', addCredentialsRequest)
        .toJS()
    ).catch(
        e => Promise.reject(e.reduxFormErrors || { _error: e.message })
    ).then(this.props.resetForm).then(getUserInfo);
  }

  render() {
    const {
        fields: {
            password, confirmPassword,
        },
        userEmail,
        handleSubmit,
        submitting,
    } = this.props;

    return <Form horizontal>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Login E-mail</Col>
        <Col xs={8} componentClass={FormControl.Static}>
          <strong>{userEmail}</strong>
        </Col>
      </FormGroup>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Password</Col>
        <Col xs={8} componentClass={Validate} {...password}>
          <FormControl type="password" {...password} />
        </Col>
      </FormGroup>
      <FormGroup>
        <Col xs={4} componentClass={ControlLabel}>Confirm Password</Col>
        <Col xs={8} componentClass={Validate} {...confirmPassword}>
          <FormControl type="password" {...confirmPassword} />
        </Col>
      </FormGroup>
      <Button disabled={submitting} type="submit"
              onClick={handleSubmit(this.save.bind(this))}
              bsStyle="primary"
              className="green-btn">
        Add Password Login
      </Button>
    </Form>;
  }
}

@reduxForm(
    {
      form: 'userInfoEditor',
      fields: [
        'firstName',
        'lastName',
        'businessName',
        'companyLogo', // in network terms read-only; written through separate API endpoint
        'phoneNumber',
        'shippingAddress',
        'ccEmails[]',
        'printPreference.orderIncludePreps',
        'printPreference.orderIncludeRecipes',
        'printPreference.orderIncludeSubRecipes',
        'printPreference.printSubRecipePreferences',
        'printPreference.breakPageOnRecipePrint',
        'printPreference.recipePrintFormatPreference',
        'printPreference.includePhotoInRecipePrint',
        'printPreference.includeNutritionInMultipleRecipePrint',
        'printPreference.includeCostBreakdownInMultipleRecipePrint',
        'hideAllRecipes',
        'sourcePriceChangeWarningFraction',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      onSubmitSuccess: () => history.goBack(),
    },
    state => ({
      initialValues: state.get('userInfo').toJS(),
      companyLogoUrl: state.getIn(['userInfo', 'companyLogo']) ?
            state.getIn(['userInfo', 'logoUrl']) + state.getIn(['userInfo', 'companyLogo'])
            : null,
      readonlyUser: state.getIn(['userInfo', 'isReadOnly']),
    })
)
class UserInfoEditor extends CrudEditor {

  constructor(props) {
    super(props);
    this.state = {
      imagePreviewUrl: props.companyLogoUrl,
    };
    this.handleImageChange = this.handleImageChange.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.companyLogoUrl !== nextProps.companyLogoUrl) {
      // going to assume that this clobbers the input value of the company logo,
      // e.g. on successful save
      this.setState({
        imagePreviewUrl: nextProps.companyLogoUrl,
      });
      if (this.fileInputRef) {
        this.fileInputRef.value = null;
      }
    }
  }

  save(values) {
    let returnPromise = saveUserInfo(_.omit(values, 'companyLogo'))
        .catch(e => Promise.reject(e.reduxFormErrors | { _error: e.message }));

    if (values.companyLogo !== this.props.fields.companyLogo.initialValue) {
      let postLogo = new FormData();

      if (values.companyLogo) {
        postLogo.append('companyLogo', values.companyLogo, values.companyLogo.name);
      } // else postLogo is empty, which clears logo

      returnPromise = returnPromise.then(() => saveLogo(postLogo));
    }

    returnPromise = returnPromise.then(this.props.resetForm).then(getUserInfo);

    return returnPromise;
  }

  handleImageChange(e) {
    e.preventDefault();

    const {
      touch,
      fields: { companyLogo }
    } = this.props;

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        imagePreviewUrl: reader.result,
      });

      companyLogo.onChange(file);
      touch(companyLogo.name);
    };

    reader.readAsDataURL(file);
  }

  handleImageClear() {

    this.setState({
      imagePreviewUrl: undefined,
    });

    this.props.fields.companyLogo.onChange(null);
    if (this.fileInputRef) { // HTML file input doesn't respect null value passed in attrs
      this.fileInputRef.value = null;
    }
  }

  render() {
    const {
      fields: {
        firstName,
        lastName,
        businessName,
        companyLogo,
        shippingAddress,
        phoneNumber,
        ccEmails,
      },
      handleSubmit,
      submitting,
      readonlyUser,
    } = this.props;

    const adaptFileEventToValue = delegate =>
      e => delegate(e.target.files[0]);

    let { imagePreviewUrl } = this.state;
    let imagePreview = imagePreviewUrl &&
      <img style={{ marginTop: '12px', maxWidth: '240px', maxHeight: '240px' }} src={imagePreviewUrl} />;

    return <Section title="Info" className="form form-horizontal">
      <Section title="Personal">
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>First Name</Col>
          <Col xs={9} componentClass={Validate} {...firstName}>
            <FormControl type="text" autoComplete="off" disabled={readonlyUser} {...firstName} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Last Name</Col>
          <Col xs={9} componentClass={Validate} {...lastName}>
            <FormControl type="text" autoComplete="off" disabled={readonlyUser} {...lastName} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={4} componentClass={ControlLabel}>Phone Number</Col>
          <Col xs={8} componentClass={Validate} {...phoneNumber}>
            <FormControl type="tel" autoComplete="off" {...phoneNumber} />
          </Col>
        </FormGroup>
      </Section>
      <Section title="Business">
        <FormGroup>
          <Col xs={4} componentClass={ControlLabel}>Business Name</Col>
          <Col xs={8} componentClass={Validate} {...businessName}>
            <FormControl type="text" disabled={readonlyUser} {...businessName} />
          </Col>
        </FormGroup>
            <FormGroup>
              <Col xs={4} componentClass={ControlLabel}>Company Logo</Col>
              <Col xs={8} componentClass={Validate} {...companyLogo}>
                <form id="logo" encType="multipart/form-data">
                  {!readonlyUser ?
                    <AddButton noun="logo" feature="GRAY_LOGO" onClick={() => {
                      if (this.fileInputRef) this.fileInputRef.click();
                    }} />
                  : null}
                  <FormControl style={{ display: 'none' }} type="file" inputRef={ref => this.fileInputRef = ref}
                               accept='image/*'
                               {...companyLogo}
                               onChange={this.handleImageChange.bind(this)}
                               onBlur={null}
                               value={null}
                  />
                </form>
                {imagePreview}

                {!readonlyUser && companyLogo.value
                    ? <div><Button bsStyle="link" onClick={this.handleImageClear.bind(this)}>
                      remove logo
                    </Button></div>
                    : null
                }
              </Col>
            </FormGroup>
        <FormGroup>
          <Col xs={4} componentClass={ControlLabel}>Shipping Address</Col>
          <Col xs={8} componentClass={Validate} {...shippingAddress}>
            <FormControl componentClass="textarea" rows={4}
                         disabled={readonlyUser}
                         autoComplete="off"
                         {...shippingAddress}
            />
          </Col>
        </FormGroup>
        {!readonlyUser ? <FormGroup>
          <Col xs={4}>
            <TextWithHelp helpText={
              'These will receive notifications and copies of activity, such as ' +
              'purchase orders, inventory checks, and prep shifts'
            }>
              <ControlLabel>Other Emails</ControlLabel>
            </TextWithHelp>
          </Col>
          <FieldArray
              fields={ccEmails}
              component={CcEmails}
          />
        </FormGroup>
        : null}
      </Section>
      {!readonlyUser ?
        <Button disabled={submitting} type="submit"
                onClick={handleSubmit(this.save.bind(this))}
                bsStyle="primary"
                className="green-btn">
          Save
        </Button>
      : null}
    </Section>;
  }
}

class CcEmails extends Component {
  render() {
    const { fields } = this.props;
    return <Col xs={8}>
      {fields.map((email, i) =>
          <Validate {...email} key={i}>
            <FormControl type="email" {...email}/>
            <InputGroup.Addon className="delete-link" onClick={() => fields.removeField(i)}>
              delete
            </InputGroup.Addon>
          </Validate>
      )}
      <AddButton noun="email" onClick={() => fields.addField('')} />
    </Col>;
  }
}

@connect(
    state => ({
      userInfo: state.get('userInfo'),
      stripePublishableKey: state.getIn(['stripeInfo', 'publishableKey']),
      stripeSummary: state.getIn(['stripeInfo', 'summary']),
      stripeProducts: state.getIn(['stripeInfo', 'products']),
    })
)
class PaymentEditor extends Component {

  componentDidMount() {
    getUserInfo();
    getStripePublishableKey();
    getStripeSummary();
    getStripeProducts();
  }

  subscribe = (token) => {

    let customerInfo = this.props.userInfo;
    if (token) {
      newEvent('add_payment_info', {});
      customerInfo = customerInfo.set('token', token.id);
    }

    return post('/plan/paymentmethod', customerInfo).then(response => {
        getStripeSummary();
    });
  }

  render() {
    const {
        stripePublishableKey,
        stripeSummary,
        userInfo,
        onChangeSubscriptionClick,
    } = this.props;

    let savedCard = stripeSummary && stripeSummary.get('card');
    const activeProduct = stripeSummary && stripeSummary.get('activeProduct');
    const activePlan = activeProduct && activeProduct.getIn(['plans', 0]);

    let activeCoupon = stripeSummary && stripeSummary.get('coupon');

    let discountDescription = '';
    let discountedCentsAmount = activePlan && activePlan.get('price');
    if (activeCoupon) {
      if (activeCoupon.has('percent_off')) {
        const percentDiscount = activeCoupon.get('percent_off');
        discountDescription = `${activeCoupon.get('percent_off')}%`;
        // for dollar amounts, assuming that stripe won't return to us anything NEAR Number.MAX_SAFE_INTEGER
        discountedCentsAmount = discountedCentsAmount * (100 - percentDiscount) / 100;
      } else if (activeCoupon.has('amount_off')) {
        const centsOff = activeCoupon.get('amount_off');
        discountDescription = `\$${centsOff / 100}`;
        discountedCentsAmount = Math.max(0, discountedCentsAmount - centsOff);
      } else {
        noticeError(`Stripe discount without percent_off or amount_off for user ${userInfo.get('id')}`);
      }
    }

    const periodEnd = stripeSummary && moment(stripeSummary.get('periodEnd'), moment.ISO_8601);
    const periodEndString = periodEnd && periodEnd.format(dateFormat);

    const isCancelled = stripeSummary && stripeSummary.get('isCancelled');

    const cancelSubscriptionText = [
        <p key="1">
          Are you sure you want to cancel your subscription?
          Your cancellation will be effective at the end of your next billing period
          ({periodEndString}). Your data will be saved in case you
          re-open your account.
        </p>,
        <p key="2">
          We'll be sad to see you go!
        </p>,
    ];
    const uncancelSubscriptionText = <p>
      Your subscription will no longer be cancelled, and your card will be billed on ({periodEndString})
    </p>;

    const cancelSubscription = () => unsubscribe().then(getStripeSummary);
    const uncancelSubscription = () => uncancel().then(getStripeSummary);

    return <Section title="Payment" className="form form-horizontal" id="payment">
                { stripePublishableKey && stripeSummary && activeProduct && activePlan ?
                  <div>
                    <h5>Subscription Plan:</h5>
                    { activeProduct.get('name') }: ${ activePlan.get('price') / 100.0 }/month
                    { discountDescription && ` (with ${discountDescription} off: \$${discountedCentsAmount / 100.0}/month)`}
                    { isCancelled ? ', ending on' : ', next charge on' } {periodEndString}

                    <br />
                    <Button onClick={onChangeSubscriptionClick}>Change Subscription Level</Button>
                    <br />
                    <br />
                    <h5>Card Info:</h5>
                    { savedCard ?
                      savedCard.get('brand') + ' ending in ' + savedCard.get('last4')
                      : 'Please enter a payment method' }
                    <br /><br />
                    <div className="btn-row">
                      <StripeCheckout
                          name="Parsley"
                          description="Update payment method"
                          panelLabel="Save"
                          currency="USD"
                          token={this.subscribe}
                          email={userInfo.get('email')}
                          allowRememberMe={false}
                          stripeKey={stripePublishableKey}>
                        <Button>
                          { savedCard ? 'Change' : 'Add' } payment method
                        </Button>
                      </StripeCheckout>
                      { isCancelled ?
                          <ConfirmedButton
                              confirmButtonStyle="success" modalText={uncancelSubscriptionText}
                              modalTitle="Restart Subscription" modalButtonText="Restart Subscription"
                              onClick={uncancelSubscription}
                          >
                            Restart Subscription
                          </ConfirmedButton> :
                          <ConfirmedButton
                              bsStyle="danger" confirmButtonStyle="danger" modalText={cancelSubscriptionText}
                              modalTitle="Cancel Subscription" modalButtonText="Cancel Subscription"
                              onClick={cancelSubscription}
                          >
                            Cancel Subscription
                          </ConfirmedButton>
                      }
                    </div>
                  </div>
                : <LoadingSpinner /> }
        </Section>;
  }
}
