// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { withRouter } from 'react-router';
import { history } from '../../navigation';
import { connect } from 'react-redux';
import ImportReceivablesPricing from './ImportReceivablesPricing';
import { importReceivablesPricing } from '../../webapi/endpoints';

type ImportReceivablesProps = {
  details: Immutable.List<Immutable.Map<string, any>>,
};

@withRouter
@connect(
  state => {
    const details = state.get('importReceivablesPricingDetails');
    return {
      details,
    };
  },
)
export default class ImportReceivables extends Component<ImportReceivablesProps> {

  onSaveSuppliers = (supplierData: {id: number, sources: Array<*>}) => importReceivablesPricing(supplierData);

  render() {
    const { details, ...rest } = this.props;
    return <div>
      {/* $FlowFixMe: can't simulate extra props passed in by decorators */}
      <ImportReceivablesPricing
        title="Import Receivables Pricing"
        details={details}
        onSaveSuppliers={this.onSaveSuppliers.bind(this)}
        onFinishImport={() => history.push('/suppliers')}
        {...rest}
      />
    </div>;
  }

  componentDidMount() {
    const { details } = this.props;
    if (!details) history.push('/suppliers');
  }
}