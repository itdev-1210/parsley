import React, { Component } from 'react';
import Immutable from 'immutable';
import _ from 'lodash';

import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {
  Modal,
  Button, ControlLabel, Row, Col,
  FormControl, FormGroup, ButtonToolbar,
  Tabs, Tab,
} from 'react-bootstrap';
import naturalCompare from 'string-natural-compare';
import Onboarding from '../Onboarding';

import Section from '../Section';
import Checkbox from '../Checkbox';
import Select from '../Select';
import AddButton from '../AddButton';
import Validate from 'common/ui-components/Validate';
import isEmail from 'validator/lib/isEmail';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import ConfirmationModal, { DeleteConfirmationModal } from '../modals/ConfirmationModal';

import {
  getOwnerInfo,
  getUpstreamInfo,
  getSharedAccounts,
  sendUserInvite,
  leaveAccount,
  removeSharedUser,
  getOnboardingInfo,
  updateSharedAccount,
  getPendingSharedInvites,
  updatePendingShareInvite,
  removePendingShareInvite,
} from "../../webapi/endpoints";
import { getApiErrorMsg } from 'common/webapi/utils';
import { featureIsAvailable } from 'common/utils/features';

import { valueFromEvent } from '../../utils/form-utils';
import { PERMISSION_LEVELS } from 'common/utils/constants';
import { getUserInfo } from 'common/webapi/userEndpoints';

@connect(
  state => ({
    userInfo: state.get('userInfo'),
    userInvitation: state.get('userInvitation'),
    upstreamInfo: state.get('upstreamInfo'),
  }),
)
export default class UserManager extends Component {
  render() {

    const { userInfo, userInvitation, upstreamInfo } = this.props;

    let content;
    if (!userInfo) { // need to have userInfo to even know which case we fall under
      content = <LoadingSpinner />;
    } else if (!userInfo.get('isAccountOwner')) { // shared user
      content = <ContributionInfo
        name={userInfo.get('businessName')}
      />;
    } else if (!userInvitation) { // if accountOwner, need to wait for invitations info
      content = <LoadingSpinner />;
    } else {
      content = <UserSharedAccounts
          sharedAccounts={userInvitation.get('sharedAccounts')}
          isReadOnlyDisabled={!featureIsAvailable('READONLY_USER')}
          isOperationsDisabled={!featureIsAvailable('OPERATIONS_USER')}
      />;
    }
    return <div className='account-settings'>
      <h3>Manage Users</h3>
      {content}

      {upstreamInfo.get('info') && <div style={{marginTop: '50px'}}>
        <h3>Manage Multiple Location</h3>
        <Section title="Multi Location">
          <div>Central Account: {upstreamInfo.getIn(['info', 'name'])} ({upstreamInfo.getIn(['info', 'email'])})</div>
        </Section>
      </div>}

      <Onboarding pageKey="manage_users" />
    </div>;
  }

  componentWillMount() {
    getOwnerInfo();
    getUserInfo();
    getUpstreamInfo();
    getSharedAccounts();
    getOnboardingInfo();
  }
}

@reduxForm({
  form: 'contribution-info',
  fields: [],
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint)
})
class ContributionInfo extends Component {

  render() {
    const {name, submitting, handleSubmit} = this.props;

    return <Row>
      <Col xs={12}>You are contributing to {name}'s account</Col>
      <Col xs={12}>
        <ButtonToolbar className='pull-right'>
          <Button
            type='button' bsStyle='default'
            onClick={handleSubmit(this.handleLeaveAccount.bind(this))}
            disabled={submitting}
            >
            {submitting ? 'Leaving' : 'Leave'}
          </Button>
        </ButtonToolbar>
      </Col>
    </Row>
  }

  handleLeaveAccount = () => leaveAccount(this.props.id).then(
    resp => Promise.resolve(window.location = '/'),
    err => Promise.reject((err.readableErrors) ? err.readableErrors._error : err.error)
  )
}

function validateInviteUser(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (!isEmail(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.level) {
    errors.level = 'Required';
  }

  return errors;
}

@reduxForm(
  {
    form: 'user-invitation',
    fields: ['email', 'level'],
    validate: validateInviteUser,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: {
      level: ownProps.isReadOnlyDisabled && 'shared',
    }
  }),
)
class UserInvitation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formError: '',
      disableSubmit: false,
      invitationSent: false,
    }
  }

  save(values, {}, props) {
    return sendUserInvite(values).then(
      () => {
        props.onInvited(values.email, values.level);
        props.hide();
      },
      errors => {
        this.setState({
          formInfo: '',
          formError: errors.reduxFormErrors && errors.reduxFormErrors._error,
          disableSubmit: true,
        });
      }
    );
  }

  render() {

    const {
      fields: { email, level },
      handleSubmit,
      submitting,
      isReadOnlyDisabled,
      isOperationsDisabled,
      hide,
    } = this.props;
    let selectableOptions = collaboratorOptions;

    if (isReadOnlyDisabled) selectableOptions = selectableOptions && selectableOptions.filter(opt => opt.value !== 'recipe-read-only');
    if (isOperationsDisabled) selectableOptions = selectableOptions && selectableOptions.filter(opt => opt.value !== 'operations');

    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>Invite User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {(this.state.formError) ? <Row><Col className='crud-global-errors mb-1'>{this.state.formError}</Col></Row> : null}
        <Row>
          <FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Email</Col>
            <Col xs={9}>
              <Validate {...email}>
                <FormControl type='text' autoComplete="off" {...email} onChange={(evt) => {
                  this.setState({disableSubmit: false, invitationSent: false});
                  email.onChange(evt);
                }} />
              </Validate>
            </Col>
          </FormGroup>
        </Row>
        {selectableOptions.length > 1 && <Row className="mt-1"><FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Permission</Col>
            <Col xs={5}>
              <Validate {...level}>
                  <Select
                    {...level}
                    options={selectableOptions}
                    disabled={submitting}
                  />
                </Validate>
            </Col>
        </FormGroup></Row>}
      </Modal.Body>
      <Modal.Footer>
        <Button disabled={submitting || this.state.disableSubmit} type='submit' onClick={handleSubmit(this.save.bind(this))} >
          { submitting ? 'Sending' : this.state.invitationSent ? 'Sent' : 'Send' }
        </Button>
      </Modal.Footer>
    </Modal>;
  }
}

@reduxForm(
  {
    form: 'user-shared-accounts',
    fields: [
      'permissions[].userId',
      'permissions[].permission',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: {
      permissions: ownProps.sharedAccounts && ownProps.sharedAccounts
        .sort((valueA, valueB) => naturalCompare(valueA.get('email'), valueB.get('email')))
          .map(sa => ({
            userId: sa.get('sharedAccount'),
            permission: sa.get('permissionLevel'),
          })).toJS(),
    },
  }),
)
class UserSharedAccounts extends Component {

  constructor(props) {
    super(props);
    this.state = {
      formInfo: null,
      removingAccount: null,
      removedAccounts: [],
      removeInProgress: false,
      removeAccountError: null,
      updateSharedAccount: null,
      updatingSharedAccount: false,
      shouldShowInvitationModal: false,
    }

    this.update = this.update.bind(this);
    this.hideUpdateModal = this.hideUpdateModal.bind(this);
  }

  static defaultProps = {
    isReadOnlyDisabled: false,
    isOperationsDisabled: false,
  }

  openInvitationModal = () => this.setState(() => ({shouldShowInvitationModal: true}));

  hideModal = () => this.setState(() => ({
    shouldShowInvitationModal: false,
    shouldShowUpdateConfirmModal: false,
    updateSharedAccount: null,
    updatingSharedAccount: false,
  }));

  update(userId, permission) {
    return this.setState(() => ({updatingSharedAccount: true}), () => {
      return updateSharedAccount({userId, permission})
        .then(() => {
          this.hideModal();
        });
    });
  }

  hideUpdateModal() {
    const { updateSharedAccount } = this.state;
    const { permissions } = this.props.fields;
    const { permission } = _.first(permissions.filter(perm => perm.userId.value === updateSharedAccount));
    permission.onChange(permission.initialValue);
    this.hideModal();
  }

  render() {
    const {
      sharedAccounts, isReadOnlyDisabled, isOperationsDisabled,
      fields: { permissions },
    } = this.props;
    const {
      formInfo,
      removingAccount, removedAccounts, removeAccountError, removeInProgress,
      shouldShowInvitationModal,
      updateSharedAccount, updatingSharedAccount,
    } = this.state;
    let editablePermissions = permissions;
    let selectableOptions = collaboratorOptions;

    if (isOperationsDisabled) {
      editablePermissions = editablePermissions && editablePermissions.filter(perm => perm.permission.value !== 'operations');
      selectableOptions = selectableOptions.filter(opt => opt.value !== 'operations');
    }

    if (isReadOnlyDisabled) {
      editablePermissions = editablePermissions.filter(perm => perm.permission.value !== 'recipe-read-only');
      selectableOptions = selectableOptions.filter(opt => opt.value !== 'recipe-read-only');
    }

    const confirmUpdateModal = updateSharedAccount && <Modal show onHide={this.hideUpdateModal}>
      <Modal.Header closeButton>
        <Modal.Title>Confirm User Updates</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {
          _.first(editablePermissions.filter(perm => perm.userId.value === updateSharedAccount).map(perm => {
            const acc = sharedAccounts.filter(sa => sa.get('sharedAccount') === perm.userId.value).first();
            const permLabel = _.first(collaboratorOptions.filter(opt => opt.value === perm.permission.value)).label;
            return `Are you sure you want to change permission of ${acc.get('email')} to ${permLabel} ?`;
          }))
        }
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning"
          onClick={this.hideUpdateModal}
          disabled={updatingSharedAccount}
          >
          cancel
        </Button>
        <Button
          bsStyle="success"
          disabled={updatingSharedAccount}
          onClick={() => {
            const {userId, permission} = _.first(editablePermissions.filter(perm => perm.userId.value === updateSharedAccount));
            this.update(userId.value, permission.value);
          }}
          >
          Update
        </Button>
      </Modal.Footer>
    </Modal>

    return <div>
      {shouldShowInvitationModal && <UserInvitation
        isReadOnlyDisabled={isReadOnlyDisabled}
        isOperationsDisabled={isOperationsDisabled}
        hide={this.hideModal}
        onInvited={(invitedEmail, invitedLevel) => this.setState(() => ({formInfo: `Invitation Sent to ${invitedEmail}`}), getPendingSharedInvites)}
      />}
      {confirmUpdateModal}

      <div>
        {sharedAccounts && <Row className="mb-1 mt-1">
          <Col xs={6} md={2}>
            <ButtonToolbar className="pull-left">
              <AddButton
                noun="user"
                onClick={this.openInvitationModal}
              />
            </ButtonToolbar>
          </Col>
          <Col xs={6} md={10}>
            {formInfo && <div className="mt-1">{formInfo}</div>}
          </Col>
        </Row>}
        <Section title="Active Users">
          {
            !sharedAccounts ? <LoadingSpinner /> :
              (editablePermissions.size < 1) ?
                <p>Account shared with no users</p> :
                editablePermissions.map((perm, idx) => {
                  const { userId, permission } = perm;
                  const acc = sharedAccounts.filter(sa => sa.get('sharedAccount') === userId.value).first();
                  return <Row key={idx} className='mb-1 toggle-buttons'>
                    <Col xs={1} className="account-settings-index-col">{`${idx+1}.`}</Col>
                    <Col className={selectableOptions.length > 1
                        ? "col-xs-5 account-settings-email-col-5"
                        : "col-xs-9 account-settings-email-col-9"
                      }>
                      {acc.get('email')}
                    </Col>
                    {selectableOptions.length > 1 && <Col xs={4} className="account-settings-perm-col">
                      <Validate {...permission}>
                        <Select
                          {...permission}
                          options={selectableOptions}
                          disabled={removedAccounts.includes(acc.get('sharedAccount'))}
                          onChange={ev => {
                            permission.onChange(valueFromEvent(ev));
                            this.setState(() => ({
                              updateSharedAccount: userId.value,
                            }));
                          }}
                        />
                      </Validate>
                    </Col>}
                    <Col
                      xs={2}
                      className="account-settings-remove-col"
                      style={selectableOptions.length > 1 ? {} : { textAlign: 'right' }}
                      >
                      <Button
                        className="pull-right"
                        disabled={removedAccounts.includes(acc.get('sharedAccount'))}
                        onClick={() => this.setState({removingAccount: acc})}
                        >
                        { removedAccounts.includes(acc.get('sharedAccount')) ? 'Removed' : 'Remove' }
                      </Button>
                    </Col>
                  </Row>
                })
          }
        </Section>
      </div>

      <Section title='Pending Invites'>
        <UserPendingShareInvites
          isReadOnlyDisabled={isReadOnlyDisabled}
          isOperationsDisabled={isOperationsDisabled}
        />
      </Section>
      {
        removingAccount ?
          <DeleteConfirmationModal
            title='Remove User'
            deleteVerb='Confirm'
            deleteVerbParticle="Removing"
            onHide={() => this.setState({removingAccount : null, removeAccountError: null, removeInProgress: false})}
            onDelete={() => this.handleRemoveAccount(removingAccount.get('sharedAccount'))}
            longDescription={`${removingAccount.get('firstName')} ${removingAccount.get('lastName')} at ${removingAccount.get('email')}`}
            deleteError={removeAccountError}
            inProgress={removeInProgress}
          />
          : null
      }
    </div>
  }

  handleRemoveAccount(sharedAccountId) {
    const prevRemovedAccounts = [...this.state.removedAccounts];
    this.setState({removeInProgress: true});
    removeSharedUser(sharedAccountId).then(
        resp => {
          prevRemovedAccounts.push(sharedAccountId);
          this.setState({
            removingAccount: null,
            removedAccounts: prevRemovedAccounts,
            removeAccountError: null,
            removeInProgress: false,
          })
        },
        err => {
          this.setState({removeAccountError: getApiErrorMsg(err)})
        }
    )
  }
}

const collaboratorOptions = _.map(PERMISSION_LEVELS, (humanReadableName, internalName) => ({
  value: internalName, label: humanReadableName,
}));

@reduxForm(
  {
    form: 'user-pending-share-invites',
    fields: [
      'invites[].id',
      'invites[].inviteeLevel',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const pendingShareInvites = state.getIn(['pendingShareInvites', 'pendingShareInvites']);
    return {
      pendingShareInvites,
      initialValues: {
        invites: pendingShareInvites && pendingShareInvites
          .sort((invA, invB) => naturalCompare(invA.get('inviteeEmail'), invB.get('inviteeEmail')))
          .map(inv => ({
            id: inv.get('id'),
            inviteeLevel: inv.get('inviteeLevel'),
          })).toJS(),
      },
    }
  },
)
class UserPendingShareInvites extends Component {

  constructor(props) {
    super(props);
    this.state = {
      resentInvitations: [],
      removedInvitations: [],
      updatePendingInvitation: null,
      updatingPendingInvitation: false,
      updatePendingInvitationError: null,
      resendPendingInvitation: null,
      resendingPendingInvitation: false,
      resendPendingInvitationError: null,
      removePendingInvitation: null,
      removingPendingInvitation: false,
      removePendingInvitationError: null,
    }
    this.update = this.update.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.hideUpdateModal = this.hideUpdateModal.bind(this);
    this.resend = this.resend.bind(this);
  }

  update(id, inviteeLevel) {
    return this.setState(() => ({updatingPendingInvitation: true,}), () => {
      return updatePendingShareInvite({id, inviteeLevel, resendEmail: false}).then(
        this.hideModal,
        err => this.setState(() => ({updatePendingInvitationError: getApiErrorMsg(err)})),
      );
    })
  }

  resend(id) {
    const prevResentInvitations = [...this.state.resentInvitations];
    const { pendingShareInvites } = this.props;
    const { inviteeLevel } = pendingShareInvites
      .find(inv => inv.get('id') === id)
      .toJS();
    return this.setState(() => ({resendingPendingInvitation: true}), () => {
      return updatePendingShareInvite({id, inviteeLevel, resendEmail: true}).then(
        () => {
          prevResentInvitations.push(id);
          this.setState(() => ({resentInvitations: prevResentInvitations}), this.hideModal);
        },
        err => this.setState(() => ({resendPendingInvitationError: getApiErrorMsg(err)})),
      );
    });
  }

  remove(id) {
    const prevRemovedAccounts = [...this.state.removedInvitations];
    return this.setState(() => ({removingPendingInvitation: true}), () => {
      return removePendingShareInvite(id).then(
        () => {
          prevRemovedAccounts.push(id);
          this.setState(() => ({removedInvitations: prevRemovedAccounts}), () => {
            this.hideModal();
            const { invites } = this.props.fields;
            invites.removeField(invites.findIndex(inv => inv.id.value === id));
          });
        },
        err => this.setState(() => ({removePendingInvitationError: getApiErrorMsg(err)})),
      )
    });
  }

  hideModal = () => this.setState(() => ({
    updatePendingInvitation: null,
    updatingPendingInvitation: false,
    updatePendingInvitationError: null,
    removePendingInvitation: null,
    removingPendingInvitation: false,
    removePendingInvitationError: null,
    resendPendingInvitation: null,
    resendingPendingInvitation: false,
    resendPendingInvitationError: null,
  }));

  hideUpdateModal() {
    const { updatePendingInvitation } = this.state;
    const { invites } = this.props.fields;
    const { inviteeLevel } = _.first(invites.filter(inv => inv.id.value === updatePendingInvitation));
    inviteeLevel.onChange(inviteeLevel.initialValue);
    this.hideModal();
  }

  render() {
    const { pendingShareInvites, isReadOnlyDisabled, isOperationsDisabled } = this.props;
    const { invites } = this.props.fields;
    const {
      removedInvitations, resentInvitations,
      updatePendingInvitation, updatingPendingInvitation, updatePendingInvitationError,
      removePendingInvitation, removingPendingInvitation, removePendingInvitationError,
      resendPendingInvitation, resendingPendingInvitation, resendPendingInvitationError,
    } = this.state;
    // operations exists only db not enabled
    let editableInvites = invites;
    let selectableOptions = collaboratorOptions;

    if (isOperationsDisabled) {
      editableInvites = editableInvites && editableInvites.filter(inv => inv.inviteeLevel.value !== 'operations');
      selectableOptions = selectableOptions.filter(opt => opt.value !== 'operations');
    }

    if (isReadOnlyDisabled) {
      editableInvites = editableInvites && editableInvites.filter(inv => inv.inviteeLevel.value !== 'recipe-read-only');
      selectableOptions = selectableOptions.filter(opt => opt.value !== 'recipe-read-only');
    }

    const confirmUpdateModal = updatePendingInvitation && <Modal show onHide={this.hideUpdateModal}>
      <Modal.Header closeButton>
        <Modal.Title>Confirm Invitation Updates</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {
          _.first(editableInvites.filter(inv => inv.id.value === updatePendingInvitation).map(inv => {
            const pendingInv = pendingShareInvites.filter(psi => psi.get('id') === inv.id.value).first();
            const newInviteLevel = _.first(collaboratorOptions.filter(opt => opt.value === inv.inviteeLevel.value)).label;
            return `Are you sure you want to change permission of invitaton for ${pendingInv.get('inviteeEmail')} to ${newInviteLevel}`;
          }))
        }
        {updatePendingInvitationError && <p className="text-danger">{updatePendingInvitationError}</p>}
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning"
          onClick={this.hideUpdateModal}
          disabled={updatingPendingInvitation}
          >
          cancel
        </Button>
        <Button bsStyle="success"
          disabled={updatingPendingInvitation}
          onClick={() => {
            const { id, inviteeLevel } = _.first(editableInvites.filter(inv => inv.id.value === updatePendingInvitation));
            this.update(id.value, inviteeLevel.value);
          }}
          >
          Update
        </Button>
      </Modal.Footer>
    </Modal>

    return <div>
      {confirmUpdateModal}
      {removePendingInvitation && <DeleteConfirmationModal
        title="Remove Pending Invitation"
        deleteVerb="Confirm"
        deleteVerbParticle="Removing"
        onHide={this.hideModal}
        onDelete={() => this.remove(removePendingInvitation)}
        fullDescription={
          pendingShareInvites
            .filter(inv => inv.get('id') === removePendingInvitation)
            .map(inv => `Are you sure you want to remove invitation sent to ${inv.get('inviteeEmail')}?`)
            .first()
        }
        deleteError={removePendingInvitationError}
        inProgress={removingPendingInvitation}
      />}
      {
        !pendingShareInvites
          ? <LoadingSpinner />
          : (pendingShareInvites.size < 1)
              ? <p>No Invitations Sent</p>
              : editableInvites.map((inv, idx) => {
                const { id, inviteeLevel } = inv;
                const pendingInv = pendingShareInvites.filter(psi => psi.get('id') === id.value).first();
                if (!pendingInv) return <Row><LoadingSpinner /></Row>;

                return <Row key={idx} className="mb-1 mt-1">
                  <Col xs={1} className="account-settings-index-col">{`${idx+1}.`}</Col>
                  <Col className={selectableOptions.length > 1
                      ? "col-xs-4 pending-invites-email-col-4"
                      : "col-xs-7 pending-invites-email-col-7"
                    }>
                    {pendingInv.get('inviteeEmail')}
                  </Col>
                  {selectableOptions.length > 1 && <Col xs={4} className="account-settings-perm-col">
                    <Validate {...inviteeLevel}>
                      <Select
                        {...inviteeLevel}
                        options={selectableOptions}
                        disabled={removedInvitations.includes(id.value)}
                        onChange={ev => {
                          inviteeLevel.onChange(valueFromEvent(ev));
                          this.setState(() => ({
                            updatePendingInvitation: id.value,
                          }));
                        }}
                      />
                    </Validate>
                  </Col>}
                  <Col
                    xs={2}
                    className="account-settings-remove-col"
                    >
                    <Button
                      disabled={resentInvitations.includes(id.value) || resendPendingInvitation === id.value}
                      onClick={() => {
                        this.setState(
                          () => ({resendPendingInvitation: id.value}),
                          () => this.resend(id.value),
                        );
                      }}>
                      {
                        resendingPendingInvitation && resendPendingInvitation === id.value
                          ? 'Sending'
                          : resentInvitations.includes(id.value)
                              ? 'Sent'
                              : 'Resend'
                      }
                    </Button>
                  </Col>
                  <Col
                    xs={2}
                    className="account-settings-remove-col"
                    style={!isReadOnlyDisabled ? {} : { textAlign: 'right' }}
                    >
                    <Button
                      className="pull-right"
                      disabled={removedInvitations.includes(id.value)}
                      onClick={() => this.setState(() => ({removePendingInvitation: id.value}))}
                      >
                      { removedInvitations.includes(id.value) ? 'Removed' : 'Remove' }
                    </Button>
                  </Col>
                </Row>
              })
      }
    </div>
  }

  componentWillMount() {
    getPendingSharedInvites();
  }
}
