import React, { Component } from 'react';
import _ from 'lodash';

import isEmail from 'validator/lib/isEmail';
import {
  checkFields, deepArrayField, deepObjectField, required, optional,
  isBlank, nonnegative, commonSourceValidators,
} from '../../utils/validation-utils';
import { deepOmit } from '../../utils/general-algorithms';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';

import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import { withRouter } from 'react-router';

import { history } from '../../navigation';

import { Row, Col, Well, Form, ControlLabel, FormControl, FormGroup, Button } from 'react-bootstrap';

import { actionCreators as SupplierActions } from '../../reducers/supplierDetails';
import {
  getSupplierDetails, saveSupplier, deleteSupplier,
  getMeasures, getIngredientList,
  getMultipleProductsUsageInfo, lookupSupplierName,
  getOnboardingInfo,
} from '../../webapi/endpoints';

import Validate from 'common/ui-components/Validate';

import Section from '../Section';

import TitleInput from '../TitleInput';
import AddButton from '../AddButton';
import UploadButton from '../UploadButton';
import SuppliedIngredientsDisplay from '../SuppliedIngredientsDisplay';
import SupplierUploadCsv from './SupplierUploadCsv';
import { actionCreators as UIStateActions } from '../../reducers/uiState/supplierEdit';
import NewIngredientModal from '../modals/NewIngredientModal';
import NewUnitModal from '../modals/NewUnitModal';
import EditIngredientModal from '../modals/EditIngredientModal';

import { FieldArray } from '../../utils/redux-upgrade-shims';
import { convertSupplierJSONToFormValues, supplierFormFields, shallowToObject } from '../../utils/form-utils';
import { featureIsSupported } from '../../utils/features';
import { scrollToTopErrorTooltip } from '../../utils/ui-utils';

export const inputLabelWidths = {
  xs: 12,
  md: 2,
};

export const shortInputControlWidths = {
  xs: 12,
  md: 4,
};

export const commentInputControlWidths = {
  xs: 12,
  md: 7,
};

function asyncValidate(values, dispatch, props) {
  if (props.initialValues.name === values.name.trim()) {
    return Promise.resolve();
  }
  return lookupSupplierName(values.name.trim())
    .then(response => {
      if (response && CrudEditor.sourceId(props.params) !== response) {
        throw { name: 'Name is already used' };
      }
    });
}

function validate(values) {
  // the top-level object and individual contacts happen to have the same structure,
  // so share the validators between them
  const contactValidators = {
    name: required(),
    email: optional(val => !isEmail(val) ? 'Not valid e-mail' : null),
  };

  const sourceValidators = {
    ...commonSourceValidators,
    product: required(),
  };

  return checkFields(values, {
    ...contactValidators,
    contacts: deepArrayField(deepObjectField(contactValidators)),
    sources: deepArrayField(deepObjectField(sourceValidators)),
  });
}

export function getProductIdsFromSources(sources, fromForm = true) {
  let ret = _.uniq(sources.map(s => fromForm ? s.product.value : s.product));
  ret = _.filter(ret, productId => Boolean(productId) || productId === 0);
  return _.sortBy(ret, productId => productId);
}

@withRouter
@reduxForm(
    {
      form: 'supplier_edit',
      fields: supplierFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      asyncValidate,
      asyncBlurFields: ['name'],
      onSubmitSuccess: () => history.push('/suppliers'),
      // TODO: Move this universal behaviour to CrudEditor
      // we can't define `onSubmitFail` in CrudEditor,
      // and the `submit` function isn't reached if the `validation` functions fail though
      onSubmitFail: () => setTimeout(scrollToTopErrorTooltip, 200),
    },
    (state, ownProps) => {
      const supplierDetails = state.get('supplierDetails');
      const detailedIngredients = state.get('detailedIngredients');
      const allIngredients = state.get('ingredients');
      return {
        initialValues: !CrudEditor.isCreatingNew(ownProps.params) || CrudEditor.isCopying(ownProps.params)
            ? CrudEditor.initialValues(
                supplierDetails,
                {
                  allIngredients, // used by convertSupplierJSONToFormValues
                  ...ownProps.params,
                },
                convertSupplierJSONToFormValues)
            : {},
        mainInfoLoaded: Boolean(supplierDetails),
        allMeasures: state.getIn(['measures', 'measures']),
        allIngredients,
        uiState: state.get('supplierEdit'),
        detailedIngredients,
        userVolumePref: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        userWeightPref: state.getIn(['userInfo', 'purchaseWeightSystem']),
        isMultiLocationDownstream: state.getIn(['userInfo', 'isMultiLocationDownstream']),
        percentImportUpdate: state.getIn(['userInfo', 'sourcePriceChangeWarningFraction']),
      };
    },
    dispatch => ({
      supplierActions: bindActionCreators(SupplierActions, dispatch),
      uiStateActions: bindActionCreators(UIStateActions, dispatch),
    }),
)
export default class SupplierEdit extends CrudEditor {
  constructor(props, context) {
    super(props, context);
    this.state = {
      // when this is true, all contact sections will set focus on mount
      // we set it true after the first time the 'add contact' button is clicked, so that
      // SupplierContactSections that mount *after* that will set focus, but not the initial ones
      alreadyAddedContact: false,
      importingFromCSV: false,
      editingIngredient: null,
    };
    this.productIds = getProductIdsFromSources(this.props.fields.sources);
    this.reportDeleteSource = () => {
     this.deletedSource = true;
    };
    this.reportImportSource = () => {
      this.sourcesImported = true;
    }
    ['uploadCsv',
      'finishUploadCsv',
      'openEditIngredientModal',
      'closeEditIngredientModal',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });
  }
  handleSave(id, deets) {
    return saveSupplier(id, deepOmit(deets, isBlank));
  }

  renderBody() {
    const {
        uiState, uiStateActions, untouch, allMeasures, allIngredients, detailedIngredients,
        userVolumePref, userWeightPref, isMultiLocationDownstream,
    } = this.props;
    const { alreadyAddedContact } = this.state;
    const {
        name, accountId, contactName, phone, email, comments, instructions, contacts, sources, syncUpstreamOwner,
    } = this.props.fields;

    return (
      <div className={syncUpstreamOwner.value ? 'synced-supplier' : ''}>
        <Row><Col xs={15} md={12}>
          <div>
            <Form horizontal>
              <Row>
                <Col xs={12} md={6}>
                  <TitleInput noun="Supplier"
                              inputRef={input => this.initialInput = input}
                              disabled={isMultiLocationDownstream && syncUpstreamOwner.value}
                              {...name}
                  />
                </Col>
              </Row>
              <FormGroup>
                <Col componentClass={ControlLabel} {...inputLabelWidths}>Account ID:</Col>
                <Col {...shortInputControlWidths}>
                  <Validate controlId="account-id">
                    <FormControl type="text" placeholder="Optional" {...accountId} />
                  </Validate>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col componentClass={ControlLabel} md={2}>Contact Name:</Col>
                <Col {...shortInputControlWidths}>
                  <Validate {...contactName}>
                    <FormControl type="text" placeholder="Optional" {...contactName} />
                  </Validate>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col componentClass={ControlLabel} md={2}>Phone:</Col>
                <Col {...shortInputControlWidths}>
                  <Validate {...phone}>
                    <FormControl type="text" placeholder="Optional" {...phone} />
                  </Validate>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col componentClass={ControlLabel} md={2}>Email:</Col>
                <Col {...shortInputControlWidths}>
                  <Validate {...email}>
                    <FormControl type="email" placeholder="Optional" {...email}/>
                  </Validate>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col componentClass={ControlLabel} md={2}>Comments:</Col>
                <Col {...commentInputControlWidths}>
                  <Validate {...comments}>
                    <FormControl componentClass="textarea" {...comments}/>
                  </Validate>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col componentClass={ControlLabel} md={2}>Special Instructions:</Col>
                <Col {...commentInputControlWidths}>
                  <Validate {...instructions}>
                    <FormControl componentClass="textarea" {...instructions}/>
                  </Validate>
                </Col>
              </FormGroup>
            </Form>
          </div>

          <FieldArray
              component={SupplierContactsBlock}
              fields={contacts}
              props={{
                alreadyAddedContact,
                setAddedContact: () => this.setState({ alreadyAddedContact: true }),
              }}
          />

          <Section title="Supplied Ingredients">
            <div className="suppliedIngredientsList">
              <FieldArray
                component={SuppliedIngredientsDisplay}
                fields={sources}
                props={{
                  isMultiLocationDownstream,
                  reportDeleteSource: this.reportDeleteSource,
                  supplierId: parseInt(this.props.params.id),
                  allMeasures, allIngredients, detailedIngredients,
                  uiStateActions,
                  untouch,
                  showingNewIngredientModal: uiState.get('showNewIngredientModal'),
                  showingImportIngredientsModal: uiState.get('showImportIngredientsModal'),
                  userVolumePref,
                  userWeightPref,
                  openEditIngredientModal: this.openEditIngredientModal
                }}
              />
            </div>
          </Section>
        </Col></Row>
        <Onboarding pageKey="supplier_editor" />
      </div>);
  }

  render() {
    const { uiState, uiStateActions, params } = this.props;
    const { sources } = this.props.fields;
    let view = this.state.importingFromCSV ?
      <SupplierUploadCsv
        supplierId={CrudEditor.isCreatingNew(params) ? undefined : parseInt(params.id)}
        finishUploadCsv={this.finishUploadCsv}
        openEditIngredientModal={this.openEditIngredientModal}
        showingNewIngredientModal={uiState.get('showNewIngredientModal')}
        reportImportSource={this.reportImportSource}
        sources={sources}
        supplierForm={this.props.values}
        {...this.props} />
      : super.render();

    return <div>
      {view}
      { uiState.get('showNewIngredientModal') ?
        <NewIngredientModal key="new-ingredient-modal"
            newIngredient={uiState.get('showNewIngredientModal')}
            uiStateName="supplierEdit"
            hide={uiStateActions.closeNewIngredientModal}
            showSuppliersAndCost={false}
        />
        : null }
      { uiState.get('showNewUnitModal') ?
        <NewUnitModal key="new-unit-modal"
            newUnit={uiState.get('showNewUnitModal')}
            uiStateName="supplierEdit"
            hide={uiStateActions.closeNewUnitModal}
        />
        : null }
      { this.state.editingIngredient ?
        <EditIngredientModal
          onHide={this.closeEditIngredientModal}
          productId={this.state.editingIngredient}
          hideSuppliersAndCostSection
        />
        : null }
    </div>;
  }

  deleteItem() {
    return deleteSupplier(this.props.params.id);
  }

  uploadCsv() {
    this.setState({ importingFromCSV: true });
  }

  finishUploadCsv(supplierId) {
    if (supplierId) {
      this.clearRouteLeaveHook();
      history.push(`/suppliers/${supplierId}`);
      getSupplierDetails(supplierId);
    }
    this.setState({ importingFromCSV: false });
  }

  openEditIngredientModal(productId) {
    this.setState({
      editingIngredient: productId,
    });
  }

  closeEditIngredientModal() {
    this.setState({
      editingIngredient: null,
    });
  }

  componentWillMount() {
    super.componentWillMount();

    const { router, route } = this.props;

    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      // prompt if props is dirty or user has deleted a source row
      if (this.props.dirty || this.deletedSource || this.sourcesImported ) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });

    getMeasures();
    getIngredientList();
    getOnboardingInfo();

    if (this.sourceId() !== null) {
      getSupplierDetails(this.sourceId());
    }

    getMultipleProductsUsageInfo(this.productIds, 50);
  }

  componentWillReceiveProps(nextProps) {
    const nextProductIds = getProductIdsFromSources(nextProps.fields.sources);
    if (!_.isEqual(this.productIds, nextProductIds)) {
      const newProductIds = _.difference(nextProductIds, this.productIds);
      this.productIds = nextProductIds;
      getMultipleProductsUsageInfo(newProductIds, 50);
    }
  }

  componentWillUnmount() {
    if (this.sourceId() !== null)
      this.props.supplierActions.dropDetails();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.importingFromCSV && !this.state.importingFromCSV) {
      window.scrollTo(0, 0);
    }
  }

  makeExtraButtons() {
    const { valid, handleSubmit } = this.props;
    return <UploadButton
      onClick={valid ? this.uploadCsv : handleSubmit(() => {})}/>;
  }

  listUrl = '/suppliers';
  noun = 'supplier';
}

class SupplierContactsBlock extends Component {
  render() {
    const {
      fields: contacts,
      alreadyAddedContact,
      setAddedContact,
    } = this.props;

    return <Section title="Additional Contacts">
      {contacts.map((c, i) =>
          <SupplierContact
              fields={c} key={i}
              removeContact={() => contacts.removeField(i)}
              grabFocus={alreadyAddedContact}
          />)}
      <Row><Col xs={12} md={9}>
        <AddButton noun="Contact" onClick={() => {
          contacts.addField();
          setAddedContact();
        }}/>
      </Col></Row>
    </Section>;
  }
}

class SupplierContact extends Component {

  render() {
    const {
      fields: {
        name, phone, email, comments,
      },
      removeContact,
    } = this.props;

    return <Well className="supplier-contact">
      <Form horizontal>
        <FormGroup>
          <Col componentClass={ControlLabel} {...inputLabelWidths}>
            <Validate {...name}>
              Name:
            </Validate>
          </Col>
          <Col {...shortInputControlWidths}>
            <input type="text" className="form-control" {...name}
                ref={input => this.initialInput = input}
            />
          </Col>
          <Col xs={12} md={1} mdOffset={5}>
            <Button bsStyle="success" className="pull-right"
                    onClick={removeContact}>
              delete
            </Button>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} {...inputLabelWidths}>Phone:</Col>
          <Col {...shortInputControlWidths}>
            <Validate {...phone}>
              <FormControl type="text" {...phone}/>
            </Validate>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} {...inputLabelWidths}>Email:</Col>
          <Col {...shortInputControlWidths}>
            <Validate {...email}>
              <FormControl type="email" {...email}/>
            </Validate>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} {...inputLabelWidths}>Comments:</Col>
          <Col {...commentInputControlWidths}>
            <Validate {...comments}>
              <FormControl componentClass="textarea" {...comments}/>
            </Validate>
          </Col>
        </FormGroup>
      </Form>
    </Well>;
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }
}
