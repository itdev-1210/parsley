import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { getOnboardingWidgetInfo } from '../../webapi/endpoints';

import {
  Grid, Row, Col,
  Button, Modal,
  ListGroup, ListGroupItem,
  FormControl,
} from 'react-bootstrap';

import SearchableList, { ScrollableLink } from './helpers/SearchableList';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import {
  onboardingBulkEditFields,
  valueFromEvent
} from '../../utils/form-utils';

@connect(
  state => ({
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
)
export default class OnboardingList extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { location, onboardingWidgetsInfo, } = this.props;

    let content;
    if (!onboardingWidgetsInfo) {
      content = <LoadingSpinner />;
    } else {
      content = <SearchableList
        searchKey="onboarding-wigdet-list"
        noun="Widget"
        reduxStatePath={['onboardingWidgetsInfo', 'widgets']}
        baseUrl={this.props.location.pathname}
        checkable={false}
        taggable={false}
        copiable={false}
        newButton={<div/>}
        sortItemsFunc={item => item.get('name')}
        renderSectionItem={item => {
          return <ListGroupItem key={item.get('id')}>
          <ScrollableLink
              id={`list-item-onboarding-step-${item.get('id')}`}
              item={item}
              className="list-item-main-link"
              shouldScroll={item => location.state && item.get('id') === location.state.prevObjId}
              to={{
                pathname: location.pathname + '/' + item.get('id').toString(),
              }}
            >
            {item.get('name')}
          </ScrollableLink>
        </ListGroupItem>;
        }}
        mainBodyOptions={{ md: 8, xs: 12 }}
        searchWidthOptions={{ md: 8, xs: 12 }}
      />;
    }

    return <Grid>
      {content}
    </Grid>;
  }

  componentDidMount() {
    getOnboardingWidgetInfo();
  }
}
