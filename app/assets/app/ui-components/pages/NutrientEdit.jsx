// @flow
import React, { Component } from 'react';
import { reduxForm } from "redux-form";

import { history } from "../../navigation";

import {
  Grid, Row, Col,
  FormControl,
  Button,
} from 'react-bootstrap';

import Checkbox from '../Checkbox';
import AddButton from '../AddButton';

import Validate from 'common/ui-components/Validate';

import {
  checkFields, required, optional,
  deepArrayField, deepObjectField,
  positive, isBlank,
} from '../../utils/validation-utils';
import { deepOmit } from "../../utils/general-algorithms";

import { getNutrients, saveNutrients } from "../../webapi/endpoints";

function validate(values) {
  return checkFields(values, { nutrients: deepArrayField(deepObjectField({
    id: optional(), // could be new
    nndbsrId: required(),
    nndbsrName: required(),
    parsleyName: optional(),
    unit: required(),
    active: required(),
  }))});
}

// pretty tolerant, because this represents a value in the entry process
type FormValues = Array<{
  id: ?number,
  nndbsrId: ?number,
  nndbsrName: ?string,
  parsleyName: ?string,
  unit: ?string,
  active: boolean,
}>

@reduxForm(
    {
      form: 'nutrient-edit',
      fields: [
        "nutrients[].id",
        "nutrients[].nndbsrId",
        "nutrients[].nndbsrName",
        "nutrients[].parsleyName",
        "nutrients[].unit", // name, not Parsley unit - these will be things like mg
        "nutrients[].active",
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      onSubmitSuccess: () => history.push("/admin"),
    },
    (state, origProps) => ({
      initialValues: state.get('nutrients') && {
        nutrients: state.get('nutrients').toJS()
      },
      mainInfoLoaded: state.get('nutrients'),
    })
)
export default class NutrientEdit extends Component<any> {

  addNutrient() {
    this.props.fields.nutrients.addField({active: false});
  }
  removeNutrient(i: number) {
    this.props.fields.nutrients.removeField(i);
  }

  render() {
    const {
      fields: { nutrients },
      submitting, handleSubmit,
    } = this.props;

    return <Grid fluid>
      <h4>Nutrients</h4>
      <Row className="indent-bottom">
        <Col xs={12} md={9} className="crud-top-buttons">
          <Button disabled={submitting}
                  onClick={handleSubmit(this.submit.bind(this))}
                  bsStyle={ submitting ? "default" : "primary" }
                  className="green-btn">
            { submitting ? "saving" : "save"}
          </Button>
        </Col>
      </Row>
      {nutrients.map(({id, nndbsrId, nndbsrName, parsleyName, unit, active }, i) =>
        <Row>
          <Col xs={12} md={2}>
            <Validate {...parsleyName} compact>
              <FormControl type="text" placeholder="Parsley Name" {...parsleyName} />
            </Validate>
          </Col>
          <Col xs={12} md={2}>
            <Validate {...nndbsrName} compact>
              <FormControl type="text" placeholder="NNDB Name" {...nndbsrName} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Validate {...nndbsrId} compact>
              <FormControl type="text" placeholder="NNDB ID" {...nndbsrId} />
            </Validate>
          </Col>
          <Col xs={12} md={2}>
            <Validate {...unit} compact>
              <FormControl type="text" placeholder="Unit Name" {...unit} />
            </Validate>
          </Col>
          <Col xs={12} md={1}>
            <Checkbox {...active}>
              active
            </Checkbox>
          </Col>
          <Col xs={12} md={1}>
            <Button bsStyle="warning" onClick={() => nutrients.removeField(i)}>
              delete
            </Button>
          </Col>
        </Row>
      )}
      <AddButton onClick={this.addNutrient.bind(this)} noun="nutrient" />
    </Grid>;
  }

  submit(values: FormValues) {
    values = deepOmit(values, isBlank);

    // copy-pasted from MeasureEdit
    for (let u of values.nutrients) {
      if (typeof(u.active) !== typeof(true)) {
        u.active = (u.active === "true");
      }
    }

    return saveNutrients(values.nutrients)
        .then( // copy-pasted from CrudEditor
            success => {
              this.clearRouteLeaveHook();
              return success;
            },
            e => {
              if (e.reduxFormErrors) {
                return Promise.reject(e.reduxFormErrors)
              } else {
                console.log(e.toString());
                console.log(e.stack);
                return Promise.reject({_error: e.message})
              }
            }
        )
  }

  clearRouteLeaveHook: () => void;

  componentWillMount() {
    const { router, route } = this.props;
    getNutrients();
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return "You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?";
      } else {
        return null;
      }
    });
  }
}