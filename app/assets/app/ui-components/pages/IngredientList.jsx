import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Grid } from 'react-bootstrap';
import { getOnboardingInfo, getIngredientList, deleteProduct, getUserInfo, getLocationList, getCanSyncAllLocations, syncAllLocations } from '../../webapi/endpoints';
import Onboarding from '../Onboarding';
import SearchableList from './helpers/SearchableList';
import { actionCreators as UIStateActions } from '../../reducers/uiState/ingredientList';
import { actionCreators } from '../../reducers/uiState/searchableList';
import AddToCategoryModal from '../modals/AddToCategoryModal';
import RemoveFromCategoryModal from '../modals/RemoveFromCategoryModal';
import AddAllergensCharacteristicsModal from '../modals/AddAllergensCharacteristicsModal';
import RemoveAllergensCharacteristicsModal from '../modals/RemoveAllergensCharacteristicsModal';
import Checkbox from '../Checkbox';
import { featureIsSupported } from '../../utils/features';
import Immutable from 'immutable';
import { ingredientListBoost } from '../../utils/search';

@connect(
    state => ({
      user: state.get('userInfo'),
      onboardingInfo: state.get('onboardingInfo'),
      shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
      listUiState: state.getIn(['searchableList', 'ingredients']),
      uiState: state.getIn(['ingredientList']),
      hasLocations: (state.get('locations') || Immutable.List()).size > 0,
    }),
    dispatch => {
      let actions = Immutable.Map(bindActionCreators(actionCreators, dispatch)).map(
        creator => creator.bind(this, 'ingredients')
      ).toObject();
      actions.modalActions = bindActionCreators(UIStateActions, dispatch);
      return actions;
    }
)
export default class IngredientList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayedItemsIds: Immutable.Set(),
      showAddAllergensCharacteristicsModal: false,
      showRemoveAllergensCharacteristicsyModal: false,
      addingAllergensCharacteristicsIngredients: null,
      removingAllergensCharacteristicsIngredients: null,
      canSync: false
    };
    this.updateDisplayedItemsIds = displayedItemsIds => {
      if (!displayedItemsIds.equals(this.state.displayedItemsIds)) {
        this.setState({ displayedItemsIds });
      }
    };
  }

  openAddAllergensCharacteristicsModal(ingredients) {
    this.setState({
      addingAllergensCharacteristicsIngredients: ingredients,
      showAddAllergensCharacteristicsModal: true,
    });
  }

  closeAddAllergensCharacteristicsModal() {
    this.setState({
      addingAllergensCharacteristicsIngredients: null,
      showAddAllergensCharacteristicsModal: false,
    });
  }

  openRemoveAllergensCharacteristicsModal(ingredients) {
    this.setState({
      removingAllergensCharacteristicsIngredients: ingredients,
      showRemoveAllergensCharacteristicsyModal: true,
    });
  }

  closeRemoveAllergensCharacteristicsModal() {
    this.setState({
      removingAllergensCharacteristicsIngredients: null,
      showRemoveAllergensCharacteristicsyModal: false,
    });
  }

  render() {

    const { listUiState, uiState,
      modalActions: {
        openAddToCategoryModal,
        closeAddToCategoryModal,
        openRemoveFromCategoryModal,
        closeRemoveFromCategoryModal
      },
      setCheckedItems,
      user,
      hasLocations,
      onboardingInfo,
      shouldShowOnboarding,
    } = this.props;

    const checkedItems = listUiState && listUiState.get('checkedItems');
    const canSetSelectAll = checkedItems && checkedItems.intersect(this.state.displayedItemsIds).size > 0;
    const selectAllChecked = canSetSelectAll && this.state.displayedItemsIds.isSubset(checkedItems);
    const selectAllIndeterminate = canSetSelectAll && !this.state.displayedItemsIds.isSubset(checkedItems);

    return <Grid fluid style={{ position: 'relative' }} className="ingredient-list-page">
      {featureIsSupported('MULTI_LOCATION') && hasLocations ?
        <Button
          style={
            /* zIndex needs to be high enough for the button to be clickable
              and low enough to be below popup modals */
            { position: 'absolute', right: '385px', top: '12px', zIndex: 1020 }
          }
          disabled={!this.state.canSync}
          onClick={() =>
            syncAllLocations().then(() => this.setState({ canSync: false }))
          }
        >
          {this.state.canSync ? 'Sync' : 'Synced'}
        </Button>
      : null}
      <SearchableList
        searchKey="ingredients"
        noun="Ingredient"
        reduxStatePath="ingredients"
        baseUrl={this.props.location.pathname}
        deleteItems={ids => SearchableList.deleteItems(ids, deleteProduct)}
        refreshItems={getIngredientList}
        checkable
        taggable tagTypes={['ingredients', 'inventories']}
        tagTooltips={{
          inventories: 'You can add Ingredient Locations from within the Ingredient Editor, or \
          if you want to do it in bulk (many ingredients at a time) go to the Take Inventory page, \
          select the ingredients and sub-recipes you want, and click on the Add to Category button'
        }}
        copiable
        searchBoost={ingredientListBoost}
        updateDisplayedItemsIds={this.updateDisplayedItemsIds}
        getClassnames={item => {
          if (item.get('isSubRecipe')) return ['text-italic'];
          if (item.get('manuallyCreated')) return ['manually-created'];
          return [];
        }}
        shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}
        shouldClearSearchFilter={false}
      >
        <Button
          onClick={() => { if (this.selectAllCheckbox) this.selectAllCheckbox.click(); }}
          style={{ width: '121px', textAlign: 'left' }}>
            <Checkbox
              ref={checkbox => this.selectAllCheckbox = checkbox ? checkbox.input : null}
              inline
              checked={selectAllChecked}
              indeterminate={selectAllIndeterminate}
              onChange={ev => ev.target.checked ? setCheckedItems(this.state.displayedItemsIds) : setCheckedItems(Immutable.Set())}
              style={{ pointerEvents: 'none', marginBottom: '-2px' }}
            /> { selectAllChecked ? 'Deselect All' : 'Select All'}
        </Button>
        <Button
          disabled={!(checkedItems && checkedItems.size)}
          onClick={() => openAddToCategoryModal(checkedItems)}>
          Add to Category
        </Button>
        <Button
          disabled={!(checkedItems && checkedItems.size)}
          onClick={() => openRemoveFromCategoryModal(checkedItems)}>
          Remove from Category
        </Button>
        {user && user.get('isStdlib') ? <div style={{ margin: '10px 0px 12px 0px', minWidth: '600px', float: 'left' }}>
          <Button
            disabled={!(checkedItems && checkedItems.size)}
            style={{ marginRight: '6px' }}
            onClick={() => this.openAddAllergensCharacteristicsModal(checkedItems)}>
            Add Allergens and Characteristics
          </Button>
          <Button
            disabled={!(checkedItems && checkedItems.size)}
            onClick={() => this.openRemoveAllergensCharacteristicsModal(checkedItems)}>
            Remove Allergens and Characteristics
          </Button>
        </div>
        : null }
      </SearchableList>

      { uiState.get('showAddToCategoryModal') ?
         <AddToCategoryModal
            hide={() => closeAddToCategoryModal()}
            onSubmitSuccess={() => setTimeout(getIngredientList, 10)}
            tagType='ingredients'
            targetIds={uiState.get('addingToCategoryIngredients')}
         />
      : null }

      { uiState.get('showRemoveFromCategoryModal') ?
         <RemoveFromCategoryModal
            hide={() => closeRemoveFromCategoryModal()}
            onSubmitSuccess={() => setTimeout(getIngredientList, 10)}
            tagType='ingredients'
            targetIds={uiState.get('removingFromCategoryIngredients')}
         />
      : null }

      { this.state.showAddAllergensCharacteristicsModal ?
         <AddAllergensCharacteristicsModal
            hide={() => this.closeAddAllergensCharacteristicsModal()}
            targetIds={this.state.addingAllergensCharacteristicsIngredients}
         />
      : null }

      { this.state.showRemoveAllergensCharacteristicsyModal ?
         <RemoveAllergensCharacteristicsModal
            hide={() => this.closeRemoveAllergensCharacteristicsModal()}
            targetIds={this.state.removingAllergensCharacteristicsIngredients}
         />
      : null }
      <Onboarding pageKey="ingredient_list" />
    </Grid>;
  }

  componentWillMount() {
    getUserInfo();
    getLocationList();
    getIngredientList();
    getOnboardingInfo();
    getCanSyncAllLocations().then(canSync => this.setState({ canSync }));
  }
}
