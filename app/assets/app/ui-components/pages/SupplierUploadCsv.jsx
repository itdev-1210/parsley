import React, { Component } from 'react';
import _ from 'lodash';
import { Button, Col, Row, Grid } from 'react-bootstrap';
import SupplierImportIngredients from '../SupplierImportIngredients';
import SupplierCsvReader from '../SupplierCsvReader';
import SuppliedIngredientsDisplay from '../SuppliedIngredientsDisplay';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import Section from '../Section';
import { history } from '../../navigation';
import { reduxForm } from 'redux-form';
import { commonSourceFormFields, shallowToObject } from '../../utils/form-utils';
import { saveSupplier, addImportedSourcesBySupplier } from '../../webapi/endpoints';
import {
  checkFields, deepArrayField, deepObjectField, required,
  isBlank, commonSourceValidators,
} from '../../utils/validation-utils';
import { deepOmit } from '../../utils/general-algorithms';


export default class SupplierUploadCsv extends Component {

  onSaveImportedSources: Function

  constructor(props, context) {
    super(props, context);
    this.listUrl = '/suppliers';
    this.noun = 'supplier';
    this.bodyWidthSet = { xs: 12, md: 10, mdOffset: 2 };
    this.uploadCsvCallback = this.uploadCsvCallback.bind(this);

    this.dataCsv = null;
    this.state = {
      csvLoaded: false,
    };
  }

  uploadCsvCallback(data) {
    this.dataCsv = data;
    this.setState({ csvLoaded: true });
  }

  render() {
    const { csvLoaded } = this.state;
    const { supplierId,
      finishUploadCsv,
      fields,
      uiStateActions,
      uiState,
      allIngredients,
      userVolumePref,
      userWeightPref,
      allMeasures,
      detailedIngredients,
      openEditIngredientModal,
      percentImportUpdate,
      reportImportSource,
      supplierForm } = this.props;
    const { name, sources } = fields;

    return <Grid fluid>
      <Row>
        <Col {...this.bodyWidthSet}>
          <Row>
            <Col className="hidden-xs" xs={2}>
              <a className="crud-back-link" style={{ cursor: 'pointer' }} onClick={() => finishUploadCsv()}>
                &lt; {this.noun}
              </a>
            </Col>
            <Col className="crud-top-buttons" xs={3} xsOffset={csvLoaded ? 7 : 5}>
              <span>
                {
                  !csvLoaded ?
                  <SupplierCsvReader noun="UPLOAD PRICE LIST" onClick={this.uploadCsvCallback}/>
                  : null
                }
                <Button
                  bsStyle={'primary'}
                  className="green-btn"
                  onClick={csvLoaded ? this.onSaveImportedSources : () => finishUploadCsv()}>
                  {csvLoaded ? 'Save' : 'Exit'}
                </Button>
              </span>
            </Col>
          </Row>
          <Row>
            <Col>
              <h2>{name.value} Price List Import</h2>
            </Col>
          </Row>
          {
            csvLoaded ?
            <SupplierUploadDisplay
              reportImportSource={reportImportSource}
              uiStateActions={uiStateActions}
              uiState={uiState}
              allIngredients={allIngredients}
              userVolumePref={userVolumePref}
              userWeightPref={userWeightPref}
              allMeasures={allMeasures}
              detailedIngredients={detailedIngredients}
              openEditIngredientModal={openEditIngredientModal}
              percentImportUpdate={percentImportUpdate}
              csvData={this.dataCsv}
              supplierId={supplierId}
              onSave={finishUploadCsv}
              onSaveCallback={(onSaveFn) => this.onSaveImportedSources = onSaveFn}
              sources={sources}
              supplierForm={supplierForm}
              />
            : <SupplierUploadInstructions onUpload={this.uploadCsvCallback} onCancel={finishUploadCsv}/>
          }
        </Col>
      </Row>
    </Grid>;
  }
}


function validate(values) {
  const sourceValidators = {
    ...commonSourceValidators,
    product: required(),
    cost: required(value => {
      if (isNaN(value)) {
        return 'Unreadable value';
      } else if (value < 0) {
        return 'Cannot be negative';
      }
      return null;
    }),
  };

  return checkFields(values, {
    sourcesFromCsv: deepArrayField(deepObjectField(sourceValidators)),
  });
}
@reduxForm(
  {
    form: 'supplier_upload_csv',
    fields: [
      ...commonSourceFormFields.map(f => `sourcesFromCsv[].${f}`),
      'sourcesFromCsv[].product',
    ],
    validate,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: {},
  }),
)
class SupplierUploadDisplay extends Component {
  constructor(props, context) {
    super(props, context);
    this.addUploadedIngredient = this.addUploadedIngredient.bind(this);
    this.saveAddedAndUpload = this.saveAddedAndUpload.bind(this);
    this.deleteImportedSource = this.deleteImportedSource.bind(this);
    this.state = {
      submitting: false,
    };
  }

  deleteImportedSource = (id) => {
    const { fields, sources } = this.props;
    const { sourcesFromCsv } = fields;
    const idxFromCsv = sourcesFromCsv.findIndex(source => source.id.value === id);
    const idx = sources.findIndex(source => source.id.value === id);
    sourcesFromCsv.removeField(idxFromCsv);
    sources.removeField(idx);
  }

  saveAddedAndUpload() {
    const { supplierId, onSave,  supplierForm } = this.props;
    const currentFields = this.getCurrentFieldsFn();
    let deets = {};
    if (currentFields) {
      deets = Object.assign({}, {
        ...supplierForm,
        sources: [...supplierForm.sources, currentFields],
      });
    } else {
      deets = Object.assign({}, supplierForm);
    }

    saveSupplier(supplierId || 'new', deepOmit(deets, isBlank))
      .then((supId) => onSave(supplierId || supId));
  }

  addUploadedIngredient(ingredient: ProductSource) {
    const { reportImportSource } = this.props;
    const {
      sourcesFromCsv,
    } = this.props.fields;
    sourcesFromCsv.addField({
      ...ingredient,
      supplier: this.props.supplierId,
    });
    reportImportSource();
  }

  render() {
    const {
      supplierId,
      csvData,
      uiStateActions,
      uiState,
      allIngredients,
      userVolumePref,
      userWeightPref,
      untouch,
      allMeasures,
      detailedIngredients,
      handleSubmit,
      sources,
      openEditIngredientModal,
      percentImportUpdate,
    } = this.props;

    const {
      sourcesFromCsv,
    } = this.props.fields;

    const { submitting } = this.state;

    return <Row>
      <SupplierImportIngredients
        ingredients={csvData}
        handleAdding={this.addUploadedIngredient}
        uiState={uiState}
        uiStateActions={uiStateActions}
        sources={sources}
        allIngredients={allIngredients}
        supplierId={supplierId}
        showingNewIngredientModal={uiState.get('showNewIngredientModal')}
        userVolumePref={userVolumePref}
        userWeightPref={userWeightPref}
        untouch={untouch}
        allMeasures={allMeasures}
        openEditIngredientModal={openEditIngredientModal}
        percentImportUpdate={percentImportUpdate}
        getCurrentFieldsFn={fn => this.getCurrentFieldsFn = fn}
      />
      <br/>
      <SupplierUploadSource
        sources={_.intersectionWith(sources, sourcesFromCsv, (s, sc) => s.id.value === sc.id.value)}
        supplierId={supplierId}
        uiState={uiState}
        uiStateActions={uiStateActions}
        userVolumePref={userVolumePref}
        userWeightPref={userWeightPref}
        untouch={untouch}
        allMeasures={allMeasures}
        allIngredients={allIngredients}
        detailedIngredients={detailedIngredients}
        openEditIngredientModal={openEditIngredientModal}
        deleteSourceFn={this.deleteImportedSource}
        />
      <hr className="green-hr"/>
      <Row className="indent-bottom">
        <Col className="crud-top-buttons">
          <Button
            disabled={submitting}
            bsStyle={submitting ? 'default' : 'primary'}
            className="green-btn"
            onClick={handleSubmit(this.saveAddedAndUpload)}>
            Save
          </Button>
        </Col>
      </Row>
    </Row>;
  }

  componentDidMount() {
    const { handleSubmit, onSaveCallback } = this.props;
    onSaveCallback(handleSubmit(this.saveAddedAndUpload));
  }
}

class SupplierUploadSource extends Component {
  render() {
    const {
      supplierId, sources, allMeasures, allIngredients, detailedIngredients, deleteSourceFn,
      uiStateActions, uiState, userVolumePref, userWeightPref, untouch, openEditIngredientModal,
    } = this.props;
    if (sources.length > 0) {
      return <Section title="Ingredients Imported">
        <div className="suppliedIngredientsList ingredients-imported-list">
          {
            sources.length > 0 ?
                <FieldArray
                    component={SuppliedIngredientsDisplay}
                    fields={_.sortBy(sources, s => s.id.value)}
                    props={{
                      supplierId,
                      reportDeleteSource: () => {},
                      isReadOnly: true,
                      allMeasures,
                      allIngredients,
                      detailedIngredients,
                      uiStateActions,
                      untouch,
                      showingNewIngredientModal: uiState.get('showNewIngredientModal'),
                      showingImportIngredientsModal: uiState.get('showImportIngredientsModal'),
                      userVolumePref,
                      userWeightPref,
                      openEditIngredientModal,
                      deleteSourceFn,
                    }}
                />
                :
                <h4 align="center">Ingredients have not been imported</h4>

          }
        </div>
      </Section>;
    } else {
      return null;
    }
  }
}

function SupplierUploadInstructions(props) {
  return <Row>
    <Col>
      <h4>Before uploading your price list, please convert it to CSV format and make the following adjustments to the file:</h4>
      <br />
      <ul className="supplier-upload-csv-adjustments-list">
        <li><h4>The first row in the file should be the column names (if there are extra rows on top, delete them)</h4></li>
        <li><h4>Change the column names to the following. Parsley will understand them in any order, and will ignore extra columns.</h4></li>
        <ul>
          <li><h4>
            Name (required) -
            in many suppliers' price-list files, this also includes packaging information
          </h4></li>
          <li><h4>
            Price (required)
          </h4></li>
          <li><h4>
            SKU (recommended) -
            the supplier's unique ingredient identifier,
            otherwise referred to as PN (Product Number), PLU, Ingredient Number,
            or other fun acronyms
          </h4></li>
          <li><h4>
            Purchase Package (optional) -
            e.g. CS/30#, 2l/btl, 6/32oz, etc.
          </h4></li>
          <li><h4>
            Pricing Unit (optional) -
            the package or unit of measurement that the price refers to,
            for example CS, lb, or Btl. When this column is missing, the price
            column usually refers to the price per full package.
          </h4></li>
          {/* <li><h4>Manufacturer(this is the manufacturer's name)</h4></li>
          <li><h4>Manufacturer number (this is the unique ingredint identifier given by the manufacturer)</h4></li>*/}
        </ul>
      </ul>
      <br />
    </Col>
    <br/>
  </Row>;
}
