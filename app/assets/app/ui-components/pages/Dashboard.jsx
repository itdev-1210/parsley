// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import SideBar from '../SideBar';
import { Grid, Row, Col } from 'react-bootstrap';
import RecipesDashboard from './dashboard/RecipesDashboard';
import PurchaseHistoryDashboard from './dashboard/PurchaseHistoryDashboard';
import IngredientPriceHistoryDashboard from './dashboard/IngredientPriceHistoryDashboard';
import IngredientPriceChangesDashboard from './dashboard/IngredientPriceChangesDashboard';
import { getOnboardingWidgetInfo, getPurchaseList } from '../../webapi/endpoints';
import { featureIsSupported } from '../../utils/features';

type Props = {
  onboardingWidgetsInfo: Immutable.Map<string, any>,
};
type State = {
  activeCategories: Array<string>
};

@connect(
  state => ({
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
)
export default class Dashboard extends Component<Props, State> {
  categories: Array<string>;

  constructor(props: Props) {
    super(props);
    this.state = {
      activeCategories: [],
    };
    this.onChangeCategories = this.onChangeCategories.bind(this);

    if (featureIsSupported('MULTI_DASHBOARD'))
      this.categories = ['Recipes', 'Purchase History', 'Ingredient Price History', 'Ingredient Price Changes'];
    else
      this.categories = ['Recipes', 'Ingredient Price History'];
  }

  onChangeCategories = (categoryName: string, isActive: boolean) => {
    const { activeCategories } = this.state;
    if (isActive) {
      this.setState({
        activeCategories: activeCategories.filter(c => c !== categoryName),
      });
    } else {
      this.setState({
        activeCategories: activeCategories.concat(categoryName),
      });
    }
  }

  componentByCategoryName = (categoryName: string) => {
     switch (categoryName) {
      case 'Recipes':
        return RecipesDashboard;
      case 'Purchase History':
        return PurchaseHistoryDashboard;
      case 'Ingredient Price History':
        return IngredientPriceHistoryDashboard;
      case 'Ingredient Price Changes':
        return IngredientPriceChangesDashboard;
      default:
        return RecipesDashboard;
    }
  }

  render () {
    const { activeCategories } = this.state;
    const categoriesOptions = activeCategories.length === 0 ? this.categories : activeCategories;
    const { onboardingWidgetsInfo } = this.props;

    return <Grid fluid>
      <Row>
        <Col md={2} xs={12}/>
        <Col md={10} xs={12}>
          <Row>
            <Col md={3} xs={6} className="title-block">
              <h3 className="list-title" style={{ marginBottom: '38px' }}>Dashboard</h3>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="dashboard">
        <Col md={2} xs={12} style={{ paddingTop: '7px' }}>
          <div className="leftcol">
            <h4>Information Categories</h4>
            <SideBar
              categories={this.categories}
              activeCategories={activeCategories}
              onClick={this.onChangeCategories}
            />
          </div>
        </Col>
        <Col md={10} xs={12} className="dashboard-container">
          <div className="dashboard-body">
            {
              onboardingWidgetsInfo ?
              <div>
                {
                  categoriesOptions.map(categoryName => {
                    const DashboardComponent = this.componentByCategoryName(categoryName);
                    return <Row key={categoryName}>
                      <Col sm={12}>
                        {
                          /** $FlowFixMe: can't simulate extra props passed in by decorators */
                          <DashboardComponent/>
                        }
                      </Col>
                    </Row>;
                  })
                }
              </div>
              : <div/>
             }
          </div>
        </Col>
      </Row>
    </Grid>;
  }

  componentDidMount() {
    getOnboardingWidgetInfo();
    getPurchaseList();
  }
}