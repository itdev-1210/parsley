import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import ReactPropTypes from 'prop-types';
import Immutable from 'immutable';
import _ from 'lodash';
import moment from 'moment';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import Validate from 'common/ui-components/Validate';
import {
    Col, Row,
    Form, FormGroup, FormControl, ControlLabel,
    Button,
    ListGroup, ListGroupItem,
    Glyphicon,
    Collapse,
    Well, Grid,
} from 'react-bootstrap';

import dragula from 'react-dragula';
import Dropzone from 'react-dropzone';

import { withRouter } from 'react-router';
import { history } from '../../navigation';

import { actionCreators as ProductActions } from '../../reducers/productDetails';
import { actionCreators as IngredientActions } from '../../reducers/detailedIngredients';
import { actionCreators as ReverseDependencyActions } from '../../reducers/reverseDependencies';
import { actionCreators as MenuActions } from '../../reducers/menuDetails';
import {
    getProductDetails,
    saveProduct,
    saveRecipePhoto,
    saveRecipeStepPhoto,
    deleteProduct,
    getIngredientList,
    getProductUsageInfo,
    getMultipleProductsUsageInfo,
    getReverseDependencies,
    lookupProductName,
    getProductionOrder,
    getMeasures,
    getMenuList,
    getMenuDetails,
    saveUserInfo,
    getUserInfo,
    getNutrients,
    getOnboardingInfo,
    getMultipleProductsDetails,
    getRecipeList,
} from '../../webapi/endpoints';

import { actionCreators as UIStateActions } from '../../reducers/uiState/recipeEdit';
import NewIngredientModal from '../modals/NewIngredientModal';
import EditIngredientModal from '../modals/EditIngredientModal';
import NewUnitModal from '../modals/NewUnitModal';

import {
  convertPerQuantityValue, purchasingRound,
  divideQuantities,
  rescaleUnits,
  convert,
  unitDisplayName,
  unitDisplayPrecision,
  unitDisplayQuantum,
  pickPerQuantityValueDisplayUnit,
  getMeasure,
} from '../../utils/unit-conversions';
import {
    checkFields,
    deepArrayField, deepObjectField,
    quantity as quantityValidator,
    required, optional, positive,
    commonProductValidators,
} from '../../utils/validation-utils';
import {
  convertProductFormValuesToJSON,
  productFormFields,
  convertProductJSONToFormValues,
  shallowToObject,
  getNNDBSRIdToNutrientIdMap, valueFromEvent,
} from '../../utils/form-utils';
import { uploadImage } from '../../utils/file-utils';
import { printWindowJSSnippet } from '../../utils/printing';

import Section from '../Section';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { changeNewLineToBreak } from 'common/utils/reactHelper';
import { featureIsSupported } from '../../utils/features';
import { featureIsAvailable } from 'common/utils/features';

import Alert from '../Alert';
import Select from '../Select';
import TextWithHelp from '../TextWithHelp';
import TitleInput from '../TitleInput';
import TagSelector from '../TagSelector';
import AddButton from '../AddButton';
import PrintButton from '../PrintButton';
import NutritionDisplay, {
  nutrient_fields,
} from '../NutritionDisplay';
import QuantityInput from '../QuantityInput';
import CurrencyInput from '../CurrencyInput';
import PercentInput, { toPercent } from '../PercentInput';
import Checkbox from '../Checkbox';
import HideableText from '../HideableText';
import { MaskedNumberInput } from '../MaskedInput';

import PrintRecipeModal from '../modals/PrintRecipeModal';
import PrintRecipeNutritionModal from '../modals/PrintRecipeNutritionModal';
import ResizeRecipeModal from '../modals/ResizeRecipeModal';
import IngredientSourcesModal from '../modals/IngredientSourcesModal';
import CostBreakdown from '../modals/CostBreakdown';
import IngredientNutrientModal from '../modals/IngredientNutrientModal';
import ViewPhotoModal from '../modals/ViewPhotoModal';
import { DeleteConfirmationModal } from '../modals/ConfirmationModal';
import ConversionEditor from '../ConversionEditor';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { indexTypes } from '../../reducers/indexesCache';
import {
  getRecipeBaseOrder,
  getRecipeProductionQuantity,
} from '../../utils/recipe-utils';
import {
  ingredientListSearchOpts,
} from '../../utils/search';
import { dateTimeShortFormat } from 'common/utils/constants';
import classNames from 'classnames';
import { noticeError } from 'common/utils/errorAnalytics';

function validate(values, props) {

  const recipeQuantityValidator = values.ingredient
      ? quantityValidator
      : deepObjectField({ amount: required(positive) });

  let recipeNutrients = {};
  if (values.recipe.recipeNutrients.packaged) {
    recipeNutrients.servingAmount = required(positive);
    recipeNutrients.servingUnit = required(positive);
  }

  const recipeValidators = {
    ...commonProductValidators,
    recipe: deepObjectField({
      recipeSize: required(recipeQuantityValidator),
      maxBatch: optional(recipeQuantityValidator),
      // quantum: optional(recipeQuantityValidator),
      isQuantized: required(),
      batchSize: optional(),
      batchLabel: optional(),

      steps: deepArrayField(deepObjectField({
        description: optional(),
        ingredients: deepArrayField(deepObjectField({
          ingredient: required(),
          description: optional(),
          quantity: optional(quantityValidator),
        })),
      })),
      recipeNutrients: deepObjectField(recipeNutrients),
      itemNumber: optional((number) => {
        if (props.allRecipes &&
          props.allRecipes.some(r => r.get('itemNumber') === number && r.get('id') !== CrudEditor.sourceId(props.params))) {
            return 'Item number is already used';
        }
      }),
    }),
  };

  return checkFields(values, recipeValidators);
}

const newRecipeTemplate = Immutable.fromJS({
  info: {
    lowestPricePreferred: true,
  },
  ingredient: false,
  salable: true,
  recipeTags: [],
  recipe: {
    advancePrep: false,
    isQuantized: false,
    batchLabel: 'batches',
    steps: [ // single blank step to make it clear how this is done
        {},
    ],
    recipeNutrients: {
      packaged: false,
      servingAmount: 1,
    },
  },
});

function asyncValidate(values, dispatch, props) {
  return lookupProductName(values.name.trim())
    .then(response => {
      if (response && CrudEditor.sourceId(props.params) !== response) {
        // error message should match IngredientEdit.jsx::asyncValidate and api.Products.createProduct
        throw { name: 'Name is already used' };
      }
    });
}

function usedIngredientsFromSteps(steps) {
  const allIngredientUsages = steps.flatMap(s => s.get('ingredients'));
  return allIngredientUsages
      .map(u => u.get('ingredient'))
      .filter(id => Boolean(id)) // ignore usages where ingredient hasn't been picked
      .toSet();
}

@withRouter
@reduxForm(
    {
      form: 'all',
      fields: productFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      asyncValidate,
      alwaysAsyncValidate: true,
      touchOnBlur: false,
      asyncBlurFields: ['name'],
      onSubmitSuccess: () => history.push('/recipes'),
    },
    (state, ownProps) => {
      const productId = parseInt(CrudEditor.sourceId(ownProps.params));
      const details = state.getIn(['productDetails', productId]);
      const logoUrl = state.getIn(['userInfo', 'logoUrl']);
      const userInfo = state.get('userInfo');
      const permissionLevel = userInfo.get('permissionLevel');
      const allNutrients = state.get('nutrients');
      const params = { ...ownProps.params, allNutrients };

      return {
        initialValues: !CrudEditor.isCreatingNew(ownProps.params) || CrudEditor.isCopying(ownProps.params)
            ? CrudEditor.initialValues(
                details,
                params,
                convertProductJSONToFormValues)
            : newRecipeTemplate.toJS(),
        mainInfoLoaded: Boolean(details),
        // only used for checking if this has loaded - initialValues is consumed by the redux-form wrapper
        productDetails: !CrudEditor.isCreatingNew(ownProps.params) || CrudEditor.isCopying(ownProps.params)
            ? CrudEditor.initialValues(
                details,
                params,
                convertProductJSONToFormValues)
            : newRecipeTemplate.toJS(),
        allRecipes: state.get('recipes'),
        allIngredients: state.get('ingredients'),
        detailedIngredients: state.get('detailedIngredients') || Immutable.Map(),
        menus: state.get('menus'),
        userVolumePref: state.getIn(['userInfo', 'volumeSystem']),
        userWeightPref: state.getIn(['userInfo', 'weightSystem']),
        allMeasures: state.getIn(['measures', 'measures']),
        allUnits: state.getIn(['measures', 'units']),
        allNutrients,
        productionOrder: state.getIn(['productionOrder', 'productionOrder']),
        logoUrl,
        companyLogoUrl: state.getIn(['userInfo', 'companyLogo']) ?
              logoUrl + state.getIn(['userInfo', 'companyLogo'])
              : null,
        recipePhotoUrl: details && details.photo ? logoUrl + details.photo : null,
        uiState: state.get('recipeEdit'),
        userInfo,
        permissionLevel,
        readonlyUser: (['recipe-read-only', 'operations'].includes(permissionLevel)),
        upstreamProductInfo: details && Immutable.fromJS(details.upstreamInfo),
        syncedRecipe: !CrudEditor.isCreatingNew(ownProps.params) && Boolean(userInfo && userInfo.get('isMultiLocationDownstream'))
          && Boolean(details && details.syncUpstream && details.syncUpstreamOwner),
        appFileDraggingOver: state.getIn(['appUI', 'fileDraggingOver']),
      };
    },
    dispatch => ({
      productActions: bindActionCreators(ProductActions, dispatch),
      ingredientActions: bindActionCreators(IngredientActions, dispatch),
      reverseDependencyActions: bindActionCreators(ReverseDependencyActions, dispatch),
      menuActions: bindActionCreators(MenuActions, dispatch),
      uiStateActions: bindActionCreators(UIStateActions, dispatch),
    }),
)
export default class RecipeEdit extends CrudEditor {

  constructor(props, context) {
    super(props, context);

    this.state = {
      previewingPhoto: null,
      removingPhoto: false,
      recipePhotoUrl: props.recipePhotoUrl,
      itemsDragged: false,
      showConversions: false, // only relevant to sub-recipes
      titleUserClick: false,
    };

    ['handlePhotoChange',
      'handleAddPhoto',
      'handleDropPhoto',
      'previewPhoto',
      'cancelPreviewPhoto',
      'removePhoto',
      'cancelRemovePhoto',
      'handlePhotoClear',
      'openEditIngredientModal',
      'closeEditIngredientModal',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });
  }

  handleSave(id, deets) {
    const {
      ingredientActions,
      params,
      fields: {
        photo, recipe,
      },
      syncedRecipe,
      productDetails,
    } = this.props;

    if (syncedRecipe) {
      // the following fields are changed by resizing, but resizing should not
      // be saved to DB
      for (const propName of [
        'steps',
        'recipeSize', 'quantum',
        'batchSize',
      ]) {
        deets.recipe[propName] = productDetails.recipe[propName];
      }
    }

    return saveProduct(id, convertProductFormValuesToJSON(deets))
        .then(productId => {
          const initialPhoto = photo.initialValue;
          // The photo has changed
          if ((deets.photo || initialPhoto) && deets.photo !== initialPhoto) {
            const recipePhotoFormData = new FormData();
            recipePhotoFormData.append('photo', deets.photo);
            return saveRecipePhoto(productId, recipePhotoFormData).then(() => productId);
          }
          return Promise.resolve(productId);
        })
      .then(productId => Promise.all(deets.recipe.steps.map((s, idx) => {
          const initialPhoto = recipe.steps[idx].photo.initialValue;
          // The photo has changed
          if ((s.photo || initialPhoto) && s.photo !== initialPhoto) {
            const recipeStepPhotoFormData = new FormData();
            recipeStepPhotoFormData.append('photo', s.photo);
            return saveRecipeStepPhoto(productId, idx, recipeStepPhotoFormData);
          }
          return Promise.resolve();
        })))
        .then(success => {
          if (params.id) ingredientActions.dropInfo(params.id);
          return success;
        });
  }

  estimateQuantity(fields, detailedIngredients) {
    const estimateForUnit = (measure, unit, ingredients) => {
      const scaled = getIngredientQuantity(measure, unit, ingredients);
      const rescaled = rescaleUnits({
        amount: scaled,
        unit, measure,
      });
      const unitName = unitDisplayName(rescaled.unit);
      return {
        quantity: rescaled && purchasingRound(rescaled.amount, unitDisplayPrecision(rescaled.unit), unitDisplayQuantum(rescaled.unit)) + ' ' + unitName,
        measure: rescaled && this.props.allMeasures.getIn([measure, 'name']).toLowerCase(),
      };
    };

    const getIngredientQuantity = (measure, unit, ingredients) => ingredients.filter(i => {
        if (!i.quantity.amount.value || !i.quantity.unit.value) return false;

        // only factor ingredient into estimate if we can actually convert
        const ingredientConversions = detailedIngredients.getIn([i.ingredient.value, 'measures']);
        if (!ingredientConversions) return false;
        if (!ingredientConversions.some(c => c.get('measure') == measure)) return false;

        return true;
      })
      .map(i => convert(i.quantity.amount.value,
            i.quantity.unit.value, unit,
            detailedIngredients.getIn([i.ingredient.value, 'measures'])))
      .reduce((a, b) => a + b, 0);

    // control default behaviour of function
    const secondaryDisplayLimit = 0.8;
    // default to lb and liter
    const defaultMeasures = { weight: { measure: 2, unit: 4 }, volume: { measure: 3, unit: 10 } };
    if (this.props.userWeightPref === 'metric') defaultMeasures.weight.unit = 3; // set to kg
    if (this.props.userVolumePref === 'imperial') defaultMeasures.volume.unit = 16; // set to fl oz

    if (!fields.ingredient.value) {
 return {};
}

    const steps = fields.recipe.steps;
    const ingredients = steps.map(s => s.ingredients).reduce((a, b) => a.concat(b), []);

    const weightQuantity = getIngredientQuantity(defaultMeasures.weight.measure, defaultMeasures.weight.unit, ingredients);
    // returns weight.unit quantity of ingredients that have volume measure
    const volumeQuantity = getIngredientQuantity(defaultMeasures.volume.measure, defaultMeasures.weight.unit, ingredients);

    const primaryEstimate = estimateForUnit(defaultMeasures.weight.measure, defaultMeasures.weight.unit, ingredients);
    const secondaryEstimate = estimateForUnit(defaultMeasures.volume.measure, defaultMeasures.volume.unit, ingredients);

    if (weightQuantity * secondaryDisplayLimit <= volumeQuantity) {
      if (primaryEstimate.quantity == 0) {
        return secondaryEstimate;
      } else {
        return {
          ...primaryEstimate,
          secondaryQuantity: secondaryEstimate.quantity,
          secondaryMeasure: secondaryEstimate.measure,
        };
      }
    } else {
      return primaryEstimate;
    }
  }

  getPrintedQuantity = (amount, measure, unit) => {
    const { ...deets } = this.props.values;
    return getRecipeProductionQuantity(
        { amount, measure, unit },
        deets.recipe.isQuantized ? deets.recipe.recipeSize : null,
        Immutable.fromJS(deets.measures),
    );
  };

  prepareBaseOrder = (amount, measure, unit) => getRecipeBaseOrder(this.props.values, { amount, measure, unit }, true);

  static prepareBreakdownRecipeOrder(formValues, costingUnitId, costingMeasureId, isFromRecipeEditForm) {
    const unquantizedDeets = { ...formValues, isQuantized: false };

    return getRecipeBaseOrder(
        unquantizedDeets,
        { amount: 1, unit: costingUnitId, measure: costingMeasureId },
        isFromRecipeEditForm,
    );
  }

  makeExtraButtons() {
    return <PrintButton onClick={this.props.uiStateActions.openPrintModal} />;
  }

  resizeRecipe = (newSize, newBatchSize) => {
    const {
      recipe: {
        recipeSize: recipeSizeFields,
        batchSize,
        steps,
      },
      measures: measureFields,
    } = this.props.fields;

    const conversions = Immutable.fromJS(measureFields.map(shallowToObject));
    const oldSize = shallowToObject(recipeSizeFields);
    const scalingFactor = newSize.unit
        ? divideQuantities(newSize, oldSize, conversions)
        : Number(newSize.amount) / oldSize.amount;

    recipeSizeFields.amount.onChange(newSize.amount);
    recipeSizeFields.measure.onChange(newSize.measure);
    recipeSizeFields.unit.onChange(newSize.unit);
    batchSize.onChange(newBatchSize);

    steps.map((step) => {
      step.ingredients.map((ingredient) => {
        const quantity = ingredient.quantity;
        if (quantity.amount.value && quantity.unit.value) {
          const { amount: newAmount, unit: newUnit } = rescaleUnits({
            ...shallowToObject(quantity),
            amount: quantity.amount.value * scalingFactor,
          });
          quantity.amount.onChange(newAmount);
          quantity.unit.onChange(newUnit);
        }
      });
    });

    this.props.uiStateActions.closeResizeModal();
  };

  usesDummyConversion() {
    // if we have only one, we don't want the user to have to mess with it if
    // they want to change the recipe size, so we just change it for them
    return this.props.fields.measures.length <= 1;
  }

  // creates a conversion for new nested recipes or sets unit as primary for recipes with measures
  createOrUpdateDummyConversion = (newUnit) => {
    const {
      fields: {
        measures: conversionFields,
      },
      allMeasures,
    } = this.props;
    const newMeasure = getMeasure(newUnit);

    /* Create default conversion if needed for this measure*/
    const candidateNewConversion = {
      conversion: 1,
      conversionMeasure: newMeasure,
      conversionUnit: newUnit,
      amount: 1,
      measure: newMeasure,
      preferredUnit: newUnit,
    };

    if (conversionFields.length === 1) {
      // might end up being a no-op, but might not
      conversionFields.removeField();
    }

    conversionFields.addField(candidateNewConversion);
  };

  changeRecipeSizeUnit = (newUnit) => {
    if (this.usesDummyConversion()) {
      this.createOrUpdateDummyConversion(newUnit);
    }

    this.props.fields.recipe.recipeSize.unit.onChange(newUnit);
  };

  handlePhotoChange(file) {
    uploadImage(file)
        .then(({ photoUrl, processedFile }) => {
          this.setState({ recipePhotoUrl: photoUrl });
          this.props.fields.photo.onChange(processedFile);
        });
  }

  handleAddPhoto(e) {
    e.preventDefault();
    this.handlePhotoChange(e.target.files[0]);
  }

  handleDropPhoto(files) {
    this.handlePhotoChange(files[0]);
  }

  previewPhoto(e) {
    this.setState({
      previewingPhoto: this.state.recipePhotoUrl,
      previewPhotoDistanceFromTop: e.target.getBoundingClientRect().top,
    });
  }

  cancelPreviewPhoto() {
    this.setState({
      previewingPhoto: null,
    });
  }

  removePhoto() {
    this.setState({
      removingPhoto: true,
    });
  }

  cancelRemovePhoto() {
    this.setState({
      removingPhoto: false,
    });
  }

  handlePhotoClear() {
    this.setState({
      recipePhotoUrl: undefined,
      previewingPhoto: null,
      removingPhoto: false,
    });

    this.props.fields.photo.onChange(null);
    if (this.addPhotoInputRef) { // HTML file input doesn't respect null value passed in attrs
      this.addPhotoInputRef.value = null;
    }
  }

  openEditIngredientModal(productId) {
    this.setState({
      editingIngredient: productId,
    });
  }

  closeEditIngredientModal() {
    this.setState({
      editingIngredient: null,
    });
  }

  renderBody() {

    let {
      name, description, measures, recipe, ingredientTags, recipeTags, inventoryTags, ingredient, salable,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit, photo, ownerName,
      info: { lowestPricePreferred },
    } = this.props.fields;
    const steps = recipe.steps;
    const revisionHistory = recipe.history;
    const recipeNutrients = recipe.recipeNutrients;
    const {
      allMeasures, allIngredients, allNutrients,
      detailedIngredients, uiState, uiStateActions, logoUrl,
      appFileDraggingOver, readonlyUser, permissionLevel, syncedRecipe,
      upstreamProductInfo, values: formValues, valid, handleSubmit,
    } = this.props;
    const isOperationUser = permissionLevel === 'operations';
    const disabledEditing = readonlyUser || syncedRecipe;
    const { showConversions } = this.state;

    const conversions = Immutable.fromJS(measures.map(shallowToObject));

    const conversionsReady = (!this.sourceId() || measures.length > 0) && Boolean(allMeasures);
    function onChangeConversionMeasure(oldMeasure, newMeasure) {
      if (oldMeasure == newMeasure) return;

      for (let quantityName of ['recipeSize', 'maxBatch']) {
        let quantityMeasure = recipe[quantityName].measure.value;
        if (quantityMeasure == oldMeasure && quantityMeasure != newMeasure) {
          recipe[quantityName].measure.onChange(undefined);
          recipe[quantityName].unit.onChange(undefined);
        }
      }
    }

    const [
      sizeInputControlWidth, sizeInputControlWidthMd,
    ] = [6, 3];

    const costInformationLoading = this.checkIfCostInfoLoading(formValues, detailedIngredients, allMeasures);
    const costPerRecipe = costInformationLoading ? undefined : RecipeEdit.getCostPerRecipe(formValues, detailedIngredients);
    let {
        costPerRecipeWarning,
        costBreakdown,
    } = this.costPerRecipe(this.props.fields, this.props.detailedIngredients, this.props.allMeasures, syncedRecipe);

    const {
      costPerUnit, costingUnitId, costingUnitName, costingMeasureId, costPerUnitWarning,
    } = costInformationLoading || !allMeasures ? {} :
        RecipeEdit.costPerUnit(
            costPerRecipe,
            _.get(formValues, ['recipe', 'recipeSize']),
            !ingredient.value,
            conversions,
            allMeasures);

    const { quantity: quantityEstimate, measure: quantityEstimateMeasure,
            secondaryQuantity: secondaryQuantityEstimate, secondaryMeasure: secondaryQuantityMeasure } =
        this.estimateQuantity(this.props.fields, detailedIngredients);

    const noNutrientInfoWarnings = this.examineIngredientNutrients(steps, detailedIngredients);

    let costWarnings = [];
    if (costPerRecipeWarning) {
      costWarnings.push(<HideableText key="perRecipe"
          nonHiddenPart="The following ingredients are not included in the cost calculation:"
      >
        {costPerRecipeWarning}
      </HideableText>);
    }

    {
      let originalIngredientOnChange = ingredient.onChange;
      ingredient = { ...ingredient }; // don't want to modify original, since that'll persist across calls
      ingredient.onChange = ev => {
        const newValue = ev.target.checked;
        originalIngredientOnChange(newValue);
        // In the future, might want this to be orthogonal to 'ingredient', hence it being a separate value
        // in the API, and logic in RecipeEdit depending directly on salable.value rather than on !ingredient.value
        salable.onChange(!newValue);

        if (newValue) {
          nutrientServingMeasure.onChange(nutrientServingMeasure.initialValue);
          nutrientServingUnit.onChange(nutrientServingUnit.initialValue);
          // remove price if recipe marked as sub-recipe
          recipe.price.onChange(undefined);
        } else {
          // just need to call removeField the right number of times. no single clearField to call, sadly
          for (let i in measures) {
            measures.removeField();
          }
          recipe.maxBatch.unit.onChange(undefined);
          recipe.maxBatch.measure.onChange(undefined);
          nutrientServingMeasure.onChange(undefined);
          nutrientServingUnit.onChange(undefined);
          recipe.price.onChange(recipe.price.initialValue);
        }
        // clear fields, disable error messages with untouch
        let clearFields = ['amount', 'unit', 'measure'];
        clearFields.forEach(field => recipe.recipeSize[field].onChange(undefined));
        clearFields.forEach(field => this.props.untouch(recipe.recipeSize[field].name));
      };
    }

    const recipePrice = recipe.price.value;
    const hasCostWarnings = costPerRecipeWarning;
    const hasNutrientWarnings = noNutrientInfoWarnings.length;

    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);
    const haveEnoughNutrientData = _.every(nutrient_fields, nndbsrId => nndbsrIdToNutrientIdMap[nndbsrId]);

    let onBlurForTitle = (e) => {
      if (!this.state.titleUserClick) {
       e.preventDefault();
       this.setState({ titleUserClick: true });
      } else {
        name.onBlur(name.value);
      }
    };

    if (allNutrients && !haveEnoughNutrientData) {
      noticeError('Lacking nutrient data for NutritionDisplay');
    }

    let photoAddButton = null;
    if (disabledEditing) {
      if (this.state.recipePhotoUrl) {
        photoAddButton = <div className="recipe-edit-preview-photo">
          <img
              draggable="false"
              src={this.state.recipePhotoUrl}
              onClick={this.previewPhoto}
          />
        </div>;
      }
    } else if (this.state.recipePhotoUrl) {
      photoAddButton = <div>
        <div className="recipe-edit-add-photo">
            <span className="new-link" onClick={this.removePhoto}>
              - Photo
          </span>
        </div>
        <div className="recipe-edit-preview-photo">
          <img
              draggable="false"
              src={this.state.recipePhotoUrl}
              onClick={this.previewPhoto}
          />
        </div>
      </div>;
    } else {
      photoAddButton = <div className="recipe-edit-add-photo">
          <span className="new-link" onClick={() => {
            if (this.addPhotoInputRef) this.addPhotoInputRef.click();
          }}>
            + Photo
        </span>
        <FormControl style={{ display: 'none' }} type="file" inputRef={ref => this.addPhotoInputRef = ref}
            accept="image/*"
            {...photo}
            onChange={this.handleAddPhoto}
            onBlur={null}
            value={null} />
      </div>;
    }

    const costFixed = costPerUnit && costPerUnit.toFixed(2);
    const foodCostPortion = !costInformationLoading && recipePrice && costPerUnit ? costPerUnit / recipePrice : undefined;

    // Used in Menus
    let usedInMenusDisplay;
    if (!salable.value || this.isCreatingNew() || readonlyUser) {
      usedInMenusDisplay = null;
    } else if (this.state.usedMenus) {
      usedInMenusDisplay = <ListGroup className="used-in-menus-list">
        {this.state.usedMenus.map((menu) =>
            <ListGroupItem key={menu.get('id')}>
              <a className="non-obvious-link" href={'/menus/' + menu.get('id')}>
                {menu.get('name')}
              </a>
            </ListGroupItem>,
        )}
      </ListGroup>;
    } else {
      usedInMenusDisplay = <LoadingSpinner />;
    }

    // Revision History
    const dateDisplay = (dateString) => {
      const date = moment(dateString);
      return date.format(dateTimeShortFormat);
    };
    let revisionHistoryEntries = [];
    if (recipe.createdAt.value) {
      revisionHistoryEntries = [<ListGroupItem key="creation">
        Created by {ownerName.value} {recipe.createdAt.value ? `on ${dateDisplay(recipe.createdAt.value)}` : ''}
      </ListGroupItem>];
    }
    revisionHistoryEntries = revisionHistoryEntries.concat(revisionHistory.filter(
        entry => entry.ownerName.value,
    ).map(entry => <ListGroupItem key={entry.id.value}>
        Modified by {entry.ownerName.value} on {dateDisplay(entry.lastModified.value)}
    </ListGroupItem>));

    return (
        <div className="recipe-edit">
          <Form className="recipe-edit-metadata-section" horizontal style={{ position: 'relative' }}>
            { disabledEditing ? null : <Dropzone
              className={!this.state.recipePhotoUrl && appFileDraggingOver ? 'recipe-photo-active-dropzone' : 'recipe-photo-dropzone'}
              acceptClassName="recipe-photo-active-dropzone"
              rejectClassName="recipe-photo-reject-dropzone"
              disabled={Boolean(this.state.recipePhotoUrl)}
              disableClick
              accept="image/*"
              onDrop={this.handleDropPhoto}
            >
              DROP PHOTO HERE
            </Dropzone>}
            <Row>
              <Col xs={12} md={6}>
                <TitleInput
                    disabled={disabledEditing}
                    inputRef={input => this.initialInput = input}
                    {...name}
                    noun="Recipe"
                    onBlur={onBlurForTitle}
                    style={ingredient.value ? { fontStyle: 'italic' } : null}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={6}>
                <FormGroup>
                  <ControlLabel>Description:</ControlLabel>
                  {!(disabledEditing) ?
                    <Validate {...description}>
                      <FormControl
                        componentClass="textarea"
                        placeholder="Add description (optional)..."
                        {...description}
                      />
                    </Validate>
                  : <div className="readonly-description" style={syncedRecipe ? { backgroundColor: '#fff' } : {}}>
                      {description.value}
                  </div>}
                  { photoAddButton }
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={6}>
                <FormGroup className="recipe-category-form">
                  <Col xs={12} componentClass={ControlLabel}>Recipe Categories:</Col>
                  <Col xs={12}>
                    <TagSelector
                        disabled={readonlyUser}
                        {...recipeTags}
                        optional
                        tagType="recipes"
                        optionName="category"
                    />
                  </Col>
                </FormGroup>
              </Col>
              {
                !ingredient.value ?
                <Col xs={12} md={2} mdOffset={4}>
                  <FormGroup className="recipe-category-form">
                    <Col xs={12} componentClass={ControlLabel}>Recipe Item Number:</Col>
                    <Col xs={12} >
                      {!disabledEditing ?
                        <Validate {...recipe.itemNumber}>
                          <FormControl
                            placeholder="Add Item Number (optional)"
                            {...recipe.itemNumber}
                          />
                        </Validate>
                      : <div className="readonly-description" style={syncedRecipe ? { backgroundColor: '#fff' } : {}}>
                          {recipe.itemNumber.value}
                      </div>}
                    </Col>
                  </FormGroup>
                </Col>
                : null
              }
            </Row>
          </Form>
          {formValues && (featureIsAvailable('NUTRITIONAL_INFO') || featureIsSupported('NUTRITIONAL_INFO')) && haveEnoughNutrientData ?
              <Section
                  title="Nutrition Facts"
                  style={{ position: 'relative' }}
                  toggleButtonStyle={{ marginLeft: '15px' }}
                  toggleCondition={() => {
                    const isValid = !recipeNutrients.packaged.value || (recipeNutrients.servingAmount.value && recipeNutrients.servingUnit.value);
                    if (!isValid) {
                      // to show error tooltips
                      recipeNutrients.servingAmount.touched = true;
                      recipeNutrients.servingUnit.touched = true;
                      this.forceUpdate();
                    }
                    return isValid;
                  }}
                  collapsible
                  onEnter={() => this.setState({ showNutrition: true })}
                  onExit={() => {
                    this.setState({ showNutrition: false });
                  }}
                  feature="NUTRITIONAL_INFO"
                  additionalButtons={[
                    <PrintButton key="print-button" style={{ marginLeft: '6px' }} onClick={() => this.props.uiStateActions.openPrintNutritionModal()}/>,
                  ]}
              >
                <NutritionDisplay
                    productType="recipes"
                    isSubRecipeInRecipeEdit={ingredient.value}
                    allNutrients={allNutrients}
                    detailedIngredients={detailedIngredients}
                    conversions={conversions}
                    measures={allMeasures}
                    recipeNutrients={recipeNutrients}
                    nutrientServingAmount={nutrientServingAmount}
                    nutrientServingMeasure={nutrientServingMeasure}
                    nutrientServingUnit={nutrientServingUnit}
                    recipe={formValues.recipe}
                    isReadOnly={readonlyUser}
                />
                {hasNutrientWarnings && this.state.showNutrition ?
                    <Alert bsStyle="info" className="recipe-status-alert cost-alert">
                      <HideableText
                          nonHiddenPart="The following ingredients are not included in the Nutrition Facts:"
                      >
                        {noNutrientInfoWarnings}
                      </HideableText>
                    </Alert>
                    : null}
              </Section>
              : null}
          <Section id="recipe-edit-yield-and-cost">
            {!readonlyUser ?
                <React.Fragment>
                  <Row id="recipe-edit-yield-and-cost-title">
                    <Col xs={12}>
                      <div>
                        <h4>{costPerUnitWarning || 'Yield and Cost'}</h4>
                        <Checkbox
                          disabled={syncedRecipe}
                          {...ingredient}
                          onChange={isSubRep => {
                            recipe.advancePrep.onChange(isSubRep); // sub-recipes are by default advance-prep, regular by default not
                            lowestPricePreferred.onChange(true);
                            ingredient.onChange(isSubRep);
                          }}
                        >
                          <span>This is a Sub-Recipe, can be used as in ingredient</span>
                        </Checkbox>
                      </div>
                    </Col>
                  </Row>
                </React.Fragment>
            : null}
            <Row>
              <Col xs={12} md={3}>
                <Form horizontal>
                  <FormGroup controlId="recipe-size">
                    <Col componentClass={ControlLabel} xs={12} md={2}>
                      Yields:
                    </Col>
                    {allMeasures ?
                        <Col componentClass={RecipeSize} xs={4} md={6}
                             recipeBatchClassName={ingredient.value ? 'recipe-edit-sub-recipe-batch-size' : 'recipe-edit-batch-size'}
                             batchSizeField={recipe.batchSize}
                             batchLabel={recipe.batchLabel}
                             definingBatches
                             multiMeasure
                             useUnits={ingredient.value}
                             amountField={recipe.recipeSize.amount}
                             measureField={recipe.recipeSize.measure}
                             unitField={{
                              ...recipe.recipeSize.unit,
                              onChange: this.changeRecipeSizeUnit,
                              value: recipe.recipeSize.unit.value,
                             }}
                             nutrientServingAmountField={nutrientServingAmount}
                             nutrientServingMeasureField={nutrientServingMeasure}
                             nutrientServingUnitField={nutrientServingUnit}
                             measures={allMeasures}
                             conversions={conversions}
                             ignoreConversions={this.usesDummyConversion()}
                             disabled={disabledEditing}
                        />
                        : <Col xs={sizeInputControlWidth} md={sizeInputControlWidthMd}><LoadingSpinner /></Col>
                    }
                  </FormGroup>
                </Form>
              </Col>
            </Row>
            {!readonlyUser && (quantityEstimate || secondaryQuantityEstimate) ?
                <Row style={{ marginBottom: '12px' }}>
                  <Col xs={12} md={5}>
                    {quantityEstimate ?
                        <span>Raw ingredient {quantityEstimateMeasure} is approx. {quantityEstimate}</span>
                        : null}
                    {secondaryQuantityEstimate ?
                        <span> and {secondaryQuantityMeasure} is {secondaryQuantityEstimate}</span>
                        : null}
                  </Col>
                </Row>
            : null}
            { !ingredient.value ? null
                : [
                  <Row style={{ margin: '12px 0px 5px' }} key="ingredient-tags">
                    <FormGroup>
                      <Col xs={2} componentClass={ControlLabel}>GL Categories:</Col>
                      <Col xs={10} md={6}>
                        <TagSelector
                            {...ingredientTags}
                            optional
                            tagType="ingredients"
                            optionName="category"
                        />
                      </Col>
                    </FormGroup>
                  </Row>,
                  <Row style = {{ marginBottom: '5px' }} key="inventory-tags">
                    <FormGroup>
                      <Col xs={2} componentClass={ControlLabel}>Ingredient Locations:</Col>
                      <Col xs={10} md={6}>
                        <Validate compact {...inventoryTags}>
                          <TagSelector
                              {...inventoryTags}
                              optional
                              tagType="inventories"
                              optionName="location"
                          />
                        </Validate>
                      </Col>
                    </FormGroup>
                  </Row>
                ]
            }
            <Row>
              <Col xs={12} md={3} className="remove-padding">
                {!readonlyUser ?
                    <Checkbox {...recipe.isQuantized} disabled={syncedRecipe}>
                      <TextWithHelp helpText="If checked, this recipe can only be produced in whole multiples of the recipe size">
                        Recipe size is a basic unit
                      </TextWithHelp>
                    </Checkbox>
                    : recipe.isQuantized.value ? 'This recipe is a basic unit' : ''}
              </Col>
              {
                ingredient.value ?
                  <Col xs={12} md={6} className="remove-padding">
                    <Checkbox {...recipe.advancePrep} disabled={syncedRecipe}>
                      <span>Advance Prep</span>
                    </Checkbox>
                  </Col>
                : null
              }
            </Row>
            {!readonlyUser ?
              <Row id="recipe-edit-cost-row" key="row-cost-info" style={{ marginTop: '12px' }}>
                <Col md={3} xs={12} className="remove-padding">
                  <div className="recipe-edit-cost-display">
                    <p className="recipe-edit-cost-label font-18">Cost per {costingUnitName}:</p>
                    { costPerUnit ? <p className="recipe-edit-cost-label font-18"> {'$' + costFixed} </p> : null }
                  </div>
                </Col>
                {costPerUnit != null ?
                    [
                      costInformationLoading ? <Col md={6} xs={12}><LoadingSpinner /></Col> :
                          !ingredient.value && (costPerUnit != null) ?
                              [
                                <Col md={3} xs={12} className="remove-padding">
                                  <div className="recipe-edit-cost-display" style={{ alignItems: 'center' }}>
                                    <p className="recipe-edit-cost-label">Price per {costingUnitName}: </p>
                                    <div style={{ width: '3em' /* had to do this to get it to match percentInput !important styles */ }}>
                                      <CurrencyInput {...recipe.price} value={recipePrice} />
                                    </div>
                                  </div>
                                </Col>,
                                <Col md={3} xs={12} className="remove-padding">
                                  <div className="recipe-edit-cost-display">
                                    <p className="recipe-edit-cost-label" > Food Cost Percent: </p>
                                    <PercentInput onChange={(val) => recipe.price.onChange(costPerUnit / val)}
                                        value={foodCostPortion} />
                                    <p className="recipe-percent-sign"> %</p>
                                  </div>
                                </Col>,
                              ]
                              : <Col md={6} xs={12} />,
                    ] : <Col md={6} xs={12} /> // spacer to replace price inputs
                }
                {costInformationLoading ? null :
                  <Col md={2} xs={4}>
                    <Button
                      className="btn-recipe-edit-md-break"
                      style={{ float: 'right' }}
                      onClick={
                        valid ?
                        () => uiStateActions.openCostModal()
                        : handleSubmit(() => {})
                      }>
                      Cost Breakdown
                      </Button>
                  </Col>
                }
              </Row>
            : null}
          </Section>
          {!readonlyUser && ingredient.value ?
            <Section title="Measurement Conversions" collapsible>
              <br/>
              { conversionsReady ?
                <FieldArray
                  component={ConversionEditor}
                  fields={measures}
                  props={{
                    uniqueName: 'recipe-edit-conversion-editor-main',
                    onChangeMeasure: onChangeConversionMeasure.bind(this),
                    usedMeasures: this.usedMeasuresWithMessages(),
                    allMeasures,
                    reverseDependencies: this.reverseDependencies,
                    upstreamProductInfo,
                  }}
                /> : <LoadingSpinner /> }
            </Section>
          : null }
          <Section style={{ 'position': 'relative' }} title="Ingredients & Preparation Instructions">
              <Button className="btn-recipe-edit-right" style={{ 'top': '9px' }} disabled={!recipe.recipeSize.amount.value} onClick={() => uiStateActions.openResizeModal()}>
                Scale Recipe
              </Button>
            { allIngredients && allMeasures ?
                <FieldArray component={RecipeStepsEditor}
                  fields={steps}
                  props={{
                    appFileDraggingOver, logoUrl,
                    itemsDraggedHook: () => this.setState({ itemsDragged: true }),
                    readonlyUser, syncedRecipe, disabledEditing,
                    openEditIngredientModal: this.openEditIngredientModal,
                  }}
                />
              : <LoadingSpinner />
            }
            { (!readonlyUser || isOperationUser) && hasCostWarnings ? <Alert bsStyle="info" className="recipe-status-alert cost-alert">
              {costWarnings}
            </Alert>
            : null }
          </Section>



          <Section title="Used in Menus" collapsible toggleButtonStyle={{ marginLeft: '10px' }}>
            {usedInMenusDisplay}
          </Section>
          <Section title="Revision History" collapsible>
            <ListGroup className="used-in-menus-list">
              {revisionHistoryEntries}
            </ListGroup>
          </Section>

        { uiState.get('showIngredientCostModal') ?
          <IngredientSourcesModal key="ingredient-edit-cost-modal"
                                  ingredientId={uiState.get('showIngredientCostModal')}
                                  isHidden={uiState.get('showIngredientCostModalIsHidden')}
                                  hide={uiStateActions.closeIngredientCostModal} />
          : null }

        { uiState.get('showIngredientNutrientModal') ?
          <IngredientNutrientModal key="ingredient-edit-nutrient-modal"
                                   ingredientId={uiState.getIn(['showIngredientNutrientModal', 'ingredientId'])}
                                   hide={() => {
                                     getMultipleProductsUsageInfo(uiState.getIn(['showIngredientNutrientModal', 'preppedIngredientIds']));
                                     uiStateActions.closeIngredientNutrientModal();
                                   }} />
          : null }

        { uiState.get('showPrintModal') ?
          <PrintRecipeModal key="print-modal"
             page="recipe"
             shouldPrintRecipes
             batchSize={recipe.batchSize.value}
             batchLabel={recipe.batchLabel.value}
             baseRecipe={this.props.values}
             getBaseOrder={this.prepareBaseOrder}
             getPrintedQuantity={this.getPrintedQuantity}
             amount={recipe.recipeSize.amount.value}
             measure={recipe.recipeSize.measure.value}
             unit={recipe.recipeSize.unit.value}
             ingredient={ingredient.value}
             allMeasures={allMeasures}
             conversions={conversions}
             companyLogoUrl={this.props.companyLogoUrl}
             hide={uiStateActions.closePrintModal} />
          : null }

        { uiState.get('showPrintNutritionModal') ?
          <PrintRecipeNutritionModal key="print-nutrition-modal"
            page="recipes"
            allNutrients={allNutrients}
            detailedIngredients={detailedIngredients}
            isSubRecipeInRecipeEdit={ingredient.value}
            conversions={conversions}
            measures={allMeasures}
            recipeNutrients={recipeNutrients}
            nutrientServingAmount={nutrientServingAmount}
            nutrientServingMeasure={nutrientServingMeasure}
            nutrientServingUnit={nutrientServingUnit}
            name={name.value}
            recipe={formValues.recipe}
            companyLogoUrl={this.props.companyLogoUrl}
            hide={uiStateActions.closePrintNutritionModal} />
          : null }

        { uiState.get('showResizeModal') ?
          <ResizeRecipeModal key="resize-modal"
             batchSize={recipe.batchSize.value}
             batchLabel={recipe.batchLabel.value}
             resizeRecipe={this.resizeRecipe}
             amount={recipe.recipeSize.amount.value}
             measure={recipe.recipeSize.measure.value}
             unit={recipe.recipeSize.unit.value}
             isQuantized={recipe.isQuantized.value}
             ingredient={ingredient.value}
             allMeasures={allMeasures}
             conversions={conversions}
             hide={uiStateActions.closeResizeModal}
             readonlyUser={readonlyUser}
          />
          : null }

        { uiState.get('showCostModal') ?
          <CostBreakdown
            key="cost-modal"
            isRecipe
            name={name.value}
            totalCost={costFixed}
            totalPrice={recipePrice}
            foodCostPercent={toPercent(foodCostPortion)}
            baseOrder={RecipeEdit.prepareBreakdownRecipeOrder(this.props.values, costingUnitId, costingMeasureId, true)}
            hide={uiStateActions.closeCostModal}
            costingUnitName={costingUnitName}
            companyLogoUrl={this.props.companyLogoUrl}
          />
          : null }

        { uiState.get('showNewIngredientModal') ?
          <NewIngredientModal key="new-ingredient-modal"
             newIngredient={uiState.get('showNewIngredientModal')}
             isHidden={uiState.get('showNewIngredientModalIsHidden')}
             uiStateName="recipeEdit"
             hide={uiStateActions.closeNewIngredientModal} />
          : null }

        { uiState.get('showNewUnitModal') ?
          <NewUnitModal key="new-unit-modal"
             newUnit={uiState.get('showNewUnitModal')}
             uiStateName="recipeEdit"
             hide={uiStateActions.closeNewUnitModal}
          />
          : null }

        { this.state.previewingPhoto ?
          <ViewPhotoModal
            photoURL={this.state.previewingPhoto}
            previewPhotoDistanceFromTop={this.state.previewPhotoDistanceFromTop}
            matchPreviewPhotos="top"
            isHidden={this.state.removingPhoto}
            onDelete={this.removePhoto}
            onHide={this.cancelPreviewPhoto}
            readonlyUser={readonlyUser}
          />
        : null}

        {this.state.removingPhoto ?
          <DeleteConfirmationModal
            onHide={this.cancelRemovePhoto}
            onDelete={this.handlePhotoClear}
            deleteVerb="remove"
            titleNoun="Photo"
            fullDescription="Are you sure you want to remove this photo?"
            className="remove-recipe-preview-photo"
          />
        : null}

        {this.state.editingIngredient ?
          <EditIngredientModal
            onHide={this.closeEditIngredientModal}
            productId={this.state.editingIngredient}
          />
        : null}

          <Onboarding pageKey="recipe_editor" />
        </div>);
  }

  render() {
    const { readonlyUser, syncedRecipe } = this.props;

    const className = classNames('recipe-edit-page', {
      ['readonly-recipe-page']: readonlyUser,
      ['synced-recipe']: !readonlyUser && syncedRecipe,
    });

    const view = super.render();
    return <div className={className}>
      {view}
    </div>;
  }

  deleteItem() {
    return deleteProduct(this.props.params.id);
  }

  stillLoading(props = this.props) {
    return super.stillLoading(props) || !props.allMeasures;
  }

  setupRecipe(productId) {
    if (productId !== null) {
      getProductDetails(productId).then(product => {
        // get recipe for ingredient and each preparation
        const ingredients = [productId,
          ..._.flatMap(product.simplePreps, val => val.outputProduct)];
        this.reverseDependencies = {};
        ingredients.forEach(ingredient => {
          getReverseDependencies(ingredient)
              .then(recipes => this.reverseDependencies[ingredient] = recipes);
        });

        // pre-fetch usage info of ingredients in this recipe's steps
        this.fetchIngredientInfos(Immutable.fromJS(product.recipe.steps));
      });
    }
  }

  componentWillMount() {
    super.componentWillMount();
    const { router, route } = this.props;
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.readonlyUser) return null;
      // prompt if props is dirty or items are dragged but only when editing stored order
      if (this.props.dirty || (this.state.itemsDragged && !CrudEditor.isCreatingNew(this.props.params))) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });

    if (!this.isCreatingNew() && this.props.menus) {
      this.getRecipeMenus(this.props.menus);
    }

    getMeasures();
    getMenuList();
    getIngredientList();
    getOnboardingInfo();
    getNutrients();
    getRecipeList();

    this.setupRecipe(this.sourceId());
  }

  componentWillUnmount() {
    if (this.sourceId() !== null) {
      this.props.productActions.dropDetails(this.sourceId());
      if (!this.isCopying()) {
        this.props.reverseDependencyActions.drop(this.sourceId());
      }
    }
  }

  listUrl = '/recipes';
  noun = 'recipe';

  /*
   * All the stuff dealing with ingredient usage
   */

  usedMeasuresWithMessages() {
    let returnValue = Immutable.Map();

    let sourceFields = this.props.fields.sources;
    returnValue = returnValue.merge(
        sourceFields.map(s => s.measure.value)
            .filter(_.identity)
            .map(measureId => [measureId, 'supplier/s']),
    );

    return returnValue;
  }

  componentWillReceiveProps(nextProps) {
    // Switched to another recipe
    if (nextProps.params.id !== this.props.params.id) {
      this.setupRecipe(nextProps.params.id);
    }
    if (nextProps.dirty && nextProps.fields.recipe.steps.values !== this.props.fields.recipe.steps.values) {
      this.fetchIngredientInfos(nextProps.fields.recipe.steps);
    }

    if (!this.isCreatingNew() && !Immutable.is(this.props.menus, nextProps.menus)) {
      this.getRecipeMenus(nextProps.menus);
    }

    if (this.props.recipePhotoUrl !== nextProps.recipePhotoUrl) {
      this.setState({
        recipePhotoUrl: nextProps.recipePhotoUrl,
      });
      if (this.addPhotoInputRef) {
        this.addPhotoInputRef.value = null;
      }
    }
  }

  getRecipeMenus(allMenus) {
    // set up this.state.usedMenus
    const {
        routeParams: {
            id: recipeId,
        },
        menuActions,
    } = this.props;
    Promise.all( // Get the items that are listed in the menu
        allMenus.map((menu, menuId) => getMenuDetails(menu.get('id'))
            .then(menuDetails => {
              menuActions.dropDetails(); // keep things neat in the redux store

              // After the menu info returns, look up each item in the menu
              const usedRecipeIds = _.flatten(menuDetails.sections.map((section) => section.items.map((item) => item.product)));
              const menuIsUsed = usedRecipeIds.indexOf(parseFloat(recipeId)) > -1;
              return allMenus.get(menuId).set('used', menuIsUsed);
            }),
        ),
    ).then(listWithUsedFlags => {
      let usedList = Immutable.List(listWithUsedFlags).filter(menu => menu.get('used'));
      this.setState({ usedMenus: usedList });
    });
  }

  fetchIngredientInfos(steps) {
    const requiredIds = usedIngredientsFromSteps(steps);
        // .subtract(IngredientStore.ingredientIds);
    return getMultipleProductsUsageInfo(requiredIds, 50)
      .then(usageInfos => {
        // need the base ingredients of preps too
        const baseIngredientsOfPrepsIds = _(usageInfos)
                                          .filter(info => info.isPrepOf)
                                          .map(info => info.isPrepOf);

        // need nutrient info of sub-ingredients (PARS-2260)
        const subIngredientIds = _(usageInfos)
                                 .flatMap(info => info.nutritionInfo.ingredientList.map(i => i.id))
                                 .value();

        const additionalFetchIds = baseIngredientsOfPrepsIds.concat(subIngredientIds).filter(id => !requiredIds.includes(id)).value();
        getMultipleProductsUsageInfo(additionalFetchIds, 50);
      });
  }

  checkIfCostInfoLoading(recipeDeets, detailedIngredients, allMeasures) {

    if (!allMeasures) return true;

    if (typeof recipeDeets !== 'undefined' && recipeDeets.recipe.steps.length) {
      for (let step of recipeDeets.recipe.steps) {
        for (let ingredient of step.ingredients) {
          if (!ingredient || !ingredient.ingredient) continue;
          const ingredientId = ingredient.ingredient;
          const details = detailedIngredients.get(ingredientId);

          if (!details) return true;

          const rawIngredientId = details.get('isPrepOf') || ingredientId;

          if (!detailedIngredients.get(rawIngredientId)) return true;
        }
      }
    }

    return false;
  }

  static getCostPerRecipe(recipeDeets, detailedIngredients) {
    let ingredientCosts = [];
    if (typeof recipeDeets !== 'undefined' && recipeDeets.recipe.steps.length) {
      for (let step of recipeDeets.recipe.steps) {
        for (let ingredient of step.ingredients) {
          if (!ingredient || !ingredient.ingredient) continue;

          const ingredientId = ingredient.ingredient;
          const details = detailedIngredients.get(ingredientId);
          const rawIngredientId = details.get('isPrepOf') || ingredientId;
          const rawIngredientDetails = detailedIngredients.get(rawIngredientId);

          const quantity = ingredient.quantity || ingredient;
          if (quantity.amount && quantity.unit && details.has('unitCost')) {
            // If the ingredient has a well-defined unit cost push the subtotal onto a list to be added up
            const ingredientCost = convertPerQuantityValue(details.get('unitCost'),
              1, details.get('primaryUnit'),
              quantity.amount, quantity.unit,
              details.get('measures'),
            );
            ingredientCosts.push(ingredientCost);
          }
        }
      }
    }

    return ingredientCosts.length === 0 ? 0 : ingredientCosts.reduce((a, b) => a + b);
  }

  costPerRecipe(fields, detailedIngredients, allMeasures, isSyncedRecipe) {

    const deets = fields;
    let ingredientCosts = [];

    // these two should contain only *raw* ingredient IDs
    let noCostIngredients = Immutable.Set();
    let noQuantityIngredients = Immutable.Set();

    let costBreakdown = [];
    if (!allMeasures) return { costInformationLoading: true }; // won't be able to do math otherwise

    // If the recipe has steps
    if (typeof deets !== 'undefined' && deets.recipe.steps.length) {
      for (let step of deets.recipe.steps) {
        for (let ingredient of step.ingredients) {
          if (!ingredient.ingredient.value) continue;

          const quantity = ingredient.quantity;
          let ingredientId = ingredient.ingredient.value;
          const details = detailedIngredients.get(ingredientId);

          if (!details) continue;

          const rawIngredientId = details.get('isPrepOf') || ingredientId;
          const rawIngredientDetails = detailedIngredients.get(rawIngredientId);

          // we need this info too, if we're going to successfully display costing warnings
          // IngredientUsage's componentDidMount() handler will make sure we get it
          if (!rawIngredientDetails) return { costInformationLoading: true };

          if (quantity.amount.value && quantity.unit.value && details.has('unitCost')) {
            // If the ingredient has a well-defined unit cost push the subtotal onto a list to be added up
            const ingredientCost = convertPerQuantityValue(details.get('unitCost'),
                1, details.get('primaryUnit'),
                quantity.amount.value, quantity.unit.value,
                details.get('measures'));

            costBreakdown.push({
              ingredient: ingredientId,
              ingredientCost,
            });
          } else {
            costBreakdown.push({
              ingredient: ingredientId,
              ingredientCost: undefined, // so the user will still see the ingredients
            });

            /**
             *  give feedback to the user if we can
             */

            // prepped ingredients are all inHouse, so check the raw version
            if (!rawIngredientDetails.get('inHouse') && !details.has('unitCost')) {
              noCostIngredients = noCostIngredients.add(rawIngredientId);
            }

            if (!(quantity.amount.value && quantity.unit.value)
              && !ingredient.ingredient.active && !ingredient.quantity.amount.active && !ingredient.quantity.unit.active) {
              noQuantityIngredients = noQuantityIngredients.add(rawIngredientId);
            }
          }
        }
      }
    }

    let recipeWarnings = Immutable.Map();

    for (const ingredientId of isSyncedRecipe ? noCostIngredients : noCostIngredients.union(noQuantityIngredients)) {
      let warnings = [];
      const detailedIngredient = detailedIngredients.get(ingredientId);

      if (noQuantityIngredients.includes(ingredientId)) {
        warnings.push(<li key="noquantity">
          No quantity entered
          { /* <Button style={{float: "right"}} onClick={() => somehowFocusTheInput()}>Specify Quantity</Button> */}
        </li>);
      }

      if (noCostIngredients.includes(ingredientId)) {
        warnings.push(<li key="nocost">
          No cost info
          <Button
              disabled={this.props.permissionLevel === 'recipe-read-only' || this.props.syncedRecipe}
              onClick={() => this.props.uiStateActions.openIngredientCostModal(ingredientId)}
          >
            Update
          </Button>
        </li>);
      }

      recipeWarnings = recipeWarnings.set(ingredientId, <div>
        <h5>{ detailedIngredient.get('name') }</h5>
        <ul key={ingredientId} className="recipe-alerts-list">
          {warnings}
        </ul>
      </div>);
    }

    const costPerRecipeWarning = recipeWarnings.isEmpty() ? null :
        recipeWarnings.sortBy((warnings, ingredientId) =>
            detailedIngredients.getIn([ingredientId, 'name']).toLowerCase(),
        ).valueSeq().toArray();

    return {
      costPerRecipeWarning,
      costBreakdown,
    };
  }

  /*
   * Select a display unit for the cost, and calculate the cost per unit
   *
   * caller needs to make sure all measures are available
   */
  static costPerUnit(costPerRecipe, recipeSize, usesPortions, conversions, allMeasures) {

    let costPerUnit, costPerUnitWarning;
    // a default for error cases
    let costingUnitId = recipeSize.unit;
    let costingMeasureId = recipeSize.measure;
    let costingUnitName;
    if (usesPortions) costingUnitName = 'Portion';
    else if (costingUnitId) costingUnitName = unitDisplayName(costingUnitId, conversions);
    else costingUnitName = 'Unit';

    if (costPerRecipe == null) {
      costPerUnit = null;
    } else if (!recipeSize.amount) {
      costPerUnit = null;
      if (costPerRecipe > 0) {
        costPerUnitWarning = usesPortions ?
          'Enter the Number of Portions to get Cost Per Portion' :
          'Enter the Yield to get Cost';
      }
    } else if (usesPortions) {
      costPerUnit = costPerRecipe / recipeSize.amount;
      costingUnitName = 'Portion';
    } else if (!costingUnitId) {
      costPerUnit = null;
      if (costPerRecipe > 0) costPerUnitWarning = 'Enter the Yield to get Cost';
    } else {
      const fullMeasureInfo = allMeasures.get(costingMeasureId);
      const costingUnit = pickPerQuantityValueDisplayUnit(
          {
            measure: fullMeasureInfo,
            unit: fullMeasureInfo.getIn(['units', costingUnitId]),
          },
          [ // perQuantityValues (only one)
            { val: costPerRecipe, amount: recipeSize.amount, unit: costingUnitId },
          ],
          conversions,
      );

      costPerUnit = convertPerQuantityValue(costPerRecipe,
          recipeSize.amount, costingUnitId,
          1, costingUnit.get('id'),
          conversions,
      );
      costingUnitId = costingUnit.get('id');
      costingUnitName = unitDisplayName(costingUnit.get('id'), conversions);
    }

    return {
      costPerUnit,
      costingUnitId,
      costingUnitName,
      costingMeasureId,
      costPerUnitWarning,
    };
  }

  examineIngredientNutrients(steps, detailedIngredients) {
    if (!featureIsSupported('NUTRITIONAL_INFO')) {
      return [];
    }
    let warnings = [];

    const allIngredientIds = _.flatMap(steps, s =>
        s.ingredients,
    ).map(ingredientUsage => ingredientUsage.ingredient.value);

    let prepsByIngredient = Immutable.Map();
    let rawIngredientIds = Immutable.Set();

    allIngredientIds.forEach(id => {
      const details = detailedIngredients.get(id);
      if (!details) return;
      const rawId = details.get('isPrepOf');
      if (rawId) {
        prepsByIngredient = prepsByIngredient.update(rawId, Immutable.List(), preps => preps.push(id));
        rawIngredientIds = rawIngredientIds.add(rawId);
      } else if (details.get('inHouse')) {
        // Replace sub-recipes with their sub-ingredients (PARS-2260)
        rawIngredientIds = rawIngredientIds.concat(details.getIn(['nutritionInfo', 'ingredientList']).map(i => i.get('id')));
      } else {
        rawIngredientIds = rawIngredientIds.add(id);
      }
    });

    for (let id of rawIngredientIds) {
      const deets = detailedIngredients.get(id);
      if (!deets) continue;
      if (deets.getIn(['nutritionInfo', 'characteristicList']).includes('non-edible')) continue; // skip non-edible ingredients
      if (!deets.get('nutrientServingAmount') || !deets.get('nutrientServingUnit')) {
        if (deets.getIn(['nutritionInfo', 'nutritions']).some(amount => amount)) {
          warnings.push(
              <div>
                <h5>{ deets.get('name') }</h5>
                <ul key={id} className="recipe-alerts-list">
                  <li key="no-serving-size">
                    No serving size
                    <Button onClick={() => this.props.uiStateActions.openIngredientNutrientModal(
                        id, prepsByIngredient.get(id, Immutable.List()).toJS(),
                    )}>
                      Update
                    </Button>
                  </li>
                </ul>
              </div>,
          );
        } else {
          warnings.push(
              <div>
                <h5>{deets.get('name')}</h5>
                <ul key={id} className="recipe-alerts-list">
                  <li key="no-nutrient-info">
                    No Nutrition Facts
                    <Button
                        onClick={() => this.props.uiStateActions.openIngredientNutrientModal(
                            id, prepsByIngredient.get(id, Immutable.List()).toJS(),
                        )}>
                      Update
                    </Button>
                  </li>
                </ul>
              </div>,
          );
        }
      }
    }

    return warnings;
  }
}

/*
 * Assorted components used only by RecipeEdit
 */

export class RecipeSize extends React.Component {

  render() {
    const {
      recipeBatchClassName,
      definingBatches,
      batchLabel,
      sizeRatio,
      batchSizeField,
      amountField,
      measureField,
      unitField,
      nutrientServingAmountField,
      nutrientServingMeasureField,
      nutrientServingUnitField,
      disabled,
      conversions,
    } = this.props;

    const quantityInput = <QuantityInput
      {...this.props}
      unitInputAddonStyle={{ maxWidth: 95 }}
      reduxFormFields={{
        amountField: {
          ...amountField,
          onChange: newAmount => {
            amountField.onChange(newAmount);
            if (!definingBatches && sizeRatio) batchSizeField.onChange( unitField.value ? convert(newAmount, unitField.value, unitField.initialValue, conversions) : newAmount / sizeRatio);
          },
        },
        measureField,
        unitField: {
          ...unitField,
          onChange: newUnit => {
            unitField.onChange(newUnit);
            if (!definingBatches && sizeRatio)
              batchSizeField.onChange(convert(amountField.value, newUnit, unitField.initialValue, conversions) / sizeRatio);
          },
        },
        nutrientServingAmountField,
        nutrientServingMeasureField,
        nutrientServingUnitField,
      }}
    />;

    const recipeBatchSize = <RecipeBatchSize
      recipeBatchClassName={recipeBatchClassName}
      definingBatches={definingBatches}
      amountField={amountField}
      batchSizeField={batchSizeField}
      batchLabel={batchLabel}
      sizeRatio={sizeRatio}
      disabled={disabled}
      unitField={unitField}
      conversions={conversions}
    />;

    return definingBatches ?
      <div style={{ display: 'flex' }}>{quantityInput} {recipeBatchSize}</div>
    : <div style={{ display: 'flex' }}>{recipeBatchSize} {quantityInput}</div>;
  }
}

class RecipeBatchSize extends React.Component {
  render() {

    const { recipeBatchClassName, definingBatches, amountField, batchSizeField, batchLabel, sizeRatio, disabled, unitField, conversions } = this.props;

    if (!definingBatches && !sizeRatio) {
      return null;
    }

    return <div className={'recipe-size-batch ' + recipeBatchClassName}>
      {definingBatches ? ', equivalent to (optional)' : ''}
      <MaskedNumberInput
        {...batchSizeField}
        onChange={newBatchSize => {
          batchSizeField.onChange(newBatchSize);
          if (!definingBatches && sizeRatio) amountField.onChange(newBatchSize * sizeRatio / (unitField.value ? convert(1, unitField.value, unitField.initialValue, conversions) : 1));
        }}
        normalizeInitial
        disabled={disabled}
      />
      {definingBatches ?
        <FormControl type="text"
          disabled={disabled}
          {...batchLabel}
        />
      : <span>&nbsp;{batchLabel} or </span>}
    </div>;
  }
}

class RecipeStepsEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      alreadyAddedStep: false,
    };

    this.ingDrake = dragula({
      direction: 'vertical',
      moves(el, container, handle) {
        return handle.classList.contains('drag-recipe-ingredient');
      },
    });
    this.ingDrake.on('drop', this.reorderIngredients.bind(this));
    this.ingDrake.on('drag', this.highlightDropArea.bind(this));
    this.ingDrake.on('dragend', this.removeHighlightDropArea.bind(this));
  }

  highlightDropArea(el, source) {
    _.forEach(_.filter(this.ingDrake.containers, c => c !== source), container => container.classList.add('drop-area'));
  }

  removeHighlightDropArea(el) {
    _.forEach(this.ingDrake.containers, container => container.classList.remove('drop-area'));
  }

  reorderSteps(el, target, source, sibling) {
    function order(element) {
      // We've given the DOM element a class of "order-2" for example
      // and want to extract that number "2"
      if (!element || !element.className) {
        return null;
      }
      let orderString = String(element.className)
          .split(' ')
          .filter((c) => c.indexOf('order') > -1)[0]
          .split('-')[1];
      if (typeof orderString == typeof'') return parseInt(orderString);
      return orderString;
    }

    // We want to move steps in the React state,
    // instead of swapping the order of DOM elements
    // (which is what the dragula library does)
    //
    // First, we swap the DOM elements back
    // to their original positions
    let originalPosition = order(el);
    // sibling is undefined if moved to end; redux-field X.addField() interprets that the Right Way
    let targetPosition = sibling && order(sibling);

    let original_sibling = document.getElementsByClassName('order-' + (Number(originalPosition) + 1))[0];
    let parentNode = el.parentNode;
    parentNode.removeChild(el);
    parentNode.insertBefore(el, original_sibling);

    // Then, we update the redux-form state to reflect the change.
    const { fields: steps } = this.props;

    // so addField will correctly interpret it as a missing argument, and hence "end of array"
    if (targetPosition == null) targetPosition = undefined;

    steps.addField({}, targetPosition);

    if (targetPosition == undefined) {
      targetPosition = steps.length; // for the swap step - move original field to end
    } else if (targetPosition < originalPosition) {
      // the original has been pushed right by one place by the inserted placeholder
      originalPosition++;
    }

    steps.swapFields(originalPosition, targetPosition);
    steps.removeField(originalPosition);

    this.props.itemsDraggedHook();
  }

  reorderIngredients(el, target, source, sibling) {
    // index are appended at the end of id
    // steps: recipe-ingredients-[stepIdx]
    // ingredients: recipe-ingredient-usage-container-[stepIdx]-[ingredientIdx]
    function ordinalFromId(element) {
      if (!element || !element.id) {
        return null;
      }

      return Number(element.id.split('-').pop());
    }

    function getIngredientObj(ing) {
      return {
        ingredient: ing.ingredient.value,
        description: ing.description.value,
        quantity: {
          amount: ing.quantity.amount.value,
          measure: ing.quantity.measure.value,
          unit: ing.quantity.unit.value,
        },
      };
    }

    /**
     * NOTE:
     * By the time this method is called elements are already swapped.
     * However, our react props order is still same.
     *
     * So record changes, put element back then only swap props
     */

    // record changes
    const sourceIngredientIdx = ordinalFromId(el);
    const sourceStepIdx = ordinalFromId(source);
    const targetStepIdx = ordinalFromId(target);
    const siblingIngredientIdx = ordinalFromId(sibling);

    // put element back
    // 1. remove element added to target
    // 2. & 3. find original sibling of source ingredient
    // 4. if source ingredient had next sibling then insert before it
    //     else it must be last ingredient from source step so append at last
    target.removeChild(el);
    const orgSourceSibIngId = `recipe-ingredient-usage-container-${sourceStepIdx}-${sourceIngredientIdx + 1}`;
    const orgSourceSibIng = document.getElementById(orgSourceSibIngId);
    orgSourceSibIng ? source.insertBefore(el, orgSourceSibIng) : source.appendChild(el);

    const { fields: steps } = this.props;
    // ingredient to be dropped
    const ing = getIngredientObj(steps[sourceStepIdx].ingredients[sourceIngredientIdx]);

    if (sourceStepIdx != targetStepIdx) { // ingredients swapped among steps
      if (_.isNull(siblingIngredientIdx)) { // ingredient is dropped at end
        // add ingredient at the end of target step
        steps[targetStepIdx].ingredients.addField(ing);
        // remove ingredient from source step
        steps[sourceStepIdx].ingredients.removeField(sourceIngredientIdx);
      } else { // ingredient is dropped at top or between ingredients
        // add ingredient before sibling ingredient
        steps[targetStepIdx].ingredients.addField(ing, siblingIngredientIdx);
        // remove ingredient from source step
        steps[sourceStepIdx].ingredients.removeField(sourceIngredientIdx);
      }

    } else { // ingredient swapped within step

      if (_.isNull(siblingIngredientIdx)) { // ingredient id dropped at end
        // add new ingredient at end
        steps[sourceStepIdx].ingredients.addField();
        // swap new empty ingredient and source ingredient
        steps[sourceStepIdx].ingredients.swapFields(sourceIngredientIdx, steps[sourceStepIdx].ingredients.length);
        // remove newly added empty ingredient
        steps[sourceStepIdx].ingredients.removeField(sourceIngredientIdx);
      } else { // ingredient is dropped at top or between ingredients

        /**
         * NOTE: directly swapping ingredient fields will not work.
         * Source ingredient should be before sibling ingredient
         * and all other below it should be moved down.
         * So, simply add source ingredient before sibling ingredient.
         * This shifts every ingredient below it by 1. Now, previous
         * ingredient should be removed.
         *
         * When ingredient is moving up, source ingredient will also
         * shift down (its index is added by 1) else it will remain in same place.
         * So, if ingredient is moved up, remove one step shifted ingredient
         * else remove from same place
         */

        // add ingredient before sibling ingredient
        steps[sourceStepIdx].ingredients.addField(ing, siblingIngredientIdx);
        // when moved up sibling ingredient index is always less than source ingredient index
        // when moved down sibling ingredient index is always greater than source ingredient index
        const movingUp = siblingIngredientIdx < sourceIngredientIdx;
        if (movingUp) {
          // remove one step shifted ingredient
          steps[sourceStepIdx].ingredients.removeField(sourceIngredientIdx + 1);
        } else {
          // remove ingredient from same place
          steps[sourceStepIdx].ingredients.removeField(sourceIngredientIdx);
        }
      }
    }

    this.props.itemsDraggedHook();
  }

  componentDidMount() {

    // Find the containing div for the recipe steps
    // and make it a dragula container
    // (the elements within will be draggable)
    const container = document.getElementById('recipe-steps');
    const drake = dragula([container], {
      direction: 'vertical',
      moves(el, container, handle) {
        return handle.classList.contains('drag-recipe-step');
      },
    });
    // Call a function when a step is successfully dropped
    // into a new location in the list
    drake.on('drop', this.reorderSteps.bind(this));

  }

  render() {

    const { fields: steps, logoUrl, appFileDraggingOver, readonlyUser, syncedRecipe, disabledEditing, openEditIngredientModal } = this.props;

    return <div>
      <div id="recipe-steps">
      { steps.map((step, stepIndex) =>
          <Step ordinal={stepIndex} key={stepIndex}
                grabFocus={this.state.alreadyAddedStep}
                logoUrl={logoUrl}
                appFileDraggingOver={appFileDraggingOver}
                step={step}
                ingDrake={this.ingDrake}
                deleteStep={() => steps.removeField(stepIndex)}
                readonlyUser={readonlyUser}
                syncedRecipe={syncedRecipe}
                disabledEditing={disabledEditing}
                openEditIngredientModal={openEditIngredientModal}
          />,
      ) }
      </div>
      {!(disabledEditing) ?
        <AddButton noun="step" className="btn-addstep"
          onClick={() => {
            steps.addField();
            this.setState({ alreadyAddedStep: true });
          }}
        />
      : null}
    </div>;
  }
}

class Step extends Component {
  constructor(props) {
    super(props);

    this.state = {
      alreadyAddedIngredient: false,
      showStepDragHandle: true,
      previewingPhoto: null,
      removingPhoto: false,
      recipeStepPhotoUrl: props.step.photo.value ? props.logoUrl + props.step.photo.value : null,
    };

    ['handlePhotoChange',
      'handleAddPhoto',
      'handleDropPhoto',
      'previewPhoto',
      'removePhoto',
      'cancelRemovePhoto',
      'handlePhotoClear',
      'cancelPreviewPhoto',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });
  }

  handlePhotoChange(file) {
    uploadImage(file)
        .then(({ photoUrl, processedFile }) => {
          this.setState({ recipeStepPhotoUrl: photoUrl });
          this.props.step.photo.onChange(processedFile);
        });
  }

  handleAddPhoto(e) {
    e.preventDefault();
    this.handlePhotoChange(e.target.files[0]);
  }

  handleDropPhoto(files) {
    this.handlePhotoChange(files[0]);
  }

  previewPhoto(e) {
    this.setState({
      previewingPhoto: this.state.recipeStepPhotoUrl,
      previewPhotoDistanceFromTop: e.target.getBoundingClientRect().bottom,
    });
  }

  cancelPreviewPhoto() {
    this.setState({
      previewingPhoto: null,
    });
  }

  removePhoto() {
    this.setState({
      removingPhoto: true,
    });
  }

  cancelRemovePhoto() {
    this.setState({
      removingPhoto: false,
    });
  }

  handlePhotoClear() {
    this.setState({
      recipeStepPhotoUrl: undefined,
      previewingPhoto: null,
      removingPhoto: false,
    });

    this.props.step.photo.onChange(null);
    if (this.addPhotoInputRef) { // HTML file input doesn't respect null value passed in attrs
      this.addPhotoInputRef.value = null;
    }
  }

  showStepDragHandle = () => {
    this.setState({ showStepDragHandle: true });
  };

  hideStepDragHandle = () => {
    this.setState({ showStepDragHandle: false });
  };

  static propTypes = {
    ordinal: ReactPropTypes.number.isRequired,
    deleteStep: ReactPropTypes.func.isRequired,
    grabFocus: ReactPropTypes.bool.isRequired,
  };

  render() {
    const { ordinal, deleteStep, step, ingDrake, appFileDraggingOver, readonlyUser, syncedRecipe, disabledEditing, openEditIngredientModal } = this.props;

    let photoAddButton = null;
    if (!disabledEditing) {
      if (this.state.recipeStepPhotoUrl) {
        photoAddButton = <div className="recipe-step-add-photo">
            <span className="new-link"
                  onClick={this.removePhoto}>
              - Photo
            </span>
          </div>;
      } else {
        photoAddButton = <div className="recipe-step-add-photo">
          <span className="new-link" onClick={() => {
            if (this.addPhotoInputRef) this.addPhotoInputRef.click();
          }}>
            + Photo
          </span>
          <FormControl style={{ display: 'none' }} type="file"
                       inputRef={ref => this.addPhotoInputRef = ref}
                       accept="image/*"
                       {...step.photo}
                       onChange={this.handleAddPhoto}
                       onBlur={null}
                       value={null}/>
        </div>;
      }
    }

    const stepPhotoContent = this.state.recipeStepPhotoUrl && <div>
      <img className="recipe-step-preview-photo"
        draggable="false"
        src={this.state.recipeStepPhotoUrl}
        onClick={this.previewPhoto}
        style={disabledEditing ? { float: 'right', marginBottom: '7px' } : { marginTop: '12px' }}
      />
    </div>;
    const stepInstructionContent = !(disabledEditing) ?
      <Validate {...step.description}>
        <FormControl
          componentClass="textarea"
          inputRef={input => this.initialInput = input}
          {...step.description}
          placeholder="Add instructions..."
        />
      </Validate>
      : step.description.value ? <div className="readonly-description">{step.description.value}</div> : null;

    return (
      <Well className={'recipe-step order-' + ordinal}>
        <div className="recipe-step-content">
          <div className="recipe-order">
            <p>{ordinal + 1}.</p>
            <p>{ !(disabledEditing) && this.state.showStepDragHandle ? <Glyphicon glyph="align-justify" className="drag-recipe-step" /> : null }</p>
          </div>
          <div className="recipe-ingredients-content">
            <FieldArray component={IngredientUsagesEditor}
              fields={step.ingredients}
              props={{
                alreadyAddedIngredient: this.state.alreadyAddedIngredient,
                showStepDragHandle: () => this.showStepDragHandle(),
                hideStepDragHandle: () => this.hideStepDragHandle(),
                ingDrake,
                ordinal,
                setAddedIngredient: () => this.setState({ alreadyAddedIngredient: true }),
                readonlyUser,
                syncedRecipe,
                disabledEditing,
                openEditIngredientModal,
              }}
            />
          </div>

          { /* Wrapper div necessary for Safari (and maybe Firefox?) layout.
          Quoth Surendra: "The main reason for collapsed text-area is being
          wrapped inside table-cell element. Current text-area element for
          instruction was wrapped with span element which will have two
          classes: recipe_instructions and input-group. Although input-group
          has display type set to table (~block) it is overrode by [see main.css].

          So, text area being inside table-cell behaved weird.
          Fix: wrap parent element of text-area inside new div. this will make
          parent element of text-area instruction not being first child of
          recipe_step_content and will preserve display property of input-group
          class of parent span element"

          */ }
          <div style={{ height: '100%' }}>
            {disabledEditing ?
              <div style={{ display: 'flex' }}>
                <div style={{ width: 'calc(100% - 75px)' }}>{stepInstructionContent}</div>
                {stepPhotoContent}
              </div>
            : <div style={{ height: '100%' }}>
                <Dropzone
                    className={!this.state.recipeStepPhotoUrl && appFileDraggingOver ? 'recipe-step-photo-active-dropzone' : 'recipe-step-photo-dropzone'}
                    acceptClassName="recipe-step-photo-active-dropzone"
                    rejectClassName="recipe-step-photo-reject-dropzone"
                    disabled={readonlyUser || Boolean(this.state.recipeStepPhotoUrl)}
                    disableClick
                    accept="image/*"
                    onDrop={this.handleDropPhoto}
                  >
                    <div className="recipe-step-dropzone-text">DROP PHOTO HERE</div>
                    <div className="recipe-instructions">
                      {stepInstructionContent}
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingLeft: '5px' }}>
                      {!(disabledEditing) ?
                        <div className="delete-link">
                          <span onClick={deleteStep}>
                            delete
                          </span>
                        </div>
                      : null}
                      {stepPhotoContent}
                      {photoAddButton}
                    </div>
                  </Dropzone>
              </div>
            }
          </div>
        </div>


        {this.state.previewingPhoto ?
          <ViewPhotoModal
            photoURL={this.state.previewingPhoto}
            previewPhotoDistanceFromTop={this.state.previewPhotoDistanceFromTop}
            matchPreviewPhotos="bottom"
            isHidden={this.state.removingPhoto}
            onDelete={this.removePhoto}
            onHide={this.cancelPreviewPhoto}
            readonlyUser={disabledEditing}
          />
        : null}

        {this.state.removingPhoto ?
          <DeleteConfirmationModal
            onHide={this.cancelRemovePhoto}
            onDelete={this.handlePhotoClear}
            deleteVerb="remove"
            titleNoun="Photo"
            fullDescription="Are you sure you want to remove this photo?"
            className="remove-recipe-preview-photo"
          />
        : null}
      </Well>);
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextPhoto = nextProps.step.photo.value;
    if ((!nextPhoto || typeof nextPhoto === 'string') && this.props.step.photo.value !== nextPhoto) {
      this.setState({
        recipeStepPhotoUrl: nextPhoto ? nextProps.logoUrl + nextPhoto : null,
      });
      if (this.addPhotoInputRef) {
        this.addPhotoInputRef.value = null;
      }
    }
  }
}

class IngredientUsagesEditor extends Component {
  render() {
    const {
      fields, showStepDragHandle, hideStepDragHandle, ingDrake, ordinal,
      alreadyAddedIngredient, setAddedIngredient, readonlyUser, syncedRecipe, disabledEditing,
      openEditIngredientModal,
    } = this.props;
    return <div>
      <div className="recipe-ingredients" id={'recipe-ingredients-' + ordinal}>
        { fields.map((ingredient, ingredientIndex) =>
            <IngredientUsage key={ingredientIndex}
                             stepOrdinal={ordinal}
                             ingOrdinal={ingredientIndex}
                             grabFocus={alreadyAddedIngredient}
                             ingredient={ingredient}
                             ingDrake={ingDrake}
                             deleteIngredient={() => fields.removeField(ingredientIndex)}
                             showStepDragHandle={showStepDragHandle}
                             hideStepDragHandle={hideStepDragHandle}
                             readonlyUser={readonlyUser}
                             syncedRecipe={syncedRecipe}
                             disabledEditing={disabledEditing}
                             openEditIngredientModal={openEditIngredientModal}
                             lastRow={ingredientIndex === fields.length - 1}
            />,
        ) }
      </div>
      { !(disabledEditing) ?
        <AddButton noun="ingredients"
          onClick={() => {
            fields.addField();
            // watch out for this scope changing when refactoring
            setAddedIngredient();
          }}
        />
      : null}
    </div>;
  }

  componentDidMount() {
    const { ingDrake, ordinal } = this.props;
    if (ingDrake) {
      ingDrake.containers.push(document.getElementById('recipe-ingredients-' + ordinal));
    }
  }
}

const mapStateToPropsIngredientUsage = (state) => ({
    allIngredients: state.get('ingredients'),
    allMeasures: state.getIn(['measures', 'measures']),
    detailedIngredientInfo: state.get('detailedIngredients') || Immutable.Map(),
  });
const mapDispatchToPropsIngredientUsage = (dispatch) => ({
    getProductUsageInfo: bindActionCreators(getProductUsageInfo, dispatch),
    uiStateActions: bindActionCreators(UIStateActions, dispatch),
  });

@connect(mapStateToPropsIngredientUsage, mapDispatchToPropsIngredientUsage)
class IngredientUsage extends Component {
  render() {

    const {
      ingredient, deleteIngredient, allIngredients, allMeasures, detailedIngredientInfo,
      uiStateActions, stepOrdinal, ingOrdinal, showStepDragHandle, hideStepDragHandle, readonlyUser, syncedRecipe, disabledEditing,
      openEditIngredientModal, lastRow,
    } = this.props;
    const changeIngredient = newIngdt => {
      if (newIngdt && newIngdt.value && newIngdt.label) {
        newIngdt = newIngdt.value;
      }

      if (newIngdt) {
        if (detailedIngredientInfo.getIn([ingredientId, 'isPrepOf']) == newIngdt) {
          return;
        }
        getProductUsageInfo(newIngdt);
        ingredient.ingredient.onChange(
            this.checkMeasure(Immutable.Map({ ingredient: newIngdt, measure: ingredient.quantity.measure.value }), detailedIngredientInfo)
                .get('ingredient'));
      } else {
        ingredient.ingredient.onChange(newIngdt);
      }
    };

    const ingredientId = ingredient.ingredient.value;
    const conversions = detailedIngredientInfo.getIn([ingredientId, 'measures'])
        || Immutable.List();
    const selectedIngredient = allIngredients && allIngredients.find(i => i.get('id') === ingredientId);

    const isPrep = detailedIngredientInfo.hasIn([ingredientId, 'isPrepOf']);
    // raw in the sense of a physical raw ingredient, not in the sense of an unprocessed piece of data
    const rawIngredientId = isPrep
        ? detailedIngredientInfo.getIn([ingredientId, 'isPrepOf'])
        : ingredientId;

    const allPreps = detailedIngredientInfo.getIn([rawIngredientId, 'simplePreps'])
        || Immutable.List();

    const renderIngredientPrep = () => {
      if (allPreps.isEmpty()) {
        return disabledEditing ?
          <div>{ingredient.description.value}</div>
          : <Validate compact {...ingredient.description}>
              <FormControl
                disabled={readonlyUser}
                type="text"
                placeholder={readonlyUser ? '' : 'Preparation'}
                {...ingredient.description}
              />
            </Validate>;
      } else if ((disabledEditing) && isPrep) {
        const prepName = allPreps.findKey(p => p === ingredientId);
        return <div>{prepName}</div>;
      } else {
        return <Select
          clearable
          options={allPreps.map((outputProduct, name) => ({
            value: outputProduct,
            label: name,
          })).valueSeq().toArray()}
          disabled={disabledEditing || allPreps.isEmpty()}
          value={isPrep ? ingredientId : ''}
          onChange={(newValue) => {
            if (newValue) {
              getProductUsageInfo(newValue)
                .then(_.partial(ingredient.ingredient.onChange, newValue));
            } else {
              ingredient.ingredient.onChange(rawIngredientId);
            }
          }}
          placeholder={disabledEditing ? '' : 'Preparation'}
          autosize={false}
        />;
      }
    };

    return (
        <div className="recipe-ingredient-usage-container"
             id={'recipe-ingredient-usage-container-' + stepOrdinal + '-' + ingOrdinal}
             onMouseEnter={hideStepDragHandle}
             onMouseLeave={showStepDragHandle}
          >
          <div className="recipe-ingredient-usage">
            <div className="ingredient-name">
              <Validate compact {...ingredient.ingredient}>
                <Select
                    disabled={disabledEditing}
                    creatable
                    autosize={false}
                    {...ingredient.ingredient}
                    handlerOnFocus={ingredient.ingredient.onFocus}
                    value={rawIngredientId || ''}
                    onChange={changeIngredient}
                    selectOnBlur={false}
                    createNew={(item) => {
                      item.setProductId = ingredient.ingredient.onBlur;
                      uiStateActions.openNewIngredientModal(item);
                    }}
                    promptTextCreator={(label) => "Add New Ingredient"}
                    options={this.ingredientOptions}
                    searchOpts={ingredientListSearchOpts}
                    searchIndexName={indexTypes.INGREDIENTS}
                    inputRef={select => this.initialInput = select}
                    additionalLabel={productId =>
                      <span onClick={() => openEditIngredientModal(productId)} className="select-addtional-right-aligned-clickable">edit</span>
                    }
                />
              </Validate>
            </div>
            {!(disabledEditing) ? <Glyphicon glyph="menu-hamburger" className="drag-recipe-ingredient"/> : null}
            <div
              className="ingredient-quantity"
              style={
                syncedRecipe && !_.isNumber(_.get(ingredient, ['quantity', 'amount', 'value']))
                  ? { visibility: 'hidden' }
                  : {}
              }>
              <QuantityInput
                  canAddNewUnit={conversions.size < allMeasures.size}
                  openNewUnitModal={uiStateActions.openNewUnitModal}
                  productId={ingredientId}
                  compact clearable autoAdjust multiMeasure
                  hideEmptyInputs={readonlyUser}
                  disabled={disabledEditing || !detailedIngredientInfo.has(ingredient.ingredient.value)}
                  reduxFormFields={_.mapKeys(ingredient.quantity, (value, key) => key + 'Field')}
                  measures={allMeasures}
                  conversions={conversions}
                  clearWholeInput={false}
              />
            </div>
            <div className="ingredient-prep">
              {renderIngredientPrep()}
            </div>
            {!(disabledEditing) ?
              <div className="delete-link" onClick={deleteIngredient}>
                remove
              </div>
            : null}
          </div>
          {readonlyUser && lastRow ? null : <hr className="well-child-hr" />}
        </div>);
  }

  setupIngredientOptions(allIngredients) {
    this.ingredientOptions = allIngredients
        .filter(i => !i.get('tombstone'))
        .map(i => {
          let className = '';
          if (i.get('isSubRecipe')) className = 'text-italic ';
          else if (i.get('manuallyCreated')) className = 'manually-created';
          return {
            value: i.get('id'),
            label: i.get('name'),
            isUsed: i.get('isUsed'),
            className,
          };
        })
        .toJS();
  }

  constructor(props, context) {
    super(props, context);
    this.setupIngredientOptions(props.allIngredients);
  }

  componentWillReceiveProps(nextProps) {
    if (!Immutable.is(nextProps.allIngredients, this.props.allIngredients)) {
      this.setupIngredientOptions(nextProps.allIngredients);
    }

    if (nextProps.detailedIngredientInfo != this.props.detailedIngredientInfo) {
      const fields = this.props.ingredient;
      const usageValue = Immutable.fromJS({ ingredient: fields.ingredient.value, measure: fields.quantity.measure.value });
      const newUsageValue = this.checkMeasure(usageValue, nextProps.detailedIngredientInfo);
      if (newUsageValue.get('measure') != usageValue.get('measure')) {
        fields.quantity.measure.onChange(newUsageValue.get('measure'));
      }
    }
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }

  checkMeasure(newUsageValue, newIngredientInfo) {
    // newUsageValue = Immutable.fromJS({ ingredient: 1, measure: 2 });
    // to be used after a ingredient info received, or ingredient changed; if invalid unit, clear unit value
    const ingredientId = newUsageValue.get('ingredient');
    const measureId = newUsageValue.get('measure');
    if (newIngredientInfo.has(ingredientId)) {
      const conversions = newIngredientInfo.getIn([ingredientId, 'measures']);
      if (!conversions.some(c => c.get('measure') == measureId)) {
        return newUsageValue.withMutations(v => v
            .delete('measure')
            .delete('unit'),
        );
      }
    }
    // don't have detailed ingredient info yet, or do have it and measure is valid
    return newUsageValue;
  }
}
