import React, { Component } from 'react';
import { Row, Col, ListGroup, ListGroupItem, Button, DropdownButton } from 'react-bootstrap';
import ReactPropTypes from 'prop-types';
import { Link, withRouter } from 'react-router';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import _ from 'lodash';
import Immutable from 'immutable';
import classnames from 'classnames';
import * as ParsleyPropTypes from '../../utils/PropTypes';
import { getTagsList } from '../../../webapi/endpoints';

import SearchableListHeader from '../../SearchableListHeader';
import NewButton from '../../NewButton';
import SearchBox from '../../SearchBox';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { DeleteConfirmationModal } from '../../modals/ConfirmationModal';
import Checkbox from '../../Checkbox';

import { actionCreators } from '../../../reducers/uiState/searchableList';
import { getSearchObject } from '../../../utils/search';
import { allTagTypes } from '../../../utils/tags';
import TextWithHelp from '../../TextWithHelp';

function uiStateName(reduxStatePath) {
  if (typeof reduxStatePath === typeof'') {
    return reduxStatePath;
  } else {
    return reduxStatePath.join('.');
  }
}

@withRouter
export class ScrollableLink extends Component {

  render() {
    const { children, id, ...rest } = this.props;
    return <Link id={id} {...rest}>{children}</Link>;
  }

  componentDidMount() {
    if (this.props.shouldScroll(this.props.item)) {
      const nextLocation = {
        pathname: this.props.location.pathname,
        hash: '#' + this.props.id,
      };
      this.props.router.replace(nextLocation);
    }
  }
}

const KEYS = ['name'];

const leftSpacerOptions = {
  sm: 2,
  xs: 12,
};
const searchWidthOptions = {
  sm: 3,
  xs: 12,
};

export const searchableListLoadingSpinner = <Row>
  <Col {...leftSpacerOptions} />
  <Col {...searchWidthOptions}><LoadingSpinner /></Col>
</Row>;

@withRouter
@connect(
    (state, ownProps) => {
      let { reduxStatePath, extraItemsReduxPaths } = ownProps;

      let props = {
        uiState: state.getIn(['searchableList', uiStateName(reduxStatePath)]),
      };

      const getItems = path => state.getIn(typeof path === typeof'' ? [path] : path);

      props.items = (getItems(reduxStatePath) || Immutable.List())
          // defaultProps aren't seen in @connect()
          .concat(Immutable.fromJS(extraItemsReduxPaths || []).flatMap(getItems));

      if (ownProps.sortItemsFunc) {
        props.items = props.items.sortBy(ownProps.sortItemsFunc);
      }

      if (ownProps.taggable && ownProps.tagTypes) {
        props.allTags = {};
        ownProps.tagTypes.forEach(t => {
          props.allTags[t] = (state.getIn(['tags', t]) || Immutable.List())
                                              .sort((a, b) => a.get('name').localeCompare(b.get('name')));
        });
      }

      return props;
    },
    // hacky, but JS objects don't have a mapValues method.
    (dispatch, ownProps) => Immutable.Map(bindActionCreators(actionCreators, dispatch)).map(
        creator => creator.bind(this, uiStateName(ownProps.reduxStatePath)),
    ).toObject(),
)
export default class SearchableList extends Component {

  static propTypes = {
    // from redux state
    items: ParsleyPropTypes.summaryList,

    // from redux dispatch
    setSearchTerm: ReactPropTypes.func.isRequired,
    setSelectedTags: ReactPropTypes.func.isRequired,
    checkItem: ReactPropTypes.func,
    uncheckItem: ReactPropTypes.func,
    showDeleteConfirmation: ReactPropTypes.func,
    hideDeleteConfirmation: ReactPropTypes.func,

    /*
     * ownProps follow
     */
    // flags needed before mounting and unmounting
    shouldClearSearchFilter: ReactPropTypes.bool,

    // stuff to call in componentWillUnmount
    leavePage: ReactPropTypes.func.isRequired,

    // required iff deletable
    deleteItems: ReactPropTypes.func,
    refreshItems: ReactPropTypes.func,
    shouldShowNameOnSingleItemDeleting: ReactPropTypes.bool,
    shouldScroll: ReactPropTypes.func,

    newButton: ReactPropTypes.element,
    ListComponent: ReactPropTypes.func,
    checkable: ReactPropTypes.bool,
    copiable: ReactPropTypes.bool,
    taggable: ReactPropTypes.bool,
    deletable: ReactPropTypes.bool,
    getClassnames: ReactPropTypes.func,

    noun: ReactPropTypes.string.isRequired,
    searchKey: ReactPropTypes.string.isRequired,
    newButtonNoun: ReactPropTypes.string.isRequired,
    title: ReactPropTypes.string,
    baseUrl: ReactPropTypes.string.isRequired,
    reduxStatePath: ReactPropTypes.oneOfType([
      ReactPropTypes.string, // key for get()
      ReactPropTypes.arrayOf(ReactPropTypes.string), // argument for getIn)
    ]).isRequired,
    extraItemsReduxPaths: ReactPropTypes.arrayOf(
        // array of same type as reduxStatePath
        ReactPropTypes.oneOfType([
          ReactPropTypes.string, // key for get()
          ReactPropTypes.arrayOf(ReactPropTypes.string), // argument for getIn)
        ]),
    ),

    renderSectionItem: ReactPropTypes.func,
    groupingFunction: ReactPropTypes.func, // uses extraData, returns group ID; should sort in display order
    groupIDToHeader: ReactPropTypes.func,
    groupSortKey: ReactPropTypes.func,
    groupReverseSort: ReactPropTypes.bool,
    preProcessGroupItems: ReactPropTypes.func,

    searchBoost: ReactPropTypes.func,

    fullPage: ReactPropTypes.bool,

    hideAllRecipes: ReactPropTypes.bool,
    readonly: ReactPropTypes.bool,

    updateDisplayedItemsIds: ReactPropTypes.func,
    isStillLoading: ReactPropTypes.func,
    deleteActionText: ReactPropTypes.string,
    searchKeys: ReactPropTypes.string,
    nameField: ReactPropTypes.string,
    prepareItemLinkText: ReactPropTypes.func,
    leftSpacerOptions: ParsleyPropTypes.columnWidthSet,
    searchWidthOptions: ParsleyPropTypes.columnWidthSet,
    mainBodyOptions: ParsleyPropTypes.columnWidthSet,
  };

  static defaultProps = {
    newButton: null,
    ListComponent: ListGroup,

    shouldClearSearchFilter: true,

    checkable: false,
    copiable: true,
    taggable: false,
    deletable: true,
    fullPage: true,
    hideAllRecipes: false,
    readonly: false,
    shouldScroll: () => false,
    getClassnames: () => [],
    isStillLoading: () => false,

    extraItemsReduxPaths: [],

    groupSortKey: _.identity,
    groupReverseSort: false,
    deleteActionText: 'delete',
    searchKeys: KEYS,
    nameField: 'name',
    leftSpacerOptions: { sm: 2, xs: 12 },
    searchWidthOptions: { sm: 3, xs: 12 },
    mainBodyOptions: { sm: 10, xs: 12 },
    tagTooltips: {},
  };

  constructor() {
    super();
    this.search = null;
    this.state = {
      deleteError: null,
      deleteInProgress: false,
    };
    this.displayedItemIds = Immutable.Set();
  }

  render() {
    const {
        checkable, copiable, taggable, tagTypes, deletable, fullPage, // boolean flags
        noun, newButtonNoun, title, baseUrl, newButton, ListComponent,
        getClassnames,
        readonly,
        searchKey,

        reduxStatePath, shouldScroll,
        uiState, items, allTags,
        shouldShowNameOnSingleItemDeleting,

        groupingFunction, groupIDToHeader,
        groupSortKey, groupReverseSort,
        sectionFunction,
        itemWrapperFunction,
        preProcessGroupItems,

        renderSectionItem: originalRenderSectionItem,
        SectionItemComponent,
        sectionItemComponentsProps,

        // design props
        leftSpacerOptions, searchWidthOptions, mainBodyOptions,

        // state-changing functions
        checkItem, uncheckItem,
        setSelectedTags,
        setSearchTerm,
        showDeleteConfirmation, hideDeleteConfirmation,
        deleteItems, refreshItems,

        isStillLoading,
        deleteActionText,
        nameField,
        prepareItemLinkText,
        optionTagFunction,
        children, // items to appear in the top button bar
        tagTooltips,
    } = this.props;

    if (!items || !uiState || (isStillLoading && isStillLoading())) return <LoadingSpinner />;

    const displayedItems = this.displayedItems || this.getDisplayedItems(this.props);

    let renderSectionItem = originalRenderSectionItem;
    if (!renderSectionItem && !SectionItemComponent) {
      renderSectionItem = (item) => {
        const mainLink = <ScrollableLink
          id={`list-item-${reduxStatePath}-${item.get('id')}`}
          item={item}
          shouldScroll={shouldScroll}
          className={classnames('list-item-main-link', ...getClassnames(item))}
          to={{
            pathname: baseUrl + '/' + item.get('id').toString(),
            state: {
              searchTerm: uiState.get('searchTerm'),
            },
          }}>
          {prepareItemLinkText ? prepareItemLinkText(item) : item.get(nameField)}
        </ScrollableLink>;

        return <ListGroupItem key={item.get('id')}>
          {!readonly && checkable
            ? <Checkbox
              inline
              checked={uiState.get('checkedItems').includes(item.get('id'))}
              onChange={ev => ev.target.checked
                ? checkItem(item.get('id'))
                : uncheckItem(item.get('id'))
              }>{itemWrapperFunction ? itemWrapperFunction(item.get('id'), mainLink) : mainLink}
            </Checkbox>
            : itemWrapperFunction ? itemWrapperFunction(item.get('id'), mainLink) : mainLink
          }
          {!readonly && deletable ?
            <span
              onClick={() => showDeleteConfirmation(Immutable.List([item.get('id')]))}
              className="pull-right action-link">
              {deleteActionText}
              </span> :
            null}
          {!readonly && copiable ?
            <Link
              to={baseUrl + '/new/from/' + item.get('id').toString()}
              className="pull-right list-item-copy-link action-link">
              copy
            </Link> :
            null}
          { optionTagFunction &&
            <span className="searchable-list-tag">
              {optionTagFunction(item)}
            </span>
          }
        </ListGroupItem>;
      };
    }

    let mainList;
    if (groupingFunction) {
      let groupedItems = displayedItems.groupBy(groupingFunction)
          .sortBy((items, groupID) => groupSortKey(groupID));

      if (groupReverseSort) {
        groupedItems = groupedItems.reverse();
      }

      const groups = groupedItems.map((items, groupID) =>
          <div key={groupID} className="searchable-list searchable-list-group">
            {groupIDToHeader(groupID, items)}
            <ListComponent>
              {(preProcessGroupItems ? preProcessGroupItems(items) : items).map(item =>
                SectionItemComponent ? <SectionItemComponent item={item} {...sectionItemComponentsProps}/> : renderSectionItem(item),
              )}
            </ListComponent>
          </div>).valueSeq().toArray();

      if (sectionFunction) {
        mainList = <div>{sectionFunction(groupedItems, groups)}</div>;
      } else {
        mainList = <div>{groups}</div>;
      }

    } else {
      mainList = <ListComponent className="searchable-list">
        {displayedItems.map(item =>
          SectionItemComponent ? <SectionItemComponent item={item} {...sectionItemComponentsProps}/> : renderSectionItem(item),
        )}
      </ListComponent>;
    }

    // set up the list of items for the ACTIONS dropdown, so that we can easily
    // omit the whole thing if it's going to be empty
    let actionButtons = [];
    // if (taggable) {
    //   actionButtons.push(
    //       <MenuItem key="tags-head" header>tags</MenuItem>,
    //       <MenuItem key="add-tags">Add Tags (TODO)</MenuItem>,
    //       <MenuItem key="rm-tags">Remove Tags (TODO)</MenuItem>
    //   )
    // }

    let header;
    if (fullPage) {
      header = <Row>
        <Col {...leftSpacerOptions} />
        <Col {...mainBodyOptions}>
          <SearchableListHeader
              searchKey={searchKey}
              title={title || noun + 's'}
              searchUpdated={setSearchTerm}
              currentQuery={uiState.get('searchTerm')}
          />

          { readonly ? null :
              newButton ? newButton :
                  <NewButton
                      id={`search-new-btn-${searchKey}`}
                      noun={newButtonNoun || noun}
                      newPage={baseUrl + '/new'}/>
          }
        </Col>
      </Row>;
    } else {
      header = <Row>
        <Col {...searchWidthOptions}>
          <SearchBox id={`search-list-search-box-${searchKey}`} value={uiState.get('searchTerm')} onChange={setSearchTerm} />
        </Col>
      </Row>;
    }

    let modal;
    if (uiState.get('deleteConfirmationItems')) {
      let deletionItems = uiState.get('deleteConfirmationItems');

      const nounIsPluralizable = !(
          noun.endsWith('s') || noun.includes(' ') // no already-plural, no complicated phrases
      );
      const deletionNoun = (nounIsPluralizable && deletionItems.size !== 1 ? noun + 's' : noun).toLowerCase();
      let deletionItemNames = deletionItems
          .map(id => items
              .find(i => i.get('id') === id))
          .map(i => i && `"${i.get(nameField)}"`); // don't error out if the item is already deleted

      modal = <DeleteConfirmationModal
          onHide={() => {
            hideDeleteConfirmation(); this.setState({ deleteError: null });
          }}
          onDelete={() => {
            this.setState({ deleteInProgress: true });
            deleteItems(deletionItems.toJS())
                .then(
                    () => {
                      deletionItems.forEach(uncheckItem);
                      hideDeleteConfirmation();
                      refreshItems();
                    },
                    (error) => this.setState({
                        deleteError: error.reduxFormErrors._error,
                    }),
                ).finally(() => this.setState({
                  deleteInProgress: false,
                }));
          }}
          titleNoun={
            shouldShowNameOnSingleItemDeleting && deletionItems.size == 1
              ? displayedItems.find(item => item.get('id') === deletionItems.first()).get(nameField)
              : deletionNoun}
          longDescription={`the ${deletionNoun} ${deletionItemNames.join(', ')}`}
          deleteError={this.state.deleteError}
          inProgress={this.state.deleteInProgress}
      />;
    } else {
      modal = null;
    }

    return <div>
      {modal}
      {header}
      <Row>
        <Col id={`search-left-col-${searchKey}`} {...leftSpacerOptions}>
          {taggable && tagTypes ? tagTypes.map(tagType =>
                  <div className="leftcol" key="tags-toggles">
                    {
                      tagTooltips[tagType] ?
                      <TextWithHelp helpText={tagTooltips[tagType]}>
                        <h4>{allTagTypes[tagType].pluralName}</h4>
                      </TextWithHelp>
                      : <h4>{allTagTypes[tagType].pluralName}</h4>
                    }
                    {allTags[tagType].map(t => {
                      let isActive = uiState.get('selectedTags').find(_t => _t.id === t.get('id')) !== undefined;
                      let onClick = () => {
                        if (isActive) {
                          setSelectedTags(uiState.get('selectedTags').filter(_t => _t.id !== t.get('id')));
                        } else {
                          setSelectedTags(uiState.get('selectedTags').add({ id: t.get('id'), type: tagType }));
                        }
                      };
                      return (
                          <Button key={t.get('id')} bsStyle="info"
                              className="tagFilter"
                              active={isActive}
                              onClick={onClick}>
                            {t.get('name')}
                          </Button>
                      );
                    })}
                  </div>,
              )
              : null }
        </Col>
        <Col id={`search-main-body-${searchKey}`} {...mainBodyOptions}>
          <div className="searchable-list-body">
            <div className="searchable-list-button-bar" id={`search-list-action-${searchKey}`}>
              {children}
              { !readonly && checkable && deletable ?
                  <Button
                      id={`search-list-delete-${searchKey}`}
                      onClick={() => showDeleteConfirmation(this.getCheckedIds())}
                      bsStyle="danger"
                      disabled={this.getCheckedIds().isEmpty()}>
                    delete
                  </Button> :
                  null
              }
              { actionButtons.length > 0 ?
                  <DropdownButton
                      bsStyle="danger" title="actions"
                      disabled={this.getCheckedIds().isEmpty()}
                      id="list-actions-dropdown">
                    {actionButtons}
                  </DropdownButton> :
                  null
              }
            </div>
            {mainList}
          </div>
        </Col>
      </Row>
    </div>;
  }

  getCheckedIds() {
    return this.props.uiState.get('checkedItems');
  }

  getSearchResults(value) {
    const result = this.search.searchWithQueryFn(value, 1.5);
    return Immutable.fromJS(result);
  }

  getDisplayedItems(props) {

    const { items, uiState, taggable, hideAllRecipes, tagTypes } = props;
    if (!items) return Immutable.List();

    if (hideAllRecipes && uiState.get('selectedTags').isEmpty() && !uiState.get('searchTerm')) return Immutable.List();

    const textSearchResults = uiState.get('searchTerm')
      ? this.getSearchResults(uiState.get('searchTerm'))
      : items;

    const groupedSelectedTags = uiState.get('selectedTags').groupBy(t => t.type).map(tags => tags.map(t => t.id));
    var tagFilterResults = null;
    if (tagTypes[0] = "recipes") {
      tagFilterResults = textSearchResults.filter(i => !taggable // don't check if no tags
                || uiState.get('selectedTags').isEmpty() // default behavior, may change
                || groupedSelectedTags.every(tags => tags.intersect(i.get('tags')).size >= uiState.get('selectedTags').size));
    } else {
      tagFilterResults = textSearchResults.filter(i => !taggable // don't check if no tags
                || uiState.get('selectedTags').isEmpty() // default behavior, may change
                || groupedSelectedTags.every(tags => !tags.intersect(i.get('tags')).isEmpty()));
    }

    // all entities that *can* be tombstoned should use the same format in their
    // list endpoints
    return tagFilterResults.filter(i => !i.get('tombstone'));
  }


  setupSearch(props) {
    const { items, searchBoost, searchKeys } = props;

    if (items) {
      this.search = getSearchObject(items.toJS(), searchKeys, { resultBoost: searchBoost });
    }
  }

  componentWillMount() {
    const { enterPage, uiState, taggable, tagTypes } = this.props;
    if (taggable && tagTypes) {
      tagTypes.forEach(t => getTagsList(t));
    }

    this.setupSearch(this.props);
    if (!uiState) {
      enterPage();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.items !== this.props.items) {
      this.setupSearch(nextProps);
    }

    this.displayedItems = this.getDisplayedItems(nextProps);

    // When the parent component keeps track of this component's displayed items id list
    if (typeof this.props.updateDisplayedItemsIds === 'function') {

      const newDisplayedItemIds = this.displayedItems.map(i => i.get('id')).toSet();

      // The displayed items' id list will change
      // -> the parent component will re-render
      // -> skip this re-render and re-render this component when its parent re-renders instead
      if (!newDisplayedItemIds.equals(this.displayedItemIds)) {
        this.displayedItemIds = newDisplayedItemIds;
        this.ignoreThisUpdate = true;
        this.props.updateDisplayedItemsIds(newDisplayedItemIds);
      } else {
        // The displayed items' id list won't change -> the parent component won't re-render -> re-render this component right away
        // we have to re-render even when the id list won't change, as this is how we currently update checkboxes
        // we can elaborate `shouldComponentUpdate` more if this component turns out to be slow
        this.ignoreThisUpdate = false;
      }
    }
  }

  shouldComponentUpdate() {
    return !this.ignoreThisUpdate;
  }

  componentWillUnmount() {
    const { location: currentLocation, router: { location: nextLocation }, leavePage } = this.props;
    if (!nextLocation.pathname.match(currentLocation.pathname)) {
      leavePage();
    }
  }

  static deleteItems(items, deleteItem) {
    // takes an array, not Immutable.List
    // recursive through promises.
    return Promise.all(
        items.map(i => deleteItem(i)),
    );
  }
}
