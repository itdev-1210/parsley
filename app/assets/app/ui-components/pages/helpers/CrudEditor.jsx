import React, { Component } from 'react';

import ReactPropTypes from 'prop-types'
import Immutable from 'immutable';

import { Col, Grid, Row } from 'react-bootstrap';
import CrudHeader, { crudEditorLoadingSpinner } from '../../CrudHeader';
import CrudFooter from '../../CrudFooter';

import { isBlank } from '../../../utils/validation-utils';
import { deepOmit } from '../../../utils/general-algorithms';
import { addSideEffect, transformErrorValue } from 'common/utils/promises';

export default class CrudEditor extends Component {
  static propTypes = {
    route: ReactPropTypes.object.isRequired, // injected by react-router
    submitText: ReactPropTypes.string,
    submittingText: ReactPropTypes.string,
    deleteText: ReactPropTypes.string,
    deletingText: ReactPropTypes.string,
  };

  static defaultProps = {
    submitText: 'save',
    submittingText: 'saving',
    deleteText: 'delete',
    deletingText: 'deleting',
  };

  state = {};

  // OVERRIDE IN SUBCLASSES
  containerClass = undefined;
  listUrl = null;
  noun = 'item';
  deleteTitle = undefined;
  printNoun = null;
  showFooterExtraButtons = true;
  bodyWidthSet = {xs: 12, md: 10, mdOffset: 2};
  headerWidthSet = undefined;
  footerWidthSet = undefined;

  submit(values) {
    return this.handleSave(this.props.params.id, values)
        .then(success => {
              this.clearRouteLeaveHook();
              return success;
            },
            e => {
              if (e.reduxFormErrors) {
                return Promise.reject(e.reduxFormErrors);
              } else {
                console.log(e.toString());
                console.log(e.stack);
                return Promise.reject({ _error: e.message });
              }
            });
  }

  render() {
    const {
      handleSubmit, submitting, error: globalError, editingDisabled, deleteDisabled, shouldHideDelete, readonlyUser,
      submitText, submittingText,
      deleteText, deletingText,
    } = this.props;

    // Check that the subclass contains the required methods
    // This check runs after the constructor function so the methods should be here
    if (process.env.NODE_ENV !== 'production') {
      let requiredMethods = [
        'deleteItem',
        'renderBody',
        'handleSave',
      ];
      requiredMethods.forEach(methodName => {
        if (!this[methodName]) {
          throw `CrudEditor subclass ${this.constructor.name} must specify/implement ${methodName}`;
        }
      });
    }

    return (<Grid fluid className={this.containerClass}>
      <CrudHeader noun={this.noun}
                  deleteTitle={this.deleteTitle}
                  listTitle={this.listTitle}
                  isNewItem={this.isCreatingNew()} listUrl={this.listUrl}
                  onDelete={this.onDelete.bind(this)}
                  onSave={handleSubmit(this.submit.bind(this))}
                  extraButtons={this.makeExtraButtons()}
                  submitting={submitting}
                  deleting={this.state.deleting}
                  globalError={globalError}
                  editingDisabled={editingDisabled}
                  deleteDisabled={deleteDisabled}
                  shouldHideDelete={shouldHideDelete}
                  bodyWidths={this.bodyWidths}
                  colWidthSet={this.headerWidthSet}
                  readonlyUser={readonlyUser}
                  submitText={submitText}
                  submittingText={submittingText}
                  deleteText={deleteText}
                  deletingText={deletingText}
      />
      {
        this.sourceId() && this.stillLoading() ?
            crudEditorLoadingSpinner :
            // If this following bit gets more complicated, factor it out into a CrudBody
            <Row><Col {...this.bodyWidthSet}>
              {this.renderBody()}
            </Col></Row>
      }
      { this.sourceId() && this.stillLoading() ?
        null :
        <CrudFooter noun={this.noun}
                  deleteTitle={this.deleteTitle}
                  isNewItem={this.isCreatingNew()} listUrl={this.listUrl}
                  onDelete={this.onDelete.bind(this)}
                  onSave={handleSubmit(this.submit.bind(this))}
                  extraButtons={this.makeExtraButtons()}
                  submitting={submitting}
                  deleting={this.state.deleting}
                  globalError={globalError}
                  showExtraButtons={this.showFooterExtraButtons}
                  editingDisabled={editingDisabled}
                  deleteDisabled={deleteDisabled}
                  shouldHideDelete={shouldHideDelete}
                  colWidthSet={this.footerWidthSet}
                  readonlyUser={readonlyUser}
                  submitText={submitText}
                  submittingText={submittingText}
                  deleteText={deleteText}
                  deletingText={deletingText}
      />
      }
    </Grid>);
  }

  componentWillMount() {
    const { router, route } = this.props;
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
  }

  componentDidMount() {
    if (this.initialInput && (!this.sourceId() || !this.stillLoading())) {
      // form controls are rendering on the initial render
      this.initialInput.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.initialInput && this.sourceId() &&
        this.stillLoading(prevProps) && !this.stillLoading(this.props)) {
      // form controls have rendered for the first time
      this.initialInput.focus();
    }
  }

  stillLoading(props = this.props) {
    return !props.mainInfoLoaded;
  }

  static isCreatingNew(params) {
    return params.id === 'new';
  }

  static isCopying(params) {
    return params.origId !== undefined;
  }

  static initialValues(rawValues, params, ...transformers) {
    if (!rawValues) return;

    let returnValue = rawValues;

    if (CrudEditor.isCopying(params)) {
      // don't mutate original object
      returnValue = Object.assign({}, returnValue, { name: `Copy of ${returnValue.name}` });
    }

    for (let t of transformers) {
      returnValue = t(returnValue, params);
      if (!returnValue) return; // if any point in the pipeline returns null, later points should not try to recover
    }

    if (Immutable.isImmutable(returnValue)) {
      returnValue = returnValue.toJS();
    }
    return returnValue;
  }

  static sourceId(params) {
    if (!CrudEditor.isCreatingNew(params))
      return parseInt(params.id);
    else if (CrudEditor.isCopying(params))
      return parseInt(params.origId);
    else
        return null;
  }

  isCreatingNew() {
    return CrudEditor.isCreatingNew(this.props.params);
  }

  isCopying() {
    return CrudEditor.isCopying(this.props.params);
  }

  sourceId() {
    return CrudEditor.sourceId(this.props.params);
  }

  goToList() {
    this.props.router.push(this.listUrl);
  }

  onDelete() {
    this.setState({ 'deleting': true });
    return this.deleteItem(this.props.params.id)
        .then(
            success => this.goToList(),
            transformErrorValue(addSideEffect(() =>
                this.setState({ deleting: false })))
        );
  }

  makeExtraButtons() {
    return null; // can be overridden by subclasses, but not required
  }
}
