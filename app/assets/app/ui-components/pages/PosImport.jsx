// @flow
import React, { Component } from 'react';
import type { Node } from 'react';
import Immutable from 'immutable';
import { Grid, Row, Col, Button, Well, FormGroup, ControlLabel, FormControl, Modal } from 'react-bootstrap';
import formatNum from 'format-num';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import moment from 'moment';
import _ from 'lodash';
import classnames from "classnames";

import Validate from 'common/ui-components/Validate';
import Select from '../Select';
import { history } from '../../navigation';
import { getMultipleProductsUsageInfo, getRecipeList, saveOrder,
  getPosImportPreferences, updatePosImportPreferences } from '../../webapi/endpoints';
import {
    checkFields,
    required, optional,
} from '../../utils/validation-utils';
import type { ReactRouterProps } from 'common/utils/routing';

import { shallowToObject, posImportFields } from '../../utils/form-utils';
import DatePicker from 'react-bootstrap-date-picker';
import AddCodeToPosImportModal from '../modals/AddCodeToPosImportModal';
import { actionCreators as UIStateActions } from '../../reducers/uiState/posImport';
import type { SelectOption } from '../Select';
import { dateLongFormat } from 'common/utils/constants';

function validate(values) {
  const posValidators = {
    itemCodeHeader: required(),
    dateSoldHeader: required(),
    quantitySoldHeader: required(),
    averagePriceHeader: optional(),
    nameHeader: optional(),
  };
  return checkFields(values, posValidators);
}


type Props = {
  fields: any,
  uiActions: any,
  uiState: Immutable.Map<string, any>,
  fields: any,
  recipes: Immutable.List<Immutable.Map<string, any>>,
  dirty: boolean,
  handleSubmit: Function,
  details: Immutable.Map<string, any>,
} & ReactRouterProps

type State = {
  data: Array<*>,
  uploaded: boolean,
  formattedData: Array<*>,
}

@withRouter
@reduxForm(
    {
      form: 'pos-import',
      fields: [
        ...posImportFields,
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => {
      const posImportPreferences = state.get('posImportPreferences');
      const details = state.get('posCsvDetails');
      let initialValues = {};

      if (details && posImportPreferences && posImportPreferences.size > 0) {
        const preferenceHeaders = posImportPreferences.get(0);
        const csvHeaders = details.get('headers');
        posImportFields.forEach(field => {
          if (csvHeaders.includes(preferenceHeaders.get(field)))
            initialValues[field] = preferenceHeaders.get(field);
        });
      }

      return {
        recipes: (state.get('recipes') || Immutable.List()).filter(r => !r.get('ingredient') && !r.get('tombstone')),
        details,
        uiState: state.get('posImport'),
        initialValues,
      };
    },
    dispatch => ({
      uiActions: bindActionCreators(UIStateActions, dispatch),
    }),
)
export default class PosImport extends Component<Props, State> {

  clearRouteLeaveHook: any
  exitingRecipes: Array<*>
  newRecipes: Array<*>
  customDateLabel = 'Enter Date Manually'

  constructor(props: Props) {
    super(props);
    this.state = {
      headers: [],
      data: [],
      loaded: false,
      formattedData: [],
      uploaded: false,
    };

    this.uploadData = this.uploadData.bind(this);
    this.prepareData = this.prepareData.bind(this);
    this.closeEmptyFieldModal = this.closeEmptyFieldModal.bind(this);
    this.findNewCodes = this.findNewCodes.bind(this);
    this.findEmptyQuantities = this.findEmptyQuantities.bind(this);
  }

  save = () => {
    const { formattedData } = this.state;
    const { fields } = this.props;
    const data = _(formattedData)
        .groupBy(d => d.dateSold)
        .map((items, date) => ({ name: `POS Import ${moment(date).format(dateLongFormat)}`, date, items }))
        .value();

    const ordersPromise = data.map(d => {
      const items = d.items.map(i => ({ product: i.id, orderedQuantity: { amount: i.quantitySold } }));
      const orderData = {
        items,
        name: d.name,
        transaction: {
          isFinalized: true,
          time: moment(d.date).format(),
          includeInCalculatedInventory: true,
          deltas: d.items.map(i => ({
            product: i.id,
            quantity: { amount: i.quantitySold },
            cashFlow: i.totalPrice,
          })),
        },
        usedMenu: {
          name: 'Custom Order',
          sections: [{ items }],
        },
      };
      return saveOrder('new', orderData);
    });

    Promise.all([...ordersPromise, updatePosImportPreferences(shallowToObject(fields))])
      .then(success => {
        this.clearRouteLeaveHook();
        history.push('/orders');
      },
      e => {
        if (e.reduxFormErrors) {
          return Promise.reject(e.reduxFormErrors);
        } else {
          console.log(e.toString());
          console.log(e.stack);
          return Promise.reject({ _error: e.message });
        }
      });
  }

  prepareData = () => {
    const { recipes, fields, details, uiActions: { openEmptyCodeModal, openDuplicateItemsNumberModal } } = this.props;
    const { itemCodeHeader, dateSoldHeader, quantitySoldHeader, averagePriceHeader, customDate, nameHeader } = shallowToObject(fields);
    const normalizeItemCode = (code: string) => code
        .toLocaleLowerCase() // matches should be case-insensitive
        .trim() // matches should ignore leading/trailing whitespace, which is common in buggy CSV export implementations
        .replace(/^0+/, ''); // ignore leading zeroes, because users are likely to omit them in data entry
    const findRecipeByCode = (code: string) => recipes.find(r =>
        r.get('itemNumber') &&
        // $FlowFixMe: only with records can we assert member existence/value
        normalizeItemCode(r.get('itemNumber')) === normalizeItemCode(code),
    );

    const data = details.get('data', Immutable.List())
        .map((d, index) => {
          const recipe = findRecipeByCode(d.get(itemCodeHeader));
          return {
            rowNumber: index + 2,
            itemCode: d.get(itemCodeHeader) && d.get(itemCodeHeader).trim(),
            dateSold: customDate || d.get(dateSoldHeader),
            quantitySold: d.get(quantitySoldHeader).trim() && (parseInt(d.get(quantitySoldHeader)) || 0),
            name: nameHeader && d.get(nameHeader) && d.get(nameHeader).trim(),
            averagePrice: averagePriceHeader && d.get(averagePriceHeader) && d.get(averagePriceHeader).trim(),
            id: recipe && recipe.get('id'),
          };
        })
        .filterNot(d => /\s/.test(d.itemCode));

    const sameItemNumberRows = data
        .filter(d => d.itemCode)
        .groupBy(d => `${d.dateSold}-${d.itemCode}`)
        .filter(itemDate => itemDate.size > 1)
        .toList().flatten().map(item => item.rowNumber).sort();

    if (sameItemNumberRows.size > 0) {
      openDuplicateItemsNumberModal(sameItemNumberRows.toJS());
    } else {
    const [newAndEmptyCodeRows, existingCodeRows] = _.partition(data.toJS(), d => !d.id);
    const [emptyCodeRows, newCodeRows] = _.partition(newAndEmptyCodeRows, d => !d.itemCode);
    this.exitingRecipes = existingCodeRows;
    this.newRecipes = newCodeRows;
    if (emptyCodeRows.length > 0)
      openEmptyCodeModal(emptyCodeRows.map(r => r.rowNumber));
    else
      this.findEmptyQuantities();
    }
  }

  findEmptyQuantities = () => {
    const { uiActions: { openEmptyQuantityModal, closeEmptyCodeModal } } = this.props;
    closeEmptyCodeModal();

    const emptyQuantityRows = [...this.newRecipes, ...this.exitingRecipes]
      .filter(d => !_.isFinite(d.quantitySold) && d.itemCode);
    if (emptyQuantityRows.length > 0)
      openEmptyQuantityModal(emptyQuantityRows.map(r => r.rowNumber));
    else
      this.findNewCodes();
  }

  findNewCodes = () => {
    const { uiActions: { openAddCodeModal, closeEmptyQuantityModal } } = this.props;
    closeEmptyQuantityModal();
    this.newRecipes = this.newRecipes
        .filter(newRecipe => _.isFinite(newRecipe.quantitySold) && newRecipe.itemCode);
    if (this.newRecipes.length > 0)
      openAddCodeModal(this.newRecipes);
    else
      this.uploadData([]);
  }

  uploadData = async (newRecipes: Array<*>) => {
    const { uiActions: { closeAddCodeModal } } = this.props;
    const recipesToShow = [...this.exitingRecipes, ...newRecipes].filter(r => _.isFinite(r.quantitySold) && r.id);

    closeAddCodeModal();
    const usageInfo = await getMultipleProductsUsageInfo(recipesToShow.map(d => d.id));
    const formattedData = recipesToShow.map(rec => {
      const recipeInfo = usageInfo[rec.id];
      let averagePrice = recipeInfo.portionPrice;

      if (rec.averagePrice) {
        const csvAveragePrice = parseFloat(rec.averagePrice);
        if (_.isFinite(csvAveragePrice)) {
          averagePrice = csvAveragePrice;
        }
      }
      return {
        ...rec,
        name: recipeInfo.name,
        averagePrice,
        totalPrice: _.isFinite(averagePrice) ? rec.quantitySold * averagePrice : undefined,
      };
    });
    this.setState({
      formattedData,
      uploaded: true,
    });
  }

  closeEmptyFieldModal = (closeFun: Function) => {
    const { fields } = this.props;
    updatePosImportPreferences(shallowToObject(fields))
      .then(success => {
        closeFun();
        this.clearRouteLeaveHook();
        history.push('/orders');
      });
  }

  componentDidMount() {
    const { router, route, details } = this.props;
    if (!details) history.push('/orders');
    getRecipeList();
    getPosImportPreferences();
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
  }

  componentDidUpdate(prevProps: Props) {
    const { dateSoldHeader, customDate } = this.props.fields;
    const isCustomDate = dateSoldHeader.value === this.customDateLabel;
    if (prevProps.fields.customDate.value && !customDate.value && isCustomDate) {
      dateSoldHeader.onChange('');
    }

    if (prevProps.fields.dateSoldHeader.value !== dateSoldHeader.value) {
      if (isCustomDate) {
        customDate.onChange(moment().startOf('day').format());
      } else {
        customDate.onChange('');
      }
    }
  }

  render() {
    const { uploaded, formattedData } = this.state;
    const { fields, handleSubmit, details, uiState, recipes,
      uiActions: { closeEmptyCodeModal, closeEmptyQuantityModal, closeDuplicateItemsNumberModal } } = this.props;

    if (!details) return null;

    const { itemCodeHeader, dateSoldHeader, quantitySoldHeader, averagePriceHeader, customDate, nameHeader } = fields;
    const selectedHeaders = [itemCodeHeader, dateSoldHeader, quantitySoldHeader, averagePriceHeader, nameHeader]
        .map(i => i.value).filter(h => h !== '');
    // $FlowFixMe: only with records can we assert member existence/value
    const headersOption = (details.get('headers'): Immutable.List<string>)
        .filter(h => !selectedHeaders.includes(h))
        .map(h => ({ value: h, label: h }));
    const placeholder = 'Select Column';

    return <Grid fluid style={{ position: 'relative' }}>
      <div>
        <Row>
          <Col md={2} xs={12}/>
          <Col md={10} xs={12}>
            <div style={{ width: '72%' }}>
              <Row >
                <Col className="hidden-xs title-block" xs={3}>
                  <h3 className="list-title" style={{ marginBottom: '38px' }}>POS Import</h3>
                </Col>
                <Col xs={5} sm={4} md={9} className="crud-top-buttons">
                  <span>
                    <Button bsStyle="primary" onClick={handleSubmit(this.save)} disabled={!uploaded}>SAVE</Button>
                    <Button bsStyle="primary" onClick={() => history.push('/orders')}>EXIT</Button>
                  </span>
                </Col>
              </Row>
              <div>
                  <div className="pos-import-container">
                    <Row>
                      <Col md={4} xs={12}>
                        <h5>Data</h5>
                      </Col>
                      <Col md={6} xs={12}>
                        <h5>Pmix Column Name</h5>
                      </Col>
                    </Row>
                      <div>
                        <PosImportRow
                          key="itemCodeRow"
                          label="Item Number"
                          item={itemCodeHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <PosImportRow
                          className={classnames('pos-import-date-row', customDate.value ? 'pos-import-custom-date' : '')}
                          key="dateSoldRow"
                          label="Date Sold"
                          item={dateSoldHeader}
                          extraOption={!customDate.value ? this.customDateLabel : undefined}
                          options={headersOption}
                        >
                          {
                            customDate.value &&
                            <CustomDateSelect
                              customDate={customDate}
                            />
                          }
                        </PosImportRow>
                        <PosImportRow
                          key="quantitySoldRow"
                          label="Quantity Sold"
                          item={quantitySoldHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <PosImportRow
                          key="averagePriceRow"
                          label="Average Price (optional)"
                          item={averagePriceHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <PosImportRow
                          key="nameRow"
                          label="Name (optional)"
                          item={nameHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                      </div>
                  </div>
                {
                  uploaded ?
                  <div>
                    <Well className="order-section">
                      <Row className="pos-import-table-title">
                        <Col xs={2}><h6>Date</h6></Col>
                        <Col xs={3}><h6>Code</h6></Col>
                        <Col xs={3}><h6>Name</h6></Col>
                        <Col xs={1}><h6>Quantity</h6></Col>
                        <Col xs={1}><h6>Price</h6></Col>
                        <Col xs={1}><h6>Total</h6></Col>
                      </Row>
                      {
                        formattedData.map((d, index) =>
                          <DataImportedRow
                            key={`item ${index}`}
                            item={d}
                          />,
                        )
                      }
                    </Well>
                  </div>
                  : null
                }
              </div>
              <Row>
                <Col xs={12} className="crud-top-buttons" style={{ marginBottom: '15px' }}>
                  <span>
                    <Button bsStyle="primary" disabled={uploaded} onClick={handleSubmit(this.prepareData.bind(this))}>Confirm and Upload</Button>
                    <Button bsStyle="primary" disabled={!uploaded} onClick={handleSubmit(this.save)}>SAVE</Button>
                  </span>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
      {
        uiState.get('showAddCodeModal') &&
        <div>
          <AddCodeToPosImportModal
            recipes={recipes}
            unknowRecipes={uiState.get('recipesCode', [])}
            hide={this.uploadData.bind(this)}
          />
        </div>
      }
      {
        uiState.get('showEmptyCodeModal') &&
        <div>
          <BlankItemFieldModalAlert
            hide={() => this.closeEmptyFieldModal(closeEmptyCodeModal)}
            onSkip={this.findEmptyQuantities}
            noun="Item Number"
            nouns="Item Number"
            emptyCodeRows={uiState.get('emptyCodeRows', [])}
          />
        </div>
      }
      {
        uiState.get('showEmptyQuantityModal') &&
        <div>
          <BlankItemFieldModalAlert
            hide={() => this.closeEmptyFieldModal(closeEmptyQuantityModal)}
            onSkip={this.findNewCodes}
            noun="Quantity"
            nouns="Quantities"
            emptyCodeRows={uiState.get('emptyQuantityRows', [])}
          />
        </div>
      }
      {
        uiState.get('showDuplicateItemNumberModal') &&
        <div>
          <PosImportModalAlert
            title="duplicate Item Numbers"
            bodyText={
              `Encountered  duplicate Item Numbers on lines${uiState.get('duplicateItemNumbers', []).map(e => ` #${e}`).toString()}, please fix it in the file and re-import`
            }
            hide={() => this.closeEmptyFieldModal(closeDuplicateItemsNumberModal)}
            hideLabel="Exit"
          />;
        </div>
      }
    </Grid>;
  }
}

type PosImportRowProps = {
  label: string,
  item: any,
  options: Immutable.List<SelectOption>,
  extraOption?: string,
  className?: string,
  children?: ?Node
}

export function PosImportRow(props: PosImportRowProps) {
  const { label, item, options, children, extraOption, className, ...rest } = props;
  let selectOptions = options.toJS();
  [item.value, extraOption].filter(i => i).forEach(opt => {
    selectOptions.unshift({
      label: opt,
      value: opt,
    });
  });
  return <Row className={className}>
    <Col md={4} xs={12}>
      <h5>{label}</h5>
    </Col>
    <Col md={4} xs={12}>
      <Validate {...item}>
      <Select
        key={item.value}
        {...item}
        options={selectOptions}
        {...rest}
      />
      </Validate>
    </Col>
    <Col md={4} xs={12}>
      {children}
    </Col>
  </Row>;
}

function DataImportedRow(props: {item: Object}) {
  const { item } = props;
  return <div>
    <hr className="well-child-hr"/>
    <Row>
      <Col xs={2}><h6>{moment(item.dateSold).format(dateLongFormat)}</h6></Col>
      <Col xs={3}><h6>{item.itemCode}</h6></Col>
      <Col xs={3}><h6>{item.name}</h6></Col>
      <Col xs={1}><h6>{item.quantitySold}</h6></Col>
      <Col xs={1}><h6>{_.isFinite(item.averagePrice)
          ? `$${formatNum(item.averagePrice, { maxFraction: 2 })}`
          : undefined
      }</h6></Col>
      <Col xs={1}><h6>{_.isFinite(item.totalPrice)
          ? `$${formatNum(item.totalPrice, { maxFraction: 2 })}`
          : undefined
      }</h6></Col>
    </Row>
  </div>;
}


type CustomDateSelectProps = {
  customDate: any,
}

export class CustomDateSelect extends Component<CustomDateSelectProps> {

  componentWillUnmount() {
    const { customDate } = this.props;
    customDate.onChange('');
  }

  render() {
    const { customDate } = this.props;
    return <FormGroup>
      <Col xs={2} componentClass={ControlLabel}>Date </Col>
      <Col xs={10}>
        {
            <Validate {...customDate}>
              <FormControl
                componentClass={DatePicker}
                {...customDate}
                onChange={(newValue) => {
                  if (newValue === null) customDate.onChange(undefined);
                  else {
                    customDate.onChange(moment(newValue).format('YYYY-MM-DD'));
                  }
                }}
                onBlur={event => {
                  if (!event || !event.target) return;
                  customDate.onBlur(event);
                }}
                maxDate={moment().startOf('day').format()}
                autoFocus
                clearButtonElement="X"
              />
            </Validate>
        }
      </Col>
    </FormGroup>;
  }
}

type BlankItemFieldModalAlertProps = {
  hide: Function,
  onSkip: Function,
  emptyCodeRows: Array<string>,
  noun: string,
  nouns: string
}

function BlankItemFieldModalAlert(props: BlankItemFieldModalAlertProps) {
  const { hide, onSkip, emptyCodeRows, noun, nouns } = props;
  const emptyCodesLabel = emptyCodeRows.map(cr => `#${cr} `);
  const [label, skipLabel] = emptyCodeRows.length > 1 ?
      [`Encountered  blank ${nouns} on lines`, 'Skip Those Lines'] : [`Encountered a blank ${noun} on line`, 'Skip Line'];

  return <PosImportModalAlert
    title={`Blank ${noun}`}
    bodyText={`${label} ${emptyCodesLabel.toString()}`}
    skipLabel={skipLabel}
    onSkip={onSkip}
    hide={hide}
    hideLabel="Cancel Import"
  />;
}


type PosImportModalAlertProps = {
  title: string,
  bodyText: string,
  skipLabel?: string,
  onSkip?: Function,
  hide: Function,
  hideLabel: string,
}
function PosImportModalAlert(props: PosImportModalAlertProps) {
    const { title, bodyText, skipLabel, onSkip, hide, hideLabel } = props;
    return <Modal show onHide={hide} className="small-modal pos-import-blank-item-field-modal">
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ minHeight: '90px' }}>
        <Row>
          <Col xs={12}>
            <h5>{bodyText}</h5>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Row>
          <Col xs={12}>
            {
              skipLabel && onSkip ?
                <Button
                  bsStyle="default"
                  style={{ float: 'left' }}
                  onClick={onSkip}>
                  {skipLabel}
                </Button>
              : null
            }
            <Button
              bsStyle="default"
              style={{ float: 'right' }}
              onClick={hide}>
              {hideLabel}
            </Button>
          </Col>
        </Row>
      </Modal.Footer>
    </Modal>;
}