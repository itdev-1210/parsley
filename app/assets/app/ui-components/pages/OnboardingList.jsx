import React, { Component } from 'react';

import _ from 'lodash';

import { connect } from 'react-redux';

import { reduxForm } from 'redux-form';

import { getOnboardingInfo, updateOnboardingInfo } from '../../webapi/endpoints';

import {
  Grid, Row, Col,
  Button, Modal,
  ListGroup, ListGroupItem,
  FormControl,
} from 'react-bootstrap';

import SearchableList, { ScrollableLink } from './helpers/SearchableList';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import {
  onboardingBulkEditFields,
  valueFromEvent
} from '../../utils/form-utils';

@connect(
  state => ({
    onboardingInfo: state.get('onboardingInfo'),
    listUiState: state.getIn(['searchableList', 'ongoardingInfo', 'steps'])
  })
)
export default class OnboardingList extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { location, onboardingInfo, listUiState, } = this.props;

    let content;
    if (!onboardingInfo) {
      content = <LoadingSpinner />;
    } else {
      content = <SearchableList
        searchKey="onboarding-info"
        noun="Onboarding"
        groupingFunction={item => item.get('page')}
        groupIDToHeader={(groupKey, groupItem) => {
          return <div style={{textDecoration: 'underline'}}>
            {onboardingInfo.get('pages').find(p => p.get('key') == groupKey).get('name')}
            {
              onboardingInfo.get('steps').filter(p => p.get('page') === groupKey).size > 1 ?
                <button
                  className='pull-right rearrange-steps-link'
                  onClick={() => this.setState({rearrangePageKey: groupKey})}>
                  Rearrange
                </button>
                : null
            }
          </div>
        }}
        reduxStatePath={['onboardingInfo', 'steps']}
        baseUrl={this.props.location.pathname}
        checkable={false}
        taggable={false}
        copiable={false}
        sortItemsFunc={item => item.get('index')}
        renderSectionItem={item => {
          return <ListGroupItem key={item.get('id')}>
            <ScrollableLink
                id={`list-item-onboarding-step-${item.get('id')}`}
                item={item}
                shouldScroll
                className="list-item-main-link"
                shouldScroll={item => location.state && item.get('id') === location.state.prevObjId}
                to={{
                  pathname: location.pathname + '/' + item.get('id').toString(),
                  state: {
                    searchTerm: listUiState && listUiState.get('searchTerm'),
                  }
                }}
              >
              {item.get('name')}
            </ScrollableLink>
          </ListGroupItem>
        }}
        mainBodyOptions={{md: 8, xs: 12}}
      />;
    }

    return <Grid>
      {content}
      {
        this.state.rearrangePageKey ?
          <OnboardingStepArranger
            pageKey={this.state.rearrangePageKey}
            onboardingInfo={onboardingInfo}
            hide={() => this.setState({rearrangePageKey: null})}
          />
          : null
      }
    </Grid>;
  }

  componentWillMount() {
    getOnboardingInfo();
  }
}


const rearrangeValidator = (values, props) => {

};

@reduxForm(
  {
     form: 'onboarding_step_arranger',
     fields: onboardingBulkEditFields,
     getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    return {
      initialValues: ownProps.onboardingInfo.toJS(),
    }
  }
)
class OnboardingStepArranger extends Component {

  submit(values) {

    const numDirtyPage = _(values.steps)
      .groupBy(step => step.page)
      .mapValues(steps => steps.map(s => s.index))
      .find(v => !_.isEqual(v.sort(), _.range(0, v.length)));

    if (numDirtyPage) {
      return Promise.reject({_error: "Please arrange step number from 0 to some positive number."})
    }

    return updateOnboardingInfo(values).then(getOnboardingInfo).then(this.props.hide);
  }

  render() {
    const {
      pageKey, onboardingInfo, hide,
      fields, error: globalError, handleSubmit,
    } = this.props;
    const thisPage = onboardingInfo.get('pages').find(p => p.get('key') === pageKey);
    const thisPageSteps = fields.steps.filter(step => step.page.value === thisPage.get('key'));

    const indexWidthProps = {xs: 1};
    const titleWidthProps = {xs: 11};
    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>{thisPage.get('name')} Steps</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { globalError && <p className="text-danger">{globalError}</p>}
        <ListGroup>
          <ListGroupItem>
            <Row>
              <Col {...indexWidthProps}>Index</Col>
              <Col {...titleWidthProps}>Title</Col>
            </Row>
          </ListGroupItem>
          {thisPageSteps.sort(s => s.index.initialValue).map ( step => <ListGroupItem>
              <Row>
                <Col {...indexWidthProps}>
                  <FormControl
                    id={`onboarding-step-aranger-${step.id.value}`}
                    type='number'
                    {...step.index}
                  />
                </Col>
                <Col {...titleWidthProps}>{step.title.value}</Col>
              </Row>
            </ListGroupItem>
          )}
        </ListGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleSubmit(this.submit.bind(this))} >Save</Button>
      </Modal.Footer>
    </Modal>
  }
}
