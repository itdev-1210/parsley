import React, { Component } from 'react';

import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import { history } from '../../navigation';
import { bindActionCreators } from 'redux';

import _ from 'lodash';
import moment from 'moment';
import Immutable from 'immutable';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';
import OrderList from './OrderList';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';
import Select from '../Select';
import CurrencyInput from '../CurrencyInput';

import PackagedAmountInput from '../PackagedAmountInput';
import { supplierIdToName } from './SupplierList';
import { MaskedNumberInput } from '../MaskedInput';
import AddButton from '../AddButton';

import {
  Row, Col, ListGroup, ListGroupItem,
} from 'react-bootstrap';

import { actionCreators as PurchaseDetailsActions } from '../../reducers/purchaseOrderDetails';
import {
  getMeasures,
  getOrderList,
  getSupplierList,
  getPurchaseDetails,
  receivePurchase,
  getOnboardingInfo,
  getSupplierSourcedIngredients, getIngredientList, getMultipleProductsUsageInfo, getProductUsageInfo,
} from '../../webapi/endpoints';

import { packageInfoFromTxn, effectivePackageSize, costingSize, receivingUnitName } from '../../utils/packaging';
import { valueFromEvent, shallowToObject } from '../../utils/form-utils';
import { divideQuantities, purchasingRound, convert } from '../../utils/unit-conversions';
import {
  checkFields, deepArrayField, deepObjectField,
  required, positive, number,
} from '../../utils/validation-utils';
import { dateFormat, dateWithWeekdayFormat, timeFormat } from 'common/utils/constants';
import { FieldArray } from '../../utils/redux-upgrade-shims';

function validate(values) {

  const receivePurchaseValidator = {
    supplier: required(),
    transaction: deepObjectField({
      // other transaction fields are filled in by handleSave
      deltas: deepArrayField(deepObjectField({
        product: required(positive),
        quantity: deepObjectField({
          amount: required(number),
          measure: required(),
          unit: required(),
        }),
      })),
    }),
  };

  return checkFields(values, receivePurchaseValidator);
}

const ingredientSorter = (product, allIngredients) => allIngredients
    .find(i => i.get('id') === product)
    .get('name');

@withRouter
@reduxForm(
  {
    form: 'receive-purchase',
    fields: [
      'supplier',
      'transaction.deltas[].product',
      'transaction.deltas[].quantity.amount',
      'transaction.deltas[].quantity.measure',
      'transaction.deltas[].quantity.unit',
      'transaction.deltas[].productSource',
      'transaction.deltas[].cashFlow',
      'transaction.deltas[].unitCost',
      'transaction.deltas[].userAdded',
      'transaction.deltas[].packageSize',
      'transaction.deltas[].packageName',
      'transaction.deltas[].packaged',
      'transaction.deltas[].pricePer',
      'transaction.deltas[].pricingUnit',
      'transaction.deltas[].product',
      'transaction.deltas[].sourceMeasure',
      'transaction.deltas[].sourceUnit',
      'transaction.deltas[].superPackageSize',
      'transaction.deltas[].subPackageName',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
  },
  (state, ownProps) => {
    const orders = state.get('orders');
    const units = state.getIn(['measures', 'units']);
    const suppliers = state.get('suppliers');
    const allIngredients = state.get('ingredients');
    const detailedIngredients = state.get('detailedIngredients');
    const purchaseDetails = state.get('purchaseOrderDetails');
    const isPurchaseReceived = purchaseDetails && (purchaseDetails.get('confirmedTransaction') || purchaseDetails.get('receivedDirectly'));
    const supplierSourcedIngredients = state.get('supplierSourcedIngredients');
    const orderedTxnDeltas = purchaseDetails && purchaseDetails.getIn(['transaction', 'deltas'], Immutable.List());
    const mainInfoLoaded = orders && units && suppliers && allIngredients && detailedIngredients && purchaseDetails && supplierSourcedIngredients;
    const isReceivedDirectly = purchaseDetails && purchaseDetails.get('receivedDirectly');
    const getTransaction = () => {
      const confirmedTransaction = !isReceivedDirectly ? purchaseDetails.get('confirmedTransaction') : purchaseDetails.get('transaction');
      if (confirmedTransaction)
        return confirmedTransaction.get('deltas');
      else
        return purchaseDetails.getIn(['transaction', 'deltas']);
    };
    return {
      isReceivedDirectly,
      submitText: 'Confirm Receipt',
      shouldHideDelete: true,
      editingDisabled: purchaseDetails && (purchaseDetails.get('confirmedTransaction') || purchaseDetails.get('receivedDirectly')),
      orders,
      units,
      suppliers,
      allIngredients,
      detailedIngredients,
      purchaseDetails,
      supplierSourcedIngredients,
      mainInfoLoaded,
      allMeasures: state.getIn(['measures', 'measures']),
      isPurchaseReceived,
      initialValues: {
        id: CrudEditor.sourceId(ownProps.params),
        supplier: purchaseDetails && purchaseDetails.get('supplier'),
        transaction: {
          deltas: mainInfoLoaded && !detailedIngredients.isEmpty()
            && getTransaction().map(delta => {
              const supplierId = purchaseDetails.get('supplier');
              const thisSupplierSources = supplierSourcedIngredients.get(supplierId);
              const thisProductSource = thisSupplierSources && thisSupplierSources.find(s => s.get('id') === delta.get('productSource'));
              const thisProductCostingSize = costingSize(thisProductSource);
              const detailedProductInfo = detailedIngredients.get(delta.get('product'));
              const retDel = delta
                .update('cashFlow', thisCashFlow => {
                  let deltaCostingSize;
                  let cashFlow;
                  if (thisProductSource) {
                    deltaCostingSize = thisProductCostingSize;
                    cashFlow = thisProductSource.get('cost');
                  } else {
                    const txnDel = orderedTxnDeltas.find(d => d.get('product') === delta.get('product'));
                    deltaCostingSize = costingSize(packageInfoFromTxn(txnDel));
                    cashFlow = thisCashFlow;
                  }

                  if (detailedProductInfo && units && deltaCostingSize && !isPurchaseReceived) {
                    return purchasingRound(
                        purchasingRound(
                          divideQuantities(
                            delta.get('quantity', Immutable.Map()).toJS(),
                            deltaCostingSize,
                            detailedProductInfo.get('measures'),
                          ),
                          units.getIn([delta.getIn(['quantity', 'unit']), 'precision']),
                        ) * cashFlow,
                        2,
                      );
                  } else {
                    return thisCashFlow;
                  }
                })
                .update('quantity', thisQuantity => units ?
                  Immutable.fromJS({
                    amount: purchasingRound(
                      thisQuantity.get('amount'),
                      units.getIn([thisQuantity.get('unit'), 'precision']),
                    ),
                    measure: thisQuantity.get('measure'),
                    unit: thisQuantity.get('unit'),
                  })
                  : thisQuantity,
                );
              return retDel
                .set('unitCost', (thisProductSource && thisProductSource.get('cost')) || delta.get('cashFlow'))
                .toJS();
            })
            .sortBy(delta => ingredientSorter(delta.product, allIngredients))
            .toJS(),
        },
      },
    };
  },
  dispatch => ({
    purchaseDetailsActions: bindActionCreators(PurchaseDetailsActions, dispatch),
  }),
)
export default class PurchaseOrderEdit extends CrudEditor {

  isOrderInPurchase = orderId => this.props.purchaseDetails.get('orders').contains(orderId);

  deleteItem() {
    return Promise.resolve(0);
  }

  addItem() {
    const { deltas } = this.props.fields.transaction;
    deltas.addField({ userAdded: true });
  }

  handleSave = (id, values) => {

    values.transaction.isFinalized = true;
    values.transaction.xactType = 'purchase-order';
    values.transaction.time = (new Date()).toISOString();
    let transactionDeltas = [...values.transaction.deltas];

    values.transaction.deltas = transactionDeltas.filter(td => td.product).map(delta => ({
        ...delta,
        cashFlow: -1 * delta.cashFlow,
      }));

    return receivePurchase(id, values).then(() => history.goBack());
  };

  renderBody() {
    const {
      editingDisabled, allMeasures, isReceivedDirectly,
      units, orders, suppliers, purchaseDetails, isPurchaseReceived,
      allIngredients, detailedIngredients, supplierSourcedIngredients,
      fields: { transaction: { deltas } },
    } = this.props;
    const sourcedIngredients = supplierSourcedIngredients.get(purchaseDetails.get('supplier'));
    const purchasedTime = moment(purchaseDetails.get('emailedAt') || purchaseDetails.get('printedAt') || purchaseDetails.get('importedAt'));
    const totalCost = (
      editingDisabled ?
        purchaseDetails.getIn([isReceivedDirectly ? 'transaction' : 'confirmedTransaction', 'deltas']).map(d => d.get('cashFlow'))
        : deltas.map(d => d.cashFlow.value)
      ).filter(c => _.isNumber(c)).reduce((a, b) => a + b, 0);
    return <div className="receive-purchase-container">
      <Row className="mb-1">
        <Col xs={12} md={8} style={{ fontSize: '20px' }}>
          {purchaseDetails.get('emailedAt') ? 'Emailed to' : isReceivedDirectly ? 'Received from' : purchaseDetails.get('printedAt') ? 'Printed for' : ''} {supplierIdToName(purchaseDetails.get('supplier'), suppliers)}
          {isReceivedDirectly ? '' : ` on ${purchasedTime.format(dateFormat)}, ${purchasedTime.format(timeFormat)}`}
          {isReceivedDirectly ? '' : ` by ${purchaseDetails.get('creatorFirstName')} ${purchaseDetails.get('creatorLastName')}`}
          { isPurchaseReceived ? <br/> : null }
          {isPurchaseReceived ?
            <span style={{ textDecoration: 'underline', fontSize: '17px' }}>
            Received on {moment(purchaseDetails.get('receivedAt')).format(dateWithWeekdayFormat)} at {`${moment(purchaseDetails.get('receivedAt')).format(timeFormat)} `}
            by {`${purchaseDetails.get('receiverFirstName')} ${purchaseDetails.get('receiverLastName')}`}
            </span>
            : null
          }
        </Col>
      </Row>
      {
        !isReceivedDirectly && purchaseDetails.get('orders').size > 0 ?
        <Row className="mb-1">
          <Col xs={12}>Orders for</Col>
          <Col xs={12} style={{ paddingLeft: '0' }}>
            <ListGroup>
              {
                orders
                  .groupBy(OrderList.groupingFunction)
                  .filter(v => v.map(o => o.get('id')).some(orderId => this.isOrderInPurchase(orderId)))
                  .map(v => {
                    if (v.map(o => o.get('id')).every(orderId => this.isOrderInPurchase(orderId))) {
                      return Immutable.List([
                        <ListGroupItem className="mb-1 receive-purchase-forecast-item">
                          {moment(v.first().get('date')).format(dateWithWeekdayFormat)}
                        </ListGroupItem>,
                      ]);
                    } else {
                      return v.filter(o => this.isOrderInPurchase(o.get('id'))).map(o =>
                        <ListGroupItem className="mb-1 receive-purchase-forecast-item">
                          {`${o.get('name')} (${moment(o.get('date')).format(dateWithWeekdayFormat)})`}
                        </ListGroupItem>,
                      );
                    }
                  }).valueSeq().flatten(true).toArray()
              }
            </ListGroup>
          </Col>
        </Row>
        : null
      }
      <Row>
        <Col xs={12}>
          <Row>
            <Col xs={3} className="mb-1">Ingredient</Col>
            {
              !purchaseDetails.get('imported') && !isReceivedDirectly ?
                <Col xs={3} className="mb-1">
                  Ordered
                </Col>
              : <Col xs={3} />
            }
            <Col xs={3} className={'mb-1'}>
              Received
            </Col>
            <Col xs={2} className="mb-1">Unit Price</Col>
            <Col xs={1} className="mb-1">{!isPurchaseReceived ? 'Total' : 'Cost'}</Col>
          </Row>
        </Col>
        <Col xs={12} style={{ paddingLeft: '0' }}>
          <ListGroup>
            <FieldArray fields={deltas} component={ItemsEditor} props={{
              allIngredients, isReceivedDirectly,
              purchaseDetails, isPurchaseReceived,
              sourcedIngredients, detailedIngredients, allMeasures, units,
            }}
            />
          </ListGroup>
        </Col>
      </Row>
      <Row className="receiving-footer">
        <Col xs={12} md={2} className="text-left">
          {
            !editingDisabled &&
            <AddButton onClick={this.addItem.bind(this)}/>
          }
        </Col>
        <Col xs={12} md={2} mdOffset={8} className="text-right">Total: ${purchasingRound(totalCost, 2)}</Col>
      </Row>
      <Onboarding pageKey="receiving_editor" />
    </div>;
  }

  async componentWillMount() {
    getMeasures();
    getOrderList();
    getSupplierList();
    getIngredientList();
    getOnboardingInfo();
    const order = await getPurchaseDetails(this.sourceId(this.props.params));
    getMultipleProductsUsageInfo(
        order.transaction.deltas.map(d => d.product),
    );
    getSupplierSourcedIngredients(order.supplier);
  }

  componentWillUnmount() {
    this.props.purchaseDetailsActions.dropDetails();
  }

  listUrl = '/purchase_orders';
  listTitle = 'Receiving';
  showFooterExtraButtons = false;
  bodyWidthSet = { xs: 12, md: 9, mdOffset: 2 }
  headerWidthSet = { xs: 12, md: 9, mdOffset: 2 }
  footerWidthSet = { xs: 5, sm: 4, md: 5 }
}

class ItemsEditor extends Component {
 render() {
   const { fields: deltas,
     purchaseDetails, isPurchaseReceived, isReceivedDirectly,
     allIngredients,
     sourcedIngredients, detailedIngredients, allMeasures, units,
   } = this.props;
   if (!deltas) {
     return undefined;
   }
   return deltas.map((txnDel, idx) => {

     const orderedTxnDel = purchaseDetails.getIn(['transaction', 'deltas'], Immutable.List()).find(delta => delta.get('product') === txnDel.product.value);
     const confirmedTxnDel = isReceivedDirectly
          ? orderedTxnDel
          : purchaseDetails.getIn(['confirmedTransaction', 'deltas'], Immutable.List()).find(delta => delta.get('product') === txnDel.product.value);
     const ingredientInfo = detailedIngredients.get(txnDel.product.value);
     const conversionsForReceived = isPurchaseReceived && confirmedTxnDel.get('measures');
     const conversionsForOrdered = isPurchaseReceived
         ? conversionsForReceived
         : ingredientInfo && ingredientInfo.get('measures');
     const thisIngredientSources = ingredientInfo && sourcedIngredients
         && sourcedIngredients.filter(s => s.get('product') === txnDel.product.value);
     const packageOptions = thisIngredientSources && thisIngredientSources.map(s => ({
       value: s.get('id'),
       label: PackagedAmountInput.fullPackageName(s, conversionsForOrdered),
     }));
     const {
       product, quantity, productSource, cashFlow,
       unitCost, userAdded,
     } = txnDel;

     const productSourceForReceived = isPurchaseReceived && packageInfoFromTxn(confirmedTxnDel);
     const isProductSourceDeleted = productSource && thisIngredientSources && !thisIngredientSources.some(s => s.get('id') === productSource.value);

     const productSourceForOrdered = isPurchaseReceived
         ? productSourceForReceived
         : isProductSourceDeleted ?
           packageInfoFromTxn(orderedTxnDel)
           : productSource && thisIngredientSources && thisIngredientSources.find(s => s.get('id') === productSource.value);

     const costingSizeForOrdered = costingSize(productSourceForOrdered);
     const costingSizeFoReceived = isPurchaseReceived && costingSize(productSourceForReceived);

     const effectivePackageForOrdered = isPurchaseReceived
         ? effectivePackageSize(productSourceForReceived)
         : productSourceForOrdered && effectivePackageSize(productSourceForOrdered);

     if (!userAdded.value && !(product && packageOptions && effectivePackageForOrdered)) {
       return <ListGroupItem><Row><Col xs={12}><LoadingSpinner/></Col></Row></ListGroupItem>;
     }

     let itemNameDisplay;
     if (userAdded.value) {

       let unorderedItems = Immutable.Set();
       if (sourcedIngredients && allIngredients) {
         const orderedItems = deltas.map(i => i.product.value).filter(p => p !== product.value);
         const unorderedItemIds = sourcedIngredients.map(s => s.get('product'))
             .toSet().subtract(orderedItems);
         unorderedItems = allIngredients.filter(i => unorderedItemIds.includes(i.get('id')))
             .map(i => ({ value: i.get('id'), label: i.get('name') })).toArray();
       }

       itemNameDisplay = <Validate {...txnDel.product}><Select
           {...txnDel.product}
           options={unorderedItems}
           onChange={ev => {
             const newProductId = valueFromEvent(ev);
             if (!detailedIngredients.has(newProductId)) {
               getProductUsageInfo(newProductId);
             }

             const newIngredientSource = sourcedIngredients && sourcedIngredients.filter(s => s.get('product') === newProductId);
             if (newIngredientSource && !newIngredientSource.isEmpty()) {
               const ingredientSource = newIngredientSource.first();
               txnDel.unitCost.onChange(ingredientSource.get('cost'));
               txnDel.productSource.onChange(ingredientSource.get('id'));
               // use same value or set it to zero if absent
               txnDel.quantity.amount.onChange(txnDel.quantity.amount.value || 0);
               txnDel.quantity.measure.onChange(ingredientSource.get('measure'));
               txnDel.quantity.unit.onChange(ingredientSource.get('unit'));
             } else {
               txnDel.productSource.onChange(null);
             }
             txnDel.product.onChange(ev);
           }}
       /></Validate>;
     } else if (!userAdded.value) {
       itemNameDisplay = ingredientInfo ? ingredientInfo.get('name') : '';
     }

     const confirmedTxnAmount = ingredientInfo && isPurchaseReceived && divideQuantities(
         confirmedTxnDel.get('quantity').toJS(),
         costingSizeFoReceived,
         conversionsForReceived,
     );
     const receivingPrecision = units && units.getIn([quantity.unit.value, 'precision']);
     const amountReceiving = ingredientInfo && purchasingRound(
         divideQuantities(
             shallowToObject(quantity),
             costingSizeForOrdered,
             conversionsForOrdered,
         ),
         receivingPrecision,
     );
     return <ListGroupItem className="receive-purchase-item mb-1">
       <Row>
         <Col xs={3}>{itemNameDisplay}</Col>
         {product.value && <React.Fragment>
           {
             isReceivedDirectly || purchaseDetails.get('imported') || userAdded.value || !orderedTxnDel ? <Col xs={3} />
                 : <Col xs={3}>
                   {purchasingRound(divideQuantities(orderedTxnDel.get('quantity').toJS(), effectivePackageForOrdered, conversionsForOrdered), 2)}
                   <span>
                          {productSourceForOrdered.get('packaged') ? 'x' : ' '}
                     {
                       !isPurchaseReceived && !isProductSourceDeleted
                           ? packageOptions.find(po => po.value === productSource.value).label
                           : PackagedAmountInput.fullPackageName(productSourceForOrdered, conversionsForOrdered)
                     }
                        </span>
                 </Col>
           }
           {
             !isPurchaseReceived && allMeasures
                 ? <Col xs={3}>
                   <MaskedNumberInput
                       style={{ width: '20%', marginRight: '10px' }}
                       disabled={!productSourceForOrdered}
                       normalizeInitial
                       precision={units.getIn([quantity.unit.value, 'precision'])}
                       quantum={units.getIn([quantity.unit.value, 'quantum'])}
                       value={amountReceiving}
                       onChange={(ev) => {
                         const newAmount = valueFromEvent(ev);
                         quantity.amount.onChange(
                             purchasingRound(
                                 convert(
                                     newAmount * costingSizeForOrdered.amount,
                                     costingSizeForOrdered.unit,
                                     quantity.unit.value,
                                     conversionsForOrdered,
                                 ),
                                 receivingPrecision,
                             ),
                         );
                         cashFlow.onChange(
                             purchasingRound(
                                 newAmount * unitCost.value,
                                 2,
                             ),
                         );
                       }}
                   />
                   {productSourceForOrdered && ingredientInfo && receivingUnitName(productSourceForOrdered, conversionsForOrdered, false)}
                 </Col>
                 : <Col xs={3}>
                   {purchasingRound(confirmedTxnAmount, receivingPrecision)}
                   &nbsp;{productSourceForReceived && receivingUnitName(productSourceForReceived, conversionsForReceived, false)}
                 </Col>
           }
           {
             !isPurchaseReceived && allMeasures
                 ? <Col xs={2} style={{ padding: '0' }}>
                   <div className="receiving-unit-cost">
                     <CurrencyInput
                         inModal={false} compact placeholder="Cost"
                         {...unitCost}
                         onChange={e => {

                           const newUnitCost = valueFromEvent(e);
                           cashFlow.onChange(
                               purchasingRound(
                                   amountReceiving * newUnitCost,
                                   2,
                               ),
                           );

                           unitCost.onChange(e);
                         }}
                     />
                   </div>
                   {productSourceForOrdered && <div className="receiving-unit-source" style={{ verticalAlign: 'super' }}>
                     &nbsp;{`per ${receivingUnitName(productSourceForOrdered, conversionsForOrdered)}`}
                   </div>}
                 </Col>
                 : <Col xs={2}>
                   ${confirmedTxnAmount === 0 ? 0 : purchasingRound(confirmedTxnDel.get('cashFlow') / confirmedTxnAmount, 2)} &nbsp;
                   {productSourceForReceived && `per ${receivingUnitName(productSourceForReceived, conversionsForReceived)}`}
                 </Col>
           }
           {
             !isPurchaseReceived && allMeasures
                 ? <Col xs={1} className="receiving-cash-flow">${cashFlow.value}</Col>
                 : <Col xs={1} className="receiving-cash-flow">${confirmedTxnDel.get('cashFlow')}</Col>
           }
         </React.Fragment>
         }
       </Row>
     </ListGroupItem>;
   });
 }
}
