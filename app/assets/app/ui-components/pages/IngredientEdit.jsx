// @flow
import React from 'react';
import Immutable from 'immutable';
import _ from 'lodash';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';

import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import {
  Button, Collapse,
  ControlLabel, Form, FormGroup,
  Col, Row,
} from 'react-bootstrap';

import { getMeasures, getSupplierList, getRecipeList, getProductDetails, getOnboardingInfo,
  saveProductSources,
  saveProduct, deleteProduct, lookupProductName, getReverseDependencies, getNutrients, getProductUsageInfo } from '../../webapi/endpoints';

import { withRouter } from 'react-router';
import { history } from '../../navigation';

import { actionCreators as ProductActions } from '../../reducers/productDetails';
import { actionCreators as IngredientActions } from '../../reducers/detailedIngredients';
import { actionCreators as ReverseDependencyActions } from '../../reducers/reverseDependencies';

import {
  checkFields, commonProductValidators, isBlank, number, positive, required,
} from '../../utils/validation-utils';
import { getLowestCostSourceId } from '../../utils/packaging';

import {
    convertProductFormValuesToJSON,
    productFormFields,
    convertProductJSONToFormValues,
    getEmptyNutrientInfo,
    getNNDBSRIdToNutrientIdMap,
    shallowToObject,
} from '../../utils/form-utils';

import Section from '../Section';
import TitleInput from '../TitleInput';
import TagSelector from '../TagSelector';
import Checkbox from '../Checkbox';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';

import ConversionEditor from '../ConversionEditor';
import SourceEditor, { InHouseDummySource } from '../SourceEditor';
import SimplePreparationsEditor from '../SimplePreparationsEditor';
import RecipesListUsingIngredients from '../RecipesListUsingIngredients';
import { handleLowestPricePreferred } from '../modals/IngredientSourcesModal';
import NutritionDisplay, {
  nutrient_fields,
} from '../NutritionDisplay';
import { featureIsSupported } from '../../utils/features';

import type { ProductInfo, ProductSource } from '../utils/PropTypes';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { noticeError } from 'common/utils/errorAnalytics';

export function validate(values: Object) {

  const ingredientValidators = {
    ...commonProductValidators,
  };

  let errors = checkFields(values, ingredientValidators);

  let parentMeasures = values.measures.map(_.iteratee('measure'));

  /*
   * global checking - simplePreps. Not necessary in RecipeEdit, since simple preps are not modified there,
   * only echoed back to the server
   */
  _.merge(errors.simplePreps, values.simplePreps.map(prep => {
    if (!prep.yieldMeasure) return {}; // error should not override missing-value error

    let prepMeasures = prep.usesParentMeasures
            ? parentMeasures
            : prep.measures.map(_.iteratee('measure'));
    let validYieldMeasures = _.intersection(prepMeasures, parentMeasures);

    if (_.isEmpty(validYieldMeasures)) {
      return { yieldMeasure: 'An ingredient and its preparation must have at least one measure in common, so a yield can be calculated' };
    } else if (!validYieldMeasures.includes(prep.yieldMeasure)) {
      return { yieldMeasure: 'Measure must also be defined for both raw ingredient and preparation' };
    } else {
      return {};
    }
  }));

  _.merge(errors.simplePreps, SimplePreparationsEditor.validationFunc(values.simplePreps));

  if (values.nutrientInfo.some(nutrient => nutrient.amount)) { // exists and is not zero
    // can't do the necessary math if no serving size
    const servingSizeValidators = {
      nutrientServingAmount: required(positive),
      nutrientServingMeasure: required(number),
      nutrientServingUnit: required(number),
    };

    _.merge(errors, checkFields(values, servingSizeValidators));
  }

  const inventoryTagNames = (values.inventoryTags || []).map(t => t.name.toLowerCase());
  if (_.some(values.tags, t => inventoryTagNames.indexOf(t.name.toLowerCase()) !== -1)) {
    errors.inventoryTags = 'Overlapping tags';
  }

  return errors;
}

export const newIngredientTemplate = Immutable.fromJS({
  info: {
    lowestPricePreferred: true,
  },
  ingredient: true,
  salable: false,
  measures: [
    {
      isPrimary: true,
      conversion: 1,
    },
  ],

  ingredientTags: [],
  inventoryTags: [],

  allergens: {
    fish: null,
    crustaceanShellfish: null,
    treeNuts: null,
  },
});

export function asyncValidate (isRunningFromModal: boolean, values: Object, dispatch: $FlowTODO, props: $FlowTODO): Promise<void> {
  if (props.initialValues.name === values.name.trim()) {
    return Promise.resolve();
  }
  return lookupProductName(values.name.trim())
    .then(response => {
      if (response && (isRunningFromModal || CrudEditor.sourceId(props.params) !== response)) {
        // error message should match RecipeEdit.jsx::asyncValidate and api.Products.createProduct
        throw { name: 'Name is already used' };
      }
    });
}

export function usedMeasuresWithMessages(
    sources: Array<ProductSource>, simplePreps: Array<$FlowTODO>, nutrientServingMeasure: ?number,
): Immutable.Map<number, string> {
  let usedMeasureMessages = Immutable.Map();

  let sourceUsedMeasures = sources
    .map(s => s.measure)
    .filter(_.negate(isBlank));
  usedMeasureMessages = usedMeasureMessages.merge(Immutable.Map(
    sourceUsedMeasures.map(measureId => [measureId, 'supplier/s']),
  ));

  let prepUsedMeasures = simplePreps
    .map(s => s.yieldMeasure)
    .filter(_.negate(isBlank));
  usedMeasureMessages = usedMeasureMessages.merge(Immutable.Map(
    prepUsedMeasures.map(measureId => [measureId, 'preparation yields']),
  ));

  if (nutrientServingMeasure) {
    usedMeasureMessages = usedMeasureMessages.set(nutrientServingMeasure, 'nutrient serving size');
  }

  return usedMeasureMessages;
}

export function sortSourcesBySupplierNameTransformer(allSuppliers: Immutable.List<$FlowTODO>, formValues: Immutable.Map<string, $FlowTODO>, params: Object) {

  return formValues && formValues.update('sources', sources =>
      sources && sources.sortBy(source =>
          allSuppliers.find(s => s.get('id') === source.get('supplier'), null, Immutable.Map()).get('name'),
      ),
  );
}

@withRouter
@reduxForm(
    {
      form: 'all',
      fields: productFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      asyncValidate: _.partial(asyncValidate, false),
      asyncBlurFields: ['name'],
      onSubmitSuccess: () => history.push('/ingredients'),
    },
    (state, ownProps) => {

      const userInfo = state.get('userInfo');
      const allNutrients = state.get('nutrients');
      const allProductDetails = state.get('productDetails');
      const productDetails = allProductDetails.get(parseInt(CrudEditor.sourceId(ownProps.params)));

      const params = ownProps.params;
      params.allNutrients = allNutrients;

      const isOperationsUser = userInfo && userInfo.get('permissionLevel') === 'operations';
      const isRecipeReadOnly = userInfo && userInfo.get('permissionLevel') === 'recipe-read-only';
      const isSyncedIngredient = Boolean(userInfo && userInfo.get('isMultiLocationDownstream'))
        && Boolean(productDetails && productDetails.syncUpstream && productDetails.syncUpstreamOwner);
      const allSuppliers = state.get('suppliers');
      return {
        initialValues: !CrudEditor.isCreatingNew(ownProps.params) || CrudEditor.isCopying(ownProps.params)
        ? CrudEditor.initialValues(
            productDetails,
            params,
            convertProductJSONToFormValues,
            _.partial(sortSourcesBySupplierNameTransformer, allSuppliers),
          )
        : newIngredientTemplate.set('nutrientInfo', getEmptyNutrientInfo(allNutrients)).toJS(),
        detailedIngredients: state.get('detailedIngredients') || Immutable.Map(),
        productDetails,
        mainInfoLoaded: productDetails && allNutrients,
        allMeasures: state.getIn(['measures', 'measures']),
        allSuppliers,
        allNutrients,
        onboardingInfo: state.get('onboardingInfo'),
        shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
        isOperationsUser,
        isRecipeReadOnly,
        isSyncedIngredient,
        deleteDisabled: isOperationsUser,
        isReadOnly: isOperationsUser || isSyncedIngredient,
        upstreamProductInfo: productDetails && Immutable.fromJS(productDetails.upstreamInfo),
        userVolumePref: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        userWeightPref: state.getIn(['userInfo', 'purchaseWeightSystem']),
      };
    },
    dispatch => ({
      productActions: bindActionCreators(ProductActions, dispatch),
      ingredientActions: bindActionCreators(IngredientActions, dispatch),
      reverseDependencyActions: bindActionCreators(ReverseDependencyActions, dispatch),
    }),
)
export default class IngredientEdit extends CrudEditor {

  handleSave(id: number, deets: ProductInfo) {
    const {
      ingredientActions,
      params,
      isOperationsUser,
    } = this.props;
    const jsonValue = convertProductFormValuesToJSON(deets);
    const { info: { lowestPricePreferred }, purchaseInfo } = convertProductFormValuesToJSON(deets);
    return (isOperationsUser
          ? saveProductSources(id, { lowestPricePreferred, purchaseInfo })
          : saveProduct(id, jsonValue)
        )
        .then(success => {
          if (params.id) ingredientActions.dropInfo(params.id);
          return success;
        });
  }

  renderBody() {

    const {
      name, measures, sources, ingredientTags, recipeTags, inventoryTags, recipe, simplePreps, nutrientInfo,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      allergens, characteristics,
      info: { lowestPricePreferred },
    } = this.props.fields;

    const {
      allMeasures, allSuppliers, allNutrients, untouch, productDetails, detailedIngredients,
      values, userVolumePref, userWeightPref, isReadOnly, isSyncedIngredient, isOperationsUser, isRecipeReadOnly, upstreamProductInfo,
    } = this.props;

    const isSubRecipe = values.recipe.recipeSize.amount; // proxy for "do we even have a recipe"

    const reverseDependencies = this.reverseDependencies;
    const conversions = Immutable.fromJS(values.measures);

    const conversionsReady = (!this.sourceId() || measures.length > 0) && Boolean(allMeasures);
    const sourcesReady = conversionsReady && measures.length > 0 && Boolean(allSuppliers);

    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);
    const haveEnoughNutrientData = _.every(nutrient_fields, nndbsrId => nndbsrIdToNutrientIdMap[nndbsrId]);
    if (allNutrients && !haveEnoughNutrientData) {
      noticeError('Lacking nutrient data for NutritionDisplay');
    }

    return <div className={isReadOnly ? 'readonly' : ''}>
      <Form horizontal>
        <Row>
          <Col xs={5}>
            <TitleInput
                disabled={isReadOnly}
                noun="Ingredient"
                inputRef={input => this.initialInput = input}
                {...name}
                style={isSubRecipe ? { fontStyle: 'italic' } : null}
            />
          </Col>
        </Row>
        <FormGroup>
          <Col xs={2} componentClass={ControlLabel}>GL Categories:</Col>
          <Col xs={10} md={5}>
            <TagSelector
                {...ingredientTags}
                optional
                tagType="ingredients"
                optionName="category"
                disabled={isOperationsUser}
            />
          </Col>
        </FormGroup>
        { !isSubRecipe ? null
        : <FormGroup>
          <Col xs={2} componentClass={ControlLabel}>Recipe Categories:</Col>
          <Col xs={10} md={5}>
            <TagSelector
                {...recipeTags}
                optional
                tagType="recipes"
                optionName="category"
                disabled={isOperationsUser}
            />
          </Col>
        </FormGroup>
        }
        <FormGroup>
          <Col xs={2} componentClass={ControlLabel}>Ingredient Locations:</Col>
          <Col xs={10} md={5}>
            <Validate compact {...inventoryTags}>
              <TagSelector
                  {...inventoryTags}
                  optional
                  tagType="inventories"
                  disabled={isOperationsUser}
                  optionName="location"
              />
            </Validate>
          </Col>
        </FormGroup>
      </Form>
      <Section title="Measurement Conversions">
        { conversionsReady ?
            <FieldArray
                component={ConversionEditor}
                fields={measures}
                props={{
                  uniqueName: 'ingredient-edit-conversion-editor-main',
                  usedMeasures: this.usedMeasuresWithMessages(),
                  allMeasures,
                  reverseDependencies,
                  upstreamProductInfo: isSyncedIngredient && upstreamProductInfo,
                  disabled: isOperationsUser,
                }}
            /> : <LoadingSpinner /> }
      </Section>
      { haveEnoughNutrientData ?
        <Section title="Nutrition Info" collapsible>
          {allNutrients ?
            <div>
            { productDetails && productDetails.recipe ?
              <NutritionDisplay
                isReadOnly={isOperationsUser}
                productType="recipes"
                isSubRecipeInIngredientEdit
                productId={this.sourceId()}
                allNutrients={allNutrients}
                conversions={conversions}
                measures={allMeasures}
                detailedIngredients={detailedIngredients}
                nutrientServingAmount={nutrientServingAmount}
                nutrientServingMeasure={nutrientServingMeasure}
                nutrientServingUnit={nutrientServingUnit}
                recipe={productDetails.recipe}
              />
              : <NutritionDisplay
                isReadOnly={isReadOnly}
                productType="ingredients"
                nutrientInfo={nutrientInfo}
                allNutrients={allNutrients}
                nutrientServingAmount={nutrientServingAmount}
                nutrientServingMeasure={nutrientServingMeasure}
                nutrientServingUnit={nutrientServingUnit}
                conversions={conversions}
                measures={allMeasures}
                allergens={allergens}
                characteristics={characteristics}
              />
            }
            </div>
          : <LoadingSpinner/>}
        </Section> : null
      }
      <Section
        className="supplied-ingredient-section"
        >
        {sourcesReady && <React.Fragment>
          <Row id="ing-edit-suppliers-and-costs">
            <Col xs={12}>
              <div>
                <h4>Suppliers & Costs</h4>
                <Checkbox
                  {...lowestPricePreferred}
                  onChange={ev => handleLowestPricePreferred(
                    ev,
                    lowestPricePreferred,
                    sources,
                    conversions,
                  )}
                  disabled={isRecipeReadOnly}
                  >
                  Automatically Select the Lowest Cost Option
                </Checkbox>
              </div>
            </Col>
          </Row>
        </React.Fragment>}
        { sourcesReady ?
            isSubRecipe ?
                <InHouseDummySource productId={parseInt(this.props.params.id)}
                                    uniqueName="ingredient-edit-source-editor-main"
                                    conversions={conversions}
                                    measures={allMeasures}
                                    recipeFields={recipe}
                                    isReadOnly={isReadOnly || lowestPricePreferred.value}
                /> :
                <FieldArray component={SourceEditor}
                    fields={sources}
                    props={{
                      uniqueName: 'ingredient-edit-source-editor-main',
                      conversions,
                      suppliers: allSuppliers,
                      measures: allMeasures,
                      untouch,
                      userVolumePref,
                      userWeightPref,
                      isReadOnly: isOperationsUser,
                      isSyncedIngredient,
                      isSelectable: !lowestPricePreferred.value,
                      isLowestPricePreferred: lowestPricePreferred.value,
                      disableUpdatingCost: false,
                    }}
                />
            : <LoadingSpinner />
        }
      </Section>
      <Section title="Preparations & Yields">
        <FieldArray
            component={SimplePreparationsEditor}
            fields={simplePreps}
            props={{ parentMeasures: measures, upstreamPrepKeys: upstreamProductInfo && upstreamProductInfo.get('prepKeys'), isReadOnly }}
        />
      </Section>

      {
        !this.isCreatingNew() ?
          <Section title="Used for">
            <RecipesListUsingIngredients ingredientId={parseInt(this.props.params.id)}/>
          </Section>
          : null
      }
      <Onboarding pageKey="ingredient_editor" />
    </div>;
  }

  deleteItem() {
    return deleteProduct(this.props.params.id);
  }

  listUrl = '/ingredients';
  noun = 'ingredient';

  usedMeasuresWithMessages() {
    const {
      values: {
        sources,
        simplePreps,
        nutrientServingMeasure,
      },
    } = this.props;
    return usedMeasuresWithMessages(sources, simplePreps, nutrientServingMeasure);
  }

  componentWillMount() {
    super.componentWillMount();

    const productId = this.sourceId();
    if (productId !== null) {
      getProductUsageInfo(productId);
      getProductDetails(productId).then(product => {
        if (!this.isCopying()) { // don't care about dependencies for the original
          // get recipe for ingredient and each preparation
          const ingredients = [productId,
            ..._.flatMap(product.simplePreps, val => val.outputProduct)];
          this.reverseDependencies = {};
          ingredients.forEach(ingredient => {
            getReverseDependencies(ingredient)
                .then(recipes => this.reverseDependencies[ingredient] = recipes);
          });
        }
      });
    }
    getRecipeList();
    getMeasures();
    getSupplierList();
    getOnboardingInfo();
    getNutrients();
  }

  componentWillUnmount() {
    if (this.sourceId() !== null) {
      this.props.productActions.dropDetails(this.sourceId());
      if (!this.isCopying()) {
        this.props.reverseDependencyActions.drop(this.sourceId());
      }
    }
  }
}
