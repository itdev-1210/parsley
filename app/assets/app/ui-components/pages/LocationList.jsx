import React from 'react';
import { connect } from 'react-redux';

import { Grid } from 'react-bootstrap';
import { Link } from 'react-router';
import Onboarding from '../Onboarding';

import SearchableList, { searchableListLoadingSpinner } from './helpers/SearchableList';

import {
  removeLocation,
  getLocationList,
  getOnboardingInfo,
} from '../../webapi/endpoints';

@connect(
  state => ({
    locations: state.get('locations'),
  }),
)
export default class LocationList extends React.Component {

  render() {
    const { locations, location } = this.props;

    let content;
    if (!locations) {
      content = searchableListLoadingSpinner;
    } else {
      content = <SearchableList
        noun="Location"
        reduxStatePath="locations"

        baseUrl={location.pathname}
        copiable={false}
        searchKeys={['upstreamRelativeName']}
        deleteActionText={'remove'}
        nameField='upstreamRelativeName'
        deleteItems={ids => SearchableList.deleteItems(ids, removeLocation)}
        refreshItems={getLocationList}

        prepareItemLinkText={item => {
          if (item.get('downstreamActivated')) {
            return item.get('upstreamRelativeName');
          }
          return `${item.get('upstreamRelativeName')} (Not yet activated)`
        }}
      />;
    }

    return <Grid fluid>
      {content}
      <Onboarding pageKey="location_list" />
    </Grid>;
  }

  componentWillMount() {
    getLocationList();
    getOnboardingInfo();
  }
}
