// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { withRouter } from 'react-router';
import { history } from '../../navigation';
import { connect } from 'react-redux';
import ImportReceivablesPricing from './ImportReceivablesPricing';
import { importReceipts } from '../../webapi/endpoints';

type ImportReceiptsProps = {
  details: Immutable.List<Immutable.Map<string, any>>,
};

@withRouter
@connect(
  state => {
    const details = state.get('importReceiptsDetails');
    return {
      details,
    };
  },
)
export default class ImportReceipts extends Component<ImportReceiptsProps> {

  onSaveSuppliers = (supplierData: {id: number, sources?: Array<*>}) => {
    if (supplierData.sources)
      return importReceipts(supplierData);
    else
      return Promise.resolve();
  }

  render() {
    const { details, ...rest } = this.props;
    return <div>
      {/* $FlowFixMe: can't simulate extra props passed in by decorators */}
      <ImportReceivablesPricing
        title="Import Receipts"
        details={details}
        onSaveSuppliers={this.onSaveSuppliers.bind(this)}
        onFinishImport={() => history.push('/purchase_orders')}
        {...rest}
      />
    </div>;
  }

  componentDidMount() {
    const { details } = this.props;
    if (!details) history.push('/suppliers');
  }
}