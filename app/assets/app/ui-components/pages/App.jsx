import React from 'react';
import AppNavbar from '../Navbar';
import NotificationSystem from 'react-notification-system';
import { connect } from 'react-redux';
import { actionCreators as NotifyActions } from '../../reducers/notification';
import { actionCreators as AppUIActions } from '../../reducers/uiState/appUI';
import reduxStore from '../../store';

const notificationStyle = {
  NotificationItem: {
    DefaultStyle: {
      fontFamily: 'serif',
      fontSize: '20px',
      marginTop: '12px',
      marginRight: '12px'
    }
  }
};

@connect(
  state => ({
    notification: state.get('notification'),
    fileDraggingOver: state.getIn(['appUI', 'fileDraggingOver']),
  })
)
export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.getNotificationSystem = notificationSystem => { this.notificationSystem = notificationSystem; };
    this.handleDragOver = this.handleDragOver.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.notification !== null) {
      this.notificationSystem.addNotification(newProps.notification);
      // Clear the notification in store so it won't be mistakenly shown again when App receives new props
      reduxStore.dispatch(NotifyActions.clearNotification());
    }
  }

  // When the user drags a file over our app
  handleDragOver() {
    if (!this.props.fileDraggingOver) {
      reduxStore.dispatch(AppUIActions.fileDragEnter());
    }
    this.setDragLeave();
  }

  // We will only show "drop here" areas for maximum 0.5 seconds
  // since the last drag movement, before sinaling `fileDragLeave`
  // onDragEnter & onDragLeave are way harder to control
  // and while this is not perfect, it's actually quite close
  // to how the Facebook web app works
  setDragLeave() {
    clearTimeout(this.intervalSetDragLeave);
    this.intervalSetDragLeave = setTimeout(() => {
      if (this.props.fileDraggingOver) {
        reduxStore.dispatch(AppUIActions.fileDragLeave());
      }
    }, 500);
  }

  render() {
    return (
      <div onDragOver={this.handleDragOver}>
        <AppNavbar currentLocation={this.props.location}/>
        <main>{this.props.children}</main>
        <NotificationSystem ref={this.getNotificationSystem} style={notificationStyle}/>
      </div>);
  }
}
