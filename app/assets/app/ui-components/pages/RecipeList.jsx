import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Grid, Badge } from 'react-bootstrap';

import moment from 'moment';

import { Link, withRouter } from 'react-router';

import {
  getMeasures, getRecipeList, getLocationList, deleteProduct, getMultipleProductsDetails, getMultipleProductsUsageInfo,
  getCanSyncAllLocations, syncAllLocations,
  getOnboardingInfo,
} from '../../webapi/endpoints';
import Onboarding from '../Onboarding';
import SearchableList, { searchableListLoadingSpinner } from './helpers/SearchableList';
import { nutrient_round_groups, roundNutritionAmount, nutrition_allergens, nutrition_characteristics, getRecipeAllergensString } from '../NutritionDisplay';

import { actionCreators as UIStateActions } from '../../reducers/uiState/recipeList';
import { actionCreators } from '../../reducers/uiState/searchableList';
import { unitDisplayName, convertPerQuantityValue, divideQuantities } from '../../utils/unit-conversions';

import AddToCategoryModal from '../modals/AddToCategoryModal';
import RemoveFromCategoryModal from '../modals/RemoveFromCategoryModal';
import ExportRecipeDataModal from '../modals/ExportRecipeDataModal';
import PrintMultipleRecipeInfoModal from '../modals/PrintMultipleRecipeInfoModal';

import Checkbox from '../Checkbox';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import Immutable from 'immutable';
import _ from 'lodash';
import { csvQuote, downloadCSVDataAsFile } from '../../utils/file-utils';
import { recipePrintFormatOptions } from '../../utils/form-utils';
import { featureIsSupported } from '../../utils/features';

import { isOlderThan } from '../../utils/general-algorithms';
import userInfo from 'common/redux/reducers/userInfo';

function exportCSV(title, header, rows) {
  const csv = [header, ...rows].join('\n');
  downloadCSVDataAsFile(`${title}.csv`, csv);
}

function sortRecipes(recipes) {
  _.each(recipes, (r, id) => recipes[id].id = id);
  return Immutable.Map(recipes).toList().sort((a, b) => {
    if (a.ingredient === b.ingredient) {
      return a.name.localeCompare(b.name);
    } else if (a.ingredient) {
      return 1;
    } else {
      return -1;
    }
  }).toJS();
}

function calculateCostPerPortion(r, productDetails) {
  let costPerPortion = 0;

  for (let step of r.recipe.steps) {
    for (let ingredient of step.ingredients) {
      if (!ingredient.ingredient) continue;

      let ingredientId = ingredient.ingredient;
      const details = productDetails[ingredientId];

      if (ingredient.amount && ingredient.unit && details.unitCost) {
        const ingredientCost = convertPerQuantityValue(details.unitCost,
          1, details.primaryUnit,
          ingredient.amount, ingredient.unit,
          Immutable.fromJS(details.measures));
        costPerPortion += ingredientCost;
      }
    }
  }

  if (!r.ingredient) costPerPortion /= r.recipe.recipeSize.amount;
  return costPerPortion;
}

@withRouter
@connect(
  state => ({
    recipes: state.get('recipes'),
    user: state.get('userInfo'),
    listUiState: state.getIn(['searchableList', 'recipes']),
    uiState: state.getIn(['recipeList']),
    hasLocations: (state.get('locations') || Immutable.List()).size > 0,
    newRecipeThresholdDays: state.getIn(['userInfo', 'newRecipeThresholdDays']),
  }),
  dispatch => {
    let actions = Immutable.Map(bindActionCreators(actionCreators, dispatch)).map(
      creator => creator.bind(this, 'recipes'),
    ).toObject();
    actions.modalActions = bindActionCreators(UIStateActions, dispatch);
    return actions;
  },
)
export default class RecipeList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayedItemsIds: Immutable.Set(),
      canSync: false,
      exportingData: false,
    };
    this.updateDisplayedItemsIds = displayedItemsIds => {
      if (!displayedItemsIds.equals(this.state.displayedItemsIds)) {
        this.setState({ displayedItemsIds });
      }
    };

    this.exportToCSV = this.exportToCSV.bind(this);
    this.startExportingToCSV = this.startExportingToCSV.bind(this);
    this.stopExportingToCSV = this.stopExportingToCSV.bind(this);
  }

  exportToCSV(option) {
    const { listUiState, recipes } = this.props;
    const checkedItems = listUiState && listUiState.get('checkedItems');
    const checkedRecipes = recipes.filter(r => checkedItems.contains(r.get('id')));
    const subRecipes = checkedRecipes.filter(r => r.get('ingredient')).map(r => r.get('id'));

    switch (option) {
      case 'goodeggs':
        getMeasures().then(() => {
          getMultipleProductsDetails(checkedRecipes.filter(r => !r.get('ingredient')).map(r => r.get('id')))
            .then(recipeDetails => {
              const sortedRecipes = sortRecipes(recipeDetails);
              const header = ['Recipe Name', 'Description', 'Size(oz)', 'Ingredients', 'COGS', 'Retail Price', 'Allergens'];
              const productIds = _(sortedRecipes)
                                    .flatMap(r => r.recipe.steps)
                                    .flatMap(s => s.ingredients)
                                    .map(i => i.ingredient)
                                    .concat(sortedRecipes.map(r => r.id))
                                    .uniq()
                                    .value();

              getMultipleProductsUsageInfo(productIds).then(productDetails => {
                const rows = sortedRecipes.map(r => {

                  let size = '';
                  _.each(r.recipe.steps, s => {
                    if (s.ingredients[0]) {
                      size = s.ingredients[0].amount;
                      return false;
                    }
                  });

                  const costPerRecipe = calculateCostPerPortion(r, productDetails);

                  return [
                    csvQuote(r.name),
                    csvQuote(r.description || ''),
                    size ? Number(size.toFixed(2)) : '',
                    csvQuote(productDetails[r.id].nutritionInfo.ingredientList.sort((a, b) => b.weight - a.weight).map(i => i.name).join(', ')),
                    costPerRecipe ? costPerRecipe.toFixed(2) : '',
                    r.recipe.price,
                    csvQuote(getRecipeAllergensString(Immutable.Map(productDetails[r.id].nutritionInfo.allergenList))),
                  ];
                });

                exportCSV('goodeggs_recipes', header, rows);
              });
            });
        });
        break;

      case 'recipes':
        getMeasures().then(() => {
          getMultipleProductsDetails(checkedRecipes.map(r => r.get('id')))
            .then(recipeDetails => {
              const sortedRecipes = sortRecipes(recipeDetails);
              const header = ['Recipe name', 'Sub-Recipe', 'Step number', 'Ingredient', 'Preparation', 'Quantity', 'UOM', 'Instructions'];
              const ingredientIds = _(sortedRecipes).flatMap(r => r.recipe.steps).flatMap(s => s.ingredients).map(i => i.ingredient).uniq().value();
              getMultipleProductsUsageInfo(ingredientIds).then(ingredientDetails => {
                const rows = _(sortedRecipes).map(r => r.recipe.steps.map((s, sIdx) => s.ingredients.map(i => [
                  csvQuote(r.name),
                  r.ingredient ? 'Y' : 'N',
                  sIdx + 1,
                  csvQuote(ingredientDetails[i.ingredient].name),
                  ingredientDetails[i.ingredient].isPrepOf ? csvQuote(ingredientDetails[i.ingredient].name) : '',
                  i.amount ? i.amount.toFixed(2) : 0,
                  i.unit ? csvQuote(unitDisplayName(i.unit)) : '',
                  sIdx === 0 ? (s.description ? csvQuote(s.description) : '') : '',
                ])))
                .flatMap(r => {
                  if (r.length > 0) r.push([[]]); // Add an empty row between recipes
                  return r;
                }).flatMap().value();
                exportCSV('parsley_recipes', header, rows);
              });
            });
        });
        break;

      case 'cost':
        getMeasures().then(() => {
          getMultipleProductsDetails(checkedRecipes.map(r => r.get('id')))
            .then(recipeDetails => {
              const sortedRecipes = sortRecipes(recipeDetails);
              const header = ['Recipe name', 'Sub-Recipe', 'Quantity', 'UOM', 'Ingredient', 'Cost', 'Percent'];
              const ingredientIds = _(sortedRecipes).flatMap(r => r.recipe.steps).flatMap(s => s.ingredients).map(i => i.ingredient).uniq().value();
              getMultipleProductsUsageInfo(ingredientIds).then(ingredientDetails => {
                const rows = _(sortedRecipes).map(r => {

                  const ingredientsCost = {};
                  let costPerRecipe = 0;
                  for (let step of r.recipe.steps) {
                    for (let ingredient of step.ingredients) {
                      if (!ingredient.ingredient) continue;

                      let ingredientId = ingredient.ingredient;
                      const details = ingredientDetails[ingredientId];

                      if (ingredient.amount && ingredient.unit && details.unitCost) {
                        let ingredientCost = convertPerQuantityValue(details.unitCost,
                          1, details.primaryUnit,
                          ingredient.amount, ingredient.unit,
                          Immutable.fromJS(details.measures));

                        if (!r.ingredient) ingredientCost /= r.recipe.recipeSize.amount;

                        costPerRecipe += ingredientCost;
                        ingredientsCost[ingredientId] = ingredientCost;
                      }
                    }
                  }

                  return r.recipe.steps.map(s => s.ingredients.map(i => {
                    const details = ingredientDetails[i.ingredient];
                    const cost = ingredientsCost[i.ingredient];
                    return [
                      csvQuote(r.name),
                      r.ingredient ? 'Y' : 'N',
                      i.amount ? i.amount.toFixed(2) : 0,
                      i.unit ? csvQuote(unitDisplayName(i.unit)) : '',
                      csvQuote(details.name),
                      cost && costPerRecipe ? `$${cost.toFixed(2)}` : '',
                      cost && costPerRecipe ? (cost / costPerRecipe * 100.0).toFixed(1) + '%' : '',
                    ];
                  }));
                })
                .flatMap(r => {
                  if (r.length > 0) r.push([[]]); // Add an empty row between recipes
                  return r;
                }).flatMap().value();
                exportCSV('parsley_recipes_cost_information', header, rows);
              });
            });
        });
        break;

      case 'nutrition':
        getMeasures().then(() => {
          getMultipleProductsUsageInfo(checkedRecipes.map(r => r.get('id'))).then(recipeDetails => {
            getMultipleProductsDetails(subRecipes.toJS()).then(subRecipesDetails => {
              _.each(recipeDetails, (val, key) => {
                if (subRecipes.contains(parseInt(key))) {
                  val.ingredient = true;
                  val.originalNutrientServingSize = {
                    amount: subRecipesDetails[key].nutrientServingAmount,
                    measure: subRecipesDetails[key].nutrientServingMeasure,
                    unit: subRecipesDetails[key].nutrientServingUnit,
                  };
                  recipeDetails[key] = val;
                }
              });
              const sortedRecipes = sortRecipes(recipeDetails);
              const header = [
                'Recipe name', 'Serving Size',
                'Calories', 'Total Fat', 'Saturated Fat', 'Trans Fat', 'Cholesterol', 'Sodium', 'Total Carbohydrate', 'Dietary Fiber', 'Total Sugars', 'Added Sugars', 'Protein', 'Vitamin D', 'Calcium', 'Iron', 'Potassium',
                '', 'Ingredients',
                '', 'Milk', 'Eggs', 'Fish', 'Crustacean or Shellfish', 'Tree Nuts', 'Wheat', 'Peanuts', 'Soybeans', 'Molluscs', 'Cereals containing Gluten', 'Celery', 'Mustard', 'Sesame Seeds', 'Sulphur Dioxide and Sulphites', 'Lupin',
                '', 'Added Sugar', 'Contains Meat', 'Contains Pork', 'Contains Corn', 'Contains Poultry', 'Contains Ingredients', 'None-Edible',
              ];
              const rows = _.map(sortedRecipes, r => {
                let nutrientValueMultiplier = 1;
                if (r.ingredient) {
                  nutrientValueMultiplier = divideQuantities(r.originalNutrientServingSize, {
                    amount: r.nutrientServingAmount,
                    measure: r.nutrientServingMeasure,
                    unit: r.nutrientServingUnit,
                  }, Immutable.fromJS(r.measures));
                } else {
                  nutrientValueMultiplier = r.dbNutrientServingAmount / r.nutritionInfo.recipeSize.amount;
                }
                return [
                  csvQuote(r.name),
                  csvQuote(r.ingredient ?
                    r.originalNutrientServingSize.amount + ' ' + unitDisplayName(r.originalNutrientServingSize.unit)
                  : r.dbNutrientServingAmount + ' Portions'
                  ),
                  ..._.map(nutrient_round_groups, (roundGroup, field) => roundNutritionAmount(roundGroup, r.nutritionInfo.nutritions[field] * nutrientValueMultiplier)),
                  '',
                  csvQuote(r.nutritionInfo.ingredientList.sort((a, b) => b.weight - a.weight).map(i => i.name).join(', ')),
                  '',
                  ..._.map(nutrition_allergens, info => r.nutritionInfo.allergenList[info.fieldName || info.name] != null ? 'T' : 'F'),
                  '',
                  ..._.map(nutrition_characteristics, info => r.nutritionInfo.characteristicList.indexOf(info.fieldName) !== -1 ? 'T' : 'F'),
                ];
              });
              exportCSV('parsley_recipes_nutritional_information', header, rows);
            });
          });
        });
        break;

      case 'cost_price_food_percent':
        getMeasures().then(() => {
          getMultipleProductsDetails(checkedRecipes.map(r => r.get('id')))
            .then(recipeDetails => {
              const sortedRecipes = sortRecipes(recipeDetails);
              const header = ['Name', 'Recipe cost', 'Price', 'Food Cost Percent'];
              const ingredientIds = _(sortedRecipes).flatMap(r => r.recipe.steps).flatMap(s => s.ingredients).map(i => i.ingredient).uniq().value();
              getMultipleProductsUsageInfo(ingredientIds).then(ingredientDetails => {
                const rows = _(sortedRecipes).map(r => {
                  let costPerPortion = calculateCostPerPortion(r, ingredientDetails);
                  return [
                    csvQuote(r.name),
                    costPerPortion ? costPerPortion.toFixed(2) : '',
                    r.recipe.price ? r.recipe.price.toFixed(2) : '',
                    costPerPortion && r.recipe.price ? (costPerPortion / r.recipe.price * 100).toFixed(2) + '%' : '',
                  ];
                })
                .value();
                exportCSV('parsley_recipes_cost_information', header, rows);
              });
            });
        });
        break;

      default:
        break;
    }
  }

  startExportingToCSV() {
    this.setState({
      exportingData: true,
    });
  }

  stopExportingToCSV() {
    this.setState({
      exportingData: false,
    });
  }

  render() {

    const {
      recipes,
      user, router,
      listUiState, uiState,
      modalActions: {
        openAddToCategoryModal,
        closeAddToCategoryModal,
        openRemoveFromCategoryModal,
        closeRemoveFromCategoryModal,
        openPrintMultipleRecipeInfoModal,
        closePrintMultipleRecipeInfoModal,
      },
      setCheckedItems,
      hasLocations,
      newRecipeThresholdDays,
    } = this.props;

    const checkedItems = listUiState && listUiState.get('checkedItems');

    let content;
    if (!recipes) {
      content = searchableListLoadingSpinner;
    } else if (user.get('isStdlib')) {
      // stdlib is redirected here on login, but shouldn't be allowed here
      router.replace('/ingredients');
    } else {

      const canSetSelectAll = checkedItems && checkedItems.intersect(this.state.displayedItemsIds).size > 0;
      const selectAllChecked = canSetSelectAll && this.state.displayedItemsIds.isSubset(checkedItems);
      const selectAllIndeterminate = canSetSelectAll && !this.state.displayedItemsIds.isSubset(checkedItems);

      const tagDateRecipe = (recipe) => {
        const createdAt = recipe.get('createdAt') && moment(recipe.get('createdAt'), moment.ISO_8601);
        const lastModified = recipe.get('lastModified') && moment(recipe.get('lastModified'), moment.ISO_8601);

        const newRecipeThreshold = moment.duration({ days: newRecipeThresholdDays });
        const isNew = createdAt && !isOlderThan(createdAt, newRecipeThreshold);
        const isUpdated = lastModified && !isOlderThan(lastModified, newRecipeThreshold);
        if (isNew)
          return <Badge>new</Badge>;
        else if (isUpdated)
          return <Badge>updated</Badge>;
      }

      content = <SearchableList
        noun="Recipe"
        searchKey="recipes"
        reduxStatePath="recipes"

        baseUrl={this.props.location.pathname}

        deleteItems={ids => SearchableList.deleteItems(ids, deleteProduct)}
        refreshItems={getRecipeList}
        checkable
        taggable tagTypes={['recipes']}
        copiable={user && user.get('permissionLevel') != 'operations'}
        deletable={user && user.get('permissionLevel') != 'operations'}
        hideAllRecipes={user.get('hideAllRecipes')}

        optionTagFunction={tagDateRecipe}
        groupingFunction={item => item.get('ingredient')}
        groupIDToHeader={ingredient => <h4>{ingredient
          ? 'Sub-Recipes (can be used as ingredients)'
          : 'Regular Recipes'
        }</h4>}

        getClassnames={item => item.get('ingredient') ? ['text-italic'] : []}
        shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}
        shouldClearSearchFilter={false}
        shouldShowNameOnSingleItemDeleting={true}
        updateDisplayedItemsIds={this.updateDisplayedItemsIds}
        readonly={user.get('isReadOnly')}
      >
        {!user.get('isReadOnly') ? [
          <Button
            key="select-all-button"
            onClick={() => {
              if (this.selectAllCheckbox) this.selectAllCheckbox.click();
            }}
            style={{ width: '121px', textAlign: 'left' }}>
            <Checkbox
              ref={checkbox => this.selectAllCheckbox = checkbox ? checkbox.input : null}
              inline
              checked={selectAllChecked}
              indeterminate={selectAllIndeterminate}
              onChange={ev => ev.target.checked ? setCheckedItems(this.state.displayedItemsIds) : setCheckedItems(Immutable.Set())}
              style={{ pointerEvents: 'none', marginBottom: '-2px' }}
            /> {selectAllChecked ? 'Deselect All' : 'Select All'}
          </Button>,
          <Button
            key="add-to-category-button"
            disabled={!(checkedItems && checkedItems.size)}
            onClick={() => openAddToCategoryModal(checkedItems)}>
            Add to Category
          </Button>,
          <Button
            key="remove-from-category-button"
            disabled={!(checkedItems && checkedItems.size)}
            onClick={() => openRemoveFromCategoryModal(checkedItems)}>
            Remove from Category
          </Button>,
        ] : null}
      </SearchableList>;
    }

    return <Grid fluid style={{ position: 'relative' }}>
      {featureIsSupported('MULTI_LOCATION') && hasLocations ?
        <Button
          id="recipe-list-sync"
          disabled={!this.state.canSync}
          onClick={() =>
            syncAllLocations().then(() => this.setState({ canSync: false }))
          }
        >
          {this.state.canSync ? 'Sync' : 'Synced'}
        </Button>
      : null}
      {user && !user.get('isReadOnly') ?
        <FeatureGuardedButton
          id="recipe-list-export"
          feature="RECIPE_EXPORT"
          download="parsley-users.csv"
          type="text/csv"
          onClick={() => this.startExportingToCSV()}
          disabled={!(checkedItems && checkedItems.size)}
        >
          Export to CSV
        </FeatureGuardedButton>
      : null}
      {user && !user.get('isReadOnly') && <Button
        id="recipe-list-print"
        key='show-print-recipes-info-modal-button'
        disabled={!(checkedItems && checkedItems.size)}
        onClick={() => openPrintMultipleRecipeInfoModal(checkedItems)}>
        Print Recipes
      </Button>}
      {content}
      <Onboarding pageKey="recipe_list" />
      {uiState.get('showAddToCategoryModal') ?
        <AddToCategoryModal
          hide={() => closeAddToCategoryModal()}
          onSubmitSuccess={() => setTimeout(getRecipeList, 10)}
          tagType="recipes"
          targetIds={uiState.get('addingToCategoryRecipes')}
        />
        : null}

      {uiState.get('showRemoveFromCategoryModal') ?
        <RemoveFromCategoryModal
          hide={() => closeRemoveFromCategoryModal()}
          onSubmitSuccess={() => setTimeout(getRecipeList, 10)}
          tagType="recipes"
          targetIds={uiState.get('removingFromCategoryRecipes')}
        />
        : null}

      { uiState.get('showPrintMultipleRecipeInfoModal') && <PrintMultipleRecipeInfoModal
        recipeIdList={uiState.get('printingRecipes')}
        formatOptions={recipePrintFormatOptions}
        hide={closePrintMultipleRecipeInfoModal}
        />
      }
      {this.state.exportingData ?
        <ExportRecipeDataModal
          hide={this.stopExportingToCSV}
          output={option => {
            this.exportToCSV(option);
            this.stopExportingToCSV();
          }}
          user={user}
        />
      : null}
    </Grid>;
  }

  componentWillMount() {
    getRecipeList();
    getLocationList();
    getOnboardingInfo();
    getCanSyncAllLocations().then(canSync => this.setState({ canSync }));
  }
}
