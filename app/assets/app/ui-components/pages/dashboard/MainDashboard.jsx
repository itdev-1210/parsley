// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import PurchaseHistoryDashboard from './PurchaseHistoryDashboard';
import RecipesDashboard from './RecipesDashboard';
import IngredientPriceHistoryDashboard from './IngredientPriceHistoryDashboard';

type Props = {}

export function MainDashboard(props: Props) {
  return <div>
    <Row>
      <Col sm={12}>
        {/** $FlowFixMe: can't simulate extra props passed in by decorators */}
        <RecipesDashboard/>
      </Col>
    </Row>
    <Row>
      <Col sm={12}>
        {/** $FlowFixMe: can't simulate extra props passed in by decorators */}
        <PurchaseHistoryDashboard/>
      </Col>
    </Row>
    <Row>
      <Col sm={12}>
        {/** $FlowFixMe: can't simulate extra props passed in by decorators */}
        <IngredientPriceHistoryDashboard/>
      </Col>
    </Row>
  </div>;

}