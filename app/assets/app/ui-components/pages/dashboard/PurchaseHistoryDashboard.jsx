// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { Row, Col } from 'react-bootstrap';
import { Card } from '../../Card';
import Chart from '../../Chart';
import Select from '../../Select';
import Validate from 'common/ui-components/Validate';
import { getSupplierList, getIngredientList, getTagsList,
  getPurchaseHistoryGraphData, getPurchaseList } from '../../../webapi/endpoints';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {
  checkFields, required, optional,
} from '../../../utils/validation-utils';
import {
  shallowToObject,
} from '../../../utils/form-utils';
import type { PurchaseHistoryPayload } from '../../utils/PropTypes';
import * as Chartist from 'chartist';
import formatNum from 'format-num';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import CardPlaceHolder from '../../CardPlaceHolder';

function validate(values) {
  const sourceValidators = {
    period: required(),
    purchaseType: required(),
    purchaseValue: values.purchaseType && values.purchaseType !== 'total' ? required() : optional(),
  };

  return checkFields(values, { ...sourceValidators });
}

type Props = {
  ingredients?: Immutable.List<Immutable.Map<string, any>>,
  suppliers?: Immutable.List<Immutable.Map<string, any>>,
  tags?: Immutable.List<Immutable.Map<string, any>>,
  purchaseOrders?: Immutable.List<Immutable.Map<string, any>>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

type PurchaseHistoryCardProps = {
  ingredients: Immutable.List<Immutable.Map<string, any>>,
  suppliers: Immutable.List<Immutable.Map<string, any>>,
  tags: Immutable.List<Immutable.Map<string, any>>,
  purchaseOrders: Immutable.List<Immutable.Map<string, any>>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
  fields: any,
  handleSubmit: Function,
  submitting: boolean,
}

type PurchaseHistoryCardState = {
  labels?: Array<string>,
  values?: Array<*>,
}

@connect(
  state => ({
    ingredients: state.get('ingredients'),
    suppliers: state.getIn(['suppliers']),
    tags: state.getIn(['tags', 'ingredients']),
    purchaseOrders: state.get('purchaseOrders'),
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
)


export default class PurchaseHistoryDashboard extends Component<Props> {
  render() {
    const { ingredients, suppliers, tags, purchaseOrders, onboardingWidgetsInfo } = this.props;
    return <div>
      <Row>
        <Col sm={12}>
          <Card
            title="Purchase History"
            content={
              ingredients && suppliers && tags && purchaseOrders ?
              // $FlowFixMe: can't simulate extra props passed in by decorators
              <PurchaseHistoryCard
                ingredients={ingredients}
                suppliers={suppliers}
                tags={tags}
                purchaseOrders={purchaseOrders}
                onboardingWidgetsInfo={onboardingWidgetsInfo}
              />
              : <LoadingSpinner/>
            }
          />
        </Col>
      </Row>
    </div>;
  }

  componentDidMount() {
    getSupplierList();
    getIngredientList();
    getTagsList('ingredients');
    getPurchaseList();
  }
}

@reduxForm(
  {
    form: 'purchase-history-dashboard',
    fields: [
      'period',
      'purchaseType',
      'purchaseValue',
    ],
    validate,
    touchOnBlur: false,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => (
    {
      initialValues: {
        period: 'month',
        purchaseType: 'total',
        purchasesValue: undefined,
      },
    }
  ),
)
class PurchaseHistoryCard extends Component<PurchaseHistoryCardProps, PurchaseHistoryCardState> {
  constructor(props: any) {
    super(props);
    this.state = {
      values: [],
      labels: [],
    };
    this.updateGraphData = this.updateGraphData.bind(this);
  }

  updateGraphData = (period: string, purchaseType: string, purchaseValue?: string) => {
    if (purchaseType === 'total' || purchaseValue) {
      let data: PurchaseHistoryPayload = {
        period,
        purchaseType,
      };
      if (purchaseValue)
        data.purchaseValue = purchaseValue;
      const that = this;
      getPurchaseHistoryGraphData(data)
        .then(({ values, labels }) => {
            that.setState({
              values: [values],
              labels,
            });
        });
    }
  }

  getSelectLabelOptions(type: string) {
    const { ingredients, suppliers, tags } = this.props;
    let options;
    let label;
    let getOptionsFromList = (list, filterFn = () => true) =>
      list
        .filter(filterFn)
        .map(s =>
            ({ value: s.get('id'), label: s.get('name') }),
        ).toJS();

    switch (type) {
      case 'Supplier':
        label = 'Supplier';
        options = suppliers ?
            getOptionsFromList(suppliers, s => !s.get('tombstone'))
            : [];
        break;
      case 'Category':
        label = 'Ingredient (GL) Category';
        options = tags ?
            getOptionsFromList(tags)
            : [];
        break;
      case 'Ingredient':
        label = 'Ingredient';
        options = ingredients ?
            getOptionsFromList(ingredients, i => !i.get('tombstone') && !i.get('isSubRecipe'))
            : [];
        break;
      default:
        return ['', []];
    }
    return [label, options];
  }

  render() {
    const { values, labels } = this.state;
    const { fields, purchaseOrders, onboardingWidgetsInfo } = this.props;
    const { period, purchaseType, purchaseValue } = fields;
    const [labelSelect, optionsSelect] = this.getSelectLabelOptions(purchaseType.value);
    const widgetName = 'purchase_history';
    const widgetInfo = onboardingWidgetsInfo.get('widgets', Immutable.List()).find(w => w.get('name') === widgetName);
    const isVisited = onboardingWidgetsInfo.get('visitedWidgetsKey', Immutable.List()).includes(widgetInfo.get('id'));


    if (purchaseOrders.size < 1 && !isVisited) {
      return <CardPlaceHolder
        text={widgetInfo.get('customMessage') || widgetInfo.get('defaultMessage')}
        widgetId={widgetInfo.get('id')}
      />;
    }

    return <div>
      <Row>
        <Col xs={12} md={3}>
          <h5>Period</h5>
          <Validate {...period}>
            <Select
              options={[
                {
                  label: 'By Day',
                  value: 'day',
                },
                {
                  label: 'By Week',
                  value: 'week',
                },
                {
                  label: 'By Month',
                  value: 'month',
                },
                {
                  label: 'Week-To-Date',
                  value: 'week-to-date',
                },
                {
                  label: 'Month-To-Date',
                  value: 'month-to-date',
                },
                {
                  label: 'Year-To-Date',
                  value: 'year-to-date',
                },
              ]}
              {...period}
              onChange={
                (value) => {
                  period.onChange(value);
                  this.updateGraphData(value, purchaseType.value, purchaseValue.value);
                }
              }
            />
          </Validate>
        </Col>
        <Col xs={12} md={3}>
          <h5>Purchases</h5>
          <Validate {...purchaseType}>
            <Select
              options={[
                {
                  label: 'Total',
                  value: 'total',
                },
                {
                  label: 'By Supplier',
                  value: 'Supplier',
                },
                {
                  label: 'By Ingredient (GL) Category',
                  value: 'Category',
                },
                {
                  label: 'By Individual Ingredient',
                  value: 'Ingredient',
                },
              ]}
              {...purchaseType}
              onChange={(value) => {
                purchaseType.onChange(value);
                purchaseValue.onChange(undefined);
                this.updateGraphData(period.value, value, undefined);
              }}
            />
          </Validate>
        </Col>
        <Col xs={12} md={3}>
          {
            (purchaseType.value && purchaseType.value !== 'total') &&
            <React.Fragment>
              <h5>{labelSelect}</h5>
              <Validate {...purchaseValue}>
                <Select
                  key={`${purchaseType.value}-key`}
                  options={optionsSelect}
                  {...purchaseValue}
                  onChange={(value) => {
                    purchaseValue.onChange(value);
                    this.updateGraphData(period.value, purchaseType.value, value);
                  }}
                />
              </Validate>
            </React.Fragment>
          }
        </Col>
      </Row>
      <Chart
        type="Line"
        labels={labels}
        series={values}
        options={{
          fullWidth: true,
          chartPadding: {
            right: 60,
            left: 40,
            bottom: 30,
          },
          axisY: {
            onlyInteger: true,
            labelInterpolationFnc: (value) => `$${formatNum(value)}`,
            width: 41,
            labelOffset: {
              x: -10,
              y: 0,
            },
          },
          plugins: [
            Chartist.plugins.ctAxisTitle({
              axisX: {
                axisTitle: 'Date',
                axisClass: 'ct-axis-title',
                offset: {
                  x: 0,
                  y: 50,
                },
                textAnchor: 'middle',
              },
              axisY: {
                axisTitle: 'Spend',
                axisClass: 'ct-axis-title',
                offset: {
                  x: 0,
                  y: 15,
                },
                textAnchor: 'middle',
                flipTitle: true,
              },
            }),
          ],
        }}
      />
    </div>
  }

  componentDidMount() {
    const { fields } = this.props;
    const formInfo: PurchaseHistoryPayload = shallowToObject(fields);
    getSupplierList();
    getIngredientList();
    getTagsList('ingredients');
    this.updateGraphData(formInfo.period, formInfo.purchaseType, formInfo.purchaseValue);
  }
}