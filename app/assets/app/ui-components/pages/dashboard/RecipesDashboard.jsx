// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { Card } from '../../Card';
import { connect } from 'react-redux';
import { getTotalRecipes, getTotalRecipesAddedChanged } from '../../../webapi/endpoints';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import RecipesDashboardModal from '../../modals/RecipesDashboardModal';
import CardPlaceHolder from '../../CardPlaceHolder';
import { actionCreators as UIStateActions } from '../../../reducers/uiState/recipesDashboard';
import { bindActionCreators } from 'redux';

type Props = {
  totalRecipes: Immutable.Map<*, *>,
  uiState: Immutable.Map<string, any>,
  totalRecipesAddedChanged: Immutable.Map<*, *>,
  newRecipeThresholdDays: number,
  openRecipesAddedChangedDetailsModal: Function,
  closeRecipesAddedChangedDetailsModal: Function,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

type TotalRecipesCardProps = {
  totalRecipes: Immutable.Map<*, *>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

type NewUpdatedRecipesProps = {
  totalRecipesAddedChanged: Immutable.Map<*, *>,
  totalRecipes: Immutable.Map<*, *>,
  newRecipeThresholdDays: number,
  openRecipesAddedChangedDetailsModal: Function,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

@connect(
  state => ({
    totalRecipes: state.get('totalRecipes'),
    newRecipeThresholdDays: state.getIn(['userInfo', 'newRecipeThresholdDays']),
    totalRecipesAddedChanged: state.get('totalRecipesAddedChanged'),
    uiState: state.getIn(['recipesDashboard']),
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
  dispatch => {
    let actions = bindActionCreators(UIStateActions, dispatch);
    return actions;
  },
)
export default class RecipesDashboard extends Component<Props> {

  render() {
    const {
      totalRecipes,
      uiState,
      totalRecipesAddedChanged,
      newRecipeThresholdDays,
      openRecipesAddedChangedDetailsModal,
      closeRecipesAddedChangedDetailsModal,
      onboardingWidgetsInfo,
    } = this.props;

    return <div>
      <Row>
        <Col md={6} sm={12}>
          {
            totalRecipes ?
            <TotalRecipesCard
              totalRecipes={totalRecipes}
              onboardingWidgetsInfo={onboardingWidgetsInfo}/>
            : <LoadingSpinner/>
          }
        </Col>
        <Col md={6} sm={12}>
          {
            totalRecipesAddedChanged && totalRecipes ?
            <NewUpdatedRecipesCard
              totalRecipesAddedChanged={totalRecipesAddedChanged}
              totalRecipes={totalRecipes}
              newRecipeThresholdDays={newRecipeThresholdDays}
              openRecipesAddedChangedDetailsModal={openRecipesAddedChangedDetailsModal}
              onboardingWidgetsInfo={onboardingWidgetsInfo}
            />
            : <LoadingSpinner/>
          }
        </Col>
      </Row>
      {uiState.get('showRecipesAddedChangedDetailsModal') ?
      <RecipesDashboardModal
        hide={() => closeRecipesAddedChangedDetailsModal()}
      />
      : null}
    </div>;
  }

  componentDidMount() {
    getTotalRecipes();
    getTotalRecipesAddedChanged();
  }
}

function TotalRecipesCard(props: TotalRecipesCardProps) {
  const { totalRecipes, onboardingWidgetsInfo } = props;
  const widgetName = 'num_recipes';
  const chefIcon = serverRoutes.controllers.Assets.versioned('images/chef_cloud_hat.png').url;
  // $FlowFixMe: only with records can we assert member existence/value
  const widgetInfo = onboardingWidgetsInfo.get('widgets').find(w => w.get('name') === widgetName);
  // $FlowFixMe: only with records can we assert member existence/value
  const isVisited = onboardingWidgetsInfo.get('visitedWidgetsKey').includes(widgetInfo.get('id'));
  return <div>
    <Card
      id="recipesInfo"
      title="Number of Recipes"
      icon={chefIcon}
      content={
        // $FlowFixMe: only with records can we assert member existence/value
        totalRecipes.get('total') > 0 || isVisited ?
        <div>
          <Row>
            <Col xs={4}>
              <div className="numbers">
                <h4>Total</h4>
                <h4 className="stats-value">{totalRecipes.get('total')}</h4>
              </div>
            </Col>
            <Col xs={4}>
              <div className="numbers">
                <h4>Top Level</h4>
                <h4 className="stats-value">{totalRecipes.get('topLevel')}</h4>
              </div>
            </Col>
            <Col xs={4}>
              <div className="numbers">
                <h4>Sub-Recipes</h4>
                <h4 className="stats-value">{totalRecipes.get('subRecipes')}</h4>
              </div>
            </Col>
          </Row>
          <div style={{ height: '29px' }}/>
        </div>
        : <CardPlaceHolder
            text={widgetInfo.get('customMessage') || widgetInfo.get('defaultMessage')}
            widgetId={widgetInfo.get('id')}
          />
      }
    />
  </div>;
}

function NewUpdatedRecipesCard(props: NewUpdatedRecipesProps) {
  const {
    newRecipeThresholdDays,
    totalRecipesAddedChanged,
    openRecipesAddedChangedDetailsModal,
    onboardingWidgetsInfo,
    totalRecipes } = props;

  const chefIcon = serverRoutes.controllers.Assets.versioned('images/chef_cloud_hat.png').url;
  const added = totalRecipesAddedChanged.get('added');
  const updated = totalRecipesAddedChanged.get('changed');
  const widgetName = 'new_update_recipes';
  // $FlowFixMe: only with records can we assert member existence/value
  const widgetInfo = onboardingWidgetsInfo.get('widgets').find(w => w.get('name') === widgetName);
  // $FlowFixMe: only with records can we assert member existence/value
  const isVisited = onboardingWidgetsInfo.get('visitedWidgetsKey').includes(widgetInfo.get('id'));

  return <div>
    <Card
      id="recipesInfo"
      title="New or Updated Recipes"
      description={newRecipeThresholdDays ? `in the past ${newRecipeThresholdDays} days` : ''}
      icon={chefIcon}
      content={
        // $FlowFixMe: only with records can we assert member existence/value
        totalRecipes.get('total') > 0 || isVisited ?
        <div>
          <Row>
            <Col xsOffset={2} xs={4}>
              <div className="numbers">
                <h4>New</h4>
                <h4 className="stats-value">{added}</h4>
              </div>
            </Col>
            <Col xs={4}>
              <div className="numbers">
                <h4>Updated</h4>
                <h4 className="stats-value">{updated}</h4>
              </div>
            </Col>
          </Row>
          <Row>
            <Col mdOffset={4} md={4} xs={12}>
            <Button
              bsStyle="default"
              className="green-btn"
              onClick={() => openRecipesAddedChangedDetailsModal()}>
              Details
            </Button>
            </Col>
          </Row>
        </div>
        : <CardPlaceHolder
            text={widgetInfo.get('customMessage') || widgetInfo.get('defaultMessage')}
            widgetId={widgetInfo.get('id')}
          />
      }
    />
  </div>;
}