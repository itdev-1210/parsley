// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import Immutable from 'immutable';
import moment from 'moment';
import { Row, Col } from 'react-bootstrap';
import { reduxForm } from 'redux-form';
import 'chartist-plugin-axistitle';
import * as Chartist from 'chartist';
import formatNum from 'format-num';
import { connect } from 'react-redux';
import { Card } from '../../Card';
import Chart from '../../Chart';
import Select from '../../Select';
import Validate from 'common/ui-components/Validate';
import { getIngredientList, getMeasures, getIngredientPriceHistory, getProductUsageInfo } from '../../../webapi/endpoints';
import {
  checkFields, required,
} from '../../../utils/validation-utils';
import { costingSize } from '../../../utils/packaging';
import { purchasingRound, unitDisplayName, convertPerQuantityValue, getCommonPurchaseUnits } from '../../../utils/unit-conversions';
import CardPlaceHolder from '../../CardPlaceHolder';
import { dateFormat } from 'common/utils/constants';

function validate(values) {
  const sourceValidators = {
    ingredientId: required(),
  };

  return checkFields(values, { ...sourceValidators });
}

type IngredientPriceHistoryDashboardProps = {
  ingredients: Immutable.List<Immutable.Map<string, any>>,
  measures: Immutable.Map<string, any>,
  purchaseOrders: Immutable.List<Immutable.Map<string, any>>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

@connect(
  state => ({
    ingredients: state.get('ingredients'),
    measures: state.getIn(['measures', 'measures']),
    purchaseOrders: state.get('purchaseOrders'),
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
)
export default class IngredientPriceHistoryDashboard extends Component<IngredientPriceHistoryDashboardProps> {

  render() {
    const { ingredients, measures, purchaseOrders, onboardingWidgetsInfo } = this.props;

    if (!ingredients || !measures) {
      return null;
    }

    const ingredientList = ingredients
        .filter(i => !i.get('tombstone') && !i.get('isSubRecipe') && i.get('isPriced'))
        .map(s =>
            ({ value: s.get('id'), label: s.get('name') }),
        ).toJS();

    return <div>
      <Row>
        <Col sm={12}>
          <Card
            title="Ingredient price history"
            content={
               /** $FlowFixMe: can't simulate extra props passed in by decorators */
              <IngredientPriceHistory
                allIngredients={ingredientList}
                purchaseOrders={purchaseOrders}
                onboardingWidgetsInfo={onboardingWidgetsInfo}
              />
            }
          />
        </Col>
      </Row>
    </div>;
  }

  componentDidMount() {
    getIngredientList();
    getMeasures();
  }
}

type Props = {
  allIngredients: Array<{label: string, value: number}>,
  fields: any,
  handleSubmit: Function,
  submitting: boolean,
  userVolumePref?: string,
  userWeightPref?: string,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
  purchaseOrders: Immutable.List<Immutable.Map<string, any>>,
}

type State = {
  values: Array<*>,
  productName: string,
  unitName: string,
  minDate: string,
  labelFormat: string,
  dateRange: Array<*>,
}

type Period = 'year' | 'month' | 'week';

@reduxForm(
  {
    form: 'ingredient-price-history-dashboard',
    fields: [
      'ingredientId',
      'period',
    ],
    validate,
    touchOnBlur: false,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => (
    {
      initialValues: {
        period: 'month',
      },
      measures: state.getIn(['measures', 'measures']),
      ingredients: state.get('ingredients'),
      userVolumePref: state.getIn(['userInfo', 'purchaseVolumeSystem']),
      userWeightPref: state.getIn(['userInfo', 'purchaseWeightSystem']),
      onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
    }
  ),
)
class IngredientPriceHistory extends Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.updateGraphData = this.updateGraphData.bind(this);
    this.getDateInfo = this.getDateInfo.bind(this);
    this.getDateRange = this.getDateRange.bind(this);
    this.state = {
      values: [],
      productName: '',
      unitName: '',
      minDate: '',
      labelFormat: '',
      dateRange: [],
    };
  }

  getDateInfo = (period: Period) => {
    switch (period) {
      case 'year':
        return [moment().subtract(1, 'years').format(), 'MMM, YYYY'];
      case 'week':
        return [moment().subtract(1, 'weeks').format(), dateFormat];
      default:
        return [moment().subtract(1, 'months').format(), dateFormat];
    }
  }

  getDateRange = (period: Period, startDate: string, endDate: string) => {
    let dates = [];
    let intervalPeriod;
    let interval;
    switch (period) {
      case 'year':
        interval = 1;
        intervalPeriod = 'months';
        break;
      case 'week':
        interval = 1;
        intervalPeriod = 'days';
        break;
      default:
        interval = 2;
        intervalPeriod = 'days';
        break;
    }
    let currDate = moment(startDate).startOf('day');
    let lastDate = moment(endDate).startOf('day');
    while (currDate.diff(lastDate) < 0) {
      dates.push(currDate.clone());
      currDate.add(interval, intervalPeriod);
    }
    dates.push(lastDate);
    return dates;
  }

  updateGraphData = (ingredientId: number, period: Period) => {
    const { userVolumePref, userWeightPref } = this.props;
    const [minDate, labelFormat] = this.getDateInfo(period);
    const dateRange = this.getDateRange(period, minDate, moment().format());
    const that = this;
    that.setState({
      productName: '',
      unitName: '',
    });
    Promise.all([
        getIngredientPriceHistory(ingredientId),
        getProductUsageInfo(ingredientId),
      ])
      .then(array => {
        const prices: Array<*> = array[0];
        const [pricesBeforeDate, pricesAfterDate] = _.partition(prices, (price) => moment(price.priceInfo.createAt) < moment(minDate));
        // $FlowFixMe: no fromJS() typing
        let filteredPrices: Immutable.List<Immutable.Map<string, *>> = Immutable.fromJS(pricesAfterDate);
        let firstPrice: Immutable.Map<string, *>;
        let lastPrice: Immutable.Map<string, *>;

        if (pricesBeforeDate.length)
          // $FlowFixMe: no fromJS() typing
          firstPrice = Immutable.fromJS(pricesBeforeDate).last();
        else
          firstPrice = Immutable.Map({ priceInfo: Immutable.Map({ withoutData: true, createAt: undefined }) });
        firstPrice = firstPrice.setIn(['priceInfo', 'createAt'], minDate);
        filteredPrices = filteredPrices.unshift(firstPrice);

        lastPrice = filteredPrices.last();
        lastPrice = lastPrice.setIn(['priceInfo', 'createAt'], moment().format());
        filteredPrices = filteredPrices.push(lastPrice);

        const productUsage = array[1];
        let userPurchaseSystem = 'metric';
        if (productUsage.primaryMeasure === 2) userPurchaseSystem = userWeightPref;
        if (productUsage.primaryMeasure === 3) userPurchaseSystem = userVolumePref;
        const preferredUnit = getCommonPurchaseUnits(productUsage.primaryMeasure, userPurchaseSystem === 'metric').first();
        const unitName = unitDisplayName(preferredUnit.get('id'), null, true);
        let values = [];

        filteredPrices.forEach((immutablePrice: Immutable.Map<string, any>) => {
          // $FlowFixMe: only with records can we assert member existence/value
          const priceInfo = (immutablePrice.get('priceInfo'): Immutable.Map<string, number>);
          const priceDate = immutablePrice.getIn(['priceInfo', 'createAt']);
          let costPerComparisonUnit;
          if (!priceInfo.get('withoutData')) {
            const conversionsForProduct = immutablePrice.get('measures', Immutable.List());
            const quantity = costingSize(priceInfo);
            costPerComparisonUnit = purchasingRound(convertPerQuantityValue(priceInfo.get('cost', 0),
              quantity.amount, quantity.unit,
              1, preferredUnit.get('id'),
              conversionsForProduct,
            ), 2, 0);
            values.push({ x: moment(priceDate, moment.ISO_8601).startOf('day'), y: costPerComparisonUnit });
          }
        });
        that.setState({
          values,
          unitName,
          productName: productUsage.name,
          minDate: moment(minDate).valueOf(),
          labelFormat,
          dateRange,
        });
      });
  }

  render() {
    const { values, unitName, productName, minDate, labelFormat, dateRange } = this.state;
    const { fields, allIngredients, onboardingWidgetsInfo, purchaseOrders } = this.props;
    const { ingredientId, period } = fields;
    const label = unitName ? `Price(${unitName})` : 'Price';

    const maxValue = _.maxBy(values, 'y');

    const widgetName = 'ingredient_price_history';
    const widgetInfo = onboardingWidgetsInfo.get('widgets', Immutable.List()).find(w => w.get('name') === widgetName);
    const isVisited = onboardingWidgetsInfo.get('visitedWidgetsKey', Immutable.Set()).includes(widgetInfo.get('id'));

    if (purchaseOrders && purchaseOrders.size < 1 && !isVisited && widgetInfo) {
      return <CardPlaceHolder
        text={widgetInfo.get('customMessage') || widgetInfo.get('defaultMessage')}
        widgetId={widgetInfo.get('id')}
      />;
    }

    return <div>
      <Row>
        <Col xs={12} md={3}>
          <h5>Ingredient</h5>
          <Validate {...ingredientId}>
            <Select
              options={allIngredients}
              {...ingredientId}
              onChange={
                (ingredientSelected) => {
                  ingredientId.onChange(ingredientSelected);
                  if (period.value) {
                    this.updateGraphData(ingredientSelected, period.value);
                  }
                }
              }
            />
          </Validate>
        </Col>
        <Col xs={12} md={3}>
          <h5>Period</h5>
          <Validate {...period}>
            <Select
              options={[
                {
                  label: 'Last Week',
                  value: 'week',
                },
                {
                  label: 'Last Month',
                  value: 'month',
                },
                {
                  label: 'Last Year',
                  value: 'year',
                },
              ]}
              {...period}
              onChange={
                (selectedPeriod) => {
                  period.onChange(selectedPeriod);
                  if (ingredientId.value) {
                    this.updateGraphData(ingredientId.value, selectedPeriod);
                  }
                }
              }
            />
          </Validate>
        </Col>
      </Row>
      <Chart
        type="Line"
        key={`product ${productName} -${unitName}-${period.value}`}
        series={[{ name: 'series-1', data: values }]}
        options={{
          fullWidth: true,
          lineSmooth: Chartist.Interpolation.step({
            postpone: true,
            fillHoles: false,
          }),
          showPoint: true,
          chartPadding: {
            right: 60,
            left: 40,
            bottom: 30,
            top: 30,
          },
          classNames: {
            label: 'ct-label ct-label-price-history',
          },
          axisX: {
            type: Chartist.FixedScaleAxis,
            low: moment(minDate).startOf('day').valueOf(),
            ticks: dateRange,
            labelInterpolationFnc: (value) => value ? moment(value).startOf('day').format(labelFormat) : '',
          },
          axisY: {
            onlyInteger: false,
            high: maxValue && maxValue.y * 1.1,
            low: 0,
            labelInterpolationFnc: (value) => `$${formatNum(value)} `,
            width: 41,
            labelOffset: {
              x: -10,
              y: 0,
            },
          },
          plugins: [
            Chartist.plugins.ctAxisTitle({
              axisX: {
                axisTitle: 'Date',
                axisClass: 'ct-axis-title',
                offset: {
                  x: 0,
                  y: 57,
                },
                textAnchor: 'middle',
              },
              axisY: {
                axisTitle: label,
                axisClass: 'ct-axis-title',
                offset: {
                  x: 0,
                  y: 15,
                },
                textAnchor: 'middle',
                flipTitle: true,
              },
            }),
          ],
        }}
      />
    </div>;
  }
}