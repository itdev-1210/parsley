// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import moment from 'moment';
import { Row, Col, Table } from 'react-bootstrap';
import 'chartist-plugin-axistitle';
import formatNum from 'format-num';
import { connect } from 'react-redux';
import DatePicker from 'react-bootstrap-date-picker';
import { Card } from '../../Card';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Radio from 'common/ui-components/Radio';
import { getIngredientList, getMeasures, getRecentPriceChanges } from '../../../webapi/endpoints';
import CardPlaceHolder from '../../CardPlaceHolder';

type IngredientPriceHistoryDashboardProps = {
  ingredients: Immutable.List<Immutable.Map<string, any>>,
  measures: Immutable.Map<string, any>,
  purchaseOrders: Immutable.List<Immutable.Map<string, any>>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

@connect(
  state => ({
    ingredients: state.get('ingredients'),
    measures: state.getIn(['measures', 'measures']),
    purchaseOrders: state.get('purchaseOrders'),
    onboardingWidgetsInfo: state.get('onboardingWidgetsInfo'),
  }),
)
export default class IngredientPriceChangesDashboard extends Component<IngredientPriceHistoryDashboardProps> {

  render() {
    const { ingredients, measures, purchaseOrders, onboardingWidgetsInfo } = this.props;

    if (!ingredients || !measures) {
      return null;
    }

    const ingredientList = ingredients
        .filter(i => !i.get('tombstone') && !i.get('isSubRecipe') && i.get('isPriced'))
        .map(s =>
            ({ value: s.get('id'), label: s.get('name') }),
        ).toJS();

    return <div>
      <Row>
        <Col xs={8}>
          <div>
            <Card
              title="Ingredient Price Changes"
              content={
                 /** $FlowFixMe: can't simulate extra props passed in by decorators */
                <RecentPriceChange
                  allIngredients={ingredientList}
                  purchaseOrders={purchaseOrders}
                  onboardingWidgetsInfo={onboardingWidgetsInfo}
                />
              }
            />
          </div>
        </Col>
      </Row>
    </div>;
  }

  componentDidMount() {
    getIngredientList();
    getMeasures();
  }
}

type RecentPriceChangeProps = {
  allIngredients: Array<{label: string, value: number}>,
  recentPriceChanges?: Immutable.Map<string, Immutable.List<*>>,
  purchaseOrders: Immutable.List<Immutable.Map<string, any>>,
  onboardingWidgetsInfo: Immutable.Map<string, any>,
}

type RecentPriceChangeState = {
  filterBy: 'biggestPriceChanges' | 'monthlySpend',
  date: string,
}

@connect(
  state => ({
    recentPriceChanges: state.get('recentPriceChanges'),
  }),
)
class RecentPriceChange extends Component<RecentPriceChangeProps, RecentPriceChangeState> {
  constructor(props) {
    super(props);
    this.state = {
      filterBy: 'biggestPriceChanges',
      date: moment().subtract(1, 'month').format(),
    };
    this.handleSelection = this.handleSelection.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
  }

  handleSelection = (filterBy) => {
    this.setState({ filterBy });
  }

  onChangeDate = (newDate) => {
    const date = moment(newDate).format();
    this.setState({ date });
    getRecentPriceChanges(date);
  }

  render() {
    const { recentPriceChanges, allIngredients, purchaseOrders, onboardingWidgetsInfo } = this.props;
    const { filterBy, date } = this.state;
    const widgetName = 'biggest_recent_price_changes';
    const widgetInfo = onboardingWidgetsInfo.get('widgets', Immutable.List()).find(w => w.get('name') === widgetName);
    const isVisited = onboardingWidgetsInfo.get('visitedWidgetsKey', Immutable.Set()).includes(widgetInfo.get('id'));

    if (purchaseOrders && purchaseOrders.size < 1 && !isVisited) {
      return <CardPlaceHolder
        text={widgetInfo.get('customMessage') || widgetInfo.get('defaultMessage')}
        widgetId={widgetInfo.get('id')}
      />;
    }

    return <div>
      <Row style={{ marginLeft: '3%' }}>
        <Col xs={12} md={9}>
          <Row>
            <Col xs={1}><label>From:</label></Col>
            <Col xs={4}>
              <DatePicker
                value={date}
                maxDate={moment().startOf('day').format()}
                onChange={this.onChangeDate}
                showClearButton={false}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <br/>
      <Row>
        <Col xs={4}>
          <Radio
            checked={filterBy === 'biggestPriceChanges'}
            onChange={() => this.handleSelection('biggestPriceChanges')}>
            Recent Price Changes
          </Radio>
        </Col>
        <Col xs={5}>
          <Radio
            checked={filterBy === 'monthlySpend'}
            onChange={() => this.handleSelection('monthlySpend')}>
            Effect on Monthly Spend
          </Radio>
        </Col>
      </Row>
      <Table striped className="recent-prices-table" style={{ marginBottom: '0px' }}>
        <thead>
        <tr>
          <th style={{ width: '70%' }}>Ingredient</th>
          <th>Percentage</th>
        </tr>
        </thead>
        <tbody>
          {
            recentPriceChanges && allIngredients ?
              recentPriceChanges.get(filterBy, Immutable.List()).sortBy(
                rec => rec.get('percentage'),
              ).reverse()
              .map((rec, index) => {
                const ingredientInfo = allIngredients.find(i => i.value === rec.get('productId'));
                const percentage = rec.get('percentage') * 100;
                if (ingredientInfo) {
                  return <tr className="separator-line" key={index}>
                    <td>{ingredientInfo.label}</td>
                    <td className="align-right-important">{formatNum(percentage, { maxFraction: 2 })}%</td>
                  </tr>;
                } else {
                  return null;
                }
              })
            : <tr><td colSpan={2}><LoadingSpinner /></td></tr>
          }
        </tbody>
      </Table>
    </div>;
  }

  componentDidMount() {
    getRecentPriceChanges(this.state.date);
  }
}