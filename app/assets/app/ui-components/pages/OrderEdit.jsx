import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import Immutable from 'immutable';
import _ from 'lodash';
import moment from 'moment';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';
import AddButton from '../AddButton';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import dragula from 'react-dragula';

import {
  Well, Col, Row, ControlLabel, FormControl,
  FormGroup, Button, Modal, Glyphicon,
} from 'react-bootstrap';

import Select from '../Select';
import QuantityInput from '../QuantityInput';
import PrintButton from '../PrintButton';
import MaskedInput from '../MaskedInput';
import DatePicker from 'react-bootstrap-date-picker';
import Checkbox from '../Checkbox';
import TextWithHelp from '../TextWithHelp';

import { withRouter } from 'react-router';
import { history } from '../../navigation';

import {
    getOrderDetails,
    saveOrder,
    saveMenu,
    deleteOrder,
    getMenuItemsList,
    getMultipleProductsDetails,
    getProductUsageInfo,
    getMultipleProductsUsageInfo,
    getMeasures,
    getMenuList,
    getMenuDetails,
    getProductionOrder,
    lookupMenuName,
    getPurchaseList,
    getOnboardingInfo,
} from '../../webapi/endpoints';

import { actionCreators as OrderActions } from '../../reducers/orderDetails';
import { actionCreators as MenuActions } from '../../reducers/menuDetails';
import { actionCreators as UIStateActions } from '../../reducers/uiState/orderEdit';
import {
  actionCreators as ProductionOrderActions,
  productionOrderFromState,
} from '../../reducers/productionOrder';
import { indexTypes } from '../../reducers/indexesCache';

import {
    purchasingRound,
    unitDisplayName,
    unitDisplayPrecision,
    unitDisplayQuantum,
    rescaleUnits,
    convert,
} from '../../utils/unit-conversions';
import { printContent } from '../../utils/printing';

import {
    checkFields, deepArrayField, deepObjectField,
    required, optional,
    nonnegative, isBlank,
    portions as portionsValidator,
    quantity as quantityValidator,
} from '../../utils/validation-utils';

import Validate from 'common/ui-components/Validate';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import TitleInput from '../TitleInput';
import CurrencyDisplay from '../CurrencyDisplay';
import ShoppingList from '../ShoppingList';
import CostBreakdown from '../modals/CostBreakdown';
import SendModal from '../modals/SendModal';
import PrintMenu from '../PrintMenu';
import PrintRecipeModal from '../modals/PrintRecipeModal';
import OrderPrintOptionsModal from '../modals/OrderPrintOptionsModal';
import {
  convertOrderJSONToFormValues, quantityFormFields, shallowToObject, convertOrderFormValuesToJSON,
  valueFromEvent,
} from '../../utils/form-utils';
import { isBeforeToday } from '../../utils/general-algorithms';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { dateFormat } from 'common/utils/constants';

function validate(values, props) {
  const itemsValidators = deepArrayField(deepObjectField({
    product: required(),
    orderedCost: optional(nonnegative),
  }));

  const orderValidators = {
    name: required(),
    hasForecasts: optional(),
    description: optional(),
    date: required(),
    items: itemsValidators,
    usedMenu: required(),
    sections: deepArrayField(deepObjectField({
      name: optional(),
      items: itemsValidators,
    })),
    subtractFromInventory: required(),
  };

  let errors = checkFields(values, orderValidators);

  // if product have amount field entered and have units (conversions)
  // then unit fields should be selected
  values.sections.forEach((section, section_idx) => {
    section.items.forEach((item, item_idx) => {
      if (!_.get(item, ['orderedQuantity', 'amount'])) {
        return;
      }
      const itemConversions = props.recipeUsageInfo.getIn([item.product, 'measures']);

      const validator = itemConversions && !itemConversions.isEmpty()
          ? quantityValidator
          : portionsValidator;


      _.merge(errors.sections[section_idx].items[item_idx],
          { orderedQuantity: validator(item.orderedQuantity) },
      );
    });
  });

  return errors;
}

let newOrderTemplate = Immutable.fromJS({
  usedMenu: 'custom_menu',
  date: moment().add(1, 'day').format(),
  sections: [
    {
      items: [
        {},
      ],
    },
  ],
  subtractFromInventory: true,
});

function matchItemsWithMenu(itemFields, menu, isCreatingNew) {
  const menuProducts = menu
      .get('sections')
      .flatMap(s => s.get('items'))
      .map(i => i.get('product'))
      .toSet();
  const productIndices = Immutable.Map(itemFields.map((item, i) => [item.product.value, i]));
  const itemFieldsProducts = productIndices.keySeq().toSet();
  const toAdd = menuProducts.subtract(itemFieldsProducts);

  let toRemove;
  let orderProducts;
  if (isCreatingNew) {
    toRemove = Immutable.Set();
  } else {
    orderProducts = itemFields.map(i => i.product.value);
    toRemove = Immutable.Set(orderProducts).subtract(menuProducts);
  }

  const removalIndices = toRemove.map(productId => productIndices.get(productId))
      .sort().toArray();

  // need to remove in reverse order, to ensure indices stay stable during removal
  removalIndices.reverse();
  for (let i of removalIndices) { // i is index *in itemFields*
    itemFields.removeField(i);
  }

  for (let productId of toAdd) {
    itemFields.addField({ product: productId });
  }
}

@withRouter
@reduxForm(
    {
      form: 'all',
      fields: [
        'name',
        'description',
        'date',
        'coversCount',

        'items[].product',
        ...quantityFormFields.map(f => `items[].orderedQuantity.${f}`),
        'items[].orderedCost',
        'items[].cashFlow',

        'sections[].name',
        'sections[].items[].product',
        ...quantityFormFields.map(f => `sections[].items[].orderedQuantity.${f}`),
        'sections[].items[].orderedCost',
        'sections[].items[].cashFlow',
        'usedMenu',
        'subtractFromInventory',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      onSubmitSuccess: () => history.push('/orders'),
      overwriteOnInitialValuesChange: false,
    },
    (state, ownProps) => {
      const menuItems = state.get('menuItems');
      const mainInfoLoaded = state.get('orderDetails') && menuItems;
      const isCopying = CrudEditor.isCopying(ownProps.params);

      let initialValues;
      if (CrudEditor.isCreatingNew(ownProps.params) && !isCopying) {
        initialValues = newOrderTemplate.toJS();
      } else if (mainInfoLoaded) {
        const isCopyingNewForecast = isCopying && !isBeforeToday(state.get('orderDetails').transaction.time);
        const orderDetailConvertedToFields = CrudEditor.initialValues(state.get('orderDetails'), ownProps.params, convertOrderJSONToFormValues);

        initialValues = isCopying ?
          isCopyingNewForecast ?
            { ...orderDetailConvertedToFields, date: moment(state.get('orderDetails').transaction.time).add(1, 'day').local().format() }
            : {
              ..._(orderDetailConvertedToFields).update('sections', ss =>
                // PARS-1870: When making a copy of a past order, if one of the recipes does not exist anymore, don't include it
                _(ss).map(s =>
                  _(s).update('items', items =>
                    _(items).filter(item => // filter items not listed in menu items i.e. not recipe anymore
                      menuItems.map(m => m.get('id')).includes(item.product),
                    ).value(),
                  ).value(),
                ).filter(s => // filter empty sections
                  !_.isEmpty(s.items),
                ).value(),
              ).value(),
              date: newOrderTemplate.get('date'),
            }
          : orderDetailConvertedToFields;
      }

      const logoUrl = state.getIn(['userInfo', 'logoUrl']);
      const uiState = state.get('orderEdit');
      const editOrderDisabled = !CrudEditor.isCopying(ownProps.params) && state.get('orderDetails') && isBeforeToday(state.get('orderDetails').transaction.time);
      return {
        myInitialValues: initialValues,
        initialValues,
        mainInfoLoaded,
        orderDetails: state.get('orderDetails'),
        purchaseOrders: state.get('purchaseOrders'),
        allMeasures: state.getIn(['measures', 'measures']),
        allUnits: state.getIn(['measures', 'units']),
        productDetails: state.get('productDetails'),
        detailedIngredients: state.get('detailedIngredients'),
        productionOrder: uiState.get('printRecipesOrder')
          && productionOrderFromState(state, uiState.get('printRecipesOrder'), true),
        menuItems,
        menuDetails: state.get('menuDetails'),
        recipeUsageInfo: state.get('detailedIngredients'),
        onboardingInfo: state.get('onboardingInfo'),
        editOrderDisabled,
        deleteDisabled: editOrderDisabled,
        showCustomComponent: Boolean(state.getIn(['form']) && state.getIn(['form']).all && state.getIn(['form']).all.usedMenu
            && (state.getIn(['form']).all.usedMenu.value === 'custom_menu')),
        shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
        uiState,
        companyLogoUrl: state.getIn(['userInfo', 'companyLogo']) ?
              logoUrl + state.getIn(['userInfo', 'companyLogo'])
              : null,
      };
    },
    dispatch => ({
      orderActions: bindActionCreators(OrderActions, dispatch),
      menuActions: bindActionCreators(MenuActions, dispatch),
      productionOrderActions: bindActionCreators(ProductionOrderActions, dispatch),
      uiStateActions: bindActionCreators(UIStateActions, dispatch),
    }),
)
export default class OrderEdit extends CrudEditor {
  updateLastValues(nextProps) {
    for (const field of ['name', 'description', 'date', 'coversCount', 'usedMenu', 'subtractFromInventory'])
      this.lastValues[field] = {
        value: nextProps.fields[field].value,
        error: nextProps.fields[field].error,
        touched: nextProps.fields[field].touched,
        valid: nextProps.fields[field].valid,
      };
    this.lastValues.sections = nextProps.fields.sections.map(s => ({
      name: s.name.value,
      items: s.items.map(i => ({
          product: i.product ? i.product.value : undefined,
          orderedCost: i.orderedCost ? i.orderedCost.value : undefined,
          cashFlow: i.cashFlow ? i.cashFlow.value : undefined,
          orderedQuantity: i.orderedQuantity ? {
            amount: i.orderedQuantity.amount ? i.orderedQuantity.amount.value : undefined,
            measure: i.orderedQuantity.measure ? i.orderedQuantity.measure.value : undefined,
            unitCost: i.orderedQuantity.unitCost ? i.orderedQuantity.unitCost.value : undefined,
          } : undefined,
      })),
    }));
  }

  shouldComponentUpdate(nextProps, nextState) {
    // Compare this.props with nextProps, prevent the OrderEdit component from reRender() when no change in props.
    // This make it's subComponent (e.g. EditSupplierEmail) performance faster because the redux-form forces the ancestors
    // component to reRender().
    if (!this.lastValues) {
      this.lastValues = nextProps.myInitialValues || [];
      this.updateLastValues(nextProps);
      return true;
    }
    // compare initialValues
    for (const field of ['name', 'description', 'date', 'coversCount', 'usedMenu', 'subtractFromInventory'])
    for (const property of ['value', 'valid', 'error', 'touched']) {
      if (this.lastValues[field][property] !== nextProps.fields[field][property]) {
        this.updateLastValues(nextProps);
        return true;
      }
    }

    if ((this.lastValues.sections || []).length !== nextProps.fields.sections.length) {
      this.updateLastValues(nextProps);
      return true;
    }

    for (let sectionIdx = 0; sectionIdx < nextProps.fields.sections.length; sectionIdx++) {
      if (this.lastValues.sections[sectionIdx].name !== nextProps.fields.sections[sectionIdx].name.value) {
        this.lastValues.sections[sectionIdx].name = nextProps.fields.sections[sectionIdx].name.value;
        return true;
      }
      if (this.lastValues.sections[sectionIdx].items.length !== nextProps.fields.sections[sectionIdx].items.length) {
        this.updateLastValues(nextProps);
        return true;
      }
      for (let itemIdx = 0; itemIdx < this.lastValues.sections[sectionIdx].items.length; itemIdx++)
        for (const field of ['product', 'orderedCost', 'cashFlow'])
          if (this.lastValues.sections[sectionIdx].items[itemIdx] !== undefined && nextProps.fields.sections[sectionIdx].items[itemIdx][field] !== undefined) {
            if (this.lastValues.sections[sectionIdx].items[itemIdx][field] !== nextProps.fields.sections[sectionIdx].items[itemIdx][field].value) {
              this.lastValues.sections[sectionIdx].items[itemIdx][field] = nextProps.fields.sections[sectionIdx].items[itemIdx][field].value;
              return true;
            }
            for ( const f of quantityFormFields) {
              if (this.lastValues.sections[sectionIdx].items[itemIdx].orderedQuantity[f] !== nextProps.fields.sections[sectionIdx].items[itemIdx].orderedQuantity[f].value) {
                this.lastValues.sections[sectionIdx].items[itemIdx].orderedQuantity[f] = nextProps.fields.sections[sectionIdx].items[itemIdx].orderedQuantity[f].value;
                return true;
              }
            }
          } else if (this.lastValues.sections[sectionIdx].items[itemIdx] !== undefined || nextProps.fields.sections[sectionIdx].items[itemIdx][field] != undefined) {
            return true;
          }
    }

    if (nextProps.menuDetails) {
      if (!this.lastValues.menuDetailsId || this.lastValues.menuDetailsId !== nextProps.menuDetails.id) {
        this.lastValues.menuDetailsId = nextProps.menuDetails.id;
        return true;
      }
    }

    // compare other props' and state's properties
    return this.props.mainInfoLoaded !== nextProps.mainInfoLoaded
        || this.props.productDetails.size !== nextProps.productDetails.size
        || this.props.detailedIngredients.size !== nextProps.detailedIngredients.size
        || (this.props.productionOrder && !this.props.productionOrder.equals(nextProps.productionOrder))
        || this.props.recipeUsageInfo.size !== nextProps.recipeUsageInfo.size
        || !this.props.uiState.equals(nextProps.uiState)
        || this.state.alreadyAddedSection !== nextState.alreadyAddedSection
        || !this.state.shoppingEmailStatus.equals(nextState.shoppingEmailStatus)
        || this.state.lastActiveSupplier !== nextState.lastActiveSupplier
        || this.state.editingDisabled !== nextState.editingDisabled
        || this.state.statusMsgKey !== nextState.statusMsgKey
        || this.state.isOrderDirty !== nextState.isOrderDirty
        || this.state.step !== nextState.step;
  }

  constructor(props, context) {
    super(props, context);

    this.orderSectionsDrake = dragula({
      direction: 'vertical',
      moves: (el, container, handle) => handle.classList.contains('drag-order-section'),
    });
    this.orderSectionsDrake.on('drop', this.reorderSections.bind(this));

    this.orderItemsDrake = dragula({
      direction: 'vertical',
      moves: (el, container, handle) => handle.classList.contains('drag-order-section-item'),
    });
    this.orderItemsDrake.on('drop', this.reorderSectionItems.bind(this));

    this.state = {
      alreadyAddedSection: false,
      shoppingEmailStatus: Immutable.Map(),
      lastActiveSupplier: -1,
      step: 'editor',
      editingDisabled: false,
      statusMsgKey: '',
      isOrderDirty: false, // helpful when fields (items/sections) are dragged/removed or new men loaded
    };
  }

  reorderSectionItems(el, target, source, sibling) {

    function getOrdinalFromId(element) {
      if (!element || !element.id) {
        return null;
      }

      return Number(element.id.split('-').pop());
    }

    function getItemObj(item) {
      return {
        product: item.product.value,
        orderedQuantity: shallowToObject(item.orderedQuantity),
        orderedCost: item.orderedCost.value,
      };
    }

    /**
     * NOTE:
     * Refer reorderIngredients of RecipeEdit
     */

    const sourceItemIndex = getOrdinalFromId(el);
    const targetSectionIndex = getOrdinalFromId(target);
    const sourceSectionIndex = getOrdinalFromId(source);
    const siblingItemIndex = getOrdinalFromId(sibling);

    target.removeChild(el);
    const orgSiblingId = `order-section-item-${sourceSectionIndex}-${sourceItemIndex + 1}`;
    const orgSibling = document.getElementById(orgSiblingId);
    if (orgSibling) {
      source.insertBefore(el, orgSibling);
    } else {
      source.appendChild(el);
    }

    const { sections } = this.props.fields;

    const item = getItemObj(sections[sourceSectionIndex].items[sourceItemIndex]);

    if (sourceSectionIndex != targetSectionIndex) {
      if (_.isNull(siblingItemIndex)) {
        sections[targetSectionIndex].items.addField(item);
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      } else {
        sections[targetSectionIndex].items.addField(item, siblingItemIndex);
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      }
    } else if (_.isNull(siblingItemIndex)) {
        sections[sourceSectionIndex].items.addField();
        sections[sourceSectionIndex].items.swapFields(sourceItemIndex, sections[sourceSectionIndex].items.length - 1);
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      } else {

        sections[sourceSectionIndex].items.addField(item, siblingItemIndex);
        const movingUp = siblingItemIndex < sourceItemIndex;

        if (movingUp) {
          sections[sourceSectionIndex].items.removeField(sourceItemIndex + 1);
        } else {
          sections[sourceSectionIndex].items.removeField(sourceItemIndex);
        }
      }

    this.setState({ isOrderDirty: true });
  }

  reorderSections(el, target, source, sibling) {
    function getOrdinalFromId(element) {
      if (!element || !element.id) {
        return null;
      }

      return Number(element.id.split('-').pop());
    }

    /**
     * NOTE:
     * By the time this method is called orders are already swapped.
     * However, our react props are still same.
     *
     * So record the changes, put element back to original place
     * and then swap props
     */

    // record changes
    const sourceIndex = getOrdinalFromId(el);
    const siblingIndex = getOrdinalFromId(sibling);

    // put element back
    // 1. remove moved element
    // 2. & 3. find original sibling of source ingredient
    // 4. if original source ingredient found then insert before it
    //      else it must be last ingredient so make it last ingredient
    target.removeChild(el);
    const orgSourceSiblingId = `order-section-${sourceIndex + 1}`;
    const orgSourceSibling = document.getElementById(orgSourceSiblingId);
    if (orgSourceSibling) {
      source.insertBefore(el, orgSourceSibling);
    } else {
      source.appendChild(el);
    }

    // swap props begins:

    // somehow fields are not changing its values so using full reference path
    // var { fields } = this.props;

    if (_.isNull(siblingIndex)) { // order is moved to last

      // 1. add empty order at the end
      // 2. swap empty ingredient and source ingredient
      // 3. remove source ingredient
      this.props.fields.sections.addField();
      this.props.fields.sections.swapFields(sourceIndex, this.props.fields.sections.length - 1);
      this.props.fields.sections.removeField(sourceIndex);
    } else {

      /**
       * NOTE: We have three methods available to play
       * with fields: addField, removeField and swapFields.
       *
       * Directly swapping orders will not work.
       * And its tedious to make plain JS object from props.sections
       * and add it before sibling.
       *
       * So, instead, we add empty section before sibling,
       * then swap source and newly added empty section
       * and then remove swapped empty section.
       */

      // add empty section before sibling
      this.props.fields.sections.addField({}, siblingIndex);

      // swap and then remove swapped empty section
      if (siblingIndex < sourceIndex) { // moving up
        // when moving up, source section will also shift down when empty section is added
        this.props.fields.sections.swapFields(siblingIndex, sourceIndex + 1);
        this.props.fields.sections.removeField(sourceIndex + 1);
      } else {
        this.props.fields.sections.swapFields(siblingIndex, sourceIndex);
        this.props.fields.sections.removeField(sourceIndex);
      }
    }

    this.setState({ isOrderDirty: true });
  }

  orderSectionsDraggable() {
    this.orderSectionsDrake.containers = [];
    this.orderSectionsDrake.containers.push(document.getElementById('custom-orders-container'));
  }

  getMenuSections = () => (this.props.fields.sections || Immutable.Map())
    // filter if no section name and dish selected
    .filter(s => s.name.value || !_.isEmpty(s.items.map(i => i.product.value).filter(_.isNumber)))
    .map(section => {
      const newSection = { name: section.name.value };
      newSection.items = section.items
        // filter if no dish selected
        .filter(item => _.isNumber(item.product.value))
        .map(item => ({ product: item.product.value }));
      return newSection;
    });

  // checks if dishes are changed in selected menu
  isMenuDirty = () => {
    // not dirty if menu details not populated
    if (!this.props.menuDetails) return false;
    // compare menu details and section items
    return !_.isEqual(
      _.get(this.props.menuDetails, ['sections'], []),
      this.getMenuSections(),
    );
  };

  handleSave = (id, deets) => saveOrder(id, convertOrderFormValuesToJSON(deets));

  handleMenu = (id, menuDeets) => saveMenu(id, menuDeets).then(
    menuId => getMenuList().then(this.getMenuAndProductDetails(menuId, () => this.setState({ step: 'editor', statusMsgKey: 'menuSaved' }))),
    e => {
      if (e.reduxFormErrors) {
        return Promise.reject(e.reduxFormErrors);
      } else {
        console.log(e.toString());
        console.log(e.stack);
        return Promise.reject({ _error: e.message });
      }
    },
  );

  isItemSelected() {
    if (!this.props.showCustomComponent) return true; // predefined menus are always valid

    // check if any sections have one item selected
    return this.props.fields.sections.some(section =>
      section.items.some(item =>
          _.isNumber(item.product.value),
      ),
    );
  }

  makeExtraButtons() {

    return this.props.editOrderDisabled ? [] : [
      <PrintButton key="order-print" onClick={this.printOrder} disabled={!this.isItemSelected()} noun="print" />,
      <Button key="purchasing" onClick={() => this.showShoppingList(true)} disabled={!this.isItemSelected()}>Purchase Orders</Button>,
    ];
  }

  getBaseOrder() {

    const prepareItem = obj => ({
      product: obj.product.value,
      // make sure to drop blank unit/measure fields for un-measured recipes
      quantity: _.omitBy(shallowToObject(obj.orderedQuantity), isBlank),
    });

    const { sections } = this.props.fields;

    const baseOrderFields = _(sections)
        .filter(s => !s.length)
        .flatMap(s =>
            s.items
            // truthiness is what we want - not "", 0, or null
                .filter(i => i.product.value && _.get(i, ['orderedQuantity', 'amount', 'value'])),
        ).value();

    const erroringFields = _(baseOrderFields)
        .flatMap(fields => _.values(fields.orderedQuantity))
        .filter(field => field.error)
        .value();

    if (erroringFields.length > 0) {
      erroringFields.forEach(field => this.props.touch(field.name));
      return null;
    }

    return Immutable.fromJS(baseOrderFields.map(prepareItem));
  }

  showShoppingList = (allowResendEmail) => {

    if (allowResendEmail == true) {
      this.setState({
        lastActiveSupplier: -1,
        shoppingEmailStatus: Immutable.Map(),
      });
    }
    const baseOrder = this.getBaseOrder();

    if (baseOrder) {
      getProductionOrder(baseOrder, true);
      this.props.uiStateActions.openShoppingListModal(baseOrder);
    }
  };

  printOrder = () => {
    const baseOrder = this.getBaseOrder();
    if (baseOrder) {
      this.props.uiStateActions.openPrintOptionModal(baseOrder);
    }
  }

  printPrep = () => {
    const baseOrder = this.getBaseOrder();
    if (baseOrder) {
      this.props.uiStateActions.printPrep(baseOrder);
    }
  };

  printSubRecipes = () => {
    const baseOrder = this.getBaseOrder();
    if (baseOrder) {
      this.props.uiStateActions.printSubRecipes(baseOrder);
    }
  };

  printRecipes = () => {
    const baseOrder = this.getBaseOrder();
    if (baseOrder) {
      this.props.uiStateActions.printRecipes(baseOrder);
    }
  };

  calculateItemCost = (item, recipeUsageInfo) => {
    const conversions = recipeUsageInfo.getIn([item.product.value, 'measures']);
    const unitCost = recipeUsageInfo.getIn([item.product.value, 'unitCost'], 0);
    const primaryUnit = recipeUsageInfo.getIn([item.product.value, 'primaryUnit']);

    if (!unitCost) {
      return 0;
    }

    const orderedQuantity = item.orderedQuantity;
    if (conversions && conversions.isEmpty()) {
      return _.isNumber(orderedQuantity.amount.value) ? purchasingRound(orderedQuantity.amount.value * unitCost, 2) : 0;
    }

    const _orderedQuantity = shallowToObject(orderedQuantity);
    return _orderedQuantity.amount && _orderedQuantity.unit ?
      purchasingRound(
        convert(
          _orderedQuantity.amount, _orderedQuantity.unit, primaryUnit, conversions,
        ) * unitCost, 2,
      ) : 0;
  };

  getTotalCost = (sections, recipeUsageInfo) => {
    if (!sections && !sections.items && recipeUsageInfo) return null;
    // Calculate the total cost of all sections listed in the menu
    return sections
      .map(section =>
        section.items
          .map(item => this.calculateItemCost(item, recipeUsageInfo))
          .reduce((a, b) => a + b, 0),
      )
      .reduce((a, b) => a + b, 0);
  };

  getTotalCostHavingPriceAssigned = (sections, recipeUsageInfo) => {
    if (!sections && !sections.items && !recipeUsageInfo) return null;
    return sections.map(section =>
      section.items
        // select item only if price is present
        .filter(item =>
          Boolean(recipeUsageInfo.getIn([item.product.value, 'unitCost']))
          && Boolean(recipeUsageInfo.getIn([item.product.value, 'portionPrice'])),
        )
        .map(item => this.calculateItemCost(item, recipeUsageInfo))
        .reduce((a, b) => a + b, 0),
    ).reduce((a, b) => a + b, 0);
  };

  // add price of each item in a section and then add all sections price
  getTotalPriceHavingPriceAssigned = (sections, recipeUsageInfo) => {
    const { editOrderDisabled } = this.props;
    if (!sections && !sections.items && !recipeUsageInfo) return null;

    return sections
      .map((section) =>
        section.items
          .filter(item => Boolean(recipeUsageInfo.getIn([item.product.value, 'portionPrice'])))
          .map(item => {
            if (editOrderDisabled)
              return purchasingRound(item.cashFlow.value, 0);
            else {
              const portionPrice = recipeUsageInfo.getIn([item.product.value, 'portionPrice']);
              const portion = item.orderedQuantity.amount.value;
              return purchasingRound(portion * portionPrice, 0);
            }
          })
          .reduce((a, b) => a + b, 0),
      )
      .reduce((a, b) => a + b, 0);
  };

  getTotalCostPercent = (sections, recipeUsageInfo) => {
    if (!sections && !sections.items && !recipeUsageInfo) return null;

    return purchasingRound(
        this.getTotalCostHavingPriceAssigned(sections, recipeUsageInfo) / this.getTotalPriceHavingPriceAssigned(sections, recipeUsageInfo) * 100.0, 1,
    );
  };

  costsDistribution = (sections, recipeUsageInfo) => ({
    totalCost: this.getTotalCost(sections, recipeUsageInfo),
    totalPrice: this.getTotalPriceHavingPriceAssigned(sections, recipeUsageInfo),
    foodCostPercent: this.getTotalCostPercent(sections, recipeUsageInfo),
  });

  renderCustomOrder = () => {

    const { sections, coversCount } = this.props.fields;
    const { allMeasures, recipeUsageInfo, editOrderDisabled, menuItems } = this.props;

    const { totalCost, totalPrice, foodCostPercent } = this.costsDistribution(sections, recipeUsageInfo);

    const allUsedItems = Immutable.Set(_(sections)
        .flatMap(s => s.items)
        .filter(_.identity) // empty string or null or undefined is what new fields are initialized to
        .map(item => item.product.value)
        .value());

    return <div>
      <FieldArray component={OrderSections} fields={sections} props={{
        menuItems,
        allMeasures, allUsedItems,
        editOrderDisabled,
        alreadyAddedSection: this.state.alreadyAddedSection,
        calculateItemCost: this.calculateItemCost,
        orderItemsDrake: this.orderItemsDrake,
      }} />
      <div className={editOrderDisabled ? 'order-footer order-footer-disabled' : 'order-footer'}>
        {
          // TODO: rework for redux-form upgrade (using our FieldArray shim here had weird focus/click issues)
          editOrderDisabled ? null :
            <AddButton
              noun="section"
              className="order-footer-add"
              onClick={() => {
                sections.addField();
                this.setState({ alreadyAddedSection: true });
              }}
            />
        }
        <div className="order-footer-content">
          <div className="order-footer-items-content">
            <div className="order-footer-total">
              {sections.length ? 'Total:' : null}
            </div>
            <div className="order-footer-cost">
              { totalCost
                  ? <CurrencyDisplay value={totalCost} />
                  : sections.length ? '$0.00' : null
              }
            </div>
            <div className="order-footer-price">
              { totalPrice
                ? <CurrencyDisplay value={totalPrice} />
                : null
              }
            </div>
            <div className="order-footer-food-cost-percent">
              Food Cost Percent:&nbsp;
                {(sections.length && Number(foodCostPercent)) > 0 ? `${foodCostPercent}%` : null}
            </div>
          </div>
        </div>

        <div className="order-footer-content mt-2 covers-content">
          <div className="order-footer-items-content">
            <div className="order-footer-total">
              Number of Covers (Diners):
            </div>
            <div className="order-footer-price">
              {
                editOrderDisabled
                  ? coversCount.value
                  : <Validate {...coversCount}>
                      <FormControl
                        type="number"
                        {...coversCount}
                        onChange={evt => {
                          const newVal = parseInt(valueFromEvent(evt));
                          coversCount.onChange(isNaN(newVal) ? null : newVal);
                        }}
                      />
                    </Validate>
              }
            </div>
            <div className="order-footer-food-cost-percent">
              Cost Per Cover:&nbsp;
              {totalCost > 0 && coversCount.value > 0 && <CurrencyDisplay value={totalCost / coversCount.value} />}
            </div>
          </div>
        </div>

      </div>
    </div>;
  };

  showCostModal = () => {
    const baseOrder = this.getBaseOrder();

    if (baseOrder) {
      this.props.uiStateActions.openCostModal(baseOrder);
    }

  };

  copyMenuDetailsToFieldValues = (initialize = false) => {
    const { menuDetails, fields: { sections } } = this.props;

    // save previous items
    const orderedItems = _.flatMap(sections, s => s.items).map(item => ({
      product: item.product.value,
      orderedQuantity: {
        amount: item.orderedQuantity.amount.value,
        measure: item.orderedQuantity.measure.value,
        unit: item.orderedQuantity.unit.value,
      },
    }));

    // clear fields
    _.forEach(sections, s => sections.removeField());

    // copy sections from menu to field
    // and also populate ordered quantiy if item was selected
    for (const s of menuDetails.sections) {
      let fieldValues = _.omit(s, 'items');
      fieldValues.items = s.items.map(i => {
        const quantityFields = orderedItems.find(
            itemField => itemField.product === i.product,
        );
        return Object.assign({},
          i,
          { orderedQuantity: initialize && !quantityFields ? { amount: 0 } : quantityFields && quantityFields.orderedQuantity },
        );
      });
      sections.addField(fieldValues);
    }

    this.orderSectionsDraggable();
  };

  renderBody() {

    const {
      name, description, date,
      sections, subtractFromInventory,
    } = this.props.fields;

    const {
        uiStateActions,

        uiState, menuDetails, allMeasures, allUnits, editOrderDisabled, purchaseOrders,
        productionOrder, productDetails, detailedIngredients,
        companyLogoUrl,
        recipeUsageInfo,
    } = this.props;

    let stepContent;
    switch (this.state.step) {
      case 'confirm':
        stepContent = <ConfirmMenuModal
          hide={this.cancelSave}
          switchToReplaceMenu={() => {
            this.setState({ step: 'prompt-replace' });
          }}
          switchToEditName={() => {
            this.setState({ step: 'prompt-name' });
          }}
        />;
        break;
      case 'prompt-replace':
        stepContent = <ConfirmReplaceMenu
          hide={this.cancelSave}
          menuName={menuDetails.name}
          replaceExisting={() => {
            const menuDeets = {
              name: menuDetails.name,
              sections: this.getMenuSections(),
            };
            return this.handleMenu(menuDetails.id, menuDeets);
          }}
        />;
        break;
      case 'prompt-name':
        stepContent = <MenuNameEdit hide={this.cancelSave} handleComplete={(values) => {
          const menuDeets = {
            name: values.name,
            sections: this.getMenuSections(),
          };
          return this.handleMenu('new', menuDeets);
        }}/>;
        break;
      default:
        break;
    }

    let statusMessage;
    switch (this.state.statusMsgKey) {
      case 'menuLoaded':
        statusMessage = `You have loaded the "${_.get(menuDetails, ['name'])}" menu`;
        break;
      case 'menuSaved':
        statusMessage = `Menu has been saved as "${_.get(menuDetails, ['name'])}"`;
        break;
      default:
        break;
    }

    const { totalCost, totalPrice, foodCostPercent } = this.costsDistribution(sections, recipeUsageInfo);
    return (
        <div>
          <Onboarding pageKey="order_editor" />
          {
            editOrderDisabled ?
              <Row><Col xs={12} mdOffset={2} md={8} className="crud-global-warning">
                Past Orders are not editable
              </Col></Row>
              : null
          }
          <Row>
            <Col xs={12} md={4} className="form-horizontal order-form">
              <div className={subtractFromInventory.value ? '' : 'non-substract-from-inventory'}>
                { editOrderDisabled ?
                  <h3>{name.value}</h3> :
                  <TitleInput
                    inputRef={input => this.initialInput = input}
                    noun="Event or Forecast"
                    {...name}
                  />
                }
              </div>
              <div>
                <FormGroup>
                  <ControlLabel>Description:</ControlLabel>
                  {
                    editOrderDisabled ?
                      <div style={disabledDescStyle}>
                        {description.value === '' ? 'No description provided' : description.value}
                      </div> :
                      <Validate {...description}>
                        <FormControl componentClass="textarea"
                                     placeholder="Add description..."
                                     {...description}
                        />
                      </Validate>
                  }
                </FormGroup>
              </div>
            </Col>
            <Col className="form-horizontal" xs={12} mdOffset={1} md={4}>
              <FormGroup>
                <Col xs={2} componentClass={ControlLabel}>Date:</Col>
                <Col xs={8}>
                  {
                    editOrderDisabled ?
                      <div style={{ paddingTop: '2px' }}>{moment(date.value).format('ddd ' + dateFormat)}</div> :
                      <Validate {...date}>
                        <FormControl
                          componentClass={DatePicker}
                          {...date}
                          onChange={(newValue) => {
                            if (newValue === null) date.onChange(undefined); // i.e. translate null to undefined
                            else date.onChange(newValue);
                          }}
                          onBlur={event => {
                            if (!event || !event.target) return;
                            date.onBlur(event);
                          }}
                          minDate={moment().startOf('day').format()}
                        />
                      </Validate>
                  }
                </Col>
              </FormGroup>
              <div>
                <Checkbox
                  {...subtractFromInventory}>
                  <TextWithHelp helpText="If checked, then this order will be subtracted from Parsley's Current Expected Inventory on the date of the event">
                    Subtract from Current Inventory on this Date
                  </TextWithHelp>
                </Checkbox>
              </div>

            </Col>
            <Col md={4} />
          </Row>
          <Row>
            <Col xs={12}>
              {
                editOrderDisabled && statusMessage ? '' :
                  <div className="crud-global-warning mb-1">
                    {statusMessage}
                  </div>
              }
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={4} className="form-horizontal">
              {
                editOrderDisabled ? '' :
                  <Button
                    bsStyle="default"
                    onClick={uiStateActions.openLoadMenuModal}
                    >
                    Load Menu
                  </Button>
              }
            </Col>
            <Col xs={12} md={8} className="menu-actions">
              {
                editOrderDisabled ? '' :
                  <Button className="pull-right" onClick={() => this.showCostModal()}>
                    Cost Breakdown
                  </Button>
              }
              {
                editOrderDisabled ? '' :
                  <Button
                    bsStyle="default"
                    className="pull-right"
                    onClick={() => menuDetails ?
                      this.setState({ step: 'confirm' })
                      : this.setState({
                        step: 'prompt-name',
                        statusMsgKey: 'menuSaved',
                      })
                    }
                    disabled={
                      (menuDetails && !this.isMenuDirty())
                      || _.isEmpty(this.getMenuSections())
                    }
                  >
                    Save as Menu
                  </Button>
              }
              {
                editOrderDisabled ? '' :
                  <PrintButton
                    onClick={uiStateActions.printMenu}
                    noun="menu"
                    className="pull-right"
                  />
              }
            </Col>
          </Row>
          <Row>
              <div>
                {this.renderCustomOrder()}
              </div>
          </Row>
          { uiState.get('showShoppingList') ?
              <ShoppingListModal key="shopping-list-modal"
                                 orderId={this.sourceId()}
                                 baseOrder={uiState.get('shoppingListOrder')}
                                 purchaseOrders={purchaseOrders}
                                 hide={uiStateActions.closeShoppingListModal}
                                 shoppingEmailStatus={this.state.shoppingEmailStatus}
                                 lastActiveSupplier={this.state.lastActiveSupplier}
                                 close={() => uiStateActions.closeShoppingListModal()}
                                 send={(supplierId, userEditedOrder) => uiStateActions.openSendModal(supplierId, userEditedOrder)}
              />
              : null }

          { uiState.get('showCostModal') ?
            <CostBreakdown key="cost-modal"
              page="order"
              name={name.value}
              totalCost={totalCost}
              totalPrice={totalPrice}
              foodCostPercent={foodCostPercent}
              companyLogoUrl={companyLogoUrl}
              baseOrder={uiState.get('baseOrder')}
              hide={uiStateActions.closeCostModal} />
           : null }

          { uiState.get('showLoadMenuModal') ?
            <LoadMenuModal
              key="load-menu-modal"
              hide={uiStateActions.closeLoadMenuModal}
              onLoadMenuRequest={vals => {
                if (_.isEmpty(this.getMenuSections())) {
                  this.getMenuAndProductDetails(vals.menuSelected)
                    .then(() => Promise.resolve(this.copyMenuDetailsToFieldValues()))
                    .then(
                      () => {
                        this.setState({ isOrderDirty: true });
                        uiStateActions.closeLoadMenuModal();
                      },
                      err => Promise.reject(err),
                    );
                } else {
                  uiStateActions.openLoadMenuConfirm(vals.menuSelected);
                }
              }}
            /> : null
          }
          { uiState.get('showLoadMenuConfirm') ?
            <LoadMenuConfirm
              key="load-menu-confirm"
              hide={uiStateActions.closeLoadMenuConfirm}
              selectedMenuId={uiState.get('selectedMenuId')}
              loadMenu={(formVals) => this.getMenuAndProductDetails(
                  formVals.selectedMenu,
                )
                .then(() => Promise.resolve(this.copyMenuDetailsToFieldValues()))
                .then(
                  () => {
                    this.setState({
                      isOrderDirty: true,
                      statusMsgKey: 'menuLoaded',
                    });
                    uiStateActions.closeLoadMenuConfirm();
                  },
                  err => Promise.reject(err),
                )
              }
            /> : null
          }
          {uiState.get('printStep') === 'show_print_options' && <OrderPrintOptionsModal
              hide={uiStateActions.closePrintOptionModal}
              onPrintCalled={(selectedOpts) =>
                selectedOpts.includes('print_recipes') || selectedOpts.includes('print_sub_recipes')
                  ? uiStateActions.openPrintRecipeModal(selectedOpts)
                  : this.printPrep()
              }
          />}
          { uiState.get('printStep') === 'show_print_recipe_options' ?
             <PrintRecipeModal key="print-recipe-modal"
                page="order"
                getBaseOrder={() => uiState.get('printRecipesOrder')}
                orderName={name.value}
                orderDate={date.value}
                allMeasures={allMeasures}
                conversions={allMeasures}
                companyLogoUrl={companyLogoUrl}
                hide={uiStateActions.closePrintRecipeModal}
                title={'Print Setup'}
                shouldPrintRecipes={uiState.get('selectedPrintOptions').includes('print_recipes')}
                shouldPrintSubRecipes={uiState.get('selectedPrintOptions').includes('print_sub_recipes')}
                shouldIncludePrep={uiState.get('selectedPrintOptions').includes('print_preps')}
                prepProps={{
                  allUnits: allUnits,
                  productionOrder: productionOrder,
                  productDetails: productDetails,
                  detailedIngredients: detailedIngredients,
                  baseOrder: uiState.get('printRecipesOrder'),
                  companyLogoUrl: companyLogoUrl,
                  orderName: name.value,
                  orderDate: date.value,
                  shouldPrintPrep: false,
                  prep: true,
                }}
             />
              : null }
          { uiState.get('printPrep') ?
             <PrintPreps
                allUnits={allUnits}
                productionOrder={productionOrder}
                productDetails={productDetails}
                detailedIngredients={detailedIngredients}
                companyLogoUrl={companyLogoUrl}
                baseOrder={uiState.get('printRecipesOrder')}
                orderName={name.value}
                orderDate={date.value}
                onPrintComplete={uiStateActions.printPrepComplete}
                shouldPrintPrep
                prep />
              : null }
          { uiState.get('printMenu') ?
            <PrintMenu
              onComplete={uiStateActions.completePrintMenu}
              name={name}
              date={date}
              sections={sections}
            />
            : null
          }
          { uiState.get('showSendModal') ?
             <SendModal
                supplierId={uiState.get('sendSupplierId')}
                orders={this.isCreatingNew() ? [] : [this.sourceId()]}
                orderDeets={uiState.get('userEditedOrder')}
                page={'edit'}
                hide={() => {
                  this.setState({ lastActiveSupplier: uiState.get('sendSupplierId') });
                  uiStateActions.closeSendModal();
                  this.showShoppingList();
                }}
                afterModalSent={() => {
                  this.setState({
                    shoppingEmailStatus: this.state.shoppingEmailStatus.merge(Immutable.Map({ [uiState.get('sendSupplierId')]: true })),
                  });
                }}
                isEmailSent={this.state.shoppingEmailStatus.get(`${uiState.get('sendSupplierId')}`)}
             />
          : null }
          {stepContent}
        </div>);
  }

  cancelSave = () => {
    this.setState({ step: 'editor' });
  };

  deleteItem() {
    return deleteOrder(this.props.params.id);
  }

  getMenuAndProductDetails(menuId, cb) {
    if (menuId === 'custom_menu') {
      return Promise.resolve();
    }

    return getMenuDetails(menuId).then(menu => {
      matchItemsWithMenu(
          this.props.fields.items,
          Immutable.fromJS(this.props.menuDetails),
          this.isCreatingNew(),
      );

      const ingredientIds = _.flatMap(menu.sections, s => s.items).map(item => item.product);
      if (cb) {
        return getMultipleProductsUsageInfo(ingredientIds).then(() => cb());
      } else {
        return getMultipleProductsUsageInfo(ingredientIds);
      }
    });
  }

  getCustomOrderDetails(items) {
    // get recipeUsageInfo for all using props
    return getMultipleProductsUsageInfo(items.map(i => i.product));
  }

  componentWillMount() {
    super.componentWillMount();
    const { router, route, productionOrderActions, editOrderDisabled } = this.props;
    const { sections } = this.props.fields;

    newOrderTemplate = newOrderTemplate.set('date', moment().add(1, 'day').format());

    // cleanup
    productionOrderActions.dropDetails();

    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      // prompt if props is dirty or items dragged/removed or new menu loaded
      if (this.props.dirty || this.state.isOrderDirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });

    if (this.sourceId() !== null) {
      getOrderDetails(this.sourceId())
          .then((deets) => {
            if (deets.usedMenu && typeof deets.usedMenu === 'object') { // duplicate convertOrderJSONToFormValues logic because we can't trust props to have been updated before this
              this.getCustomOrderDetails(_.flatMap(deets.usedMenu.sections, section => section.items));
              if (!editOrderDisabled && _.isEmpty(deets.usedMenu.sections)) {
                sections.addField();
              }
            } else {
              this.getMenuAndProductDetails(deets.usedMenu)
                  .then(() => Promise.resolve(this.copyMenuDetailsToFieldValues(true)));
            }

            return deets;
          });
    }


    getMeasures();
    getMenuList();
    getPurchaseList();
    getMenuItemsList();
    getOnboardingInfo();
  }

  componentWillUnmount() {
    this.props.uiStateActions.leaveOrderEdit();
    this.props.menuActions.dropDetails();

    if (this.sourceId() !== null)
      this.props.orderActions.dropDetails();
  }

  componentDidUpdate() {
    if (!this.props.editOrderDisabled) {
      this.orderSectionsDraggable();
    }
  }

  listUrl = '/orders';
  listTitle = 'Plans & Purchasing'
  deleteTitle='delete sales'
  containerClass = 'order-container'

  // ALL places where this is used in CrudEditor should be overridden by listTitle et al.
  // noun = "event or forecast";

  showFooterExtraButtons = false;

}

const ShoppingListModal = (props) => <Modal show onHide={props.hide} className="medium-modal">
  <Modal.Header closeButton>
    <Modal.Title>Purchase Orders</Modal.Title>
  </Modal.Header>
  <Modal.Body>
    <ShoppingList
      page={'edit'}
      orderId={props.orderId}
      baseOrder={props.baseOrder}
      purchaseOrders={props.purchaseOrders}
      shoppingEmailStatus={props.shoppingEmailStatus}
      scrollToSupplier={props.lastActiveSupplier}
      close={props.close}
      send={props.send}
    />
  </Modal.Body>
  <Modal.Footer>
    <Button bsStyle="default" onClick={props.hide} >
      Done
    </Button>
  </Modal.Footer>
</Modal>;

class ConfirmMenuModal extends Component {
  render() {
    const { hide, switchToReplaceMenu, switchToEditName } = this.props;

    return <Modal show onHide={hide} className="small-modal">
      <Modal.Header closeButton>
        <Modal.Title>Save as Menu</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ minHeight: '120px' }}>
        <Row className="mb-2">
          <Col xs={12} className="text-center">
            <Button bsStyle="default" onClick={switchToReplaceMenu}>Replace Existing Menu</Button>
          </Col>
        </Row>
        <Row className="mb-2">
          <Col xs={12} className="text-center">
            <Button bsStyle="default" onClick={switchToEditName}>Create New Menu</Button>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>;
  }
}

@reduxForm({
  form: 'confirm-replace-menu',
  fields: [],
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
})
class ConfirmReplaceMenu extends Component {
  render() {

    const { hide, menuName, replaceExisting, handleSubmit, submitting, error } = this.props;

    return <Modal show onHide={hide} className="modal-base">
      <Modal.Header closeButton>
        <Modal.Title>Replace Menu</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row><Col xs={12}>Are you sure you want to replace the menu titled '{menuName}' ?</Col></Row>
        {
          error ? <Row><Col xs={12} className={'text-danger'}>{error}</Col></Row> : null
        }
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={hide}>
          cancel
        </Button>
        <Button bsStyle="danger" onClick={handleSubmit(replaceExisting.bind(this))}>
          {submitting ? 'replacing' : 'replace'}
        </Button>
      </Modal.Footer>
    </Modal>;
  }
}

const createMenuAsyncValidate = (values, dispatch, props) => lookupMenuName(values.name.trim())
  .then(response => {
    if (response != null) {
      throw { name: 'Name is already used' };
    }
  });

@reduxForm({
  form: 'menu-name-edit',
  fields: ['name'],
  validate: (value) => {
    const error = {};
    if (!value.name) {
      error.name = 'Required';
    }
    return error;
  },
  asyncValidate: createMenuAsyncValidate,
  asyncBlurFields: ['name'],
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
})
class MenuNameEdit extends Component {

  render() {

    const { hide, handleComplete, fields, submitting, handleSubmit, error } = this.props;

    return (
        <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Enter Menu Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col xs={12} className={'text-danger'}>{error}</Col>
          </Row>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <Validate {...fields.name} inModal>
                  <FormControl type="text" bsSize="large" placeholder="Menu Name" {...fields.name} />
                </Validate>
              </FormGroup>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="default" type="submit" disabled={submitting} onClick={handleSubmit(handleComplete.bind(this))}>Save</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

@reduxForm({
  form: 'load-menu-modal',
  fields: ['menuSelected'],
  validate: values => {
    const err = {};
    if (!values.menuSelected) {
      err.menuSelected = 'Required';
    }
    return err;
  },
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
})
class LoadMenuModal extends Component {
  render() {
    const {
      fields: { menuSelected },
      hide, onLoadMenuRequest,
      handleSubmit,
    } = this.props;

    return <Modal show onHide={hide} className="small-modal">
      <Modal.Header closeButton>
        <Modal.Title>Select Menu</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col xs={12}>
            <FormGroup>
              <Validate {...menuSelected} inModal>
                <SelectMenu {...menuSelected} />
              </Validate>
            </FormGroup>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="default" type="submit" onClick={handleSubmit(onLoadMenuRequest.bind(this))}>Load</Button>
      </Modal.Footer>
    </Modal>;
  }
}

@reduxForm(
  {
    form: 'load-menu-confirm',
    fields: ['selectedMenu'],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: {
      selectedMenu: ownProps.selectedMenuId,
    },
    menus: state.get('menus'),
  }),
)
class LoadMenuConfirm extends Component {
  render() {
    const { hide, selectedMenuId, menus, loadMenu, handleSubmit, submitting, error } = this.props;

    const menuName = menus.find(m => m.get('id') === selectedMenuId).get('name');

    return <Modal show onHide={hide} className="modal-base">
      <Modal.Header closeButton>
        <Modal.Title>Replace Menu</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row><Col xs={12}>Are you sure you want to replace the current menu with '{menuName}' ?</Col></Row>
        <Row><Col xs={12}>You may lose some of the data you just entered</Col></Row>
        {
          error ? <Row><Col xs={12} className={'text-danger'}>{error}</Col></Row> : null
        }
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={hide}>
          cancel
        </Button>
        <Button bsStyle="danger" onClick={handleSubmit(loadMenu.bind(this))}>
          {submitting ? 'replacing' : 'replace'}
        </Button>
      </Modal.Footer>
    </Modal>;
  }
}

const colAttrs = {
  titles: {
    className: 'order-section-title',
    xs: 4,
  },
  orderAmount: {
    className: 'order-section-quantity',
    xs: 3,
  },
  totalCost: {
    className: 'order-section-cost',
    xs: 2,
  },
  price: {
    className: 'order-section-price',
    xs: 2,
  },
  spacer: {
    className: 'order-section-spacer',
  },
};

const disabledDescStyle = {
  backgroundColor: '#dcdcdc',
  minHeight: '80px',
  padding: '8px',
  color: '#808080',
};

class OrderSections extends Component {
  render() {
    const {
      fields: sections,
      menuItems,
      allMeasures, allUsedItems,
      editOrderDisabled, alreadyAddedSection,
      calculateItemCost,
      orderItemsDrake,
    } = this.props;
    return <div id="custom-orders-container" className={editOrderDisabled ? 'order-editor-disabled' : null}>
      {
        sections.map((section, sectionIndex) =>
            <OrderSection key={sectionIndex}
                          {...section}
                          menuItems={menuItems}
                          ordinal={sectionIndex}
                          orderItemsDrake={orderItemsDrake}
                          deleteSection={() => {
                            sections.removeField(sectionIndex);
                            this.setState({ isOrderDirty: true });
                          }}
                          allUsedItems={allUsedItems}
                          allMeasures={allMeasures}
                          grabFocus={alreadyAddedSection}
                          editOrderDisabled={editOrderDisabled}
                          onItemsRemoved={() => this.setState({ isOrderDirty: true })}
                          calculateItemCost={calculateItemCost}
            />)
      }
    </div>;
  }
}

@connect(
  state => ({
    recipeUsageInfo: state.get('detailedIngredients'),
  }),
)
class OrderSection extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showDragHandle: false,
    };
  }

  showDragHandle = () => {
    this.setState({ showDragHandle: true });
  };

  hideDragHandle = () => {
    this.setState({ showDragHandle: false });
  };

  componentWillMount() {
    const { items } = this.props;

    if (!items.length) {
      items.addField();
    }
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }

    const { ordinal, orderItemsDrake } = this.props;
    orderItemsDrake.containers.push(document.getElementById('order-section-items-' + ordinal));
  }

  render() {
    const {
      name, items, ordinal, deleteSection,
      allUsedItems, allMeasures, editOrderDisabled, onItemsRemoved,
      calculateItemCost, recipeUsageInfo,
      menuItems,
    } = this.props;

    return (
     <Well className="order-section" id={'order-section-' + ordinal} onMouseLeave={this.hideDragHandle}>
       {editOrderDisabled ? null : <div className="order-section-overlay" id={'order-section-overlay-' + ordinal} onMouseEnter={this.showDragHandle} onMouseLeave={this.hideDragHandle} />}
        <div className="order-section-content">
          {
            editOrderDisabled ? null :
              <div className="order-section-index">
                <p>{ this.state.showDragHandle ? <Glyphicon glyph="align-justify" className="drag-order-section" onMouseEnter={this.showDragHandle} /> : null }</p>
              </div>
          }
          <div className="order-section-items-content">
            <div className={editOrderDisabled ? 'order-section-headers' : 'order-section-headers order-section-headers-enabled'}>

              <div {...colAttrs.titles}>
                {
                  editOrderDisabled ?
                    name.value :
                    <Validate className="menu-section-name-input" {...name}>
                      <FormControl className="form-control-title" type="text"
                                   {...name}
                                   inputRef={input => this.initialInput = input}
                                   placeholder="Section Name"
                      />
                    </Validate>
                }
              </div>
              { !editOrderDisabled && <div className="drag-order-section-item" /> }
              <div {...colAttrs.orderAmount}/>
              <div {...colAttrs.totalCost}>Cost</div>
              <div {...colAttrs.price}>Price</div>
              <div {...colAttrs.spacer} />
            </div>
            <hr className="well-child-hr"/>
            <div className="order-section-items" id={'order-section-items-' + ordinal}>
              {
                items.map((item, itemIndex) =>
                  <div key={itemIndex} id={'order-section-item-' + ordinal + '-' + itemIndex}>
                    <Validate key={itemIndex} {...item}>
                      <CustomMenuItem
                        menuItems={menuItems}
                        sectionOrdinal={ordinal}
                        itemOrdinal={itemIndex}
                        showSectionDragHandle={this.state.showDragHandle}
                        deleteRecipe={() => {
                          items.removeField(itemIndex);
                          onItemsRemoved();
                        }}
                        allUsedItems={allUsedItems}
                        allMeasures={allMeasures}
                        grabFocus={this.state.alreadyAddedRecipeUsage}
                        editOrderDisabled={editOrderDisabled}
                        isLastItem={itemIndex === items.length - 1}
                        itemCost={calculateItemCost(item, recipeUsageInfo)}
                        {...item}
                        />
                    </Validate>
                  </div>,
                )
              }
            </div>
            {
              editOrderDisabled ? null :
                <AddButton noun="dishes" className="menu-section-add-item-button"
                           onClick={() => {
                             items.addField();
                             this.setState({ alreadyAddedRecipeUsage: true });
                           }}

                />
            }

          </div>
            <div className="delete-link">
              {editOrderDisabled ? null : <span onClick={deleteSection}>delete</span>}
            </div>
        </div>
      </Well>
    );
  }
}

@connect(
    state => ({
      recipeUsageInfo: state.get('detailedIngredients'),
    }),
)
class CustomMenuItem extends Component {

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { product } = nextProps;
    if (product.value && product.value !== this.props.product.value) {
      getProductUsageInfo(product.value);
    }
  }

  render() {
    const {
      sectionOrdinal,
      itemOrdinal,
      showSectionDragHandle,
      allUsedItems,
      allMeasures,
      menuItems,
      recipeUsageInfo,
      deleteRecipe,
      editOrderDisabled,
      isLastItem,
      // form fields
      orderedQuantity,
      product,
      itemCost,
      cashFlow,
    } = this.props;
    const unitCost = recipeUsageInfo.getIn([product.value, 'unitCost'], 0);
    const price = editOrderDisabled ? cashFlow.value : recipeUsageInfo.getIn([product.value, 'portionPrice'], 0);
    const conversions = recipeUsageInfo.getIn([product.value, 'measures']);
    const productName = menuItems && !menuItems.filter(i => i.get('id') === product.value).isEmpty() ? menuItems.find(i => i.get('id') === product.value).get('name') : null;
    const selectedMenuItem = menuItems && menuItems.find(i => i.get('id') === product.value);

    return <div>
      <div className="order-section-item">
        <div {...colAttrs.titles}>
          { menuItems ?
              editOrderDisabled ?
                  productName :
                  <Select
                      {...product}
                      autosize={false}
                      inputRef={select => this.initialInput = select}
                      onBlur={() => {
                        product.onBlur(product.value);
                        // clear measure and unit populated if:
                        if (
                            // new product has no measures defined
                            (conversions && conversions.isEmpty()) ||
                            // measure selected but not defined for new product
                            (orderedQuantity.measure.value && !conversions.find(c => c.get('measure') == orderedQuantity.measure.value))
                        ) {
                          orderedQuantity.measure.onChange(undefined);
                          orderedQuantity.unit.onChange(undefined);
                        }
                      }}
                      options={menuItems
                          .filter(i =>
                              !i.get('tombstone') &&
                              (i.get('id') == product.value || !allUsedItems.includes(i.get('id'))),
                          )
                          .map(i => ({
                            value: i.get('id'),
                            label: i.get('name'),
                            className: i.get('ingredient') ? 'text-italic' : null,
                          })).toJS()}
                      style={selectedMenuItem && selectedMenuItem.get('ingredient') ? { fontStyle: 'italic' } : null}
                      searchIndexName={indexTypes.MENU_ITEMS}
                  />
              : <LoadingSpinner /> }
        </div>
        {
          editOrderDisabled ?
              null : <Glyphicon glyph="menu-hamburger" className="drag-order-section-item" style={showSectionDragHandle ? { visibility: 'hidden' } : null}/>
        }
        <div {...colAttrs.orderAmount}>
          <div>
          {
            editOrderDisabled ?
                `${orderedQuantity.amount.value || 0} ${orderedQuantity.unit.value ? unitDisplayName(orderedQuantity.unit.value, allMeasures) : 'portions'}` :
                product.value && allMeasures && conversions ?
                    <QuantityInput
                        compact autoAdjust multiMeasure
                        useUnits={!conversions.isEmpty()}
                        measures={allMeasures}
                        conversions={conversions}
                        reduxFormFields={{
                          amountField: {
                            ...orderedQuantity.amount,
                            onChange: newAmount => {
                              if (typeof newAmount != typeof 0) {
                                return;
                              }
                              cashFlow.onChange(newAmount * price);
                              orderedQuantity.amount.onChange(newAmount);
                            },
                          },
                          measureField: orderedQuantity.measure,
                          unitField: orderedQuantity.unit,
                        }}
                    /> : null
          }
          </div>
        </div>
        <div {...colAttrs.totalCost}>
          {
            unitCost
                ? <CurrencyDisplay value={itemCost} />
                : '$0.00'
          }

        </div>
        <div {...colAttrs.price} style={editOrderDisabled ? { width: '24%' } : null}>
          { Number(orderedQuantity.amount.value) > 0 ?
              price ?
                  <CurrencyDisplay value={purchasingRound(editOrderDisabled ? price : price * orderedQuantity.amount.value)} />
                  : 'Not Priced'
              : ''
          }
        </div>
        {
          editOrderDisabled ? null :
              <span className="delete-link"
                  onClick={deleteRecipe}>
              remove
            </span>
        }
      </div>
      {
        isLastItem ? null :
            <hr className="well-child-hr" key={'hr-' + sectionOrdinal + '-' + itemOrdinal}/>
      }
    </div>;
  }
}

@connect(
    state => ({
      menus: state.get('menus'),
    }),
)
class SelectMenu extends Component {

  render() {

    const {
      onBlur, // removing this from other formField fields
      menus, ...formField
    } = this.props;
    // if we saved Custom Order - formField.value is an object
    // in the first validation we filter "null", so in the second validation we can check  formField.value of typeof
    const value = formField.value ? typeof formField.value === 'object' ? 'custom_menu' : formField.value : '';

    return menus && formField ?
        <Select
          {...formField}
          value={value}
          options={menus.map(i => ({
                value: i.get('id'),
                label: i.get('name'),
              })).toJS()}
        /> : <LoadingSpinner />;
  }

}

export class PrintPreps extends Component {

  static getNameFromOrderSection(productId, section) {
    let entry = section.find(e => e.get('product') == productId);
    return entry && entry.get('name');
  }

  static getPurchasedIngredientName(productId, props) {
    const { productionOrder } = props;
    if (!productionOrder) return null;
    return PrintPreps.getNameFromOrderSection(productId, productionOrder.get('shoppingList'));
  }

  static getPurchasedIngredientConversions(productId, props) {
    const { detailedIngredients } = props;
    if (!detailedIngredients.has(productId)) return null;

    return detailedIngredients.getIn([productId, 'measures']);
  }

  static getRecipeName(productId, props) {
    const { productionOrder } = props;
    if (!productionOrder) return null;
    return PrintPreps.getNameFromOrderSection(productId, productionOrder.get('recipeList'));
  }

  static getRecipeConversions(productId, props) {
    const { productDetails } = props;
    if (!productDetails.has(productId)) return null;

    return Immutable.fromJS(productDetails.get(productId).measures);
  }

  static getIngredientName(productId, props) {
    return PrintPreps.getPurchasedIngredientName(productId, props) || PrintPreps.getRecipeName(productId, props);
  }

  static getIngredientUnitName(unitId, productId, props) {
    return unitDisplayName(unitId,
        PrintPreps.getRecipeConversions(productId, props) || PrintPreps.getPurchasedIngredientConversions(productId, props),
    );
  }

  static getContent(props) {
    const {
      baseOrder, productionOrder, productDetails, allUnits, orderName, orderDate, companyLogoUrl,
      shouldPrintPrep,
    } = props;

    if (!productionOrder) return <div />

    let directlyOrderedItems = baseOrder.map(item => item.get('product')).toSet();
    let recipeList = productionOrder.get('recipeList')
        .filter(item => !item.get('isSimplePrep'))
        // weird string magic to sort first by nested-or-not, then by name
        .sortBy(
            item => `${Number(directlyOrderedItems.contains(item.get('product')))} ${item.get('name')}`,
        );

    const logo = <div className="logo">
                <img id="powered-by-logo" className="navbar-brand" />
              </div>;

    const grayLabel = companyLogoUrl && <table style={{ width: '100%' }}>
      <tr>
        <td style={{ width: '90%' }} />
        <td>
          <div className="gray-label" style={{ position: 'static' }}><img id="company-logo" /></div>
        </td>
      </tr>
    </table>;

    const simplePreps = productionOrder.get('recipeList')
        .filter(item => item.get('isSimplePrep'));
    const rawIngredientPreps = simplePreps.groupBy(prep => parseInt(
          prep.get('billOfMaterials').keySeq().first(),
    )).map(ingredientPreps => { // mapping values in a map
      const idQuantityPairs = ingredientPreps.map(prep => [
        prep.get('product'),
        prep.get('totalProduced'),
      ]);
      return Immutable.Map(idQuantityPairs);
    }).sortBy(({}, rawIngredientId) => PrintPreps.getIngredientName(rawIngredientId, props).toLocaleUpperCase());

    const nestedRecipes = recipeList.filter(r => r.get('isAdvancePrep') && Boolean(r.getIn(['totalProduced', 'measure'])));

    function dateTimeDisplay(iso8601String) {
      return moment(iso8601String).format('LL');
    }

    if (rawIngredientPreps.size + nestedRecipes.size < 1) {
      return <body>
        {shouldPrintPrep && logo}
        {shouldPrintPrep && grayLabel}
      </body>;
    }

    return <body>
      {shouldPrintPrep && logo}
      {shouldPrintPrep && grayLabel}
      <h1 className="print-header">Prep for {orderName} &ndash; {dateTimeDisplay(orderDate)}</h1>
      <div className="print-preps break-page-after">
        { rawIngredientPreps.size ? rawIngredientPreps.map((prepUsage, rawIngredientId) => {
            let rawIngredientName = PrintPreps.getIngredientName(rawIngredientId, props);

            return <div key={rawIngredientId}>
              <h1 className="print-header">
                {rawIngredientName}
              </h1>
              { prepUsage.map((preppedQuantity, prepId) => {
                  let details = Immutable.fromJS(productDetails.get(parseInt(prepId)));
                  let hasAmount = preppedQuantity.get('amount') != null && preppedQuantity.get('amount') > 0;

                  let { amount: displayAmount, unit: displayUnit } = preppedQuantity.get('amount') && preppedQuantity.get('unit')
                    ? rescaleUnits(preppedQuantity.toJS())
                    : [preppedQuantity.get('amount'), preppedQuantity.get('unit')];

                  return <div key={prepId}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {PrintPreps.getIngredientName(prepId, props).replace(rawIngredientName + ', ', '')}
                    {
                      hasAmount
                          ? ': ' +
                            purchasingRound(displayAmount, unitDisplayPrecision(displayUnit), unitDisplayQuantum(displayUnit)) +
                            unitDisplayName(displayUnit, details.get('measures'))
                          : null
                    }
                  </div>;
                }).valueSeq()
              }
            </div>;
          }).valueSeq()
        : null }
        { nestedRecipes.size ? nestedRecipes.map((item) => {
          let productId = item.get('product');
          let details = Immutable.fromJS(productDetails.get(productId));

          let producedQuantity = item.get('totalProduced');
          let amount = producedQuantity.get('amount');
          let { amount: displayAmount, unit: displayUnit } = amount && producedQuantity.get('unit')
            ? rescaleUnits(producedQuantity.toJS())
            : [amount, producedQuantity.get('unit')];

          let unitName = displayUnit
              ? unitDisplayName(displayUnit, details.get('measures'))
              : amount == 1 ? 'Portion'
              : 'Portions';

            return <div>
              <h1 key={productId} className="print-header print-header-sub-recipe" style={{ display: 'inline-block' }}>{item.get('name')}</h1>
              : { purchasingRound(displayAmount, unitDisplayPrecision(displayUnit), unitDisplayQuantum(displayUnit)) } {unitName}
            </div>;
          })
        : null }
      </div>
    </body>;
  }

  printAllRecipes(props) {
    const { companyLogoUrl, onPrintComplete } = props;
    const content = ReactDOMServer.renderToString(PrintPreps.getContent(props));

    const imageUrlListWithId = [{ url: '/assets/images/poweredby.png', id: 'powered-by-logo' }];

    if (companyLogoUrl) {
      imageUrlListWithId.push({ url: companyLogoUrl, id: 'company-logo' });
    }

    onPrintComplete();
    return printContent(content, imageUrlListWithId);
  }


  render() {
    const { shouldPrintPrep } = this.props;
    return shouldPrintPrep ? <div /> : PrintPreps.getContent(this.props);
  }

  static allUsedRecipes(props) {
    const {
        baseOrder, productionOrder,
    } = props;

    return productionOrder
        ? productionOrder.get('recipeList').map(r => r.get('product')).toSet()
        : baseOrder.map(o => o.get('product')).toSet();
  }

  static allPurchasedIngredients(props) {
    const productionOrder = props.productionOrder;
    return productionOrder
        ? productionOrder.get('shoppingList').map(item => item.get('product')).toSet()
        : Immutable.Set();
  }

  static readyToPrint(props) {
    return props.productionOrder &&
        PrintPreps.allUsedRecipes(props).isSubset(props.productDetails.keySeq()) &&
        PrintPreps.allPurchasedIngredients(props).isSubset(props.detailedIngredients.keySeq());

  }

  static checkProps(thisProps, nextProps) {
    if (!_.isNil(nextProps.baseOrder) && !Immutable.is(thisProps.baseOrder, nextProps.baseOrder)) {
      getProductionOrder(nextProps.baseOrder, true); // don't know when this would happen, but whatevs
    }

    if (!Immutable.is(thisProps.baseOrder, nextProps.baseOrder)
        || !Immutable.is(thisProps.productionOrder, nextProps.productionOrder)) {
      const ingredientIds = PrintPreps.allUsedRecipes(nextProps)
          .subtract(PrintPreps.allUsedRecipes(thisProps));
      getMultipleProductsDetails(ingredientIds);
    }

    if (!Immutable.is(thisProps.productionOrder, nextProps.productionOrder)) {
      const ingredientIds = PrintPreps.allPurchasedIngredients(nextProps)
          .subtract(PrintPreps.allPurchasedIngredients(thisProps));
      getMultipleProductsUsageInfo(ingredientIds);
    }
  }

  static loadData(props) {
    getMeasures();
    getProductionOrder(props.baseOrder, true);

    // we probably don't have the full production order info yet, but we want to at least pull in the root set
    // of products; http/2 likes parallelism!
    const usedRecipesIngredientIds = PrintPreps.allUsedRecipes(props);
    getMultipleProductsDetails(usedRecipesIngredientIds);
    const purchasedIngredientIds = PrintPreps.allPurchasedIngredients(props);
    getMultipleProductsUsageInfo(purchasedIngredientIds);
  }

  componentWillReceiveProps(nextProps) {

    const { shouldPrintPrep } = this.props;

    if (shouldPrintPrep) {
      PrintPreps.checkProps(this.props, nextProps);

      if (!PrintPreps.readyToPrint(this.props) && PrintPreps.readyToPrint(nextProps)) {
        this.printAllRecipes(nextProps);
      }
    }
  }

  componentWillMount() {

    if (this.props.shouldPrintPrep) {

      PrintPreps.loadData(this.props);

      if (PrintPreps.readyToPrint(this.props)) {
        this.printAllRecipes(this.props);
      }
    }
  }
}
