import React, { Component } from 'react';
import Immutable from 'immutable';
import _ from 'lodash';

import { checkFields, deepArrayField, deepObjectField, isBlank, optional, required } from '../../utils/validation-utils';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import dragula from 'react-dragula';

import { Well, Col, Row, FormControl, Glyphicon } from 'react-bootstrap';
import Select from '../Select';
import Validate from 'common/ui-components/Validate';

import { withRouter } from 'react-router';
import { history } from '../../navigation';

import { getMenuDetails, saveMenu, deleteMenu, getMenuItemsList, getOnboardingInfo }
    from '../../webapi/endpoints';

import { actionCreators as MenuActions } from '../../reducers/menuDetails';
import { actionCreators as UIStateActions } from '../../reducers/uiState/menuEdit';
import { indexTypes } from '../../reducers/indexesCache';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { deepOmit } from '../../utils/general-algorithms';

import TitleInput from '../TitleInput';
import AddButton from '../AddButton';
import PrintButton from '../PrintButton';
import PrintMenu from '../PrintMenu';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { convertMenuJSONToFormValues } from '../../utils/form-utils';

function validate(values) {

  const menuValidators = {
    name: required(),
    sections: deepArrayField(deepObjectField({
      name: optional(),
      items: deepArrayField(deepObjectField({
        product: prod => isBlank(prod) ? 'Please select an item' : null,
      })),
    })),
  };

  return checkFields(values, menuValidators);
}

const newMenuTemplate = Immutable.fromJS({
  sections: [
    {
      items: [
        {},
      ],
    },
  ],
});

@withRouter
@reduxForm(
    {
      form: 'menu_edit',
      fields: [
        'name',
        'sections[].name',
        'sections[].items[].product',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      onSubmitSuccess: () => history.push('/menus'),
    },
    (state, ownProps) => ({
      initialValues: !CrudEditor.isCreatingNew(ownProps.params) || CrudEditor.isCopying(ownProps.params)
          ? CrudEditor.initialValues(state.get('menuDetails'), ownProps.params, convertMenuJSONToFormValues)
          : newMenuTemplate.toJS(),
      mainInfoLoaded: Boolean(state.get('menuDetails')),
      menuItems: state.get('menuItems'),
      uiState: state.get('menuEdit'),
    }),
    dispatch => ({
      menuActions: bindActionCreators(MenuActions, dispatch),
      uiStateActions: bindActionCreators(UIStateActions, dispatch),
    }),
)
export default class MenuEdit extends CrudEditor {
  convertFormValuesToJSON = (deets) => {
    let formatted = Immutable.fromJS(deepOmit(deets, isBlank));
    // The API requires product IDs to be integers, not strings
    if (formatted.has('sections')) {
      formatted = formatted.update('sections', allSections => allSections.map(
          section => {
            if (section.has('items')) {
              section = section.update('items', allItems => allItems.map(
                  item => item.update('product', parseInt),
              ));
            }
            return section;
          },
      ));
    }
    return formatted.toJS();
  };
  handleSave = (id, deets) => saveMenu(id, this.convertFormValuesToJSON(deets));

  constructor(props, context) {
    super(props, context);
    this.menuSectionsDrake = dragula({
      direction: 'vertical',
      moves: (el, container, handle) => handle.classList.contains('drag-menu-section'),
    });
    this.recipeUsageDrake = dragula({
      direction: 'vertical',
      moves: (el, container, handle) => handle.classList.contains('drag-recipe-usage'),
    });
    this.menuSectionsDrake.on('drop', this.reorderMenuSections.bind(this));
    this.recipeUsageDrake.on('drop', this.reorderRecipeUsage.bind(this));
    this.state = {
      alreadyAddedSection: false,
    };
  }

  makeSectionsDraggable = () => {
    this.menuSectionsDrake.containers = [];
    this.menuSectionsDrake.containers.push(document.getElementById('menu-sections'));
  };

  makeExtraButtons() {
    const { uiStateActions } = this.props;
    return [
      <PrintButton onClick={uiStateActions.printMenu} />,
    ];
  }

  reorderMenuSections(el, target, source, sibling) {
    function getOrdinalFromId(element) {
      if (!element || !element.id) {
        return null;
      }

      return Number(element.id.split('-').pop());
    }

    // record changes
    const sourceIndex = getOrdinalFromId(el);
    const siblingIndex = getOrdinalFromId(sibling);

    // put element back
    // 1. remove moved element
    // 2. & 3. find original sibling of source ingredient
    // 4. if original source ingredient found then insert before it
    //      else it must be last ingredient so make it last ingredient
    target.removeChild(el);
    const orgSourceSiblingId = `menu-section-${sourceIndex + 1}`;
    const orgSourceSibling = document.getElementById(orgSourceSiblingId);
    if (orgSourceSibling) {
      source.insertBefore(el, orgSourceSibling);
    } else {
      source.appendChild(el);
    }

    // swap props begins:

    // somehow fields are not changing its values so using full reference path
    // var { fields } = this.props;

    if (_.isNull(siblingIndex)) { // order is moved to last

      // 1. add empty order at the end
      // 2. swap empty ingredient and source ingredient
      // 3. remove source ingredient
      this.props.fields.sections.addField();
      this.props.fields.sections.swapFields(sourceIndex, this.props.fields.sections.length - 1);
      this.props.fields.sections.removeField(sourceIndex);
    } else {

      /**
       * NOTE: We have three methods available to play
       * with fields: addField, removeField and swapFields.
       *
       * Directly swapping orders will not work.
       * And its tedious to make plain JS object from props.sections
       * and add it before sibling.
       *
       * So, instead, we add empty section before sibling,
       * then swap source and newly added empty section
       * and then remove swapped empty section.
       */

        // add empty section before sibling
      this.props.fields.sections.addField({}, siblingIndex);

      // swap and then remove swapped empty section
      if (siblingIndex < sourceIndex) { // moving up
        // when moving up, source section will also shift down when empty section is added
        this.props.fields.sections.swapFields(siblingIndex, sourceIndex + 1);
        this.props.fields.sections.removeField(sourceIndex + 1);
      } else {
        this.props.fields.sections.swapFields(siblingIndex, sourceIndex);
        this.props.fields.sections.removeField(sourceIndex);
      }
    }
  }

  reorderRecipeUsage(el, target, source, sibling) {
    function getOrdinalFromId(element) {
      if (!element || !element.id) {
        return null;
      }

      return Number(element.id.split('-').pop());
    }

    function getItemObj(item) {
      return {
        product: item.product.value,
      };
    }

    /**
     * NOTE:
     * Refer reorderIngredients of RecipeEdit
     */

    const sourceItemIndex = getOrdinalFromId(el);
    const targetSectionIndex = getOrdinalFromId(target);
    const sourceSectionIndex = getOrdinalFromId(source);
    const siblingItemIndex = getOrdinalFromId(sibling);

    target.removeChild(el);
    const orgSiblingId = `menu-section-item-${sourceSectionIndex}-${sourceItemIndex + 1}`;
    const orgSibling = document.getElementById(orgSiblingId);
    if (orgSibling ) {
      source.insertBefore(el, orgSibling);
    } else {
      source.appendChild(el);
    }

    const { sections } = this.props.fields;

    const item = getItemObj(sections[sourceSectionIndex].items[sourceItemIndex]);

    if (sourceSectionIndex != targetSectionIndex) {
      if (_.isNull(siblingItemIndex)) {
        sections[targetSectionIndex].items.addField(item);
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      } else {
        sections[targetSectionIndex].items.addField(item, siblingItemIndex);
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      }
    } else if (_.isNull(siblingItemIndex)) {
      sections[sourceSectionIndex].items.addField();
      sections[sourceSectionIndex].items.swapFields(sourceItemIndex, sections[sourceSectionIndex].items.length - 1);
      sections[sourceSectionIndex].items.removeField(sourceItemIndex);
    } else {

      sections[sourceSectionIndex].items.addField(item, siblingItemIndex);
      const movingUp = siblingItemIndex < sourceItemIndex;

      if (movingUp) {
        sections[sourceSectionIndex].items.removeField(sourceItemIndex + 1);
      } else {
        sections[sourceSectionIndex].items.removeField(sourceItemIndex);
      }
    }
  }

  renderBody() {

    const {
        name, sections,
    } = this.props.fields;
    const { uiStateActions, uiState } = this.props;

    const allUsedItems = Immutable.Set(_(sections)
        .flatMap(s => s.items)
        .filter(_.identity) // empty string or null or undefined is what new fields are initialized to
        .map(item => item.product.value)
        .value());

    return <div>
      <Row>
        <Col xs={12} md={4} className="form-horizontal remove-padding">
          <TitleInput
              noun="Menu"
              inputRef={input => this.initialInput = input}
              {...name}
          />
        </Col>
      </Row>
      <FieldArray fields={sections} component={MenuSections} props={{
        allUsedItems,
        menuSectionsDrake: this.menuSectionsDrake,
        recipeUsageDrake: this.recipeUsageDrake,
      }} />
      {
        uiState.get('printMenu') ?
            <PrintMenu
                name={this.props.fields.name}
                sections={this.props.fields.sections}
                onComplete={uiStateActions.completePrintMenu}
            />
            : null
      }
      <Onboarding pageKey="menu_editor" />
    </div>;
  }

  deleteItem() {
    return deleteMenu(this.props.params.id);
  }

  componentWillMount() {
    super.componentWillMount();
    if (this.sourceId() !== null)
      getMenuDetails(this.sourceId()).then(() => this.makeSectionsDraggable());

    getMenuItemsList();
    getOnboardingInfo();
  }

  componentWillUnmount() {
    if (this.sourceId() !== null)
      this.props.menuActions.dropDetails();
  }

  componentDidMount = () => this.makeSectionsDraggable();

  listUrl = '/menus';
  noun = 'menu';

}

class MenuSections extends Component {
  state = {
    alreadyAddedSection: false,
  }

  render() {
    const { fields, allUsedItems, recipeUsageDrake } = this.props;

    return [
      <div key="menu-sections" id="menu-sections">
        { fields.map((section, sectionIndex) =>
            <MenuSection
                key={sectionIndex}
                ordinal={sectionIndex}
                {...section}
                deleteSection={() => fields.removeField(sectionIndex)}
                allUsedItems={allUsedItems}
                grabFocus={this.state.alreadyAddedSection}
                recipeUsageDrake={recipeUsageDrake}
            />,
        ) }
      </div>,
      <AddButton key="add-section" noun="section"
                 onClick={() => {
                   fields.addField();
                   this.setState({ alreadyAddedSection: true });
                 }}
      />,
    ];
  }
}

export class MenuSection extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      showSectionDragHandle: false,
    };
  }

  showSectionDragHandle = () => this.setState({ showSectionDragHandle: true });

  hideSectionDragHandle = () => this.setState({ showSectionDragHandle: false });

  render() {
    let { name, items, deleteSection, allUsedItems, ordinal } = this.props;

    return <Well className="menu-section" id={'menu-section-' + ordinal}>
      <div className="menu-section-overlay" id={'menu-section-overlay-' + ordinal} onMouseEnter={this.showSectionDragHandle} onMouseLeave={this.hideSectionDragHandle} />
      <div className="menu-section-content">

        <div className="menu-section-header">
          <Validate className="menu-section-name-input" {...name}>
            <FormControl className="form-control-title" type="text"
                         {...name}
                         inputRef={input => this.initialInput = input}
                         placeholder="Section Name"
              />
          </Validate>
        </div>
        <hr className="well-child-hr"/>

        <div className="menu-section-body">
          <div className="menu-section-index">
            <p>
              <Glyphicon glyph="align-justify" className="drag-menu-section" onMouseEnter={this.showSectionDragHandle} style={this.state.showSectionDragHandle ? { zIndex: 59 } : { visibility: 'hidden' }} />
            </p>
          </div>
          <FieldArray component={RecipeUsages} fields={items} props={{
            ordinal,
            allUsedItems,
          }} />
        </div>

      </div>
      <div className="delete-link">
        <span onClick={deleteSection}>DELETE</span>
      </div>
      </Well>;
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }

    const { ordinal, recipeUsageDrake } = this.props;
    recipeUsageDrake.containers.push(document.getElementById('menu-section-items-' + ordinal));
  }
}

class RecipeUsages extends Component {
  state = {
    alreadyAddedRecipeUsage: false,
  };

  render() {
    const { fields, ordinal, allUsedItems } = this.props;
    return <div className="menu-section-items-content">
      <div className="menu-section-items" id={'menu-section-items-' + ordinal}>
        {fields.map((item, itemIndex) =>
            <RecipeUsage
                key={itemIndex}
                deleteRecipe={() => fields.removeField(itemIndex)}
                allUsedItems={allUsedItems}
                grabFocus={this.state.alreadyAddedRecipeUsage}
                {...item.product}
                sectionOrdinal={ordinal}
                ordinal={itemIndex}
            />,
        )}
      </div>
      <AddButton noun="dishes" className="menu-section-add-item-button"
                 onClick={() => {
                   fields.addField();
                   this.setState({ alreadyAddedRecipeUsage: true });
                 }}

      />
    </div>;
  }
}

@connect(
  state => ({
    menuItems: state.get('menuItems'),
  }),
)
class RecipeUsage extends Component {

  render() {

    const { allUsedItems, menuItems, deleteRecipe, sectionOrdinal, ordinal, ...item } = this.props;
    let content;
    if (menuItems && menuItems.size) {
      const selectedItem = menuItems.find(i => i.get('id') == item.value);
      content = <Validate {...item}>
        <Select
            {...item}
            inputRef={select => this.initialInput = select}
            options={menuItems
                .filter(i => !i.get('tombstone'))
                .filter(i => i.get('id') == item.value || !allUsedItems.includes(i.get('id')))
                .map(i => ({
                  value: i.get('id'),
                  label: i.get('name'),
                  className: i.get('ingredient') ? 'text-italic' : null,
                })).toJS()}
            style={selectedItem && selectedItem.get('ingredient') ? { fontStyle: 'italic' } : null}
            searchIndexName={indexTypes.MENU_ITEMS}
        />
      </Validate>;
    } else {
      content = <LoadingSpinner />;
    }


    return <div className="menu-section-item" id={'menu-section-item-' + sectionOrdinal + '-' + ordinal}>
      <div className="menu-recipe-usage">
        <div className="menu-recipe-usage-select">
          {content}
        </div>
        <Glyphicon glyph="align-justify" className="drag-recipe-usage" />
        <span className="delete-link"
              onClick={deleteRecipe}>
              remove
            </span>
      </div>
      <hr className="well-child-hr" />
    </div>;
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }
}
