import React, { Component } from 'react';
import Immutable from 'immutable';

import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import formatNum from 'format-num';

import { Button, Grid } from 'react-bootstrap';
import { Link } from 'react-router';
import { history } from '../../navigation';

import store from '../../store';
import { csvQuote, downloadCSVDataAsFile } from '../../utils/file-utils';

import {
  getLocationList, getSupplierList, deleteSupplier, getSupplierDetails,
  getMeasures, getMultipleProductsUsageInfo, getCanSyncAllLocations, syncAllLocations,
  getOnboardingInfo,
} from '../../webapi/endpoints';
import {
  convertPerQuantityValue,
  unitDisplayName,
} from '../../utils/unit-conversions';
import { featureIsSupported } from '../../utils/features';
import Onboarding from '../Onboarding';

import UploadCsvModal from '../modals/UploadCsvModal';

import SearchableList, { searchableListLoadingSpinner } from './helpers/SearchableList';
import { pickAllComparisonUnits } from '../../utils/packaging';
import { actionCreators as UIStateActions } from '../../reducers/uiState/supplierList';
import { actionCreators as ImportReceivablesActions } from '../../reducers/importReceivablesPricingDetails';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';

export const supplierIdToName = (id, suppliers) => suppliers.find(s => s.get('id') === id, null, Immutable.Map()).get('name', 'Unknown Source');

@connect(
  state => ({
    suppliers: state.get('suppliers'),
    user: state.get('userInfo'),
    hasLocations: (state.get('locations') || Immutable.List()).size > 0,
    uiState: state.get('supplierList'),
  }),
  dispatch => ({
    uiActions: bindActionCreators(UIStateActions, dispatch),
    importReceivablesActions: bindActionCreators(ImportReceivablesActions, dispatch),
  }),
)
export default class SupplierList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      canSync: false
    };
  }

  render() {
    const {
      suppliers, user, hasLocations,
      uiState,
      uiActions: { openImportReceivablesPricingModal, closeImportReceivablesPricingModal },
      importReceivablesActions: { csvDetails },
    } = this.props;

    let content;
    if (!suppliers) {
      content = searchableListLoadingSpinner;
    } else {
      content = <div>
        {featureIsSupported('MULTI_LOCATION') && hasLocations ?
          <Button
            style={
              /* zIndex needs to be high enough for the button to be clickable
                and low enough to be below popup modals */
              { position: 'absolute', right: '510px', top: '12px', zIndex: 1020 }
            }
            disabled={!this.state.canSync}
            onClick={() =>
              syncAllLocations().then(() => this.setState({ canSync: false }))
            }
          >
            {this.state.canSync ? 'Sync' : 'Synced'}
          </Button>
        : null}
        <Button
          style={
            /* zIndex needs to be high enough for the button to be clickable
              and low enough to be below popup modals */
            { position: 'absolute', right: '385px', top: '12px', zIndex: 1020 }
          }
          download="parsley-users.csv"
          type="text/csv"
          onClick={() => downloadCSVData(suppliers.toJS())}
        >
          Export to CSV
        </Button>
        <FeatureGuardedButton
          feature="INVOICE_IMPORT"
          style={
            /* zIndex needs to be high enough for the button to be clickable
              and low enough to be below popup modals */
            { position: 'absolute', right: '180px', top: '12px', zIndex: 1020 }
          }
          onClick={openImportReceivablesPricingModal}
        >
          Import Receivables Pricing
        </FeatureGuardedButton>
        <SearchableList
          searchKey="suppliers"
          noun="Supplier"
          reduxStatePath="suppliers"

          baseUrl={this.props.location.pathname}

          deleteItems={ids => SearchableList.deleteItems(ids, deleteSupplier)}
          refreshItems={getSupplierList}
          checkable={false}
          taggable={false}
          copiable={false}
          shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}
        />
      </div>
    }

    return <Grid fluid style={{ position: 'relative' }}>
      {content}
      <Onboarding pageKey="supplier_list" />
      {
        uiState.get('showImportReceivablesPricingModal') ?
          <UploadCsvModal
            title="Import Receivables Pricing"
            onHide={closeImportReceivablesPricingModal}
            onUpload={(headers, data) => {
              csvDetails(headers, data);
              closeImportReceivablesPricingModal();
              history.push('import-receivables-pricing');
            }}
          />
      : null }
    </Grid>;
  }

  componentWillMount() {
    getSupplierList();
    getLocationList();
    getOnboardingInfo();
    getCanSyncAllLocations().then(canSync => this.setState({ canSync }));
  }
}

function formatCost(cost) {
  return formatNum(cost, {maxFraction: 2});
}

function sourceObjToCSVRow(supplierName, ingredientDetails, comparisonUnit, categoryColumnCnt, source) {

  const name = csvQuote(ingredientDetails.get('name'));

  const categories = [];
  for (let i = 0; i < categoryColumnCnt; i++) {
    categories.push(
        ingredientDetails.getIn(['ingredientTags', i])
            ? csvQuote(ingredientDetails.getIn(['ingredientTags', i, 'name']))
            : ''
    );
  }

  let superPackageName = source.subPackageName ? source.packageName + ' of' : '';
  let superPackageSize = superPackageName ? source.superPackageSize : '';

  let subPackageName = source.subPackageName || source.packageName;
  subPackageName = subPackageName ? subPackageName + ' of' : '';
  let subPackageSize = subPackageName ? source.packageSize : '';

  // If sub-package fields are empty -> Move super-oackage fields there
  if (!subPackageName && !subPackageSize && superPackageName && superPackageSize) {
    subPackageName = superPackageName;
    subPackageSize = superPackageSize;
    superPackageName = '';
    superPackageSize = '';
  }

  // Pure measurement: no size, measurement unit only
  if (!superPackageName && !subPackageName) superPackageSize = '';

  const conversions = ingredientDetails.get('measures');

  const unit = csvQuote(unitDisplayName(source.unit, conversions));
  const sku = source.sku || '';
  const cost = source.cost ? formatCost(source.cost) : '';

  const costComparisonUOM = unitDisplayName(comparisonUnit.get('id'), conversions);
  let costPerUOM = '';
  if (source.cost) { // package sizes are required
    costPerUOM = formatCost(convertPerQuantityValue(cost,
        source.packageSize * source.superPackageSize, source.unit,
        1, comparisonUnit.get('id'),
        conversions,
    ));
  };

  return [
    name,
    ...categories,
    csvQuote(supplierName),
    superPackageName,
    superPackageSize,
    subPackageName,
    subPackageSize,
    unit,
    sku,
    cost,
    costComparisonUOM,
    costPerUOM,
  ];
}

function downloadCSVData(suppliers) {

  function doDownload() {
    // awaiting late, so that we do all these in parallel
    const gettingAllSupplierDetails = Promise.all(
        suppliers.map(supplier => getSupplierDetails(supplier.id))
    );
    const gettingMeasures = getMeasures(); // kick this off early

    gettingAllSupplierDetails.then(supplierDetails => {

    const allSources = supplierDetails.flatMap(details => details.sources);
    return getMultipleProductsUsageInfo(allSources.map(s => s.product))
      // k/v pairs for Immutable.Map
    .then(deets => _.map(deets, (v, k) => [parseInt(k), Immutable.fromJS(v)]))
    .then(rawDetailedIngredients => {

    const detailedIngredients = Immutable.Map(rawDetailedIngredients);

    const categoryColumnCnt = detailedIngredients.valueSeq()
        .map(ingdt => ingdt.get('ingredientTags').size)
        .max();

    // needed for assorted things, but used straight from store
    return gettingMeasures.then(_ => {
    const allMeasures = store.getState().getIn(['measures', 'measures']);

    const comparisonUnits = pickAllComparisonUnits(
        allSources,
        detailedIngredients.map(deets => deets.get('measures')),
        allMeasures,
    );

    const suppliedIngredients = supplierDetails.flatMap(details =>
        details.sources.map(source => sourceObjToCSVRow(
            details.name,
            detailedIngredients.get(source.product),
            comparisonUnits[source.product],
            categoryColumnCnt,
            source
        ))
    );

    let columnHeaders = ['Ingredient'];
    for (let i = 1; i <= categoryColumnCnt; i++) columnHeaders.push('Category ' + i);
    columnHeaders.push(
        'Supplier',
        'Super-Package Type',
        'Package Quantity',
        'Package Type',
        'Quantity',
        'Unit Of Measure',
        'SKU',
        'Cost per package/UOM',
        'Cost Comparision Unit',
        'Cost per Comparison Unit'
    );
    const header = columnHeaders.join(',');
    const csv = [header, ...suppliedIngredients].join('\n');

    downloadCSVDataAsFile('parsley-supplied-ingredients.csv', csv);
  })})})};

  return doDownload(); // kick off the async future in the background
}
