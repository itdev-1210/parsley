import React, { Component } from "react";
import { connect } from 'react-redux';

import { Grid } from "react-bootstrap";

import { getMenuList, deleteMenu, getRecipeList, getOnboardingInfo } from "../../webapi/endpoints";
import Onboarding from '../Onboarding';
import SearchableList, { searchableListLoadingSpinner } from './helpers/SearchableList';

@connect(
    state => ({
      menus: state.get('menus'),
      recipes: state.get('recipes'),
      onboardingInfo: state.get('onboardingInfo'),
      shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
    })
)
export default class MenuList extends Component {
  render() {
    const {
      menus, recipes,
    } = this.props;

    let content;
    if (!menus) {
      content = searchableListLoadingSpinner;
    } else {
      content = <SearchableList
          searchKey="menus"
          noun="Menu"
          reduxStatePath="menus"

          baseUrl={this.props.location.pathname}

          deleteItems={ids => SearchableList.deleteItems(ids, deleteMenu)}
          refreshItems={getMenuList}
          shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}
          checkable={true}
          taggable={false}
          copiable={true}
      />
    }
    return <Grid fluid>
      {content}
      <Onboarding pageKey="menu_list" />
    </Grid>;
  }

  componentWillMount() {
    getOnboardingInfo();
    getMenuList().then(menus =>
      menus.length == 0
          // we're going to need to know whether to display the "make a menu" or "make recipes first" text
          ? getRecipeList()
          : null
    );
  }
}
