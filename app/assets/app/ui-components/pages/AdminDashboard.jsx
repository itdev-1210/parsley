import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';

import { Link } from 'react-router';

export default class AdminDashboard extends Component {

  render() {
    return <Grid fluid>
      <h1>Admin Tools</h1>
      <p>
        <Link to={this.props.location.pathname + "/measures"}>
          Edit Measures
        </Link>
      </p>
      <p>
        <Link to={this.props.location.pathname + "/nutrients"}>
          Edit Nutrients
        </Link>
      </p>
      <p>
        <Link to={this.props.location.pathname + "/users"}>
          View Users
        </Link>
      </p>
      <p>
        <Link to={this.props.location.pathname + "/onboardings"}>
          Edit Onboarding
        </Link>
      </p>
       <p>
        <Link to={this.props.location.pathname + "/onboardingsWidgets"}>
          Edit Default Widgets Message
        </Link>
      </p>
    </Grid>;
  }
}