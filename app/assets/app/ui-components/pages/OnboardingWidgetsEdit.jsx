import React from 'react';

import _ from 'lodash';

import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { reduxForm } from 'redux-form';
import { history } from '../../navigation';

import { Row, Col, FormControl } from 'react-bootstrap';

import CrudEditor from './helpers/CrudEditor';

import TitleInput from '../TitleInput';
import Select from '../Select';
import OnboardingTooltip from '../OnboardingTooltip';

import Validate from 'common/ui-components/Validate';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import { actionCreators as OnboardingActions } from '../../reducers/onboardingInfo';

import { getOnboardingWidgetInfo, updateOnboardingWidgetInfo } from '../../webapi/endpoints';
import { readFileAsArrayBUffer, arrayBufferToBase64 } from '../../utils/file-utils';
import {
  valueFromEvent,
  convertFormValuesToOnboardingJson,
  onboardingStepFields,
  shallowToObject,
} from '../../utils/form-utils';
import { checkFields, optional, required } from '../../utils/validation-utils';

const validate = values => {
  const validators = {
    id: required(),
    message: required(),
  };

  return checkFields(values, validators);
};

@withRouter
@reduxForm(
  {
    form: 'onboarding_edit',
    fields: [
      'id',
      'message',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
  },
  (state, ownProps) => {
    const onboardingWidgetsInfo = state.get('onboardingWidgetsInfo');
    const values = onboardingWidgetsInfo && onboardingWidgetsInfo.get('widgets').find(s => s.get('id') === CrudEditor.sourceId(ownProps.params)).toJS();
    let initialValues;
    if (values) {
      initialValues = {
        id: values.id,
        message: values.customMessage || values.defaultMessage,
      };
    }
    return {
      mainInfoLoaded: onboardingWidgetsInfo,
      initialValues,
    };
  },
  dispatch => ({
    onboardingActions: bindActionCreators(OnboardingActions, dispatch),
  }),
)
export default class OnboardingWidgetsEdit extends CrudEditor {

  handleSave = (id, values) => {
    const { fields, params, mainInfoLoaded } = this.props;
    const data = shallowToObject(fields);
    const widgetInfo = mainInfoLoaded.get('widgets').find(s => s.get('id') === data.id);
    if (widgetInfo) {
      data.name = widgetInfo.get('name');
      data.defaultMessage = widgetInfo.get('defaultMessage');
      data.customMessage = data.message;
    }
    return updateOnboardingWidgetInfo(data)
      .then(() => {
        if (CrudEditor.isCreatingNew(params)) {
          return Promise.resolve();
        }
      })
      .then(() => history.push('/admin/onboardingsWidgets'));
  }

  deleteItem = (value) => {
  }

  componentWillMount() {
    const { mainInfoLoaded } = this.props;
    if (!mainInfoLoaded) {
      getOnboardingWidgetInfo();
    }
  }

  renderBody() {
    const { mainInfoLoaded } = this.props;
    const {
      id,
      message,
    } = this.props.fields;

    if (!mainInfoLoaded) {
      return <LoadingSpinner />;
    }

    const availableWidgets = mainInfoLoaded.get('widgets')
        .sortBy(p => p.get('id')).map(p => ({
          value: p.get('id'),
          label: p.get('name'),
        })).toJS();

    return <div className="onboarding-step-editor-container">
      <Row>
        <Col xs={12} md={6}>
          Widget: &nbsp;
          <Validate {...id}>
            <Select
              {...id}
              autoSize
              options={availableWidgets}
              onChange={value => {
                const widget = mainInfoLoaded.get('widgets').find(s => s.get('id') === value);
                if (widget) {
                  message.onChange(widget.get('customMessage') || widget.get('defaultMessage'));
                }
                id.onChange(value);
              }}
            />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Message: &nbsp;
          <Validate {...message}>
            <FormControl componentClass="textarea" placeholder="Add description ..." {...message} />
          </Validate>
        </Col>
      </Row>
    </div>;
  }

  listUrl = '/admin/onboardingsWidgets';
}