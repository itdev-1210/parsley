// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import moment from 'moment';
import _ from 'lodash';

import { deepOmit, sanitizePrice } from '../../utils/general-algorithms';
import SupplierImportIngredients from '../SupplierImportIngredients';
import { history } from '../../navigation';
import { getSupplierList, getPosImportPreferences,
  getMeasures, getIngredientList, getMultipleSuppliersDetails } from '../../webapi/endpoints';
import { checkFields, isBlank, required, optional } from '../../utils/validation-utils';
import type { ReactRouterProps } from 'common/utils/routing';

import { shallowToObject, importReceivablesPricingFields, commonSourceFormFields } from '../../utils/form-utils';
import AddSupplierToImportReceivablesModal from '../modals/AddSupplierToImportReceivablesModal';
import NewIngredientModal from '../modals/NewIngredientModal';
import NewUnitModal from '../modals/NewUnitModal';

import { actionCreators as UIStateActions } from '../../reducers/uiState/importReceivablesPricing';
import { actionCreators as UISupplierActions } from '../../reducers/uiState/supplierEdit';
import { PosImportRow as ImportRow } from './PosImport';

function validate(values) {
  const posValidators = {
    supplierNameHeader: required(),
    dateHeader: required(),
    ingredientNameHeader: required(),
    priceHeader: required(),
    skuHeader: optional(),
    readerSkuHeader: optional(),
    purchasingPackageHeader: optional(),
    pricingUnitHeader: optional(),
  };
  return checkFields(values, posValidators);
}

type Props = {
  fields: any,
  uiActions: any,
  uiState: Immutable.Map<string, any>,
  fields: any,
  suppliers: Immutable.List<Immutable.Map<string, any>>,
  dirty: boolean,
  handleSubmit: Function,
  details: Immutable.Map<string, any>,
  uiSupplierState: Immutable.Map<string, any>,
  uiSupplierActions: any,
  allIngredients: Immutable.List<Immutable.Map<string, any>>,
  allMeasures: Immutable.List<Immutable.Map<string, any>>,
  userVolumePref: boolean,
  userWeightPref: boolean,
  percentImportUpdate: number,
  onSaveSuppliers: Function,
  onFinishImport: Function,
  title: string,
} & ReactRouterProps

type State = {
  data: Array<*>,
  uploaded: boolean,
  formattedData: Array<*>,
}

@withRouter
@reduxForm(
    {
      form: 'import-receivables-pricing',
      fields: [
        ...importReceivablesPricingFields,
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => {
      const { details } = ownProps;
      const posImportPreferences = state.get('posImportPreferences');
      let initialValues = {};

      if (details && posImportPreferences && posImportPreferences.size > 0) {
        const preferenceHeaders = posImportPreferences.get(0);
        const csvHeaders = details.get('headers');
        importReceivablesPricingFields.forEach(field => {
          if (csvHeaders.includes(preferenceHeaders.get(field)))
            initialValues[field] = preferenceHeaders.get(field);
        });
      }

      return {
        suppliers: state.get('suppliers'),
        details,
        uiState: state.get('importReceivablesPricing'),
        uiSupplierState: state.get('supplierEdit'),
        allMeasures: state.getIn(['measures', 'measures']),
        allIngredients: state.get('ingredients'),
        userVolumePref: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        userWeightPref: state.getIn(['userInfo', 'purchaseWeightSystem']),
        percentImportUpdate: state.getIn(['userInfo', 'sourcePriceChangeWarningFraction']),
        initialValues,
      };
    },
    dispatch => ({
      uiActions: bindActionCreators(UIStateActions, dispatch),
      uiSupplierActions: bindActionCreators(UISupplierActions, dispatch),
    }),
)
export default class ImportReceivablesPricing extends Component<Props, State> {

  clearRouteLeaveHook: any
  exitingSuppliers: Array<*>
  unknowSuppliers: Array<*>
  onSubmitFn: Function

  constructor(props: Props) {
    super(props);
    this.state = {
      headers: [],
      data: [],
      loaded: false,
      formattedData: [],
      uploaded: false,
    };

    this.uploadData = this.uploadData.bind(this);
    this.prepareData = this.prepareData.bind(this);
    this.onSubmitFn = () => {};
  }

  save = () => {
    if (this.onSubmitFn)
      return this.onSubmitFn()
        .then(success => {
          this.clearRouteLeaveHook();
          this.props.onFinishImport();
        },
        e => {
          if (e.reduxFormErrors) {
            return Promise.reject(e.reduxFormErrors);
          } else {
            console.log(e.toString());
            console.log(e.stack);
            return Promise.reject({ _error: e.message });
          }
        });
  }

  prepareData = () => {
    const { suppliers, fields, details, uiActions: { openAddSupplierModal } } = this.props;
    const { supplierNameHeader, dateHeader, ingredientNameHeader, priceHeader, skuHeader,
      readerSkuHeader, purchasingPackageHeader, pricingUnitHeader } = shallowToObject(fields);

    const normalizeSupplierName = (supplierName: string) => supplierName
        .toLocaleLowerCase() // matches should be case-insensitive
        .trim(); // matches should ignore leading/trailing whitespace, which is common in buggy CSV export implementations

    const findSupplierByName = (supplierName: string) => suppliers.find(r =>
        r.get('name') &&
        // $FlowFixMe: only with records can we assert member existence/value
        normalizeSupplierName(r.get('name')) === normalizeSupplierName(supplierName),
    );

    const data = details.get('data', Immutable.List())
        .map((d, index) => {
          const supplier = findSupplierByName(d.get(supplierNameHeader));
          return {
            rowNumber: index + 2,
            supplierId: supplier && supplier.get('id'),
            supplierName: d.get(supplierNameHeader) && d.get(supplierNameHeader).trim(),
            date: moment(d.get(dateHeader)).format(),
            name: d.get(ingredientNameHeader),
            sku: d.get(skuHeader),
            readerSku: d.get(readerSkuHeader),
            purchasepackage: d.get(purchasingPackageHeader),
            pricingunit: d.get(pricingUnitHeader),
            sanitizedPrice: sanitizePrice(d.get(priceHeader) || 0),
          };
        });

    const [newSupplierRows, existingSupplierRows] = _.partition(data.toJS(), d => !d.supplierId);
    this.exitingSuppliers = existingSupplierRows;
    this.unknowSuppliers = newSupplierRows;

    if (newSupplierRows.length > 0)
      openAddSupplierModal();
  }

  uploadData = (newSupplierRows: Array<*>) => {
    const { uiActions: { closeAddSupplierModal } } = this.props;
    closeAddSupplierModal();
    const sourcesToShow = _([...this.exitingSuppliers, ...newSupplierRows])
        .sortBy(d => [d.supplierId, d.rowNumber]).value();
    this.setState({
      formattedData: sourcesToShow,
      uploaded: true,
    });
  }

  componentDidMount() {
    const { router, route, details } = this.props;
    if (!details) history.push('/suppliers');
    getPosImportPreferences();
    getSupplierList();
    getMeasures();
    getIngredientList();
    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
  }

  render() {
    const { uploaded, formattedData } = this.state;
    const { fields, handleSubmit, details, uiState, suppliers, uiActions, onFinishImport,
      uiSupplierState, uiSupplierActions, title, ...rest } = this.props;

    if (!details) return null;

    const { supplierNameHeader, dateHeader, ingredientNameHeader, priceHeader,
       skuHeader, readerSkuHeader, purchasingPackageHeader, pricingUnitHeader } = fields;
    const selectedHeaders = [supplierNameHeader, dateHeader, ingredientNameHeader, priceHeader,
        skuHeader, readerSkuHeader, purchasingPackageHeader, pricingUnitHeader]
          .map(i => i.value).filter(h => h !== '');
    // $FlowFixMe: only with records can we assert member existence/value
    const headersOption = details.get('headers')
        .filter(h => !selectedHeaders.includes(h))
        .map(h => ({ value: h, label: h }));
    const placeholder = 'Select Column';

    return <Grid fluid style={{ position: 'relative' }}>
      <div>
        <Row>
          <Col md={2} xs={12}/>
          <Col md={10} xs={12}>
            <div style={{ width: '100%' }}>
              <Row >
                <Col className="hidden-xs title-block" xs={6}>
                  <h3 className="list-title" style={{ marginBottom: '38px' }}>{title}</h3>
                </Col>
                <Col xs={6} sm={4} md={6} className="crud-top-buttons">
                  <span>
                    <Button bsStyle="primary" onClick={this.save} disabled={!uploaded}>SAVE</Button>
                    <Button bsStyle="primary" onClick={onFinishImport}>EXIT</Button>
                  </span>
                </Col>
              </Row>
              <div>
                  <div className="pos-import-container">
                    <Row>
                      <Col md={4} xs={12}>
                        <h5>Data</h5>
                      </Col>
                      <Col md={6} xs={12}>
                        <h5>Column Name</h5>
                      </Col>
                    </Row>
                      <div>
                        <ImportRow
                          key="supplierNameRow"
                          label="Supplier Name"
                          item={supplierNameHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="dateRow"
                          label="Date"
                          item={dateHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="ingredientNameRow"
                          label="Ingredient Name"
                          item={ingredientNameHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                         <ImportRow
                          key="ingredientPriceRow"
                          label="Price"
                          item={priceHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="skuRow"
                          label="SKU"
                          item={skuHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="readerSkuRow"
                          label="Reader's SKU"
                          item={readerSkuHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="purchasingPackageRow"
                          label="Purchasing Package"
                          item={purchasingPackageHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                        <ImportRow
                          key="pricingUnitRow"
                          label="Pricing Unit"
                          item={pricingUnitHeader}
                          options={headersOption}
                          placeholder={placeholder}
                        />
                      </div>
                  </div>
                {
                  uploaded ?
                  <div>
                    {/* $FlowFixMe: can't simulate extra props passed in by decorators */}
                    <DataImportedEditor
                      {...rest}
                      csvRows={formattedData}
                      uiState={uiState}
                      uiActions={uiActions}
                      uiSupplierState={uiSupplierState}
                      uiSupplierActions={uiSupplierActions}
                      onSubmitData={onSubmitFn => this.onSubmitFn = onSubmitFn}
                    />
                  </div>
                  : null
                }
              </div>
              <Row>
                <Col xs={12} className="crud-top-buttons" style={{ marginBottom: '15px' }}>
                  <span>
                    <Button bsStyle="primary" disabled={uploaded} onClick={handleSubmit(this.prepareData.bind(this))}>Start Import</Button>
                    <Button bsStyle="primary" disabled={!uploaded} onClick={this.save}>SAVE</Button>
                  </span>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
      {
        uiState.get('showAddSupplier') &&
        <div>
          <AddSupplierToImportReceivablesModal
            suppliers={suppliers}
            unknowSupplierRows={this.unknowSuppliers || []}
            hide={this.uploadData.bind(this)}
            uiActions={uiActions}
            uiState={uiState}
          />
        </div>
      }
      { uiSupplierState.get('showNewIngredientModal') ?
        <NewIngredientModal key="new-ingredient-modal"
            newIngredient={uiSupplierState.get('showNewIngredientModal')}
            uiStateName="supplierEdit"
            hide={uiSupplierActions.closeNewIngredientModal}
            showSuppliersAndCost={false}
        />
        : null }
      { uiSupplierState.get('showNewUnitModal') ?
        <NewUnitModal key="new-unit-modal"
            newUnit={uiSupplierState.get('showNewUnitModal')}
            uiStateName="supplierEdit"
            hide={uiSupplierActions.closeNewUnitModal}
        />
        : null }
    </Grid>;
  }
}

type DataImportedEditorProps = {
  csvRows: Array<*>,
  values: Object,
  fields: any,
  allIngredients: Immutable.List<Immutable.Map<string, any>>,
  allMeasures: Immutable.List<Immutable.Map<string, any>>,
  userVolumePref: boolean,
  userWeightPref: boolean,
  percentImportUpdate: number,
  uiSupplierState: Immutable.Map<string, any>,
  uiSupplierActions: any,
  onSubmitData: Function,
  onSaveSuppliers: Function,
}


type DataImportedEditorState = {
  currentSupplierId: number,
  currentSupplierIndex: number,
  lastSavedSupplierId?: number,
  finishImport: boolean,
}

@reduxForm(
  {
    form: 'import_receivables_data_form',
    fields: [
      'suppliers[].id',
       ...commonSourceFormFields.map(f => `suppliers[].sources[].${f}`),
      'suppliers[].sources[].product',
      'suppliers[].sources[].supplierName',
      'suppliers[].sources[].date',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const suppliersMap = state.get('detailedSuppliers');
    let suppliers = Immutable.List();
    if (suppliersMap) {
      suppliers = suppliersMap.entrySeq().map(([supplierId, supplierInfo]) => (
        {
          id: supplierId,
          sources: [...supplierInfo.get('sources').toJS()],
        }
      )).toJS();
    }
    return {
      initialValues: { suppliers },
    };
  },
)
class DataImportedEditor extends Component<DataImportedEditorProps, DataImportedEditorState> {

  constructor(props) {
    super(props);
    const currentSupplierId = props.csvRows.length > 0 ? props.csvRows[0].supplierId : -1;
    this.state = {
      finishImport: false,
      currentSupplierId,
      currentSupplierIndex: 0,
      lastSavedSupplierId: undefined,
    };
    this.saveCurrentSupplier = this.saveCurrentSupplier.bind(this);
    this.nextSupplier = this.nextSupplier.bind(this);
  }

  saveCurrentSupplier = () => {
    const { currentSupplierId, lastSavedSupplierId } = this.state;
    const { values, onSaveSuppliers } = this.props;
    if (lastSavedSupplierId !== currentSupplierId) {
      const supplier = values.suppliers.find(s => s.id === currentSupplierId);
      return onSaveSuppliers(deepOmit(supplier, isBlank));
    } else {
      return Promise.resolve();
    }
  }

  nextSupplier = () => {
    const { suppliers } = this.props.fields;
    const { currentSupplierIndex, currentSupplierId } = this.state;
    this.saveCurrentSupplier()
      .then(() => {
        const nextSupplierIndex = currentSupplierIndex + 1;
        if (nextSupplierIndex < suppliers.length) {
          const nextSupplier = suppliers[nextSupplierIndex];
          this.setState({
            finishImport: false,
            currentSupplierId: nextSupplier.id.value,
            currentSupplierIndex: nextSupplierIndex,
            lastSavedSupplierId: currentSupplierId,
          });
        } else {
          this.setState({
            lastSavedSupplierId: currentSupplierId,
          });
        }
      });
  }

  render() {
    const { fields, csvRows, ...rest } = this.props;
    const { suppliers } = fields;
    const { currentSupplierId } = this.state;
    const supplier = suppliers.find(s => s.id.value === currentSupplierId);
    if (!supplier)
      return null;
    const supplierId = supplier.id.value;
    const sources = supplier.sources;
    const ingredients = csvRows.filter(row => row.supplierId === currentSupplierId);
    return <div>
      {/* $FlowFixMe: can't simulate extra props passed in by decorators */}
      <DataImportedForm
        key={supplierId}
        {...rest}
        supplierId={supplierId}
        sources={sources}
        ingredients={ingredients}
        handleFinish={() => this.setState({ finishImport: true })}
      />
    </div>;
  }

  componentDidUpdate(prevProps, prepState) {
    const { finishImport } = this.state;
    if (finishImport && !prepState.finishImport) {
      this.nextSupplier();
    }
  }

  componentDidMount() {
    const { csvRows, onSubmitData } = this.props;
    onSubmitData(this.saveCurrentSupplier);
    // $FlowFixMe: no fromJS() typing
    const ids = Immutable.fromJS(csvRows.map(c => c.supplierId)).toSet();
    getMultipleSuppliersDetails(ids);
  }
}


type DataImportedFormProps = {
  ingredients: Array<*>,
  supplierId: number,
  sources: any,
  allIngredients: Immutable.List<Immutable.Map<string, any>>,
  allMeasures: Immutable.List<Immutable.Map<string, any>>,
  userVolumePref: boolean,
  userWeightPref: boolean,
  percentImportUpdate: number,
  uiSupplierState: Immutable.Map<string, any>,
  uiSupplierActions: any,
  handleFinish: Function,
}

class DataImportedForm extends Component<DataImportedFormProps> {

  handleAutoSkip = (element) => {
    const { sources } = this.props;
    const sourceByIngredientName = sources.find(s => s.date.value && s.supplierIngredientName.value === element.name);
    if (sourceByIngredientName) {
      const sourcesValues = shallowToObject(sourceByIngredientName);
      sources.addField({
        ...sourcesValues,
        date: element.date,
        cost: element.price,
      });
    }
  }

  render() {
    const { ingredients, supplierId, sources, allIngredients, allMeasures, handleFinish,
      uiSupplierState, uiSupplierActions, userVolumePref, userWeightPref, percentImportUpdate } = this.props;

    return <div>
      <SupplierImportIngredients
        key={supplierId}
        ingredients={ingredients}
        handleNextIngredient={(nextElement) => {}}
        handleReceivablesAutoSkip={this.handleAutoSkip.bind(this)}
        handleFinish={handleFinish}
        uiState={uiSupplierState}
        uiStateActions={uiSupplierActions}
        sources={sources}
        allIngredients={allIngredients}
        supplierId={supplierId}
        showingNewIngredientModal={uiSupplierState.get('showNewIngredientModal')}
        userVolumePref={userVolumePref}
        userWeightPref={userWeightPref}
        untouch={false}
        allMeasures={allMeasures}
        openEditIngredientModal={() => {}}
        percentImportUpdate={percentImportUpdate}
        getCurrentFieldsFn={() => {}}
        importingReceivables
        isImportReceivables
      />
    </div>;
  }
}