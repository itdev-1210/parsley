import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Modal, Table, FormControl, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { history } from '../../navigation';
import { getTagsList, saveCategories, getOnboardingInfo } from '../../webapi/endpoints';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Checkbox from '../Checkbox';
import AddButton from '../AddButton';
import Validate from 'common/ui-components/Validate';
import { get } from 'common/webapi/utils';
import _, { some } from 'lodash';
import Immutable from 'immutable';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import { withRouter } from 'react-router';

import { allTagTypes } from '../../utils/tags';
import Onboarding from '../Onboarding';


export function IngredientCategoryEditor(props) {
  return <CategoryEditor tagType="ingredients" {...props} />;
}

export function InventoryCategoryEditor(props) {
  return <CategoryEditor tagType="inventories" {...props} />;
}

export function RecipeCategoryEditor(props) {
  return <CategoryEditor tagType="recipes" {...props} />;
}

function validate(values) {
  const errors = { tags: [] };

  const duplicatedNames = _(values.tags)
    .filter(t => t.newTag && !t.removed)
    .countBy(t => t.newTag)
    .pickBy(count => count > 1).value();

  values.tags.forEach(t => {
    if (t.removed) {
      errors.tags.push(undefined);
    } else if (!t.newTag) {
      errors.tags.push({ newTag: 'Required' });
    } else if (duplicatedNames[t.newTag]) {
      errors.tags.push({ newTag: 'Duplicated Name' });
    } else {
      errors.tags.push(undefined);
    }
  });

  return errors;
}

@reduxForm(
  {
    form: 'category_edit',
    fields: [
      'tags[].oldTag',
      'tags[].newTag',
      'tags[].removed',
      'tags[].merged',
    ],
    enableReinitialize: true,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
    asyncBlurFields: ['name'],
  },
  (state, ownProps) => {
    const tags = state.getIn(['tags', ownProps.tagType]);
    return {
      tags,
      initialValues: {
        tags: tags ? tags.sort((tag1, tag2) => tag1.get('name').localeCompare(tag2.get('name'))).map(tag => ({
          oldTag: tag.get('name'),
          newTag: tag.get('name'),
          removed: false,
        })).toJS() : [],
      },
    };
  }
)
class CategoryEditor extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      toDeleteCategories: Immutable.Map(),
      checkedTags: Immutable.Map(),
      showMergeModal: false,
      showDeleteCategoriesWarningModal: false,
    };
    ['save', 'cancel', 'hideDeleteCategoriesWarningModal', 'submit', 'handleAdd', 'handleDelete', 'handleMerge', 'hideMergeModal', 'toggleTag', 'mergeTag'].forEach(funcName => {
      this[funcName] = this[funcName].bind(this);
    });
  }

  submit(values) {
    // At this point, all tags have correct oldTag/newTag
    // But since we can't remove removed tags completely from the form to support the old backend logic
    // There are still cases where we have to clean our tags, before sending them to the API

    // Remove all empty tag rows (get removed/merged after being added)
    values.tags = values.tags.filter(t => !(t.oldTag === '' && t.removed));
    // Remove dummy rows used to represent merges
    values.tags = values.tags.filter(t => !t.merged);

    // Don't remove tags that were re-added after being removed
    // Eg: In our database we have a tag called "dry"
    // -> at the beginning we have a tag: "dry" -> "dry"
    // When we delete it the tag becomes "dry" -> ""
    // But if we add a new tag and name it dry, then we have both "dry" -> "" and "" -> "dry" at the same time -> no change should be made
    const removedOldTags = {};
    values.tags.forEach((tag, idx) => {
      if (tag.removed) removedOldTags[tag.oldTag] = idx;
    });
    values.tags.forEach(tag => {
      if (tag && removedOldTags[tag.newTag] !== undefined) {
        values.tags[removedOldTags[tag.newTag]] = null;
      }
    });
    values.tags = values.tags.filter(t => t !== null);

    // Final clean before send
    values.tags = values.tags.map(t => {
      delete t.removed;
      delete t.merged;
      return t;
    });

    return saveCategories(this.props.tagType, values).then(() => {
      this.clearRouteLeaveHook();
      history.push('/edit_categories');
    });
  }

  cancel() {
    this.clearRouteLeaveHook();
    history.push('/edit_categories');
  }

  handleAdd() {
    this.props.fields.tags.addField({
      oldTag: '',
      newTag: '',
      removed: false,
      merged: false,
    });
  }

  // Apart from removing the exact tag, we have to remove all tags that were supposed to change to the tag also
  // Eg: We merge 3 existed tag into one new tag, then delete this new tag -> all 3 existed should be removed
  // With the old logic, we have to set a tag to an empty string to remove it (replace the tag in a product/recipe's tag column with an empty string)
  // Therefore we can't just remove the tag from the form here
  // To keep this delete logic simple, we don't remove new tags that get deleted before save here, invalid tags will be filtered out in submit
  handleDelete(newTag) {

    let toDeleteCategories = this.state.toDeleteCategories;

    this.props.fields.tags.forEach(tag => {
      if (tag.newTag.value === newTag) {
        tag.removed.onChange(true);
        tag.newTag.onChange('');

        if (tag.oldTag.value) {
          toDeleteCategories = toDeleteCategories.set(tag.oldTag.value, true);
        }
      }
    });

    this.setState({ toDeleteCategories });
  }

  handleMerge() {
    this.setState({
      showMergeModal: true,
    });
  }

  hideMergeModal() {
    this.setState({
      showMergeModal: false,
    });
  }

  toggleTag(idx, newTag) {
    let checkedTags = this.state.checkedTags;
    if (checkedTags.has(newTag)) {
      checkedTags = checkedTags.delete(newTag);
    } else {
      checkedTags = checkedTags.set(newTag, idx);
    }
    this.setState({ checkedTags });
  }

  // With the old logic, we have to set a tag to its new tag to merge it (replace the tag in a product/recipe's tag column with the new tag)
  // Therefore we can't just remove the tag from the form here
  // To keep this merge logic simple, we don't remove new tags that get merged before save here, invalid tags will be filtered out in submit
  mergeTag(newTagName) {

    let toDeleteCategories = this.state.toDeleteCategories;

    this.state.checkedTags.forEach(idx => {
      const tag = this.props.fields.tags[idx];
      tag.removed.onChange(true);
      tag.newTag.onChange(newTagName);

      if (tag.oldTag.value) {
        toDeleteCategories = toDeleteCategories.set(tag.oldTag.value, true);
      }
    });

    this.props.fields.tags.addField({
      // just here for UI reasons; backend will work off of the newTag entries
      // in the other tags
      oldTag: '',
      newTag: newTagName,
      removed: false,
      merged: true,
    });

    this.setState({
      checkedTags: Immutable.Map(),
      toDeleteCategories,
    });
  }

  hideDeleteCategoriesWarningModal() {
    this.setState({
      showDeleteCategoriesWarningModal: false,
    });
  }

  save(evt) {

    // BELOW: Show a warning dialog if had deleted a tag
    // let toDeleteCategories = this.state.toDeleteCategories;
    // // skip tags that were first deleted but later on re-added
    // this.props.fields.tags.forEach(tag => {
    //   toDeleteCategories = toDeleteCategories.delete(tag.newTag.value);
    // });
    //
    // if (toDeleteCategories.size > 0) {
    //   this.setState({
    //     showDeleteCategoriesWarningModal: true
    //   });
    // } else {
    //   this.props.handleSubmit(this.submit)(evt);
    // }

    this.props.handleSubmit(this.submit)(evt);
  }

  getFormButtons(havingErrors) {
    const { submitting } = this.props;
    return <div style={{ padding: '15px 0px' }} className="pull-right">
              <Button disabled={submitting || havingErrors}
                onClick={this.save}
                bsStyle={submitting ? 'default' : 'primary'}
                className="green-btn">
                { submitting ? 'saving' : 'save'}
              </Button>
              <Button onClick={this.cancel} className="btn-danger" style={{ marginLeft: '12px' }}>Cancel</Button>
            </div>;
  }

  render() {

    const { tags } = this.props.fields;

    const havingErrors = some(tags, t => !t.newTag.valid);
    const { tagType } = this.props;

    return <div className="account-settings">
        <div style={{ paddingBottom: '12px' }}>
          <h3>{allTagTypes[this.props.tagType].pluralName}</h3>
          <Button style={{ margin: '15px 0px' }} onClick={this.handleMerge} disabled={havingErrors || this.state.checkedTags.size <= 1}>Merge Selected</Button>
          {this.getFormButtons(havingErrors)}
        </div>
        {!tags ? <LoadingSpinner/> :
          <div>
            <Table style={{ marginBottom: '0px' }}>
              <tbody>
                {tags.map((t, idx) => t.removed.value ? null :
                  <TagRow
                    key={idx}
                    tag={t}
                    handleDelete={() => this.handleDelete(t.newTag.value)}
                    toggleTag={() => this.toggleTag(idx, t.newTag.value)}
                  />
                )}
              </tbody>
            </Table>
            <AddButton onClick={this.handleAdd}/>
            {this.getFormButtons(havingErrors)}
            {this.state.showMergeModal ?
              <MergeCategoriesModal
                hide={this.hideMergeModal}
                tags={this.state.checkedTags}
                mergeTag={this.mergeTag}
              />
            : null}
            {this.state.showDeleteCategoriesWarningModal ?
              <ConfirmDeleteCategoriesModal
                hide={this.hideDeleteCategoriesWarningModal}
                tags={this.state.toDeleteCategories}
                save={this.props.handleSubmit(this.submit)}
              />
            : null}
          </div>
        }
        <Onboarding pageKey={tagType === 'ingredients' ? 'ingredient_categories' : 'recipe_categories'} />
      </div>;
  }

  componentWillMount () {
    const { router, route, tagType } = this.props;

    getTagsList(tagType);
    getOnboardingInfo();

    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
  }
}

class TagRow extends Component {
  render() {

    const { tag, handleDelete, toggleTag } = this.props;

    return <tr>
        <td>
          {tag.newTag.value ? <Checkbox onChange={toggleTag}/> : null}
        </td>
        <td>
          <Validate {...tag.newTag}>
            <FormControl {...tag.newTag}/>
          </Validate>
        </td>
        <td>
          <span onClick={handleDelete} className="pull-right list-item-delete-link">
            delete
          </span>
        </td>
      </tr>;
  }
}

class MergeCategoriesModal extends Component {

  constructor(props) {
    super(props);
    this.state = { canSave: false };
    this.save = this.save.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  save() {
    this.props.mergeTag(this.input.value);
    this.props.hide();
  }

  handleChange() {
    this.setState({
      canSave: Boolean(this.input.value),
    });
  }

  render() {

    const { hide, tags } = this.props;

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Merge Selected Categories</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>The following categories will be merged:</p>
          <ListGroup className="green-barred-list-group" style={{ fontWeight: 'bold' }}>
            {tags.map((value, key) =>
              <ListGroupItem key={key}>{key}</ListGroupItem>
            ).toList().toJS()}
          </ListGroup>
          <p>Please enter a new name for the merged categories:</p>
          <div><FormControl inputRef={ref => { this.input = ref; }} onChange={this.handleChange}/></div>
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            <Button bsStyle="primary" onClick={this.save} disabled={!this.state.canSave}>Save</Button>
            <Button onClick={hide} className="btn-danger" style={{ marginLeft: '12px' }}>Cancel</Button>
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }

  componentDidMount() {
    if (this.input) {
      this.input.focus();
    }
  }
}

class ConfirmDeleteCategoriesModal extends React.Component {

  render() {

    const { hide, save, tags } = this.props;

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>The categories</p>
          <ListGroup className="green-barred-list-group" style={{ fontWeight: 'bold' }}>
            {tags.map((value, key) =>
              <ListGroupItem key={key}>{key}</ListGroupItem>
            )}
          </ListGroup>
          <p>will be deleted, are you sure?</p>
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            <Button bsStyle="primary" onClick={() => { hide(); save(); }}>Save</Button>
            <Button onClick={hide} className="btn-danger" style={{ marginLeft: '12px' }}>Cancel</Button>
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }
}

type Props = {
  router: any,
};

@withRouter
export class EditCategories extends Component<Props> {
  render() {
    const { router } = this.props;
    const categoriesButtonStyle = { 'display': 'block', 'width': '180px', 'margin': '2em' };
    return <div className="account-settings">
      <div style={{ paddingBottom: '12px' }}>
        <h3>Select Category Type to Edit</h3>
      </div>
      <FeatureGuardedButton
        key="ingredient-categories"
        style={categoriesButtonStyle}
        href="/ingredient_categories"
        onClick={ev => {
          router.push('/ingredient_categories');
          ev.preventDefault();
        }}
      >
      GL Categories
      </FeatureGuardedButton>
      <FeatureGuardedButton
        key="recipe_categories"
        style={categoriesButtonStyle}
        href="/recipe_categories"
        onClick={ev => {
          router.push('/recipe_categories');
          ev.preventDefault();
        }}
      >
      Recipe Categories
      </FeatureGuardedButton>
      <FeatureGuardedButton
        key="inventory-categories"
        style={categoriesButtonStyle}
        href="/inventory-categories"
        onClick={ev => {
          router.push('/inventory_categories');
          ev.preventDefault();
        }}
      >
      Ingredient Locations
      </FeatureGuardedButton>
    </div>
  }
}