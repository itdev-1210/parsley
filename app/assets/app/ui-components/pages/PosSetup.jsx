// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import CrudEditor from './helpers/CrudEditor';
import { history } from '../../navigation';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { Row, Col, Table, FormControl } from 'react-bootstrap';
import Validate from 'common/ui-components/Validate';
import Section from '../Section';
import { withRouter } from 'react-router';
import { reduxForm } from 'redux-form';
import { getMenuItemsList,
    getProductUsageInfo,
    getMultipleProductsUsageInfo, getPosSetup, updatePosSetup } from '../../webapi/endpoints';
import AddButton from '../AddButton';
import Select from '../Select';
import CurrencyInput from '../CurrencyInput';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import {
  checkFields, deepArrayField, deepObjectField, required, optional, isBlank
} from '../../utils/validation-utils';
import { actionCreators as MainInfoLoadedActions } from '../../reducers/mainInfoLoaded';
import { scrollToTopErrorTooltip } from '../../utils/ui-utils';
import type { FormField } from '../../utils/form-utils';
import { deepOmit } from '../../utils/general-algorithms';

function validateItem(item, recipes) {
  const validateResult = {};
  const requiredFields = ['id', 'itemNumber'];
  requiredFields.forEach(field => {
    if (isBlank(item[field])) validateResult[field] = 'Required';
  });

  if (!isBlank(item.itemNumber)) {
    const indexItemNumber = recipes.findIndex(r => r.itemNumber === item.itemNumber);
    const indexItem = recipes.findIndex(r => r.id === item.id);
    if (indexItemNumber !== indexItem) {
      validateResult.itemNumber = 'Item Number is Already in Use';
    }
  }
  return validateResult;
}

function validate(values) {
  const validators = {
    recipes: deepArrayField(item => validateItem(item, values.recipes)),
  };
  return checkFields(values, validators);
}

@withRouter
@reduxForm(
    {
      form: 'pos-setup',
      fields: [
        'recipes[].id',
        'recipes[].itemNumber',
        'recipes[].price',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      enableReinitialize: true,
      overwriteOnInitialValuesChange: true,
      onSubmitSuccess: () => history.goBack(),
      // // TODO: Move this universal behaviour to CrudEditor
      // // we can't define `onSubmitFail` in CrudEditor,
      // // and the `submit` function isn't reached if the `validation` functions fail though
      onSubmitFail: () => setTimeout(scrollToTopErrorTooltip, 200),
    },
    (state, ownProps) => {
      const menuItems = state.get('menuItems');
      const recipeUsageInfo = state.get('detailedIngredients');
      const posSetupInfo = state.get('posSetup');
      const mainInfoLoaded = menuItems && recipeUsageInfo && posSetupInfo;
      let recipes = [];
      if (mainInfoLoaded) {
        recipes = posSetupInfo.map(p => {
          const recipeDetails = recipeUsageInfo.get(p.get('recipeId'));
          if (recipeDetails) {
            return {
              id: p.get('recipeId'),
              name: recipeDetails.get('name'),
              itemNumber: recipeDetails.get('itemNumber'),
              price: recipeDetails.get('portionPrice'),
            };
          } else return {};
        }).sortBy(p => p.name && p.name.toLowerCase()).toJS();
      }

      return {
        initialValues: { recipes },
        menuItems,
        mainInfoLoaded,
        recipeUsageInfo,
        posSetupInfo,
        deleteDisabled: true,
      };
    },
)
export default class PosSetup extends CrudEditor {

  deleteItem() {}

  handleSave(id: number, deets: any) {
    return updatePosSetup(deepOmit(deets.recipes, isBlank));
  }

  renderBody() {
    const { fields, menuItems, recipeUsageInfo } = this.props;
    const { recipes } = fields;

    return <div>
      <Section>
        <div className="suppliedIngredientsList">
          {
            menuItems && recipeUsageInfo ?
             /** $FlowFixMe: can't simulate extra props passed in by decorators */
              <FieldArray
                component={PosSetupRecipesDisplay}
                fields={recipes}
                props={{
                  menuItems,
                }}
             />
            : <LoadingSpinner/>
          }
        </div>
      </Section>
    </div>;
  }

  componentDidMount() {
    getMenuItemsList();
    getPosSetup().then(posSetupInfo =>
      getMultipleProductsUsageInfo(posSetupInfo.map(p => p.recipeId)),
    );
  }

  listTitle = 'POS setup';
}

type PosSetupRecipesDisplayProps= {
  fields: any,
  menuItems: Immutable.List<Immutable.Map<string, any>>,
}

class PosSetupRecipesDisplay extends Component<PosSetupRecipesDisplayProps> {
  constructor(props, context) {
    super(props, context);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleAdd = () => {
    const { fields: recipes } = this.props;
    recipes.addField({});
  }

  handleDelete = (id: number) => {
    const { fields: recipes } = this.props;
    const idx = recipes.findIndex(recipe => recipe.id.value === id);
    recipes.removeField(idx);
  }

  render() {
    const { menuItems, fields: recipes } = this.props;
    const addedRecipes = recipes.map(item => item.id.value);

    return <div>
      <Row>
        <Col xs={12} md={10} mdOffset={1} className="pos-setup-table-container" >
          <Table className="pos-setup-table" style={{ borderCollapse: 'collapse' }}>
            <thead>
            <tr>
              <th >Menu Item</th>
              <th style={{ width: '190px' }}>Item Number</th>
              <th colSpan={2} style={{ width: '85px', paddingLeft: '5px' }}>Price (optional)</th>
              <th style={{ width: '85px' }}/>
            </tr>
            </thead>
            <tbody>
            {
              recipes.map((recipe, index) =>
                /** $FlowFixMe: can't simulate extra props passed in by decorators */
                <PosSetupRecipeRow
                  key={`${recipe.id.value}-${index}`}
                  recipe={recipe}
                  menuItems={menuItems}
                  addedRecipes={addedRecipes}
                  handleDelete={this.handleDelete}
                />,
              )
            }
            </tbody>
          </Table>
          <AddButton onClick={this.handleAdd}/>
        </Col>
      </Row>
    </div>;
  }
}

type PosSetupRecipeRowProps= {
  recipe: { [fieldName: string]: FormField<any> },
  menuItems: Immutable.List<Immutable.Map<string, any>>,
  recipeUsageInfo: Immutable.Map<number, any>,
  addedRecipes: Immutable.List<string>,
  handleDelete: Function,
}

@connect(
    state => ({
      recipeUsageInfo: state.get('detailedIngredients'),
    }),
)
class PosSetupRecipeRow extends Component<PosSetupRecipeRowProps> {

    onChangeRecipe = (value: number) => {
      const {
        recipe: {
          id, price, itemNumber,
        },
        recipeUsageInfo,
      } = this.props;
      const mRecipe = recipeUsageInfo && recipeUsageInfo.get(value);
      if (mRecipe) {
        price.onChange(mRecipe.get('portionPrice'));
      } else {
        getProductUsageInfo(value).then(deets => {
          price.onChange(deets.portionPrice);
          itemNumber.onChange(deets.itemNumber);
        });
      }
      id.onChange(value);
  }

  render() {
    const { recipe, menuItems, addedRecipes, handleDelete } = this.props;
    const { id, itemNumber, price } = recipe;
     const menuItemOptions = menuItems
        .filter(i =>
          (!i.get('tombstone') && !i.get('ingredient')) &&
            // $FlowFixMe: only with records can we assert member existence/value
          (i.get('id') == id.value || !addedRecipes.includes(i.get('id'))),
        )
        .map(i => ({
          // $FlowFixMe: only with records can we assert member existence/value
          value: (i.get('id'): number),
          // $FlowFixMe: only with records can we assert member existence/value
          label: (i.get('name'): string),
        })).toArray();

    return <tr>
      <td>
        <Validate {...id}>
          <Select
            options={menuItemOptions}
            {...id}
            // $FlowFixMe - we know all possible values are numbers
            onChange={this.onChangeRecipe.bind(this)}
          />
        </Validate>
      </td>
      <td style={{ textAlign: 'center', width: '190px' }}>
        <div>
          <Validate {...itemNumber}>
            <FormControl type="text" placeholder="Item Number" {...itemNumber}/>
          </Validate>
        </div>
      </td>
      <td style={{ textAlign: 'center', width: '100px' }}>
        <div>
          <Validate {...price}>
            <CurrencyInput placeholder="Price" {...price}/>
          </Validate>
        </div>
      </td>
       <td style={{ width: '85px' }}>
          <div style={{ height: '27px', width: '89px' }}>
            <span onClick={() => handleDelete(id.value)} className="pull-right list-item-delete-link">
              delete
            </span>
          </div>
        </td>
    </tr>;
  }
}