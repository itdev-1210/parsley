import React, { Component } from "react";
import { connect } from 'react-redux';

import { Grid, Row, Col } from "react-bootstrap";
import { ListGroup, ListGroupItem } from "react-bootstrap";

import { Link } from "react-router";

import NewButton from "../NewButton";
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import { getMeasures, deleteMeasure }
    from "../../webapi/endpoints.jsx";

@connect(
  state => ({
    measures: state.getIn(["measures", "measures"]),
  })
)
export default class MeasureList extends Component {
  render() {
    const {
        measures, location
    } = this.props;

    if (!measures) {
      return <LoadingSpinner />;
    }

    var listItems = measures.map((m, id) =>
            <ListGroupItem key={id}>
              <Link
                  to={location.pathname + "/" + id.toString()}>
                {m.get("name")}
              </Link>
              <span onClick={this.deleteMeasure.bind(this, id)} className="pull-right">
                delete
              </span>
            </ListGroupItem>
    );
    return (
        <Grid fluid>
          <Row>
            <Col md={3} s={6} xs={12}>
              <h3 className="list-title">Measures</h3>
            </Col>
          </Row>
          <Row>
            <NewButton
                noun="Measure"
                newPage="/measures/new" />
          </Row>
          <Row>
            <ListGroup>
              {listItems}
            </ListGroup>
          </Row>
        </Grid>
    );
  }

  deleteMeasure(id, ev, reactId) {
    deleteMeasure(id).then(getMeasures);
  }

  componentDidMount() {
    getMeasures();
  }
}
