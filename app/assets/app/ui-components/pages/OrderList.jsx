import React, { Component } from 'react';
import _ from 'lodash';

import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { history } from '../../navigation';

import Immutable from 'immutable';

import { Button, Grid, ListGroupItem } from 'react-bootstrap';
import { Link } from 'react-router';

import Checkbox from '../Checkbox';
import Onboarding from '../Onboarding';
import PurchasingFlow from '../PurchasingFlow';
import SendModal from '../modals/SendModal';
import UploadCsvModal from '../modals/UploadCsvModal';
import SearchableList, { ScrollableLink, searchableListLoadingSpinner } from './helpers/SearchableList';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import PosCsvReader from '../PosCsvReader';

import {
  getOrderList,
  deleteOrder,
  getRecipeList,
  getSupplierList,
  getPurchaseList,
  getIngredientParsExists,
  getOnboardingInfo,
} from '../../webapi/endpoints';
import { actionCreators as ListUIStateActions } from '../../reducers/uiState/searchableList';
import { actionCreators as UIStateActions } from '../../reducers/uiState/orderList';
import { actionCreators as PosCsvStateActions } from '../../reducers/posCsvDetails';
import {
  startOfDay,
  isBeforeToday,
} from '../../utils/general-algorithms';
import { dateWithWeekdayFormat, timeFormat } from 'common/utils/constants';
import {
  keyIn,
} from 'common/utils/predicates';
import { featureIsSupported } from '../../utils/features';
import { featureIsAvailable } from 'common/utils/features';
import { memoize } from '../../utils/performance';

@connect(
    state => ({
      orders: state.get('orders'),
      recipes: state.get('recipes'),
      suppliers: state.get('suppliers'),
      listUiState: state.getIn(['searchableList', 'orders']),
      uiState: state.getIn(['orderList']),
      purchaseOrders: state.get('purchaseOrders') && state.get('purchaseOrders').filterNot(po => po.get('isImported')),
    }),
    dispatch => ({
      listUIActions: _.mapValues(bindActionCreators(ListUIStateActions, dispatch),
          creator => creator.bind(this, 'orders')),
      uiActions: bindActionCreators(UIStateActions, dispatch),
      posCsvActions: bindActionCreators(PosCsvStateActions, dispatch),
    }),
)
export default class OrderList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      shoppingEmailStatus: Immutable.Map(),
    };
    this.setPars = this.setPars.bind(this);
  }

  setPars() {
    this.props.router.push('/inventories/pars');
  }

  static isPurchaseOrder = item => item.get('xact') || item.get('supplier');
  static groupingFunction = item => startOfDay(OrderList.isPurchaseOrder(item) ? item.get('emailedAt') || item.get('printedAt') : item.get('date')).valueOf();
  static preProcessGroupItems = memoize(groupItems => {
    // helper functions
    const isPrintedOrder = item => OrderList.isPurchaseOrder(item) && item.get('printedAt');
    const isEmailedOrder = item => OrderList.isPurchaseOrder(item) && item.get('emailedAt');
    const isImportedOrder = item => OrderList.isPurchaseOrder(item) && item.get('isImported');
    const getPurchaseTime = item => moment(isPrintedOrder(item) ? item.get('printedAt') : item.get('emailedAt'));

    const [groupPurchaseOrders, groupRegularOrders] = _.partition(groupItems.filterNot(isImportedOrder).toArray(), OrderList.isPurchaseOrder);
    const processedPurchaseOrders = groupPurchaseOrders.map(item => {
      const relevantPurchases = groupItems
          .filter(itemInGroup => !isImportedOrder(item) && isPrintedOrder(item) ? isPrintedOrder(itemInGroup) : isEmailedOrder(itemInGroup))
          .sort((itemA, itemB) => {
                const purchaseTimeItemA = getPurchaseTime(itemA).valueOf();
                const purchaseTimeItemB = getPurchaseTime(itemB).valueOf();
                if (purchaseTimeItemB > purchaseTimeItemA) {
                  return -1;
                } else return 1;
              },
          );

      // if multiple POs in group, we consolidate them into a single one;
      // we do this by ignoring the non-first ones in the group...
      if (relevantPurchases.size > 1 && relevantPurchases.first().get('id') !== item.get('id')) {
        return null;
      }

      // ...and putting info for *all* of the POs in the group into the processed
      // item for the first one
      return item.set('relevantSuppliers', relevantPurchases
          .groupBy(p => p.get('supplier'))
          .map(purchaseOrdersForSupplier =>
              purchaseOrdersForSupplier.map(po => po.get('id')),
          ),
      ).set(
          // number of purchase made in this group is lost
          // by grouping suppliers into relevantSuppliers above
          // so save it as a reference
          'purchaseCount',
          relevantPurchases.size,
      );
    }).filter(processedItem => !_.isNull(processedItem));

    // had to go to an array to use _.partition
    return Immutable.List(groupRegularOrders).concat(processedPurchaseOrders);
  });

  getPurchaseText = (checkedItems, orders, includePar) => {
    if (!checkedItems || checkedItems.isEmpty()) {
      return includePar ? 'Purchase to Par' : 'Purchase to Forecast';
    }
    return includePar ? 'Purchase to Par plus Selected Events' : 'Purchase for Selected Events';
  }

  render() {
    const {
        orders, recipes,

        listUiState, uiState,
        listUIActions: {
          checkItem, uncheckItem, showDeleteConfirmation,
        },
        uiActions: {
          beginPurchasing, endPurchasing,
          showPurchasing,
          closeSendModal,
          openPosImportModal,
          closePosImportModal,
        },
        purchaseOrders, suppliers,
        posCsvActions: { csvDetails },
    } = this.props;

    let content;
    const checkedItems = listUiState && listUiState.get('checkedItems');
    if (!(orders && purchaseOrders && suppliers)) {
      content = searchableListLoadingSpinner;
    } else {
      const dateDisplay = (dateString) => {
        const date = moment(dateString);
        return date.format(dateWithWeekdayFormat);
      };

      const dateToCheckboxProps = (date) => {
        const relevantOrders = orders
            .filter(o => startOfDay(o.get('date')).format() === date)
            .map(o => o.get('id'));

        if (relevantOrders.isEmpty()) {
          return { checked: false, indeterminate: false, disabled: true };
        } else if (relevantOrders.isSubset(checkedItems)) {
          return { checked: true, indeterminate: false,
            onChange() {
              relevantOrders.forEach(uncheckItem);
            },
          };
        } else {
          // indeterminate state acts like unchecked state except for display purposes; clicking causes all to be checked
          return {
            checked: false, indeterminate: relevantOrders.some(o => checkedItems.includes(o)),
            onChange() {
              relevantOrders.forEach(checkItem);
            },
          };
        }
      };

      const supplierIdToName = id => suppliers.find(s => s.get('id') === id, null, Immutable.Map()).get('name', 'Unknown Source');
      const baseUrl = this.props.location.pathname;

      content = <SearchableList
          searchKey="sales"
          noun="Sales"
          newButtonNoun="Forecast or Event"
          title="Plans & Purchasing"
          reduxStatePath="orders"
          extraItemsReduxPaths={featureIsAvailable('RECEIVING') ? ['purchaseOrders'] : []}

          baseUrl={baseUrl}

          deleteItems={ids => SearchableList.deleteItems(ids, deleteOrder)}
          refreshItems={getOrderList}
          checkable
          taggable={false}
          copiable
          shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}

          groupingFunction={OrderList.groupingFunction}
          groupIDToHeader={(dateValue, groupItems) =>
            dateValue ?
              isBeforeToday(moment(dateValue)) ?
                <span className="h4 order-list-date-header ml-2 past-order">{dateDisplay(moment(dateValue).format())}</span>
                : groupItems.every(OrderList.isPurchaseOrder) ?
                    <span className="h4 order-list-date-header ml-2">{dateDisplay(moment(dateValue).format())}</span>
                    : <Checkbox inline {...dateToCheckboxProps(moment(dateValue).format())} className="h4 order-list-date-header">
                      {dateDisplay(moment(dateValue).format())}
                    </Checkbox>
            : null
          }
          sectionFunction={(groupedItems, groupedDivs) => {
            const index = groupedItems.keySeq().toJS().findIndex(groupedDateKey => isBeforeToday(moment(groupedDateKey)));

            const sectionBreak = <div className="searchable-break" style={(index < 1) ? { paddingTop: '8px' } : {}}>
              {(index < 0) ? <div className="no-past-order"><hr className="gray-hr"/>There are no past sales</div> : <div><h3>Past</h3><hr className="green-hr" /></div>}
            </div>;

            if (index < 0) groupedDivs.push(sectionBreak);
            else groupedDivs.splice(index, 0, sectionBreak);

            return groupedDivs;
          }}

          // TODO: HERE IT IS
          preProcessGroupItems={OrderList.preProcessGroupItems}

          renderSectionItem={item => {

            if (OrderList.isPurchaseOrder(item)) {
              return !featureIsSupported('RECEIVING') || item.get('isImported') ? null : <div style={{ textDecoration: 'underline' }}>
                <ListGroupItem key={item.get('id')} style={{marginLeft: '40px'}}>
                  <span id={`list-item-po-${item.get('id')}`} className="list-item-main-link purchase-list-item-received" style={{whiteSpace: 'initial', color: '#07b245'}}>
                    <span className="list-item-check" style={{top: '7px'}}/>
                      Order{item.get('purchaseCount') < 2 ? '' : 's'}
                      {item.get('emailedAt') ? ' emailed to ' : ' printed for '}
                      {item.get('relevantSuppliers').entrySeq().map(([supplierId, purchaseOrdersForSupplier], index, map) =>
                        <Link key={`${item.get('id')}-${supplierId}`} id={`list-item-po-${item.get('id')}`}
                          to={`/purchase_orders/${(purchaseOrdersForSupplier.first())}`}>
                          {supplierIdToName(supplierId)}{map.size - 1 > index ? ', ' : ''}
                        </Link>,
                      ).toArray()}
                  </span>
                </ListGroupItem>
              </div>;
            }

            const mainLink = <ScrollableLink
                id={`list-item-orders-${item.get('id')}`}
                item={item}
                shouldScroll={item => this.props.location.state && item.get('id') === this.props.location.state.prevObjId}
                className={`list-item-main-link ${item.get('subtractFromCurrentInventory') ? '' : 'non-substract-from-inventory'}`}
                to={{
                  pathname: this.props.location.pathname + '/' + item.get('id').toString(),
                  state: {
                    searchTerm: listUiState.get('searchTerm'),
                  },
                }}
              >
              {item.get('name')}
            </ScrollableLink>;

            return <div>
              <ListGroupItem key={item.get('id')} className="order-list-item">
                <Checkbox
                    inline
                    checked={listUiState.get('checkedItems').includes(item.get('id'))}
                    onChange={ev => ev.target.checked
                      ? checkItem(item.get('id'))
                      : uncheckItem(item.get('id'))
                    }
                  >{mainLink}
                </Checkbox>
                <span
                  onClick={() => showDeleteConfirmation(Immutable.List([item.get('id')]))}
                  className="pull-right action-link">
                  delete
                </span>
                <Link
                  to={baseUrl + '/new/from/' + item.get('id').toString()}
                  className="pull-right list-item-delete-link action-link">
                  copy
                </Link>
              </ListGroupItem>
            </div>;
          }}
          groupReverseSort
          >
        <FeatureGuardedButton className="purchase-btn" disabled={!(checkedItems && checkedItems.size)}
                feature={checkedItems && checkedItems.size > 1 ? 'MULTI_PURCHASE' : ''}
                onClick={() => {
                  this.setState({ shoppingEmailStatus: Immutable.Map() });
                  // filter checked orders and then pick id + date of order
                  beginPurchasing(orders.filter(o => checkedItems.includes(o.get('id'))).map(o => o.filter(keyIn('id', 'date'))), false, true);
                }}
                style={checkedItems && checkedItems.size > 1 && !featureIsSupported('MULTI_PURCHASE')
                  ? {overflow: 'visible', maxWidth: 'fit-content'}
                  : {}
                }
                >
          {this.getPurchaseText(checkedItems, orders)}
        </FeatureGuardedButton>
        <FeatureGuardedButton className="purchase-btn"
          feature="PAR_LEVELS"
          style={featureIsSupported('PAR_LEVELS') ? {} : {overflow: 'visible', maxWidth: 'fit-content'}}
          onClick={() => { // note on above line - this assumes all users with PAR_LEVELS also have MULTI_PURCHASE
            this.setState({ shoppingEmailStatus: Immutable.Map() });
            // filter checked orders and then pick id + date of order
            beginPurchasing(orders.filter(o => checkedItems.includes(o.get('id'))).map(o => o.filter(keyIn('id', 'date'))), true, true);
          }}>
          {this.getPurchaseText(checkedItems, orders, true)}
        </FeatureGuardedButton>
        <Button
          className="purchase-btn"
          onClick={() => {
            this.setState({ shoppingEmailStatus: Immutable.Map() });
            beginPurchasing(Immutable.Set(), false, false);
          }}
        >
          Purchase Direct
        </Button>
      </SearchableList>;
    }

    const setParBtnStyle = { position: 'absolute', right: '448px', top: '12px', zIndex: 1020 };
    return <Grid fluid style={{ position: 'relative' }} className='order-list'>

      <FeatureGuardedButton
        feature="PAR_LEVELS"
        wrapperStyle={featureIsSupported('PAR_LEVELS') ? {} : setParBtnStyle}
        className="purchase-btn"
        style={featureIsSupported('PAR_LEVELS') ? setParBtnStyle : {overflow: 'visible', maxWidth: 'fit-content'}}
        onClick={this.setPars}
      >
        {this.state.ingredientParsExists ? 'View/Edit Pars' : 'Set Pars'}
      </FeatureGuardedButton>
      <FeatureGuardedButton
        feature="POS"
        className="purchase-btn"
        style={{ ...setParBtnStyle, right: '268px' }}
        onClick={openPosImportModal}
      >
        Import POS Sales Data
      </FeatureGuardedButton>
      {content}
      <Onboarding pageKey="order_list" />
      {uiState.has('purchasingForOrders')
          ? <PurchasingFlow
              orders={uiState.get('purchasingForOrders')}
              includePar={uiState.get('includePar')}
              fromPurchaseOrder={uiState.get('fromPurchaseOrder')}
              purchaseOrders={purchaseOrders}
              hide={endPurchasing}
              purchaseEmailStatus={this.state.shoppingEmailStatus}
              display={uiState.get('showPurchasing')}
            />
          : null
      }
      { uiState.get('showSendModal') ?
         <SendModal
            supplierId={uiState.get('sendSupplierId')}
            orders={checkedItems.toJS()}
            orderDeets={uiState.get('userEditedOrder')}
            page={'list'}
            hide={() => {
              closeSendModal();
              showPurchasing();
            }}
            afterModalSent={() => {
              this.setState({
                shoppingEmailStatus: this.state.shoppingEmailStatus.merge(Immutable.Map({ [uiState.get('sendSupplierId')]: true })),
              });
            }}
            isEmailSent={this.state.shoppingEmailStatus.get(`${uiState.get('sendSupplierId')}`)}
         />
      : null }
      {
        uiState.get('showPosImportModal') ?
          <UploadCsvModal
            title="Import POS Sales Data"
            onHide={closePosImportModal}
            onUpload={(headers, data) => {
              csvDetails(headers, data);
              closePosImportModal();
              history.push('/pos-import');
            }}
          />
      : null }
    </Grid>;
  }

  componentWillMount() {
    getIngredientParsExists().then(ingredientParsExists => this.setState({ ingredientParsExists }));
    getSupplierList();
    getPurchaseList();
    getOnboardingInfo();
    getOrderList().then(orders =>
        orders.length === 0
            // we're going to need to know whether to display the "make an order" or "make recipes first" text
            ? getRecipeList()
            : null,
    );
  }
}
