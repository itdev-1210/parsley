import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Button, Grid, Table, Row, Col, Tooltip, OverlayTrigger, InputGroup } from 'react-bootstrap';
import { Link, withRouter } from 'react-router';
import { history } from '../../navigation';
import {
  getInventoryLastTakenTime,
  getIngredientList,
  getIngredientPars,
  getNewInventoryItemList,
  getInventoryDetails,
  getPreviousInventoryDetails,
  saveInventory,
  getCurrentInventory,
  saveIngredientPars,
  getUserInfo,
  getProductDetails,
  getProductUsageInfo,
  getMeasures,
  getSupplierList,
  getMultipleProductsUsageInfo,
  getOnboardingInfo,
  getSupplierSourcedIngredients,
} from '../../webapi/endpoints';
import { actionCreators as InventoryDetailsActions } from '../../reducers/inventoryDetails';
import { actionCreators as SearchableListActions } from '../../reducers/uiState/searchableList';
import { actionCreators as InventoryItemsActions } from '../../reducers/inventoryItems';
import { actionCreators as IngredientParsActions } from '../../reducers/ingredientPars';
import { actionCreators as MainInfoLoadedActions } from '../../reducers/mainInfoLoaded';

import { FieldArray } from '../../utils/redux-upgrade-shims';
import Onboarding from '../Onboarding';
import SearchableList from './helpers/SearchableList';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';
import Checkbox from '../Checkbox';
import { MaskedNumberInput } from '../MaskedInput';
import CurrencyDisplay from '../CurrencyDisplay';
import PackagedAmountInput from '../PackagedAmountInput';
import Select from '../Select';
import {
  getCommonPurchaseUnits,
  convertPerQuantityValue,
  truncated,
  divideQuantities,
  unitDisplayName,
  getPrimaryUnitId,
} from '../../utils/unit-conversions';
import { deepArrayField, isBlank, checkFields } from '../../utils/validation-utils';
import {
  PackageSizeInput, RootUnitInput,
  SubPackageSizeInput,
} from '../SourceFormComponents';
import Immutable from 'immutable';
import _ from 'lodash';
import moment from 'moment';
import formatNum from 'format-num';

import AddToCategoryModal from '../modals/AddToCategoryModal';
import RemoveFromCategoryModal from '../modals/RemoveFromCategoryModal';
import IngredientSourcesModal from '../modals/IngredientSourcesModal';
import { DeleteConfirmationModal } from '../modals/ConfirmationModal';
import AddInventoryItemModal from '../modals/AddInventoryItemModal';
import NewIngredientModal from '../modals/NewIngredientModal';
import InventoryShrinkageModal from '../modals/InventoryShrinkageModal';

import {
  costingSize,
  effectivePackageSize, expandSourceOptions,
  newSourceTemplate
} from '../../utils/packaging';
import { quantityFormFields, shallowToObject } from '../../utils/form-utils';
import { csvQuote, downloadCSVDataAsFile } from '../../utils/file-utils';
import { scrollToTopErrorTooltip } from '../../utils/ui-utils';
import {
  ingredientListSearchOpts,
} from '../../utils/search';
import { defaultComparator } from 'common/utils/sorting';
import { FeatureGuardedButton } from '../FeatureGuardedInputs';
import { dateWithWeekdayFormat } from 'common/utils/constants';

const spaceToAmountInput = '3px';
let modifiedAfterDate = null;
let lastItemsId = Immutable.Set();

function getListComponent(setPars) {
  return props => <Table className="take-inventory">
      <colgroup>
        <col style={{ width: '275px' }} />
        <col style={{ width: '425px' }} />
        <col style={{ width: '175px' }} />
        <col style={{ width: '75px' }} />
      </colgroup>
      <thead>
        <tr>
          <th>Ingredient</th>
          <th style={{ paddingLeft: '12px' }}>{setPars ? 'Par Level' : 'Quantity on Hand'}</th>
          <th style={{ textAlign: 'right' }}>Value</th>
          <th />
        </tr>
      </thead>
      <tbody>{props.children}</tbody>
    </Table>;
}

// can't inline these, because React needs the variable names for its
// reconciliation algorithm for some reason
const ParListComponent = getListComponent(true);
const InventoryItemsListComponent = getListComponent(false);

function validateItem(item) {
  const requiredFields = ['id', 'product', 'packaged', 'packageSize', 'superPackageSize'];
  const validateResult = {};
  requiredFields.forEach(field => {
    if (isBlank(item[field])) validateResult[field] = 'Required';
  });

  if ((item.quantity.amount || item.packaged) && (!item.quantity.measure || !item.quantity.unit)) {
    validateResult.quantity = {
      measure: 'Required',
      unit: 'Required',
    }
  }

  return validateResult;
}

function validate(values) {

  const validators = {
    items: deepArrayField(validateItem),
  };

  // In case the submit button suddenly stops working
  // console.log(checkFields(values, validators));

  return checkFields(values, validators);
}

function getInitialFormValue(productId, details, productIdToSourcesMap, supplierIdToNameMap, item, purchaseWeightSystem, purchaseVolumeSystem, setPars) {
  quantityFormFields.forEach(field => {
    const value = item.get(field);
    if (typeof value === 'number') {
      item = item.setIn(['quantity', field], value);
    }
    item = item.delete(field);
  });

  // Advanced preps
  if (details && details.has('isPrepOf')) {
    const measure = item.getIn(['quantity', 'measure']) || details.get('primaryMeasure');
    let userPurchaseSystem = 'metric';
    if (measure === 2) userPurchaseSystem = purchaseWeightSystem;
    if (measure === 3) userPurchaseSystem = purchaseVolumeSystem;
    const commonPurchaseUnits = getCommonPurchaseUnits(measure, userPurchaseSystem === 'metric');
    item = item.setIn(['quantity', 'measure'], measure);
    item = item.setIn(['quantity', 'unit'], commonPurchaseUnits.getIn([0, 'id']));
    item = item.set('packaged', false);
    item = item.set('packageSize', 1);
    item = item.set('superPackageSize', 1);

  } else if (details && details.get('inHouse')) {
    // Sub-recipe -- no specific source

    productIdToSourcesMap[productId] = Immutable.List();
    if (!item.hasIn(['quantity', 'measure']) || !item.hasIn(['quantity', 'unit'])) {
      item = item.setIn(['quantity', 'measure'], details.get('primaryMeasure'));
      item = item.setIn(['quantity', 'unit'], details.get('primaryUnit'));
    }
    item = item.set('packaged', false);
    item = item.set('packageSize', 1);
    item = item.set('superPackageSize', 1);

  } else if (details && productIdToSourcesMap.hasOwnProperty(productId)) {

    const conversions = details.get('measures');

    // Find all sources for this ingredient
    // Original sources
    let productSources = productIdToSourcesMap[productId];
    const preferredSourceId = details.get('preferredSource');
    let preferredSupplierName;

    if (preferredSourceId) {
      const preferredSource = productSources.find(s => s.get('id') === preferredSourceId);
      preferredSupplierName = supplierIdToNameMap[preferredSource.get('supplier')];
    }

    productSources.forEach(source => {
      productSources = productSources.concat(Immutable.fromJS(expandSourceOptions(
          source, productSources,
          conversions,
          purchaseWeightSystem, purchaseVolumeSystem,
      )));
    });

    // User's selected unit
    const selectedUnit = item.getIn(['quantity', 'unit']);
    if (selectedUnit) {

      function sourceMatchesItem(s) {
        return s.get('unit') === selectedUnit
            && s.get('packaged') === item.get('packaged')
            && s.get('packageName') == item.get('packageName')
            && s.get('packageSize') === item.get('packageSize')
            && s.get('subPackageName') == item.get('subPackageName')
            && s.get('superPackageSize') === item.get('superPackageSize')
      }


      // Find if there's any source that matches the user's selected package
      // If none is existed, eg:
      // The user first takes `1lb of Caviar` but don't update its cost info (no source)
      // The user later adds sources to Caviar in the Ingredient page:
      //    - case of 1kg by supplier A
      //    - jar of 220ml by supplier B
      // As we want to persist the user's selected package
      // We'll add 2 `lb` packages, according to each source above:
      //    - 1st lb from supplier A, with cost info converted from `case of 1kg`
      //    - 2nd lb from supplier B, with cost info converted from `jar of 220ml`
      if (productSources.some(sourceMatchesItem)) {
        productSources.forEach(source => {

          // Check if a similar source has existed
          // If there is, select the cheaper one
          // Eg: The user selected 1kg and the same supplier A has 2 sources:
          //     - 1L           - $1
          //     - bottle of 1L - $1.2
          // In this case we convert and use "1L -$1" only
          // So no multiple `lb` sources with different cost appear
          const cost = convertPerQuantityValue(
            source.get('cost'),
            source.get('superPackageSize') * source.get('packageSize'),
            source.get('unit'),
            item.get('superPackageSize') * item.get('packageSize'),
            selectedUnit,
            conversions
          );
          const supplier = source.get('supplier');

          const existingIdx = productSources.findIndex(s =>
              sourceMatchesItem(s) && (!supplier || s.get('supplier') === supplier),
          );

          if (existingIdx !== -1) {
            if (productSources.getIn([existingIdx, 'cost']) > cost) {
              productSources = productSources.setIn([existingIdx, 'cost'], cost);
            }
          } else {
            let pricePer = 'unit';
            if (item.get('subPackageName')) {
              pricePer = 'super-package';
            } else if (item.get('packageName')) {
              pricePer = 'package';
            }
            const measure = item.getIn(['quantity', 'measure']);

            productSources = productSources.push(Immutable.fromJS({
              cost,
              measure,
              unit: selectedUnit,
              pricePer,
              pricingMeasure: measure,
              pricingUnit: selectedUnit,
              packaged: item.get('packaged'),
              packageName: item.get('packageName'),
              packageSize: item.get('packageSize'),
              subPackageName: item.get('subPackageName'),
              superPackageSize: item.get('superPackageSize'),
              supplier,
            }));
          }
        });
      }
    }

    let additionalSourceIdCounter = -1; // Just to distinguish sources in the client-side, not saved to the db
    productSources = productSources.map(source => {
      if (source.get('id') >= 0) {
        return source;
      } else {
        return source.set('id', additionalSourceIdCounter--);
      }
    });

    productSources = productSources
      .map(source =>
        source.set('supplierName', supplierIdToNameMap[source.get('supplier')])
      ).sort((a, b) => {
        const supplierNameA = a.get('supplierName');
        const supplierNameB = b.get('supplierName');
        // From the same supplier: Super package first, ordinary package second, UoM last
        if (supplierNameA === supplierNameB) {
          if (a.get('subPackageName') && !b.get('subPackageName')) {
            return -1;
          } else if (!a.get('subPackageName') && b.get('subPackageName')) {
            return 1;
          }
          if (a.get('packageName') && !b.get('packageName')) {
            return -1;
          } else if (!a.get('packageName') && b.get('packageName')) {
            return 1;
          }
          return 0;
        } else if (supplierNameA === preferredSupplierName) {
          return -1;
        } else if (supplierNameB === preferredSupplierName) {
          return 1;
        } else {
          return supplierNameA.localeCompare(supplierNameB);
        }
      });

    // The selected source that we want to find
    let selectedSource;

    // Find the exact source if possible
    // Use `==` for optional fields, so null == undefined
    // As empty fields in thisProductSources are undefined,
    // while empty fields in item are null
    const exactSource = productSources.find(s =>
      s.get('unit') === selectedUnit
      && s.get('packaged') === item.get('packaged')
      && s.get('packageName') == item.get('packageName')
      && s.get('packageSize') === item.get('packageSize')
      && s.get('subPackageName') == item.get('subPackageName')
      && s.get('superPackageSize') === item.get('superPackageSize')
      && s.get('supplier') == item.get('supplier')
    );

    if (exactSource) {
      selectedSource = exactSource.get('id');
    } else if (selectedUnit) { // Can't find the exact source, try to find the best replacement

      // Find the closest source before falling back to the ingredient's preferred source
      // By comparing to the selected values in the inventory item, we first find all sources from the same supplier,
      //  if there are ties, continue to find on those tie sources the ones with the same measuer,
      //  continue recursively until there's only one closest source or until the end of the priority list.
      const priorityOrder = ['supplier', 'measure', 'unit', 'packaged', 'packageName', 'subPackagedName', 'packageSize', 'superPackagedSize'];
      const findClosestSource = (currentSources, priorityFieldIdx) => {
        if (priorityFieldIdx >= priorityOrder.length) return null;

        const field = priorityOrder[priorityFieldIdx];
        const itemField = field === 'measure' || field === 'unit' ? item.getIn(['quantity', field]) : item.get(field);
        const filteredSources = currentSources.filter(s => s.get(field) === itemField);

        if (filteredSources.size === 0) {
          return findClosestSource(currentSources, priorityFieldIdx + 1);
        } else if (filteredSources.size === 1) {
          return filteredSources.first();
        } else {
          const bestOfTheRest = findClosestSource(filteredSources, priorityFieldIdx + 1);
          if (bestOfTheRest) return bestOfTheRest;
          // Ties! Select the cheapest one!
          return filteredSources.sortBy(s => s.get('cost')).first();
        }
      };

      // Find the closest source with the same unit of measurement
      const closestSource = findClosestSource(productSources, 0);
      if (closestSource) {
        selectedSource = closestSource.get('id');
      } else if (preferredSourceId) { // Fall back to the preferred source if possible
        selectedSource = preferredSourceId;
      } else if (productSources.size > 0) { // Fall back to the first source if possible
        selectedSource = productSources.getIn([0, 'id']);
      }
    } else { // The user hasn't taken this ingredient yet -> just select the first source
      selectedSource = productSources.getIn([0, 'id']);
    }

    item = item.set('selectedSource', selectedSource);

    const source = productSources.find(s => s.get('id') === selectedSource).toJS();
    if (source) {
      item = item.setIn(['quantity', 'measure'], source.measure);
      item = item.setIn(['quantity', 'unit'], source.unit);
      item = item.set('packaged', source.packaged);
      item = item.set('packageSize', source.packageSize);
      item = item.set('superPackageSize', source.superPackageSize);
      item = item.set('packageName', source.packageName);
      item = item.set('subPackageName', source.subPackageName);
      item = item.set('supplier', source.supplier);
      item = item.set('supplierName', source.supplierName);
    }

    productIdToSourcesMap[productId] = productSources;

  } else {
    // No source information
    // But `item` might still hold saved data
    // In the case the user deleted an ingredient's sources,
    //  after taking it in that day inventory

    // Set default values for required fields if needed
    if (!item.get('packaged')) {
      item = item.set('packaged', false);
    }
    if (!item.get('packageSize')) {
      item = item.set('packageSize', 1);
    }
    if (!item.get('superPackageSize')) {
      item = item.set('superPackageSize', 1);
    }

    // Apply `PackagedAmountUnit`'s logic
    // See PARS-1496
    const currentAmount = item.getIn(['quantity', 'amount']);
    if (currentAmount) {
      item = item.setIn(['quantity', 'amount'], currentAmount / item.get('packageSize') / item.get('superPackageSize'));
    }
  }

  // Empty Par levels should be initialized to 0
  if (setPars && typeof item.getIn(['quantity', 'amount']) !== typeof 0) {
    item = item.setIn(['quantity', 'amount'], 0);
  }

  return item;
}

@reduxForm(
  {
    form: 'inventory-items-list',
    fields: [
      'items[].id',
      'items[].product',
      'items[].quantity.amount',
      'items[].quantity.measure',
      'items[].quantity.unit',
      'items[].packaged',
      'items[].packageName',
      'items[].packageSize',
      'items[].subPackageName',
      'items[].superPackageSize',
      'items[].supplier',
      'items[].supplierName',
      'items[].selectedSource',
      'items[].manuallyAdded',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    enableReinitialize: true,
    validate,
    // Have to wait a bit for the error tooltips to be rendered
    onSubmitFail: () => setTimeout(scrollToTopErrorTooltip, 200),
  },
  (state, ownProps) => {

    const mainInfoLoadedReduxPath = ownProps.setPars ? 'setPars' : 'takeInventory';
    const mainInfoLoaded = state.getIn(['mainInfoLoaded', mainInfoLoadedReduxPath]);
    if (!mainInfoLoaded) return { mainInfoLoadedReduxPath };

    const detailedIngredients = state.get('detailedIngredients');
    const suppliers = state.get('suppliers') || Immutable.List();
    const supplierSourcedIngredients = state.get('supplierSourcedIngredients');
    let supplierIdToNameMap = {};
    const purchaseWeightSystem = state.getIn(['userInfo', 'purchaseWeightSystem']);
    const purchaseVolumeSystem = state.getIn(['userInfo', 'purchaseVolumeSystem']);

    suppliers.forEach(supplier => {
      supplierIdToNameMap[supplier.get('id')] = supplier.get('name');
    });

    const productIdToSourcesMap = supplierSourcedIngredients
        // save supplier in the source objects, since we'll be throwing away map keys
        .map((sources, supplierId) => sources.map(s => s.set('supplier', supplierId)))
        .valueSeq().flatten(true).toList() // flat list of sources - must be List so we have the right methods attached
        .groupBy(s => s.get('product'))
        .toObject();

    const itemListReduxPath = ownProps.setPars ? 'ingredientPars' : 'inventoryItems';
    const refreshItemList = ownProps.setPars ? getIngredientPars : getNewInventoryItemList;
    const itemActions = ownProps.setPars ? IngredientParsActions : InventoryItemsActions;

    const items = (state.get(itemListReduxPath) || Immutable.List()).map(item => {
      const productId = item.get('product');
      const details = detailedIngredients.get(productId);
      if (!details) return item;
      return getInitialFormValue(productId, details, productIdToSourcesMap, supplierIdToNameMap, item, purchaseWeightSystem, purchaseVolumeSystem, ownProps.setPars);
    }).toJS();

    return {
      allMeasures: state.getIn(['measures', 'measures']),
      productIdToSourcesMap,
      supplierIdToNameMap,
      itemListReduxPath,
      refreshItemList,
      itemActions,
      initialValues: { items },
      detailedIngredients,
      ingredientsNameMap: Immutable.Map((state.get('ingredients') || Immutable.List()).map(i => [i.get('id'), i.get('name')])),
      purchaseWeightSystem,
      purchaseVolumeSystem,
      onboardingInfo: state.get('onboardingInfo'),
      shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
      supplierSourcedIngredients: state.get('supplierSourcedIngredients'),
      mainInfoLoaded,
      mainInfoLoadedReduxPath,
      overwriteOnInitialValuesChange: false,
    };
  },
)
class TakeInventory extends Component {

  constructor(props) {
    super(props);
    // TODO: Move proper state into TakeInventoryItems
    this.state = {
      displayedItemsIds: Immutable.Set(),
      showAddIngredientModal: false,
      openIngredientCostModalProduct: null,
      toDeleteInventoryItem: null,
      showAddToCategoryModal: false,
      showRemoveFromCategoryModal: false,
      showNewIngredientModal: false,
      showNewUnitModal: false,
      showShrinkageModal: false,

      unmergedPreviousInventory: true,
      mergingPreviousInventory: false,
      mergedPreviousInventory: false,
    };
    this.updatedCostProductIds = Immutable.Set();
    this.removedProductIds = Immutable.Set();

    this.updateDisplayedItemsIds = displayedItemsIds => {
      if (!displayedItemsIds.equals(this.state.displayedItemsIds)) {
        this.setState({ displayedItemsIds });
      }
    };

    [ 'submit',
      'showAddIngredientModal',
      'closeAddIngredientModal',
      'addIngredient',
      'openIngredientCostModal',
      'closeIngredientCostModal',
      'openAddToCategoryModal',
      'closeAddToCategoryModal',
      'openRemoveFromCategoryModal',
      'closeRemoveFromCategoryModal',
      'openNewIngredientModal',
      'closeNewIngredientModal',
      'showDeleteConfirmation',
      'hideDeleteConfirmation',
      'openShrinkageModal',
      'closeShrinkageModal',
      'deleteInventoryItem',
      'calculateCost',
      'getTotalCost',
      'mergePreviousInventory',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });
  }

  // Check if an inventory item has changed?
  inventoryItemHasChanged(item) {
    const fieldItem = this.props.fields.items.find(i => i.product.value === item.product);
    if (!fieldItem) {
      return true;
    }

    if (typeof fieldItem.quantity.amount.value !== 'number') {
      return false;
    }

    if (fieldItem.quantity.amount.value !== fieldItem.quantity.amount.initialValue) {
      return true;
    }

    if (_.some(quantityFormFields, field => fieldItem.quantity[field].dirty)) {
      return true;
    }

    return _.some(['packageName', 'packageSize', 'packaged', 'subPackageName', 'superPackageSize', 'supplier', 'supplierName'], field =>
      fieldItem[field].dirty
    );
  }

  // Only submit changes to support multiple users operating at the same time
  submit(values) {

    const changedItems = values.items.filter(item =>
      item.manuallyAdded || // new item
      this.updatedCostProductIds.contains(item.product) ||
      this.inventoryItemHasChanged(item)
    );

    const deets = changedItems.map(i => {
      delete i.id;
      i.cost = this.calculateCost(i.product);
      quantityFormFields.forEach(field => {
        i[field] = i.quantity[field];
      });
      delete i.quantity;

      // Apply `PackagedAmountInput`'s logic to no-source items
      // See PARS-1496
      if (!i.selectedSource && i.amount) {
        i.amount *= i.packageSize * i.superPackageSize;
      }

      return i;
    });

    const saveFunc = this.props.setPars ? saveIngredientPars : saveInventory;

    saveFunc(deets, this.removedProductIds, !this.state.unmergedPreviousInventory).then(() => {
      this.clearRouteLeaveHook();
      history.push(this.props.setPars ? '/orders' : '/inventories');
    });
  }

  showAddIngredientModal() {
    this.setState({
      showAddIngredientModal: true,
    });
  }

  closeAddIngredientModal() {
    this.setState({
      showAddIngredientModal: false,
    });
  }

  addIngredient(productId, fullName) {

    const {
      fields: { items },
      supplierIdToNameMap,
      purchaseWeightSystem,
      purchaseVolumeSystem,
      setPars,
      itemActions,
      productIdToSourcesMap,
    } = this.props;

    // Assume that this productId hasn't been used
    // See AddInventoryItemModal's usedIngredientIdsMap
    this.closeAddIngredientModal();
    getProductUsageInfo(productId).then(details => {
      const newItem = getInitialFormValue(productId, Immutable.fromJS(details),
          productIdToSourcesMap, supplierIdToNameMap,
          Immutable.fromJS({
            id: productId,
            product: productId,
            name: fullName || details.name, // details.name just the prep name for advanced preps, not the full name
          }),
          purchaseWeightSystem, purchaseVolumeSystem,
          setPars,
      ).toJS();

      newItem.manuallyAdded = true;
      // RF-MIGRATION: Use array field helpers
      items.addField(newItem);
      window.reduxStore.dispatch(itemActions.addIngredient(newItem));
      this.addedOrDeletedInventory = true;
    });
  }

  showDeleteConfirmation(productId, name) {
    this.setState({
      toDeleteInventoryItem: { productId, name },
    });
  }

  hideDeleteConfirmation() {
    this.setState({
      toDeleteInventoryItem: null,
    });
  }

  deleteInventoryItem() {
    const productId = this.state.toDeleteInventoryItem.productId;
    const idx = this.props.fields.items.findIndex(
      i => i.product.value === productId
    );
    this.props.fields.items.removeField(idx);
    window.reduxStore.dispatch(this.props.itemActions.removeIngredient(productId));
    this.hideDeleteConfirmation();
    this.addedOrDeletedInventory = true;
    this.removedProductIds = this.removedProductIds.add(productId);
  }

  openIngredientCostModal(product) {
    this.setState({
      openIngredientCostModalProduct: product,
    });
  }

  closeIngredientCostModal() {
    this.setState({
      openIngredientCostModalProduct: null,
    });
  }

  openAddToCategoryModal(items) {
    this.setState({
      showAddToCategoryModal: true,
      addingToCategoryInventories: items,
    });
  }

  closeAddToCategoryModal() {
    this.setState({
      showAddToCategoryModal: false,
    });
  }

  openRemoveFromCategoryModal(items) {
    this.setState({
      showRemoveFromCategoryModal: true,
      removingFromCategoryInventories: items,
    });
  }

  closeRemoveFromCategoryModal() {
    this.setState({
      showRemoveFromCategoryModal: false,
    });
  }

  openNewIngredientModal(newIngredient) {
    this.setState({
      showNewIngredientModal: newIngredient,
    });
  }

  closeNewIngredientModal() {
    this.setState({
      showNewIngredientModal: false,
    });
  }


  openShrinkageModal() {
    this.setState({
      showShrinkageModal: true,
    });
  }

  closeShrinkageModal() {
    this.setState({
      showShrinkageModal: false,
    });
  }

  // We might want to re-calculate cost only when either the amount or packaging input has changed
  // It might be tricky since we support quite a few different types of input though
  calculateCost(productId) {
    const {
      productIdToSourcesMap, detailedIngredients,
      fields,
    } = this.props;

    const itemDetails = detailedIngredients.get(productId);
    if (!itemDetails) return undefined;
    const fieldItem = fields.items.find(i => i.product.value === productId);
    if (!fieldItem || typeof fieldItem.quantity.amount.value !== 'number') return undefined;

    const source = (productIdToSourcesMap[productId] || Immutable.List()).find(
      s => s.get('id') === fieldItem.selectedSource.value
    );

    const conversions = itemDetails.get('measures');

    if (itemDetails.has('isPrepOf')) {
      return convertPerQuantityValue(itemDetails.get('unitCost'),
          1, itemDetails.get('primaryUnit'),
          fieldItem.quantity.amount.value, fieldItem.quantity.unit.value,
          conversions
      );
    } else if (itemDetails.get('inHouse')) {
      return convertPerQuantityValue(itemDetails.get('unitCost'),
          1, itemDetails.get('primaryUnit'),
          fieldItem.quantity.amount.value, fieldItem.quantity.unit.value,
          conversions
      );
    } else if (source && typeof fieldItem.quantity.amount.value === 'number' && fieldItem.quantity.unit.value) {
      const packageSize = costingSize(source);
      return convertPerQuantityValue(source.get('cost'),
          packageSize.amount, packageSize.unit,
          fieldItem.quantity.amount.value, fieldItem.quantity.unit.value,
          conversions
      );
    }

    return undefined;
  }

  getTotalCost() {
    return this.state.displayedItemsIds.map(id => this.calculateCost(id))
      .reduce((sum, cost) => cost ? sum + cost : sum, 0);
  }

  mergePreviousInventory() {
    this.setState({
      unmergedPreviousInventory: false,
      mergingPreviousInventory: true,
    });

    getPreviousInventoryDetails().then(details => {
      if (details && details.items) {
        details.items.forEach(item => {
          if (typeof item.amount === 'number') {
            const todayFieldItem = this.props.fields.items.find(i => i.product.value === item.product);
            if (todayFieldItem && !todayFieldItem.quantity.amount.value) {
              todayFieldItem.quantity.amount.onChange(item.amount);
            }
          }
        });
      }

      this.setState({
        mergingPreviousInventory: false,
        mergedPreviousInventory: true,
      });
    });
  }

  render() {

    const {
      fields: { items },
      productIdToSourcesMap,
      submitting, deleting, editingDisabled, handleSubmit, untouch,
      onboardingInfo,
      mainInfoLoaded,
      setPars,
      itemListReduxPath,
      refreshItemList,
      itemActions,
      shouldShowOnboarding,
      detailedIngredients,
      ingredientsNameMap,
    } = this.props;

    if (!mainInfoLoaded) {
      return <Col xs={12} md={10} mdOffset={2}>
        <LoadingSpinner />
      </Col>;
    }

    let usedIngredientIdsMap = {};
    items.forEach(i => {
      usedIngredientIdsMap[i.product.value] = true;
    });

    // For the Merge-Previous-Inventory button
    let mergePreviousInventoryLabel = 'Merge Previous Inventory';
    if (this.state.mergingPreviousInventory) {
      mergePreviousInventoryLabel = 'Merging...';
    } else if (this.state.mergedPreviousInventory) {
      mergePreviousInventoryLabel = 'Previous Inventory Merged';
    }
    const mergePreviousInventoryTooltip = <Tooltip>
      Merges the values from your previous inventory, so you can continue taking inventory started on a previous day
    </Tooltip>;
    const mergePreviousInventoryButton = <Button
      className="green-btn"
      style={{ width: '200px' }}
      onClick={this.mergePreviousInventory}
      disabled={!this.state.hasPreviousInventories || !this.state.unmergedPreviousInventory}
    >
      {mergePreviousInventoryLabel}
    </Button>;

    return <Grid fluid style={{ display: 'relative' }} className="inventory-details--page">
      <Row className="indent-bottom">
        <Col xs={12} md={10} mdOffset={2}>
          <Row>
            <Col className="hidden-xs" xs={2}>
              <Link className="crud-back-link" to={{
                pathname: setPars ? '/orders' : '/inventories',
              }}>
                &lt; {setPars ? 'Ordering' : 'Inventory'}
              </Link>
            </Col>
            <Col style={{ width: '975px' }} className="crud-top-buttons">
              {setPars ? null :
                this.state.unmergedPreviousInventory ? mergePreviousInventoryButton
                  : <OverlayTrigger placement="bottom" overlay={mergePreviousInventoryTooltip}>
                    {mergePreviousInventoryButton}
                  </OverlayTrigger>}
              {setPars ? null :
                <Button
                  className="green-btn"
                  onClick={() => downloadCSVData(detailedIngredients, ingredientsNameMap, {
                    date: moment().format('YYYYMMDD'),
                    items: items.map(i => {
                      const quantity = shallowToObject(i.quantity);
                      i = shallowToObject(i);
                      i.cost = this.calculateCost(i.product);
                      quantityFormFields.forEach(field => {
                        i[field] = quantity[field];
                      });
                      return i;
                    }),
                  }, true)}
                >
                  Export to CSV
                </Button>}
              <Button disabled={submitting || deleting || editingDisabled}
                onClick={handleSubmit(this.submit)}
                bsStyle={submitting ? 'default' : 'primary'}
                className="green-btn">
                { submitting ? 'saving' : 'save'}
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col mdOffset={2}>
          <h1 style={{ fontSize: '30px', margin: '6px 0px 25px' }}>
            {setPars ? 'Set Par Levels' : moment().format(dateWithWeekdayFormat)}
          </h1>
        </Col>
      </Row>

      <FieldArray component={TakeInventoryItems} fields={items}
          props={{
            updateDisplayedItemsIds: this.updateDisplayedItemsIds,
            displayedItemsIds: this.state.displayedItemsIds,
            showDeleteConfirmation: this.showDeleteConfirmation,
            openIngredientCostModal: this.openIngredientCostModal,
            openAddToCategoryModal: this.openAddToCategoryModal,
            openRemoveFromCategoryModal: this.openRemoveFromCategoryModal,
            calculateCost: this.calculateCost,
            getTotalCost: this.getTotalCost,
            showAddIngredientModal: this.showAddIngredientModal,
            productIdToSourcesMap,
            ingredientsNameMap,
            untouch,
            handleSubmit: handleSubmit(this.submit),
            submitting,
            setPars,
            itemListReduxPath,
            hasPreviousInventories: this.state.hasPreviousInventories,
            openShrinkageModal: this.openShrinkageModal,
          }}
      />

      {this.state.showAddIngredientModal ?
        <AddInventoryItemModal
          usedIngredientIdsMap={usedIngredientIdsMap}
          onChange={this.addIngredient}
          openNewIngredientModal={this.openNewIngredientModal}
          hide={this.closeAddIngredientModal} />
        : null}

      {this.state.showNewIngredientModal ?
        <NewIngredientModal key="new-ingredient-modal"
          newIngredient={this.state.showNewIngredientModal}
          hide={this.closeNewIngredientModal} />
        : null}

      {this.state.openIngredientCostModalProduct ?
        <IngredientSourcesModal
          ingredientId={this.state.openIngredientCostModalProduct.productId}
          preEnteredSource={this.state.openIngredientCostModalProduct.preEnteredSource}
          postSave={ingredientId => {
            this.updatedCostProductIds = this.updatedCostProductIds.add(ingredientId);
            getProductDetails(ingredientId).then(details => {
              const itemInField = items.find(i => i.product.value === ingredientId);
              let source;
              if (details.purchaseInfo.preferredSource) {
                source = details.purchaseInfo.preferredSource;
              } else {
                source = details.purchaseInfo.otherSources[0];
              }
              getSupplierSourcedIngredients(source.supplier)
                .then(() => {
                  // Negate `PackagedAmountInput`'s logic, see PARS-1614
                  itemInField.quantity.amount.onChange(itemInField.quantity.amount.value * source.packageSize);
                  itemInField.selectedSource.onChange(source.id);
                });
            });
          }}
          hide={this.closeIngredientCostModal} />
        : null}

      {this.state.toDeleteInventoryItem ?
        <DeleteConfirmationModal
          titleNoun={this.state.toDeleteInventoryItem.name}
          onHide={this.hideDeleteConfirmation}
          onDelete={this.deleteInventoryItem}
          deleteVerb="remove"
          fullDescription={`Are you sure you want to remove ${this.state.toDeleteInventoryItem.name}?`}/>
      : null}

      {this.state.showAddToCategoryModal ?
        <AddToCategoryModal
          hide={() => this.closeAddToCategoryModal()}
          tagType="inventories"
          targetType={itemListReduxPath}
          targetIds={this.state.addingToCategoryInventories}
          onSubmitSuccess={tags => {
            window.reduxStore.dispatch(
              itemActions.addIngredientToCategory(this.state.addingToCategoryInventories, tags)
            );

            setTimeout(refreshItemList, 10);
            window.reduxStore.dispatch(SearchableListActions.uncheckAll(itemListReduxPath));
          }}
        />
        : null}

      {this.state.showRemoveFromCategoryModal ?
        <RemoveFromCategoryModal
          hide={() => this.closeRemoveFromCategoryModal()}
          tagType="inventories"
          targetType={itemListReduxPath}
          targetIds={this.state.removingFromCategoryInventories}
          onSubmitSuccess={tags => {
            window.reduxStore.dispatch(
              itemActions.removeIngredientFromCategory(this.state.removingFromCategoryInventories, tags)
            );

            setTimeout(refreshItemList, 10);
            window.reduxStore.dispatch(SearchableListActions.uncheckAll(itemListReduxPath));
          }}
        />
        : null}

      {this.state.showShrinkageModal ?
        <InventoryShrinkageModal
          items={this.props.fields.items.filter(i => i.quantity.amount.value)}
          detailedIngredients={detailedIngredients}
          hide={this.closeShrinkageModal}
        />
      : null}

      <Onboarding pageKey="inventory_editor" />
    </Grid>;
  }

  componentWillMount() {

    const { router, route, setPars, mainInfoLoadedReduxPath } = this.props;

    const _getNewInventoryItemList = () => getNewInventoryItemList().then(todayData => {
      if (todayData.mergedPreviousInventory) this.setState({ unmergedPreviousInventory: false, mergedPreviousInventory: true });
      return todayData.items;
    });
    let getItemList = setPars ? getIngredientPars : _getNewInventoryItemList;

    Promise.all([
      getInventoryLastTakenTime().then(lastTakenTime =>
        this.setState({ hasPreviousInventories: Boolean(lastTakenTime) }),
      ),
      getIngredientList(),
      getUserInfo(),
      getMeasures(),
      getSupplierList().then(suppliers => Promise.all(
          suppliers.map(s => getSupplierSourcedIngredients(s.id)),
      )),
      getOnboardingInfo(),
      getItemList().then(items => {
        const ingredientIds = items.map(i => i.product);
        const [newItems, previousItems] = _.partition(ingredientIds, id => !lastItemsId.includes(id));
        lastItemsId = Immutable.Set(ingredientIds);

        const getNewItems = getMultipleProductsUsageInfo(newItems, 25);
        const getPreviousItems = getMultipleProductsUsageInfo(previousItems, 50, { modifiedAfterDate });

        modifiedAfterDate = moment().utc().format(); // update for next fetch

        return Promise.all([getNewItems, getPreviousItems]);
      })
    ])
    .then(() => window.reduxStore.dispatch(MainInfoLoadedActions.setMainInfoLoaded(mainInfoLoadedReduxPath, true)));

    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty || this.addedOrDeletedInventory) {
        return 'You have unsaved changes which will be lost if you leave without saving. Do you want to leave this page?';
      } else {
        return null;
      }
    });
  }

  componentWillUnmount() {
    window.reduxStore.dispatch(MainInfoLoadedActions.setMainInfoLoaded(this.props.mainInfoLoadedReduxPath, false));
  }
}

@withRouter
@connect(
    (state, ownProps) => ({
      listUiState: state.getIn(['searchableList', ownProps.itemListReduxPath]),
      detailedIngredients: state.get('detailedIngredients'),
      purchaseWeightSystem: state.getIn(['userInfo', 'purchaseWeightSystem']),
      purchaseVolumeSystem: state.getIn(['userInfo', 'purchaseVolumeSystem']),
      units: state.getIn(['measures', 'units']),
      allIngredients: state.get('ingredients'),
      allMeasures: state.getIn(['measures', 'measures']),
    }),
    (dispatch, ownProps) => _(bindActionCreators(SearchableListActions, dispatch))
        .omit('showDeleteConfirmation')
        .mapValues(creator => creator.bind(this, ownProps.itemListReduxPath))
        .value(),
)
class TakeInventoryItems extends Component {

  render() {
    const {
      listUiState, setCheckedItems,
      showAddIngredientModal,
      openAddToCategoryModal, openRemoveFromCategoryModal,
      submitting, handleSubmit,
      updateDisplayedItemsIds,
      displayedItemsIds,
      getTotalCost,
      itemListReduxPath,
      setPars,
      hasPreviousInventories,
      openShrinkageModal,
    } = this.props;

    const checkedItems = listUiState && listUiState.get('checkedItems');
    const canSetSelectAll = checkedItems && checkedItems.intersect(displayedItemsIds).size > 0;
    const selectAllChecked = canSetSelectAll && displayedItemsIds.isSubset(checkedItems);
    const selectAllIndeterminate = canSetSelectAll && !displayedItemsIds.isSubset(checkedItems);
    const newButton = <span className="new-link" onClick={showAddIngredientModal}>+ Add Ingredient</span>;

    return <SearchableList
        noun="Ingredient"
        searchKey="ingredients"
        reduxStatePath={itemListReduxPath}
        baseUrl={this.props.location.pathname}
        newButton={newButton}
        ListComponent={setPars ? ParListComponent : InventoryItemsListComponent}
        checkable
        deletable={false}
        taggable tagTypes={['inventories', 'ingredients']}
        tagTooltips={{
          ingredients: 'Add GL Categories from within the Ingredient Editor, or \
          if you want to do it in bulk by selecting a group and doing Add to Category, \
          do that on the Ingredient List page'
        }}
        sortItemsFunc={item => item.get('name').toLowerCase()}
        searchOpts={ingredientListSearchOpts}
        updateDisplayedItemsIds={updateDisplayedItemsIds}
        getClassnames={item => item.get('salable') ? ['text-italic'] : []}
        shouldScroll={item => this.props.location.state && item.get('product') === this.props.location.state.prevObjId}
        SectionItemComponent={TakeInventoryItem}
        sectionItemComponentsProps={this.props}
    >
      <div style={{ position: 'absolute', top: '-70px', right: '170px', fontSize: '17px' }}>
        Total Value: &nbsp;
        <CurrencyDisplay value={getTotalCost()} />
      </div>
      <Button
          onClick={() => { if (this.selectAllCheckbox) this.selectAllCheckbox.click(); }}
          style={{ width: '121px', textAlign: 'left' }}>
        <Checkbox
            ref={checkbox => this.selectAllCheckbox = checkbox ? checkbox.input : null}
            inline
            checked={selectAllChecked}
            indeterminate={selectAllIndeterminate}
            onChange={ev => ev.target.checked ? setCheckedItems(displayedItemsIds) : setCheckedItems(Immutable.Set())}
            style={{ pointerEvents: 'none', marginBottom: '-2px' }}
        /> {selectAllChecked ? 'Deselect All' : 'Select All'}
      </Button>
      <Button
          disabled={!(checkedItems && checkedItems.size)}
          onClick={() => openAddToCategoryModal(checkedItems)}>
        Add to Location
      </Button>
      <Button
          disabled={!(checkedItems && checkedItems.size)}
          onClick={() => openRemoveFromCategoryModal(checkedItems)}>
        Remove from Location
      </Button>
      {setPars ? null :
        <FeatureGuardedButton
          feature="INVENTORY_SHRINKAGE"
          className="green-btn"
          disabled={!hasPreviousInventories}
          onClick={openShrinkageModal}
          style={{ position: 'absolute', right: 'calc(100% - 980px)' }}
        >
          Shrinkage
        </FeatureGuardedButton>
      }
      <div style={{ position: 'absolute', bottom: '-50px', right: '100px', paddingBottom: '30px', fontSize: '17px' }}>
        Total Value: &nbsp;
        <CurrencyDisplay value={getTotalCost()} />
        <Button
          style={{ marginLeft: '30px' }}
          disabled={submitting}
          onClick={handleSubmit}
          bsStyle={submitting ? 'default' : 'primary'}
          className="green-btn">
          {submitting ? 'saving' : 'save'}
        </Button>
      </div>
    </SearchableList>;
  }
}

class TakeInventoryItem extends Component {

  shouldComponentUpdate(nextProps) {
    const productId = this.props.item.get('product');

    // Checkbox's state has changed?
    if (this.props.listUiState.get('checkedItems').includes(productId) !== nextProps.listUiState.get('checkedItems').includes(productId)) {
      return true;
    }

    const nextProductId = nextProps.item.get('product');
    if (productId !== nextProductId) return true;

    const nextFieldItem = nextProps.fields.find(i => i.product.value === productId);
    if (!nextFieldItem) return true;

    // when `amount` is changed,
    // its value in `this.props` is always updated before `shouldComponentUpdate` is called
    // we have to save it to `this.last_amount` or else `this-amount` and `next-amount` will always be equal
    // the cause might be that `redux-form` doesn't use Immutable for its fields
    const quantityChanged = _.some(['amount', 'measure', 'unit'], field => {
      if (nextFieldItem.quantity[field].value !== this['last_' + field]) {
        this['last_' + field] = nextFieldItem.quantity[field].value;
        return true;
      }
      return false;
    });
    if (quantityChanged) return true;

    // remember to show error tooltips (see PARS-2013)
    if (!nextFieldItem.quantity.measure.value || !nextFieldItem.quantity.unit.value) return true;

    const fieldItem = this.props.fields.find(i => i.product.value === productId);
    return !_.isEqual(shallowToObject(fieldItem), shallowToObject(nextFieldItem));
  }

  render() {
    const {
      item, fields,
      ingredientsNameMap,
      detailedIngredients,
      listUiState, checkItem, uncheckItem, units, productIdToSourcesMap, allMeasures, untouch,
      calculateCost,
      openIngredientCostModal,
      showDeleteConfirmation,
      setPars,
      purchaseWeightSystem,
      purchaseVolumeSystem,
    } = this.props;

    const productId = item.get('product');
    const itemDetails = detailedIngredients.get(productId);
    if (!itemDetails) return null;
    const fieldItem = fields.find(i => i.product.value === productId);
    if (!fieldItem) return null;

    const conversions = itemDetails.get('measures');
    const source = (productIdToSourcesMap[productId] || Immutable.List()).find(s => s.get('id') === fieldItem.selectedSource.value);
    const cost = calculateCost(productId);

    let packagedAmountInput, costInfo;

    // Advanced preps
    if (itemDetails.has('isPrepOf')) {
      if (!fieldItem.quantity.unit.value) return <td/>;

      packagedAmountInput = <td className="inventory-quantity">
        <Validate {...fieldItem.quantity.amount}>
          <MaskedNumberInput
              {...fieldItem.quantity.amount}
              normalizeInitial
              style={{ marginLeft: '3px' }}
          />
          <span style={{ color: '#000', marginLeft: '15px' }}>
            {unitDisplayName(fieldItem.quantity.unit.value)}
            &nbsp;&nbsp;
            (In-House)
          </span>
        </Validate>
      </td>;

      if (itemDetails.get('unitCost')) {
        costInfo = <CurrencyDisplay value={cost} matchVertical/>;
      } else {
        costInfo = <div>No cost info</div>;
      }


    } else if (itemDetails.get('inHouse')) {
      let unitOptions = Immutable.List([itemDetails.get('primaryUnit')]);

      let userPurchaseSystem = 'metric';
      if (itemDetails.get('primaryMeasure') === 2) userPurchaseSystem = purchaseWeightSystem;
      if (itemDetails.get('primaryMeasure') === 3) userPurchaseSystem = purchaseVolumeSystem;
      const commonPurchaseUnits = getCommonPurchaseUnits(itemDetails.get('primaryMeasure'), userPurchaseSystem === 'metric').map(u => u.get('id'));
      unitOptions = unitOptions.concat(commonPurchaseUnits).toSet().map(u => {
        let label = unitDisplayName(u) + '\u2003(In-House)';
        return {
          value: u,
          label,
        };
      }).toArray();

      packagedAmountInput = <td className="inventory-quantity">
        <Validate {...fieldItem.quantity.amount}>
          <MaskedNumberInput
              {...fieldItem.quantity.amount}
              normalizeInitial
              style={{ marginLeft: '3px' }}
          />
          <InputGroup.Addon>
            { unitOptions.length === 1 ?
              <div style={{ textAlign: 'left', paddingLeft: '13px', width: '165px' }}>{unitOptions[0].label}</div>
              : <Select
                options={unitOptions}
                {...fieldItem.quantity.unit}
              />
            }
          </InputGroup.Addon>
        </Validate>
      </td>;

      costInfo = <CurrencyDisplay value={cost} matchVertical/>;

    } else if (source) {
      packagedAmountInput = <td className="inventory-quantity">
        <PackagedAmountInput
            defaultEmptyValue={setPars ? 0 : ''}
            sources={productIdToSourcesMap[productId] || Immutable.List()}
            quantityFields={fieldItem.quantity}
            selectedSource={fieldItem.selectedSource}
            units={units}
            packagedField={fieldItem.packaged}
            packageNameField={fieldItem.packageName}
            subPackageNameField={fieldItem.subPackageName}
            packageSizeField={fieldItem.packageSize}
            superPackageSizeField={fieldItem.superPackageSize}
            supplierField={fieldItem.supplier}
            supplierNameField={fieldItem.supplierName}
            conversions={conversions}
            amountInputStyle={{ padding: '0px ' + spaceToAmountInput }}
            autoAdjust={false}
        />
      </td>;

      costInfo = <CurrencyDisplay value={cost} matchVertical/>;

    } else {
      const fields = {
        unit: fieldItem.quantity.unit,
        measure: fieldItem.quantity.measure,
        packaged: fieldItem.packaged,
        packageName: fieldItem.packageName,
        subPackageName: fieldItem.subPackageName,
        packageSize: fieldItem.packageSize,
        superPackageSize: fieldItem.superPackageSize,
      };

      packagedAmountInput = <td className="inventory-source">
        <table style={{ paddingLeft: '7px' }}>
          <tbody>
          <tr>
            <td style={{ padding: '0px ' + spaceToAmountInput }}>
              <Validate {...fieldItem.quantity.amount}>
                <MaskedNumberInput
                    {...fieldItem.quantity.amount}
                    normalizeInitial
                />
              </Validate>
            </td>
            <td style={{ paddingLeft: '2px', maxWidth: '75px' }}>
              <RootUnitInput
                  conversions={conversions} measures={allMeasures}
                  fields={fields}
                  untouch={untouch}
              />
            </td>
            <td style={{ padding: '0px 5px', maxWidth: '136px' }}>
              <PackageSizeInput handleQuantityChange={this.handleQuantityChange}
                  conversions={conversions} measures={allMeasures}
                  fields={fields}
                  untouch={untouch}
              />
            </td>
            <td style={{ paddingLeft: '0px' }}
                className={fieldItem.subPackageName.value ? 'take-inventory-sub-package-size' : ''}
            >
              <SubPackageSizeInput
                  conversions={conversions} measures={allMeasures}
                  fields={fields}
              />
            </td>
          </tr>
          </tbody>
        </table>
      </td>;

      costInfo = <div>
        <Button style={{ height: '25px', fontSize: '12px' }} onClick={() => {

          let preEnteredSource = {
            ...newSourceTemplate,
            id: -1,
            unit: fieldItem.quantity.unit.value,
            measure: fieldItem.quantity.measure.value,
            superPackageSize: fieldItem.superPackageSize.value || 1,
            packageSize: fieldItem.packageSize.value || 1,
            packaged: fieldItem.packaged.value || false,
            preferred: true,
          };

          ['packageName', 'subPackageName'].forEach(field => {
            if (fieldItem[field].value != null && fieldItem[field].value !== '') {
              preEnteredSource[field] = fieldItem[field].value;
            }
          });

          if (preEnteredSource.subPackageName) {
            preEnteredSource.pricePer = 'super-package';
          } else if (preEnteredSource.packaged) {
            preEnteredSource.pricePer = 'package';
          } else {
            preEnteredSource.pricePer = 'unit';
            preEnteredSource.pricingMeasure = fieldItem.quantity.measure.value;
            preEnteredSource.pricingUnit = fieldItem.quantity.unit.value;
          }

          openIngredientCostModal({
            productId,
            preEnteredSource,
          });
        }}>
          Update
        </Button>
        &nbsp;&nbsp;
        No cost info
      </div>;
    }

    let name = itemDetails.get('name');
    let nameStyle = {};
    if (itemDetails.has('isPrepOf')) {
      name = <span>
        {ingredientsNameMap.get(itemDetails.get('isPrepOf'))}
        &nbsp;-&nbsp;
        <span style={{ textDecoration: 'underline' }}>{name}</span>
      </span>;
    } else if (itemDetails.get('inHouse')) {
      nameStyle.fontStyle = 'italic';
    }

    return <tr key={productId} className="inventory-item-row">
      <td className="inventory-name">
        <Checkbox
            inline
            checked={listUiState.get('checkedItems').includes(productId)}
            style={nameStyle}
            onChange={ev => ev.target.checked
                ? checkItem(productId)
                : uncheckItem(productId)
            }>{name}</Checkbox>
      </td>
      {packagedAmountInput}
      <td className="inventory-cost">
        {costInfo}
      </td>
      <td className="inventory-remove-link">
                <span
                    onClick={() => showDeleteConfirmation(
                      productId,
                      (itemDetails.has('isPrepOf') ? ingredientsNameMap.get(itemDetails.get('isPrepOf')) + ' - ' : '') + itemDetails.get('name')
                    )}
                    className="pull-right action-link">
                  remove
                </span>
      </td>
    </tr>;
  }
}

function getPastInventoryItemQuantityOnHand(item, conversions, allowEmptyAmountPackaging) {
  if (!item.unit) item.unit = getPrimaryUnitId(conversions); // TODO: Do this server-side to reduce response size?
  const source = Immutable.fromJS(item);
  const packageSize = effectivePackageSize(source);
  return {
    amount: typeof item.amount === 'number' && packageSize.amount && item.unit ? truncated(divideQuantities(item, packageSize, conversions), 2) : '',
    packaging: (allowEmptyAmountPackaging || item.amount) && item.unit ? PackagedAmountInput.fullPackageName(source, conversions) : '',
  };
}

@connect(
  state => ({
    ingredientsNameMap: Immutable.Map((state.get('ingredients') || Immutable.List()).map(i => [i.get('id'), i.get('name')])),
    onboardingInfo: state.get('onboardingInfo'),
    inventoryDetails: state.get('inventoryDetails'),
    detailedIngredients: state.get('detailedIngredients'),
    shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
  })
)
class InventoryView extends Component {

  render() {
    const loadingSpinner = <Col xs={12} md={10} mdOffset={2}>
      <LoadingSpinner />
    </Col>;

    const { ingredientsNameMap, inventoryDetails, detailedIngredients, onboardingInfo, shouldShowOnboarding } = this.props;
    if (!inventoryDetails || !detailedIngredients) return loadingSpinner;

    const inventoryItemIds = inventoryDetails.items.map(i => i.product);
    const fetchedDetailsIds = detailedIngredients.keySeq();
    if (!Immutable.Seq(inventoryItemIds).isSubset(fetchedDetailsIds)) return loadingSpinner;

    const formattedDate = moment(inventoryDetails.date).format(dateWithWeekdayFormat);

    let totalCost = 0;
    const tableBody = inventoryDetails.items
      .sort((a, b) => {
        const aDetails = detailedIngredients.get(a.product);
        const bDetails = detailedIngredients.get(b.product);
        if (!aDetails && !bDetails) return 0;
        if (!aDetails) return 1;
        if (!bDetails) return -1;
        let aName = aDetails.get('name');
        if (aDetails.has('isPrepOf')) aName = ingredientsNameMap.get(aDetails.get('isPrepOf')) + ' - ' + aName;
        let bName = bDetails.get('name');
        if (bDetails.has('isPrepOf')) bName = ingredientsNameMap.get(bDetails.get('isPrepOf')) + ' - ' + bName;
        return aName.localeCompare(bName);
      })
      .map(item => {
        const details = detailedIngredients.get(item.product);
        if (!details) return null;
        const {
          amount,
          packaging,
        } = getPastInventoryItemQuantityOnHand(item, details.get('measures'));

        if (item.cost) totalCost += item.cost;

        let name = details.get('name');
        let nameStyle = {};
        if (details.has('isPrepOf')) {
          name = <span>
            {ingredientsNameMap.get(details.get('isPrepOf'))}
            &nbsp;-&nbsp;
            <span style={{ textDecoration: 'underline' }}>{name}</span>
          </span>;
        } else if (details.get('inHouse')) {
          nameStyle.fontStyle = 'italic';
        }

        return <tr key={item.product}>
          <td
            className="inventory-name"
            style={nameStyle}
          >
            {name}
          </td>
          <td>
            {amount}
            &nbsp;&nbsp;
            {packaging}
          </td>
          <td style={{ textAlign: 'right', paddingRight: '20px' }}>
            <CurrencyDisplay value={item.cost} matchVertical/>
          </td>
        </tr>;
      });

    return <Grid fluid>
      <Row className="indent-bottom">
        <Col xs={12} md={10} mdOffset={2}>
          <Row>
            <Col className="hidden-xs" xs={2}>
              <Link className="crud-back-link" to={{
                pathname: '/inventories',
              }}>
                &lt; Inventory
              </Link>
            </Col>
            <Col style={{ width: '600px' }}>
              <Button
                style={{ position: 'absolute', left: '455px', top: '18px' }}
                onClick={() => downloadCSVData(detailedIngredients, ingredientsNameMap, inventoryDetails)}
              >
                Export to CSV
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col mdOffset={2}>
          <h1 style={{ fontSize: '30px', margin: '6px 0px 25px' }}>
            {formattedDate}
          </h1>
          <div style={{ textAlign: 'right', marginTop: '-50px', paddingRight: '30px', width: '600px' }}>
            Total Value: &nbsp;
            <CurrencyDisplay value={totalCost} />
          </div>
        </Col>
      </Row>
      <Row style={{ marginTop: '50px' }}>
        <Col mdOffset={2}>
          <Table style={{ width: '600px', marginBottom: '20px' }}>
            <colgroup>
              <col style={{ width: '275px' }} />
              <col style={{ width: '200px' }} />
              <col style={{ width: '100px' }} />
            </colgroup>
            <thead>
              <tr>
                <th>Ingredient</th>
                <th>Quantity on Hand</th>
                <th style={{ textAlign: 'center' }}>Value</th>
                <th />
              </tr>
            </thead>
            <tbody>{tableBody}</tbody>
          </Table>
          <div style={{ textAlign: 'right', padding: '0px 30px 25px 0px', width: '600px' }}>
            Total Value: &nbsp;
            <CurrencyDisplay value={totalCost} />
          </div>
        </Col>
      </Row>
      <Onboarding pageKey="inventory_editor" />
    </Grid>;
  }

  componentWillMount() {
    getMeasures();
    getIngredientList();
    getOnboardingInfo();
    getInventoryDetails(this.props.params.id).then(inventory => {
      // Today inventory -> Redirect to TakeInventory
      if (inventory.date === moment().format('YYYY-MM-DD')) {
        this.props.router.push('/inventories/take')
      } else {
        getMultipleProductsUsageInfo(inventory.items.map(i => i.product), 25);
      }
    });
  }

  componentWillUnmount() {
    window.reduxStore.dispatch(InventoryDetailsActions.dropDetails());
  }
}

@connect(
  state => ({
    inventoryCurrent: state.get('inventoryCurrent'),
  }),
)
class CurrentInventory extends Component {

  render() {
    const loadingSpinner = <Col xs={12} md={10} mdOffset={2}>
      <LoadingSpinner />
    </Col>;

    const { inventoryCurrent } = this.props;
    if (!inventoryCurrent) return loadingSpinner;

    return <Grid fluid>
      <Row className="indent-bottom">
        <Col xs={12} md={10} mdOffset={2}>
          <Row>
            <Col className="hidden-xs" xs={2}>
              <Link className="crud-back-link" to={{
                pathname: '/inventories',
              }}>
                &lt; Inventory
              </Link>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col mdOffset={2}>
          <h3>Current Expected Inventory</h3>
          <Table style={{ width: '600px', marginBottom: '20px' }}>
            <colgroup>
              <col style={{ width: '375px' }} />
              <col style={{ width: '225px' }} />
            </colgroup>
            <thead>
              <tr>
                <th>Ingredient</th>
                <th>Expected Quantity on Hand</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {inventoryCurrent.map(item => {
                let name = item.get('name');
                if (item.get('originalIngredientName')) {
                  name = <span>
                    {item.get('originalIngredientName')}
                    &nbsp;-&nbsp;
                    <span style={{ textDecoration: 'underline' }}>{name}</span>
                  </span>;
                }

                const source = item.toJS();
                ['amount', 'measure', 'unit'].forEach(field => {
                  source[field] = source.quantity[field];
                });
                const {
                  amount,
                  packaging,
                } = getPastInventoryItemQuantityOnHand(source, item.get('measures'), true);
                return <tr key={item.product}>
                  <td className="inventory-name">
                    {name}
                  </td>
                  <td>
                    {amount || 0}
                    &nbsp;&nbsp;
                    {packaging}
                  </td>
                </tr>;
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Grid>;
  }

  componentWillMount() {
    getMeasures();
    getCurrentInventory();
  }
}

function downloadCSVData(detailedIngredients, ingredientsNameMap, inventoryDetails, allowEmptyAmountPackaging) {

  const numberOfLocations = detailedIngredients
      .valueSeq()
      .map(d => d.get('inventoryTags'))
      .maxBy(i => i.size).map((i, index) => `Location ${index + 1}`).toJS();
  const inventoryItems = inventoryDetails.items.map(i => {
    const details = detailedIngredients.get(i.product);
    let name = details.get('name');
    if (details.has('isPrepOf')) {
      name = ingredientsNameMap.get(details.get('isPrepOf')) + ' - ' + name;
    }
    const {
      amount,
      packaging,
    } = getPastInventoryItemQuantityOnHand(i, details.get('measures'), allowEmptyAmountPackaging);
    return [
      csvQuote(name),
      csvQuote(details.getIn(['ingredientTags', 0, 'name']) || ''),
      csvQuote(details.getIn(['ingredientTags', 1, 'name']) || ''),
      amount,
      csvQuote(packaging),
      csvQuote(i.supplierName || ''),
      i.cost ? '$' + formatNum(i.cost, { maxFraction: 2 }) : '',
      ...details.get('inventoryTags').map(it => csvQuote(it.get('name'))).toJS(),
    ];
  })
  .sort((row1, row2) => defaultComparator(row1[0], row2[0]));

  const header = [
    'Ingredient',
    'GL Category 1',
    'GL Category 2',
    'Quantity',
    'Unit of Measure',
    'Supplier',
    'Value',
    ...numberOfLocations,
  ].join(',');
  const csv = [header, ...inventoryItems].join('\n');

  downloadCSVDataAsFile(`parsley-inventory-${moment(inventoryDetails.date).format('YYYYMMDD')}.csv`, csv);
}

export default function InventoryDetails(props) {
  if (props.params.id === 'take') {
    return <TakeInventory {...props}/>;
  } else if (props.params.id === 'pars') {
    return <TakeInventory setPars {...props}/>;
  } else if (props.params.id === 'current') {
    return <CurrentInventory {...props}/>;
  } else {
    return <InventoryView {...props} />;
  }
}
