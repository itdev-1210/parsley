import React from 'react';

import _ from 'lodash';

import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { reduxForm } from 'redux-form';
import { history } from '../../navigation';

import { Row, Col, FormControl } from 'react-bootstrap';

import CrudEditor from './helpers/CrudEditor';

import TitleInput from '../TitleInput';
import Select from '../Select';
import OnboardingTooltip from '../OnboardingTooltip';

import Validate from 'common/ui-components/Validate';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import { actionCreators as OnboardingActions } from '../../reducers/onboardingInfo';

import { getOnboardingInfo, updateOnboardingInfo, saveOnboardingImage } from '../../webapi/endpoints';
import { readFileAsArrayBUffer, arrayBufferToBase64 } from '../../utils/file-utils';
import {
  valueFromEvent,
  convertFormValuesToOnboardingJson,
  onboardingStepFields,
} from '../../utils/form-utils';
import { checkFields, optional, required } from '../../utils/validation-utils';

const cssColorReg = /^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;

const validate = values => {
  const stepValidators = {
    page: required(),
    placement: required(),
    targetSelector: optional(),
    name: required(),
    title: optional(),
    description: required(),
    leftImage: optional(),
    rightImage: optional(),
    nextButtonText: required(),
    backButtonText: optional(),
    bodyWidth: required(),
    titleBgColor: required(),
    descBgColor: required(),
    borderColor: required(),
    nextBgColor: required(),
    closeBtnColor: required(),
  };

  const cssErrText = 'CSS hex color code required';
  let errors = checkFields(values, stepValidators);

  if (values.placement && values.placement !== 'center' && !values.targetSelector) {
    errors.targetSelector = 'required';
  }

  if (values.targetSelector) {
    // JS doesn't have a way to check for valid selector, but we can just try using it for
    // selection and see if we get an error
    try {
      // using dummy element instead of actual document because don't want to actually scan
      // whole tree on success
      const dummyElement = document.createElement('a');
      dummyElement.querySelector(values.targetSelector);
    } catch (domException) {
      errors.targetSelector = 'invalid selector';
    }
  }

  if (values.nextButtonText) {
    if (!values.nextBgColor) {
      errors.nextBgColor = 'required';
    } else if (!values.nextBgColor.match(cssColorReg)) {
      errors.nextBgColor = cssErrText;
    }
  }

  if (values.titleBgColor && !values.titleBgColor.match(cssColorReg)) {
    errors.titleBgColor = cssErrText;
  }
  if (values.descBgColor && !values.descBgColor.match(cssColorReg)) {
    errors.descBgColor = cssErrText;
  }
  if (values.borderColor && !values.borderColor.match(cssColorReg)) {
    errors.borderColor = cssErrText;
  }
  if (values.closeBtnColor && !values.closeBtnColor.match(cssColorReg)) {
    errors.closeBtnColor = cssErrText;
  }

  return errors;
};

@withRouter
@reduxForm(
  {
    form: 'onboarding_edit',
    fields: onboardingStepFields,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
  },
  (state, ownProps) => {
    const onboardingInfo = state.get('onboardingInfo');
    let initialValues;
    if (!CrudEditor.isCreatingNew(ownProps.params)) {
      initialValues = onboardingInfo && onboardingInfo.get('steps').find(s => s.get('id') === CrudEditor.sourceId(ownProps.params)).toJS();
    } else {
      initialValues = {
        id: -1,
        bodyWidth: 350,
        nextButtonText: 'Next',
        titleBgColor: '06351f',
        descBgColor: 'ffffff',
        borderColor: 'e7e7e7',
        nextBgColor: '06351f',
        closeBtnColor: 'ffffff',
      };
    }
    return {
      mainInfoLoaded: onboardingInfo,
      onboardingInfo,
      initialValues,
    };
  },
  dispatch => ({
    onboardingActions: bindActionCreators(OnboardingActions, dispatch),
  }),
)
export default class OnboardingEdit extends CrudEditor {

  constructor(props) {
    super(props);

    this.state = {
      uploadedLeftImage: null,
      uploadedRightImage: null,
    };
    this.handleImageUpload = this.handleImageUpload.bind(this);
  }

  handleImageUpload = (isLeft: boolean, evt) => {
    const uploadedImage = evt.target && evt.target.files && evt.target.files[0];
    if (uploadedImage) {
      readFileAsArrayBUffer(uploadedImage).then(result => {

        const imageURL = 'data:' + uploadedImage.type + ';base64,' + arrayBufferToBase64(result);

        this.setState(
          isLeft
            ? { uploadedLeftImage: uploadedImage, uploadedLeftImageURL: imageURL }
            : { uploadedRightImage: uploadedImage, uploadedRightImageURL: imageURL },
        );
      });
    }
  }

  handleSave = (id, values) => {
    const { onboardingInfo, params } = this.props;
    const {
      uploadedLeftImage, uploadedRightImage,
    } = this.state;
    return updateOnboardingInfo(convertFormValuesToOnboardingJson(onboardingInfo, values))
      .then(() => {

        if (CrudEditor.isCreatingNew(params)) {
          return Promise.resolve();
        }

        const leftImageFormData = new FormData();
        const rightImageFormData = new FormData();
        if (uploadedLeftImage) {
 leftImageFormData.append('photo', uploadedLeftImage);
}
        if (uploadedRightImage) {
 rightImageFormData.append('photo', uploadedRightImage);
}
        return Promise.all([
          uploadedLeftImage ? saveOnboardingImage(id, true, leftImageFormData) : Promise.resolve(),
          uploadedRightImage ? saveOnboardingImage(id, false, rightImageFormData) : Promise.resolve(),
        ]);
      })
      .then(() => history.push('/admin/onboardings'));
  }

  deleteItem = () => {
    const { onboardingInfo } = this.props;
    const stepIdToDelete = this.props.params.id;

    return updateOnboardingInfo(onboardingInfo.update('steps', steps => {
          const stepToDelete = steps.find(s => s.get('id') === Number(stepIdToDelete));
          return steps.delete(steps.indexOf(stepToDelete)).map(step => {
            if (step.get('page') === stepToDelete.get('page') && step.get('index') > stepToDelete.get('index')) {
              return step.set('index', step.get('index') - 1);
            }
            return step;
          });
        }),
      )
      .then(() => history.push('/admin/onboardings'));
  };

  componentWillMount() {
    const { onboardingInfo } = this.props;
    if (!onboardingInfo) {
      getOnboardingInfo();
    }
  }

  renderBody() {
    const { mainInfoLoaded, onboardingInfo } = this.props;
    const {
      uploadedLeftImage, uploadedRightImage,
      uploadedLeftImageURL, uploadedRightImageURL,
    } = this.state;
    const {
      page, index, placement,
      targetSelector,
      name,
      title, description,
      leftImage, rightImage,
      nextButtonText, backButtonText,
      bodyWidth,
      titleBgColor, descBgColor, borderColor, nextBgColor,
      closeBtnColor,
    } = this.props.fields;

    if (!mainInfoLoaded) {
      return <LoadingSpinner />;
    }

    return <div className="onboarding-step-editor-container">

      <Row>
        <Col xs={12} md={6}>
          Name: &nbsp;
          <TitleInput noun="" inputRef={inp => this.initialInput = inp} {...name} />
        </Col>
        <Col xs={12} md={6}>
          Step: {index.value}
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Title: &nbsp;
          <Validate {...title}>
            <FormControl type="text" bsSize="large" {...title} />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Description: &nbsp;
          <Validate {...description}>
            <FormControl componentClass="textarea" placeholder="Add description ..." {...description} />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Page: &nbsp;
          <Validate {...page}>
            <Select
              {...page}
              autoSize
              options={onboardingInfo.get('pages').sortBy(p => p.get('name')).map(p => ({
                value: p.get('key'),
                label: p.get('name'),
              })).toJS()}
              onChange={ev => {
                const newPageKey = valueFromEvent(ev);
                page.onChange(newPageKey);
                if (newPageKey !== page.initialValue) {
                  index.onChange(
                    onboardingInfo
                      .get('steps')
                      .filter(s => s.get('page') === newPageKey)
                      .size,
                  );
                }
              }}
            />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Placement: &nbsp;
          <Validate {...placement}>
            <Select
              {...placement}
              clearable
              autoSize
              options={[
                { value: 'center', label: 'Center' },
                { value: 'top', label: 'Top' },
                { value: 'bottom', label: 'Bottom' },
                { value: 'left', label: 'Left' },
                { value: 'right', label: 'Right' },
              ]}
            />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Left Image: &nbsp;
          <Validate {...leftImage}>
            <Select
              {...leftImage}
              clearable
              autoSize
              options={[
                { value: '/assets/images/hand_ok.png', label: 'Hand Ok' },
                { value: '/assets/images/hand_palm.png', label: 'Hand Palm' },
                { value: '/assets/images/hand_pointed.png', label: 'Hand Pointed' },
                { value: '/assets/images/hand_spoon.png', label: 'Hand Spoon' },
              ]}
              onChange={evt => {
                this.setState({ uploadedLeftImage: null, uploadedLeftImageURL: null });
                leftImage.onChange(valueFromEvent(evt));
              }}
            />
          </Validate>
          Or
          <FormControl
            type="file"
            onChange={_.partial(this.handleImageUpload, true)}
            onBlur={null}
            value={null}
          />
        </Col>
        <Col xs={12} md={6}>
          Right Image: &nbsp;
          <Validate {...rightImage}>
            <Select
              {...rightImage}
              clearable
              autoSize
              options={[
                { value: '/assets/images/chef_cloud_hat.png', label: 'Chef Cloud Hat' },
                { value: '/assets/images/chef_long_hat.png', label: 'Chef Long Hat' },
              ]}
              onChange={evt => {
                this.setState({ uploadedRightImage: null, uploadedRightImageURL: null });
                rightImage.onChange(valueFromEvent(evt));
              }}
            />
          </Validate>
          Or
          <FormControl
              type="file"
              onChange={_.partial(this.handleImageUpload, false)}
              onBlur={null}
              value={null}
            />
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Next Button Text: &nbsp;
          <Validate {...nextButtonText}>
            <FormControl type="text" bsSize="large" {...nextButtonText} />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Back Button Text: &nbsp;
          <Validate {...backButtonText}>
            <FormControl type="text" bsSize="large" {...backButtonText} disabled={placement.value === 'center'}/>
          </Validate>
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={6}>
          Next Button Background Color: &nbsp;
          <Validate {...nextBgColor}>
            <FormControl type="text" bsSize="large" {...nextBgColor} />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Body Width: &nbsp;
          <Validate {...bodyWidth}>
            <FormControl type="number" bsSize="large" {...bodyWidth} />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Target CSS Selector: &nbsp;
          <Validate {...targetSelector}>
            <FormControl type="text" bsSize="large" {...targetSelector} />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Border Color: &nbsp;
          <Validate {...borderColor}>
            <FormControl type="text" bsSize="large" {...borderColor} />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Title Background Color: &nbsp;
          <Validate {...titleBgColor}>
            <FormControl type="text" bsSize="large" {...titleBgColor} />
          </Validate>
        </Col>
        <Col xs={12} md={6}>
          Body Background Color: &nbsp;
          <Validate {...descBgColor}>
            <FormControl type="text" bsSize="large" {...descBgColor} />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col xs={12} md={6}>
          Close Button Color: &nbsp;
          <Validate {...closeBtnColor}>
            <FormControl type="text" bsSize="large" {...closeBtnColor} />
          </Validate>
        </Col>
      </Row>

      <Row>
        <Col
          xs={12}
          style={{
            minHeight: '200px',
            backgroundColor: '#d3d2d2',
            padding: '20px',
          }}
          >
          <div style={{ display: 'inline-block' }}>
            <OnboardingTooltip
              pageKey={page.value}
              stepIndex={index.value}
              title={title.value}
              description={description.value}
              titleBgColor={titleBgColor.value}
              descBgColor={descBgColor.value}
              borderColor={borderColor.value}
              leftImage={uploadedLeftImage ? '' : leftImage.value}
              rightImage={uploadedRightImage ? '' : rightImage.value}
              nextButtonText={nextButtonText.value}
              backButtonText={backButtonText.value}
              bodyWidth={bodyWidth.value}
              nextBgColor={nextBgColor.value}
              closeBtnColor={closeBtnColor.value}
              handleCallback={_.identity}

              leftImageURLFromEditor={uploadedLeftImageURL}
              rightImageURLFromEditor={uploadedRightImageURL}
            />
          </div>
        </Col>
      </Row>
    </div>;
  }

  listUrl = '/admin/onboardings';
  noun = 'onboarding';
}