// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';

import base64 from 'base64-js';

import { Button, Grid, Table } from 'react-bootstrap';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import { getAccountsList, getUsersList } from '../../webapi/endpoints';
import type { AccountListInfo, UserListInfo } from '../utils/PropTypes';

function dateTimeDisplay(iso8601String) {
  return moment(iso8601String).format('LL');
}

type State = {
  accounts: ?Array<AccountListInfo>,
  users: ?Array<UserListInfo>,
}

export default class UserList extends Component<{}, State> {
  constructor(props: any, context: any) {
    super(props, context);

    // using state because I think this is the only place that'll use the relevant API endpoint
    this.state = {
      accounts: null,
      users: null,
    };
  }

  render() {
    const { accounts, users } = this.state;
    if (!accounts || !users) {
      return <Grid fluid><LoadingSpinner/></Grid>;
    }

    const downloadLink = <Button
        download="parsley-users.csv"
        href={makeCSVDumpLink(users)}
    >
      Download CSV (sans admin or stdlib users)
    </Button>;

    return <Grid fluid>
      <p>Total Accounts: {accounts.length}</p>
      <p>Total Users: {users.length}</p>
      <p>{downloadLink}</p>

      <Table condensed hover>
        <thead>
        <tr>
          <th>Accounts</th>
          <th>Owner Name</th>
          <th>Owner Email</th>
          <th>Creation Time</th>
          <th>Last Login</th>
          <th>Promo Code</th>
          <th>Subscription Level</th>
          <th>flags</th>
          <th>Phone #</th>
          <th>Company</th>
        </tr>
        </thead>
        <tbody>
        { _.sortBy(accounts, a => -a.id).map(a => {

          const flags = ['isAdmin', 'isStdlib']
              .map(flag => a[flag] ? flag.substring(2) : null)
              .filter(_.identity)
              .join(',');
          const cellValues = [
              a.id,
              a.firstName + ' ' + a.lastName,
              a.email
                  ? <a href={`mailto:${a.email}`}>{a.email}</a>
                  : null,
              a.createdAt && dateTimeDisplay(a.createdAt),
              a.lastLogin && dateTimeDisplay(a.lastLogin),
              a.promoCode,
              a.subscriptionLevel,
              flags,
              a.phoneNumber,
              a.company,
          ];

          return <tr key={a.id} id={`account-${a.id}`}>
            {cellValues.map((val, i) => <td key={i}>{val}</td>)}
          </tr>;
        })}
        </tbody>
      </Table>
      <Table condensed hover>
        <thead>
        <tr>
          <th>Users</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone #</th>
          <th>Account access</th>
        </tr>
        </thead>
        <tbody>
        {_.sortBy(users, u => -u.id).map(u => {
          const accountAccess = u.userAccounts.map(ua => {
            const account = accounts.find(a => a.id === ua.accountId);
            const accountLinkText = (account && account.businessName) || ua.accountId;
            return <li key={ua.accountId}>
              {ua.permissionLevel} on <a href={`#account-${ua.accountId}`}>{accountLinkText}</a>
            </li>;
          });

          const cellValues = [
            u.id,
            u.firstName + ' ' + u.lastName,
            u.email
                ? <a href={`mailto:${u.email}`}>{u.email}</a>
                : null,
            u.phoneNumber,
            <ul>{accountAccess}</ul>,
          ];

          return <tr key={u.id} id={`user-${u.id}`}>
            {cellValues.map((val, i) => <td key={i}>{val}</td>)}
          </tr>;
        })}
        </tbody>

      </Table>
    </Grid>;
  }

  componentWillMount() {
    getAccountsList()
        .then(accounts => this.setState({ accounts }));
    getUsersList()
        .then(users => this.setState({ users }));
  }
}

function makeCSVDumpLink(users) {
  const dumpedUsers = users; // maybe later filter these down
  const header = 'id,firstname,lastname,email,phonenumber';

  const rows = dumpedUsers.map(u =>
    [u.id, u.firstName, u.lastName, u.email, u.phoneNumber],
  );

  const csv = [header, ...rows].join('\n');

  const encoded = base64.fromByteArray(
      (new TextEncoder()).encode(csv),
  );

  return `data:text/csv;base64,${encoded}`;
}
