// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import type { Element } from 'react';

import {
  ControlLabel,
  FormGroup,
  FormControl,
  Grid,
  Button,
  InputGroup
} from 'react-bootstrap';

import Section from '../Section';

import { rawAPIFetch } from 'common/webapi/utils';
import { getAccountsList, syncStdlibUsers } from '../../webapi/endpoints';
import Select from '../Select';
import type { AccountListInfo } from '../utils/PropTypes';

type State = {
  file?: File,
  saving: boolean,
  message?: Element<*>,
  syncing: boolean,
  users?: Array<AccountListInfo>,
  userToSync?: number,
}

export default class StdlibManager extends Component<{}, State> {

  constructor(props: {}) {
    super(props);
    this.state = {
      saving: false,
      syncing: false,
    };
  }

  render () {

    const {
      message, users,
      syncing, saving,
      userToSync,
      file,
    } = this.state;

    const userOpts = _.sortBy(users || [], 'id').map(u => ({
      value: u.id,
      label: `${u.id} - ${u.email}`,
    }));

    return <Grid fluid>
      {message}
      <Section title="USDA Import">
        <FormGroup>
          <ControlLabel>
            File From Data Entry
          </ControlLabel>
          <FormControl type="file" accept=".json"
                       onChange={ev => this.setState({ file: ev.target.files[0] })} />
        </FormGroup>
        <Button
            disabled={!file || saving}
            onClick={this.submitUSDADump.bind(this)}
            bsStyle={saving ? 'default' : 'primary'}>
          {saving ? 'uploading' : 'upload'}
        </Button>
      </Section>
      <Section title="Sync to users">
        <InputGroup>
          <Select options={userOpts}
              // we are guaranteed that val is a number because that's the only type we have in options
              value={userToSync} onChange={val => this.setState({ userToSync: (val: any) })}
          />
          <InputGroup.Addon>
            {/* $FlowFixMe: flow doesn't know that onClick is only available when !disabled */}
            <Button onClick={() => this.sync([userToSync])} disabled={!userToSync || syncing}>
              Sync Selected User
            </Button>
          </InputGroup.Addon>
        </InputGroup>
        <Button onClick={this.globalSync.bind(this)} disabled={!users || syncing}>
          Sync all users
        </Button>
      </Section>
    </Grid>;
  }

  sync(users: Array<number>) {
    this.setState({ syncing: true });
    return syncStdlibUsers(users, 2)
        .then(
            response => this.setState({
              message: <span>
                {`successful sync. ${response.totalUsers} non-stdlib users existed to sync`}
                (there may have been no work to do for some of them)
              </span>,
              syncing: false,
            }),
            errorResponse => this.setState({
              message: <div>
                <p>something went wrong!</p>
                <pre>{errorResponse.message}</pre>
              </div>,
              syncing: false,
            }),
        );
  }

  globalSync() {
    // $FlowFixMe: flow doesn't know that this is only available when users have fetched already
    return this.sync(this.state.users.map(u => u.id));
  }

  submitUSDADump() {
    this.setState({ saving: true });
    return rawAPIFetch('/api/nndbsr_import', 'POST', this.state.file)
        .then(
            response => this.setState({
              message: <p>successful import.<br/>
                {response.nutrientInserted || 0} nutrient definitions inserted.<br/>
                {response.nutrientUpdated || 0} nutrient definitions updated.<br/>
                {response.ingredientInserted || 0} ingredients inserted.<br/>
                {response.ingredientUpdated || 0} ingredients updated.
              </p>,
              file: undefined,
              saving: false,
            }),
            errorResponse => this.setState({
              message: <div>
                <p>something went wrong!</p>
                <pre>{errorResponse.message}</pre>
              </div>,
              saving: false,
            }),
        );
  }

  componentWillMount() {
    getAccountsList().then(
        users => this.setState({ users }),
        error => this.setState({
          message: <div>
            <p>error fetching user list; please reload</p>
            <pre>{error.message}</pre>
          </div>,
        }),
    );
  }
}
