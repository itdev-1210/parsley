import React from 'react';

import { reduxForm } from 'redux-form';
import { withRouter } from 'react-router';
import { history } from '../../navigation';

import Immutable from 'immutable';
import _ from 'lodash';
import isEmail from 'validator/lib/isEmail'

import { Row, Col, FormControl, FormGroup } from 'react-bootstrap';

import Validate from 'common/ui-components/Validate';
import Onboarding from '../Onboarding';

import CrudEditor from './helpers/CrudEditor';
import Checkbox from '../Checkbox';
import Section from '../Section';
import TitleInput from '../TitleInput';

import { checkFields, required } from '../../utils/validation-utils';

import {
  createLocation,
  updateLocation,
  removeLocation,
  lookupLocation,
  getLocationList,
  getTagsList,
  getSupplierList,
  getLocationsSyncState,
  getOnboardingInfo,
} from '../../webapi/endpoints';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

function validate(values) {
  return checkFields(values, {
    email: required(val => !isEmail(val) ? 'Not valid-email' : null),
    upstreamRelativeName: required(),
  });
}

const asyncValidate = (values, dispatch, props) => lookupLocation({ name: values.upstreamRelativeName.trim() })
  .then(locationId => {
    if (!locationId) return {};
    if (locationId === CrudEditor.sourceId(props.params)) return {};
    throw { upstreamRelativeName: 'Name is already used' };
  });

@withRouter
@reduxForm(
  {
    form: 'locationEdit',
    fields: [
      'email',
      'upstreamRelativeName',
      'recipes[].id',
      'recipes[].name',
      'recipes[].checked',
      'suppliers[].id',
      'suppliers[].name',
      'suppliers[].checked',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
    asyncValidate,
    asyncBlurFields: ['upstreamRelativeName'],
    onSubmitSuccess: () => {
      history.push('/manage-locations');
      return true;
    },
  },
  (state, ownProps) => {
    const locations = state.get('locations');
    const isCreatingNew = CrudEditor.isCreatingNew(ownProps.params);
    const downstreamId = CrudEditor.sourceId(ownProps.params);
    const locationDetail = !isCreatingNew && locations && locations.find(l => l.get('id') === downstreamId, null, Immutable.Map());
    let initialValues = !isCreatingNew && locationDetail ? locationDetail.toJS() : {};
    const isInvitationNotAccepted = !isCreatingNew && locationDetail && !locationDetail.get('downstreamActivated');

    let submitText, submittingText;
    if (isInvitationNotAccepted) {
      submitText = 'resend';
      submittingText = 'sending';
    } else if (isCreatingNew) {
      submitText = 'send';
      submittingText = 'sending';
    } else {
      submitText = 'sync';
      submittingText = 'syncing';
    }

    const locationId = ownProps.params.id;
    const syncState = state.getIn(['locationsSyncState', locationId]);
    if (syncState || isCreatingNew) {
      const allRecipes = (state.getIn(['tags', 'recipes']) || Immutable.List()).sort((a, b) => a.get('name').localeCompare(b.get('name')));
      const allSuppliers = state.get('suppliers') || Immutable.List();
      const syncedRecipes = (syncState || Immutable.Map()).get('recipes') || Immutable.List();
      const syncedSuppliers = (syncState || Immutable.Map()).get('suppliers') || Immutable.List();

      initialValues.recipes = allRecipes.map(cat => ({
        id: cat.get('id'),
        name: cat.get('name'),
        checked: syncedRecipes.contains(cat.get('id')),
      })).toJS();

      initialValues.suppliers = allSuppliers.filter(
          s => !s.get('tombstone'),
      ).map(s => ({
        id: s.get('id'),
        name: s.get('name'),
        checked: syncedSuppliers.contains(s.get('id')),
      })).toJS();
    }

    return {
      locations,
      isCreatingNew,
      initialValues,
      locationDetail,
      isInvitationNotAccepted,
      mainInfoLoaded: Boolean(locations) && (syncState || isCreatingNew),
      submitText,
      submittingText,
      deleteText: !isCreatingNew ? 'remove' : 'cancel',
      deletingText: !isCreatingNew ? 'removing' : 'cancel',
    };
  },
)
export default class LocationEdit extends CrudEditor {

  handleSave(id, values) {
    this.clearRouteLeaveHook();

    const { isCreatingNew } = this.props;
    values.recipes = values.recipes.filter(r => r.checked).map(r => r.id);
    values.suppliers = values.suppliers.filter(s => s.checked).map(s => s.id);

    return (isCreatingNew
      ? createLocation(values)
      : updateLocation(id, _.omit(values, ['email']))
    ).then(() => this.goToList());
  }

  deleteItem() {
    return removeLocation(CrudEditor.sourceId(this.props.params));
  }

  checkAll(fields) {
    fields.forEach(f => {
      f.checked.onChange(true);
    });
  }

  unCheckAll(fields) {
    fields.forEach(f => {
      f.checked.onChange(false);
    });
  }

  renderBody() {
    const { fields, isCreatingNew, isInvitationNotAccepted } = this.props;
    const {
      email, upstreamRelativeName,
      recipes, suppliers,
    } = fields;

    if (!recipes || !suppliers) {
      return <Col xs={12} md={10} mdOffset={2}>
        <LoadingSpinner/>
      </Col>;
    }

    return (
      <div className="form-horizontal">
        {
          isInvitationNotAccepted && <FormGroup>
            <Col xs={12} className="crud-global-warning" style={{ textAlign: 'right' }}>Invitation not yet accepted</Col>
          </FormGroup>
        }
        <FormGroup>
          <Col xs={1} md={1}>Name:</Col>
          <Col xs={11} md={4}>
            <TitleInput
              noun="Location"
              inputRef={input => this.initialInput = input}
              {...upstreamRelativeName}
            />
          </Col>
          <Col xs={1} md={1}>Email:</Col>
          <Col xs={11} md={4} className={!isCreatingNew ? 'readonly' : null}>
            <Validate {...email}>
              <FormControl type="text" autoComplete="off" {...email} disabled={!isCreatingNew} />
            </Validate>
          </Col>
        </FormGroup>
        <Row>
        <Col xs={6} md={5}>
          <Section title="Recipes">
          <h5>
            <Checkbox
              inline
              checked={_.every(recipes, r => r.checked.value)}
              indeterminate={_.some(recipes, r => r.checked.value) && _.some(recipes, r => !r.checked.value)}
              onChange={ev => ev.target.checked ? this.checkAll(recipes) : this.unCheckAll(recipes)}
            /> Select All
          </h5>
          { recipes.map(r =>
            <div key={r.name.value}>
              <Checkbox
                inline
                checked={r.checked.value}
                onChange={r.checked.onChange}
              /> {r.name.value}
            </div>)
          }
          </Section>
        </Col>
        <Col xs={6} md={5}>
          <Section title="Suppliers">
          <h5>
            <Checkbox
              inline
              checked={_.every(suppliers, s => s.checked.value)}
              indeterminate={_.some(suppliers, s => s.checked.value) && _.some(suppliers, s => !s.checked.value)}
              onChange={ev => ev.target.checked ? this.checkAll(suppliers) : this.unCheckAll(suppliers)}
            /> Select All
          </h5>
          { suppliers.map(s =>
            <div key={s.name.value}>
              <Checkbox
                inline
                checked={s.checked.value}
                onChange={s.checked.onChange}
              /> {s.name.value}
            </div>)
          }
          </Section>
        </Col>
      </Row>
        <Onboarding pageKey="location_editor" />
      </div>
    );
  }

  componentWillMount() {
    const { locations, params, router, route, isCreatingNew, isInvitationNotAccepted } = this.props;

    if (!locations) {
      getLocationList();
    }
    getTagsList('recipes');
    getSupplierList();
    getOnboardingInfo();
    if (!CrudEditor.isCreatingNew(params)) {
      getLocationsSyncState(params.id);
    }

    this.clearRouteLeaveHook = router.setRouteLeaveHook(route, () => {
      if (this.props.dirty && !isCreatingNew && !isInvitationNotAccepted) {
        return 'You have unsaved changes that will be lost if you leave without syncing. Do you still want to leave?';
      } else {
        return null;
      }
    });
  }

  listUrl = '/manage-locations';
  noun = 'location'
}
