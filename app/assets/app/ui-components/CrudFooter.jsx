import React, { Component } from "react";
import marked from 'common/utils/marked';
import ReactPropTypes from 'prop-types'

import { Button, Col, Row } from "react-bootstrap";
import { Link } from "react-router";
import { withRouter } from "react-router";

import { columnWidthSet } from "./utils/PropTypes";

import { DeleteConfirmationModal } from "./modals/ConfirmationModal";

@withRouter
export default class CrudFooter extends Component {
  static propTypes = {
    noun: ReactPropTypes.string.isRequired,
    isNewItem: ReactPropTypes.bool.isRequired, // determines text of delete button and warning dialog
    listUrl: ReactPropTypes.string.isRequired, // for the back-ish link
    deleteTitle: ReactPropTypes.string,
    submitText: ReactPropTypes.string.isRequired,
    submittingText: ReactPropTypes.string.isRequired,
    deleteText: ReactPropTypes.string.isRequired,
    deletingText: ReactPropTypes.string.isRequired,

    onDelete: ReactPropTypes.func.isRequired,
    onSave: ReactPropTypes.func.isRequired,
    extraButtons: ReactPropTypes.arrayOf(ReactPropTypes.element),

    submitting: ReactPropTypes.bool.isRequired,
    deleting: ReactPropTypes.bool,
    globalError: ReactPropTypes.string,
    showExtraButtons: ReactPropTypes.bool,
    deleteDisabled: ReactPropTypes.bool,
    shouldHideDelete: ReactPropTypes.bool,
    readonlyUser: ReactPropTypes.bool,
    colWidthSet: ReactPropTypes.objectOf(columnWidthSet),
  };

  static defaultProps = {
    colWidthSet: {xs: 5, sm: 4, md: 6}
  }

  constructor(props) {
    super(props);
    this.state = { showDeleteConfirmation: false }
  }

  render() {
    const {
        noun, isNewItem, listUrl, deleteTitle,
        submitText, submittingText,
        deleteText, deletingText,
        onDelete, onSave,
        extraButtons, showExtraButtons, colWidthSet,
        submitting, deleting, globalError, editingDisabled, deleteDisabled, shouldHideDelete,
        readonlyUser,
    } = this.props;
    const globalErrorDisplay = globalError
        ? <div dangerouslySetInnerHTML={{ __html: marked(globalError)}} />
        : null;

    function deleteOrCancel() {
      if (isNewItem) {
        this.props.router.push(listUrl);
      } else {
        this.setState({showDeleteConfirmation: true});
      }
    }
    function handleDelete() {
      return onDelete().catch(
          error => this.setState({
            deletionErrorMessage: error.reduxFormErrors._error
          })
      )
    }


    return (<div className="crud-footer">
      <Row className="indent-bottom">
        <Col className="hidden-xs" xs={2} mdOffset={2}>
        </Col>
        <Col className="crud-global-errors" xs={5} sm={6} md={2}>
          { globalErrorDisplay }
        </Col>
        <Col {...colWidthSet} className="crud-top-buttons">
          { (showExtraButtons) ? extraButtons : null }
          {!readonlyUser ?
            <span>
              <Button disabled={submitting || deleting || editingDisabled}
                onClick={onSave}
                bsStyle={submitting ? "default" : "primary"}
                className="green-btn">
                {submitting ? submittingText : submitText}
              </Button>
              {
                !shouldHideDelete ?
                  <Button
                    disabled={editingDisabled || deleteDisabled}
                    onClick={deleteOrCancel.bind(this)}
                    bsStyle="danger">
                    {isNewItem ? "cancel" :
                      deleting ? deletingText :
                        deleteText
                    }
                  </Button>
                  : null
              }
            </span>
          : null}
        </Col>
      </Row>
      { this.state.showDeleteConfirmation ?
          <DeleteConfirmationModal
              onHide={() => this.setState({
                showDeleteConfirmation: false, deletionErrorMessage: undefined
              })}
              onDelete={handleDelete.bind(this)}
              inProgress={deleting}
              deleteError={this.state.deletionErrorMessage}
              title={deleteTitle}
              titleNoun={noun}
          />
          : null
      }
    </div>);
  }
}
