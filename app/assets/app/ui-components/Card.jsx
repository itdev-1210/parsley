// @flow
import React, { Component } from 'react';
import type { Element } from 'react';
import { Row, Col } from 'react-bootstrap';

type Props = {
  title?: string,
  icon?: string,
  description?: string,
  content: Element<*>,
  footer?: Element<*>,
}

export class Card extends Component<Props> {

  static defaultProps = {
    title: '',
    icon: '',
    description: '',
    footer: undefined,
  }

  render() {
    const { title, icon, description, content, footer } = this.props;
    return (
      <div className="card">
        {
          title &&
          <div className="header">
            <Row>
              <Col xs={11}>
                <h4 className="title">{title}</h4>
                <p className="category">{description}</p>
              </Col>
              <Col xs={1}>
                <div className="icon-big text-center icon-warning">
                  <img className="icon-card" src={icon}/>
                </div>
              </Col>
            </Row>
            <hr />
          </div>
        }
        <div className="content">
          {content}
          {
            footer &&
            <div className="footer">
              <hr />
              {footer}
            </div>
          }
        </div>
      </div>
    );
  }
}

export default Card;