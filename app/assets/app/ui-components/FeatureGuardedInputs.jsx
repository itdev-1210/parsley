// @flow
import React, { Component } from "react";
import type { ChildrenArray } from 'react';

import { Button } from "react-bootstrap";

import { featureGuarded } from "./utils/FeatureGating";
import type { FeatureName } from 'common/utils/features';

type Props = {
  feature?: FeatureName,
  children: ChildrenArray<*>,
};
@featureGuarded
export class FeatureGuardedButton extends Component<Props, {}> {
  render() {
    const { feature, children, ...restProps } = this.props;

    return <Button {...restProps}>{children}</Button>;
  }
}