// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import Immutable from 'immutable';

import type { ProductSource } from './utils/PropTypes';
import Validate from 'common/ui-components/Validate';
import UnitInput from './UnitInput';
import {
  builtinPackages, effectivePackageSize, nestedPackages,
} from '../utils/packaging';
import { convertPerQuantityValue } from '../utils/unit-conversions';
import type {
  Measures,
  Conversions,
  MeasureSystem
} from '../utils/unit-conversions';

import { shallowToObject, valueFromEvent } from '../utils/form-utils';
import type { FormEvent, FormField } from '../utils/form-utils';
import QuantityInput from './QuantityInput';
import type { NewUnitDescriptor } from './modals/NewUnitModal';

type CommonProps = {
  inModal?: boolean,
  measures: Measures,
  conversions: Conversions,
  // need NonMaybeType because, despite being functionally identical, Flow doesn't
  // consider FormField<V> and FormField<?V> to be the same type
  fields: $ObjMap<ProductSource, <V>(V) => FormField<$NonMaybeType<V>>>,
  compact: boolean,
  disabled?: boolean,
}

type UnitManipulationProps = {
  productId?: number,
  canAddNewUnit: boolean,
  openNewUnitModal?: NewUnitDescriptor => void,
  untouch: string => void,
}

// lets user input unit *or* package type
export class RootUnitInput extends Component<CommonProps & UnitManipulationProps> {

  /**
   * Handles changes to the first unit *control*, not to the package unit control
   */
  handleUnitChange = (ev: FormEvent<number | string>) => {
    const newUnit = valueFromEvent(ev);
    const {
      conversions,
      untouch,
      fields: {
        measure,
        unit,
        cost,
        packaged,
        packageName,
        packageSize,
        subPackageName,
        superPackageSize,
        pricePer,
        pricingMeasure,
        pricingUnit,
      },
    } = this.props;
    const oldValues = shallowToObject(this.props.fields);

    if (typeof(newUnit) != 'number') { // new unit is packaged
      // change from superpackage type (flat, case) to single package type
      // $FlowFixMe - arrays of constants are weird
      if (
          (builtinPackages[oldValues.packageName] && builtinPackages[oldValues.packageName].canBeSuperPackage) &&
          (!builtinPackages[newUnit] || !builtinPackages[newUnit].canBeSuperPackage)
      ) {
        subPackageName.onChange('');
        superPackageSize.onChange(1);
        // packageSize goes in the place superPackageSize did in the UI, so swap
        packageSize.onChange(oldValues.superPackageSize);
      }

      packageName.onChange(newUnit); // might just be change in package name

      if (!oldValues.packaged) {
        // changing from non-packaged to packaged
        packaged.onChange(true);
        if (pricePer) pricePer.onChange('package');
        packageSize.onChange(undefined);
        untouch(packageSize.name); // field is "fresh" to the user

        if (pricingMeasure) pricingMeasure.onChange(undefined);
        if (pricingUnit) pricingUnit.onChange(undefined);

        // default package unit/measure is old unit/measure, package size should already be 1
        // no other changes involved
      }
    } else {
      const newPackageSize = 1;

      if (oldValues.packaged) {
        packaged.onChange(false);
        packageName.onChange(undefined);
        packageSize.onChange(newPackageSize);
        subPackageName.onChange('');
        superPackageSize.onChange(1);
        if (pricePer) pricePer.onChange('unit');
      }

      unit.onChange(newUnit);

      if (pricingMeasure) pricingMeasure.onChange(measure);
      if (pricingUnit) pricingUnit.onChange(newUnit);

      const {
        amount: oldPackageSize, unit: oldUnit,
      } = effectivePackageSize(Immutable.fromJS(oldValues));
      if (oldUnit && cost) {
        cost.onChange(!cost.value ? cost.value :
            convertPerQuantityValue(cost.value,
                oldPackageSize, oldUnit,
                newPackageSize, newUnit,
                conversions),
        );
      }
    }
  };

  render() {
    const {
      conversions,
      measures,
      inModal,
      productId,
      canAddNewUnit,
      openNewUnitModal,
      fields: {
        unit,
        measure,
        packaged,
        packageName,
      },
      compact,
      disabled,
    } = this.props;

    return <Validate inModal={inModal} compact={compact} {...(packaged.value ? packaged : unit)}
              style={{ 'paddingLeft': '14px'/* again accounting for static 5em width set on un*/ }}>
      <UnitInput
          disabled={disabled}
          multiMeasure
          measures={measures}
          conversions={conversions}
          wide={false}
          reduxFormFields={({
            measureField: {
              ...measure,
              onBlur() {},
              onChange(newMeasure) {
                if (typeof(newMeasure) != typeof(0)) {
                  return;
                } else {
                  measure.onChange(newMeasure);
                }
              },
            },
            unitField: {
              ...unit,
              onChange: this.handleUnitChange,
              value: packaged.value ? packageName.value || 'pkg' : unit.value,

              onBlur: () => packaged.value ? undefined : unit.onBlur(unit.value), // don't let it plop a package name into "unit"
            },
          })}
          productId={productId}
          canAddNewUnit={canAddNewUnit}
          openNewUnitModal={openNewUnitModal}
          extraOptions={_(builtinPackages).map(({ displayName, alternateName }, internalName) => ({
            value: internalName,
            label: `${displayName} of`,
            altName: alternateName,
          })).sortBy(opt => opt.label).value()}
      />
    </Validate>;
  }
}

export class PackageSizeInput extends Component<CommonProps & UnitManipulationProps> {
  handleUnitChange = (ev: FormEvent<number | string>) => {
    const newUnit = valueFromEvent(ev);
    const {
      untouch,
      fields: {
        unit,
        packageSize,
        subPackageName,
        superPackageSize,
        pricePer,
      },
    } = this.props;
    const oldValues = shallowToObject(this.props.fields);

    if (typeof(newUnit) !== 'number') {
      // new "unit" is a subpackage
      subPackageName.onChange(newUnit);

      if (!oldValues.subPackageName) {
        // changing from plain package to nested package
        superPackageSize.onChange(oldValues.packageSize);
        untouch(packageSize.name);
        untouch(unit.name);
        unit.onChange(undefined);
        packageSize.onChange(undefined);

        // old package is new super-package; user-visible text doesn't change
        if (oldValues.pricePer === 'package') {
          pricePer.onChange('super-package');
        }
      }
    } else {
      // new unit is a plain old unit
      unit.onChange(newUnit);

      if (oldValues.subPackageName) {
        // changing from nested package to plain package
        untouch(packageSize.name);
        subPackageName.onChange(undefined);
        superPackageSize.onChange(1);
        // packageSize goes in the place superPackageSize did in the UI, so swap
        packageSize.onChange(oldValues.superPackageSize);

        // old super-package is new package; user-visible text doesn't change
        if (oldValues.pricePer === 'super-package') {
          pricePer.onChange('package');
        }
      }
    }
  };

  render() {
    const {
      conversions,
      measures,
      inModal,
      productId,
      canAddNewUnit,
      openNewUnitModal,
      fields: {
        unit,
        measure,
        packaged,
        packageName,
        packageSize,
        subPackageName,
        superPackageSize,
      },
      compact,
      disabled,
    } = this.props;
    if (!packaged.value) return null;

    return <QuantityInput
        disabled={disabled}
        inModal={inModal}
        compact={compact}
        autoAdjust={false}
        measures={measures} conversions={conversions}
        reduxFormFields={({
          amountField: subPackageName.value ? superPackageSize : packageSize,
          // $FlowFixMe
          measureField: {
              ...measure,
            onBlur() {},
            onChange(newMeasure) {
              if (typeof(newMeasure) != typeof(0)) {
                return;
              } else {
                measure.onChange(newMeasure);
              }
            },
          },
          unitField: {
            ...unit,
            onBlur() {},
            onChange: this.handleUnitChange,
            value: subPackageName.value ? subPackageName.value || 'pkg' : unit.value,
          },
        })}
        productId={productId}
        canAddNewUnit={canAddNewUnit}
        openNewUnitModal={openNewUnitModal}
        multiMeasure
        extraOptions={
          packageName.value && builtinPackages[packageName.value].canBeSuperPackage
              ? _.sortBy(nestedPackages, 'label')
              : []
        }
    />;
  }
}

export class SubPackageSizeInput extends Component<CommonProps & UnitManipulationProps> {
  render() {
    const {
      conversions,
      measures,
      inModal,
      productId,
      canAddNewUnit,
      openNewUnitModal,
      fields: {
        unit,
        measure,
        subPackageName,
        packageSize,
      },
      compact,
      disabled,
    } = this.props;

    if (!subPackageName.value) return null;

    return <QuantityInput
        disabled={disabled}
        inModal={inModal}
        compact={compact}
        autoAdjust={false}
        measures={measures} conversions={conversions}
        reduxFormFields={({
          amountField: packageSize,
          measureField: measure,
          unitField: unit,
        })}
        productId={productId}
        canAddNewUnit={canAddNewUnit}
        openNewUnitModal={openNewUnitModal}
        multiMeasure
    />;

  }
}

type PricePerProps = {
  userWeightPref: MeasureSystem,
  userVolumePref: MeasureSystem,
}
export class PricePerInput extends Component<CommonProps & PricePerProps> {
  handleUnitChange = (ev: FormEvent<number | 'super-package' | 'package'>) => {
    const newUnit = valueFromEvent(ev);
    const {
      fields: {
        pricePer,
        pricingMeasure,
        pricingUnit,
      },
    } = this.props;

    if (typeof(newUnit) != 'number') { // newUnit is pricePer value.
      pricingUnit.onChange(undefined);
      pricingMeasure.onChange(undefined);
      pricePer.onChange(newUnit);
    } else {
      pricePer.onChange('unit');
      pricingUnit.onChange(newUnit);
    }
  };

  render() {
    const {
      conversions,
      measures,
      inModal,
      fields: {
        packaged,
        packageName,
        subPackageName,
        pricePer,
        pricingMeasure,
        pricingUnit,
        pricingUnitLabel,
      },
      userWeightPref,
      userVolumePref,
      compact,
      disabled,
    } = this.props;

    let measureCommonPurchase = measures.map(m => {
        let filter = Boolean(m.get('id') == 3 || m.get('id') == 2);
        let isMetric =
          m.get('id') == 3 ? userVolumePref === 'metric' : userWeightPref === 'metric';
        return m.update('units', units =>
          units.filter(u => !filter || u.get('metric') == isMetric));
     });

    let optionsCost = [];
    if (packaged.value) {
      if (subPackageName.value && packageName.value) {
        optionsCost.push({
          value: 'super-package',
          label: `per ${builtinPackages[packageName.value].displayName}`,
          altName: builtinPackages[packageName.value].alternateName,
          className: null,
        });
        optionsCost.push({
          value: 'package',
          // $FlowFixMe: for some reason it only likes whichever one is first
          label: `per ${builtinPackages[subPackageName.value].displayName}`,
          // $FlowFixMe: for some reason it only likes whichever one is first
          altName: builtinPackages[subPackageName.value].alternateName,
          className: null,
        });
      } else if (packageName.value) {
        optionsCost.push({
          value: 'package',
          label: `per ${builtinPackages[packageName.value].displayName}`,
          altName: builtinPackages[packageName.value].alternateName,
          className: null,
        });
      }
    }

    const defaultPricingUnit = pricingUnitLabel ? pricingUnitLabel.value : undefined;

    return <Validate inModal={inModal} compact={compact} {...pricePer}>
      <UnitInput
          disabled={disabled}
          multiMeasure
          measures={measureCommonPurchase}
          conversions={conversions}
          wide={false}
          reduxFormFields={({
            measureField: {
              ...pricingMeasure,
              onBlur() {},
              onChange(newMeasure) {
                if (typeof(newMeasure) != 'number') {
                  return;
                } else {
                  pricingMeasure.onChange(newMeasure);
                }
              },
            },
            unitField: {
              ...pricingUnit,
              // $FlowFixMe: We can be assured that only strings we get will be the ones we put in extraOptions
              onChange: this.handleUnitChange,
              value: typeof(pricingUnit.value) != 'number' ? pricePer.value : pricingUnit.value,
              onBlur() {},
            },
          })}
          extraOptions={optionsCost}
          prefixName="per "
          onlyCommonPurchaseUnit
          defaultSearchValue={defaultPricingUnit}
      />
    </Validate>;
  }
}
