import React, { Component } from 'react';

import { Checkbox as BaseCheckbox } from 'react-bootstrap';

export default class Checkbox extends Component {
  componentWillReceiveProps(nextProps) {
    if (this.input && (nextProps.indeterminate != this.props.indeterminate)) {
      this.input.indeterminate = nextProps.indeterminate;
    }
  }

  render() {
    const { children, indeterminate, ...passthroughProps } = this.props;
    return <BaseCheckbox {...passthroughProps}
        inputRef={input => {
          this.input = input;
          if (input) input.indeterminate = indeterminate;
        }}
    >
      <span /> {/* for our fancy CSS stuff to insert the visible checkbox */}
      {children}
    </BaseCheckbox>;
  }
}
