import React from 'react';

import classnames from 'classnames';

import {
  Tooltip, OverlayTrigger,
} from 'react-bootstrap';

export default ({children, helpText, className, inModal = false, ...passthrough}) => {
  const tooltip = <Tooltip className={classnames({ 'modal-tooltip': inModal })}>
    {helpText}
    </Tooltip>;
  return <OverlayTrigger placement="top" overlay={tooltip}>
    <span className={classnames(className, "text-with-help")} {...passthrough}>
      {children}
      <img className="questionCirle" src="/assets/images/Question-cirle.svg" alt="" />
    </span>
  </OverlayTrigger>;
}
