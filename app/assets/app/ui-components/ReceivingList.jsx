import React, { Component } from 'react';
import ReactPropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';

import Immutable from 'immutable';
import _ from 'lodash';
import CurrencyInput from './CurrencyInput';

import ImmutablePropTypes from 'react-immutable-proptypes';
import * as ParsleyPropTypes from './utils/PropTypes';
import {
  getUserInfo,
  getSupplierList, getMeasures,
  getIngredientList,
  getProductUsageInfo,
  getSupplierSourcedIngredients,
  createPurchase,
  receivePurchase,
} from '../webapi/endpoints';
import { actionCreators as PurchaseOrderListActions } from '../reducers/uiState/purchaseOrderList';

import {
  shallowToObject,
  valueFromEvent,
} from '../utils/form-utils';

import { Link, withRouter } from 'react-router';
import {
  Table, Button,
} from 'react-bootstrap';
import Validate from 'common/ui-components/Validate';
import { checkFields, deepArrayField, deepObjectField, isBlank, nonnegative, required } from '../utils/validation-utils';
import { defaultComparator } from 'common/utils/sorting';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Select from './Select';
import AddButton from './AddButton';
import CurrencyDisplay from './CurrencyDisplay';
import PackagedAmountInput from './PackagedAmountInput';
import { receivingUnitName } from '../utils/packaging';
import { FieldArray } from '../utils/redux-upgrade-shims';
import { assembleItemInfo, getUserEditedOrder } from './ShoppingList';
function validate(values) {

  const validators = {
    items: deepArrayField(deepObjectField({
      product: prod => isBlank(prod) ? 'Please select an item' : null,
      totalPurchased: deepObjectField({
        amount: required(nonnegative),
      }),
    })),
  };

  return checkFields(values, validators);
}


@withRouter
@reduxForm(
    {
      form: 'shopping-list',
      fields: [
        // not for user modification - just used for cross-referencing into the full data structure
        'items[].product',

        'items[].totalPurchased.amount',
        'items[].totalPurchased.measure',
        'items[].totalPurchased.unit',
        'items[].selectedSource',
        // other fields for future user editing can be found in PropTypes.jsx:shoppingListEntry
        'items[].unitCost',

        // hidden, non-modifiable fields - necessary to maintain proper UI state for newly-added items
        'items[].userAdded',
        'items[].supplierId', // this one also just makes things easier in the common case

      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => {
      const detailedIngredients = state.get('detailedIngredients');
      return {
        allSuppliers: state.get('suppliers'),
        initialValues: {
          items: [],
        },
        purchaseWeightSystem: state.getIn(['userInfo', 'purchaseWeightSystem']),
        purchaseVolumeSystem: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        detailedIngredients,
        units: state.getIn(['measures', 'units']),
        supplierSourcedIngredients: state.get('supplierSourcedIngredients'),
      };
    },
    (dispatch, ownProps) => ({
      purchaseOrderListActions: bindActionCreators(PurchaseOrderListActions, dispatch),
    }),
)
export default class ReceivingListPublic extends Component {
  render() {
    const { fields, ...rest } = this.props;
    return <FieldArray component={ReceivingList}
      fields={fields.items}
      props={{ ...rest }}
    />;
  }
}

class ReceivingList extends Component {
  static propTypes = {
    supplierSourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource),
    page: ReactPropTypes.string.isRequired,
    displayedSuppliers: ImmutablePropTypes.setOf(ReactPropTypes.number),
    allSuppliers: ParsleyPropTypes.summaryList,
    detailedIngredients: ImmutablePropTypes.map,
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
  };

  componentWillMount() {
    getUserInfo();
    getSupplierList();
    getMeasures();
    getIngredientList(); // Get ingredients' name

    let displayedSuppliers = this.props.displayedSuppliers;
    if (displayedSuppliers) {
      for (let supplierId of displayedSuppliers.filter(Number.isInteger)) {
        getSupplierSourcedIngredients(supplierId);
      }
    }
  }

  render() {
    const {
      allSuppliers, fields, supplierSourcedIngredients,
      displayedSuppliers,
      detailedIngredients,
      purchaseOrderListActions,
      purchaseWeightSystem, purchaseVolumeSystem,
    } = this.props;
     if (!displayedSuppliers) {
       return <LoadingSpinner />;
     }

    const getSupplierName = id => Number.isInteger(id)
        ? allSuppliers.find(entry => entry.get('id') == id).get('name')
        : 'Unknown Source';

    const indexedItems = Immutable.List(fields.map((field, index) => ({ index, ...field })));
    let sortedSupplierIds = displayedSuppliers.sortBy(supplierId => Number.isInteger(supplierId) ? getSupplierName(supplierId) : '');
    let sections = sortedSupplierIds.map(supplierId => {
      const items = indexedItems.filter(item => item.supplierId.value === supplierId);
      return <ShoppingListSection
        supplierId={supplierId}
        supplierName={getSupplierName(supplierId)}
        key={String(supplierId)}
        items={items}
        removeItem={index => fields.removeField(index)}
        addItem={() => fields.addField({ supplierId, userAdded: true })}
        sourcedIngredients={supplierSourcedIngredients.get(supplierId)}
        purchaseWeightSystem={purchaseWeightSystem}
        purchaseVolumeSystem={purchaseVolumeSystem}
        detailedIngredients={this.props.detailedIngredients}
        units={this.props.units}
        page={this.props.page}
      />;
    });

    const purchaseOrder = () => {
      Promise.all(sortedSupplierIds.map(supplierToOrder => {
        const items = indexedItems.filter(item => item.supplierId.value === supplierToOrder);
        let userEditedOrder = getUserEditedOrder(items, supplierSourcedIngredients.get(supplierToOrder), detailedIngredients);
        let orders = [];
        if (supplierToOrder !== '') {
          let transactionDeltas = [...items];
          let values = transactionDeltas.filter(item => item.product).map(field => {
            const sourcedIngredients = supplierSourcedIngredients.get(supplierToOrder);
            const thisIngredientSources = sourcedIngredients &&
              sourcedIngredients.filter(s => s.get('product') == field.product.value);
            const itemInfo = assembleItemInfo(field,
              null,
              detailedIngredients,
              thisIngredientSources,
            );
            return {
              cashFlow: itemInfo.totalCost,
              product: field.product.value,
            };
          });

          userEditedOrder = userEditedOrder.map(order => ({
            ...order,
            itemInfo: {
              ...order.itemInfo,
              cashFlow: values.filter(value => value.product == order.itemInfo.product)[0].cashFlow,
            },
          }));
          return createPurchase(supplierToOrder, orders, userEditedOrder, null, true);
        }
        return Promise.resolve();
      }))
      .then(() => {
        purchaseOrderListActions.hideReceiving();
      });
    }
    let comfirmReceiveButton = <Button
          onClick={() => purchaseOrder()}
          className="pull-right" >
          Confirm Receipt
        </Button>;

    return <div>
      {comfirmReceiveButton}
      <div className="shopping-list">
        <div className="shopping-list-inside-wrapper">
          {sections}
        </div>
      </div>
      {comfirmReceiveButton}
      <div className="clearfix" />
    </div>;
  }
}

@withRouter
@connect(
    (state, ownProps) => ({
      ingredientsNameMap: Immutable.Map((state.get('ingredients') || Immutable.List()).map(i => [i.get('id'), i.get('name')])),
    }),
)
class ShoppingListSection extends Component {
  static propTypes = {
    supplierId: ReactPropTypes.number,
    supplierName: ReactPropTypes.string,
    items: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired,
    removeItem: ReactPropTypes.func.isRequired,
    addItem: ReactPropTypes.func.isRequired,
    sourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,
    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
    page: ReactPropTypes.string.isRequired,
  };

  componentDidMount () {
    const { items, addItem, units } = this.props;
    if (units && items.size === 0) {
      addItem();
    }
  }

  componentDidUpdate(prevProps) {
    const { units, items, addItem } = this.props;
    if (!prevProps.unit && units && items.size === 0) {
      addItem();
    }
  }

  render() {
    const {
        supplierName, supplierId,
        items, detailedIngredients, sourcedIngredients,
        removeItem, addItem,
        units, purchaseWeightSystem, purchaseVolumeSystem,
        ingredientsNameMap,
    } = this.props;

    const supplierLink = Number.isInteger(supplierId)
        ? <Link to={`/suppliers/${supplierId}`} target="_blank">{supplierName}</Link>
        : supplierName;
    const orderedItems = Immutable.Set(items.map(i => i.product.value).filter(_.identity));
    const unorderedItems = sourcedIngredients ?
        sourcedIngredients
            .map(s => {
              let label = s.get('supplierIngredientName') || ingredientsNameMap.get(s.get('product'));
              const conversions = detailedIngredients.getIn([s.get('id'), 'measures']);
              let ingredientsPackage = PackagedAmountInput.fullPackageName(s, conversions);
              if (ingredientsPackage) {
                label += ` (${ingredientsPackage})`;
              }
              return {
                value: s.get('id'),
                label,
              };
            }).toSet() // accept no duplicates
            .filter(product => !orderedItems.includes(product.value)) :
        Immutable.Set();
    const colAttrs = ShoppingListSection.colAttrs; // can't CSS-select for cells in a given column, so hack

    let sectionCost = 0; // accumulator

    const safeToLowerCase = (input) => input && typeof input === 'string' ? input.toLowerCase() : input;

    return <Table className="shopping-list-section" id={'shopping-list-section-' + supplierId}>

      <colgroup>
        <col {...colAttrs.name}/>
        <col {...colAttrs.requirement}/>
        <col {...colAttrs.quantity}/>
        <col {...colAttrs.unitCost}/>
        <col {...colAttrs.totalCost}/>
        <col {...colAttrs.deleteLink}/>
      </colgroup>
      <thead>
      <tr>
        <th scope="colgroup" {...colAttrs.name}>
          <h4 style={{ marginBottom: '8px' }}>{supplierLink}&nbsp;&nbsp;</h4>
        </th>
        <th scope="column" {...colAttrs.quantity} style={{ paddingTop: '30px'}}>
          {supplierId !== 'unknown' ? 'Qty Received' : null}
        </th>
        <th scope="column" {...colAttrs.unitCost} style={{ paddingTop: '30px' }}>
          {supplierId !== 'unknown' ? 'Unit Cost' : null}
        </th>
        <th scope="column" {...colAttrs.totalCost} style={{ paddingTop: '30px' }}>
          {supplierId !== 'unknown' ? 'Total' : null}
        </th>
        <th scope="column" {...colAttrs.deleteLink} style={{ paddingTop: '30px' }} />
      </tr></thead>
      <tbody>
        { !units ? <LoadingSpinner /> :
            items.map(field => {
              const thisIngredientSources = sourcedIngredients &&
                  sourcedIngredients.filter(s => s.get('product') == field.product.value);
              const itemInfo = assembleItemInfo(field, null, detailedIngredients, thisIngredientSources);
              return { ...field, itemInfo };
            })
            .sort(
              (fieldA, fieldB) =>
                  defaultComparator(fieldA.userAdded.value, fieldB.userAdded.value) // user-added should go *last*
                  || fieldA.userAdded.value ? defaultComparator(fieldA.index, fieldB.index) // sort user-added by insertion order
                      // and order-added by name
                      : defaultComparator(safeToLowerCase(fieldA.itemInfo.name),
                          safeToLowerCase(fieldB.itemInfo.name)),
            )
            .map(field => {
              const thisIngredientSources = sourcedIngredients &&
                  sourcedIngredients.filter(s => s.get('product') == field.product.value);
              const {
                  name,
                  loading,
                  conversions,
                  purchasePackage,
                  indeterminateRequirement,
                  unitCost,
                  totalCost,
              } = field.itemInfo;

              let itemNameDisplay, quantityDisplay, unitCostDisplay, totalCostDisplay;
              if (loading) {
                // each a separate declaration because I think React would react (heh) badly to the same
                // instantiated component being used multiple times
                itemNameDisplay = <LoadingSpinner />;
                unitCostDisplay = <LoadingSpinner />;
                totalCostDisplay = <LoadingSpinner />;
                quantityDisplay = <LoadingSpinner />;
              } else {
                if (field.userAdded.value) {
                  const options = unorderedItems.filter(i => i.label).sortBy(i => i.label).toArray();
                  itemNameDisplay = <Validate {...field.product}><Select
                      placeholder="Select the ingredients to purchase"
                      options={options} {...field.selectedSource}
                      onChange={ev => {
                        const newSourceId = valueFromEvent(ev);
                        let newProductId;
                        const newIngredientSource = sourcedIngredients && sourcedIngredients.filter(s => s.get('id') === newSourceId);
                        if (newIngredientSource && !newIngredientSource.isEmpty()) {
                          const source = newIngredientSource.first();
                          newProductId = source.get('product');
                          getProductUsageInfo(newProductId);
                          let unitCost = source.get('cost');
                          if (source.get('pricePer') === 'package') unitCost *= source.get('superPackageSize');
                          else if (source.get('pricePer') === 'unit') unitCost *= source.get('superPackageSize') * source.get('packageSize');
                          field.unitCost.onChange(unitCost);
                          field.selectedSource.onChange(source.get('id'));
                        } else {
                          field.selectedSource.onChange(null);
                          field.unitCost.onChange(null);
                        }
                        _.forOwn(field.totalPurchased, subfield => subfield.onChange(null));
                        field.product.onChange(newProductId);
                      }}
                  /></Validate>;
                } else {
                  itemNameDisplay = <Link
                      target="_blank"
                      to={`/ingredients/${field.product.value}`}>
                    {name}
                  </Link>;
                }

                let productSource = thisIngredientSources && thisIngredientSources.find(s => s.get('id') === field.selectedSource.value);
                if (unitCost != undefined) {
                  unitCostDisplay = <div style={{ minWidth: 150 }}>
                    <div className="receiving-direct-unit-cost">
                      <CurrencyInput
                        inModal compact placeholder="Cost"
                        {...field.unitCost}/>
                    </div>
                    {<span className="receiving-direct-unit-source" style={{ verticalAlign: 'super' }}>
                     &nbsp;{`per ${receivingUnitName(productSource, conversions)}`}
                    </span>}
                  </div>;
                }

                if (totalCost != undefined) {
                  totalCostDisplay = <CurrencyDisplay value={totalCost}/>;

                  sectionCost += totalCost;
                }

                if (field.product.value) {
                  quantityDisplay = <PackagedAmountInput
                    sources={
                      thisIngredientSources.filter(s => s.get('id') === field.selectedSource.value)
                    }
                    quantityFields={field.totalPurchased}
                    selectedSource={field.selectedSource}
                    units={units}
                    packageSize={purchasePackage}
                    conversions={conversions}
                    indeterminateRequirement={indeterminateRequirement}
                    purchaseWeightSystem={purchaseWeightSystem}
                    purchaseVolumeSystem={purchaseVolumeSystem}
                  />;
                } else {
                  quantityDisplay = <div/>;
                }
              }

              return <tr key={field.product.value} className={field.userAdded.value ? 'user-added-shopping-list-item' : null}>
                <th scope="row" {...colAttrs.name}>{itemNameDisplay}</th>
                <td {...colAttrs.quantity} {...supplierId !== 'unknown' ? {} : { colSpan: '2' }}>{quantityDisplay}</td>
                <td {...colAttrs.unitCost}>{unitCostDisplay}</td>
                <td {...colAttrs.totalCost}>{totalCostDisplay}</td>
                <td {...colAttrs.deleteLink}>
                  <span className="delete-link" onClick={() => removeItem(field.index)}>remove</span>
                </td>
              </tr>;
            })
        }
      </tbody>
      { Number.isInteger(supplierId)
          ? <tfoot>
              <tr>
                <td {...colAttrs.name}><AddButton onClick={addItem} /></td>
                <td colSpan="3"/>
                <td {...colAttrs.totalCost}>
                  <h4>
                    Grand Total: <CurrencyDisplay value={sectionCost}/>
                  </h4>
                </td>
              </tr>
            </tfoot>
          : null
      }
      </Table>;
  }

  static colAttrs = {
    name: {
      className: 'receiving-list-name',
    },
    quantity: {
      className: 'receiving-list-quantity',
    },
    requirement: {
      className: 'receiving-list-requirement',
    },
    unitCost: {
      className: 'receiving-list-unit-cost',
    },
    totalCost: {
      className: 'receiving-list-total-cost',
    },
    deleteLink: {
      className: 'receiving-list-delete-link',
    },
    sku: {
      className: 'receiving-list-sku',
    },
  };
}