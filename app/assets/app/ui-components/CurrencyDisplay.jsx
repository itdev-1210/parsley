// @flow
import React from 'react';
import formatNum from 'format-num';

type Props = {
  maxFraction?: number,
  minFraction?: number,
  value: ?number, // non-optional, but possibly null/0
  matchVertical?: boolean,
}

const CurrencyDisplay = (props: Props) => {
  let {
    maxFraction = 2,
    minFraction = 2,
    value,
    matchVertical,
  } = props;

  if (!value && value !== 0) return null;

  let displayValue = formatNum(props.value, { maxFraction, minFraction });

  // Add additional hidden characters to make vertical CurrencyDisplay(s) match
  // Eg: $2.23
  //     $4
  //     $1.3
  let additionalHiddenChars = '';
  if (matchVertical) {
    const afterDecimal = displayValue.split('.')[1];
    let afterDecimalLength;
    if (!afterDecimal) {
      additionalHiddenChars = '.';
      afterDecimalLength = 0;
    } else {
      afterDecimalLength = afterDecimal.length;
    }

    for (let i = 0; i < maxFraction - afterDecimalLength; i++) {
      additionalHiddenChars += '0';
    }
  }

  return <span>
    ${displayValue}
    <span style={{ color: 'rgba(0,0,0,0)' }}>{additionalHiddenChars}</span>
  </span>;
};

export default CurrencyDisplay;
