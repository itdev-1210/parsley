import React, { Component } from "react";
import marked from 'common/utils/marked';
import ReactPropTypes from 'prop-types'

import { Button, Col, Row } from "react-bootstrap";
import { Link } from "react-router";
import { withRouter } from "react-router";

import { columnWidthSet } from "./utils/PropTypes";

import CrudEditor from './pages/helpers/CrudEditor';
import { DeleteConfirmationModal } from "./modals/ConfirmationModal";
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

const defaultColWidthSet = { xs: 12, md: 10, mdOffset: 2 };
export const crudEditorLoadingSpinner = <Row>
  <Col {...defaultColWidthSet}><LoadingSpinner /></Col>
</Row>;


@withRouter
export default class CrudHeader extends Component {
  static propTypes = {
    noun: ReactPropTypes.string.isRequired,
    listTitle: ReactPropTypes.string,
    deleteTitle: ReactPropTypes.string,
    isNewItem: ReactPropTypes.bool.isRequired, // determines text of delete button and warning dialog
    listUrl: ReactPropTypes.string.isRequired, // for the back-ish link
    submitText: ReactPropTypes.string.isRequired,
    submittingText: ReactPropTypes.string.isRequired,
    deleteText: ReactPropTypes.string.isRequired,
    deletingText: ReactPropTypes.string.isRequired,

    onDelete: ReactPropTypes.func.isRequired,
    onSave: ReactPropTypes.func.isRequired,
    extraButtons: ReactPropTypes.arrayOf(ReactPropTypes.element),

    submitting: ReactPropTypes.bool.isRequired,
    deleting: ReactPropTypes.bool,
    globalError: ReactPropTypes.string,
    deleteDisabled: ReactPropTypes.bool,
    shouldHideDelete: ReactPropTypes.bool,
    readonlyUser: ReactPropTypes.bool,
    colWidthSet: ReactPropTypes.objectOf(columnWidthSet),
  };

  static defaultProps = {
    colWidthSet: defaultColWidthSet,
  }

  constructor(props) {
    super(props);
    this.state = { showDeleteConfirmation: false }
  }

  render() {
    const {
        noun, listTitle, deleteTitle, isNewItem, listUrl,
        submitText, submittingText,
        deleteText, deletingText,
        onDelete, onSave,
        extraButtons, colWidthSet,
        submitting, deleting, globalError, editingDisabled, deleteDisabled, shouldHideDelete, readonlyUser } = this.props;
    const globalErrorDisplay = globalError
        ? <div dangerouslySetInnerHTML={{ __html: marked(globalError)}} />
        : null;

    function deleteOrCancel() {
      if (isNewItem) {
        this.props.router.push(listUrl);
      } else {
        this.setState({showDeleteConfirmation: true});
      }
    }
    function handleDelete() {
      return onDelete().catch(
          error => this.setState({
            deletionErrorMessage: error.reduxFormErrors._error
          })
      )
    }

    return (<div>
      <Row className="indent-bottom">
        <Col {...colWidthSet}>
          <Row>
            <Col className="hidden-xs crud-back-nav" xs={2}>
              <Link className="crud-back-link" to={{pathname: listUrl, state: {prevObjId: CrudEditor.sourceId(this.props.params)}}}>
                &lt; {listTitle || (noun + "s")}
              </Link>
            </Col>
            <Col className="crud-global-errors" xs={5} sm={6} md={2}>
              { globalErrorDisplay }
            </Col>
            <Col xs={5} sm={4} md={8} className="crud-top-buttons">
              { extraButtons }
              {!readonlyUser ?
                <span>
                  <Button disabled={submitting || deleting || editingDisabled}
                    onClick={onSave}
                    bsStyle={submitting ? "default" : "primary"}
                    className="green-btn">
                    {submitting ? submittingText : submitText}
                  </Button>
                  {
                    !shouldHideDelete ?
                      <Button
                        disabled={editingDisabled || deleteDisabled}
                        onClick={deleteOrCancel.bind(this)}
                        bsStyle="danger">
                        {isNewItem ? "cancel" :
                          deleting ? deletingText :
                            deleteText
                        }
                      </Button>
                      : null
                  }
                </span>
              : null}
            </Col>
          </Row>
        </Col>
      </Row>
      { this.state.showDeleteConfirmation ?
          <DeleteConfirmationModal
              onHide={() => this.setState({
                showDeleteConfirmation: false, deletionErrorMessage: undefined
              })}
              onDelete={handleDelete.bind(this)}
              inProgress={deleting}
              deleteError={this.state.deletionErrorMessage}
              title={deleteTitle}
              titleNoun={noun}
          />
          : null
      }
    </div>);
  }
}
