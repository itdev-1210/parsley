// @flow
import React, { Component } from "react";
import classnames from "classnames";
import { Button, Glyphicon } from "react-bootstrap";

type Props = {
  onClick: () => any,
  noun: string,
  className?: string, // optional property
}

export default class PrintButton extends Component<Props> {

  static defaultProps = {
    noun: "Print"
  };

  render() {
    const { className, noun, ...rest } = this.props;
    return (
        <Button
            bsStyle="default"
            className={classnames(className, "green-btn", "indent", "print-button")}
            {...rest}
        >
          <Glyphicon glyph="print" />
          &nbsp; {noun}
        </Button>);
  }
}
