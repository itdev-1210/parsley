import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { setOnBoardingWidgetVisited, getOnboardingWidgetInfo } from '../webapi/endpoints';

const CardPlaceHolder = (props: {text: string, widgetId: number }) => {
  const { text, widgetId } = props;
  const onClickHandler = () => {
    setOnBoardingWidgetVisited(widgetId)
      .then(() =>
        getOnboardingWidgetInfo(),
      );
  };
  return <div>
    <Row>
      <Col xs={12}>
        <h4 align="center"><label>{text}</label></h4>
      </Col>
    </Row>
    <br/>
    <Row>
      <Col xs={12} md={4} mdOffset={4}>
        <Button
          bsStyle="default"
          className="green-btn"
          onClick={onClickHandler}>
          Dismiss
        </Button>
      </Col>
    </Row>
  </div>;
};

export default CardPlaceHolder;