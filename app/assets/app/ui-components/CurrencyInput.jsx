import React from "react";

import { InputGroup } from "react-bootstrap";
import { MaskedNumberInput } from './MaskedInput';

import Validate from "common/ui-components/Validate";

var CurrencyInput = props => {

  return (<Validate {...props}>
    <InputGroup.Addon>$</InputGroup.Addon>
    <MaskedNumberInput normalizeInitial {...props} />
  </Validate>);
};

export default CurrencyInput;
