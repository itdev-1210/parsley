// @flow
import React, { Component } from 'react';
import classnames from 'classnames';
import { FormControl, Button, Glyphicon } from 'react-bootstrap';

type Props = {
  noun: string,
  onClick: Event => void,
  className?: string, // optional property
}

export default class UploadButton extends Component<Props> {

  static defaultProps = {
    noun: 'UPLOAD PRICE LIST',
    openFile: false
  };

  render() {
    const { className, onClick, noun } = this.props;
    return <Button onClick={onClick} bsStyle="default" className={classnames(className, "green-btn", 'upload-button')}>
      {noun}
    </Button>
  }
}
