// @flow
import React, { Component } from 'react';
import Immutable from 'immutable';

import { InputGroup } from 'react-bootstrap';

import { MaskedNumberInput } from './MaskedInput';

import { conversionRound, convert, getMeasure } from '../utils/unit-conversions';
import type { Conversions, Measures } from '../utils/unit-conversions';

import Validate from 'common/ui-components/Validate';

import UnitInput from './UnitInput';
import { valueFromEvent } from '../utils/form-utils';
import type { FormField } from '../utils/form-utils';
import classnames from 'classnames';
import type { SelectOption } from './Select';
import type { NewUnitDescriptor } from './modals/NewUnitModal';

/*
 * requires a successful getMeasures() call to have succeeded before it can render
 */

type Props = {
  inModal?: boolean, // If this input is in a modal or not, use to set validate tooltip's z-index
  multiMeasure?: boolean, // passed through to UnitInput
  useUnits?: boolean, // if false, use a plain text label saying "portions" instead of UnitInput
  autoAdjust?: boolean, // whether amount should change when unit is changed to keep the total the same
  disabled?: boolean,
  compact?: boolean, // passed through to Validate
  clearable?: boolean, // passed through to UnitInput
  canAddNewUnit?: boolean,
  disableRounding?: boolean,
  ignoreConversions?: boolean, // display measures regardless of conversions existing
  hideEmptyInputs?: boolean,

  productId?: number,
  className?: string, // optional property
  unitInputAddonStyle?: string,

  reduxFormFields: {
    amountField: FormField<number>,
    measureField: FormField<number | string>,
    unitField: FormField<number | string>,
    nutrientServingAmountField?: FormField<number>,
    nutrientServingMeasureField?: FormField<number>,
    nutrientServingUnitField?: FormField<number>,
  },

  extraOptions?: Array<SelectOption & {
    altName?: string | Array<string>,
  }>,

  amountInputRef?: (any => any),
  unitInputRef?: (any => any),
  openNewUnitModal?: NewUnitDescriptor => void,

  measures: Measures, // see UnitInput.props.multiMeasure for semantics
  conversions: Conversions,
  clearWholeInput?: boolean,
}

type State = {
  nonEmptyOnAmountFocus: ?boolean,
}

export default class QuantityInput extends Component<Props, State> {

  static defaultProps = {
    inModal: false,
    multiMeasure: false,
    useUnits: true,
    autoAdjust: false,
    disabled: false,
    compact: false,
    clearable: false,
    canAddNewUnit: false,
    disableRounding: false,
    ignoreConversions: false,
    hideEmptyInputs: false,
    clearWholeInput: true,
  };

  render() {
    // if (this.props.multiMeasure)
    //  assert(this.props.conversions) // and this.props.measures should contain ALL measures
    // else
    //    assert(this.props.measures.size == 1 && !this.props.conversions);
    let {
      reduxFormFields,
      useUnits, autoAdjust,
      compact, measures: allMeasures,
      hideEmptyInputs, disabled, amountInputRef, unitInputRef,
      className,
      canAddNewUnit,
      disableRounding,
      openNewUnitModal,
      productId,
      inModal,
      extraOptions,
      ignoreConversions,
      unitInputAddonStyle,
      clearWholeInput,
      ...unitInputPassthrough
    } = this.props;
    const conversions = unitInputPassthrough.conversions; // want to use it both here and in UnitInput

    const {
      amountField, measureField, unitField, nutrientServingAmountField, nutrientServingMeasureField, nutrientServingUnitField,
    } = reduxFormFields;

    const currentAmount = amountField && amountField.value;
    const currentMeasure = measureField && measureField.value;
    const currentUnit = unitField && unitField.value;

    function handleUnitChange(ev) {

      // TODO something weird maybe happens when switching to an extraOption here?
      const newUnit: number = (valueFromEvent<number | string>(ev): any);

      if (!newUnit) {
        unitField.onChange(null);
        measureField.onChange(null);
        return;
      }

      let newAmount = currentAmount;

      // adjust amount only if unit is changed from a previous value
      if (autoAdjust && currentAmount && currentMeasure && currentUnit && newUnit) {
        // $FlowFixMe, TODO something weird maybe happens when switching to an extraOption here?
        newAmount = conversionRound(convert(currentAmount, currentUnit, newUnit,
          (conversions || Immutable.List())));
      } else if (currentUnit && !newUnit) {
        // user has manually cleared the unit; treat this as a clearing of the whole quantity
        newAmount = undefined;
      }

      if (newAmount !== currentAmount) amountField.onChange(newAmount);
      // UnitInput is responsible for separately sending modification event for measureField
      if (unitField) unitField.onChange(newUnit);

      // In RecipeEdit, when a recipe's measure changes, automatically fixes its nutrient serving size
      if (nutrientServingAmountField && nutrientServingMeasureField && nutrientServingUnitField) {
        const newMeasure = getMeasure(newUnit);
        if (newMeasure !== nutrientServingMeasureField.value) {
          let newServingAmount, newServingUnit;
          if (newMeasure === 1) { // count
            newServingAmount = 1;
            newServingUnit = 1; // ea
          } else if (newMeasure === 2) { // weight
            newServingAmount = 100;
            newServingUnit = 2; // g
          } else if (newMeasure === 3) { // volume
            newServingAmount = 1;
            newServingUnit = 11; // cup
          }

          if (newServingAmount && newServingUnit && getMeasure(newServingUnit)) {
            // The new measure is count/weight/volume -> set to default values
            nutrientServingAmountField.onChange(newServingAmount);
            nutrientServingMeasureField.onChange(newMeasure);
            nutrientServingUnitField.onChange(newServingUnit);
          } else {
            // The new measure is a non-standard (might be added by an admin, etc)
            // -> set to 1 amount of the new unit
            nutrientServingAmountField.onChange(1);
            nutrientServingMeasureField.onChange(newMeasure);
            nutrientServingUnitField.onChange(newUnit);
          }
        }
      }
    }

    let unitInputProps, amountInputProps;

    unitInputProps = {
      reduxFormFields: {
        measureField: {
          ...reduxFormFields.measureField,
          onChange: newMeasure => {
            if (measureField) measureField.onChange(newMeasure);
          },
        },
        unitField: {
          ...reduxFormFields.unitField,
          // only
          onChange: handleUnitChange,
        },
      },
    };
    amountInputProps = {
      inputRef: amountInputRef,
      ...amountField,
    };

    const erroredFields = [amountField, unitField, measureField].filter(
      f => f && f.touched && f.error
    );

    return (
      <Validate inModal={inModal} className={classnames(className, 'quantityInput')} compact={compact}
        {...(erroredFields.length > 0 ? erroredFields[0] : amountInputProps)}>
        { hideEmptyInputs && !amountInputProps.value ? null :
          <MaskedNumberInput
            placeholder="Qty"
            disabled={disabled}
            className="form-control"
            normalizeInitial
            {...amountInputProps}
            precision={allMeasures.getIn([
              // $FlowFixMe: fields are hard to type
              measureField.value, 'units',
              unitField.value, 'precision'])}
            quantum={disableRounding ? 0 : allMeasures.getIn([
              // $FlowFixMe: fields are hard to type
              measureField.value, 'units',
              unitField.value, 'quantum'])}
            onFocus={ev => {
              this.setState({ nonEmptyOnAmountFocus: Boolean(amountInputProps.value) });
              return amountInputProps.onFocus(ev);
            }}
            onBlur={ev => {
              if (clearWholeInput && this.state.nonEmptyOnAmountFocus && !valueFromEvent(ev) && reduxFormFields.unitField.value) {
                // changed to empty, clear whole input
                reduxFormFields.measureField.onChange(undefined);
                reduxFormFields.unitField.onChange(undefined);
              }
              this.setState({ nonEmptyOnAmountFocus: undefined });
              return amountInputProps.onBlur(ev);
            }}
          />
        }
        { useUnits ?
          hideEmptyInputs && !reduxFormFields.unitField.value ? null :
            <InputGroup.Addon style={unitInputAddonStyle}>
              <UnitInput
                canAddNewUnit={canAddNewUnit}
                openNewUnitModal={openNewUnitModal}
                productId={productId}
                {...unitInputProps}
                {...unitInputPassthrough}
                measures={allMeasures}
                inputRef={unitInputRef}
                disabled={disabled} wide={false}
                extraOptions={extraOptions}
                ignoreConversions={ignoreConversions}
              />
            </InputGroup.Addon>
        :
        <InputGroup.Addon style={{ paddingRight: '0px' }}>
          {/* separate span so that padding value will add to
            input-group-addon padding */}
          <span>portions</span>
        </InputGroup.Addon>
        }
      </Validate>
    );
  }
}
