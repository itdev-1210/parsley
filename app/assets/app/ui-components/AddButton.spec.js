import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
chai.use(chaiEnzyme());

import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';

import AddButton from './AddButton';

describe('<AddButton/>', function() {
  it('should be a button', function() {
    const wrapper = mount(<AddButton/>);
    expect(wrapper).to.have.exactly(1).descendants('button');
  });

  it('should show text indicating to add a new item', function() {
    const wrapper = mount(<AddButton noun="Item" />);
    expect(wrapper).to.contain.text('Add Item');
  });
});
