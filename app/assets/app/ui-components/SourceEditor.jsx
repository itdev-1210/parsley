import _ from 'lodash';
import React, { Component } from 'react';
import ReactPropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { actionCreators as UIStateActions } from '../reducers/uiState/ingredientEdit';
import NewSupplierModal from './modals/NewSupplierModal';

import { connect } from 'react-redux';
import { FormControl, Row, Col } from 'react-bootstrap';

import Immutable from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import * as PropTypes from './utils/PropTypes';

import { getProductUsageInfo } from '../webapi/endpoints';

import { Link } from 'react-router';
import Select from './Select';

import RadioList from './RadioList';

import Validate from 'common/ui-components/Validate';
import QuantityInput from './QuantityInput';
import CurrencyInput from './CurrencyInput';
import CurrencyDisplay from './CurrencyDisplay';
import UnitInput from './UnitInput';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';

import {
  convertPerQuantityValue, unitDisplayName,
} from '../utils/unit-conversions';
import { shallowToObject } from '../utils/form-utils';
import {
  pickComparisonUnit,
  newSourceTemplate, costingSize, getLowestCostSourceId,
} from '../utils/packaging';
import {
  PackageSizeInput, RootUnitInput,
  SubPackageSizeInput, PricePerInput,
} from './SourceFormComponents';
import { setLowestSourcePreferred } from './modals/IngredientSourcesModal';
import { indexTypes } from '../reducers/indexesCache';

const headerNames = [
  'Default',
  "Supplier/ Supplier's Ingredient Name",
  'Unit of Purchase/ SKU',
  '',
  'Cost',
  '',
];

const headerColumnWidths = [
  { xs: 1 },
  { xs: 4 },
  { xs: 3 },
  { xs: 2 },
  { xs: 2 },
];

const columnWidths = [
  { xs: 1 },
  { xs: 3 },
  { xs: 1 },
  { xs: 2 },
  { xs: 2 },
  { xs: 4 },
  { xs: 2 },
  { xs: 2 },
];

const inHouseHeaderColumnWidths = [
  { xs: 1 },
  { xs: 2 },
  { xs: 5 },
  { xs: 2 },
];

const inHouseHeaderNames = [
  'Default',
  "Supplier",
  'Unit of Purchase',
  'Cost',
  'SKU',
];

const inHouseColumnWidths = [
  { xs: 1 },
  { xs: 2 },
  { xs: 5 },
  { xs: 2 },
];

export class SourcesList extends Component {

  static propTypes = {
    setPreferred: ReactPropTypes.func, // intentionally not required - for inHouse case
    addSource: ReactPropTypes.func,
    uniqueName: ReactPropTypes.string.isRequired,
    listHeaderNames: ReactPropTypes.array.isRequired,
    listHeaderColumnWidths: ReactPropTypes.array.isRequired,
    listColumnWidths: ReactPropTypes.array.isRequired,
    isReadOnly: ReactPropTypes.bool,
  };

  render() {
    const {
        setPreferred, addSource, children, uniqueName,
        listHeaderNames, listHeaderColumnWidths, listColumnWidths,
        isReadOnly,
    } = this.props;

    return (
        <RadioList
            disabled={isReadOnly}
            headerNames={listHeaderNames}
            headerColumnWidths={listHeaderColumnWidths}
            columnWidths={listColumnWidths}
            radioInputName={uniqueName}
            onSelected={setPreferred}

            onAdd={addSource}>
          { children }
        </RadioList>);
  }
}

@connect(
  state => ({
    uiState: state.get('ingredientEdit'),
  }),
  dispatch => ({
    uiStateActions: bindActionCreators(UIStateActions, dispatch),
  }),
)
export default class SourceEditor extends Component {
  static propTypes = {
    uniqueName: ReactPropTypes.string.isRequired,
    fields: ReactPropTypes.arrayOf(PropTypes.productSourceFormField).isRequired,
    suppliers: PropTypes.summaryList.isRequired,
    measures: ImmutablePropTypes.mapOf(PropTypes.measure).isRequired,
    conversions: ImmutablePropTypes.listOf(PropTypes.conversion).isRequired,
    startWithRow: ReactPropTypes.bool,
    isHidden: ReactPropTypes.bool,
    untouch: ReactPropTypes.func.isRequired,
    isReadOnly: ReactPropTypes.bool,
    userVolumePref: ReactPropTypes.string,
    userWeightPref: ReactPropTypes.string,
    isSelectable: ReactPropTypes.bool,
    isLowestPricePreferred: ReactPropTypes.bool,
    isSyncedIngredient: ReactPropTypes.bool,
  };

  render() {

    const {
      measures,
      conversions,
      fields,
      suppliers,
      uniqueName,
      untouch,
      uiState,
      uiStateActions,
      inModal,
      isHidden,
      userWeightPref,
      userVolumePref,
      isReadOnly,
      isSelectable,
      isLowestPricePreferred,
      isSyncedIngredient,
      disableUpdatingCost,
    } = this.props;

    const comparisonUnit = pickComparisonUnit(
        Immutable.fromJS(fields.map(shallowToObject)),
        conversions, measures, userWeightPref, userVolumePref, true);

    const allRowProps = { // mostly passthrough
      measures, conversions, suppliers, comparisonUnit, untouch,
      grabFocus: this.state.alreadyAddedSource, isSelectable, isLowestPricePreferred, isSyncedIngredient,
    };

    return (
      <div>
        <SourcesList setPreferred={this.setPreferred.bind(this)} uniqueName={uniqueName} isReadOnly={isReadOnly}
          listHeaderNames={headerNames} listHeaderColumnWidths={headerColumnWidths} listColumnWidths={columnWidths}
          addSource={this.addSource.bind(this)}>
          { fields.map(source =>
              <SourceRow key={source.id.value} fields={source} showingNewSupplierModal={uiState.get('showNewSupplierModal')}
                         deleteSource={this.deleteSource.bind(this, source.id.value)}
                         uiStateActions={uiStateActions} inModal={inModal} isHidden={isHidden}
                         userWeightPref={userWeightPref} userVolumePref={userVolumePref}
                         {...allRowProps}
                         isReadOnly={isReadOnly}
                         disableUpdatingCost={disableUpdatingCost}
              />) }
        </SourcesList>
        { uiState.get('showNewSupplierModal') ?
          <NewSupplierModal
             newSupplier={uiState.get('showNewSupplierModal')}
             key="new-supplier-modal"
             hide={uiStateActions.closeNewSupplierModal}
             disabled={isReadOnly}
          />
          : null }
        </div>
      );
  }

  setPreferred = (id) => {
    const { fields } = this.props;
    const oldPreferred = _.find(fields, source => source.preferred.value);
    const newPreferred = _.find(fields, source => source.id.value === id);
    if (oldPreferred) {
      oldPreferred.preferred.onChange(false);
    }
    if (newPreferred) {
      newPreferred.preferred.onChange(true);
    }
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      nextAddedId: 1,
      alreadyAddedSource: false,
    };
  }

  addSource = () => {
    const { fields } = this.props;
    const { nextAddedId } = this.state;

    fields.addField({
      ...newSourceTemplate,
      id: -nextAddedId,
      preferred: fields.length === 0,
      supplier: null,
    });
    this.setState({
      nextAddedId: nextAddedId + 1,
      alreadyAddedSource: true,
    });
  }

  deleteSource = (id) => {
    /* Remove a source from the OrderedMap */
    const { fields } = this.props;
    const i = fields.findIndex(source => source.id.value === id);
    fields.removeField(i);
  }

  componentWillMount() {
    const { fields, startWithRow } = this.props;
    if (fields.length === 0 && startWithRow) {
      this.addSource();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { fields: nextSources, conversions: nextConversions, isLowestPricePreferred } = nextProps;
    const { fields: prevSources, conversions: prevConversions } = this.props;
    const sourceIsUsable = (s =>
        _.get(s, 'cost.valid') &&
        _.get(s, 'packageSize.valid') &&
        _.get(s, 'unit.valid') &&
        _.get(s, 'superPackageSize.valid')
    );
    const prevLowestCostSourceId = getLowestCostSourceId(
      Immutable.fromJS(prevSources.filter(sourceIsUsable).map(shallowToObject)),
      prevConversions,
    );
    const nextLowestCostSourceId = getLowestCostSourceId(
      Immutable.fromJS(nextSources.filter(sourceIsUsable).map(shallowToObject)),
      nextConversions,
    );
    if (isLowestPricePreferred && prevLowestCostSourceId !== nextLowestCostSourceId && !_.isNil(nextLowestCostSourceId)) {
      setLowestSourcePreferred(nextSources, nextConversions);
    }
  }
}

class SourceRow extends Component {
  static propTypes = {
    fields: PropTypes.productSourceFormField,
    comparisonUnit: ImmutablePropTypes.map,
    suppliers: ImmutablePropTypes.listOf(ImmutablePropTypes.map),
    deleteSource: ReactPropTypes.func.isRequired,
    untouch: ReactPropTypes.func.isRequired,
    grabFocus: ReactPropTypes.bool.isRequired,
    inModal: ReactPropTypes.bool,
    isHidden: ReactPropTypes.bool,
    isReadOnly: ReactPropTypes.bool,
    isSelectable: ReactPropTypes.bool,
    isLowestPricePreferred: ReactPropTypes.bool,
    isSyncedIngredient: ReactPropTypes.bool,
  };

  render() {
    const {
      measures: allMeasures, suppliers: allSuppliers,
      conversions, comparisonUnit, uiStateActions,
      deleteSource, inModal, isHidden, showingNewSupplierModal,
      fields: {
          id,
          measure,
          preferred,
          supplier,
          unit,
          cost,
          packaged,
          packageSize,
          subPackageName,
          superPackageSize,
          sku,
          pricePer,
          supplierIngredientName,
          manufacturerName,
          manufacturerNumber,
          syncUpstreamOwner,
      },
      userWeightPref,
      userVolumePref,
      isReadOnly,
      isSelectable,
      isLowestPricePreferred,
      isSyncedIngredient,
      disableUpdatingCost,
    } = this.props;

    const disableSource = (isSyncedIngredient && syncUpstreamOwner.value !== '') || isReadOnly;
    let compareTo;
    // resolve comparisonId when subpackage used
    if (!comparisonUnit && subPackageName) {
      compareTo = unit.value;
    } else {
      compareTo = comparisonUnit.get('id');
    }

    let costPerComparisonUnit;
    if (cost.value && packageSize.value && superPackageSize.value && typeof unit.value == 'number' &&
        pricePer.value && compareTo) {
      const pricingQuantity = costingSize(Immutable.fromJS(shallowToObject(this.props.fields)));

      costPerComparisonUnit = convertPerQuantityValue(cost.value,
          pricingQuantity.amount, pricingQuantity.unit,
          1, compareTo,
          conversions,
      );
    } else {
      costPerComparisonUnit = false;
    }

    const supplierOptions = allSuppliers.filter(
        s => !s.get('tombstone'),
    ).map(s =>
        ({ value: s.get('id'), label: s.get('name') }),
    ).toJS();

    const showPackagingUnit = typeof unit.value === 'number' &&
      (packaged.value || (measure.value === 1 && conversions.size > 1));

    // we can't just set the visibility of an ancestor component to hidden
    // because validate tooltips are rendered separately
    // we have to stop rendering these <Validate>s to make sure no tooltips are still rendered
    if (isHidden) {
      return <RadioList.Row/>;
    }

    return <div className="radio-list-container-two-item row">
      <RadioList.Row
        isSelected={Boolean(preferred.value)}
        onSelectedArgument={id.value}
        onDelete={deleteSource}
        selectable={!isReadOnly && isSelectable}
        selectTooltip={isLowestPricePreferred && 'To change the supplier for this ingredient, first de-select "Automatically Select the Lowest Cost Option" above'}
      >
        <Validate inModal={inModal} compact {...supplier} touched={showingNewSupplierModal ? false : supplier.touched}>
          <Select
            className="select-supplier"
            disabled={disableSource}
            creatable promptTextCreator={label => 'Add New Supplier'}
            createNew={item => {
              item.setSupplierId = supplier.onBlur;
              uiStateActions.openNewSupplierModal(item);
            }}
            options={supplierOptions}
            name={'supplier-'.concat(id.value)}
            inputRef={select => this.initialInput = select}
            {...supplier}
            searchIndexName={indexTypes.SUPPLIERS}
          />
        </Validate>
        <RootUnitInput
          disabled={disableSource}
          conversions={conversions} measures={allMeasures}
          inModal={inModal} fields={this.props.fields}
          untouch={this.props.untouch}
        />
        <div className="package-size">
          <PackageSizeInput
            disabled={disableSource}
            conversions={conversions} measures={allMeasures}
            inModal={inModal} fields={this.props.fields}
            untouch={this.props.untouch}
          />
        </div>
        <div className="sub-package-size">
          <SubPackageSizeInput
            disabled={disableSource}
            conversions={conversions} measures={allMeasures}
            inModal={inModal} fields={this.props.fields}
          />
        </div>
        <div className="radio-list-item-cost input-group">
          <span className="input-group-addon">
            <CurrencyInput inModal={inModal} compact placeholder="Cost" {...cost} disabled={disableUpdatingCost} />
          </span>
          <div className="price-per-input">
            {
              showPackagingUnit ? // packaging unit
              <PricePerInput
                disabled={disableSource}
                conversions={conversions}
                measures={allMeasures}
                fields={this.props.fields}
                userWeightPref={userWeightPref}
                userVolumePref={userVolumePref}
              />
              : null
            }
          </div>
        </div>
        <div className="radio-list-item-cost-comparison">
          { Boolean(costPerComparisonUnit) || costPerComparisonUnit === 0 ?
              <div style={{ 'text-align': 'center' }}>
                <CurrencyDisplay value={costPerComparisonUnit} />
                /
                {compareTo ?
                  unitDisplayName(compareTo, conversions)
                  : null}
              </div>
              : null
          }
        </div>
      </RadioList.Row>
      <hr className="well-child-hr" />
      <Row className="radio-list-item second-row">
        <Col xs={1} />
        <Col xs={4}>
          <div>
            <FormControl type="text" placeholder="Supplier's Ingredient Name (optional)" {...supplierIngredientName} disabled={disableSource} />
          </div>
        </Col>
        <Col xs={3}>
          <div className="checkbox-inline radio-list-item-sku">
            <FormControl type="text" placeholder="SKU (Optional)" {...sku} disabled={disableSource} />
          </div>
        </Col>
        <Col xs={3}>
          <div className="checkbox-inline radio-list-item-name">
            <FormControl type="text" placeholder="Manufacturer Name (Optional)" {...manufacturerName} disabled={disableSource} />
          </div>
        </Col>
        <Col xs={3}>
          <div className="checkbox-inline radio-list-item-sku">
            <FormControl type="text" placeholder="Mfg SKU (Optional)" {...manufacturerNumber} disabled={disableSource} />
          </div>
        </Col>
      </Row>
    </div>;
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }

}

@connect(
    (state, ownProps) => ({
      usageInfo: state.getIn(['detailedIngredients', ownProps.productId]),
    }),
)
export class InHouseDummySource extends Component {
  static propTypes = {
    uniqueName: ReactPropTypes.string.isRequired,
    productId: ReactPropTypes.number.isRequired,
    conversions: ImmutablePropTypes.listOf(PropTypes.conversion).isRequired,
    measures: ImmutablePropTypes.mapOf(PropTypes.measure).isRequired,
    recipeFields: ReactPropTypes.any.isRequired, // for figuring out batch size
    editRecipeOnClick: ReactPropTypes.oneOfType(ReactPropTypes.func, ReactPropTypes.string),
    isReadOnly: ReactPropTypes.bool,
  };

  static defaultProps = {
    editRecipeOnClick: () => {},
    isReadOnly: false,
  }

  componentWillMount() {
    // make the server deal with calculating cost per unit, since we're not live-updating
    getProductUsageInfo(this.props.productId);
  }

  render() {
    const {
        productId, recipeFields, conversions, measures, usageInfo, uniqueName,
        editRecipeOnClick,
        isReadOnly,
    } = this.props;

    if (!usageInfo) {
      return <LoadingSpinner />;
    }

    let batchSize;
    {
      let batchSizeFields;
      if (recipeFields.maxBatch.amount.value) {
        batchSizeFields = recipeFields.maxBatch;
      } else {
        batchSizeFields = recipeFields.recipeSize;
      }
      batchSize = Immutable.fromJS(shallowToObject(batchSizeFields));
    }

    const fakeSource = Immutable.fromJS({
      preferred: true,
      unit: usageInfo.get('primaryUnit'),
      measure: usageInfo.get('primaryMeasure'),
      packageSize: 1,
      superPackageSize: 1,
      cost: usageInfo.get('unitCost'),
    });

    // "comparison" of only one item, but whatever
    const comparisonUnit = pickComparisonUnit(
        Immutable.List([fakeSource]),
        conversions,
        measures,
    );
    const comparisonUnitCost = convertPerQuantityValue(usageInfo.get('unitCost'),
        1, usageInfo.get('primaryUnit'),
        1, comparisonUnit.get('id'), // likely the same unit, but might not be
        conversions,
    );

    let batchSizeUnitName = unitDisplayName(batchSize.get('unit'), conversions);
    const batchSizeDisplay = `batch of ${batchSize.get('amount')} ${batchSizeUnitName}`;

    return <SourcesList
      isReadOnly={isReadOnly}
      setPreferred={_.noop}
      uniqueName={uniqueName}
      listHeaderNames={inHouseHeaderNames}
      listHeaderColumnWidths={inHouseHeaderColumnWidths}
      listColumnWidths={inHouseColumnWidths}>
        <RadioList.Row deleteBehavior="hidden"
            selectable={!isReadOnly}
            isSelected onSelectedArgument={null} striped={false}>
          <div style={{ textAlign: 'center' }}>In-house</div>
          <div style={{ textAlign: 'center' }}>
            {batchSizeDisplay}
            &nbsp;&nbsp;
            {editRecipeOnClick === 'disabled' || isReadOnly ? ''
              : <Link className="non-obvious-link" to={`/recipes/${productId}`} onClick={editRecipeOnClick}>Edit Recipe</Link>
            }
          </div>

          {
            /*
            *  to have the unit-cost column line up with regular sources, add another column here (to match
            *  the CurrencyInput column in the regular editor)
            */
          }
          <div style={{ textAlign: 'center' }}>
            <CurrencyDisplay value={comparisonUnitCost} disabled={isReadOnly} />
            /
            {unitDisplayName(comparisonUnit.get('id'), conversions)}
          </div>
        </RadioList.Row>
    </SourcesList>;
  }
}
