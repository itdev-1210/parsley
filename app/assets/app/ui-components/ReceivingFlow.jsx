import React, { Component } from 'react';
import Immutable from 'immutable';

import { Button, Modal } from 'react-bootstrap';

import { actionCreators as UIStateActions } from '../reducers/uiState/purchasingFlow';
import { actionCreators as PurchaseOrderListActions } from '../reducers/uiState/purchaseOrderList';

import { getPurchaseList, getUserInfo, getSupplierList } from '../webapi/endpoints';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SupplierSelect } from './PurchasingFlow';
import ReceivingList from './ReceivingList';

type PurchasingFlowProps = {
  uiState: Immutable.Map<string, any>,
  hide: () => void,
  uiStateActions: typeof UIStateActions,
  display: boolean,
  suppliers: Immutable.List<any>,
  suppliersIdList: Immutable.Set<number>,
};

@connect(
  (state, ownProps) => {
    const suppliers = state.getIn(['suppliers']);
    const suppliersIdList = suppliers.map(s => s.get('id')).toSet();
    return {
      uiState: state.getIn(['purchasingFlow']),
      suppliers,
      suppliersIdList,
    };
  },
  dispatch => ({
    uiStateActions: bindActionCreators(UIStateActions, dispatch),
  }),
)

export default class ReceivingFlow extends Component<PurchasingFlowProps> {

  constructor(props) {
    super(props);
    this.checkAllSuppliers = this.checkAllSuppliers.bind(this);
    this.unCheckAllSuppliers = this.unCheckAllSuppliers.bind(this);
    this.checkSupplier = this.checkSupplier.bind(this);
    this.unCheckSupplier = this.unCheckSupplier.bind(this);
  }

  checkAllSuppliers() {
    const { suppliersIdList, uiStateActions } = this.props;
    suppliersIdList.forEach(id => uiStateActions.addSupplier(id));
  }

  unCheckAllSuppliers() {
    const { suppliersIdList, uiStateActions } = this.props;
    suppliersIdList.forEach(id => uiStateActions.removeSupplier(id));
  }

  checkSupplier(id: number) {
    const { uiStateActions } = this.props;
    uiStateActions.addSupplier(id);
  }

  unCheckSupplier(id: number) {
    const { uiStateActions } = this.props;
    uiStateActions.removeSupplier(id);
  }

  render() {
    const {
      hide: baseHide, uiState,
      suppliers, suppliersIdList,
      uiStateActions,
      display,
    } = this.props;

    function hide() {
      uiStateActions.leave();
      baseHide();
    }
    let body, footerButtons;
    const selectedSuppliers: Immutable.Set<number> = uiState.get('selectedSuppliers');
    let displayStyle = !display ? 'none' : null;
    const closeModalLink = <Button
    style={{ display: displayStyle, float: 'right', color: 'gray', marginRight: '8px' }}
      onClick={hide}
      bsStyle="link">
      close
    </Button>;
    footerButtons = [closeModalLink];

    switch (uiState.get('phase') || 'supplierSelect') {
      case 'supplierSelect':
        let topContinueButtonLabel = 'Continue';
        let topContinueButtonAction = uiStateActions.startReview;
        const topContinueButton = <Button
                                    disabled={selectedSuppliers.isEmpty()}
                                    onClick={topContinueButtonAction}
                                    style={{ float: 'right' }}
                                  >
                                    {topContinueButtonLabel}
                                  </Button>;
        const bottomContinueButton = React.cloneElement(topContinueButton);

        body = <div>
          {topContinueButton}
          <SupplierSelect
              suppliers={suppliers}
              usedSuppliers={suppliersIdList}
              selectedSuppliers={selectedSuppliers}
              checkSupplier={this.checkSupplier}
              unCheckSupplier={this.unCheckSupplier}
              checkAllSuppliers={this.checkAllSuppliers}
              unCheckAllSuppliers={this.unCheckAllSuppliers}
          />
        </div>;
        footerButtons.unshift(bottomContinueButton);
        break;
      case 'review':
        body = <ReceiveReview selectedSuppliers={selectedSuppliers}
        />;
        footerButtons = [
          <Button key="button-done" onClick={hide}>done</Button>,
          <Button key="button-back" style={{ float: 'left' }}
                  onClick={uiStateActions.startPickSuppliers}
          >
            back
          </Button>,
        ];
        break;
      default: break;
    }

    let modalTitle = 'Receive Direct';

    return <Modal show onHide={hide} style={(!display) ? { display: 'none' } : null}
                  className={uiState.get('phase') === 'review' ? 'medium-modal wider-modal' : 'small-modal'}>
      <Modal.Header closeButton>
        <Modal.Title>
          {modalTitle}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ marginLeft: '11px', marginRight: '11px' }}>{body}</Modal.Body>
      <Modal.Footer style={{ marginRight: '5%' }}>{footerButtons}</Modal.Footer>
    </Modal>;
  }

  componentWillMount() {

    const { uiStateActions } = this.props;
    getUserInfo();
    getSupplierList();
    uiStateActions.startPickSuppliers();
  }

  componentWillUnmount() {
    const {
      uiStateActions,
    } = this.props;
    uiStateActions.leave();
    getPurchaseList();
  }
}

type OrderReviewProps = {
  selectedSuppliers: Immutable.Set<number>,
}

@connect(
  () => {},
  dispatch => ({
    puchaseOrderListActions: bindActionCreators(PurchaseOrderListActions, dispatch),
  }),
)

class ReceiveReview extends Component<OrderReviewProps> {

  render() {
    const {
      selectedSuppliers,
      puchaseOrderListActions,
    } = this.props;
    return <ReceivingList
      displayedSuppliers={selectedSuppliers}
      close={() => puchaseOrderListActions.hidePurchasing()}
    />;
  }
}
