// @flow
import React, { Component } from 'react';
import type { Node } from 'react';

import { Button } from 'react-bootstrap';

type Props = {
  startHidden: boolean,
  hidden: ?boolean,
  nonHiddenPart: ?string,
  children: ?Node,
}

type State = {
  hidden: boolean,
  atInitialState: boolean,
}

export default class HideableText extends Component<Props, State> {

  static defaultProps = {
    startHidden: false,
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      hidden: typeof props.hidden === 'boolean' ? props.hidden : props.startHidden,
      atInitialState: true,
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (typeof nextProps.hidden === 'boolean') {
      this.setState({
        hidden: nextProps.hidden,
      });
    }
  }

  render() {
    const { hidden, atInitialState } = this.state;
    const { children, nonHiddenPart } = this.props;

    let contentClass;
    if (hidden) {
      contentClass = 'hidden'; // haven't made a slideout animation work quite as well as slidein
    } else if (!atInitialState) {
      contentClass = 'slidein';
    }

    return <div>
      {nonHiddenPart}
      <Button bsStyle="link" bsSize="small"
              onClick={() => this.setState(s => ({ ...s, hidden: !s.hidden, atInitialState: false }))}
      >
        ({hidden ? 'Show' : 'Hide'})
      </Button>
      <div className={contentClass}>
        {children}
      </div>
    </div>;
  }
}
