// @flow
import React, { Component, type Ref } from 'react';
import _ from 'lodash';
import Immutable from 'immutable';
import type { SelectValue } from './Select';
import Select, { type SelectOption } from './Select';
import type { Conversions, Measure } from '../utils/unit-conversions';
import { unitAltNames, unitDisplayName } from '../utils/unit-conversions';
import { getSearchObject } from '../utils/search';

import type { FormField } from '../utils/form-utils';
import type { NewUnitDescriptor } from './modals/NewUnitModal';

const KEYS = ['name', 'altName'];

type Props = {
  // field names
  reduxFormFields: {
    unitField: FormField<SelectValue>,
    measureField: FormField<SelectValue>,
  },

  canAddNewUnit?: boolean,
  // following two props are required iff canAddNewUnit
  openNewUnitModal?: NewUnitDescriptor => void,
  productId?: number,

  // extra options are not associated with real units or measures. if they are chosen by the user, both
  // the form fields will get a change request where both the new "unit" and new "measure" equal the value
  // of the extra option
  extraOptions: Array<SelectOption & {
    altName?: string | Array<string>,
  }>,

  prefixName: string, // determines the prefixes for unit names
  defaultSearchValue?: ?string, // if set, value will be initialized to first result of searching with this

  // multiMeasure switches between two modes:
  //  multiMeasure == false - measures prop contains only a subset of all measures
  //    measures are available to select iff they are in the measures prop
  //  multiMeasure == true - measures prop contains ALL measures
  //    measures are available to select iff they are defined in the conversions prop
  multiMeasure: boolean,
  ignoreConversions?: boolean,
  wide: boolean, // determines if we use abbreviated unit names or not
  clearable: boolean,
  disabled: boolean,
  canAddNewUnit?: boolean,
  onlyCommonPurchaseUnit: boolean,

  measures: Immutable.Map<number, Measure>,
  conversions: Conversions,

  inputRef?: Ref<*>,
};

class UnitInput extends Component<Props> {
  flattenedOptions: Array<SelectOption>;
  reverseMap: { [unit: number]: number }; // map from unit back to its measure

  constructor(props: Props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.preProcessOptions(props);
  }

  static defaultProps = {
    disabled: false,
    wide: true,
    clearable: false,
    multiMeasure: false,
    extraOptions: [],
    canAddNewUnit: false,
    prefixName: '',
    onlyCommonPurchaseUnit: false,
  };

  handleChange = (newUnit: SelectValue) => {
    const {
      extraOptions,
      reduxFormFields: {
        measureField, unitField,
      },
    } = this.props;
    let newMeasure = extraOptions.some(opt => opt.value === newUnit)
          ? newUnit // placeholder value
          // $FlowFixMe: if not in extraOptions, we know it's a number
          : this.reverseMap[newUnit];

    if (measureField && unitField) {
      measureField.onChange(newMeasure);
      unitField.onChange(newUnit);
    }
  }

  componentDidMount() {
    const {
      flattenedOptions,
      props: {
        defaultSearchValue,
      },
    } = this;
    if (flattenedOptions && defaultSearchValue) {
      const search = getSearchObject(flattenedOptions.filter(opt => !opt.disabled), ['label'], {}, 'value');
      const results = search.searchWithQueryFn(defaultSearchValue);
      if (results.length > 0) {
        this.handleChange(results[0].value);
      }
    }
    this.preProcessOptions(this.props);
  }

  UNSAFE_componentWillUpdate(nextProps: Props) {
    this.preProcessOptions(nextProps);
  }

  preProcessOptions(props: Props) {
    const {
      conversions, measures,
      extraOptions, prefixName,
      onlyCommonPurchaseUnit, ignoreConversions, multiMeasure, wide,
    } = props;
    this.flattenedOptions = [...extraOptions]; // spread == copy
    this.reverseMap = {};

    let validMeasures;
    if (multiMeasure && !ignoreConversions) {
      validMeasures = Immutable.Map(
          conversions.filter(c => !c.get('tombstone')).map(c => {
            // $FlowFixMe: only with records can we assert member existence/value
            let measureId: number = c.get('measure');
            return [measureId, measures.get(measureId)];
          }),
      );
    } else {
      validMeasures = measures;
    }

    validMeasures
        .filter(_.identity) // no undefined
        .forEach((measure, measureId) => {
          if (multiMeasure) {
            this.flattenedOptions.push({
              value: 'measure' + measureId,
              label: measure.get('name'),
              disabled: true,
            });
          }

          measure.get('units').filter(u => !onlyCommonPurchaseUnit || u.get('commonPurchaseUnit'))
              .forEach((unit, unitId) => {
                this.flattenedOptions.push({
                  value: unitId,
                  label: `${prefixName}${unitDisplayName(unitId, conversions, !wide)}`,
                  altName: unitAltNames(unitId, conversions, !wide),
                });
                this.reverseMap[unitId] = measureId;
              });
        });
  }

  render() {

    const {
        reduxFormFields,
        disabled, clearable,
        inputRef,
        openNewUnitModal,
        productId,
        canAddNewUnit,
    } = this.props;

    const { measureField, unitField } = reduxFormFields;

    // Add a new unit option at top
    let createNew;
    if (canAddNewUnit && openNewUnitModal) {
      createNew = item => {
        const creationRequest = {
          ...item,
          productId,
          currentMeasure: measureField.value,
          setUnit: (newMeasure, newUnit) => {
            measureField.onChange(newMeasure);
            unitField.onChange(newUnit);
          },
        };
        openNewUnitModal(creationRequest);
      };
    }

    return <div className="unit-input">
      { /* $FlowFixMe: onChange/onFocus event type mismatches */ }
      <Select options={this.flattenedOptions} clearable={clearable}
              creatable={canAddNewUnit}
              promptTextCreator={() => 'New Unit'}

              disabled={disabled || this.flattenedOptions.length === 0}
              placeholder="Unit"
              inputRef={inputRef}
              extraKeys={KEYS}
              {...unitField}
              handlerOnFocus={unitField.onFocus}
              onChange={this.handleChange}
              createNew={createNew}
      />
    </div>;
  }
}

export default UnitInput;
