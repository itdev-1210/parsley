import React, { Component } from 'react';
import _ from 'lodash';
import Immutable from 'immutable';
import { reduxForm } from 'redux-form';
import { Table, Button, Modal, Row, Col, Alert } from 'react-bootstrap';
import * as PropTypes from './utils/PropTypes';
import type { ProductSource } from './utils/PropTypes';
import ReactPropTypes from 'prop-types';
import Section from './Section';

import ImmutablePropTypes from 'react-immutable-proptypes';
import { SourceRow } from './SuppliedIngredientsDisplay';
import { pickAllComparisonUnits, newSourceTemplate } from '../utils/packaging';
import {
  commonSourceFormFields,
  shallowToObject,
} from '../utils/form-utils';
import { getMultipleProductsUsageInfo, getIngredientList } from '../webapi/endpoints';
import Radio from 'common/ui-components/Radio';
import {
  checkFields, required,
  commonSourceValidators,
} from '../utils/validation-utils';

import {
  getSearchObject,
  ingredientListSearchOpts,
  stripLeadingDigitToken,
} from '../utils/search';
import { indexTypes } from '../reducers/indexesCache';
import type { Measures } from '../utils/unit-conversions';
import type { FormField } from '../utils/form-utils';

function supplierIngredientNameToSearchTerm(name) {
  return name.replace(/[^0-9a-z\s]/gi, ' ').replace(/ +/g, ' ').trim();
}

export type CsvImportData = {
  name: string,
  pricingUnit: string,
  purchasePackage: string,
  cost: number,
  sku: string,
}

function validate(values) {
  const sourceValidators = {
    ...commonSourceValidators,
    product: required(),
    cost: required(value => {
      if (isNaN(value)) {
        return 'Unreadable value';
      } else if (value < 0) {
        return 'Cannot be negative';
      }
      return null;
    }),
  };

  return checkFields(values, { ...sourceValidators });
}

type FormProps = $FlowTODO;

type FormState = {
  renderChild: boolean,
};

@reduxForm(
  {
    form: 'import-ingredients-edit',
    fields: [
      ...commonSourceFormFields,
      'product',
      'purchasePackage',
      'price',
      'pricingUnitLabel',
      'date',
      'supplierName',
    ],
    touchOnBlur: false,
    validate,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const { ingredient, sources, isNewIngredient, ingredientOptions, findIngredientInSource } = ownProps;
    const detailedIngredients = state.get('detailedIngredients');
    let currentSource = {};
    ingredient.name = ingredient.name.replace(/ +/g, ' ');
    const overrideValues = {
      cost: ingredient.sanitizedPrice,
      sku: ingredient.sku,
      supplierIngredientName: ingredient.name,
      purchasePackage: ingredient.purchasepackage,
      pricingUnitLabel: ingredient.pricingunit,
      price: ingredient.price,
      date: ingredient.date,
      supplierName: ingredient.supplierName,
    };

    if (isNewIngredient) {
      let sourceId = -1;
      if (sources.length > 0) {
        sourceId = Math.min(sourceId, ...sources.map(s => s.id.value - 1));
      }
      currentSource = {
        ...newSourceTemplate,
        id: sourceId,
        ...overrideValues,
      };
    } else {
      const ingredientAdded = findIngredientInSource(ingredient);
      if (ingredientAdded) {
        currentSource = {
          ...shallowToObject(ingredientAdded),
          id: ingredientAdded.id.value,
          ...overrideValues,
        };
      }
    }
    return {
      initialValues: currentSource,
      overwriteOnInitialValuesChange: true,
      detailedIngredients,
      ingredientOptions,
    };
  },
)
class ImportIngredientForm extends Component<FormProps, FormState> {
  productId: number;

  constructor(props) {
    super(props);
    this.state = { renderChild: false };
    this.productId = props.fields.product.value;
    this.sendIngredient = this.sendIngredient.bind(this);
    this.skipIngredient = this.skipIngredient.bind(this);
    this.handleExitButton = this.handleExitButton.bind(this);
    this.getCurrentFieldsFn = this.getCurrentFieldsFn.bind(this);
    getMultipleProductsUsageInfo([this.productId], 50);
  }

  sendIngredient() {
    const { nextButton, fields, remaining } = this.props;
    if (remaining === 1)
      this.importFinalized = true;
    this.setState({ renderChild: false });
    nextButton(fields);
  }

  skipIngredient() {
    const { skipActions, remaining } = this.props;
    if (remaining === 1)
      this.importFinalized = true;
    this.setState({ renderChild: false });
    skipActions();
  }

  handleExitButton() {
    const { addOrUpdateIngredient, exitActions, valid, fields } = this.props;
    this.importFinalized = true;
    if (valid) {
      addOrUpdateIngredient(fields);
    }
    exitActions();
  }

  getCurrentFieldsFn() {
    const { fields, valid } = this.props;
    if (!this.importFinalized && valid)
      return shallowToObject(fields);
    else
      return undefined;
  }

  render() {
    const {
      allMeasures,
      detailedIngredients, isNewIngredient, userWeightPref, userVolumePref, ingredientOptions,
      handleSubmit, added, skipped, skippedUpdate, remaining,
      skipActions, exitActions, fields, ...rest
    } = this.props;

    return (
      <div>
        <Table className="table-import" style={{ marginBottom: '0px' }}>
          <thead>
            <tr>
              <th style={{ textAlign: 'center', maxWidth: '380px', minWidth: '300px' }}>Ingredient/ Supplier's Ingredient Name</th>
              <th colSpan="2" style={{ textAlign: 'center', minWidth: '250px' }}>Unit of Purchase/ SKU</th>
              <th style={{ width: '100px' }}/>
              <th colSpan="2" style={{ textAlign: 'center', width: '250px' }}>Cost</th>
              <th style={{ width: '100px' }}/>
              <th style={{ textAlign: 'center', width: '125px' }}>SKU</th>
              <th style={{ width: '89px' }}/>
            </tr>
          </thead>
          <tr className="import-data">
            <td>
              <div>
                <span className="pull-center">
                  {fields.supplierIngredientName.value}
                </span>
              </div>
            </td>
            <td/>
            <td>
              <div>
                <span className="pull-center">
                  {fields.purchasePackage.value}
                </span>
              </div>
            </td>
            <td />
            <td style={{ paddingRight: '2px' }}>
              <div>
                <span className="pull-center">{fields.price.value}</span>
              </div>
            </td>
            <td>
              <div>
                <span className="pull-center">
                  {fields.pricingUnitLabel.value}
                </span>
              </div>
            </td>
            <td />
            <td style={{ paddingLeft: '15px' }}>
              <div>
                <span className="pull-center">{fields.sku.value} </span>
              </div>
            </td>
            <td/>
          </tr>
          {
            this.state.renderChild ?
            <ImportIngredientRow
              {...rest}
              allMeasures={allMeasures}
              ingredientOptions={ingredientOptions}
              detailedIngredients={detailedIngredients}
              source={fields}
              isNewIngredient={isNewIngredient}
              userWeightPref={userWeightPref}
              userVolumePref={userVolumePref}
            />
            : null
          }
        </Table>
        <Row style={{ marginTop: '10px', marginLeft: '30%' }}>
          <Col xs={12}>
            <Row>
              <Col xsOffset={1} xs={2} md={2}>
                <Button
                    style={{ float: 'left', marginRight: '10px' }}
                    onClick={this.skipIngredient}
                > Skip
                </Button>
                <Button
                    style={{ float: 'left' }}
                    onClick={handleSubmit(this.sendIngredient)}
                    onMouseDown={(e) => e.preventDefault()}
                > Next
                </Button>
              </Col>
              <Col xs={8}>
                <Row style={{ paddingLeft: '4%', textAlign: 'center' }}>
                  <Col xs={2} md={2}>
                    <span>Added: {added}</span>
                  </Col>
                  <Col xs={3} md={3}>
                    <span>Skipped: {skipped}</span>
                  </Col>
                  <Col xs={3} md={3}>
                    <span>Updated: {skippedUpdate}</span>
                  </Col>
                  <Col xs={4} md={4}>
                    <span>Remaining: {remaining}</span>
                  </Col>
                </Row>
              </Col>
              <Col xs={1} md={1}>
                <Button style={{ float: 'right' }} onClick={this.handleExitButton}>
                  Exit
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>);
  }

  componentWillReceiveProps(nextProps) {
    const { fields: nextSource } = nextProps;
    const nextProductId = nextSource.product.value;
    if (this.productId != nextProductId) {
      this.productId = nextProductId;
      getMultipleProductsUsageInfo([nextProductId], 50);
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (!this.state.renderChild)
      this.setState({ renderChild: true });
  }

  componentDidMount() {
    this.props.getCurrentFieldsFn(this.getCurrentFieldsFn);
  }
}

type ModalProps = $FlowTODO;
type ModalState = {
  index: number,
  finished: boolean,
};

export default class SupplierImportIngredients extends Component<ModalProps, ModalState> {
  totalIngredients: number;

  constructor(props) {
    const { ingredients, allIngredients } = props;
    super(props);
    ['addIngredientToOriginalSource',
      'updateIngredientInOriginalSource',
      'nextEntry',
      'findIngredientInSource',
      'findInIngredientInAdded',
      'skipIngredient',
      'getNextIngredient',
      'finishImport',
      'getIndexIngredient',
      'updateSourceFromCsv',
      'getIngredientFromCsv',
      'matchProductName',
      'getIngredientOptions',
      'getSearch',
      'addOrUpdateIngredient',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });

    this.ingredientOptions = this.getIngredientOptions(allIngredients);
    this.search = this.getSearch();
    this.ingredients = this.getIngredientFromCsv();
    this.ingredientsAdded = [];
    this.update = 0;
    this.added = 0;
    this.skips = ingredients.length - this.ingredients.length;
    this.endCsvReached = false;
    this.isNewIngredient = false;
    this.currentIndex = -1;
    this.state = {
      finished: false,
      currentIngredient: this.getNextIngredient(),
    };
  }

  static defaultProps = {
    isHidden: false,
    untouch: false,
    handleAdding: () => {},
    handleReceivablesAutoSkip: () => {},
    handleFinish: () => {},
  };

  getIngredientFromCsv() {
    const { sources, ingredients } = this.props;
    const difference = (source, ingredient) => source.cost.value != ingredient.sanitizedPrice ||
        source.supplierIngredientName.value != ingredient.name;
    return _.filter(ingredients, (ingredient) => {
      const source = sources.find(i => i.sku.value == ingredient.sku);
      return ingredient.sku === '' || !source || difference(source, ingredient);
    });
  }

  addIngredientToOriginalSource(ingredient: ProductSource) {
    const { sources, supplierId, handleAdding } = this.props;
    const newSourceFormat = shallowToObject(ingredient);
    this.ingredientsAdded.push(newSourceFormat);
    sources.addField({
      ...newSourceFormat,
      supplier: supplierId,
    });
    handleAdding(newSourceFormat);
  }

  updateIngredientInOriginalSource(ingredient) {
    const { sources, handleAdding } = this.props;
    const originalSource = sources.find(i => i.id.value == ingredient.id.value);
    originalSource.cost.onChange(ingredient.cost.value);
    originalSource.supplierIngredientName.onChange(ingredient.supplierIngredientName.value);
    const newSourceFormat = shallowToObject(ingredient);
    newSourceFormat.id = originalSource.id.value;
    handleAdding(newSourceFormat);
  }

  addOrUpdateIngredient(ingredient) {
    if (this.isNewIngredient) {
      this.added++;
      this.addIngredientToOriginalSource(ingredient);
    } else {
      this.update++;
      this.updateIngredientInOriginalSource(ingredient);
    }
  }

  updateSourceFromCsv(source, ingredient) {
    source.cost.onChange(ingredient.sanitizedPrice);
    source.supplierIngredientName.onChange(ingredient.name);
  }

  nextEntry(ingredient) {
    this.addOrUpdateIngredient(ingredient);
    const nextIngredient = this.getNextIngredient();
    this.setState({ currentIngredient: nextIngredient });
  }

  finishImport() {
    this.setState({ finished: true });
    this.props.handleFinish();
  }

  skipIngredient = () => {
    this.skips += 1;
    const nextIngredient = this.getNextIngredient();
    this.setState({ currentIngredient: nextIngredient });
  }

  findIngredientInSource(ingredient) {
    const { sources } = this.props;
    if (ingredient) {
      if (ingredient.sku)
        return sources.find(i => i.sku.value == ingredient.sku);
      else if (ingredient.name)
        return sources.find(i => i.supplierIngredientName.value == ingredient.name);
    }
    return undefined;
  }

  findInIngredientInAdded(ingredient) {
    if (ingredient.sku === '')
      return undefined;
    else
      return this.ingredientsAdded.find((ai) => ai.sku == ingredient.sku, this);
  }

  getIndexIngredient(currentIndex) {
    if (currentIndex < this.ingredients.length) {
      const ingredientAdded = this.findIngredientInSource(this.ingredients[currentIndex]);
      if (ingredientAdded === undefined) {
        this.isNewIngredient = true;
        return currentIndex;
      } else if (this.props.isImportReceivables && ingredientAdded.date.value) {
          this.skips += 1;
          this.props.handleReceivablesAutoSkip(ingredientAdded);
          return this.getIndexIngredient(currentIndex + 1);
        } else {
          const { percentImportUpdate } = this.props;
          const currentCost = this.ingredients[currentIndex].sanitizedPrice;
          if (Math.abs(ingredientAdded.cost.value - currentCost) < ingredientAdded.cost.value * percentImportUpdate) {
            this.updateSourceFromCsv(ingredientAdded, this.ingredients[currentIndex]);
            this.update++;
            return this.getIndexIngredient(currentIndex + 1);
          } else {
            this.isNewIngredient = false;
            return currentIndex;
          }
        }
    } else
      return -1;
  }

  getNextIngredient() {
    const index = this.getIndexIngredient(this.currentIndex + 1);
    if (index != -1) {
      this.currentIndex = index;
      const ingredient = this.ingredients[index];
      ingredient.name = ingredient.name.replace(/ +/g, ' ');
      this.isNewIngredient = this.findIngredientInSource(ingredient) === undefined;
      if (this.isNewIngredient)
        ingredient.idByProductName = this.matchProductName(ingredient.name);
      return ingredient;
    } else {
      this.props.handleFinish();
      this.endCsvReached = true;
      return undefined;
    }
  }

  matchProductName(name) {
    this.search = this.getSearch();
    let arrayResult = this.search.searchWithQueryFn(supplierIngredientNameToSearchTerm(name));
    if (arrayResult.length > 0) {
       return arrayResult[0].value;
    } else {
      return '';
    }
  }

  getIngredientOptions(allIngredients) {
    return allIngredients
      .filter(s => !s.get('tombstone') && !s.get('isSubRecipe'))
      .map(s =>
          ({ value: s.get('id'), label: s.get('name'), className: null }),
      ).toJS();
  }

  getSearch() {
    return getSearchObject(
        this.ingredientOptions, ['label'], {},
        'value',
        indexTypes.IMPORT_PURCHASE_PRODUCTS,
    );
  }

  render() {
    const { hide, isHidden, sources, uiStateActions, fileName, percentImportUpdate,
      allIngredients, allMeasures, userWeightPref, userVolumePref, ingredients, ...rest } = this.props;

    const ingredient = this.state.currentIngredient;
    return <Section>
      <Row style={{ display: 'flex', alignItems: 'center' }}>
        <Col xs={2}>
          <h4>Import Ingredients</h4>
        </Col>
        {
          this.state.finished || this.endCsvReached ?
          <Col xs={8} xsOffset={1}>
            <Row>
              <Col xs={4}>
                <span>Added: {this.added}</span>
              </Col>
              <Col xs={4} >
                <span>Skipped: {this.skips}</span>
              </Col>
              <Col xs={4}>
                <span>Updated: {this.update}</span>
              </Col>
            </Row>
          </Col>
          : null
        }
      </Row>
      <Row>
        {
          !this.state.finished && !this.endCsvReached ?
          <div>
            {
              !this.isNewIngredient ?
              <Alert bsStyle="danger">
                Price change exceeds <strong>{percentImportUpdate * 100}% </strong>,&nbsp;
                original price was <strong>${this.findIngredientInSource(ingredient).cost.value}</strong>,&nbsp;
                please confirm by clicking Next, or click Skip to retain original price
              </Alert>
              : <hr/>
            }
            <ImportIngredientForm
              {...rest}
              nextButton={this.nextEntry}
              ingredientOptions={this.ingredientOptions.slice()}
              uiStateActions={uiStateActions}
              ingredient={ingredient}
              ingredients={this.ingredients}
              sources={sources}
              isNewIngredient={this.isNewIngredient}
              allMeasures={allMeasures}
              userWeightPref={userWeightPref}
              userVolumePref={userVolumePref}
              added={this.added}
              skipped={this.skips}
              skippedUpdate={this.update}
              remaining={this.ingredients.length - this.currentIndex}
              skipActions={this.skipIngredient}
              exitActions={this.finishImport}
              addOrUpdateIngredient={this.addOrUpdateIngredient}
              findIngredientInSource={this.findIngredientInSource}
            />
          </div>
          : <div>
            <h4 style={{ textAlign: 'center' }}>
            {
              this.endCsvReached ?
              'Congratulations!  End of CSV file reached'
              : ''
            }
            </h4>
          </div>
        }
      </Row>
    </Section>;
  }

  componentDidUpdate() {
    const ingredientOptions = this.getIngredientOptions(this.props.allIngredients);
    if (_.differenceBy(ingredientOptions, this.ingredientOptions, 'label').length > 0) {
      this.ingredientOptions = ingredientOptions;
      this.search = this.getSearch();
    }
  }
}

const priceListIngredientNameSearchOpts = {
  ...ingredientListSearchOpts,

  isTokenBoostable(token) {
    // short tokens are often acronyms or common words
    if (token.length <= 2) return false;

    // numbers followed by maybe a few letters are usually sizes or codes
    if (token.match(/^[\d]+(.{1,3})?/)) return false;

    return true;
  },

  preprocessingSteps: [stripLeadingDigitToken],
  tokenSeparator: /[\s\-,*#()/]+/,
};

type RowProps = {
  allMeasures: Measures,
  untouch: string => void,
  ingredientOptions: $FlowTODO, // select options; see SourceRowSelectIngredient
  isNewIngredient: boolean,
  source: $ObjMap<ProductSource, <V>(V) => FormField<V>>,
}

class ImportIngredientRow extends Component<RowProps> {

  static propTypes = {
    allMeasures: ImmutablePropTypes.mapOf(PropTypes.measure).isRequired,
    untouch: ReactPropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.handleInputRef = this.handleInputRef.bind(this);
  }

  handleInputRef(input) {
    const { source, isNewIngredient } = this.props;
    if (!this.initialInput && input && isNewIngredient) {
      this.initialInput = input;
      this.initialInput.onInputChange(supplierIngredientNameToSearchTerm(source.supplierIngredientName.value));
    }
  }

  render() {
    const {
      ingredientOptions, allMeasures, uiStateActions, untouch, isNewIngredient,
      source, openEditIngredientModal, showingNewIngredientModal,
      userVolumePref, userWeightPref, detailedIngredients, importingReceivables,
    } = this.props;

    const productsComparisonUnit = pickAllComparisonUnits(
      [source].map(shallowToObject),
      detailedIngredients.map(deets => deets.get('measures')),
      allMeasures, userWeightPref, userVolumePref, true,
    );

    return <SourceRow
      allMeasures={allMeasures}
      ingredientOptions={ingredientOptions}
      ingredientSearchOpts={priceListIngredientNameSearchOpts}
      searchIndexName="ingredientImportIndex"
      untouch={untouch}
      uiStateActions={uiStateActions}
      comparisonUnit={productsComparisonUnit[source.product.value]}
      conversions={detailedIngredients.getIn([source.product.value, 'measures'])}
      source={source}
      fromImport
      compact={false}
      autoOpen={isNewIngredient}
      openEditIngredientModal={openEditIngredientModal}
      showingNewIngredientModal={showingNewIngredientModal}
      userWeightPref={userWeightPref}
      userVolumePref={userVolumePref}
      inputRef={!this.initialInput ? this.handleInputRef : undefined}
      importingReceivables={importingReceivables}
    />;
  }
}
