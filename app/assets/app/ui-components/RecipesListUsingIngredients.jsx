import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPropTypes from 'prop-types';

import { getReverseDependencies } from '../webapi/endpoints';
import { actionCreators as ReverseDependencyActions } from '../reducers/reverseDependencies';
import SearchableList from './pages/helpers/SearchableList';
import { bindActionCreators } from 'redux';

@connect(
    (state, ownProps) => ({
        reverseDependencies: state.getIn(['reverseDependencies', ownProps.ingredientId]),
      }),
    dispatch => ({
      reverseDependencyActions: bindActionCreators(ReverseDependencyActions, dispatch),
    }),
)

export default class RecipesListUsingIngredients extends Component {
    static propTypes = {
      ingredientId: ReactPropTypes.number.isRequired,
    };

    componentWillMount() {
      getReverseDependencies(this.props.ingredientId);
    }

    render () {
      const { reverseDependencies, reverseDependencyActions, ingredientId } = this.props;
      let content;

      if (!reverseDependencies || reverseDependencies.isEmpty()) {
        content = <NoRecipesComponent />;
      } else {
        content = <SearchableList
            searchKey="recipe-ing"
            reduxStatePath={['reverseDependencies', ingredientId]}
            leavePage={reverseDependencyActions.drop}
            checkable={false} copiable={false} deletable={false}
            noun="Reverse Dependencies"
            baseUrl="/recipes"
            fullPage={false}
        />;
      }

      return (
          <div className="reverse-dependencies-list">
            {content}
          </div>
      );
    }
}

const NoRecipesComponent = () => <div className="block-text">
  <p>
    No recipes use this ingredient.
  </p>
</div>;
