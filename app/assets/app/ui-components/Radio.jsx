import React from 'react';

import { Radio as BaseRadio } from 'react-bootstrap';

export default ({children, ...passthroughProps}) =>
    <BaseRadio {...passthroughProps}>
      <span /> {/* for our fancy CSS stuff to insert the visible button */}
      {children}
    </BaseRadio>;
