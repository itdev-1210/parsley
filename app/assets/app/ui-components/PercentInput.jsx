import React, { Component } from 'react';

import { MaskedNumberInput } from './MaskedInput';
import { valueFromEvent } from "../utils/form-utils";
import { truncated } from '../utils/unit-conversions';

export const toPercent = rawValue => isNaN(rawValue) ? rawValue : truncated(rawValue * 100, 1);

/*
 * Doesn't include provision for the percent marker - just handles the division/multiplication weirdness
 */
export default function PercentInput(
    {onChange: rawOnChange, onBlur: rawOnBlur, value: rawValue, ...passthrough} // props
) {

  function onChange(ev) {
    let newVal = valueFromEvent(ev);
    return rawOnChange(newVal && newVal / 100);
  }

  function onBlur(ev) {
    if (!rawOnBlur) return;
    let newVal = valueFromEvent(ev);
    return rawOnBlur(newVal && newVal / 100);
  }

  passthrough = { onChange, onBlur, value: toPercent(rawValue), ...passthrough };

  return <MaskedNumberInput className="percent-input form-control" {...passthrough} />;
}
