// @flow

import React, { Component } from "react";
import classnames from "classnames";
import { Button, Glyphicon } from "react-bootstrap";

import { featureGuarded } from './utils/FeatureGating';

type Props = {
  onClick: () => any,
  noun: string,
  className?: string, // optional property
}

@featureGuarded
export default class AddButton extends Component<Props> {
  static defaultProps = {
    noun: ""
  };

  render() {
    const { className, noun, ...rest } = this.props;
    return (
        <Button bsStyle="default" className={classnames(className, "green-btn", "indent")}
            {...rest}>
          <Glyphicon glyph="plus" />
          Add {noun}
        </Button>);
  }
}
