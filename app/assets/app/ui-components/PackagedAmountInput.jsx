import React, { Component } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import ReactPropTypes from 'prop-types'
import { InputGroup } from 'react-bootstrap';

import * as ParsleyPropTypes from './utils/PropTypes';
import { valueFromEvent, shallowToObject } from '../utils/form-utils';
import { convert, unitDisplayName, divideQuantities, purchasingRound, rescaleUnits } from '../utils/unit-conversions';
import {
  builtinPackages,
  effectivePackageSize,
} from '../utils/packaging';
import TextWithHelp from './TextWithHelp';
import { MaskedNumberInput } from './MaskedInput';
import Select from './Select';
import Validate from 'common/ui-components/Validate';
import { noticeError } from 'common/utils/errorAnalytics';

export function IndeterminateQuantityWarning() {
  return <TextWithHelp inModal
    helpText="The quantity of this ingredient is not specified in some recipes"
  >
    +
  </TextWithHelp>;
}

export default class PackagedAmountInput extends Component {
  static propTypes = {
    quantityFields: ParsleyPropTypes.quantityField.isRequired,
    selectedSource: ParsleyPropTypes.reduxFormField(ReactPropTypes.number).isRequired,
    sources: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,
    conversions: ImmutablePropTypes.listOf(ParsleyPropTypes.conversion),
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
    amountInputStyle: ReactPropTypes.object,
    autoAdjust: ReactPropTypes.bool,
    purchaseWeightSystem: ReactPropTypes.string,
    purchaseVolumeSystem: ReactPropTypes.string,
  };

  static defaultProps = {
    autoAdjust: true,
    defaultEmptyValue: 0,
  }

  constructor(props) {
    super(props);

    this.state = {
      tempAmount: undefined,
    };
  }

  translateEvent(eventName, ev) {
    const newNumPackages = valueFromEvent(ev);
    const {
        quantityFields: { amount, measure, unit },
        selectedSource, defaultEmptyValue,
    } = this.props;
    if (selectedSource.value) {
      const packageSize = this.effectivePackageSize(selectedSource.value);

      measure[eventName](packageSize.measure);
      unit[eventName](packageSize.unit);

      if (typeof newNumPackages === 'number') {
        amount[eventName](packageSize.amount * newNumPackages);
      } else {
        amount[eventName](defaultEmptyValue);
      }
    } else {
      this.setState({ tempAmount: newNumPackages });
    }
  }

  onSourceSelect(eventName, ev) {
    const newSourceId = valueFromEvent(ev);
    const {
      sources,
      quantityFields: { amount, measure, unit },
      selectedSource,
      packagedField,
      packageNameField,
      subPackageNameField,
      packageSizeField,
      superPackageSizeField,
      supplierField,
      supplierNameField,
      autoAdjust,
      conversions,
    } = this.props;


    if (!newSourceId) return; // to avoid errors; not clearable, so no need to check that case

    const newSource = sources.find(s => s.get('id') === newSourceId);
    if (packagedField) { // from InventoryDetails: Update these additional fields
      packagedField.onChange(newSource.get('packaged'));
      packageSizeField.onChange(newSource.get('packageSize'));
      superPackageSizeField.onChange(newSource.get('superPackageSize'));
      packageNameField.onChange(newSource.get('packageName'));
      subPackageNameField.onChange(newSource.get('subPackageName'));
      supplierField.onChange(newSource.get('supplier'));
      supplierNameField.onChange(newSource.get('supplierName'));
    }

    const newPackageSize = effectivePackageSize(newSource);
    let newAmount = amount.value;

    if (!autoAdjust) {
      const oldSourceId = selectedSource.value;
      if (oldSourceId) {
        const oldPackageSize = this.effectivePackageSize(oldSourceId);

        // kind of counter-intuitive, but: we want no auto-adjustment from the
        // *user's* perspective, which means number of packages needs to stay the
        // same, even if the actual *quantity* changes
        const oldQuantity = shallowToObject({ amount, measure, unit });
        const numPackages = divideQuantities(oldQuantity, oldPackageSize, conversions);

        newAmount = newPackageSize.amount * numPackages;
      }

    // Remember to guard agaisnt non-existent unit cases,
    // like in user-added ingredients in purchase orders
    } else if (unit.value) {
      // we don't need to change the quantity, but we are changing unit so
      // convert the amount to that new unit.
      newAmount = convert(amount.value, unit.value, newPackageSize.unit, conversions);
    }

    // Default behaviour: Auto-convert the amount value according to packageSize's change
    if (this.state.tempAmount) {
      newAmount *= this.state.tempAmount;
      this.setState({ tempAmount: undefined });
    }

    amount.onChange(newAmount);
    measure.onChange(newPackageSize.measure);
    unit.onChange(newPackageSize.unit);
    selectedSource[eventName](ev);
  }

  // should match with RecipeUtils.Conversions.abbrPackageName
  static fullPackageName(source, conversions) {
    if (!source) return null;

    const packageUnitName = unitDisplayName(source.get('unit'), conversions);
    function packageKeyToDisplayName(key) {
      return builtinPackages[key] ? builtinPackages[key].displayName : key;
    }

    let returnValue;
    if (source.get('packaged')) {
      const packageName = packageKeyToDisplayName(
          source.get('subPackageName') || source.get('packageName') || 'package'
      );

      returnValue = `${source.get('packageSize')}${packageUnitName} ${packageName}`;

      if (source.get('subPackageName')) {
        const superPackageName = packageKeyToDisplayName(source.get('packageName') || 'package');
        returnValue = `${superPackageName} of ${source.get('superPackageSize')}\u00d7${returnValue}`;
      }
    } else {
      returnValue = packageUnitName;
    }

    return returnValue;
  }

  effectivePackageSize(sourceId) {
    const sourceInfo = this.props.sources.find(s => s.get('id') == sourceId);
    if (sourceId && !sourceInfo) {
      noticeError(`PackagedAmountInput tried to get sourceInfo for nonexistent source ${sourceId}`);
    }
    return effectivePackageSize(sourceInfo);
  }

  render() {

    const {
        quantityFields,
        quantityFields: { amount, unit, measure },
        selectedSource,
        conversions,
        sources, indeterminateRequirement, units,
        amountInputStyle,
        autoAdjust,
        purchaseWeightSystem,
        purchaseVolumeSystem,
    } = this.props;

    if (!sources) {

      const userPurchaseSystem = measure.value === 2 ? purchaseWeightSystem : purchaseVolumeSystem;
      let rescaledUnit = shallowToObject(quantityFields);
      if (rescaledUnit.amount && rescaledUnit.unit) {
        rescaledUnit = rescaleUnits(rescaledUnit, userPurchaseSystem === 'metric', true);
      }

      const determinateAmount = rescaledUnit.amount ?
        purchasingRound(rescaledUnit.amount, 2).toFixed(2) :
          null;
      const determinateUnit = rescaledUnit.unit ?
        ' ' + unitDisplayName(rescaledUnit.unit, conversions) :
          null;

      const indeterminateWarning = indeterminateRequirement ?
          <IndeterminateQuantityWarning key="indeterminate"/> :
          null;

      return <span>{[
          determinateAmount,
          indeterminateWarning,
          determinateUnit,
      ]}</span>;
    }

    const erroredFields = [amount, unit, measure].filter(
        f => f.touched && f.error
    );

    const packageSize = this.effectivePackageSize(selectedSource.value);
    let amountValue;
    if (!isNaN(amount.value) && typeof amount.value === 'number') {
      amountValue = packageSize && unit.value ?
      divideQuantities(
        shallowToObject({ amount, unit, measure }),
        packageSize,
        conversions,
      ) : this.state.tempAmount;
    } else {
      amountValue = '';
    }

    const packageOptions = sources.map(s => {
      let label = PackagedAmountInput.fullPackageName(s, conversions);
      if (s.has('supplierName')) label += `\u2003(${s.get('supplierName')})`;
      return {
        value: s.get('id'),
        label,
      };
    }).toArray();

    return <Validate className="quantityInput" compact
                     {...(erroredFields.length > 0 ? erroredFields[0] : amount)}>
      <div style={amountInputStyle}>
        <MaskedNumberInput
          normalizeInitial
          value={amountValue}
          precision={units.getIn([packageSize ? packageSize.unit : unit.value, 'precision'])}
          quantum={units.getIn([packageSize ? packageSize.unit : unit.value, 'quantum'])}
          onChange={this.translateEvent.bind(this, 'onChange')}
          onBlur={this.translateEvent.bind(this, 'onBlur')}
        />
      </div>
      <InputGroup.Addon>
        { packageOptions.length === 1 ?
          <div style={{ paddingLeft: '10px', width: '165px' }}>{packageOptions[0].label}</div>
          : <Select
                  disabled={!conversions /* proxy for no ingredient selected */}
                  options={packageOptions} {...selectedSource}
                  onChange={this.onSourceSelect.bind(this, 'onChange')}
                  onBlur={this.onSourceSelect.bind(this, 'onBlur')}
            />
        }
      </InputGroup.Addon>
    </Validate>;
  }
}
