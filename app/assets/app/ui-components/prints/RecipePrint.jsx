import React, { Component } from 'react';
import ReactPropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Immutable from 'immutable';
import _ from 'lodash';

import * as ParsleyPropTypes from '../utils/PropTypes';
import { recipePrintFormatOptions } from '../../utils/form-utils';
import {
  truncated,
  rescaleUnits,
  purchasingRound,
  divideQuantities,
  unitDisplayName,
  unitDisplayQuantum,
  unitDisplayPrecision,
} from '../../utils/unit-conversions';
import { changeNewLineToBreak } from 'common/utils/reactHelper';

export default class RecipeFromProductioOrderPrint extends Component {

  static propTypes = {
    idx: ReactPropTypes.number.isRequired,
    product: ReactPropTypes.object.isRequired, // shape of flow type ParsleyPropTypes.ProductSource
    printedQuantity: ParsleyPropTypes.quantity.isRequired,
    format: ReactPropTypes.oneOf(recipePrintFormatOptions.map(f => f.get('value')).toJS()).isRequired,
    includePhotos: ReactPropTypes.bool,
    breakPage: ReactPropTypes.bool,
    companyLogoUrl: ReactPropTypes.string,
    batchSize: ReactPropTypes.string,
    batchLabel: ReactPropTypes.string,

    // from state
    allIngredients: ImmutablePropTypes.list.isRequired,
    detailedIngredients: ImmutablePropTypes.list.isRequired,
    productionOrder: ParsleyPropTypes.productionOrder.isRequired,
  };

  static defaultProps = {
    includePhotos: false,
    breakPage: false,
    companyLogoUrl: '',
    batchSize: '',
    batchLabel: '',
  }

  render() {
    const {
      idx, product, printedQuantity,
      format, includePhotos,
      breakPage, companyLogoUrl, batchSize, batchLabel,

      allIngredients, detailedIngredients, productionOrder,
    } = this.props;
    const {
      name, description, photo,
      measures, recipe, ingredient,
    } = product;
    const steps = recipe.steps;
    const conversions = Immutable.fromJS(measures);

    let recipeProduced;
    const recipeFromProductionOrder = productionOrder && productionOrder.get('recipeList').find(r => r.get('name') === name);
    if (recipeFromProductionOrder) {
      // nested recipe
      recipeProduced = recipeFromProductionOrder.get('totalProduced');
    } else {
      recipeProduced = Immutable.fromJS(printedQuantity);
    }

    const thisRecipeMultiplier = recipeProduced.get('unit')
      ? divideQuantities(recipeProduced.toJS(), recipe.recipeSize, conversions)
      : recipeProduced.get('amount') / recipe.recipeSize.amount;

    const ingredientsFirst = format === 'ingredients-first';
    let recipeContainerClassNames = 'no-page-break-inside pt-1 pb-4';
    recipeContainerClassNames = breakPage ? recipeContainerClassNames + ' break-page-before break-page-after' : recipeContainerClassNames;

    const { amount: recipeProducedAmount, unit: recipeProducedUnit } = recipeProduced.get('unit')
      ? rescaleUnits(recipeProduced.toJS())
      : {
        amount: recipeProduced.get('amount'),
      };
    const recipeTitle = <h1 className="recipe-print-name">
        {ingredient ? <span className="sub-recipe-print-name">{name}</span> : name}&nbsp;
        ({batchSize ?
        `${truncated(batchSize, 2)} ${batchLabel} / `
        : null}
        { purchasingRound(recipeProducedAmount, unitDisplayPrecision(recipeProducedUnit), unitDisplayQuantum(recipeProducedUnit)) }&nbsp;
        { recipeProducedUnit
          ? unitDisplayName(recipeProducedUnit, conversions)
          : 'portions'
        })
      </h1>;
    const recipeDesc = description ? <p>{description}<br /><br /></p> : null;
    return <div className={recipeContainerClassNames}>
      <table className="recipe-logo">
        <tr style={{ height: '120px' }}>
          <td style={{ width: '90%', paddingTop: '80px' }}>{recipeTitle}</td>
          {companyLogoUrl ? <td><div className="recipe-gray-label"><img className="company-logo" /></div></td> : null}
        </tr>
        {includePhotos && photo && <tr><td colSpan="2"><img className="recipe-main-logo" id={`recipe-logo-${idx}`} /></td></tr>}
        {recipeDesc ? <tr><td colSpan={'2'}>{recipeDesc}</td></tr> : null}
      </table>
      <table className="recipe-print">
        { !ingredientsFirst ?
            <tr className="separator-line">
              <td className="ordinal header" />
              <td className="ingredient header pl-1 pr-1"><b>Ingredients</b></td>
              <td className="instructions header pl-1 pr-1"><b>Method</b></td>
            </tr>
            : null
        }
        { !ingredientsFirst ? steps.map((step, stepIndex) => (
            <tr key={`separator-line-${stepIndex}`} className="separator-line">
              <td className="ordinal">
                <div className="pt-1" />
                {stepIndex + 1}
                <div className="pb-1" />
              </td>
              <td className="ingredient pl-1 pr-1">
                <div className="pt-05" />
                { step.ingredients.map(ingredient => {
                  let ingredientId = ingredient.ingredient;

                  const isPrep = detailedIngredients.hasIn([ingredientId, 'isPrepOf']);
                  // raw in the sense of a physical raw ingredient, not in the sense of unprocessed piece of data
                  const rawIngredientId = isPrep
                    ? detailedIngredients.getIn([ingredientId, 'isPrepOf'])
                    : ingredientId;
                  const allPreps = detailedIngredients.getIn([rawIngredientId, 'simplePreps']) || Immutable.List();
                  const prepName = allPreps.findKey(id => id === ingredientId);

                  const [baseAmount, baseUnit, baseMeasure] = ['amount', 'unit', 'measure'].map(
                    key => ingredient.quantity ? parseFloat(ingredient.quantity[key]) : ingredient[key],
                  );
                  const { amount: displayAmount, unit: displayUnit } = (baseAmount && baseUnit)
                    ? rescaleUnits({
                      amount: baseAmount * thisRecipeMultiplier,
                      unit: baseUnit,
                      measure: baseMeasure,
                    })
                    : [baseAmount, baseUnit];
                  const unitName = displayUnit ?
                    unitDisplayName(
                        displayUnit,
                        detailedIngredients.getIn([rawIngredientId, 'measures']))
                    : null;
                  const thisIngredient = allIngredients.find((i) => String(i.get('id')) === String(rawIngredientId));
                  return (<div key={`step-desc-${stepIndex}`}>
                    <div className="pt-05" />
                    <span style={thisIngredient.get('isSubRecipe') ? { fontStyle: 'italic' } : {}}>{ thisIngredient.get('name') }</span>
                    {
                      prepName
                          ? ', ' + prepName
                      /*
                        hide plaintext "prep" in the cases where that input
                        would be hidden in the editor; for user purposes, any
                        value in there does not exist
                      */
                      : (allPreps.isEmpty() && ingredient.description)
                          ? ', ' + (ingredient.description)
                          : null
                    }
                    { displayAmount ? ', ' + purchasingRound(displayAmount, unitDisplayPrecision(displayUnit), unitDisplayQuantum(displayUnit)) : null }
                    { unitName }
                  </div>);
                })}
                <div className="pb-1" />
              </td>
              <td className="instructions pl-1 pr-1">
                <div className="pt-1" />
                { includePhotos && step.photo ?
                  <table className="recipe-step-table">
                    <tr>
                      <td>{changeNewLineToBreak(step.description)}</td>
                      <td className="recipe-step-photo-container"><img className="recipe-step-photo" id={`recipe-step-photo-${idx}-${stepIndex}`} /></td>
                    </tr>
                  </table>
                  : changeNewLineToBreak(step.description)
                }
                <div className="pb-1" />
              </td>
            </tr>
          )) : null
        }
        { ingredientsFirst ? <div key="step-desc-ing-first">
            <tr className="separator-line">
              <th className="ingredient header">Ingredient</th>
              <th className="amount header">Amount</th>
              <th className="unit header">Unit</th>
              <th className="preparation header">Preparation</th>
            </tr>
            { _.flatMap(steps, step => step.ingredients).map((ingredient, i) => {

                let ingredientId = ingredient.ingredient;

                const isPrep = detailedIngredients.hasIn([ingredientId, 'isPrepOf']);
                // raw in the sense of a physical raw ingredient, not in the sense of an unprocessed piece of data
                const rawIngredientId = isPrep
                    ? detailedIngredients.getIn([ingredientId, 'isPrepOf'])
                    : ingredientId;
                const allPreps = detailedIngredients.getIn([rawIngredientId, 'simplePreps'])
                          || Immutable.List();

                const [baseAmount, baseUnit, baseMeasure] = ['amount', 'unit', 'measure'].map(
                    key => ingredient.quantity ? parseFloat(ingredient.quantity[key]) : ingredient[key],
                );
                const { amount: displayAmount, unit: displayUnit } = (baseAmount && baseUnit)
                    ? rescaleUnits({
                      amount: baseAmount * thisRecipeMultiplier, baseUnit,
                      unit: baseUnit, measure: baseMeasure,
                    })
                    : [baseAmount, baseUnit];
                const thisIngredient = allIngredients.find((i) => String(i.get('id')) === String(rawIngredientId));
                return <tr className="separator-line">
                  <td key={i} className="ingredient">
                    <span style={thisIngredient.get('isSubRecipe') ? { fontStyle: 'italic' } : {}}>{ thisIngredient.get('name') }</span>
                    {
                      /*
                        hide plaintext "prep" in the cases where that input
                        would be hidden in the editor; for user purposes, any
                        value in there does not exist
                      */
                      ingredient.description && allPreps.isEmpty()
                          ? ', ' + (ingredient.description)
                          : null
                    }
                  </td>
                  <td className="amount">
                    { displayAmount !== null ? purchasingRound(displayAmount, unitDisplayPrecision(displayUnit)) : null }
                  </td>
                  <td className="unit">
                    { displayUnit ? unitDisplayName(
                        displayUnit,
                        detailedIngredients.getIn([rawIngredientId, 'measures']),
                    ) : null }
                  </td>
                  <td className="preparation">
                    {
                      /*
                        hide plaintext "prep" in the cases where that input
                        would be hidden in the editor; for user purposes, any
                        value in there does not exist
                      */
                      allPreps.isEmpty()
                          ? ingredient.description
                          : isPrep && allPreps.keyOf(ingredientId)
                    }
                  </td>
                </tr>;
            })}
          </div> : null
        }
      </table>
      { ingredientsFirst
          ? <div>
              <br /><br />
              <b>Method</b>
              <br />
              <table className="recipe-step-table">
                { steps.map((step, stepIndex) => <tr key={stepIndex}>
                      <td colSpan={includePhotos && step.photo ? '1': '2'}>
                        <b>{ stepIndex + 1 }.</b> {changeNewLineToBreak(step.description)}
                      </td>
                      {includePhotos && step.photo && <td className="recipe-step-photo-container">
                          <img className="recipe-step-photo" id={`recipe-step-photo-${idx}-${stepIndex}`} />
                        </td>
                      }
                    </tr>,
                  )
                }
              </table>
          </div>
          : null
      }
    </div>;
  }
}