import React, { Component } from 'react';
import ReactDOMServer from "react-dom/server";
import { connect } from 'react-redux';

import _ from 'lodash';
import moment from 'moment';

import {
  getMenuItemsList
} from "../webapi/endpoints";

import { printContent } from '../utils/printing';

@connect(
  (state, ownProps) => ({
    menuItems: state.get('menuItems')
  })
)
export default class PrintMenu extends Component {

  printMenu() {
    const { name, date, sections, menuItems, onComplete, } = this.props;

    const title = (!date) ? name.value : `${name.value} for ${moment(date.value).format('MM/DD/YY')}`;
    const sectionsContent = sections.map(section => {
      const { name, items } = section;

      const itemsContent = _.map(items, item => {
        const itemName = menuItems && menuItems.find(menuItem => menuItem.get('id') === _.get(item, 'product.value'));
        const itemDesc = menuItems && menuItems.find(menuItem => menuItem.get('id') === _.get(item, 'product.value'));
        return <div className="menu-section-item mb-2">
          <div className="menu-section-item-header"><i>{itemName && itemName.get('name')}</i></div>
          <div className="menu-section-item-desc">{itemDesc && itemDesc.get('description')}</div>
        </div>
      });

      return <div className="menu-section mt-2 mb-2">
        <div className="menu-section-header mb-2"><h4>{name.value}</h4></div>
        {itemsContent}
      </div>
    });

    const content = ReactDOMServer.renderToString(
      <html>
      <head>
      </head>
      <body>
      <div className="menu-content text-center">
        <h1 className="print-header underline">{title}</h1>
        {sectionsContent}
      </div>
      </body>
      </html>
    );

    return printContent(content).then(onComplete);
  }

  render() {
    return <div></div>
  }
  
  componentWillMount() {
    getMenuItemsList();
  }

  componentDidMount() {
    this.printMenu();
  }
}