// @flow

import React, { Component } from 'react';
import _ from 'lodash';
import Immutable from 'immutable';

import { connect } from 'react-redux';

import Joyride, { StepProps } from 'react-joyride';
import OnboardingTooltip from './OnboardingTooltip';

type Props = {
  pageKey: string,

  // redux-form stuff, typechecking weird, see facebook/flow#3405
  shouldShowOnboarding?: boolean,
  onboardingInfo?: Immutable.Map<*, *>
}

@connect(
    state => ({
      onboardingInfo: state.getIn(['onboardingInfo']),
      shouldShowOnboarding: state.getIn(['userInfo', 'shouldShowOnboarding']),
    }),
)
export default class Onboarding extends Component<Props> {

  render() {
    const {
      pageKey, onboardingInfo = Immutable.Map(), shouldShowOnboarding,
    } = this.props;
    if (!shouldShowOnboarding || !onboardingInfo) return null;


    // $FlowFixMe: only with records can we assert member existence/value
    const thisPageSteps = onboardingInfo.get('steps').filter(s => s.get('page') === pageKey).sortBy(s => s.get('index'));
    // $FlowFixMe: only with records can we assert member existence/value
    const thisPageNextStep = onboardingInfo.get('visitedBoxes').filter(s => s.get('page') === pageKey).first();

    let stepIndex = 0;
    if (thisPageNextStep) {
      stepIndex = thisPageNextStep.get('nextStep');
    }
    return <Joyride {...{
      showProgress: false,
      showSkipButton: false,
      continuous: thisPageSteps.size > 1,
      stepIndex,
      run: !thisPageSteps.isEmpty() && (stepIndex <= thisPageSteps.last().get('index')),
      steps: thisPageSteps.map(step => this.makeStep(step)).toJS(),
      floaterProps: {
        offset: 5,
        disableAnimation: true,
      },
    }} />;

  }

  makeStep(step: Immutable.Map<$FlowTODO, $FlowTODO>): StepProps {
    let target = step.get('targetSelector') || 'body';
    return {
      placement: step.get('placement'),
      disableBeacon: true,
      styles: {
        options: {
          zIndex: 10000,
          arrowColor: '#' + (
              step.get('placement') === 'bottom'
                  // $FlowFixMe: only with records can we assert member existence/value
                  ? step.get('titleBgColor')
                  // $FlowFixMe: only with records can we assert member existence/value
                  : step.get('descBgColor')
          ),
          overlayColor: 'rgba(0, 0, 0, 0.25)',
        },
      },
      target,
      title: step.get('title'),
      tooltipComponent: <OnboardingTooltip
          // $FlowFixMe: only with records can we assert member existence/value
          pageKey={step.get('page')}
          // $FlowFixMe: only with records can we assert member existence/value
          stepIndex={step.get('index')}
          // $FlowFixMe: only with records can we assert member existence/value
          title={step.get('title') /* overridden by 'title' above, but makes typechecker happy */}
          // $FlowFixMe: only with records can we assert member existence/value
          description={step.get('description')}
          // $FlowFixMe: only with records can we assert member existence/value
          titleBgColor={step.get('titleBgColor')}
          // $FlowFixMe: only with records can we assert member existence/value
          descBgColor={step.get('descBgColor')}
          // $FlowFixMe: only with records can we assert member existence/value
          borderColor={step.get('borderColor')}
          leftImage={step.get('leftImage')}
          rightImage={step.get('rightImage')}
          nextButtonText={step.get('nextButtonText')}
          backButtonText={step.get('backButtonText')}
          // $FlowFixMe: only with records can we assert member existence/value
          bodyWidth={step.get('bodyWidth')}
          nextBgColor={step.get('nextBgColor')}
          // $FlowFixMe: only with records can we assert member existence/value
          closeBtnColor={step.get('closeBtnColor')}
          handleCallback={_.identity}
      />,
    };
  }
}