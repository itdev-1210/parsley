// @flow
import React from "react";
import ReactPropTypes from 'prop-types'
import ImmutablePropTypes from "react-immutable-proptypes";

import _ from "lodash";
import type { PermissionLevel } from 'common/utils/constants';

export function reduxFormField(basicPropType: () => any) { // react proptypes are functions
  return ReactPropTypes.shape({ value: basicPropType });
}

export function deepReduxFormField(propTypeShape: Object) {
  return ReactPropTypes.shape(
    _.mapValues(propTypeShape, reduxFormField)
  );
}

// Assorted API types in their local representation

var basicMeasureFields = {
  name: ReactPropTypes.string.isRequired
};

var basicUnitFields = {
  name: ReactPropTypes.string.isRequired,
  abbr: ReactPropTypes.string,
  size: ReactPropTypes.number.isRequired
};

export var leafUnit = ImmutablePropTypes.contains(
    _.omit(basicUnitFields, "id")
);

export var leafMeasure = ImmutablePropTypes.contains(
    _.omit(basicMeasureFields, "id")
);

export var measure = ImmutablePropTypes.contains(_.merge({},
    basicMeasureFields,
    { units: ImmutablePropTypes.mapOf(leafUnit) }
));

export var unit = ImmutablePropTypes.contains(_.merge({},
    basicUnitFields,
    { measures: ImmutablePropTypes.mapOf(leafMeasure) }
));

const conversionShape = {
  amount: ReactPropTypes.number.isRequired, // float
  measure: ReactPropTypes.number.isRequired, // id
  preferredUnit: ReactPropTypes.number.isRequired, //id
  conversionMeasure: ReactPropTypes.number.isRequired, // id
  conversionUnit: ReactPropTypes.number.isRequired, // id
  conversion: ReactPropTypes.number.isRequired // float
};

export var conversion = ImmutablePropTypes.contains(conversionShape);
export var conversionFormField = deepReduxFormField(conversionShape);

export type ProductSource = {
  id?: number, // id - only exists for sources that already exist in the DB
  product?: number,
  preferred?: boolean,
  supplier?: number, // id,
  unit: number, // id
  measure: number, // id
  cost: ?number, // float; should be decimal, but JS is hard
  packaged: boolean,
  packageName: ?string,
  packageSize: number, // float; 1 if !packaged
  subPackageName: ?string,
  superPackageSize: ?number,
  sku: ?string,
  pricePer: ('super-package' | 'package' | 'unit'),
  pricingMeasure: ?number,
  pricingUnit: ?number,
  pricingUnitLabel: ?string,
  supplierIngredientName: ?string
};

const productSourceShape = {
  id: ReactPropTypes.number, // id - only exists for sources that already exist in the DB
  product: ReactPropTypes.number,
  preferred: ReactPropTypes.bool,
  supplier: ReactPropTypes.number, // id,
  unit: ReactPropTypes.number.isRequired, // id
  measure: ReactPropTypes.number.isRequired, // id
  cost: ReactPropTypes.number, // float; should be decimal, but JS is hard
  packaged: ReactPropTypes.bool.isRequired,
  packageName: ReactPropTypes.string,
  packageSize: ReactPropTypes.number.isRequired, // float; 1 if !packaged
  subPackageName: ReactPropTypes.string,
  superPackageSize: ReactPropTypes.number,
  sku: ReactPropTypes.string,
  pricePer: ReactPropTypes.string,
  pricingMeasure: ReactPropTypes.number, // id
  pricingUnit: ReactPropTypes.number, // id
  supplierIngredientName: ReactPropTypes.string

};

export var productSource = ImmutablePropTypes.contains(productSourceShape);
export var productSourceFormField = deepReduxFormField(productSourceShape);

export type ProductInfo = $FlowTODO;

const productInfoShape = {
  ingredient: ReactPropTypes.bool.isRequired,
  measures: ReactPropTypes.arrayOf(
    ReactPropTypes.shape({
      measure: ReactPropTypes.number.isRequired,
      preferredUnit: ReactPropTypes.number.isRequired,
      conversionMeasure: ReactPropTypes.number.isRequired,
      conversionUnit: ReactPropTypes.number.isRequired,
      conversion: ReactPropTypes.number.isRequired,
      customName: ReactPropTypes.string.isRequired
    })
  ),
  name: ReactPropTypes.string.isRequired,
  // nutrientInfo: ReactPropTypes.arrayOf(),
  // purchaseInfo: ReactPropTypes.
  salable: ReactPropTypes.bool.isRequired,
  // simplePreps: ReactPropTypes.
  // tags: ReactPropTypes.arrayOf(),
};

export var productInfo = ImmutablePropTypes.contains(productInfoShape);
export var productInfoFormField = deepReduxFormField(productInfoShape);

export type SubordinateUserDescription = {
  id: number,
  email: ?string,
  firstName: ?string,
  lastName: ?string,
  upstreamRelativeName: ?string,
};

export var upstreamProductInfo = ReactPropTypes.shape({
  measureIds: ReactPropTypes.arrayOf(ReactPropTypes.number),
  prepKeys: ReactPropTypes.arrayOf(ReactPropTypes.string),
});

export type DownStreamList = Array<SubordinateUserDescription>;

// the kind of list returned by e.g. /api/suppliers, and accepted by SearchableList and other components that
// need to display a name-only list of a certain kind of entity
export type SummaryList = Array<{
  id: number,
  name: string,
  tags: ?Array<string>,
}>

export var summaryList = ImmutablePropTypes.listOf(ImmutablePropTypes.contains({
  id: ReactPropTypes.number.isRequired,
  name: ReactPropTypes.string.isRequired,
  tags: ImmutablePropTypes.listOf(ReactPropTypes.string)
}));

export var mutableSummaryList = ReactPropTypes.arrayOf(
  ReactPropTypes.shape({
    name: ReactPropTypes.string.isRequired,
    id: ReactPropTypes.number.isRequired,
    tags: ReactPropTypes.arrayOf(ReactPropTypes.string)
  })
);

export var ingredientConsumerInfo = ImmutablePropTypes.contains({
  name: ReactPropTypes.string.isRequired,
  tags: ImmutablePropTypes.listOf(ReactPropTypes.string).isRequired,
  measures: ImmutablePropTypes.listOf(conversion).isRequired,

  preferredSource: ReactPropTypes.number, // id; not present if no info entered, or in-house
  inHouse: ReactPropTypes.bool.isRequired,

  primaryMeasure: ReactPropTypes.number.isRequired, // id
  primaryUnit: ReactPropTypes.number.isRequired, // id
  unitCost: ReactPropTypes.number // number; if not present, user needs to be prompted to add it (somehow or other)
});

export var columnWidthSet = ReactPropTypes.shape({
  xs: ReactPropTypes.number,
  sm: ReactPropTypes.number,
  md: ReactPropTypes.number,
  lg: ReactPropTypes.number
});

export var offsetSizeSet = ReactPropTypes.shape({
  xsOffset: ReactPropTypes.number,
  smOffset: ReactPropTypes.number,
  mdOffset: ReactPropTypes.number,
  lgOffset: ReactPropTypes.number
});

const quantityShape = {
  amount: ReactPropTypes.number.isRequired, // numerical
  measure: ReactPropTypes.number, // id
  unit: ReactPropTypes.number, // id
};

export const quantity = ImmutablePropTypes.contains(quantityShape);
export const quantityField = deepReduxFormField(quantityShape);

export type SelectOptionItem = {
  value: string,
  label: string,
};
export type SelectOptions = Array<SelectOptionItem>;

export var billOfMaterials = ImmutablePropTypes.mapOf(quantity);
export var shoppingListEntry = ImmutablePropTypes.contains({
  product: ReactPropTypes.number.isRequired, // id
  name: ReactPropTypes.string.isRequired,
  totalRequired: quantity.isRequired, // id
  totalPurchased: quantity.isRequired, // id
  source: productSource
});

export var productionOrder = ImmutablePropTypes.contains({
  totalCost: ReactPropTypes.number.isRequired,
  shoppingList: ImmutablePropTypes.listOf(shoppingListEntry),
  recipeList: ImmutablePropTypes.listOf(ImmutablePropTypes.contains({
    product: ReactPropTypes.number.isRequired, // id
    name: ReactPropTypes.string.isRequired,
    totalRequired: quantity.isRequired,
    totalProduced: quantity.isRequired,
    // keys are strings, unfortunately - for JSON reasons
    billOfMaterials: billOfMaterials
  })),
});

export type LocationList = $FlowTODO;

export type AccountListInfo = {
  id: number,
  email: string,
  firstName: string,
  lastName: string,
  businessName: ?string,
  isAdmin: boolean,
  isStdlib: boolean,
  acceptsTerms: boolean,
  promoCode: ?string,
  createdAt: string, // ISO-8601 date
  lastLogin: string, // ISO-8601 date
  isMultiLocationDownstream: boolean,
  subscriptionLevel: ?string,
  phoneNumber: ?string,
  company: ?string,
}

export type UserListInfo = {
  id: number,
  email: string,
  firstName: string,
  lastName: string,
  fullName: ?string,
  phoneNumber: ?string,
  userAccounts: Array<UserAccountInfo>,
}

export type UserAccountInfo = {
  owner: number,
  userId: number,
  permissionLevel: PermissionLevel,
  joinedAt: string,
}

export type GraphData = {
  labels: Array<string>,
  values: Array<number>
}

export type PurchaseHistoryPayload = {
  period: string,
  purchaseType: string,
  purchaseValue?: string,
}