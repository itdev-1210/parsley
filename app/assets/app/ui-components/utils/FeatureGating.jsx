// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import type { ChildrenArray, ComponentType } from 'react';
import type { Map } from 'immutable';

import { connect } from 'react-redux';
import classnames from 'classnames';
import { Tooltip } from 'react-bootstrap';

import FloatingOverlay from './FloatingOverlay';

import {
  featureIsAvailable,
  upgradeRequiredText,
  FEATURES, planPreferenceOrder,
} from 'common/utils/features';
import type { FeatureName, UpsellVerbiage } from 'common/utils/features';

import { featureIsSupported } from '../../utils/features';

type FeatureOverlayClassNameProps = string;
type FeatureOverlayStyleProps = { [string]: string };
type OverlayProps = {|
  feature: FeatureName,
  children: ChildrenArray<*>,
  upsellVerbiage?: UpsellVerbiage,
  wrapperClassName?: FeatureOverlayClassNameProps,
  wrapperStyle?: FeatureOverlayStyleProps,
|}

const genericFeatureDescription = {
  description: "this feature",
  infinitive: "use this feature",
};

@connect(
    state => ({
      userInfo: state.get('userInfo'),
    }),
)
export class FeatureUpsellOverlay extends Component<OverlayProps> {
  render() {
    const { feature, upsellVerbiage, wrapperClassName, wrapperStyle } = this.props;
    // $FlowFixMe: flow doesn't understand @connect
    const { userInfo } = this.props;

    const featureDesc = FEATURES[feature];
    if (!featureDesc.enabledSubscriptionLevels) {
      // should not ever hit this case; if this is null, and the feature is supported
      // in this environment, then there has to be a plan-based restriction
      // but the typechecker says this is good bulletproofing
      return null;
    } else if (featureDesc.enabledSubscriptionLevels.length === 0) {
      // feature is in beta - hide it
      return null;
    }

    const eligiblePlans = featureDesc.enabledSubscriptionLevels;
    const upgradePlans = _.dropWhile(
        planPreferenceOrder, p => p !== userInfo.get('subscriptionLevel'),
    );
    const suggestedPlan = upgradePlans.find(p => eligiblePlans.includes(p)) || eligiblePlans[0];

    const {
      description, infinitive,
    } = upsellVerbiage || featureDesc.upsellVerbiage || genericFeatureDescription;
    const userIsAccountOwner = !userInfo.get('isReadOnly') && !userInfo.get('isSharedUser');
    const callToAction = userIsAccountOwner
        ? [<a key="link" href="/account#payment" target="_blank">click here</a>, ' and']
        : 'ask your account administrator (the primary owner of the account) to';

    // TODO
    const tooltip = <Tooltip id={`feature-upsell-tooltip-${feature}`} className="feature-upsell-tooltip">
      <p>
        Your current subscription does not include {description}.
        To {infinitive}, please {callToAction} {upgradeRequiredText(suggestedPlan)}.
      </p>
    </Tooltip>;
    return <FloatingOverlay
        placement="top"
        overlay={tooltip}
        // wait long enough to make sure the user isn't just hopping the mouse
        // from button to tooltip
        delayHide={50}
        >
      <div className={classnames("feature-upsell-overlay", wrapperClassName)} style={wrapperStyle}>
        {this.props.children}
      </div>
    </FloatingOverlay>;
  }
}

/**
 * decorator - passed component must respect a 'disabled' prop
 * decorated component will now take two additional (optional) props
 *
 * @param feature: feature to gate on. if not given, component is enabled (allows optional gating)
 * @param description an infinitive verb phrase; e.g. "use your own logo on printed pages".
 *  if not given, uses the field from the feature description object
 */
export function featureGuarded(WrappedComponent: ComponentType<{ disabled?: boolean }>) {
  const reduxDecorate = connect(state => ({
    // just want to force re-render on user info arrival
    userInfo: state.get('userInfo'),
  }));

  type consumedProps = {
    feature?: FeatureName, upsellVerbiage?: UpsellVerbiage,
    wrapperClassName?: FeatureOverlayClassNameProps, wrapperStyle?: FeatureOverlayStyleProps,
    userInfo: Map<*, *>,
  }

  const Wrapped = (props: consumedProps) => {
    const { feature, upsellVerbiage, wrapperClassName, wrapperStyle, ...passthroughProps } = props;
    delete passthroughProps.userInfo; // was only here to force re-render

    if (!feature || featureIsSupported(feature)) {
      return <WrappedComponent {...passthroughProps}/>;
    } else if (featureIsAvailable(feature)) {
      // disabled prop on WrappedComponent overrides original value
      return <FeatureUpsellOverlay feature={feature} upsellVerbiage={upsellVerbiage}
                  wrapperClassName={wrapperClassName} wrapperStyle={wrapperStyle}>
        <WrappedComponent {...passthroughProps} disabled/>
      </FeatureUpsellOverlay>;
    } else { // feature is not even visible, should be hidden
      return null;
    }
  };

  return reduxDecorate(Wrapped);
}
