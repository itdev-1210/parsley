import React, { Component } from 'react';
import ReactPropTypes from 'prop-types';
import Immutable from 'immutable';
import moment from 'moment';
import { Table, FormControl, Row, Col } from 'react-bootstrap';
import Validate from 'common/ui-components/Validate';
import CurrencyInput from './CurrencyInput';
import CurrencyDisplay from './CurrencyDisplay';
import SearchBox from './SearchBox';
import * as PropTypes from './utils/PropTypes';
import ImmutablePropTypes from 'react-immutable-proptypes';
import _ from 'lodash';
import { convertPerQuantityValue, unitDisplayName } from '../utils/unit-conversions';
import { indexTypes } from '../reducers/indexesCache';

import AddButton from './AddButton';
import Select from './Select';
import { shallowToObject } from '../utils/form-utils';
import { dateLongFormat } from 'common/utils/constants';
import {
  costingSize,
  newSourceTemplate,
  pickAllComparisonUnits,
} from '../utils/packaging';
import {
  PackageSizeInput, RootUnitInput,
  SubPackageSizeInput, PricePerInput,
} from './SourceFormComponents';
import { getSearchObject, ingredientListSearchOpts } from '../utils/search';

function SourceRowSelectIngredient(props) {

  const {
    product, ingredientOptions,
    ingredientsSearchObject,
    id,
    uiStateActions, showingNewIngredientModal, openEditIngredientModal,
    compact, autoOpen, disabled, inputRef,
  } = props;

  return (
    <Validate compact={compact} {...product} touched={showingNewIngredientModal ? false : product.touched}>
      <Select
          creatable
          disabled={disabled}
          {...product}
          onChange={props.onChange}
          name={'ingredient-'.concat(id.value)}
          createNew={(item) => {
            item.setProductId = product.onBlur;
            uiStateActions.openNewIngredientModal(item);
          }}
          promptTextCreator={label => "Add New Ingredient"}
          options={ingredientOptions}
          additionalLabel={productId =>
            <span onClick={() => openEditIngredientModal(productId)} className="select-addtional-right-aligned-clickable">edit</span>
          }
          autoFocus={autoOpen}
          openMenuOnFocus={autoOpen}
          searchObject={ingredientsSearchObject}
          grabFocus={id.value < 0}
          inputRef={inputRef}
      />
    </Validate>
  );
}

function SourceSecondRow(props) {
  const { supplierIngredientName, sku, manufacturerName, manufacturerNumber, supplierName, date, importingReceivables } = props;

  return <tr className="supplied-ingredient-second-row">
    <td>
      <div className="supplied-ingredient-second-row-name">
        <FormControl type="text" placeholder="Supplier's Ingredient Name (optional)" {...supplierIngredientName}/>
      </div>
    </td>
    <td colSpan="8">
      <Row>
        <Col xs={3}>
          <div className="supplied-ingredient-second-row-sku">
            <FormControl type="text" placeholder="SKU (Optional)" {...sku}/>
          </div>
        </Col>
        <Col xs={2}/>
        <Col xs={3}>
          <div className="supplied-ingredient-second-row-name">
          {
            importingReceivables && supplierName ?
              <FormControl type="text" disabled placeholder="Supplier Name" {...supplierName}/>
            : <FormControl type="text" placeholder="Manufacturer Name (Optional)" {...manufacturerName}/>
          }
          </div>
        </Col>
        <Col xs={3}>
          {
            importingReceivables && date && date.value ?
              <FormControl type="text" disabled placeholder="Date" value={moment(date.value).format(dateLongFormat)}/>
            : <FormControl type="text" placeholder="Mfg SKU (Optional)" {...manufacturerNumber}/>
          }
        </Col>
      </Row>
    </td>
  </tr>;

}


export class SourceRow extends Component {

  static propTypes = {
    allMeasures: ImmutablePropTypes.mapOf(PropTypes.measure).isRequired,
    handleDelete: ReactPropTypes.func.isRequired,
    conversions: ImmutablePropTypes.listOf(PropTypes.conversion).isRequired,
    source: PropTypes.productSourceFormField.isRequired,
    untouch: ReactPropTypes.func.isRequired,
  };

  static defaultProps = {
    compact: true,
  };

  shouldComponentUpdate(nextProps) {
    if (!this.props.conversions && nextProps.conversions) return true;

    if (!this.props.comparisonUnit && nextProps.comparisonUnit) return true;
    if (this.props.comparisonUnit && nextProps.comparisonUnit &&
        this.props.comparisonUnit.get('id') !== nextProps.comparisonUnit.get('id')) return true;

    const fields = _.keys(this.props.source);
    const requiredFields = ['cost', 'measure', 'product', 'unit'];
    return _.some(fields, f =>
      this.props.source[f].value !== nextProps.source[f].value ||
      (requiredFields.indexOf(f) !== -1 && !nextProps.source[f].value) // Need this for required tooltips
    );
  }

  render() {

    const {
      ingredientOptions,
      ingredientsSearchObject,
      allMeasures,
      conversions,
      uiStateActions,
      handleDelete,
      comparisonUnit,
      showingNewIngredientModal,
      source: {
        cost,
        id,
        measure,
        packaged,
        packageSize,
        superPackageSize,
        product,
        sku,
        unit,
        pricePer,
        supplierIngredientName,
        syncUpstreamOwner,
        manufacturerName,
        manufacturerNumber,
        date,
        supplierName,
      },
      userWeightPref,
      userVolumePref,
      openEditIngredientModal,
      fromImport,
      compact,
      autoOpen,
      isMultiLocationDownstream,
      inputRef,
      importingReceivables,
    } = this.props;

    const productChanged = newProd => {
      if (newProd !== product.value) {
        measure.onChange(undefined);
        unit.onChange(undefined);
        product.onChange(newProd);
      }
    };

    const disabled = isMultiLocationDownstream && syncUpstreamOwner.value;

    if (!conversions) {
      return <tbody className="supplied-ingredient-body-table">
        <tr className="supplied-ingredient-row">
          <td>
            <SourceRowSelectIngredient
              id={id}
              product={product}
              ingredientOptions={ingredientOptions}
              ingredientsSearchObject={ingredientsSearchObject}
              onChange={productChanged}
              showingNewIngredientModal={showingNewIngredientModal}
              uiStateActions={uiStateActions}
              openEditIngredientModal={openEditIngredientModal}
              compact={compact}
              autoOpen={autoOpen}
              disabled={disabled}
              inputRef={inputRef}
            />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px' }} />
            <hr className="well-child-hr" />
          </td>
          <td>
            <div style={{ height: '27px', width: '89px' }}>
            {
              !fromImport ?
                <span onClick={handleDelete} className="pull-right list-item-delete-link">
                  {disabled ? '' : 'delete'}
                </span>
              : null
            }
            </div>
            <hr className="well-child-hr" />
          </td>
        </tr>
        <SourceSecondRow
          supplierIngredientName={supplierIngredientName}
          sku={sku}
          manufacturerName={manufacturerName}
          manufacturerNumber={manufacturerNumber}
          importingReceivables={importingReceivables}
          supplierName={supplierName}
          date={date}
        />
      </tbody>;
    }


    const compareTo = comparisonUnit && comparisonUnit.get('id');
    let costPerComparisonUnit = false;
    if (cost.value && packageSize.value && superPackageSize.value && typeof unit.value == 'number' && compareTo) {
      const pricingQuantity = costingSize(Immutable.fromJS(shallowToObject(this.props.source)));
      if (pricingQuantity) {
        costPerComparisonUnit =
          convertPerQuantityValue(cost.value,
              pricingQuantity.amount, pricingQuantity.unit,
              1, comparisonUnit.get('id'),
              conversions,
          );
      }
    }

    const canAddNewUnit = conversions.size < allMeasures.size;

    const showPackagingUnit = typeof unit.value === 'number' &&
      (packaged.value || (measure.value === 1 && conversions.size > 1));

    return <tbody className="supplied-ingredient-body-table">
      <tr className="supplied-ingredient-row">
        <td>
          <SourceRowSelectIngredient
              id={id}
              product={product}
              ingredientOptions={ingredientOptions}
              ingredientsSearchObject={ingredientsSearchObject}
              onChange={productChanged}
              uiStateActions={uiStateActions}
              openEditIngredientModal={openEditIngredientModal}
              compact={compact}
              autoOpen={autoOpen}
              disabled={disabled}
              inputRef={inputRef}
          />
          <hr className="well-child-hr" />
        </td>
        <td style={{ maxWidth: '94px', minWidth: '94px' }}>
          <RootUnitInput
            conversions={conversions} measures={allMeasures}
            fields={this.props.source}
            untouch={this.props.untouch}
            productId={product.value}
            canAddNewUnit={canAddNewUnit}
            openNewUnitModal={uiStateActions.openNewUnitModal}
            compact={compact}
            disabled={disabled}
          />
          <hr className="well-child-hr" />
        </td>
        <td style={{ maxWidth: '156px', minWidth: '156px'}}>
          <div style={{ height: '27px' }}>
            <PackageSizeInput handleQuantityChange={this.handleQuantityChange}
              conversions={conversions} measures={allMeasures}
              fields={this.props.source}
              untouch={this.props.untouch}
              productId={product.value}
              canAddNewUnit={canAddNewUnit}
              openNewUnitModal={uiStateActions.openNewUnitModal}
              compact={compact}
              disabled={disabled}
            />
          </div>
          <hr className="well-child-hr" />
        </td>
        <td style={{ paddingLeft: '0px' }} className="sub-package-size-input">
          <div style={{ height: '27px' }}>
            <SubPackageSizeInput
              productId={product.value}
              canAddNewUnit={canAddNewUnit}
              openNewUnitModal={uiStateActions.openNewUnitModal}
              conversions={conversions} measures={allMeasures}
              fields={this.props.source}
              compact={compact}
              disabled={disabled}
            />
          </div>
          <hr className="well-child-hr" />
        </td>
        <td className="cost-input"style={{ paddingRight: '2px' }}>
          <CurrencyInput compact={compact} placeholder="Cost" {...cost}/>
          <hr className="well-child-hr" />
        </td>
        <td className="price-per-input">
          <div style={{ height: '27px' }}>
          {
           showPackagingUnit ?
           <PricePerInput
            conversions={conversions}
            measures={allMeasures}
            fields={this.props.source}
            userWeightPref={userWeightPref}
            userVolumePref={userVolumePref}
            compact={compact}
           />
           : null
          }
          </div>
          <hr className="well-child-hr" />
        </td>
        <td colSpan="2" className="cost-per-pound">
         <div style={{ height: '26px' }}>
          { Boolean(costPerComparisonUnit) || costPerComparisonUnit === 0 ?
              <div>
                <CurrencyDisplay value={costPerComparisonUnit}/>
                /
                {Boolean(comparisonUnit) && comparisonUnit.get('id') ?
                  unitDisplayName(comparisonUnit.get('id'), conversions)
                  : null}
              </div>
              : null
          }
         </div>
         <hr className="well-child-hr" />
        </td>
        <td>
          <div style={{ height: '27px', width: '89px' }}>
          {
            !fromImport ?
              <span onClick={handleDelete} className="pull-right list-item-delete-link">
                {disabled ? '' : 'delete'}
              </span>
            : null
          }
          </div>
          <hr className="well-child-hr" />
        </td>
      </tr>
      <SourceSecondRow
        supplierIngredientName={supplierIngredientName}
        sku={sku}
        manufacturerName={manufacturerName}
        manufacturerNumber={manufacturerNumber}
        importingReceivables={importingReceivables}
        supplierName={supplierName}
        date={date}
      />
    </tbody>;
  }
}

export default class SuppliedIngredientsDisplay extends Component {

  static propTypes = {
    supplierId: ReactPropTypes.number.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    ['handleAdd',
    ].forEach(func => {
      this[func] = this[func].bind(this);
    });

    this.state = {
      searchTerm: '',
    };
  }

  handleAdd() {
    const {
      fields: sources,
      uiStateActions,
    } = this.props;

    let id = -1;
    if (sources.length > 0) {
      id = Math.min(_.minBy(sources, source => source.id.value).id.value - 1, id);
    }

    sources.addField({
      ...newSourceTemplate,
      id,
      supplier: this.props.supplierId,
    });
  }

  handleDelete(id) {
    const { fields: sources, reportDeleteSource, deleteSourceFn } = this.props;
    const idx = sources.findIndex(source => source.id.value === id);
    if (deleteSourceFn)
      deleteSourceFn(id);
    else
      sources.removeField(idx);
    reportDeleteSource();
  }

  render() {
    const {
      fields: sources, allMeasures, allIngredients, detailedIngredients, untouch, uiStateActions,
      showingNewIngredientModal, showingImportIngredientsModal, userVolumePref, userWeightPref,
      isMultiLocationDownstream, isReadOnly, openEditIngredientModal,
    } = this.props;
    const { searchTerm } = this.state;

    if (!sources || !allMeasures || !allIngredients) return <div/>;

    const ingredientOptions = allIngredients
        .filter(s => !s.get('tombstone') && !s.get('isSubRecipe'))
        .map(s =>
            ({ value: s.get('id'), label: s.get('name'), className: null }),
        ).toJS();

    const productsComparisonUnit = pickAllComparisonUnits(
        sources.map(shallowToObject),
        detailedIngredients.map(deets => deets.get('measures')),
        allMeasures, userWeightPref, userVolumePref, true,
    );

    // share the same search object among <Select>s to save some memory
    const ingredientsSearchObject = getSearchObject(ingredientOptions, ['label'], ingredientListSearchOpts, 'value', indexTypes.PURCHASE_PRODUCTS);

    let filteredSources;
    if (searchTerm) {
      const filteredIngredients = ingredientsSearchObject.searchWithQueryFn(searchTerm);
      filteredSources = filteredIngredients
      .filter(fi => sources.find(s => s.product.value === fi.value))
      .map(fi => sources.find(s => s.product.value === fi.value));
    } else {
      filteredSources = sources;
    }

    return (
      <div>
        <br/>
        <Row>
          <Col md={3} sm={6} xs={6}>
            <SearchBox
              value={searchTerm}
              onChange={(term) => this.setState({ searchTerm: term })}
            />
          </Col>
        </Row>
        <br/>
        <Table style={{ marginBottom: '0px', borderCollapse: 'collapse' }}>
          <thead>
          <tr>
            <th style={{ textAlign: 'center', maxWidth: '380px', minWidth: '300px' }}>Ingredient/ Supplier's Ingredient Name</th>
            <th colSpan="2" style={{ textAlign: 'center', minWidth: '250px' }}>Unit of Purchase/ SKU</th>
            <th style={{ width: '100px' }}/>
            <th colSpan="2" style={{ textAlign: 'center', width: '250px' }}>Cost</th>
            <th style={{ width: '100px' }}/>
            <th style={{ textAlign: 'center', width: '125px' }} />
            <th style={{ width: '85px' }}/>
          </tr>
          </thead>
          {filteredSources.map(source =>
            <SourceRow
              key={source.id.value}
              isMultiLocationDownstream={isMultiLocationDownstream}
              allMeasures={allMeasures}
              ingredientOptions={ingredientOptions}
              ingredientsSearchObject={ingredientsSearchObject}
              untouch={untouch}
              uiStateActions={uiStateActions}
              handleDelete={this.handleDelete.bind(this, source.id.value)}
              comparisonUnit={productsComparisonUnit[source.product.value]}
              conversions={detailedIngredients.getIn([source.product.value, 'measures'])}
              source={source}
              showingNewIngredientModal={showingNewIngredientModal}
              userWeightPref={userWeightPref}
              userVolumePref={userVolumePref}
              openEditIngredientModal={openEditIngredientModal}
            />,
          )}
        </Table>
        {
          !isReadOnly ?
          <AddButton onClick={this.handleAdd}/>
          : null
        }
      </div>
    );
  }
}
