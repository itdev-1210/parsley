// @flow
import React, { Component } from 'react';

import { reduxForm } from 'redux-form';
import { Button } from 'react-bootstrap';

import { getOnboardingInfo, setStepVisited } from '../webapi/endpoints';
import marked from 'common/utils/marked';

type Props = {
  pageKey: string,
  stepIndex: number,
  title: string,
  description: string,
  titleBgColor: string,
  descBgColor: string,
  borderColor: string, // TODO: actually use this
  handleCallback: () => void,

  nextButtonText: ?string,
  backButtonText: ?string,
  bodyWidth: number,
  nextBgColor: ?string,
  closeBtnColor: string,

  leftImage?: string,
  rightImage?: string,
  leftImageURLFromEditor?: string,
  rightImageURLFromEditor?: string,
}

@reduxForm(
  {
    form: 'onboarding_tooltip',
    fields: [],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    s3Bucket: state.getIn(['userInfo', 'logoUrl']),
  }),
)
export default class OnboardingTooltip extends Component<Props> {


  goNext = (values: *, dispatch: *, props: Props) => setStepVisited(props.pageKey, props.stepIndex)
      .then(() => getOnboardingInfo());

  goBack = (values: *, dispatch: *, props: Props) => setStepVisited(props.pageKey, props.stepIndex - 2)
      .then(() => getOnboardingInfo());

  render() {
    const {
      stepIndex,
      title, description,
      titleBgColor, descBgColor, nextBgColor,
      closeBtnColor,
      leftImage, rightImage,
      nextButtonText, backButtonText,
      bodyWidth,

      // $FlowFixMe: no types yet for react-redux-provided props
      handleSubmit, submitting, s3Bucket,
      leftImageURLFromEditor, rightImageURLFromEditor,
    } = this.props;

    const onboardingTitleClassNames = ['onboarding-title'];

    if (leftImage) {
      onboardingTitleClassNames.push('onboarding-title-left-logo');
    }
    if (rightImage) {
      onboardingTitleClassNames.push('onboarding-title-right-logo');
    }

    return <div className="onboarding-body" style={{ width: `${bodyWidth}px` }}>
      <Button
        bsStyle="link" className="onboarding-close"
        style={{ color: `#${closeBtnColor}` }}
        onClick={handleSubmit(this.goNext.bind(this))}
        >
        X
      </Button>
      <div
        className={onboardingTitleClassNames.join(' ')}
        style={{
          backgroundColor: `#${titleBgColor}`,
        }}>
        {title}
        {
          leftImageURLFromEditor
            ? <div className="onboarding-left-logo">
              <img id="left-logo" src={leftImageURLFromEditor} />
            </div>
            : leftImage
                ? <div className="onboarding-left-logo">
                  <img id="left-logo" src={leftImage.match(/^\/assets\/images\//) ? leftImage : s3Bucket + leftImage} />
                </div>
                : null
        }
        {
          rightImageURLFromEditor
            ? <div className="onboarding-right-logo">
              <img id="right-logo" src={rightImageURLFromEditor} />
            </div>
            : rightImage
                ? <div className="onboarding-right-logo">
                  <img id="right-logo" src={rightImage.match(/^\/assets\/images\//) ? rightImage : s3Bucket + rightImage} />
                </div>
                : null
        }
      </div>
      <div
        className="onboarding-content"
        style={{
          backgroundColor: `#${descBgColor}`,
          minHeight: '100px',
        }}
        dangerouslySetInnerHTML={{ __html: marked(description) }}
      />
      <div
        className="onboarding-footer"
        style={{
          backgroundColor: `#${descBgColor}`,
        }}>
        {
          backButtonText && stepIndex > 0 && <Button
            bsStyle="link"
            onClick={handleSubmit(this.goBack.bind(this))}>
            {backButtonText}
          </Button>
        }
        {
          nextButtonText && <Button
            onClick={handleSubmit(this.goNext.bind(this))}
            disabled={submitting}
            style={{
              backgroundColor: nextBgColor && `#${nextBgColor}`,
              borderColor: nextBgColor && `#${nextBgColor}`,
            }}>
            {nextButtonText}
          </Button>
        }
      </div>
    </div>;
  }
}