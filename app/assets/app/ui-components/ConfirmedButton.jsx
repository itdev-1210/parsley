// @flow
import React, { Component } from 'react';
import type { Node } from 'react';

import {
  Button, Modal
} from 'react-bootstrap';

type Props = {
  modalTitle: string,
  modalText: Node,
  modalButtonText: string,
  confirmButtonStyle: "success" | "warning" | "danger" | "info" | "default" | "primary" | "link",
  children: Node,
  onClick: () => Promise<mixed>,
  inProgress: ?boolean,
};

type State = {
  showingConfirmation: boolean,
  inProgress: boolean,
}

export default class ConfirmedButton extends Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      showingConfirmation: false,
      inProgress: false,
    };
  }

  render() {
    const {
        children,
        modalButtonText, modalText, modalTitle,
        confirmButtonStyle,
        onClick,...passthrough
    } = this.props;
    let modal = null;

    let hideModal = () => this.setState({showingConfirmation: false, inProgress: false});

    if (this.state.showingConfirmation) {
      modal = <Modal show onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{modalText}</Modal.Body>
        <Modal.Footer>
          <Button bsStyle="warning" onClick={hideModal}>
            cancel
          </Button>
          <Button bsStyle={confirmButtonStyle} disabled={this.state.inProgress}
                  onClick={() => {
                    this.setState({inProgress: true});
                    onClick().then(
                        hideModal,
                        error => this.setState({inProgress: false}));
                  }}>
            {modalButtonText}
          </Button>
        </Modal.Footer>

      </Modal>
    }

    return <Button onClick={() => this.setState({showingConfirmation: true})} {...passthrough}>
      {modal}
      {children}
    </Button>
  }
}