import React, { Component } from 'react';
import { Alert as BaseAlert } from 'react-bootstrap';
import classNames from 'classnames';

// Custom alert component
// See PARS-2129 for more info
export default class Alert extends Component {
  render() {
    const { className, children, ...passthroughProps } = this.props;
    const cautionMark = serverRoutes.controllers.Assets.versioned('images/parsley_caution.png').url;
    return <BaseAlert className={classNames('custom-alert', className)} {...passthroughProps}>
      <div><img src={cautionMark} style={{ padding: '0px 5px' }}/></div>
      <div className="custom-alert-children-wrapper">
        {children}
      </div>
    </BaseAlert>;
  }
}
