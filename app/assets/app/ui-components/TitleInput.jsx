import React, { Component } from 'react';
import { FormControl, FormGroup } from 'react-bootstrap';
import ReactPropTypes from 'prop-types';

import Validate from 'common/ui-components/Validate';

export default class TitleInput extends Component {
  static propTypes = {
    noun: ReactPropTypes.string.isRequired,
  };

  render() {
    let { noun, ...rest } = this.props;

    return <FormGroup controlId={rest.name}>
      <Validate className="title-input" {...rest}>
        <FormControl
            type="text"
            bsSize="large"
            placeholder={noun + ' Name'}
            {...rest}
        />
      </Validate>
    </FormGroup>;
  }
}
