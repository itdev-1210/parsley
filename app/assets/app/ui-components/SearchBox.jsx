import React from "react";

import { FormGroup, InputGroup, FormControl, Glyphicon } from "react-bootstrap";

var SearchBox = props =>
  <FormGroup controlId="search" id={props.id} className="search-box">
    <InputGroup>
      <FormControl type="text" placeholder="Search"
                   {...props}
                   onChange={ev => props.onChange(ev.target.value)}
      />
      <InputGroup.Addon><Glyphicon glyph="search"/></InputGroup.Addon>
    </InputGroup>
  </FormGroup>;

export default SearchBox;
