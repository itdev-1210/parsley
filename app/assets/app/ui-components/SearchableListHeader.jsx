import React, { Component } from "react";

import ReactPropTypes from 'prop-types'
import { Col, Row, Glyphicon } from "react-bootstrap";

import SearchBox from "./SearchBox";

export default class SearchableListHeader extends Component {
  static propTypes = {
    searchKey: ReactPropTypes.string.isRequired,
    searchUpdated: ReactPropTypes.func, // callback to pass new query
    title: ReactPropTypes.string.isRequired,
    currentQuery: ReactPropTypes.string.isRequired,
  };

  render() {
    return (
      <Row>
        <Col className="title-block" md={3} sm={6} xs={6}>
          <h3 className="list-title">{this.props.title}</h3>
          <SearchBox
                  id={`search-list-search-box-${this.props.searchKey}`}
                  value={this.props.currentQuery}
                  onChange={this.props.searchUpdated}
                  inputRef={input => this.input = input}
          />
        </Col>
      </Row>);
  }

  componentDidMount() {
    this.input.focus();
  }
}
