import React, { Component } from 'react';
import _ from 'lodash';
import Papa from 'papaparse';
import { FormControl } from 'react-bootstrap';
import classnames from 'classnames';
import ReactPropTypes from 'prop-types';
import { sanitizePrice } from "../utils/general-algorithms";

export default class SuppliedCsvReader extends Component {
  static propTypes = {
    noun: ReactPropTypes.string,
    onClick: ReactPropTypes.func.isRequired,
  }

  static defaultProps = {
    noun: 'Upload',
  };

  constructor(props, context) {
    super(props, context);
    this.handleCsv = this.handleCsv.bind(this);
    this.getHeaderNames = this.getHeaderNames.bind(this);
  }

  getHeaderNames(fields) {
    let headerNames = {};
    let findHeaderName = name => _.find(fields, (n) => n.toLowerCase() === name);
    headerNames.name = findHeaderName('name');
    headerNames.pack = findHeaderName('pricing unit');
    headerNames.uom = findHeaderName('purchase package');
    headerNames.price = findHeaderName('price');
    headerNames.sku = findHeaderName('sku');
    return headerNames;
  }

  handleCsv(file) {
    const { onClick } = this.props;
    const getHeaderNames = this.getHeaderNames;
    Papa.parse(file, {
      header: true,
      trimHeaders: true,
      skipEmptyLines: 'greedy',
      complete(results) {
        const headerNames = getHeaderNames(results.meta.fields);
        if (headerNames.name && headerNames.price) {
          const data =
              results.data
                  .map(row => {
                    const newRow = _.mapKeys(row, (val, fieldName) => fieldName.replace(' ', '').toLowerCase())
                    newRow.sanitizedPrice = sanitizePrice(newRow.price);
                    return newRow;
                  });
          onClick(data);
        } else
          alert('The File does not contain Name and Price columns. Please edit and reselect');
      },
    });
  }

  render() {
    const { noun } = this.props;
    return <label className={classnames('green-btn', 'btn', 'btn-primary', 'upload-button')}
      htmlFor="fileUpload"
      style={{ cursor: 'pointer', paddingTop: '3px' }}>
      {noun}
      <FormControl
        id="fileUpload"
        type="file"
        accept=".csv"
        value=""
        onChange={(e) => this.handleCsv(e.target.files[0])}
      />
     </label>;
  }
}