import React, { Component } from 'react';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { FloatInputRoundToTwo, MaskedNumberInput } from './MaskedInput';
import { FormControl, ListGroup, ListGroupItem } from 'react-bootstrap';
import QuantityInput from './QuantityInput';
import UnitInput from './UnitInput';
import Checkbox from './Checkbox';
import _ from 'lodash';
import Immutable from 'immutable';
import {
  convert,
  roundToNearest,
  unitDisplayName,
  getMeasure,
} from '../utils/unit-conversions';
import { getNNDBSRIdToNutrientIdMap } from '../utils/form-utils.jsx';
import Validate from 'common/ui-components/Validate';
import { valueFromEvent } from '../utils/form-utils';
import TextWithHelp from './TextWithHelp';

export const nutrition_allergens = [
  { name: 'milk' },
  { name: 'eggs' },
  { name: 'fish', additionalInfo: true },
  { name: 'crustacean or shellfish', additionalInfo: true, fieldName: 'crustaceanShellfish' },
  { name: 'tree nuts', additionalInfo: true, fieldName: 'treeNuts' },
  { name: 'wheat' },
  { name: 'peanuts' },
  { name: 'soybeans' },
  { name: 'molluscs' },
  { name: 'cereals containing gluten', fieldName: 'cerealsGluten' },
  { name: 'celery', fieldName: 'celery' },
  { name: 'mustard', fieldName: 'mustard' },
  { name: 'sesame seeds', fieldName: 'sesameSeeds' },
  { name: 'sulphur dioxide and sulphites', fieldName: 'sulphurDioxideSulphites' },
  { name: 'lupin', fieldName: 'lupin' },
];

export const nutrition_characteristics = [
  { name: 'Added Sugar', fieldName: 'addedSugar' },
  { name: 'Contains Meat', fieldName: 'meat' },
  { name: 'Contains Pork', fieldName: 'pork' },
  { name: 'Contains Corn', fieldName: 'corn' },
  { name: 'Contains Poultry', fieldName: 'poultry' },
  { name: 'Contains Ingredients', fieldName: 'ingredients' },
  {
    name: 'Non-Edible', fieldName: 'nonEdible', isBold: true,
    helpText: 'If checked, this ingredient will not appear in the ingredients list or nutrition facts of recipes that use it',
  },
];

// nndbsrIds of the displayed nutrients
export const nutrient_fields = {
  calories: '208',
  totalFat: '204',
  saturatedFat: '606',
  transFat: '605',
  cholesterol: '601',
  sodium: '307',
  total_carbohydrate: '205',
  dietary_fiber: '291',
  total_sugars: '269',
  protein: '203',
  vitamin_d: '324',
  calcium: '301',
  iron: '303',
  potassium: '306',
};

export const nutrient_default_daily_values = {
  totalFat: 80,
  saturatedFat: 20,
  cholesterol: 300,
  sodium: 2500,
  total_carbohydrate: 300,
  dietary_fiber: 25,
  vitamin_d: 20,
  calcium: 1300,
  iron: 18,
  potassium: 4000,
};

export const nutrient_round_groups = {
  calories: 1,
  totalFat: 2,
  saturatedFat: 2,
  transFat: null,
  cholesterol: 3,
  sodium: 4,
  total_carbohydrate: 5,
  dietary_fiber: 5,
  total_sugars: 5,
  added_sugar: 5,
  protein: 5,
  vitamin_d: null,
  calcium: null,
  iron: null,
  potassium: 4,
};

// Vitamin D, Calcium, Iron, Potassium
export function roundBottomNutritionPercentages(value) {
  if (value < 2) {
    return 0;
  } else {
    let roundTo;
    if (value <= 10) {
      roundTo = 2;
    } else if (value <= 50) {
      roundTo = 5;
    } else {
      roundTo = 10;
    }
    return roundToNearest(value, roundTo);
  }
}

export function roundNutritionAmount(roundGroup, value) {
  if (!value) return 0;

  let label;
  switch (roundGroup) {

    // Calories
    case 1:
      label = roundToNearest(value, value <= 50 ? 5 : 10);
      break;

    // Total Fat, and Saturated Fat:
    case 2:
      if (value < 0.5) {
        label = 0;
      } else {
        label = roundToNearest(value, value < 5 ? 0.5 : 1);
      }
      break;

    // Cholesterol
    case 3:
      if (value < 2) {
        label = 0;
      } else if (value < 5) {
        label = 'less than 5';
      } else {
        label = roundToNearest(value, 5);
      }
      break;

    // Sodium and Potassium
    case 4:
      if (value < 5) {
        label = 0;
      } else {
        label = roundToNearest(value, value <= 140 ? 5 : 10);
      }
      break;

    // Total Carbohydrate, Dietary Fiber, Total Sugars, and Added Sugars, Protein
    case 5:
      if (value < 0.5) {
        label = 0;
      } else if (value < 1) {
        label = 'less than 1';
      } else {
        label = roundToNearest(value, 1);
      }
      break;

    // Iron & Calcium percentages
    case 6:
      label = roundBottomNutritionPercentages(value);
      break;

    default:
      label = parseFloat(value.toFixed(2));
      break;
  }
  return label;
}

class NutritionAmountInput extends Component {

  render() {

    const { productType, isAddedSugar, value, roundGroup, isReadOnly } = this.props;

    if (productType === 'ingredients' && !isAddedSugar) {
      return <FloatInputRoundToTwo {...this.props} disabled={isReadOnly} />;
    }

    return <span style={{ marginLeft: '5px' }}>{roundNutritionAmount(roundGroup, value)}</span>;
  }
}

class NutritionServingSize extends Component {
  render() {
    const {
      productType, isSubRecipeInIngredientEdit, isSubRecipeInRecipeEdit, printMedia,
      conversions, measures, recipeNutrients,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      isReadOnly,
    } = this.props;

    if (!measures) return <LoadingSpinner />;

    if (productType === 'ingredients' || isSubRecipeInRecipeEdit) {

      const validMeasures = Immutable.Map(
        conversions.map(c => {
          let measureId = c.get('measure');
          return [measureId, measures.get(measureId)];
        }),
      );

      return <div className="serving-size">
        <span style={{ fontSize: '15px' }}>Serving Size: </span>
        { printMedia
            ? <span style={{ fontSize: '15px' }}>{nutrientServingAmount.value}</span>
            : <Validate compact {...nutrientServingAmount}>
              <NutritionAmountInput productType="ingredients" {...nutrientServingAmount} isReadOnly={isReadOnly} />
            </Validate>
        }
        { printMedia && <span style={{ marginLeft: '5px', fontSize: '15px' }}>
          {unitDisplayName(nutrientServingUnit.value, conversions)}
        </span>}
        { !printMedia && <Validate {...nutrientServingUnit} className="nutrient-serving-unit">
          <UnitInput
            disabled={isReadOnly}
            measures={validMeasures}
            conversions={conversions}
            reduxFormFields={({
              measureField: {
                ...nutrientServingMeasure,
                onBlur() {},
                onChange(newMeasure) {
                  if (typeof newMeasure === typeof 0) {
                    nutrientServingMeasure.onChange(newMeasure);
                  }
                },
              },
              unitField: nutrientServingUnit,
            })}
          />
        </Validate>}
      </div>;
    } else if (isSubRecipeInIngredientEdit) {
      return <span style={{ fontSize: '15px' }}>
        Serving Size: {nutrientServingAmount.value} {
          // in certain circumstances this would be blank on initial load, so checking
          // for that case
          nutrientServingUnit.value && unitDisplayName(nutrientServingUnit.value)
        }
      </span>;
    } else if (productType === 'recipes') {
      const notEditable = printMedia;

      let servingSize;
      if (!recipeNutrients.packaged.value) {
        servingSize = <div style={{ display: 'flex' }}>
          <span style={{ fontSize: '15px' }}>Serving Size:</span>
          { notEditable ?
            <span style={{ fontSize: '15px' }}>{recipeNutrients.servingAmount.value}</span>
          : <FloatInputRoundToTwo {...recipeNutrients.servingAmount} disabled={isReadOnly} />}
          { recipeNutrients.portionUnit.value && printMedia &&
            <span style={{ marginLeft: '5px', fontSize: '15px' }}>
              {unitDisplayName(recipeNutrients.servingUnit.value, conversions)}
            </span>
          }
          { recipeNutrients.portionUnit.value && !printMedia &&
            <Validate {...recipeNutrients.servingUnit} className="nutrient-serving-unit">
              <UnitInput
                disabled={isReadOnly}
                measures={measures.filter(m => m.get('id') === recipeNutrients.portionMeasure.value)}
                conversions={conversions}
                reduxFormFields={({
                  measureField: {
                    ...recipeNutrients.servingMeasure,
                    onBlur() {},
                    onChange(newMeasure) {
                      if (typeof newMeasure === typeof 0) {
                        recipeNutrients.servingMeasure.onChange(newMeasure);
                      }
                    },
                  },
                  unitField: recipeNutrients.servingUnit,
                })}
              />
            </Validate>
          }
          { !recipeNutrients.portionUnit.value && <span style={{ marginLeft: '5px', fontSize: notEditable ? '15px' : 'inherit' }}>Portion</span>}
        </div>;
      } else {
        // all weight units are converted into gr (unitSize = 1)
        // all volume units are converted into ml (unitSize = 1)
        const units = measures.getIn([recipeNutrients.servingMeasure.value, 'units']);
        let amountDelta = 1;
        if (units) {
          amountDelta = units.find(u => u.get('id') === recipeNutrients.servingUnit.value).get('size');
        }
        let unit;
        if (recipeNutrients.servingMeasure.value === 2) {
          unit = 2;
        } else if (recipeNutrients.servingMeasure.value === 3) {
          unit = 9;
        }
        const unitString = recipeNutrients.servingUnit.value ? unitDisplayName(unit) : '???';
        servingSize = <div style={{ fontSize: '15px', marginBottom: '6px' }}>
          <div>{recipeNutrients.servingsPerPackage.value} servings per container</div>
          <div>
            Serving Size {recipeNutrients.servingName.value} ({Math.round(recipeNutrients.servingAmount.value * amountDelta) } {unitString})
          </div>
        </div>;
      }

      return <div className="serving-size">
        {servingSize}
      </div>;
    }
  }
}

// Nutrition Facts for both recipe and ingredient
class NutritionFacts extends Component {

  constructor(props) {
    super(props);
    this.calciumPercentageChanged = this.calciumPercentageChanged.bind(this);
    this.ironPercentageChanged = this.ironPercentageChanged.bind(this);
  }

  calciumPercentageChanged(value) {
    const { nutritions, nutritionsDailyValues } = this.props;
    nutritions.calcium.amount.onChange(value * nutritionsDailyValues.calcium / 100);
  }

  ironPercentageChanged(value) {
    const { nutritions, nutritionsDailyValues } = this.props;
    nutritions.iron.amount.onChange(value * nutritionsDailyValues.iron / 100);
  }

  render() {

    // RF-MIGRATION: 'nutritions' will be object of field names, not of fields
    const {
      productType, isSubRecipeInRecipeEdit, isSubRecipeInIngredientEdit, printMedia,
      conversions, measures, recipeNutrients,
      nutrientServingAmount, nutrientServingUnit, nutrientServingMeasure,
      nutritions, nutritionsDailyValues,
      isReadOnly,
    } = this.props;

    return <div id="nutrition-facts" className="nutrition-info-panel">
            <h1>Nutrition Facts</h1>
            <hr className="thin" />
            <NutritionServingSize
              isReadOnly={isReadOnly}
              productType={productType}
              isSubRecipeInIngredientEdit={isSubRecipeInIngredientEdit}
              isSubRecipeInRecipeEdit={isSubRecipeInRecipeEdit}
              conversions={conversions}
              measures={measures}
              recipeNutrients={recipeNutrients}
              nutrientServingAmount={nutrientServingAmount}
              nutrientServingMeasure={nutrientServingMeasure}
              nutrientServingUnit={nutrientServingUnit}
              printMedia={printMedia}
            />
            <hr className="thick" />
            <div className="calories">
              <div>Amount per serving</div>
              <div className="calorie-label">Calories</div>
              <div className="calorie-count">
                <NutritionAmountInput productType={productType} roundGroup={1} {...nutritions.calories.amount} isReadOnly={isReadOnly} />
              </div>
            </div>
            <hr className="medium" />
            <div className="daily-value">% Daily Value*</div>
            <hr className="thin" />
            <div className="macronutrients">
              <div className="fact-label">
                <b>Total Fat</b>
                <NutritionAmountInput productType={productType} roundGroup={2} {...nutritions.totalFat.amount} isReadOnly={isReadOnly} />g
                <div className="fact-percent">{Math.round(nutritions.totalFat.amount.value / nutritionsDailyValues.totalFat * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label indent">
                Saturated Fat
                <NutritionAmountInput productType={productType} roundGroup={2} {...nutritions.saturatedFat.amount} isReadOnly={isReadOnly} />g
                <div className="fact-percent">{Math.round(nutritions.saturatedFat.amount.value / nutritionsDailyValues.saturatedFat * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label indent">
                <i>Trans</i> Fat
                <NutritionAmountInput productType={productType} {...nutritions.transFat.amount} isReadOnly={isReadOnly} />g
              </div>
              <hr className="thin" />
              <div className="fact-label">
                <b>Cholesterol</b>
                <NutritionAmountInput productType={productType} roundGroup={3} {...nutritions.cholesterol.amount} isReadOnly={isReadOnly} />mg
                <div className="fact-percent">{Math.round(nutritions.cholesterol.amount.value / nutritionsDailyValues.cholesterol * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                <b>Sodium</b>
                <NutritionAmountInput productType={productType} roundGroup={4} {...nutritions.sodium.amount} isReadOnly={isReadOnly} />mg
                <div className="fact-percent">{Math.round(nutritions.sodium.amount.value / nutritionsDailyValues.sodium * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                <b>Total Carbohydrate</b>
                <NutritionAmountInput productType={productType} roundGroup={5} {...nutritions.total_carbohydrate.amount} isReadOnly={isReadOnly} />g
                <div className="fact-percent">{Math.round(nutritions.total_carbohydrate.amount.value / nutritionsDailyValues.total_carbohydrate * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label indent">
                Dietary Fiber
                <NutritionAmountInput productType={productType} roundGroup={5} {...nutritions.dietary_fiber.amount} isReadOnly={isReadOnly} />g
                <div className="fact-percent">{Math.round(nutritions.dietary_fiber.amount.value / nutritionsDailyValues.dietary_fiber * 100)}%</div>
              </div>
              <hr className="thin" />
              <div className="fact-label indent">
                Total Sugars
                <NutritionAmountInput productType={productType} roundGroup={5} {...nutritions.total_sugars.amount} isReadOnly={isReadOnly} />g
              </div>
              <div className="indent-twice">
                <hr className="thin" />
                <div className="fact-label">
                  Includes
                  <NutritionAmountInput productType={productType} isAddedSugar roundGroup={5} value={nutritions.added_sugar || 0} isReadOnly={isReadOnly} />g Added Sugars
                  <div className="fact-percent">{nutritions.added_sugar ? Math.round(nutritions.added_sugar / 50 * 100) : 0}%</div>
                </div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                <b>Protein</b>
                <NutritionAmountInput productType={productType} roundGroup={5} {...nutritions.protein.amount} isReadOnly={isReadOnly} />g
              </div>
            </div>
            <hr className="thick" />
            <div className="micronutrients">
              <div className="fact-label">
                Vitamin D
                <NutritionAmountInput productType={productType} {...nutritions.vitamin_d.amount} isReadOnly={isReadOnly} />mcg
                { /* the unit for Vitamin D in the USDA database is IU instead of mcg.
                     1 UI = 0.025 mcg for Vitamin D */ }
                <div className="fact-percent">
                  {roundBottomNutritionPercentages(nutritions.vitamin_d.amount.value * 0.025 / nutritionsDailyValues.vitamin_d * 100)}%
                </div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                Calcium
                <NutritionAmountInput productType={productType} {...nutritions.calcium.amount} isReadOnly={isReadOnly} />mg
                <div className="fact-percent">
                  <NutritionAmountInput
                    productType={productType}
                    onChange={this.calciumPercentageChanged}
                    roundGroup={6}
                    isReadOnly={isReadOnly}
                    value={nutritions.calcium.amount.value / nutritionsDailyValues.calcium * 100}
                  />%
                </div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                Iron
                <NutritionAmountInput productType={productType} {...nutritions.iron.amount} isReadOnly={isReadOnly} />mg
                <div className="fact-percent">
                  <NutritionAmountInput
                    productType={productType}
                    onChange={this.ironPercentageChanged}
                    roundGroup={6}
                    isReadOnly={isReadOnly}
                    value={nutritions.iron.amount.value / nutritionsDailyValues.iron * 100}
                  />%
                </div>
              </div>
              <hr className="thin" />
              <div className="fact-label">
                Potassium
                <NutritionAmountInput productType={productType} roundGroup={4} {...nutritions.potassium.amount} isReadOnly={isReadOnly} />mg
                <div className="fact-percent">
                  {roundBottomNutritionPercentages(nutritions.potassium.amount.value / nutritionsDailyValues.potassium * 100)}%
                </div>
              </div>
              <hr className="medium" />
              <small>
              <p>
                * The % Daily Value (DV) tells you how much a nutrient in a serving of food contributes to a daily diet.
                2000 calories a day is used for general nutrition advice.
              </p>
              </small>
            </div>
          </div>;
  }
}

// Ingredient's Allergens
export class NutritionIngredientAllergens extends Component {

  render() {

    const { allergens, isReadOnly } = this.props;

    return <div id="allergens-list" className="nutrition-info-panel">
            <h2>Allergens</h2>
            <hr className="thin" />
            <ListGroup className="green-barred-list-group">
              {nutrition_allergens.map(allergen => {

                const fieldName = allergen.fieldName || allergen.name;
                // string/null for allegens with additional info and true/false for the normal ones
                const isChecked = allergen.additionalInfo ? typeof allergens[fieldName].value === 'string' : allergens[fieldName].value;

                return <ListGroupItem key={fieldName} style={isChecked ? { backgroundColor: '#bae08e' } : null}>
                  { allergen.additionalInfo ?
                    <div>
                      <Checkbox inline checked={isChecked}
                                disabled={isReadOnly}
                                onChange={() => {
                                  if (typeof allergens[fieldName].value === 'string') {
                                    // hacky: clear the input field first
                                    allergens[fieldName].onChange('');
                                    setTimeout(() => allergens[fieldName].onChange(null), 12);
                                  } else {
                                    allergens[fieldName].onChange('');
                                  }
                                }}
                                style={{ paddingRight: '6px' }}
                      >{allergen.name}</Checkbox>
                      <input type="text" {...allergens[fieldName]}
                          disabled={isReadOnly}
                          onBlur={ev => {
                            const val = valueFromEvent(ev);
                            if (!val) {
                              // make sure not to do anything if the user hasn't
                              // entered text - don't want to check the box just
                              // because the user highlighted the field
                              return true;
                            } else {
                              allergens[fieldName].onBlur(ev);
                            }
                          }}
                      />
                    </div>
                    :
                    <div>
                      <Checkbox inline {...allergens[fieldName]} style={{ paddingRight: '6px' }} disabled={isReadOnly}>
                        {allergen.name}
                      </Checkbox>
                    </div>
                  }
                </ListGroupItem>;
              })}
            </ListGroup>
          </div>;
  }
}

export function getRecipeAllergensString(recipeAllergens) {
  // iterating through nutrition_allergens (i.e. definitions) rather than
  // straight through recipeAllergens in order to enforce ordering
  return nutrition_allergens
    .filter(a => recipeAllergens.has(a.fieldName || a.name))
    .map(a => {
      const displayName = a.name;
      const fieldName = a.fieldName || a.name;
      const additionalInfo = recipeAllergens.get(fieldName);
      if (additionalInfo) {
        const removedDups = _.uniq(additionalInfo.split(',').map(e => e.trim())).join(', ');
        return displayName + ` (${removedDups})`;
      }
      return displayName;
    })
    .join(', ');
}

// Recipe's Allergens (+ Ingredient list)
class NutritionRecipeAllergens extends Component {

  render() {

    const {
      recipeIngredientList, recipeAllergens,
      updateIngredientListStr, editedIngredientListStr,
      isReadOnly, printMedia,
    } = this.props;

    const recipeIngredientListStr = recipeIngredientList.join(', ');

    return <div id="allergens-list">
            <div>
              <h2>Ingredients</h2>
              <p>{ updateIngredientListStr ?
                <FormControl
                  disabled={isReadOnly}
                  componentClass="textarea" style={{ width: '95%', height: '190px', textAlign: 'left' }}
                  onChange={updateIngredientListStr}>
                  {recipeIngredientListStr}
                </FormControl>
                : <span>{editedIngredientListStr ? editedIngredientListStr : recipeIngredientListStr}</span>}
              </p>
            </div>
            <div className={printMedia ? 'pt-2 pb-2' : ''}>
              <h2>Allergens</h2>
              <p>{getRecipeAllergensString(recipeAllergens).toUpperCase()}</p>
            </div>
          </div>;
  }
}

// Allergens Middleware
class NutritionAllergens extends Component {
  render() {

    const {
      productType, allergens,
      recipeIngredientList, recipeAllergens,
      updateIngredientListStr, editedIngredientListStr,
      isReadOnly, printMedia,
    } = this.props;

    if (productType === 'ingredients') {
      return <NutritionIngredientAllergens allergens={allergens} isReadOnly={isReadOnly} />;
    }

    if (productType === 'recipes') {
      return <NutritionRecipeAllergens
        isReadOnly={isReadOnly}
        printMedia={printMedia}
        recipeIngredientList={recipeIngredientList}
        recipeAllergens={recipeAllergens}
        updateIngredientListStr={updateIngredientListStr}
        editedIngredientListStr={editedIngredientListStr}/>;
    }
  }
}

// Ingredient's Characteristics
export class NutritionIngredientCharacteristics extends Component {

  render() {

    const { characteristics, inModal, fromAddCharacteristicsModal, isReadOnly } = this.props;

    function characteristicLabel(characteristic) {
      let label = characteristic.name;

      if (characteristic.isBold) {
        label = <b>{label}</b>;
      }
      if (characteristic.helpText) {
        label = <TextWithHelp helpText={characteristic.helpText}
                              inModal={inModal}
        >
          {label}
        </TextWithHelp>;
      }

      return label;
    }

    return <div id="characteristics-list" className="nutrition-info-panel">
            <h2>Characteristics</h2>
            <hr className="thin" />
            <ListGroup className="green-barred-list-group">
              {nutrition_characteristics.map(characteristic => {
                const fieldName = characteristic.fieldName || characteristic.name;
                if (fromAddCharacteristicsModal && fieldName === 'ingredients') return null;

                const isChecked = Boolean(characteristics[fieldName].value);
                return <ListGroupItem key={fieldName} disabled={isReadOnly} style={isChecked ? { backgroundColor: '#bae08e' } : null}>
                  { fieldName === 'ingredients' ?
                  <div>
                    <div>Contains the Following Ingredients</div>
                    <FormControl
                      disabled={isReadOnly}
                      componentClass="textarea"
                      style={{ marginTop: '5px', width: '95%', height: '75px', textAlign: 'left' }}
                      {...characteristics[fieldName]}
                    />
                  </div>
                  : <Checkbox inline {...characteristics[fieldName]}
                    style={{ paddingRight: '6px' }} disabled={isReadOnly}>
                    {characteristicLabel(characteristic)}
                  </Checkbox> }
                </ListGroupItem>;
              })}
            </ListGroup>
          </div>;
  }
}

// Recipe's Characteristics
class NutritionRecipeCharacteristics extends Component {

  render() {

    const { recipeAllergens, recipeCharacteristics } = this.props;

    let glutenFree = !(recipeAllergens.has('cerealsGluten') || recipeAllergens.has('wheat'));
    let vegetarian = !recipeCharacteristics.contains('pork') && !recipeCharacteristics.contains('meat') && !recipeCharacteristics.contains('poultry') && !recipeAllergens.has('fish');
    let vegan = vegetarian && !recipeAllergens.has('milk') && !recipeAllergens.has('eggs');
    let dairyFree = !recipeAllergens.has('milk');
    let cornFree = !recipeCharacteristics.contains('corn');
    let treeNutFree = !recipeAllergens.has('treeNuts');
    let peanutFree = !recipeAllergens.has('peanuts');
    let containsPork = recipeCharacteristics.contains('pork');

    return <div id="characteristics-list">
      <div>
        <h2>Characteristics</h2>
        <ul style={{ paddingLeft: '15px' }}>
          {vegetarian && !vegan ? <li>Vegetarian</li> : ''}
          {vegan ? <li>Vegan</li> : ''}
          {glutenFree ? <li>Gluten Free</li> : ''}
          {dairyFree ? <li>Dairy Free</li> : ''}
          {cornFree ? <li>Corn Free</li> : ''}
          {treeNutFree ? <li>Tree Nut Free</li> : ''}
          {peanutFree ? <li>Peanut Free</li> : ''}
          {containsPork ? <li>Contains Pork</li> : ''}
        </ul>
      </div>
    </div>;
  }
}

// Characteristics Middleware
class NutritionCharacteristics extends Component {

  render() {

    const { productType, characteristics, recipeAllergens, recipeCharacteristics, inModal, isReadOnly } = this.props;

    if (productType === 'ingredients') {
      return <NutritionIngredientCharacteristics inModal={inModal} characteristics={characteristics} isReadOnly={isReadOnly} />;
    }

    if (productType === 'recipes') {
      return <NutritionRecipeCharacteristics
        recipeAllergens={recipeAllergens}
        recipeCharacteristics={recipeCharacteristics}
      />;
    }
  }
}

// Main Entry
export default class NutritionDisplay extends Component {

  constructor(props) {
    super(props);
    this.nutritions = {};
    this.nutritionsDailyValues = {};
    this.initNutritions(props); // Required at least for print review
  }

  componentWillReceiveProps(nextProps) {
    this.initNutritions(nextProps);
  }

  initNutritions(nextProps) {
    const { productType, allNutrients, nutrientInfo } = nextProps;
    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);

    if (_.size(this.nutritionsDailyValues) === 0) {
      const nutritionDailyValueMap = Immutable.Map(allNutrients.map(v => [v.get('nndbsrId'), v.get('dailyValue')]));
      _.each(nutrient_fields, (nndbsrId, field) => {
        this.nutritionsDailyValues[field] = nutritionDailyValueMap.get(nndbsrId) || nutrient_default_daily_values[field];
      });
    }

    if (productType === 'ingredients') {
      // RF-MIGRATION: use array.forEach and stuff like that
      if (nutrientInfo.length === 0) return;
      let nutrientIdToNutrientField = {};
      nutrientInfo.forEach(n => {
        nutrientIdToNutrientField[n.nutrient.value] = n;
      });

      _.each(nutrient_fields, (nndbsrId, field) => {
        const nutrientId = nndbsrIdToNutrientIdMap[nndbsrId];
        this.nutritions[field] = nutrientIdToNutrientField[nutrientId];
      });
    }

    if (productType === 'recipes') {

      const {
        productId, detailedIngredients, recipeNutrients, nutrientServingAmount, nutrientServingUnit,
        recipe, isSubRecipeInIngredientEdit, isSubRecipeInRecipeEdit, conversions, measures,
      } = nextProps;

      // Sub-recipes in IngredientEdit
      if (isSubRecipeInIngredientEdit) {
        const productDetails = detailedIngredients.get(productId);
        if (productDetails) {
          const servingAmountInRecipeUnit = convert(nutrientServingAmount.value, nutrientServingUnit.value, recipe.recipeSize.unit, conversions);
          const nutritionInfo = productDetails.get('nutritionInfo');

          _.each(nutrient_fields, (nndbsrId, field) => {
            this.nutritions[field] = {
              amount: {
                value: nutritionInfo.getIn(['nutritions', field]) * servingAmountInRecipeUnit / recipe.recipeSize.amount,
              },
            };
          });
          this.nutritions.added_sugar = nutritionInfo.getIn(['nutritions', 'added_sugar']) * servingAmountInRecipeUnit / recipe.recipeSize.amount;

          this.recipeIngredientList = nutritionInfo.get('ingredientList').map(i => i.get('name'));
          this.recipeAllergens = nutritionInfo.get('allergenList');
          this.recipeCharacteristics = nutritionInfo.get('characteristicList');
        }
      } else {
        // Recipes in RecipeEdit

        const nutritions = {};
        _.each(nutrient_fields, (nndbsrId, field) => {
          nutritions[field] = {
            amount: {
              value: 0,
            },
          };
        });
        nutritions.added_sugar = 0;

        let ingredients = Immutable.Map();
        this.recipeAllergens = Immutable.Map();
        this.recipeCharacteristics = Immutable.Set();

        for (let step of recipe.steps) {
          for (let ingredient of step.ingredients) {

            // Size in parent recipe's step
            const amountInParent = (ingredient.quantity || ingredient).amount;
            const unitInParent = (ingredient.quantity || ingredient).unit;
            const ingredientId = ingredient.ingredient;

            // Haven't received enough information
            if (!detailedIngredients.has(ingredientId)) continue;

            const details = detailedIngredients.get(ingredientId);
            const nutritionInfo = details.get('nutritionInfo');
            if (nutritionInfo.get('nonEdible')) {
              continue;
            }

            // Allergen List
            nutritionInfo.get('allergenList').forEach((v, k) => {
              this.recipeAllergens = this.recipeAllergens.update(k, (oldV) => {
                if (v.length > 0) {
                  return oldV ? oldV + ', ' + v : v;
                } else if (oldV) {
                  return oldV;
                } else {
                  return '';
                }
              });
            });

            // Characteristic List
            this.recipeCharacteristics = this.recipeCharacteristics.concat(nutritionInfo.get('characteristicList'));

            // This ingredient's size
            const servingAmount = details.get('nutrientServingAmount');
            const servingUnit = details.get('nutrientServingUnit');

            const conversions = detailedIngredients.getIn([ingredientId, 'measures']);
            let sizeRatioToParent;

            // Enough size information to calculate sizeRatioToParent
            if (servingAmount && servingUnit && unitInParent && amountInParent) {
              sizeRatioToParent = convert(amountInParent, unitInParent, servingUnit, conversions) / servingAmount;
            }

            // Nutrition Facts
            // Enough info to calculate nutritional values?
            if (sizeRatioToParent !== undefined) { // want to respect a zero value
              _.each(nutrient_fields, (nndbsrId, field) => {
                nutritions[field].amount.value += nutritionInfo.getIn(['nutritions', field]) * sizeRatioToParent;
              });
              nutritions.added_sugar += nutritionInfo.getIn(['nutritions', 'added_sugar']) * sizeRatioToParent;
            }

            // Ingredient List
            // Calculate weight in the ingredient list
            let weightMultiplier = 0;
            if (sizeRatioToParent !== undefined) { // want to respect a zero value
              weightMultiplier = sizeRatioToParent;
            } else if (amountInParent && unitInParent) {
              // ingredient that doesn't have serving size defined
              // keep in sync with RecipeUtils.scala:calculateNutritionalInfo(),
              // for calculating contributionWeight

              // these ingredients will have a value of '1' in their ingredientList,
              // meaning the effective serving size for ingredient-list purposes
              // is 1 gram/milliliter
              if (conversions.find(c => c.get('measure') === 2)) {
                weightMultiplier = convert(amountInParent, unitInParent, 2, conversions);
              } else if (conversions.find(c => c.get('measure') === 3)) {
                // if can't convert to grams, try milliliters; for lots of stuff,
                // 1 mL = 1g (since most things are mostly water)
                weightMultiplier = convert(amountInParent, unitInParent, 9, conversions);
              }
            }

            nutritionInfo.get('ingredientList').forEach(i => {
              ingredients = ingredients.update(i.get('name'), (w = 0) =>
                w + (i.get('weight') * weightMultiplier),
              );
            });
          }
        }

        const recipeSize = recipe.recipeSize;
        if (isSubRecipeInRecipeEdit) {
          if (!recipeSize.amount || !recipeSize.unit) return; // Wait until recipe's size is inputted

          const servingAmount = parseFloat(nutrientServingAmount.value || 1);
          const servingUnit = nutrientServingUnit.value || recipeSize.unit;

          const servingAmountInRecipeUnit = convert(servingAmount, servingUnit, recipeSize.unit, conversions);
          _.each(nutrient_fields, (nndbsrId, field) => {
            nutritions[field].amount.value *= servingAmountInRecipeUnit / recipeSize.amount;
          });
          nutritions.added_sugar *= servingAmountInRecipeUnit / recipeSize.amount;
        } else {

          const servingAmount = parseFloat(recipeNutrients.servingAmount.value || 1);
          const portions = recipeSize.amount || 1;

          let scaleFactor;
          if (recipeNutrients.packaged.value) {
            scaleFactor = recipeNutrients.portionsPerPackage.value / portions / recipeNutrients.servingsPerPackage.value;
          }
          // else if (recipeNutrients.portionUnit.value) {
          //   const portionAmount = recipeNutrients.portionAmount.value || 1;
          //   const units = measures.getIn([recipeNutrients.portionMeasure.value, 'units']);
          //   const portionUnit = units.find(u => u.get('id') === recipeNutrients.portionUnit.value);
          //   const servingUnit = units.find(u => u.get('id') === recipeNutrients.servingUnit.value) || portionUnit;
          //   scaleFactor = servingAmount / (portionAmount * portionUnit.get('size') / servingUnit.get('size'));
          // }
          else {
            scaleFactor = servingAmount / portions;
          }

          _.each(nutrient_fields, (nndbsrId, field) => {
            nutritions[field].amount.value *= scaleFactor;
          });
          nutritions.added_sugar *= scaleFactor;
        }

        this.nutritions = nutritions;
        this.recipeIngredientList = ingredients.sort((a, b) => b - a).keySeq();
      }
    }
  }

  render() {

    if (_.size(this.nutritions) < _.size(nutrient_fields)) {
      return <LoadingSpinner />;
    }

    const {
      productType, isSubRecipeInIngredientEdit, isSubRecipeInRecipeEdit, inPrintModal, printMedia,
      conversions, measures, recipeNutrients,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      allergens, characteristics,
      updateIngredientListStr, editedIngredientListStr,
      inModal,
      isReadOnly,
    } = this.props;

    const recipeType = productType === 'recipes';
    const justifyContent = recipeType && !printMedia ? 'left' : 'center';
    return <div>
        { productType === 'recipes' && !isSubRecipeInIngredientEdit && !isSubRecipeInRecipeEdit && !inPrintModal && !printMedia ?
          <div className="recipe-nutrients">
            <Checkbox
              disabled={isReadOnly}
              {...recipeNutrients.packaged}
              onChange={newPackaged => {
                recipeNutrients.packaged.onChange(newPackaged);
                if (newPackaged) {
                  if (!recipeNutrients.portionsPerPackage.value) {
                    recipeNutrients.portionsPerPackage.onChange(1);
                  }
                  if (!recipeNutrients.servingsPerPackage.value) {
                    recipeNutrients.servingsPerPackage.onChange(1);
                  }
                }
              }}
            >
              This is a packaged item
            </Checkbox>
            {!recipeNutrients.packaged.value ?
              null
              /* <div style={{ display: 'flex', margin: '10px 0px' }}>
                <div style={{ minWidth: '300px' }}>Weight or Volume per Portion (optional!)</div>
                <QuantityInput
                  disabled={isReadOnly}
                  reduxFormFields={{
                    amountField: recipeNutrients.portionAmount,
                    measureField: {
                      ...recipeNutrients.portionMeasure,
                      onBlur() {},
                      onChange(newMeasure) {
                        if (typeof newMeasure === typeof 0) {
                          recipeNutrients.portionMeasure.onChange(newMeasure);
                          if (recipeNutrients.servingMeasure.value !== newMeasure) {
                            recipeNutrients.servingMeasure.onChange(newMeasure);
                          }
                        }
                      },
                    },
                    unitField: {
                      ...recipeNutrients.portionUnit,
                      onBlur() {},
                      onChange(newUnit) {
                        if (typeof newUnit === typeof 0) {
                          recipeNutrients.portionUnit.onChange(newUnit);
                          if (!recipeNutrients.portionAmount.value) {
                            recipeNutrients.portionAmount.onChange(1);
                          }
                          if (!recipeNutrients.servingUnit.value || getMeasure(recipeNutrients.servingUnit.value) !== getMeasure(newUnit)) {
                            recipeNutrients.servingUnit.onChange(newUnit);
                          }
                        }
                      },
                    },
                  }}
                  measures={measures.filter(m => m.get('id') === 2 || m.get('id') === 3)}
                />
              </div>
              */
            : <div>
                <div style={{ display: 'flex', margin: '10px 0px' }}>
                  <div style={{ minWidth: '215px' }}>Portions per Package:</div>
                  <MaskedNumberInput disabled={isReadOnly} {...recipeNutrients.portionsPerPackage}/>
                </div>
                <div style={{ display: 'flex', margin: '10px 0px' }}>
                  <div style={{ minWidth: '215px' }}>Servings per Package:</div>
                  <MaskedNumberInput disabled={isReadOnly} {...recipeNutrients.servingsPerPackage}/>
                </div>
                <div style={{ display: 'flex', margin: '10px 0px' }}>
                  <div style={{ minWidth: '215px' }}>Serving Name (optional):</div>
                  <FormControl type="text" style={{ maxWidth: '128px' }} disabled={isReadOnly} {...recipeNutrients.servingName}/>
                </div>
                <div style={{ display: 'flex', margin: '10px 0px' }}>
                  <div style={{ minWidth: '215px' }}>Weight or Volume per serving:</div>
                  <QuantityInput
                    disabled={isReadOnly}
                    reduxFormFields={{
                      amountField: recipeNutrients.servingAmount,
                      measureField: recipeNutrients.servingMeasure,
                      unitField: recipeNutrients.servingUnit,
                    }}
                    measures={measures.filter(m => m.get('id') === 2 || m.get('id') === 3)}
                  />
                </div>
              </div>
            }
          </div>
        : null }
        <div style={{ display: 'flex', justifyContent }}>
          <NutritionFacts
            isReadOnly={isReadOnly}
            productType={productType}
            isSubRecipeInIngredientEdit={isSubRecipeInIngredientEdit}
            isSubRecipeInRecipeEdit={isSubRecipeInRecipeEdit}
            printMedia={printMedia}
            conversions={conversions}
            measures={measures}
            recipeNutrients={recipeNutrients}
            nutrientServingAmount={nutrientServingAmount}
            nutrientServingMeasure={nutrientServingMeasure}
            nutrientServingUnit={nutrientServingUnit}
            nutritions={this.nutritions}
            nutritionsDailyValues={this.nutritionsDailyValues}
          />
          <div
            className={recipeType ? 'nutrition-info-panel' : ''}
            style={recipeType ? {} : { display: 'flex' }}>
            <NutritionAllergens
              isReadOnly={isReadOnly}
              printMedia={printMedia}
              productType={productType}
              allergens={allergens}
              recipeIngredientList={this.recipeIngredientList}
              recipeAllergens={this.recipeAllergens}
              updateIngredientListStr={updateIngredientListStr}
              editedIngredientListStr={editedIngredientListStr}
            />
            <NutritionCharacteristics
              isReadOnly={isReadOnly}
              productType={productType}
              characteristics={characteristics}
              recipeAllergens={this.recipeAllergens}
              recipeCharacteristics={this.recipeCharacteristics}
              inModal={inModal}
            />
          </div>
        </div>
      <div className="clearfix" />
    </div>;
  }
}
