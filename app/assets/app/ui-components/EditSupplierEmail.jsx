import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import ReactPropTypes from 'prop-types'
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';

import Immutable from 'immutable';
import _ from 'lodash';

import moment from 'moment';

import ImmutablePropTypes from 'react-immutable-proptypes';
import * as ParsleyPropTypes from './utils/PropTypes';

import {
  getSupplierDetails, saveSupplier, getUserInfo, saveUserInfo,
} from '../webapi/endpoints';
import { actionCreators as SupplierActions } from '../reducers/supplierDetails';

import { deepOmit } from "../utils/general-algorithms";

import {
  checkFields, required, optional,
} from '../utils/validation-utils';
import isEmail from 'validator/lib/isEmail'

import { Form, FormGroup, Row, Col, ControlLabel, FormControl, Button } from "react-bootstrap";

import Validate from 'common/ui-components/Validate';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';

function validate(values) {

  const validators = {
    supplierEmail: required(val => !isEmail(val) ? "Not valid e-mail" : null),
    contactName: optional(),
    instructions: optional(),
    businessName: optional(),
    shippingAddress: optional(),
    shippingPhoneNumber: optional(),
  };

  return checkFields(values, validators);
}

@reduxForm(
    {
      form: 'edit-supplier-email',
      fields: [
        'contactName',
        'instructions',
        'supplierEmail',
        'businessName',
        'shippingAddress',
        'shippingPhoneNumber',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => ({
      initialValues: !!state.get('supplierDetails') && !!state.get('userInfo')
          ? {
            supplierEmail: state.get('supplierDetails').email,
            instructions: state.get('supplierDetails').instructions,
            contactName: state.get('supplierDetails').contactName,
            ..._.pick(
                state.get('userInfo').toJS(),
                ['businessName', 'shippingAddress', 'shippingPhoneNumber']
            ),
          } : {},
      mainInfoLoaded: !!state.get('supplierDetails') && !!state.get('userInfo'),
      userInfo: state.get('userInfo'),
      supplierDetails: state.get('supplierDetails'),
    }),
    dispatch => ({
      supplierActions: bindActionCreators(SupplierActions, dispatch),
    })
)
export default class EditSupplierEmail extends Component {
  static propTypes = {
    // from caller
    supplierId: ReactPropTypes.string,
  };

  componentWillMount() {
    getUserInfo();
    if (!!this.props.supplierId)
      getSupplierDetails(this.props.supplierId);
  }

  componentWillUnmount() {
    const { supplierId, supplierActions } = this.props;
    if (!!supplierId) {
      supplierActions.dropDetails();
    }
  }

  submit = (deets) => {
    let syncInfo = Promise.resolve();

    const {
      businessName,
      shippingAddress,
      shippingPhoneNumber,
      contactName,
      supplierEmail,
      instructions,
    } = this.props.fields;

    if (shippingAddress.value != shippingAddress.initialValue
        || shippingPhoneNumber.value != shippingPhoneNumber.initialValue
        || businessName.value != businessName.initialValue) {
      const userInfo = Immutable.fromJS(this.props.userInfo)
          .set('businessName', businessName.value)
          .set('shippingAddress', shippingAddress.value)
          .set('shippingPhoneNumber', shippingPhoneNumber.value)
          .toJS();
      syncInfo = syncInfo.then(() => saveUserInfo(userInfo))
    }

    if (supplierEmail.value != supplierEmail.initialValue
        || instructions.value != instructions.initialValue
        || contactName.value != contactName.initialValue) {
      const supplierInfo = Immutable.fromJS(this.props.supplierDetails)
          .set('email', supplierEmail.value)
          .set('instructions', instructions.value)
          .set('contactName', contactName.value)
          .toJS();
      syncInfo = syncInfo.then(() => saveSupplier(this.props.supplierId, supplierInfo));
    }
    return syncInfo
        .then(() => this.props.sendEmail(supplierEmail.value))
        .then(() => this.props.hide());
  };

  render() {
    const {
      mainInfoLoaded, handleSubmit, submitting,
      supplierDetails, userInfo, isEmailSent
    } = this.props;

    const {
        contactName,
        supplierEmail,
        instructions,
        businessName,
        shippingAddress,
        shippingPhoneNumber,
    } = this.props.fields;

    if (!(mainInfoLoaded)) {
      return <LoadingSpinner />;
    }

    return <div>
      <div className="shopping-list">
        <div className="shopping-list-inside-wrapper">
          <div>
            <form onSubmit={handleSubmit(this.submit)} horizontal>
              <Button type="submit" bsStyle="primary" className="pull-right" disabled={isEmailSent || submitting}>
                {isEmailSent ? "Sent" : submitting ? "Sending..." : "Send"}
              </Button>
              <Row>
                <Col md={12}>
                  <p><b>Order Form:</b></p>
                  <p><b>Order Placed By:</b> {userInfo.get("longName")}</p>
                  <p><b>Order Date:</b> {moment().format().substring(0, 10)}</p>
                  <p><b>Vendor:</b> {supplierDetails.name}</p>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Row>
                    <Col componentClass={ControlLabel} md={4}><b>Attn:</b></Col>
                    <Col md={8}>
                      <Validate {...contactName}>
                        <FormControl type="text" className="mb-1"
                            placeholder="Name (optional)"
                            {...contactName}
                            />
                      </Validate>
                      <Validate {...supplierEmail}>
                        <FormControl type="email"
                            placeholder="Email"
                            {...supplierEmail}/>
                      </Validate>
                    </Col>
                  </Row>
                  {
                    supplierDetails.contacts.length > 0 ?
                    <Row>
                      <Col componentClass={ControlLabel} md={4}><b>Cc:</b></Col>
                      <Col md={8}>{supplierDetails.contacts.map(c => <span>{c.email}<br /></span>)}</Col>
                    </Row>
                    : null
                  }
                  <Row className="mt-6">
                    <Col componentClass={ControlLabel} md={4}><b>Special Instructions:</b></Col>
                    <Col md={8}>
                      <Validate {...instructions}>
                        <FormControl componentClass="textarea" rows={4}
                            {...instructions}
                        />
                      </Validate>
                    </Col>
                  </Row>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col componentClass={ControlLabel} md={4}>
                      <b>Deliver To:</b>
                    </Col>
                    <Col md={8}>
                      <Validate {...businessName}>
                        <FormControl type="text" className="mb-1"
                            placeholder="Business Name (optional)"
                            {...businessName} />
                      </Validate>
                      <Validate {...shippingAddress}>
                        <FormControl componentClass="textarea" rows={3} className="mb-1"
                            placeholder="Shipping Address"
                            autoComplete="off"
                            {...shippingAddress}
                        />
                      </Validate>
                      <Validate {...shippingPhoneNumber}>
                        <FormControl type="text"
                            placeholder="Shipping Phone Number"
                            {...shippingPhoneNumber} />
                      </Validate>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Button
                type="submit" bsStyle="primary" disabled={isEmailSent || submitting}
                style={{ position: 'absolute', margin: '18px 11px', right: '0px', bottom: '0px', zIndex: '2018' }}
              >
                {isEmailSent ? 'Sent' : submitting ? 'Sending...' : 'Send'}
              </Button>
            </form>
          </div>
        </div>
      </div>
      <div className="clearfix" />
    </div>;
  }
}
