import React, { Component } from "react";

import ReactPropTypes from 'prop-types'
import { withRouter } from 'react-router';

@withRouter
export default class NewButton extends Component {

  static propTypes = {
    id: ReactPropTypes.string.isRequired,
    noun: ReactPropTypes.string.isRequired,
    createItem: ReactPropTypes.func,
    newPage: ReactPropTypes.string,
    router: ReactPropTypes.object.isRequired
  };
  static defaultProps = {
    noun: "",
    createItem: null,
    newPage: "#"
  };

  navigateToNew(ev) {
    const { router, newPage } = this.props;
    if (router) {
      ev.preventDefault();
      router.push(newPage);
    }
  }

  render() {
    return (
      <span id={this.props.id} className="new-link" onClick={this.props.createItem || this.navigateToNew.bind(this)}
         href={this.props.newPage}>
        + New {this.props.noun}
      </span>);
  }

}

