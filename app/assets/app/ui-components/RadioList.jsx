import React, { Component } from "react";
import _ from "lodash";
import ReactPropTypes from 'prop-types'
import Immutable from 'immutable';

import { Row, Col } from "react-bootstrap";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

import classNames from 'classnames';

import Radio from 'common/ui-components/Radio';
import AddButton from "./AddButton";

import { columnWidthSet, offsetSizeSet } from "./utils/PropTypes";

const deleteButtonColumnWidth = 1;

export default class RadioList extends Component {
  static propTypes = {
    columnWidths: ReactPropTypes.arrayOf(columnWidthSet).isRequired,
    headerNames: ReactPropTypes.arrayOf(ReactPropTypes.string).isRequired,
    headerColumnWidths:ReactPropTypes.arrayOf(columnWidthSet),

    // name is for internal HTML purposes; unique per radio list please!
    radioInputName: ReactPropTypes.string.isRequired,
    onSelected: ReactPropTypes.func.isRequired,

    onAdd: ReactPropTypes.func,
    addButtonNoun: ReactPropTypes.string,
    // TODO: validate columnWidths.length == headerNames.length

    disabled: ReactPropTypes.bool,
    canAdd: ReactPropTypes.bool,
  };

  static defaultProps = {
    canAdd: true,
    disabled: false,
  }

  static childContextTypes = {
    columnWidths: ReactPropTypes.arrayOf(columnWidthSet).isRequired,

    radioInputName: ReactPropTypes.string.isRequired,
    onSelected: ReactPropTypes.func.isRequired,
    disabled: ReactPropTypes.bool.isRequired,
  };

  getChildContext() {
    return {
      radioInputName: this.props.radioInputName,
      onSelected: this.props.onSelected,

      columnWidths: this.props.columnWidths,
      disabled: this.props.disabled,
    }
  }

  render() {
    const {
        children, headerColumnWidths,
        columnWidths, headerNames,
        onAdd, addButtonNoun,
        disabled, canAdd,
    } = this.props;

    var headers = _.zip(
        (headerColumnWidths || columnWidths), headerNames,
        _.range(headerNames.length)).map(
        ([widthOpts, name, i]) =>
            <Col {...widthOpts} key={i}>
              {name}
            </Col>
    );

    return (
        <div>
          <Row className="radio-list-header">
            {headers}
          </Row>
          {children}
          {!onAdd ? null
              : <AddButton className="radio-list-add" onClick={onAdd}
                           noun={addButtonNoun} disabled={disabled || !canAdd} />
          }
        </div>);
  }
}

RadioList.Row = class extends Component {
  static displayName = "RadioListRow";

  constructor(props, context) {
    super(props, context);
    this.handleSelection = this.handleSelection.bind(this);
  }

  static contextTypes = {
    radioInputName: ReactPropTypes.string.isRequired,
    onSelected: ReactPropTypes.func.isRequired,

    columnWidths: ReactPropTypes.arrayOf(columnWidthSet).isRequired,
    disabled: ReactPropTypes.bool.isRequired,
  };

  static propTypes = {
    isSelected: ReactPropTypes.bool.isRequired,
    onSelectedArgument: ReactPropTypes.any.isRequired,

    onDelete: ReactPropTypes.func, // only required for deleteBehavior="enable"
    deleteBehavior: ReactPropTypes.oneOf([
      // TODO: Actually implement disabled/hidden behavior
      "enabled",
      // show, but indicate a disabled state somehow
      "disabled",
      // hide, and let content from the last column in the corresponding row spill
      // over - like for example the "In-house" source on SourceEditor
      "hidden"
    ]),
    deleteTooltip: ReactPropTypes.string,
    selectTooltip: ReactPropTypes.string,
    disabled: ReactPropTypes.bool,
    selectable: ReactPropTypes.bool,

    striped: ReactPropTypes.bool,
  };

  static defaultProps = {
    deleteBehavior: "enabled",
    disabled: false,
    striped: true,
    selectable: true,
  };

  handleSelection(ev) {
    if (this.props.isSelected) {
      // annoyingly, the onChange event for radio buttons fires even if the
      // currently-selected item is clicked (i.e. even if there's no change).
      // This is because react subscribes to the 'click' event instead of the
      // 'change' event for these elements
      return;
    }
    return this.context.onSelected(this.props.onSelectedArgument)
  }

  render() {

    var {
        children,
        deleteBehavior, onDelete, deleteTooltip, selectTooltip,
        striped, isSelected,
        selectable,
    } = this.props;

    const {
        columnWidths, 
        radioInputName, disabled,
    } = this.context;

    if (disabled) {
      deleteBehavior = "hidden";
      striped = false;
    }

    var cells = React.Children.toArray(children).map((cell, i) => {
      return <Col key={i + 1} {...columnWidths[i + 1]}>
        {cell}
      </Col>;
    });

    var radioButton = <Radio
        disabled={disabled || !selectable}
        checked={isSelected}
        name={radioInputName}
        onChange={this.handleSelection}
    />;
    if (!selectable && selectTooltip) {
      var selectTooltip = <Tooltip>{selectTooltip}</Tooltip>
      radioButton = <OverlayTrigger placement="top" overlay={selectTooltip}>
        <span className="pull-left">{radioButton}</span>
      </OverlayTrigger>;
    }

    var deleteButton;
    switch (deleteBehavior) {
      case "enabled":
        deleteButton =
            <span onClick={onDelete}
               href="#"
               className="pull-right list-item-delete-link">
              delete
            </span>;
        break;
      case "disabled":
        // TODO: validate this.props.deleteTooltip exists
        // TODO: make tooltip accessible by adding unique id attr
        var tooltip = <Tooltip>{deleteTooltip}</Tooltip>;
        deleteButton =
            <OverlayTrigger placement="top" overlay={tooltip}>
              <span className="pull-right list-item-delete-link disabled">
                delete
              </span>
            </OverlayTrigger>;
        break;
      case "hidden":
        deleteButton = null;
        break;
    }

    let rowClassNames =  classNames({
      'radio-list-item': true,
      'form-inline-striped': striped
    });

    return (
        <Row className={rowClassNames}>
          <Col {...columnWidths[0]}>
            {radioButton}
          </Col>
          {cells}
          { deleteButton ?
              <Col
                   className="radio-list-delete"
              >
                {deleteButton}
              </Col> :
              null
          }
        </Row>);
  }
}
