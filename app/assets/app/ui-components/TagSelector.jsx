import _ from 'lodash';
import React, { Component } from 'react';
import Select from './Select';
import { get } from 'common/webapi/utils';
import type from 'react';

class TagSelector extends Component {

  static defaultProps = {
    optionName: 'item',
    optional: false,
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      existingTags: null,
    };
  }

  updateTags(props) {
    const { tagType } = props;

    const handleTags = tags => {
      this.setState({
        existingTags: _.sortBy(
          tags.filter(t => t.name.length > 0), t => t.name.toLowerCase(),
        ),
      });
    }

    get(`/tags/${tagType}`).then(handleTags);
  }

  componentDidMount() {
    this.updateTags(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tagType !== this.props.tagType) {
      this.updateTags(nextProps);
    }
  }

  render() {
    let { onChange, value, optionName, ...passthrough } = this.props;

    let currentTags;
    if (!value) {
      currentTags = [];
    } else {
      currentTags = value.map(t => t.name);
    }

    if (passthrough.onBlur) {
      // see the wrapper for non-Creatable selects in './Select.jsx' for justification
      let { onBlur: oldOnBlur, value } = passthrough;
      passthrough.onBlur = () => oldOnBlur(value);
    }

    const { existingTags } = this.state;
    const allTags = existingTags ? _.union(existingTags.map(t => t.name), currentTags) : [];

    function handleChange(tags) {
      return onChange(tags.map(tag => {
        const existingTag = existingTags.find(t => t.name === tag.value);
        return {
          id: existingTag ? existingTag.id : -1,
          name: tag.value,
        };
      }));
    }

    if (!existingTags)
      return <div>Loading...</div>;

    let placeholder;
    if (this.props.disabled)
      placeholder = '';
    else if (allTags.length > 0)
      placeholder = `Select ${optionName}, or type new ${optionName} and hit "enter"`;
    else
      placeholder = `Type in new ${optionName} name and hit "enter"`;

    if (!this.props.disabled && this.props.optional) {
      placeholder += ' (optional)';
    }

    return (
        <Select
            allowBlankNewOptions={false}
            name="tag-select"
            isLoading={!existingTags}
            className="Tag_Select"
            isMulti creatable
            isClearable={false}
            selectOnBlur={false}
            placeholder={placeholder}

            options={
              allTags ? allTags.map(tag => ({ label: tag, value: tag })) : []
            }
            onChange={handleChange}

            newOptionCreator={({ label: tag, labelKey, valueKey }) => {
              return {
                [labelKey]: tag,
                [valueKey]: tag,
              };
            }}
            promptTextCreator={label => 'Add category "' + label + '"'}
            value={currentTags}
            {...passthrough}
        />
    );
  }
}

export default TagSelector;
