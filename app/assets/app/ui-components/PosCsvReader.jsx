// @flow
import React, { Component } from 'react';
import _ from 'lodash';
import Papa from 'papaparse';
import { FormControl } from 'react-bootstrap';
import classnames from 'classnames';

type Props = {
  noun: string,
  onClick: Function,
}

export default class PosCsvReader extends Component<Props> {

  constructor(props: Props) {
    super(props);
    this.handleCsv = this.handleCsv.bind(this);
  }

  handleCsv = (file: any) => {
    const { onClick } = this.props;
    Papa.parse(file, {
      header: true,
      trimHeaders: true,
      skipEmptyLines: 'greedy',
      complete(results) {
        const header = _.compact(results.meta.fields); // Removes empty headers
        const data = results.data
          .map(row => _.pick(row, header)) // Removes empty keys
          .filter(row => {
            const values = _.map(_.mapValues(row), _.trim);
            // Filters empty and header rows
            return !_.isEqual(values, header) && _.some(values, (col) => col !== '');
          });
        onClick(header, data);
      },
    });
  }

  render() {
    const { noun } = this.props;
    return <label id="pos-import-upload-botton"
      className={classnames('green-btn', 'btn', 'upload-button', 'btn-primary', 'import-pos-button')}
      htmlFor="fileUpload">
      {noun}
      <FormControl
        id="fileUpload"
        type="file"
        accept=".csv"
        value=""
        onChange={(e) => this.handleCsv(e.target.files[0])}
      />
     </label>;
  }
}