// @flow
import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import PosCsvReader from '../PosCsvReader';

type Props = {
  title: string,
  onHide: Function,
  onUpload: Function,
}

export default class UploadCsvModal extends Component<Props> {

  render() {
    const { title, onHide, onUpload } = this.props;

    return <Modal show onHide={onHide} className="small-modal">
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="pos-import-modal-container">
          <PosCsvReader
            noun="Select File"
            onClick={onUpload}
            className="purchase-btn"
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
      </Modal.Footer>
    </Modal>;
  }
}