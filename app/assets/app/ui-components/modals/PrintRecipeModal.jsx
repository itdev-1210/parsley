import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';

import _ from 'lodash';

import Immutable from 'immutable';
import { reduxForm } from 'redux-form';
import { Col, Row, Form, ControlLabel, Button, Modal } from 'react-bootstrap';
import Select from '../Select';
import { checkFields, required, optional, positive } from '../../utils/validation-utils';
import Validate from "common/ui-components/Validate";
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { RecipeSize } from '../pages/RecipeEdit';
import { PrintPreps } from '../pages/OrderEdit';
import Checkbox from '../Checkbox';
import RecipePrint from '../prints/RecipePrint';

import {
  getUserInfo,
  saveUserInfo,
  getIngredientList,
  getProductionOrder,
  getMultipleProductsDetails,
  getMultipleProductsUsageInfo,
} from '../../webapi/endpoints';
import { printContent, photoUrl } from '../../utils/printing';

@reduxForm(
    {
      form: 'printModalForm',
      fields: [
        'amount',
        'measure',
        'unit',
        'format',
        'batchSize',
        'batchLabel',
        'includeSubRecipes',
        'breakPageAfterEachRecipe',
        'includePhotoInRecipePrint',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: (values, props) => {
        return props.page === 'recipe' ? checkFields(values,
          {
            amount: required(positive),
            measure: optional(positive),
            unit: optional(positive),
            format: optional(),
            batchSize: optional(),
            batchLabel: optional(),
            includePhotoInRecipePrint: required(),
          })
          : checkFields(values, {
            breakPageAfterEachRecipe: required(),
            format: required(),
            includeSubRecipes: required(),
            includePhotoInRecipePrint: required(),
          })
      },
    },
    (state, ownProps) => ({
      userInfo: state.get('userInfo'),
      allIngredients: state.get('ingredients'),
      detailedIngredients: state.get('detailedIngredients') || Immutable.Map(),
      productionOrder: state.getIn(['productionOrder', 'productionOrder']),
      initialValues: ownProps.page === 'recipe' ? {
        'amount': ownProps.amount,
        'measure': ownProps.measure,
        'unit': ownProps.unit,
        'format': state.getIn(['userInfo', 'printPreference', 'recipePrintFormatPreference']),
        'batchSize': ownProps.batchSize,
        'batchLabel': ownProps.batchLabel,
        'includeSubRecipes': state.getIn(['userInfo', 'printPreference', 'printSubRecipePreferences']) || false,
        'includePhotoInRecipePrint': state.getIn(['userInfo', 'printPreference', 'includePhotoInRecipePrint']),
      } : {
        'format': state.getIn(['userInfo', 'printPreference', 'recipePrintFormatPreference']),
        'breakPageAfterEachRecipe': state.getIn(['userInfo', 'printPreference', 'breakPageOnRecipePrint']),
        'includeSubRecipes': ownProps.shouldPrintSubRecipes,
        'includePhotoInRecipePrint': state.getIn(['userInfo', 'printPreference', 'includePhotoInRecipePrint']),
      },
      // overwriteOnInitialValuesChange overwrites ALL values, not just the ones
      // that have changed; this means that whenever we change one of the user
      // preferences, the amount/measure fields get clobbered
      overwriteOnInitialValuesChange: false,
    })
)
export default class PrintRecipeModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      emptyContentText: null,
      isReady: false,
    };
  }

  async printRecipes(values) {
    const {
      companyLogoUrl, page, baseRecipe, userInfo, hide,
      getBaseOrder, getPrintedQuantity,
      shouldPrintRecipes, shouldIncludePrep, prepProps,
    } = this.props;
    const {
      amount, measure, unit,
      format, batchSize, batchLabel,
      includeSubRecipes, breakPageAfterEachRecipe,
      includePhotoInRecipePrint,
    } = values;

    // update preference if change
    if (
      page === 'recipe' && (
        userInfo.getIn(['printPreference', 'printSubRecipePreferences']) !== includeSubRecipes
        || userInfo.getIn(['printPreference', 'recipePrintFormatPreference']) !== format
        || userInfo.getIn(['printPreference', 'includePhotoInRecipePrint']) !== includePhotoInRecipePrint
      )
    ) {
      const userUpdate = userInfo
        .setIn(['printPreference', 'printSubRecipePreferences'], includeSubRecipes)
        .setIn(['printPreference', 'recipePrintFormatPreference'], format)
        .setIn(['printPreference', 'includePhotoInRecipePrint'], includePhotoInRecipePrint);
      saveUserInfo(userUpdate).then(getUserInfo);
    } else if (
      page === 'order' && (
        userInfo.getIn(['printPreference', 'recipePrintFormatPreference']) !== format
        || userInfo.getIn(['printPreference', 'breakPageOnRecipePrint']) !== breakPageAfterEachRecipe
        || userInfo.getIn(['printPreference', 'includePhotoInRecipePrint']) !== includePhotoInRecipePrint
      )
    ) {
      const userUpdate = userInfo
        .setIn(['printPreference', 'recipePrintFormatPreference'], format)
        .setIn(['printPreference', 'breakPageOnRecipePrint'], breakPageAfterEachRecipe)
        .setIn(['printPreference', 'includePhotoInRecipePrint'], includePhotoInRecipePrint);
      saveUserInfo(userUpdate).then(getUserInfo);
    }

    let printedQuantity, printedBatches;
    if (getPrintedQuantity) {
      printedQuantity = getPrintedQuantity ? getPrintedQuantity(amount, measure, unit) : null;
      // assuming getPrintedQuantity preserves the unit in the measured case
      printedBatches = batchSize * printedQuantity.amount / amount;
    }
    const baseOrder = getBaseOrder(amount, measure, unit);

    const emptyContentText = `There are no ${shouldPrintRecipes ? page === 'recipe' ? 'Recipe ' : 'Recipes ' : ''}${shouldPrintRecipes && includeSubRecipes ? 'or ' : ''}` +
              `${includeSubRecipes ? 'Sub-Recipes ' : ''}to print.`;
    if (baseOrder.isEmpty()) {
      this.setState({ emptyContentText });
      return Promise.resolve();
    }

    let allUsedRecipeIds;
    const productionOrder = await getProductionOrder(baseOrder, true);
    const immProductionOrder = Immutable.fromJS(productionOrder).get('recipeList');
    const recipeList = immProductionOrder.filter(r => !r.get('isSimplePrep'));
    const simplePrepList = immProductionOrder.filter(r => r.get('isSimplePrep'));

    allUsedRecipeIds = recipeList.map(r => r.get('product').toString());
    const fetchUsedRecipes = getMultipleProductsDetails(allUsedRecipeIds);

    const allMaterialsIds = recipeList
        .flatMap(r => Immutable.fromJS(r.get('billOfMaterials').keySeq().toArray()))
        .filter(m => !allUsedRecipeIds.includes(m))
        .concat(
            simplePrepList.flatMap(r =>
                Immutable.fromJS(r.get('billOfMaterials').keySeq().toArray())
            ).toJS(),
        );
    const fetchUsedMaterials = getMultipleProductsUsageInfo(allMaterialsIds);

    await fetchUsedMaterials;
    const nestedRecipeDetails = await fetchUsedRecipes;

    const logoUrl = userInfo.get('logoUrl');
    let recipeImageUrlsWithId = [];
    let stepImageUrlsWithId = [];

    function setupRecipePhotos(recipe, recipeId) {

      if (recipe.photo) {
        recipeImageUrlsWithId.push({
          url: photoUrl(recipe.photo, logoUrl),
          id: `recipe-logo-${recipeId}`,
        });
      }
      recipe.recipe.steps.forEach((step, stepIdx) => {
        if (!_.isEmpty(step.photo)) {
          stepImageUrlsWithId.push({
            url: photoUrl(step.photo, logoUrl),
            id: `recipe-step-photo-${recipeId}-${stepIdx}`,
          });
        }
      });
    }

    if (page === 'recipe' && !includeSubRecipes) {

      if (includePhotoInRecipePrint) {
        setupRecipePhotos(baseRecipe, 0);
      }
      // we've fetched more than we need to, but fuck it, can't be better
      // without working async/await
      return Promise.resolve(
          this.printContent(
              this.recipeToContent(0, baseRecipe, printedQuantity, format, printedBatches || batchSize, batchLabel, breakPageAfterEachRecipe, includePhotoInRecipePrint, companyLogoUrl),
              companyLogoUrl,
              recipeImageUrlsWithId.concat(stepImageUrlsWithId)
          ),
      );
    }

    const recipeSort = (a, b) => {
      if (a.get('name') < b.get('name')) {
        return -1;
      } else {
        return 1;
      }
    };

    const baseOrderProductIds = baseOrder.map(i => i.get('product').toString());
    const recipesToPrint = [];
    if (shouldPrintRecipes) {
      if (page === 'recipe') {

        recipesToPrint.push({
          product: baseRecipe,
          printedQuantity, format, batchSize: printedBatches || batchSize,
          batchLabel, breakPageAfterEachRecipe, companyLogoUrl
        });
      } else {
        const recipesInOrder = Immutable // page is order but print recipe button clicked
            .fromJS(_.pickBy(nestedRecipeDetails, (v, k) => baseOrderProductIds.includes(k)))
            .map((r, i) => r.set('id', allUsedRecipeIds.get(i)))
            .sort(recipeSort);
        recipesInOrder
            .forEach(r => recipesToPrint.push({ product: r.toJS(), printedQuantity, format, batchSize, batchLabel, breakPageAfterEachRecipe, companyLogoUrl }))
      }
    }

    const subRecipesToPrint = [];
    if (includeSubRecipes) {
      const subRecipeList = Immutable.fromJS(page === 'order' ? _.pickBy(nestedRecipeDetails, (v, k) => !baseOrderProductIds.includes(k)) : nestedRecipeDetails)
          .map((r, i) => r.set('id', allUsedRecipeIds.get(i)))
          .sort(recipeSort);
      subRecipeList
          .forEach(r => subRecipesToPrint.push({ product: r.toJS(), printedQuantity, format, batchSize, batchLabel, breakPageAfterEachRecipe, companyLogoUrl }))
    }

    let contents = [];

    const hasPreps = productionOrder.recipeList
        .some(item => item.isSimplePrep);
    if (shouldIncludePrep && hasPreps) {
      contents.push(<PrintPreps key="preps" {...prepProps} />);
    }

    let recipesInOrder = [];
    if (page === 'order') {
      recipesInOrder = recipesInOrder.concat(subRecipesToPrint);
      recipesInOrder = recipesInOrder.concat(recipesToPrint);
    } else {
      recipesInOrder = recipesInOrder.concat(recipesToPrint);
      recipesInOrder = recipesInOrder.concat(subRecipesToPrint);
    }

    if (includePhotoInRecipePrint) {
      recipesInOrder.map(({product, }, idx) => setupRecipePhotos(product, idx))
    }

    contents = contents.concat(recipesInOrder.map(({ product, printedQuantity, format, batchSize, batchLabel, breakPageAfterEachRecipe, companyLogoUrl }, idx) =>
        this.recipeToContent(idx, product, printedQuantity, format, batchSize, batchLabel, breakPageAfterEachRecipe, includePhotoInRecipePrint, page === 'order' || idx === 0 ? companyLogoUrl : null),
    ));
    return contents.length < 1 ?
        this.setState({ emptyContentText })
        : this.printContent(contents, companyLogoUrl, includePhotoInRecipePrint && recipeImageUrlsWithId.concat(stepImageUrlsWithId))
            .then(page === 'order' ? hide : _.noop);
  }

  printContent(contentToPrint, companyLogoUrl, imagesToPrint) {

    const content = ReactDOMServer.renderToString(
      <div className = "ml-037">
        <body>
          <div className="logo">
            <img id="powered-by-logo" className="navbar-brand" />
          </div>
          {contentToPrint}
        </body>
      </div>
    );

    let imageUrlListWithId = [{url: "/assets/images/poweredby.png", id: "powered-by-logo"}];

    if (companyLogoUrl) {
      imageUrlListWithId.push({url: companyLogoUrl, className: "company-logo"})
    }

    if (!_.isEmpty(imagesToPrint)) {
      imageUrlListWithId = imageUrlListWithId.concat(imagesToPrint);
    }

    return printContent(content, imageUrlListWithId);
  }

  recipeToContent(idx, product, printedQuantity, format, batchSize, batchLabel, breakPage, includePhotos, companyLogoUrl) {

    const {
      allIngredients,
      detailedIngredients,
      productionOrder,
    } = this.props;

    return <RecipePrint
      idx={idx}
      product={product}
      printedQuantity={printedQuantity}
      format={format}
      includePhotos={includePhotos}
      breakPage={breakPage}
      companyLogoUrl={companyLogoUrl}
      batchSize={batchSize}
      batchLabel={batchLabel}

      allIngredients={allIngredients}
      detailedIngredients={detailedIngredients}
      productionOrder={productionOrder}
    />;
  }

  render() {

    const {
      title, page, hide, printRecipe, ingredient, userInfo, allMeasures, allIngredients, conversions, batchLabel, handleSubmit, submitting, amount, batchSize,
      shouldIncludePrep,
    } = this.props;
    const { amount: amountField, measure, unit, format, includeSubRecipes, batchSize: batchSizeField, breakPageAfterEachRecipe, includePhotoInRecipePrint } = this.props.fields;
    const { isReady } = this.state;

    const [sizeInputLabelWidth, sizeInputControlWidth, sizeInputControlWidthMd] = [3, 8, 8];
    const recipeSizeWidths = batchSizeField.value ?
      {xs: sizeInputControlWidth, md: sizeInputControlWidth, lg: sizeInputControlWidth}
      : {xs: 3, md: 3, lg: 3}

    let content = <LoadingSpinner />;

    if (this.state.emptyContentText) {
      content = <Row>
        <Col xs={12}>{this.state.emptyContentText}</Col>
      </Row>
    } else if(userInfo && allMeasures && allIngredients) {

      if (shouldIncludePrep && !isReady) {
        content = <LoadingSpinner />;
      } else {
        content = <Form className="row" id="print-recipe-modal">
        <Col md={8} xs={8}>
          { page === 'recipe' ?
            <Row controlId="recipe-size">
              <Col componentClass={ControlLabel} xs={sizeInputLabelWidth}>
                print:
              </Col>
              <Col {...recipeSizeWidths}>

                <RecipeSize
                  recipeBatchClassName="print-recipe-batch-size"
                  sizeRatio={amount && batchSize ? amount / batchSize : null}
                  batchSizeField={batchSizeField}
                  batchLabel={batchLabel}
                  autoAdjust={false}
                  useUnits={ingredient}
                  amountField={amountField}
                  measureField={measure}
                  unitField={unit}
                  measures={allMeasures}
                  conversions={conversions}
                  multiMeasure

                  amountInputRef={input => this.initialInput = input}
                />

              </Col>
            </Row>
            : null
          }
          <Row controlId="recipe-format" className="mt-1">
            <Col componentClass={ControlLabel} xs={sizeInputLabelWidth}>
              format:
            </Col>
            <Col xs={sizeInputControlWidth} md={sizeInputControlWidthMd} lg={sizeInputControlWidth}>

              <Select
                  autosize={false}
                  options={[{ value: 'step-by-step', label: 'Step by Step' },
                            { value: 'ingredients-first', label: 'Ingredients First' }]}
                  {...format}
              />
            </Col>
          </Row>
          <Row xs={sizeInputControlWidth} md={sizeInputControlWidthMd} lg={sizeInputControlWidth} className="mt-1">
            <Validate {...includePhotoInRecipePrint}>
              <Checkbox {...includePhotoInRecipePrint}>
                Include Photos
              </Checkbox>
            </Validate>
          </Row>
          { page === 'recipe' ?
            <Row xs={sizeInputControlWidth} md={sizeInputControlWidthMd} lg={sizeInputControlWidth} className="mt-1">
              <Validate {...includeSubRecipes}>
                <Checkbox {...includeSubRecipes}>
                  Include Sub Recipes
                </Checkbox>
              </Validate>
            </Row>
            : null
          }
          { page === 'order' ?
            <Row xs={sizeInputControlWidth} md={sizeInputControlWidth} lg={sizeInputControlWidth} className="mt-1">
              <Validate {...breakPageAfterEachRecipe}>
                <Checkbox {...breakPageAfterEachRecipe}>
                  Print Each Recipe on a Separate Page
                </Checkbox>
              </Validate>
            </Row>
            : null
          }
        </Col>
        <Col>
          <Button type="submit"
              disabled={submitting || this.state.emptyContentText}
              onClick={handleSubmit(this.printRecipes.bind(this))} style={{ 'float': 'right' }}>
            {page === 'order' ? 'Print' : 'Print Recipe'}
          </Button>
        </Col>
      </Form>
      }
    }

    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>{title || "Print Recipe"}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {content}
      </Modal.Body>
      <Modal.Footer />
    </Modal>;
  }

  componentWillMount() {
    const { page, shouldIncludePrep, prepProps } = this.props;

    getUserInfo();

    if (page === 'order') {

      getIngredientList();
    }

    // same as PrintPreps#componentWillMount
    if (shouldIncludePrep) {
      PrintPreps.loadData(prepProps);

      if (PrintPreps.readyToPrint(prepProps)) {
        this.setState({isReady: true});
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { page, shouldIncludePrep, prepProps: thisPrepProps } = this.props;
    const { prepProps: nextPrepProps } = nextProps;
    const { isReady } = this.state;

    // same as PrintPreps#componentWillReceiveProps
    if (shouldIncludePrep) {
      PrintPreps.checkProps(thisPrepProps, nextPrepProps);

      if (PrintPreps.readyToPrint(nextPrepProps) && !isReady) {
        this.setState({isReady: true});
      }
    }
  }

  componentDidMount() {
    if (this.initialInput && this.props.allMeasures) {
      this.initialInput.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.initialInput && !prevProps.allMeasures && this.props.allMeasures) {
      this.initialInput.focus();
    }
  }
}
