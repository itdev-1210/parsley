import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Modal, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import TagSelector from '../TagSelector';
import { addToCategory, getTagsList } from '../../webapi/endpoints';
import { allTagTypes } from '../../utils/tags';

@reduxForm(
  {
    form: 'add-to-category',
    fields: [
      'tags',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: { tags: [] },
    // while we can use the same `tagType` to get targets for both ingredients & recipes,
    // we have to use targetType === 'inventoryItems'` to get targets and
    // use `tagType === 'ingredients'` for inventory items.
    targets: state.get(ownProps.targetType || ownProps.tagType).filter(i => ownProps.targetIds.has(i.get('id'))),
  })
)
export default class AddToCategoryModal extends Component {

  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit(values) {
    const { hide, tagType, targetIds } = this.props;
    return addToCategory(tagType, targetIds, values.tags)
      .then(() => hide())
      .then(() => getTagsList(tagType))
      .then(() => values.tags);
  }

  render() {

    const { hide, tagType, targets, handleSubmit, submitting } = this.props;
    const { tags } = this.props.fields;

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Add to {allTagTypes[tagType].singularName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p style={{ fontWeight: 'bold' }}>Add</p>
          <ListGroup className="green-barred-list-group">
            {targets.map(target =>
              <ListGroupItem key={target.get('name')}>{target.get('name')}</ListGroupItem>
            )}
          </ListGroup>
          <p style={{ fontWeight: 'bold' }}>to the following {allTagTypes[tagType].pluralName}</p>
          <TagSelector
            {...tags}
            tagType={tagType}
          />
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            <Button onClick={hide} className="btn-danger">Cancel</Button>
            <Button bsStyle="primary" onClick={handleSubmit(this.submit)} disabled={tags.value.length <= 0 || submitting}>Save</Button>
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }
}
