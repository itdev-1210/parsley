import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Alert, Col, Row, Form, ControlLabel, Button, Modal } from 'react-bootstrap';
import { shallowToObject } from '../../utils/form-utils';
import { checkFields, required, optional, positive } from '../../utils/validation-utils';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { RecipeSize } from '../pages/RecipeEdit';

@reduxForm(
    {
      form: 'resizeModalForm',
      fields: [
        'amount',
        'measure',
        'unit',
        'batchSize',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: (values) => checkFields(values,
          {
            amount: required(positive),
            measure: optional(positive),
            unit: optional(positive),
            batchSize: optional(),
          }),
    },
    (state, ownProps) => ({
      initialValues: {
        'amount': ownProps.amount,
        'measure': ownProps.measure,
        'unit': ownProps.unit,
        'batchSize': ownProps.batchSize,
      },
    }),
)
export default class ResizeRecipeModal extends Component {
  render() {

    const { hide, resizeRecipe, isQuantized, ingredient, allMeasures, conversions, isSubRecipe, batchLabel, readonlyUser, amount, batchSize } = this.props;
    const { amount: amountField, measure, unit, batchSize: batchSizeField } = this.props.fields;

    const [sizeInputLabelWidth, sizeInputControlWidth, sizeInputControlWidthMd] = [4, 6, 6];
    const sizeQuantity = shallowToObject(this.props.fields);

    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>Scale Recipe</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { isQuantized
            ? <Alert>Resizing this recipe will also change its basic unit.</Alert>
            : null }
        { allMeasures ?
            <Form id="resize-recipe-form">
              <Col md={6}>
                <Row controlId="recipe-size">
                  <Col componentClass={ControlLabel} xs={sizeInputLabelWidth}>
                    size:
                  </Col>
                  <Col xs={sizeInputControlWidth} md={sizeInputControlWidthMd} lg={sizeInputControlWidth}>

                    <RecipeSize
                      recipeBatchClassName="resize-recipe-batch-size"
                      sizeRatio={amount && batchSize ? amountField.initialValue / batchSizeField.initialValue : null}
                      batchSizeField={batchSizeField}
                      batchLabel={batchLabel}
                      isSubRecipe={isSubRecipe}
                      autoAdjust={false}
                      useUnits={ingredient}
                      amountField={amountField}
                      measureField={measure}
                      unitField={unit}
                      measures={allMeasures}
                      conversions={conversions}
                      multiMeasure

                      amountInputRef={input => this.initialInput = input}
                    />

                  </Col>
                </Row>
              </Col>
            </Form>
          : <LoadingSpinner /> }
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={hide} className="btn-danger">
          Cancel
        </Button>
        <Button
            type="submit" form="resize-recipe-form"
            bsStyle="primary"
            onClick={e => {
              e.preventDefault();
              resizeRecipe(sizeQuantity, batchSizeField.value);
            }}
        >
          Apply
        </Button>
      </Modal.Footer>
    </Modal>;
  }

  componentDidMount() {
    if (this.initialInput && this.props.allMeasures) {
      this.initialInput.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.initialInput && !prevProps.allMeasures && this.props.allMeasures) {
      this.initialInput.focus();
    }
  }
}
