import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import { Button, Modal } from 'react-bootstrap';
import NutritionDisplay from '../NutritionDisplay';
import { printContent } from '../../utils/printing';

export default class PrintRecipeNutritionModal extends Component {

  constructor(props) {
    super(props);
    this.updateIngredientListStr = this.updateIngredientListStr.bind(this);
    this.printNutrition = this.printNutrition.bind(this);
  }

  updateIngredientListStr(ev) {
    this.editedIngredientListStr = ev.target.value;
  }

  printNutrition() {

    const {
      companyLogoUrl, allNutrients, detailedIngredients, recipeNutrients,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      name, recipe, measures, conversions,
      isSubRecipeInRecipeEdit,
    } = this.props;

    const content = ReactDOMServer.renderToString(
        <html>
        <head>
        </head>
        <body>
        <br/>
        <div className="logo">
          <img id="powered-by-logo" className="navbar-brand" />
        </div>
        { companyLogoUrl ?
        <div className="gray-label">
          <img id="company-logo" />
        </div> : null }
        <div id="nutrition-print-preview" style={companyLogoUrl ? {paddingTop: '85px'} : {}}>
          <h1 style={{ fontSize: '30px', paddingBottom: '12px' }}>
            Nutrition Facts for {name}
          </h1>
          <NutritionDisplay
            productType="recipes"
            allNutrients={allNutrients}
            isSubRecipeInRecipeEdit={isSubRecipeInRecipeEdit}
            detailedIngredients={detailedIngredients}
            conversions={conversions}
            measures={measures}
            recipeNutrients={recipeNutrients}
            nutrientServingAmount={nutrientServingAmount}
            nutrientServingMeasure={nutrientServingMeasure}
            nutrientServingUnit={nutrientServingUnit}
            recipe={recipe}
            printMedia
            editedIngredientListStr={this.editedIngredientListStr}
          />
        </div>
        </body>
        </html>
    );

    const imageUrlListWithId = [{url: "/assets/images/poweredby.png", id: "powered-by-logo"}];

    if (companyLogoUrl) {
      imageUrlListWithId.push({url: companyLogoUrl, id: "company-logo"})
    }

    return printContent(content, imageUrlListWithId);
  }

  render() {

    const {
      hide, allNutrients, detailedIngredients, recipeNutrients,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      recipe, measures, conversions, name,
      isSubRecipeInRecipeEdit,
    } = this.props;

    return <Modal show onHide={hide} className="wider-modal">
      <Modal.Header closeButton>
        <Modal.Title>Print Nutrition Info for {name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div style={{ textAlign: 'right' }}>
          <Button type="submit" onClick={this.printNutrition}>
            Print
          </Button>
        </div>
        <NutritionDisplay
          productType="recipes"
          inPrintModal
          allNutrients={allNutrients}
          isSubRecipeInRecipeEdit={isSubRecipeInRecipeEdit}
          detailedIngredients={detailedIngredients}
          conversions={conversions}
          measures={measures}
          recipeNutrients={recipeNutrients}
          nutrientServingAmount={nutrientServingAmount}
          nutrientServingMeasure={nutrientServingMeasure}
          nutrientServingUnit={nutrientServingUnit}
          recipe={recipe}
          updateIngredientListStr={this.updateIngredientListStr}
        />
      </Modal.Body>
      <Modal.Footer />
    </Modal>;
  }
}
