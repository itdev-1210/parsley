// @flow
import Immutable from 'immutable';
import moment from 'moment';
import _ from 'lodash';
import React, { Component } from 'react';
import { Modal, Col } from 'react-bootstrap';
import { getRecipesAddedChanged } from '../../webapi/endpoints';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { dateFormat } from 'common/utils/constants';

type Props = {
  recipes?: Immutable.List<Immutable.Map<string, any>>,
  hide: Function,
}

@connect(
  state => ({
    recipes: state.get('recipesAddedChanged'),
  }),
)
export default class RecipesDashboardModal extends Component<Props> {

  render() {
    const { recipes, hide } = this.props;

    return <div style={{ position: 'relative' }}><Modal show onHide={hide} className="medium-modal">
      <Modal.Header closeButton>
        <Modal.Title>Recipes Added or Changed</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ position: 'initial' }}>
        <Col md={12}>
          <table className="recipes-dashboard-modal-table" cellSpacing="20" cellPadding="0" border="0" style={{ width: '100%' }}>
            <thead>
              <tr>
                <th scope="col"><span>Recipe</span></th>
                <th scope="col">Status</th>
                <th scope="col">By</th>
                <th scope="col">Date</th>
              </tr>
            </thead>
            <tbody> {
              recipes &&
              <React.Fragment>
                {
                  recipes.map(recipe =>
                    <tr key={recipe.get('id')}>
                      <td>
                        <Link className="list-item-main-link" to={{
                          // $FlowFixMe: only with records can we assert member existence/value
                          pathname: `/recipes/${recipe.get('id')}`,
                        }}>
                          {recipe.get('name')}
                        </Link>
                      </td>
                      <td>{recipe.get('type')}</td>
                      <td>{recipe.get('by')}</td>
                      <td>{moment(recipe.get('date'), moment.ISO_8601).format(dateFormat)}</td>
                    </tr>,
                  )
                }
              </React.Fragment>
            }
            </tbody>
          </table>
        </Col>
      </Modal.Body>
      <Modal.Footer/>
    </Modal></div>;
  }

  componentDidMount() {
    getRecipesAddedChanged();
  }
}
