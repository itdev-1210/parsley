// @flow
import React, { Component, type ElementRef } from 'react';
import Immutable from 'immutable';
import { Modal, Row, Col, Button } from 'react-bootstrap';
import Validate from 'common/ui-components/Validate';
import Select from '../Select';
import { checkFields, required } from '../../utils/validation-utils';
import { reduxForm } from 'redux-form';
import { updateItemNumber } from '../../webapi/endpoints';

type Props = {
  hide: Function,
  unknowRecipes: Array<*>,
  recipes: Immutable.List<Immutable.Map<string, any>>,
}

type State = {
  added: Array<*>,
  indexRecipe: number,
}

export default class AddCodeToPosImportModal extends Component<Props, State> {
  itemCodeToRecipeId: Immutable.Map<string, string>;
  skippedCodes: Immutable.Set<string>

  constructor(props: Props) {
    super(props);
    this.state = {
      skipped: [],
      added: [],
      indexRecipe: 0,
    };
    this.itemCodeToRecipeId = Immutable.Map();
    this.skippedCodes = Immutable.Set();
  }

  showNextRecipe = (skip: boolean = false, form: Object) => {
    const { indexRecipe, added } = this.state;
    const { unknowRecipes } = this.props;
    let newAdded = added;

    if (skip) {
      this.skippedCodes = this.skippedCodes.add(unknowRecipes[indexRecipe].itemCode);
    } else {
      this.itemCodeToRecipeId = this.itemCodeToRecipeId.set(form.recipeCode, form.recipeId);
      newAdded.push({ ...unknowRecipes[indexRecipe], id: form.recipeId });
      updateItemNumber(form.recipeId, form.recipeCode);
    }

    const [newIndex, addedSkipped] = this.getNextIndexRecipe(indexRecipe + 1);
    if (newIndex !== -1) {
      this.setState({
        indexRecipe: newIndex,
        added: [...newAdded, ...addedSkipped],
      });
    } else {
      this.props.hide([...newAdded, ...addedSkipped]);
    }
  }

  getNextIndexRecipe = (currentIndex: number, addedSkipped: Array<*> = []) => {
    const { unknowRecipes } = this.props;
    if (currentIndex < unknowRecipes.length) {
      const currentRow = unknowRecipes[currentIndex];
      const maybeRecipeId = this.itemCodeToRecipeId.get(currentRow.itemCode);
      const skip = this.skippedCodes.includes(currentRow.itemCode);
      if (skip)
        return this.getNextIndexRecipe(currentIndex + 1, addedSkipped);
      else if (maybeRecipeId)
        return this.getNextIndexRecipe(currentIndex + 1, [...addedSkipped, { ...currentRow, id: maybeRecipeId }]);
      else
        return [currentIndex, addedSkipped];
    } else return [-1, addedSkipped];
  }

  render() {
    const { hide, recipes, unknowRecipes } = this.props;
    const { indexRecipe, added } = this.state;
    const recipe = unknowRecipes[indexRecipe];
    const recipesChecked = [...added].map(d => d.id);
    return <Modal show onHide={() => hide(added)} className="medium-modal add-code-pos-import-modal">
      <Modal.Header closeButton>
        <Modal.Title>New item number</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Row>
            <Col xs={4}>
              <h5>Item Name</h5>
            </Col>
            <Col xs={4}>
              <h5>Item Number</h5>
            </Col>
            <Col xs={4}>
              <h5>Parsley Recipe Name</h5>
            </Col>
          </Row>
          <Row>
            <AddCodeToPosImportRow
              {...recipe}
              recipes={recipes
                .filter(r => !recipesChecked.includes(r.get('id')) && !r.get('ingredient') && !r.get('tombstone'))
              }
              showNextRecipe={this.showNextRecipe.bind(this)}
            />
          </Row>
        </div>
      </Modal.Body>
      <Modal.Footer>
      </Modal.Footer>
    </Modal>;
  }
}


type AddCodeToPosImportRowProps = {
  fields: any,
  handleSubmit: Function,
  showNextRecipe: Function,
  recipes: Immutable.List<Immutable.Map<string, any>>,
}

@reduxForm(
  {
    form: 'pos-import-modal',
    fields: [
      'recipeId',
      'recipeCode',
      'recipeName',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate: (values) => {
      const posValidators = {
        recipeId: required(),
      };
      return checkFields(values, posValidators);
    },
  }, (state, ownProps) => ({
    initialValues: {
      recipeCode: ownProps.itemCode,
      recipeName: ownProps.name,
      recipeId: undefined,
    },
    overwriteOnInitialValuesChange: true,
  }),
)
class AddCodeToPosImportRow extends Component<AddCodeToPosImportRowProps> {
  initialInput: ElementRef<*>;

  render() {
    const { recipes, fields, handleSubmit, showNextRecipe } = this.props;
    const { recipeCode, recipeName, recipeId } = fields;
    const recipesOption = recipes
      .filter(r => !r.get('itemNumber')).map(r => ({ value: r.get('id'), label: r.get('name') })).toJS();
    return <div>
      <Row>
        <Col xs={4}>
          {recipeName.value}
        </Col>
        <Col xs={4}>
          {recipeCode.value}
        </Col>
        <Col xs={4}>
          <Validate {...recipeId}>
            <Select
              autoFocus
              inputRef={ref => this.initialInput = ref}
              key={recipeCode.value}
              {...recipeId}
              options={recipesOption}
            />
          </Validate>
        </Col>
      </Row>
      <br/>
      <Row>
        <Col xs={12}>
          <Button
            bsStyle="primary"
            style={{ float: 'left' }}
            onClick={() => {
              showNextRecipe(true);
            }}>SKIP</Button>
          <Button
            bsStyle="primary"
            style={{ float: 'right' }}
            onClick={handleSubmit((form) => {
              showNextRecipe(false, form);
            })}>ADD</Button>
        </Col>
      </Row>
    </div>;
  }
}

