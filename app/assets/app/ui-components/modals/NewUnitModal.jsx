import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import Section from '../Section';
import ConversionEditor from '../ConversionEditor';
import { getProductUsageInfo, getProductDetails, saveProduct, getMeasures, getNutrients } from '../../webapi/endpoints';
import { deepArrayField, deepObjectField, checkFields, required, positive } from '../../utils/validation-utils';
import { Modal, Button } from 'react-bootstrap';
import { convertProductFormValuesToJSON, convertProductJSONToFormValues } from '../../utils/form-utils';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Immutable from 'immutable';
import _ from 'lodash';
import CrudEditor from '../pages/helpers/CrudEditor';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import type { SelectOption } from '../Select';

function validate(values) {
  return checkFields(values, {
    measures: deepArrayField(deepObjectField({
      measure: required(),
      preferredUnit: required(),
      conversionMeasure: required(),
      conversionUnit: required(),
      conversion: required(positive)
    }))
  });
}

export type NewUnitDescriptor = SelectOption & {
  productId: number,
  currentMeasure: number,
  setUnit: (newMeasure: number, newUnit: number) => void,
}

@reduxForm(
    {
      form: 'quickConversionEdit',
      fields: [
        'measures[].conversion',
        'measures[].conversionMeasure',
        'measures[].conversionUnit',
        'measures[].amount',
        'measures[].measure',
        'measures[].preferredUnit',
        'measures[].customName',
      ],
      validate,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    },
    (state, ownProps) => ({
      allMeasures: state.getIn(['measures', 'measures']),
      initialValues: state.getIn(['productDetails', ownProps.newUnit.productId]),
      productDetails: CrudEditor.initialValues(
        state.getIn(['productDetails', ownProps.newUnit.productId]),
        { allNutrients: state.get('nutrients') },
        convertProductJSONToFormValues
      ),
      uiState: state.get(ownProps.uiStateName)
    })
)
export default class NewUnitModal extends Component {

  submit = deets => {
    this.props.productDetails.measures = deets.measures;
    saveProduct(this.props.newUnit.productId, convertProductFormValuesToJSON(this.props.productDetails))
      .then(() => getProductUsageInfo(this.props.newUnit.productId))
      .then(() => this.props.hide()) // this needs to be before getProductDetails() call. why? who knows
      .then(() => getProductDetails(this.props.newUnit.productId))
      .then(() => {
        // give the components some time to update their conversions
        setTimeout(() => {
          const lastMeasureInList = _.last(deets.measures);
          this.props.uiState.get('showNewUnitModal').setUnit(lastMeasureInList.measure, lastMeasureInList.preferredUnit);
        }, 120);
      });
  }

  usedMeasuresWithMessages() {
    const sources = Immutable.List(this.props.productDetails.sources);
    let res;
    if (sources) {
      res = Immutable.Map(
              sources
                  .map(s => s.measure)
                  .toSet() // remove duplicates, discard original indexes
                  .map(measureId => [measureId, 'supplier/s']));
    } else {
      res = Immutable.Map();
    }
    const currentMeasure = this.props.uiState.get('showNewUnitModal').currentMeasure;
    if (currentMeasure) res = res.set(currentMeasure, 'supplier/s');
    return res;
  }

  render() {
    const { allMeasures, hide } = this.props;
    const { measures } = this.props.fields;

    return (
      <Modal show onHide={hide} className='wider-modal'>
        <Modal.Header closeButton>
          <Modal.Title>Add New Unit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Section title="Measurement Conversions">
            { allMeasures && this.props.productDetails ?
                <FieldArray
                    component={ConversionEditor}
                    fields={measures}
                    props={{
                      inModal: true,
                      uniqueName: 'ingredient-edit-conversion-editor-main',
                      usedMeasures: this.usedMeasuresWithMessages(),
                      allMeasures,
                    }}
                /> : <LoadingSpinner /> }
          </Section>
          <Button type='submit'
                  onClick={this.props.handleSubmit(this.submit)}
                  bsStyle='primary'
                  className='green-btn pull-right'>
            save
          </Button>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>
    );
  }

  componentWillMount() {
    getMeasures();
    getNutrients();
    getProductDetails(this.props.newUnit.productId);
  }
}
