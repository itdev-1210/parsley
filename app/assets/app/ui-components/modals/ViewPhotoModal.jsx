import React, { Component } from 'react';

import ReactPropTypes from 'prop-types'
import { Button, Modal } from 'react-bootstrap';

export default class ViewPhotoModal extends Component {
  static propTypes = {
    onHide: ReactPropTypes.func.isRequired,
    onDelete: ReactPropTypes.func.isRequired,
    photoURL: ReactPropTypes.string.isRequired,
    isHidden: ReactPropTypes.bool,
    readonlyUser: ReactPropTypes.bool,
    previewPhotoDistanceFromTop: ReactPropTypes.number,
    matchPreviewPhotos: ReactPropTypes.string,
  };

  static defaultProps = {
    isHidden: false,
    readonlyUser: false,
    previewPhotoDistanceFromTop: 0,
    matchPreviewPhotos: '',
  }

  render() {
    const {
      onHide, photoURL, onDelete, isHidden,
      previewPhotoDistanceFromTop, matchPreviewPhotos, readonlyUser,
    } = this.props;

    const display = isHidden ? 'none' : 'block';
    let top;
    if (previewPhotoDistanceFromTop) {
      if (matchPreviewPhotos === 'top') {
        top = previewPhotoDistanceFromTop;
      } else if (matchPreviewPhotos === 'bottom') {
        top = previewPhotoDistanceFromTop - 417; // - (~modal's height, which is actually fixed)
      }
    }
    top = top ? `${top}px` : '';

    return <Modal show onHide={onHide} style={{ display, top }} className={'view-photo-modal-match-' + matchPreviewPhotos}>
      <Modal.Header closeButton/>
      <Modal.Body>
        <img className="preview-photo-modal"
          src={photoURL}
          height="300px"
        />
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={onHide}>
          cancel
        </Button>
        {!readonlyUser ?
          <Button bsStyle="danger" onClick={onDelete}>
            remove
          </Button>
        : null}
      </Modal.Footer>
    </Modal>;
  }
}
