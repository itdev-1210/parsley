import Immutable from "immutable";
import _ from "lodash";
import React, { Component } from 'react';
import { Modal, Col, Button } from 'react-bootstrap';
import EditSupplierEmail from '../EditSupplierEmail';
import { purchasingRound } from '../../utils/unit-conversions';
import { createPurchase } from '../../webapi/endpoints';
import { defaultComparator } from 'common/utils/sorting';

export default class SendModal extends Component {

  static defaultProps = {
    afterModalSent: _.noop,
    isEmailSent: false,
    orders: [],
  };

  render() {
    const props = this.props;

    return <div style={{ position: 'relative' }}><Modal show onHide={props.hide} className='medium-modal'>
      <Modal.Header closeButton>
        <Modal.Title>Email Purchase Order</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ position: 'initial' }}>
        <EditSupplierEmail
          hide={props.hide}
          sendEmail={() => createPurchase(props.supplierId, props.orders, props.orderDeets, "email").then(() => props.afterModalSent())}
          isEmailSent={props.isEmailSent}
          supplierId={props.supplierId} />

        <br />
        <hr />
        <Col md={12}>
          <table cellSpacing='20' cellPadding='0' border='0'>
            {/* this markup should match the actually-sent version in Suppliers.scala */}
          <thead>
            <tr>
              <th width='220' scope='col' style={{ textAlign: 'left' }}>Item</th>
              <th width='80' scope='col' style={{ textAlign: 'left' }}>Quantity</th>
              <th width='160' scope='col' style={{ textAlign: 'left' }}>Unit</th>
              <th width='150' scope='col' style={{ textAlign: 'left' }}>SKU</th>
            </tr>
          </thead>
          <tbody>
            { props.orderDeets
              .sort((a, b) => (a.display.name && b.display.name ? defaultComparator(a.display.name.toLowerCase(), b.display.name.toLowerCase()) : 0))
              .map((item, idx) => (
                <tr key={idx}>
                  <td style={{ textAlign: 'left' }}>{item.display.name}</td>
                  <td style={{ textAlign: 'left' }}>{purchasingRound(item.display.numPackages, 2)}</td>
                  <td style={{ textAlign: 'left' }}>{item.display.purchasePackageName}</td>
                  <td style={{ textAlign: 'left' }}>{item.display.sku}</td>
                </tr>
              ))}
          </tbody>
          </table>
        </Col>
      </Modal.Body>
      <Modal.Footer/>
    </Modal></div>;
  }
}
