import React, { Component } from 'react';
import _ from 'lodash';
import { reduxForm } from 'redux-form';
import { Form, Button, Modal } from 'react-bootstrap';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { connect } from 'react-redux';
import { newIngredientTemplate, validate as validateIngredient } from '../pages/IngredientEdit';
import Immutable from 'immutable';
import CrudEditor from '../pages/helpers/CrudEditor';
import { bindActionCreators } from 'redux';

import { actionCreators as ProductActions } from '../../reducers/productDetails';
import {
  getSupplierList,
  getProductDetails,
  getProductUsageInfo,
  getMultipleProductsUsageInfo,
  saveProductSources,
  getNutrients,
} from '../../webapi/endpoints';
import {
  convertProductFormValuesToJSON, productFormFields,
  convertProductJSONToFormValues,
  shallowToObject,
} from '../../utils/form-utils';
import { getLowestCostSourceId } from '../../utils/packaging';
import SourceEditor from '../SourceEditor';
import Checkbox from '../Checkbox';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { noticeError } from 'common/utils/errorAnalytics';

function validate(values) {
  // don't show errors for things the user can't do anything about
  // we'll get noisy bad errors if the API complains, but at least the user will
  // know to contact us to report the bug
  return _.pick(validateIngredient(values), 'sources');
}

export const setLowestSourcePreferred = (sources, conversions) => {
  const sourceIsUsable = (s =>
          _.get(s, 'cost.valid') &&
          _.get(s, 'packageSize.valid') &&
          _.get(s, 'unit.valid') &&
          _.get(s, 'superPackageSize.valid')
  );
  const lowestCostSourceId = getLowestCostSourceId(
    Immutable.fromJS(sources.filter(sourceIsUsable).map(shallowToObject)),
    conversions,
  );
  lowestCostSourceId && sources.find(s => s.id.value === lowestCostSourceId).preferred.onChange(true);
  lowestCostSourceId && sources.filter(s => s.id.value !== lowestCostSourceId).forEach(s => s.preferred.onChange(false));
}

export const handleLowestPricePreferred = (ev, lowestPricePreferred, sources, conversions) => {
  const isChecked = Boolean(ev.target.checked);
  if (isChecked) {
    setLowestSourcePreferred(sources, conversions);
  }
  lowestPricePreferred.onChange(isChecked);
}

@reduxForm(
    {
      form: 'ingredientEditForm',

      fields: productFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
    },
    (state, ownProps) => {
      const productDetails = state.getIn(['productDetails', ownProps.ingredientId]);
      const allNutrients = state.get('nutrients') || Immutable.List();
      const initialValues = ownProps.ingredientId
          ? CrudEditor.initialValues(
              state.getIn(['productDetails', ownProps.ingredientId]),
              { allNutrients },
              convertProductJSONToFormValues)
          : newIngredientTemplate.toJS();

      // for ingredients with no existing source,
      // the user can pre-enter a source before opening this modal;
      // like in InventoryDetails.jsx's TakeInventory
      if (ownProps.preEnteredSource) {
        initialValues.sources = [ownProps.preEnteredSource];
      }

      return {
        initialValues,
        productDetails,
        mainInfoLoaded: Boolean(state.getIn(['productDetails', ownProps.ingredientId])),
        allMeasures: state.getIn(['measures', 'measures']),
        allSuppliers: state.get('suppliers'),
        userVolumePref: state.getIn(['userInfo', 'purchaseVolumeSystem']),
        userWeightPref: state.getIn(['userInfo', 'purchaseWeightSystem']),
      };
    },
    dispatch => ({
      productActions: bindActionCreators(ProductActions, dispatch),
    }),
)
class IngredientSourcesForm extends Component {

  submit = (deets) => {
    const { ingredientId, hide, postSave } = this.props;
    const { info: { lowestPricePreferred }, purchaseInfo } = convertProductFormValuesToJSON(deets);
    saveProductSources(ingredientId, { lowestPricePreferred, purchaseInfo })
      .then(() => hide())
      .then(() => {
        if (typeof postSave === 'function') {
          postSave(ingredientId);
        } else {
          getProductUsageInfo(ingredientId);
          getMultipleProductsUsageInfo(deets.simplePreps.map(prep => prep.outputProduct));
        }
      });
  };

  render() {

    const {
      sources,
      info: { lowestPricePreferred },
    } = this.props.fields;
    const {
      ingredientId,
      allSuppliers, allMeasures,
      values: { measures },
      handleSubmit, submitting, untouch, isHidden,
      userVolumePref, userWeightPref
    } = this.props;

    const conversions = Immutable.fromJS(measures);

    const conversionsReady = (!ingredientId || conversions.size > 0) && Boolean(allMeasures);
    const sourcesReady = conversionsReady && conversions.size > 0 && Boolean(allSuppliers);

     return sourcesReady ?
         <Form>
           <Checkbox
            {...lowestPricePreferred}
            onChange={ev => handleLowestPricePreferred(
              ev,
              lowestPricePreferred,
              sources,
              conversions
            )}
            style={{marginBottom: '10px'}}
            >
             Select the lowest Cost Option
           </Checkbox>
           <FieldArray component={SourceEditor}
                       fields={sources}
                       props={{
                         isHidden: isHidden,
                         startWithRow: true,
                         uniqueName: 'recipe-edit-ingredient-modal-source-editor',
                         conversions,
                         suppliers: allSuppliers,
                         measures: allMeasures,
                         untouch,
                         userVolumePref,
                         userWeightPref,
                         isSelectable: !lowestPricePreferred.value,
                         isLowestPricePreferred: lowestPricePreferred.value,
                       }}
           />
           <Button
               type="submit"
               disabled={submitting}
               bsStyle={submitting ? 'default' : 'primary'}
               onClick={handleSubmit(this.submit)}
               className="pull-right green-btn"
           >
             {submitting ? 'saving' : 'save'}
           </Button>
         </Form> :
         <LoadingSpinner />;
  }

  componentWillMount() {
    getSupplierList();
    getNutrients();
  }
}

@connect(
    (state, ownProps) => ({
      productDetails: state.getIn(['productDetails', ownProps.ingredientId]),
    })
)
export default class IngredientSourcesModal extends Component {
  render() {

    const {
      ingredientId, hide, productDetails, isHidden, preEnteredSource, postSave,
    } = this.props;

    const display = isHidden ? 'none' : 'block';
    if (productDetails && !productDetails.name) {
      noticeError(`trying to modify hidden ingredient ${ingredientId}`);
      hide();
    }

    return <Modal show onHide={hide} className="wider-modal" style={{ display }}>
      <Modal.Header closeButton>
        <Modal.Title>{ productDetails ? 'Edit ' + productDetails.name : 'Loading...'}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { productDetails ? <IngredientSourcesForm
          ingredientId={ingredientId}
          isHidden={isHidden}
          hide={hide}
          preEnteredSource={preEnteredSource}
          postSave={postSave}
        /> : null }
      </Modal.Body>
      <Modal.Footer />
    </Modal>;
  }

  componentDidMount() {
    if (!this.props.productDetails)
      getProductDetails(this.props.ingredientId);
    else this.setState({ 'ingredientReady': true });
  }
}
