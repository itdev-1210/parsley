import React, { Component } from 'react';
import ReactDOMServer from 'react-dom/server';
import ReactPropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import Immutable from 'immutable';
import _ from 'lodash';

import { Row, Col, Modal } from 'react-bootstrap';

import Validate from 'common/ui-components/Validate';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { CostBreakdown, assembleItemInfo } from './CostBreakdown';
import NutritionDisplay from '../NutritionDisplay';
import RecipeEdit from '../pages/RecipeEdit';
import RecipePrint from '../prints/RecipePrint';
import PrintButton from '../PrintButton';
import Checkbox from '../Checkbox';
import Select from '../Select';

import { toPercent } from '../PercentInput';

import { printContent, photoUrl } from '../../utils/printing';
import { getRecipeBaseOrder } from '../../utils/recipe-utils';
import { convertShoppingListJSONToFormValues, quantityFormFields } from '../../utils/form-utils';
import { featureIsSupported } from '../../utils/features';

import { actionCreators as ProductActions } from '../../reducers/productDetails';
import {
  getMeasures,
  getNutrients,
  getUserInfo,
  saveUserInfo,
  getProductDetails,
  getProductionOrder,
  getIngredientList,
  getMultipleProductsUsageInfo,
  getMultipleProductsDetails,
  getSupplierSourcedIngredients,
  receivePurchase,
} from '../../webapi/endpoints';
import { addSideEffect, transformErrorValue } from 'common/utils/promises';

@reduxForm(
  {
    form: 'printRecipesInfoForm',
    fields: [
      'format',
      'includeNutrition',
      'includeCostBreakdown',
      'breakPageOnRecipePrint',
      'includePhotoInRecipePrint',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const recipeIdList = ownProps.recipeIdList;
    const loadedProductDetails = state.get('productDetails');
    const userInfo = state.get('userInfo');
    const isAllProductsLoaded = recipeIdList.isSubset(loadedProductDetails.keySeq().toSet());
    return {
      userInfo,
      mainInfoLoaded: true,
      allMeasures: state.getIn(['measures', 'measures']),
      recipesToPrint: loadedProductDetails
        .filter((v, k) => recipeIdList.contains(k))
        .map((v, k) => ({ ...v, id: k }))
        .toList(),
      initialValues: {
        format: userInfo.getIn(['printPreference', 'recipePrintFormatPreference']),
        includeNutrition: featureIsSupported('NUTRITIONAL_INFO') ? userInfo.getIn(['printPreference', 'includeNutritionInMultipleRecipePrint']) : false,
        includeCostBreakdown: userInfo.getIn(['printPreference', 'includeCostBreakdownInMultipleRecipePrint']),
        breakPageOnRecipePrint: userInfo.getIn(['printPreference', 'breakPageOnRecipePrint']),
        includePhotoInRecipePrint: userInfo.getIn(['printPreference', 'includePhotoInRecipePrint']),
      },
      overwriteOnInitialValuesChange: false,
    };
  },
  dispatch => ({
    productActions: bindActionCreators(ProductActions, dispatch),
  }),
)
export default class PrintMultipleRecipeInfoModal extends Component {

  static propTypes = {
    recipeIdList: ReactPropTypes.arrayOf(ReactPropTypes.number).isRequired,
    formatOptions: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired, // instance of ParsleyPropTypes.SelectOptionItem

    hide: ReactPropTypes.func.isRequired,
  };

  async printRecipesInfo(values, dispatch, props) {
    const { format, includeNutrition, includeCostBreakdown, breakPageOnRecipePrint, includePhotoInRecipePrint } = values;
    const { recipeIdList, allMeasures, userInfo } = props;

    if (
      format !== userInfo.getIn(['printPreference', 'recipePrintFormatPreference'])
      || includeNutrition !== userInfo.getIn(['printPreference', 'includeNutritionInMultipleRecipePrint'])
      || includeCostBreakdown !== userInfo.getIn(['printPreference', 'includeCostBreakdownInMultipleRecipePrint'])
      || breakPageOnRecipePrint !== userInfo.getIn(['printPreference', 'breakPageOnRecipePrint'])
      || includePhotoInRecipePrint !== userInfo.getIn(['printPreference', 'includePhotoInRecipePrint'])
    ) {
      const newUserInfo = userInfo
        .setIn(['printPreference', 'recipePrintFormatPreference'], format)
        .setIn(['printPreference', 'includeNutritionInMultipleRecipePrint'], includeNutrition)
        .setIn(['printPreference', 'includeCostBreakdownInMultipleRecipePrint'], includeCostBreakdown)
        .setIn(['printPreference', 'breakPageOnRecipePrint'], breakPageOnRecipePrint)
        .setIn(['printPreference', 'includePhotoInRecipePrint'], includePhotoInRecipePrint);
      saveUserInfo(newUserInfo).then(getUserInfo);
    }

    let allFetchedMaterials = {};
    let allFetchedSupplierSourcedIngs = {};
    let allRecipeImageUrlsWithId = [];
    let allRecipeStepsImageUrlsWithId = [];

    const logoUrl = userInfo.get('logoUrl');
    const companyLogoUrl = userInfo.get('companyLogo')
      ? logoUrl + userInfo.get('companyLogo')
      : null;

    const nutrientList = await getNutrients();

    const allNutrients = Immutable.fromJS(nutrientList);
    const ingredientList = await getIngredientList();
    const allRecipesToContent = await Promise.all(recipeIdList.map(async (recipeId, recipeIdx) => {
      const recipeDetail = await getProductDetails(recipeId);
      const productionOrderObj = await getProductionOrder(getRecipeBaseOrder(recipeDetail, _.get(recipeDetail, ['recipe', 'recipeSize'])));
      const productionOrder = Immutable.fromJS(productionOrderObj);

      const shoppingList = productionOrder.get('shoppingList');
      const usedRecipes = productionOrder.get('recipeList')
          .filter(r => !r.get('isSimplePrep'));

      const supplierSourcedIngredientsToFetch = shoppingList
          .map(shoppingItem => shoppingItem.getIn(['source', 'supplier']))
          .filter(supplierId => _.isNumber(supplierId) && !_.keysIn(allFetchedSupplierSourcedIngs).includes(supplierId));
      const supplierToSourceIngObjList = await Promise.all(supplierSourcedIngredientsToFetch.map(supplierId =>
          getSupplierSourcedIngredients(supplierId).then(sourcedIngredientList => ({
            [supplierId]: sourcedIngredientList,
          })),
      ));

      supplierToSourceIngObjList.forEach(supplierToSourceObj => {
        allFetchedSupplierSourcedIngs = _.assign(allFetchedSupplierSourcedIngs, supplierToSourceObj);
      });
      const supplierSourcedIngredients =  Immutable.fromJS(allFetchedSupplierSourcedIngs);

      const shoppingMaterialsToFetch = shoppingList
          .map(shoppingItem => shoppingItem.get('product'))
          .filter(m => !_.keysIn(allFetchedMaterials).includes(m));
      const recipeMaterialsToFetch = Immutable.fromJS(_.get(recipeDetail, ['recipe', 'steps'])
          .flatMap(s =>
              s.ingredients.map(ing => ing.ingredient),
          )
          .filter(m => !_.keysIn(allFetchedMaterials).includes(m)));
      const usedMaterialsToFetch = usedRecipes
          .flatMap(r => Immutable.fromJS(r.get('billOfMaterials').keySeq().toArray()))
          .filter(m => !_.keysIn(allFetchedMaterials).includes(m));

      const fetchedMaterials = await getMultipleProductsUsageInfo(
          usedMaterialsToFetch.concat(shoppingMaterialsToFetch).concat(recipeMaterialsToFetch).toSet()
      );

      allFetchedMaterials = _.assign(allFetchedMaterials, fetchedMaterials);
      const allIngredients = Immutable.fromJS(ingredientList);
      const detailedIngredients = Immutable.fromJS(allFetchedMaterials).mapKeys(k => parseInt(k));

      const thisRecipe = _(recipeDetail);
      const thisRecipeSize = thisRecipe.get(['recipe', 'recipeSize']);

      thisRecipe.get('photo') && allRecipeImageUrlsWithId.push({
        url: photoUrl(thisRecipe.get('photo'), logoUrl),
        id: `recipe-logo-${recipeId}`,
      });
      thisRecipe.get(['recipe', 'steps'], []).forEach((step, stepIdx) => {
        if (!_.isEmpty(step.photo)) {
          allRecipeStepsImageUrlsWithId.push({
            url: photoUrl(step.photo, logoUrl),
            id: `recipe-step-photo-${recipeId}-${stepIdx}`,
          });
        }
      });
      const recipeToRecipePrint = <RecipePrint
          idx={recipeIdx}
          product={recipeDetail}
          printedQuantity={thisRecipeSize}
          format={format}
          breakPage={breakPageOnRecipePrint}
          includePhotos={includePhotoInRecipePrint}
          allIngredients={allIngredients}
          detailedIngredients={detailedIngredients}
          productionOrder={productionOrder}
          companyLogoUrl={companyLogoUrl}
      />;

      const titleToContent = (isNutrition) => <table className="recipe-logo">
        <tr style={{ height: '120px' }}>
          <td style={{ width: '90%', paddingTop: '80px' }}>
            <h1 className="recipe-print-name">
              {isNutrition ? 'Nutritional Facts for ' : 'Cost Breakdown: '}
              {
                thisRecipe.get('ingredient')
                    ? <span className="sub-recipe-print-name">{thisRecipe.get('name')}</span>
                    : thisRecipe.get('name')
              }
            </h1>
          </td>
          {companyLogoUrl ? <td>
            <div className="recipe-gray-label"><img className="company-logo"/></div>
          </td> : null}
        </tr>
      </table>;

      let recipeToNutritionInfo = null;

      const thisRecipeConversions = Immutable.fromJS(thisRecipe.get('measures'));

      if (includeNutrition) {

        let nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit = thisRecipe.pick([
          'nutrientServingAmount',
          'nutrientServingMeasure',
          'nutrientServingUnit',
        ]);
        // refer form-utils#convertProductJSONToFormValues
        if (thisRecipe.get('ingredient')) {
          if (!nutrientServingAmount) {
            nutrientServingAmount = thisRecipe.get(['recipe', 'recipeSize', 'amount']);
            if (!_.isNumber(nutrientServingUnit)) {
              nutrientServingMeasure = thisRecipe.get(['recipe', 'recipeSize', 'measure']);
              nutrientServingUnit = thisRecipe.get(['recipe', 'recipeSize', 'unit']);
            }
          }
        } else if (!nutrientServingAmount) {
          nutrientServingAmount = 1;
        }

        let recipeNutrients = {};
        ['packaged',
          'portionAmount',
          'portionMeasure',
          'portionUnit',
          'portionsPerPackage',
          'servingsPerPackage',
          'servingName',
          'servingAmount',
          'servingMeasure',
          'servingUnit',
        ].forEach(field => {
          recipeNutrients[field] = { value: recipeDetail.recipe.recipeNutrients[field] };
        });

        recipeToNutritionInfo = <div className="break-page-before">
          {titleToContent(true)}
          <NutritionDisplay
              productType="recipes"
              allNutrients={allNutrients}
              detailedIngredients={detailedIngredients}
              conversions={thisRecipeConversions}
              measures={allMeasures}
              recipeNutrients={recipeNutrients}
              nutrientServingAmount={{ value: nutrientServingAmount }}
              nutrientServingMeasure={{ value: nutrientServingMeasure }}
              nutrientServingUnit={{ value: nutrientServingUnit }}
              recipe={recipeDetail.recipe}
              printMedia
          />
        </div>;
      }

      let recipeToCostBreakdown = null;
      if (includeCostBreakdown) {

        const costPerThisRecipe = RecipeEdit.getCostPerRecipe(recipeDetail, detailedIngredients);
        const { costPerUnit, costingUnitId, costingUnitName, costingMeasureId } = RecipeEdit.costPerUnit(
            costPerThisRecipe,
            thisRecipeSize,
            !thisRecipe.get('ingredient'),
            thisRecipeConversions,
            allMeasures,
        );
        const costFixed = costPerUnit && costPerUnit.toFixed(2);
        const recipePrice = thisRecipe.get(['recipe', 'price']);
        const foodCostPortion = recipePrice && costPerUnit ? costPerUnit / recipePrice : undefined;

        const productionOrderForCB = await getProductionOrder(
            RecipeEdit.prepareBreakdownRecipeOrder(
                // convert step ingredients to form fields
                thisRecipe.set(
                    ['recipe', 'steps'],
                    thisRecipe.get(['recipe', 'steps']).map(step => ({
                      ...step,
                      ingredients: step.ingredients.map(ing =>
                          _(ing)
                              .set('quantity', _.pick(ing, quantityFormFields))
                              .omit(['amount', 'measure', 'unit'])
                              .value(),
                      ),
                    })),
                ).value(),
                costingUnitId,
                costingMeasureId,
                true,
            ),
        );
        const sourcedIngredients = supplierSourcedIngredients.toList().flatten(1);

        const cbProductionOrder = Immutable.fromJS(productionOrderForCB);
        const cbShoppingList = cbProductionOrder.get('shoppingList');

        const cbFullShoppingListItems = cbShoppingList.groupBy(item => item.get('product'))
            .map(itemList => itemList.first());

        const cbFormFields = convertShoppingListJSONToFormValues(cbShoppingList);
        const thisRecipeCostBreakdown = Immutable.List(cbFormFields.map(item => {
          const thisIngredientSources = sourcedIngredients.filter(s => s.get('product') === item.product);
          const {
            name, loading, totalCost: cost,
          } = assembleItemInfo(item, cbFullShoppingListItems, detailedIngredients, thisIngredientSources);
          return { name, loading, cost, item };
        })).sortBy(item => -(item.cost || 0));

        recipeToCostBreakdown = <div className="break-page-before no-page-break-inside mb-2">
          {titleToContent(false)}
          <table className="recipe-logo">
            <tr>
              <td>
                {CostBreakdown.breakdownDesc(
                    costingUnitName,
                    costFixed,
                    recipePrice,
                    toPercent(foodCostPortion),
                )}
              </td>
            </tr>
          </table>
          <br/>
          {CostBreakdown.displayRecipeBreakdown(thisRecipeCostBreakdown, costingUnitName)}
        </div>;
      }

      return {
        recipeName: recipeDetail.name,
        isSubRecipe: recipeDetail.ingredient,
        recipe: recipeToRecipePrint,
        costBreakdown: recipeToCostBreakdown,
        nutrition: recipeToNutritionInfo,
      };

    })).catch(transformErrorValue(addSideEffect(console.log)));

    let arr = _(allRecipesToContent)
        .groupBy(r => r.isSubRecipe)
        .mapValues(l =>
            _(l).sortBy(r => r.recipeName).flatMap(r => [r.recipe, r.costBreakdown, r.nutrition]).value(),
        );
    return this.printContent(
        // should maintain alphabetically order
        // recipes to first and then sub-recipes
        arr
            .keys()
            .sort()
            .flatMap(i => arr.value()[i])
            .value(),
        companyLogoUrl,
        includePhotoInRecipePrint ? allRecipeImageUrlsWithId.concat(allRecipeStepsImageUrlsWithId) : [],
    );
  }

  printContent(contentToPrint, companyLogoUrl, imagesToPrint) {

    const content = ReactDOMServer.renderToString(
      <div className = "ml-037">
        <body>
          <div className="logo">
            <img id="powered-by-logo" className="navbar-brand" />
          </div>
          {contentToPrint}
        </body>
      </div>
    );

    let imageUrlListWithId = [{ url: '/assets/images/poweredby.png', id: 'powered-by-logo' }];

    if (companyLogoUrl) {
      imageUrlListWithId.push({ url: companyLogoUrl, className: 'company-logo' });
    }

    if (!_.isEmpty(imagesToPrint)) {
      imageUrlListWithId = imageUrlListWithId.concat(imagesToPrint);
    }

    return printContent(content, imageUrlListWithId);
  }

  render() {
    const {
      // redux form props
      handleSubmit, submitting, fields,

      // real props
      mainInfoLoaded, recipeIdList,
      selectedFormat, formatOptions,
      includeNutrition: includeNutritionProp,
      includeCostBreakdown: includeCostBreakdownProp,
      hide,
    } = this.props;
    const { format, includeNutrition, includeCostBreakdown, breakPageOnRecipePrint, includePhotoInRecipePrint } = fields;

    let content;

    if (!mainInfoLoaded) {
      content = <LoadingSpinner />;
    } else {
      content = <Row>
        <Col xs={12} className="mb-1">
          <Select
            options={formatOptions.toJS()}
            {...format}
          />
        </Col>
        <Col xs={12} className="mb-1">
          <Validate {...breakPageOnRecipePrint}>
            <Checkbox {...breakPageOnRecipePrint}>Print One Recipe Per Page</Checkbox>
          </Validate>
        </Col>
        <Col xs={12} className="mb-1">
          <Validate {...includePhotoInRecipePrint}>
            <Checkbox {...includePhotoInRecipePrint}>Include Photos</Checkbox>
          </Validate>
        </Col>
        { featureIsSupported('NUTRITIONAL_INFO') ?
          <Col xs={12} className="mb-1">
            <Validate {...includeNutrition}>
              <Checkbox {...includeNutrition}>Include Nutrition</Checkbox>
            </Validate>
          </Col>
        : null }
        <Col xs={12} className="mb-1">
          <Validate {...includeCostBreakdown}>
            <Checkbox {...includeCostBreakdown}>Include Cost Breakdown</Checkbox>
          </Validate>
        </Col>
      </Row>;
    }

    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>Print Recipes</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {content}
      </Modal.Body>
      <Modal.Footer>
        {
          submitting || !mainInfoLoaded
          ? <PrintButton noun="Preparing Printout" disabled={submitting || !mainInfoLoaded} onClick={handleSubmit(this.printRecipesInfo.bind(this))} />
          : <PrintButton disabled={submitting || !mainInfoLoaded} onClick={handleSubmit(this.printRecipesInfo.bind(this))} />
        }
      </Modal.Footer>
    </Modal>;
  }

  componentWillMount() {
    getMeasures();
  }

  componentWillUnmount() {
    const { recipeIdList, productActions } = this.props;
    recipeIdList.forEach(productActions.dropDetails);
  }
}
