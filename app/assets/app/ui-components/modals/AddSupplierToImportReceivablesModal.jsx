// @flow
import React, { Component, type ElementRef } from 'react';
import Immutable from 'immutable';
import { Modal, Row, Col, Button } from 'react-bootstrap';
import Validate from 'common/ui-components/Validate';
import Select from '../Select';
import { checkFields, required } from '../../utils/validation-utils';
import { reduxForm } from 'redux-form';
import NewSupplierModal from './NewSupplierModal';

type Props = {
  hide: Function,
  unknowSupplierRows: Array<*>,
  suppliers: Immutable.List<Immutable.Map<string, any>>,
  uiActions: any,
  uiState: any,
}

type State = {
  added: Array<*>,
  indexRow: number,
}

export default class AddSupplierToImportReceivablesModal extends Component<Props, State> {

  nameToSupplierId: Immutable.Map<string, number>;
  skippedNames: Immutable.Set<string>;
  constructor(props: Props) {
    super(props);
    this.state = {
      added: [],
      indexRow: 0,
    };
    this.nameToSupplierId = Immutable.Map();
    this.skippedNames = Immutable.Set();
  }

  displayNextSupplierRow = (skip: boolean = false, form: Object) => {
    const { indexRow, added } = this.state;
    const { unknowSupplierRows } = this.props;
    let newAdded = added;

    if (skip) {
      this.skippedNames = this.skippedNames.add(unknowSupplierRows[indexRow].supplierName);
    } else {
      this.nameToSupplierId = this.nameToSupplierId.set(form.supplierName, form.supplierId);
      newAdded.push({ ...unknowSupplierRows[indexRow], supplierId: form.supplierId });
    }

    const [newIndex, addedSkipped] = this.getNextIndexRow(indexRow + 1);
    if (newIndex !== -1) {
      this.setState({
        indexRow: newIndex,
        added: [...newAdded, ...addedSkipped],
      });
    } else {
      this.props.hide([...newAdded, ...addedSkipped]);
    }
  }

  getNextIndexRow = (currentIndex: number, addedSkipped: Array<*> = []) => {
    const { unknowSupplierRows } = this.props;
    if (currentIndex < unknowSupplierRows.length) {
      const currentRow = unknowSupplierRows[currentIndex];
      const maybeSupplierId = this.nameToSupplierId.get(currentRow.supplierName);
      const skip = this.skippedNames.includes(currentRow.supplierName);
      if (skip)
        return this.getNextIndexRow(currentIndex + 1, addedSkipped);
      else if (maybeSupplierId)
        return this.getNextIndexRow(currentIndex + 1, [...addedSkipped, { ...currentRow, supplierId: maybeSupplierId }]);
      else
        return [currentIndex, addedSkipped];
    } else return [-1, addedSkipped];
  }

  render() {
    const { hide, unknowSupplierRows, suppliers, uiActions, uiState } = this.props;
    const { indexRow, added } = this.state;
    const csvRow = unknowSupplierRows[indexRow];
    const display = uiState.get('showNewSupplierModal') ? 'none' : 'block';
    return <Modal show onHide={() => hide(added)} className="medium-modal add-code-pos-import-modal" style={{ display }}>
      <Modal.Header closeButton>
        <Modal.Title>Set Supplier</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Row>
            <Col xs={4}>
              <h5>Supplier name</h5>
            </Col>
            <Col xs={4}>
              <h5>Parsley Supplier name</h5>
            </Col>
          </Row>
          <Row>
            <AddSupplierToImportReceivablesRow
              key={csvRow.rowNumber}
              {...csvRow}
              suppliers={suppliers.filter(r => !r.get('tombstone'))
              }
              displayNextSupplierRow={this.displayNextSupplierRow.bind(this)}
              uiActions={uiActions}
              uiState={uiState}
            />
          </Row>
        </div>
      </Modal.Body>
      <Modal.Footer>
      </Modal.Footer>
    </Modal>;
  }
}


type AddSupplierToImportReceivablesRowProps = {
  fields: any,
  handleSubmit: Function,
  displayNextSupplierRow: Function,
  suppliers: Immutable.List<Immutable.Map<string, any>>,
  rowNumber: number,
  uiState: Immutable.Map<string, any>,
  uiActions: any,
  supplierName: string,
}

@reduxForm(
  {
    form: 'import-receivables-modal',
    fields: [
      'supplierId',
      'supplierName',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate: (values) => {
      const importValidators = {
        supplierId: required(),
      };
      return checkFields(values, importValidators);
    },
  }, (state, ownProps) => ({
    initialValues: {
      supplierName: ownProps.supplierName,
      supplierId: undefined,
    },
    overwriteOnInitialValuesChange: true,
  }),
)
class AddSupplierToImportReceivablesRow extends Component<AddSupplierToImportReceivablesRowProps> {
  initialInput: ElementRef<*>;

  render() {
    const { suppliers, fields, handleSubmit, displayNextSupplierRow, rowNumber, uiState, uiActions } = this.props;
    const { supplierName, supplierId } = fields;
    const suppliersOption = suppliers.map(r => ({ value: r.get('id'), label: r.get('name') })).toJS();
    return <div>
      <Row>
        <Col xs={4}>
          {supplierName.value}
        </Col>
        <Col xs={4}>
          <Validate {...supplierId}>
            <Select
              key={rowNumber}
              {...supplierId}
              autoFocus
              openMenuOnFocus
              creatable promptTextCreator={label => 'Add New Supplier'}
              createNew={item => {
                item.setSupplierId = supplierId.onBlur;
                item.value = supplierName.value;
                uiActions.openNewSupplierModal(item);
              }}
              inputRef={ref => this.initialInput = ref}
              options={suppliersOption}
            />
          </Validate>
        </Col>
      </Row>
      <br/>
      <Row>
        <Col xs={12}>
          <Button
            bsStyle="primary"
            style={{ float: 'left' }}
            onClick={() => {
              displayNextSupplierRow(true);
            }}>SKIP</Button>
          <Button
            bsStyle="primary"
            style={{ float: 'right' }}
            onClick={handleSubmit((form) => {
              displayNextSupplierRow(false, form);
            })}>ADD</Button>
        </Col>
      </Row>
      { uiState.get('showNewSupplierModal') ?
        <NewSupplierModal
            newSupplier={uiState.get('showNewSupplierModal')}
            key="new-supplier-modal"
            hide={uiActions.closeNewSupplierModal}
        />
        : null }
    </div>;
  }

  componentDidMount() {
    const { supplierName, supplierId } = this.props.fields;
    supplierName.onChange(this.props.supplierName);
    supplierId.onChange('');
    this.initialInput.select.onChange({ label: '', value: '' }, { action: 'select-option' });
    this.initialInput.onInputChange(this.props.supplierName);
  }
}

