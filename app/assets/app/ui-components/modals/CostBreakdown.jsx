// TODO: everything in this thing needs to be renamed away from "shopping list"
import React, { Component } from 'react';
import ReactPropTypes from 'prop-types'
import ReactDOMServer from 'react-dom/server';
import { reduxForm } from 'redux-form';

import Immutable from 'immutable';
import _ from 'lodash';

import ImmutablePropTypes from 'react-immutable-proptypes';
import * as ParsleyPropTypes from '../utils/PropTypes';

import {
  getSupplierList, getMeasures,
  getProductionOrder,
  getMultipleProductsUsageInfo, getSupplierSourcedIngredients,
} from '../../webapi/endpoints';

import {
  shallowToObject,
  convertShoppingListJSONToFormValues,
} from '../../utils/form-utils';
import { convertPerQuantityValue, purchasingRound } from '../../utils/unit-conversions';

import { Link } from 'react-router';
import { Table, Modal, Button } from 'react-bootstrap';

import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import PrintButton from '../PrintButton';
import CurrencyDisplay from '../CurrencyDisplay';
import { printContent } from '../../utils/printing';
import { productionOrderFromState } from '../../reducers/productionOrder';
import { costingSize } from '../../utils/packaging';
import { FieldArray } from '../../utils/redux-upgrade-shims';

@reduxForm(
    {
      form: 'order-cost-breakdown',
      fields: [
        // not for user modification - just used for cross-referencing into the full data structure
        'items[].product',

        'items[].totalPurchased.amount',
        'items[].totalPurchased.measure',
        'items[].totalPurchased.unit',
        'items[].selectedSource',
        // other fields for future user editing can be found in PropTypes.jsx:shoppingListEntry

        // hidden, non-modifiable fields - necessary to maintain proper UI state for newly-added items
        'items[].userAdded',
        'items[].supplierId', // this one also just makes things easier in the common case

      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    },
    (state, ownProps) => ({
      fullShoppingListItems: productionOrderFromState(state, ownProps.baseOrder, false)
        && productionOrderFromState(state, ownProps.baseOrder, false).get('shoppingList')
          .groupBy(item => item.get('product'))
          // groupBy doesn't assume that the key is unique, so need to assert this
          .map(itemList => itemList.first()),
      initialValues: {
        items: productionOrderFromState(state, ownProps.baseOrder, false) && state.get('measures')
          && convertShoppingListJSONToFormValues(
              productionOrderFromState(state, ownProps.baseOrder, false).get('shoppingList')
          ),
      },
      detailedIngredients: state.get('detailedIngredients'),
      units: state.getIn(['measures', 'units']),
      supplierSourcedIngredients: state.get('supplierSourcedIngredients'),
    })
)
export default class CostBreakdownPublic extends Component {
  render() {
    const { fields, ...rest } = this.props;
    return <FieldArray component={CostBreakdown}
                       fields={fields.items}
                       props={{ ...rest }}
    />;
  }
}

export class CostBreakdown extends Component {
  static propTypes = {
    // from caller
    isRecipe: ReactPropTypes.bool.isRequired,
    totalCost: ReactPropTypes.number.isRequired,
    totalPrice: ReactPropTypes.number.isRequired,
    foodCostPercent: ReactPropTypes.number.isRequired,
    baseOrder: ImmutablePropTypes.listOf(
        ImmutablePropTypes.contains({
          product: ReactPropTypes.number.isRequired,
          quantity: ParsleyPropTypes.quantity.isRequired,
        })
    ).isRequired,
    hide: ReactPropTypes.func.isRequired,
    supplierSourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource),

    // NOT for direct use - if it's missing, it's calculated from fullShoppingListItems by getDisplayedSuppliers()
    displayedSuppliers: ImmutablePropTypes.setOf(ReactPropTypes.number),
    // TODO: find some way to represent this
    // startingInventory: ImmutablePropTypes.listOf(
    //   ImmutablePropTypes.contains({
    //     product: ReactPropTypes.number.isRequired,
    //     quantity: ParsleyPropTypes.quantity.isRequired
    //   })
    // ),

    // from redux store
    fullShoppingListItems: ImmutablePropTypes.listOf(ParsleyPropTypes.shoppingListEntry),
    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),

    // recipe edit props
    costingUnitName: ReactPropTypes.string,
    name: ReactPropTypes.string,

    // optional props
    companyLogoUrl: ReactPropTypes.string,
  };

  static usedIngredientsList(shoppingList) {
    if (shoppingList) {
      return shoppingList.map(entry => entry.get('product'));
    } else {
      return Immutable.Set();
    }
  }

  static getUsageInfo(ingredientIds) { // used to get the conversions
    getMultipleProductsUsageInfo(ingredientIds);
  }

  static getDisplayedSuppliers(props) {
    let { fullShoppingListItems, displayedSuppliers } = props;

    displayedSuppliers = displayedSuppliers
        || (fullShoppingListItems && fullShoppingListItems.map(
            i => i.getIn(['source', 'supplier']) || null
        ).toSet().add(null));
    return displayedSuppliers;
  }

  componentWillMount() {
    getSupplierList();
    getMeasures();
    getProductionOrder(this.props.baseOrder, false);
    if (this.props.fullShoppingListItems)
      CostBreakdown.getUsageInfo(CostBreakdown.usedIngredientsList(this.props.fullShoppingListItems));

    let displayedSuppliers = CostBreakdown.getDisplayedSuppliers(this.props);
    if (displayedSuppliers) {
      for (let supplierId of displayedSuppliers) {
        if (supplierId) {
          getSupplierSourcedIngredients(supplierId)
              .then(sources => CostBreakdown.getUsageInfo(sources.map(s => s.product)));
        }
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.fullShoppingListItems &&
        !Immutable.is(this.props.fullShoppingListItems, nextProps.fullShoppingListItems)
    ) {
      CostBreakdown.getUsageInfo(CostBreakdown.usedIngredientsList(nextProps.fullShoppingListItems)
          .toSet()
          .subtract(
              CostBreakdown.usedIngredientsList(this.props.fullShoppingListItems)
          ));
    }

    let displayedSuppliers = CostBreakdown.getDisplayedSuppliers(nextProps);
    if (displayedSuppliers && !Immutable.is(displayedSuppliers, CostBreakdown.getDisplayedSuppliers(this.props))) {
      for (let supplierId of displayedSuppliers.filter(Number.isInteger)) {
        getSupplierSourcedIngredients(supplierId)
            .then(sources => CostBreakdown.getUsageInfo(sources.map(s => s.product)));
      }
    }
  }

  render() {
    const {
      isRecipe, hide,
      fullShoppingListItems, fields, supplierSourcedIngredients,
      detailedIngredients, units,
      // recipe props
      costingUnitName, name,
      totalCost, totalPrice, foodCostPercent,
      companyLogoUrl,
    } = this.props;

    let content;
    let costBreakdown = Immutable.List();
    if (!(fullShoppingListItems)) {
      content = <LoadingSpinner />;
    } else {

      const indexedItems = fields.map((field, index) => ({ index, ...field }));
      const sourcedIngredients = supplierSourcedIngredients.toList().flatten(1);

      costBreakdown = Immutable.List(fields.map(field => {
          const value = {...shallowToObject(field), quantity: {...shallowToObject(field.quantity)}};
          const thisIngredientSources = sourcedIngredients &&
            sourcedIngredients.filter(s => s.get('product') === field.product.value);
          const {
            name, loading, totalCost: cost,
          } = assembleItemInfo(value, fullShoppingListItems, detailedIngredients, thisIngredientSources);
          return { name, loading, cost, value };
        }))
        .sortBy(item => -(item.cost || 0));

      content = isRecipe ?
        CostBreakdown.displayRecipeBreakdown(costBreakdown, costingUnitName)
        : <div>
            <div className="shopping-list">
              <div className="shopping-list-inside-wrapper">
                <CostBreakdownContent
                  key={1}
                  costBreakdown={costBreakdown}
                  printSection={() => this.printOrderCostBreakdown(indexedItems, sourcedIngredients)}
                  sourcedIngredients={sourcedIngredients}
                  detailedIngredients={detailedIngredients}
                  units={units}
                />
              </div>
            </div>
            <div className="clearfix" />
        </div>;
    }
    
    return <Modal show onHide={hide} className='medium-modal'>
      <Modal.Header closeButton>
        <Modal.Title>Cost Breakdown {isRecipe && `: ${name}`}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {content}
      </Modal.Body>
      <Modal.Footer>
        {
          isRecipe ?
            <PrintButton disabled={!fullShoppingListItems} onClick={() => this.printRecipeBreakdown(costBreakdown)} />
            : <Button bsStyle="default" onClick={hide}>Done</Button>
        }
      </Modal.Footer>
    </Modal>;
  }

  static displayRecipeBreakdown = (costBreakdown, costingUnitName) => {

    const cbHavingCostDefined = costBreakdown.map(cb => cb.cost).filter(cost => _.isNumber(cost));
    // actual total cost differ to total cost to display in the way
    // that each cost are rounded to second decimal point and then added in later one
    const actualTotalCost = cbHavingCostDefined.size ? cbHavingCostDefined.reduce((a, b) => a + b, 0) : 0;
    const totalCostToDisplay = cbHavingCostDefined.size
    ? cbHavingCostDefined.map(cost => Number(cost.toFixed(2))).reduce((a, b) => a + b, 0)
    : 0;

    return <Table condensed hover>
      <thead>
      <tr>
        <th style={{width: '40%'}}>Name</th>
        <th className="align-right-important" style={{width: '25%'}}>Per {costingUnitName}</th>
        <th className="align-right-important">% of Total</th>
      </tr>
      </thead>
      <tbody>
        {
          costBreakdown.map(item => {
            const { name, loading, cost: ingredientCost, value: field } = item;
  
            if (loading) {
              return <tr><td colSpan={3}><LoadingSpinner /></td></tr>
            }

            return <tr className="separator-line">
              <td>{name}</td>
              <td className="align-right-important"><CurrencyDisplay value={ingredientCost} /></td>
              <td className="align-right-important">{ ingredientCost && ((Number(ingredientCost) / actualTotalCost * 100.0).toFixed(1) + '%')}</td>
            </tr>
          })
        }
        <tr>
          <th>Total</th>
          <th className="align-right-important"><CurrencyDisplay value={totalCostToDisplay} /></th>
          <th className="align-right-important" />
        </tr>
      </tbody>
    </Table>;
  };

  static breakdownDesc = (costingUnitName, totalCost, totalPrice, foodCostPercent) => {
    return <h3>
      Cost per {costingUnitName}: <CurrencyDisplay value={totalCost} />
      {totalPrice && <span>, Price: <CurrencyDisplay value={totalPrice} />, Food Cost Percent: {foodCostPercent}</span>}
    </h3>
  }

  printRecipeBreakdown = (costBreakdown) => {

    const {
      costingUnitName, totalCost, totalPrice, foodCostPercent,
      name, companyLogoUrl,
      }  = this.props;

    let costInfo = `Cost per ${costingUnitName}: \$${totalCost}`;
    if (totalPrice) {
      costInfo += `, Price: \$${totalPrice}, Food Cost Percent: ${foodCostPercent}`
    }

    const titleContent = <div>
      <h1>Cost Breakdown: {name}</h1>
      <br/>
      {CostBreakdown.breakdownDesc(
        costingUnitName, totalCost, totalPrice, foodCostPercent,
      )}
    </div>
    const content = ReactDOMServer.renderToString(
      <html>
      <head>
      </head>
      <body>
      <div className="logo">
        <img id="powered-by-logo" className="navbar-brand" />
      </div>
      <div className="shopping-list">
        { companyLogoUrl && <table>
            <tr>
              <td style={{width: '90%'}}></td>
              <td>
                <div className="recipe-gray-label"><img id="company-logo" /></div>
              </td>
            </tr>
          </table>
        }
        {titleContent}
        {CostBreakdown.displayRecipeBreakdown(costBreakdown, costingUnitName)}
      </div>
      </body>
      </html>
    );

    const imageUrlListWithId = [{url: "/assets/images/poweredby.png", id: "powered-by-logo"}];
    if (companyLogoUrl) {
      imageUrlListWithId.push({url: companyLogoUrl, id: "company-logo"});
    }
    return printContent(content, imageUrlListWithId);
  };

  printOrderCostBreakdown = (indexedItems, sourcedIngredients) => {
    const {
      name, totalCost, totalPrice,
      foodCostPercent, companyLogoUrl,
      fullShoppingListItems, detailedIngredients, units,
    } = this.props;

    const content = ReactDOMServer.renderToString(
      <html>
      <head>
      </head>
      <body>
        <div className="logo">
          <img id="powered-by-logo" className="navbar-brand" />
        </div>
        <div className="shopping-list">
          {
            companyLogoUrl && <table>
              <tr>
                <td style={{width: '90%'}}></td>
                <td>
                  <div className="recipe-gray-label"><img id="company-logo" /></div>
                </td>
              </tr>
            </table>
          }
          <h3>Cost Breakdown for {name}: <CurrencyDisplay value={totalCost} /></h3>
          <br/>
          {totalPrice && <h3>Price: <CurrencyDisplay value={totalPrice} />&nbsp; &nbsp; &nbsp;Food Cost Percent: {foodCostPercent}</h3>}
          <PrintedCostBreakdown
            key={1} items={indexedItems}
            fullShoppingListItems={fullShoppingListItems}
            sourcedIngredients={sourcedIngredients}
            detailedIngredients={detailedIngredients}
            units={units}
          />
        </div>
      </body>
      </html>
    );

    const imageUrlListWithId = [{url: "/assets/images/poweredby.png", id: "powered-by-logo"}];
    if (companyLogoUrl) {
      imageUrlListWithId.push({url: companyLogoUrl, id: "company-logo"});
    }
    return printContent(content, imageUrlListWithId);
  };
}

export function assembleItemInfo(
    fields,
    fullShoppingListItems,
    detailedIngredients,
    thisIngredientSources
) {
  if (!fields.product) {
    return { loading: false };
  }
  const detailedProductInfo = detailedIngredients.get(fields.product);

  if (!detailedProductInfo) {
    return {
      loading: true,
    };
  }

  const conversions = detailedProductInfo.get('measures');
  let returnValue = {};

  const source = thisIngredientSources && thisIngredientSources.find(s => s.get('id') === fields.selectedSource);
  if (source) {
    const fullItem = fullShoppingListItems && fullShoppingListItems.get(fields.product);

    returnValue.unitCost = source.get('cost');

    const {
      amount: requiredAmount, unit: requiredUnit,
    } = fullItem.get('totalRequired').toJS();

    const {
      amount: packageAmount, unit: packageUnit,
    } = costingSize(source);

    if (requiredAmount && requiredUnit) {
      returnValue.totalCost = convertPerQuantityValue(returnValue.unitCost,
          packageAmount, packageUnit,
          requiredAmount, requiredUnit,
          conversions
      );
    }
  }

  return {
    name: detailedProductInfo.get('name'),
    ...returnValue,
  };
}

class PrintedCostBreakdown extends Component {
  static propTypes = {
    supplierId: ReactPropTypes.number,
    supplierName: ReactPropTypes.string.isRequired,
    items: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired,
    sourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,
    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
  };

  render() {

    const {
        fullShoppingListItems,
        items,
        sourcedIngredients, detailedIngredients,
    } = this.props;

    const costs = items
        .map(field => {

          const value = {...shallowToObject(field), quantity: {...shallowToObject(field.quantity)}};
          const thisIngredientSources = sourcedIngredients &&
              sourcedIngredients.filter(s => s.get('product') == field.product.value);
          const {
              name,
              loading,
              totalCost: cost,
          } = assembleItemInfo(value, fullShoppingListItems, detailedIngredients, thisIngredientSources);
          return { name, loading, cost, value };
        });
    const totalCost = costs.length ? costs.filter(c => Boolean(c.cost)).map(c => c.cost).reduce((a, b) => a + b, 0) : 0;

    return <section className="shopping-list-page">
      <header>
        <div className="logo">
          <img src="/assets/images/poweredby.png"/>
        </div>
      </header>
      <Table>
        <colgroup>
          <col />
          <col />
          <col />
        </colgroup>
        <thead>
        <tr>
          <th scope="column">Ingredient</th>
          <th scope="column" className="align-right-important">Percent Cost</th>
          <th scope="column" className="align-right-important">Purchase Cost</th>
        </tr></thead>
        <tbody>
          {Immutable.List(costs)
            .sortBy(item => -(item.cost || 0))
            .map(item => {
              const { name, loading, cost } = item;
              let costDisplay;
              let percentCostDisplay = 'N/A';

              if (loading) {
                return <tr />;
              }

              if (cost != undefined) {
                costDisplay = <CurrencyDisplay value={cost}/>;

                const percentCost = cost / totalCost;
                percentCostDisplay = <span>
                  {purchasingRound(percentCost * 100.0, 0) + '%'}
                  </span>;
              }
              return <tr className="separator-line">
                <td>{name}</td>
                <td className="align-right-important">{percentCostDisplay}</td>
                <td className="align-right-important">{costDisplay}</td>
              </tr>;
        }).toArray()}</tbody>
      </Table>
    </section>;
  }
}

class CostBreakdownContent extends Component {
  static propTypes = {
    supplierId: ReactPropTypes.number,
    supplierName: ReactPropTypes.string,
    printSection: ReactPropTypes.func,
    items: ImmutablePropTypes.listOf(ReactPropTypes.object).isRequired,
    fullShoppingListItems: ImmutablePropTypes.listOf(ParsleyPropTypes.shoppingListEntry).isRequired,

    sourcedIngredients: ImmutablePropTypes.listOf(ParsleyPropTypes.productSource).isRequired,

    detailedIngredients: ImmutablePropTypes.map, // TODO: actual detailed proptype
    units: ImmutablePropTypes.mapOf(ParsleyPropTypes.unit),
  };

  render() {
    const {
      printSection,
      costBreakdown,
      } = this.props;

    const totalCost = costBreakdown.size ? costBreakdown.filter(c => Boolean(c.cost)).map(c => Number(c.cost.toFixed(2))).reduce((a, b) => a + b, 0) : 0;

    return <Table className="shopping-list-section">

      <colgroup>
        <col />
        <col />
        <col />
        <col />
      </colgroup>
      <thead>
      <tr>
        <th scope="colgroup">Ingredient</th>
        <th className="text-right" scope="column">Percent of Order Cost</th>
        <th className="text-right" scope="column">Purchase Cost</th>
        <th className="text-right" scope="column">
          <PrintButton onClick={() => {
            printSection();
          }} />
        </th>
      </tr></thead>
      <tbody>
        {   costBreakdown
            .map(item => {
              const { name, loading, cost, value: field } = item;

              let itemNameDisplay, costDisplay, percentCostDisplay;
              if (loading) {
                // each a separate declaration because I think React would react (heh) badly to the same
                // instantiated component being used multiple times
                itemNameDisplay = <LoadingSpinner />;
                costDisplay = <LoadingSpinner />;
                percentCostDisplay = <LoadingSpinner />;
              } else {
                  itemNameDisplay = <Link
                      target="_blank"
                      to={`/"ingredients"/${field.product}`}>
                    {name}
                  </Link>;

                if (cost != undefined) {
                  costDisplay = <CurrencyDisplay value={cost}/>;
                }
                const percentCost = cost && (Number(cost.toFixed(2)) / totalCost);
                if (percentCost != undefined) {
                  percentCostDisplay = <span>{ percentCost ? purchasingRound(percentCost * 100.0, 0) + '%' : 'N/A'}</span>;
                }

              }

              return <tr key={field.product} className={field.userAdded ? 'user-added-shopping-list-item' : null}>
                <th scope="row">{itemNameDisplay}</th>
                <td className="text-right">{percentCostDisplay}</td>
                <td className="text-right">{costDisplay}</td>
                <td />
              </tr>;
            })
        }
      </tbody>
      </Table>;
  }
}
