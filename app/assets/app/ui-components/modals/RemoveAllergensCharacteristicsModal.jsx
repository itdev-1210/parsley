import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { reduxForm } from 'redux-form';
import { Modal, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { removeAllergensCharacteristicsFromIngredients, getProductDetails } from '../../webapi/endpoints';
import Checkbox from '../Checkbox';
import { deepOmit } from '../../utils/general-algorithms';
import Immutable from 'immutable';
import { actionCreators } from '../../reducers/uiState/searchableList';
import { FieldArray } from '../../utils/redux-upgrade-shims';

@reduxForm(
  {
    form: 'remove-allergens-characteristics-from-ingredients',
    fields: [
      'allergens[]',
      'characteristics[]',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const productDetails = state.get('productDetails');
    let s = {
      initialValues: {
        allergens: [],
        characteristics: [],
      },
      productDetails: state.get('productDetails'),
      targets: state.get('ingredients').filter(i => ownProps.targetIds.has(i.get('id'))),
      allAllergens: Immutable.Set(),
      allCharacteristics: Immutable.Set(),
    };
    if (s.targets.every(t => productDetails.has(t.get('id')))) {
      s.targets.forEach(t => {
        const { allergens, characteristics } = productDetails.get(t.get('id'));
        delete characteristics.ingredients;
        _.forEach(allergens, (v, k) => {
          if (v !== false) s.allAllergens = s.allAllergens.add(k);
        });
        _.forEach(characteristics, (v, k) => {
          if (v !== false) s.allCharacteristics = s.allCharacteristics.add(k);
        });
      });
    }
    return s;
  },
  dispatch => Immutable.Map(bindActionCreators({ uncheckAll: actionCreators.uncheckAll }, dispatch)).map(
    creator => creator.bind(this, 'ingredients')
  ).toObject()
)
export default class RemoveAllergensCharacteristicsModal extends Component {

  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit(values) {
    const { hide, targetIds, uncheckAll } = this.props;
    const { allergens, characteristics } = deepOmit(values, v => v === false);
    removeAllergensCharacteristicsFromIngredients(targetIds, allergens, characteristics);
    uncheckAll();
    hide();
  }

  render() {

    let { allergens, characteristics } = this.props.fields;
    const { hide, targets, handleSubmit, submitting, allAllergens, allCharacteristics } = this.props;

    const getBody = () => {
      if (allAllergens.size === 0 && allCharacteristics.size === 0) {
        return <div>{'The selected ingredients haven\'t any allergen and characteristic data'}</div>;
      }

      return <div>
        <p style={{ fontWeight: 'bold' }}>Remove allergens and characteristics from the following ingredients:</p>
        <ListGroup className="green-barred-list-group">
          {targets.map(target =>
            <ListGroupItem key={target.get('name')}>{target.get('name')}</ListGroupItem>
          )}
        </ListGroup>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <FieldArray component={CheckboxSection}
              fields={allergens}
              props={{
                name: 'allergens',
                all: allAllergens,
              }}
          />
          <FieldArray component={CheckboxSection}
              fields={characteristics}
              props={{
                name: 'characteristics',
                all: allCharacteristics,
              }}
          />
        </div>
      </div>;
    };

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Remove Allergens & Characteristics</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {getBody()}
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            {allAllergens.size === 0 && allCharacteristics.size === 0 ? null :
              <div>
                <Button onClick={hide} className="btn-danger">Cancel</Button>
                <Button bsStyle="primary"
                  onClick={handleSubmit(this.submit)}
                  disabled={(allAllergens.length <= 0 && allCharacteristics.length <= 0) || submitting}>Save</Button>
              </div>
            }
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }

  componentWillMount() {
    this.props.targets.forEach(t => getProductDetails(t.get('id')));
  }
}

function CheckboxSection({ name, all, fields }) {

  function checkAll() {
    all.forEach(val => {
      if (!fields.some(t => t.value === val)) {
        fields.addField(val);
      }
    });
  }

  function unCheckAll() {
    for (let i in fields) {
      fields.removeField();
    }
  }

  function check(val) {
    fields.addField(val);
  }

  function unCheck(val) {
    const idx = fields.findIndex(t => t.value === val);
    fields.removeField(idx);
  }

  return <div>
    <h4 style={{ textTransform: 'capitalize' }}>{name}</h4>
    {all.size === 0 ? <div style={{ marginRight: '60px' }}>No Data</div>
        : <div>
          <h5>
            <Checkbox
                inline
                checked={fields.length === all.size}
                indeterminate={fields.length > 0 && fields.length < all.size}
                onChange={ev => ev.target.checked ? checkAll() : unCheckAll()}
            /> Select All
          </h5>
          <ListGroup className="green-barred-list-group">
            {all.map(val => {
              const isSelected = fields.some(t => t.value === val);
              return <ListGroupItem key={val} style={(isSelected) ? { backgroundColor: '#bae08e' } : null}>
                <Checkbox inline checked={isSelected}
                          onChange={ev => ev.target.checked ? check(val) : unCheck(val)}
                /> {val}
              </ListGroupItem>;
            })}
          </ListGroup>
        </div>}
  </div>;
}
