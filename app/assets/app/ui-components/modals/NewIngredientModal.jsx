import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import ReactPropTypes from 'prop-types'
import { reduxForm } from 'redux-form';
import Immutable from 'immutable';
import _ from 'lodash';
import { Row, Col, Form, FormGroup, ControlLabel, Button, Modal, Collapse } from 'react-bootstrap';
import Select from '../Select';
import {
  getProductDetails, saveProduct, getProductUsageInfo, lookupProductName,
  getIngredientList, getMeasures, getSupplierList, getNutrients
} from '../../webapi/endpoints';
import { checkFields, optional } from '../../utils/validation-utils';
import {
  productFormFields, getEmptyNutrientInfo, shallowToObject,
  convertProductJSONToFormValues, convertProductFormValuesToJSON,
  getNNDBSRIdToNutrientIdMap,
} from '../../utils/form-utils';
import Section from '../Section';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';
import TitleInput from '../TitleInput';
import Checkbox from '../Checkbox';
import TagSelector from '../TagSelector';
import ConversionEditor from '../ConversionEditor';
import SimplePreparationsEditor from '../SimplePreparationsEditor';
import SourceEditor, { InHouseDummySource } from '../SourceEditor';
import CrudEditor from '../pages/helpers/CrudEditor';
import {
  newIngredientTemplate,
  sortSourcesBySupplierNameTransformer,
  asyncValidate, validate
} from '../pages/IngredientEdit';
import { actionCreators as ProductActions } from '../../reducers/productDetails';
import NutritionDisplay, {
  nutrient_fields,
} from '../NutritionDisplay';
import { handleLowestPricePreferred } from './IngredientSourcesModal';
import { featureIsSupported } from '../../utils/features';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { noticeError } from 'common/utils/errorAnalytics';

export type NewIngredientDescriptor = {
  setProductId: number => void,
  value: string,
  label: string,
}

@reduxForm(
    {
      form: 'quickIngredientEdit',
      fields: productFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      asyncValidate: _.partial(asyncValidate, true),
      asyncBlurFields: ['name']
    },
    (state, ownProps) => {

      const allNutrients = state.get('nutrients');
      const allSuppliers = state.get('suppliers');

      let initialValues = ownProps.copyFrom ?
      CrudEditor.initialValues(
        state.getIn(['productDetails', ownProps.copyFrom]),
        { origId: ownProps.copyFrom, allNutrients },
        convertProductJSONToFormValues,
        values => values.set('name', ownProps.ingredientName),
        _.partial(sortSourcesBySupplierNameTransformer, allSuppliers),
      ) : newIngredientTemplate
                .set('name', ownProps.ingredientName !== 'New ingredient' ? ownProps.ingredientName : '')
                .set('nutrientInfo', getEmptyNutrientInfo(allNutrients))
                .toJS();

      let productDetails = ownProps.copyFrom ?
          CrudEditor.initialValues(
            state.getIn(['productDetails', ownProps.copyFrom]),
            { origId: ownProps.copyFrom, allNutrients },
            convertProductJSONToFormValues
          ) : newIngredientTemplate
                .set('name', ownProps.ingredientName)
                .set('nutrientInfo', getEmptyNutrientInfo(allNutrients))
                .toJS();

      let mainInfoLoaded = Boolean(allNutrients);
      if (ownProps.copyFrom) {
        mainInfoLoaded = mainInfoLoaded && productDetails;
      }

      return {
        initialValues,
        // only used for checking if this has loaded - initialValues is consumed by the redux-form wrapper
        productDetails,
        mainInfoLoaded,
        allMeasures: state.getIn(['measures', 'measures']),
        allSuppliers,
        allNutrients,
        detailedIngredients: state.get('detailedIngredients') || Immutable.Map(),
        uiState: state.get(ownProps.uiStateName),
      };
    },
    dispatch => ({
      productActions: bindActionCreators(ProductActions, dispatch)
    })
)
class NewIngredientEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showNutrition: false
    };
  }

  submit = (deets) => {
    let { setProductId, uiState, hide } = this.props;
    saveProduct('new', convertProductFormValuesToJSON(deets))
        .then((id) => Promise.all([getIngredientList(), getProductUsageInfo(id)]).then(() => id))
        .then(id => {
          if (!setProductId && uiState) {
            setProductId = uiState.get('showNewIngredientModal').setProductId;
          }
          if (setProductId) setProductId(id);
        }).then(hide);
  };

  render() {
    const {
      name, measures, sources, ingredientTags, inventoryTags, recipe, simplePreps,
      nutrientInfo, nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      info: { lowestPricePreferred },
      allergens, characteristics
    } = this.props.fields;
    const {
        allMeasures, allSuppliers, allNutrients,
        handleSubmit, untouch,
        copyFrom, showSuppliersAndCost, isHidden
    } = this.props;
    const conversions = Immutable.fromJS(
          measures.map(shallowToObject)
      );

    const conversionsReady = (!copyFrom || measures.length > 0) && Boolean(allMeasures);
    const sourcesReady = conversionsReady && measures.length > 0 && Boolean(allSuppliers);

    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);
    const haveEnoughNutrientData = _.every(nutrient_fields, nndbsrId => nndbsrIdToNutrientIdMap[nndbsrId]);
    if (allNutrients && !haveEnoughNutrientData) {
      noticeError('Lacking nutrient data for NutritionDisplay');
    }

    return (
        <Form horizontal className='ingredient_edit'>
          <TitleInput
              inModal
              noun='Ingredient'
              {...name}
          />
          <FormGroup>
            <Col xs={2} componentClass={ControlLabel}>GL Categories:</Col>
            <Col xs={10} md={6}>
              <TagSelector
                  {...ingredientTags}
                  optional
                  tagType="ingredients"
                  optionName="category"
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={2} componentClass={ControlLabel}>Ingredient Locations:</Col>
            <Col xs={10} md={6}>
              <Validate compact {...inventoryTags}>
                <TagSelector
                    {...inventoryTags}
                    optional
                    tagType="inventories"
                    optionName="location"
                />
              </Validate>
            </Col>
          </FormGroup>
          <Section title="Measurement Conversions">
            { conversionsReady ?
                <FieldArray
                    component={ConversionEditor}
                    fields={measures}
                    props={{
                      inModal: true,
                      uniqueName: 'new-ingredient-popup-conversion-editor',
                      usedMeasures: this.usedMeasuresWithMessages(),
                      allMeasures,
                    }}
                /> : <LoadingSpinner /> }
          </Section>
          { haveEnoughNutrientData ?
            <Section title="Nutrition Info" collapsible>
              <NutritionDisplay
                productType="ingredients"
                nutrientInfo={nutrientInfo}
                allNutrients={allNutrients}
                nutrientServingAmount={nutrientServingAmount}
                nutrientServingMeasure={nutrientServingMeasure}
                nutrientServingUnit={nutrientServingUnit}
                conversions={conversions}
                measures={allMeasures}
                allergens={allergens}
                characteristics={characteristics}
              />
            </Section>
          : null}
          { showSuppliersAndCost ?
            <Section className="supplied-ingredient-section">
              {sourcesReady && <React.Fragment>
                <Row id="ing-edit-suppliers-and-costs">
                  <Col xs={12}>
                    <div>
                      <h4>Suppliers & Costs</h4>
                      <Checkbox
                        {...lowestPricePreferred}
                        onChange={ev => handleLowestPricePreferred(
                          ev,
                          lowestPricePreferred,
                          sources,
                          conversions
                        )}
                        >
                        Automatically Select the Lowest Cost Option
                      </Checkbox>
                    </div>
                  </Col>
                </Row>
              </React.Fragment>}
              { sourcesReady ?
                  recipe.recipeSize.amount.value ? // proxy for 'do we even have a recipe'
                      <InHouseDummySource productId={parseInt(this.props.params.id)}
                                          uniqueName='new-ingredient-popup-source-editor'
                                          conversions={conversions}
                                          measures={allMeasures}
                                          recipeFields={recipe}
                                          inModal
                      /> :
                      <FieldArray component={SourceEditor}
                                  fields={sources}
                                  props={{
                                    isHidden: isHidden,
                                    inModal: true,
                                    uniqueName: 'new-ingredient-popup-source-editor',
                                    conversions,
                                    suppliers: allSuppliers,
                                    measures: allMeasures,
                                    untouch,
                                    isSelectable: !lowestPricePreferred.value,
                                    isLowestPricePreferred: lowestPricePreferred.value,
                                  }}
                      />
                  : <LoadingSpinner />
              }
            </Section>
            : null
          }
          <Section title='Preparations & Yields'>
            <FieldArray
                component={SimplePreparationsEditor}
                fields={simplePreps}
                props={{ parentMeasures: measures }}
            />
          </Section>
          <Button type='submit'
                  onClick={handleSubmit(this.submit)}
                  bsStyle='primary'
                  className='green-btn pull-right'>
            save
          </Button>
        </Form>);
  }

  usedMeasuresWithMessages() {
    if (this.props.sources && this.props.sources.value) {
    return Immutable.Map(
        this.props.sources.value
            .map(s => s.get('measure'))
            .toSet() // remove duplicates, discard original indexes
            .map(measureId => [measureId, 'supplier/s']));
    } else {
      return Immutable.Map();
    }
  }

  componentWillMount() {
    const sourceId = this.props.copyFrom;
    if (sourceId !== null)
      getProductDetails(sourceId);

    getMeasures();
    getSupplierList();
    getNutrients();
  }

  componentWillUnmount() {
    const sourceId = this.props.copyFrom;
    if (sourceId !== null)
      this.props.productActions.dropDetails(sourceId);
  }
}

@reduxForm(
    {
      form: 'newIngredient',
      fields: ['ingredient'],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: (values) => checkFields(values, { ingredient: optional() }),
      asyncValidate,
      asyncBlurFields: ['name']
    },
    state => ({
      initialValues: {
        'ingredient': null
      },
      allIngredients: state.get('ingredients'),
      detailedIngredients: state.get('detailedIngredients')
    }),
)
export default class NewIngredientModal extends Component {

  static propTypes = {
    isHidden: ReactPropTypes.bool
  }

  static defaultProps = {
    showSuppliersAndCost: true,
    isHidden: false
  };

  constructor(props) {
    super(props);
    this.state = { step: 1 };
  }

  render() {

    const {
      newIngredient, allIngredients, detailedIngredients, hide, showSuppliersAndCost, uiStateName, isHidden
    } = this.props;

    const { ingredient } = this.props.fields;
    const step = this.state.step;

    const display = isHidden ? 'none' : 'block';

    return <Modal show onHide={hide} className='wider-modal' style={{ display }}>
      <Modal.Header closeButton>
        <Modal.Title>Add New Ingredient</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { step === 1 ?
        <div>
          <p>Copy from existing ingredient (recommended):</p>
          <Select
            {...ingredient}
            value={ingredient.value || ''}
            onChange={newValue => { if (newValue) {
                                         getProductUsageInfo(newValue);
                                         ingredient.onChange(newValue);
                                         newIngredient.value = ingredient.value ||
                                           'Copy of ' + allIngredients.find(val => val.get('id') === newValue).get('name');
                                         this.setState({ step: 2 });
                                       } else {
                                         ingredient.onChange(newValue);
                                       }
                                    }}

            onBlur={() => { ingredient.onBlur(ingredient.value); }}
            options={allIngredients.map(i => ({
              value: i.get('id'),
              label: i.get('name')
            })).toJS()}
            inputRef={input => {
              if (input && !this.input) {
                this.input = input;
                this.input.focus();
              }
            }}
          />
          <p>- or - </p>
          <Button onClick={() => this.setState({ step: 2 })}>Create new</Button>
        </div>
        : null }
        { step === 2 ?
        <div>
          { !ingredient.value || detailedIngredients.get(ingredient.value) ?
          <NewIngredientEdit
            ingredientName={newIngredient && newIngredient.value ? newIngredient.value : ''}
            setProductId={newIngredient && newIngredient.setProductId}
            uiStateName={uiStateName}
            copyFrom={ingredient.value}
            hide={hide}
            showSuppliersAndCost={showSuppliersAndCost}
            isHidden={isHidden}
          /> : <LoadingSpinner /> }
          <Button onClick={() => this.setState({ step: 1 })}>Back</Button>
        </div>
        : null }
      </Modal.Body>
      <Modal.Footer/>
    </Modal>;
  }
}
