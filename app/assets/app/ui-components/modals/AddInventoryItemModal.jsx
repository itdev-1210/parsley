import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'react-bootstrap';
import Select from '../Select';
import { getIngredientList, getAdvancedIngredientPreps } from '../../webapi/endpoints';
import Immutable from 'immutable';
import { indexTypes } from '../../reducers/indexesCache';

@connect(
  state => {
    const allIngredients = state.get('ingredients') || Immutable.List();
    const allAdvancedIngredientPreps = state.get('advancedIngredientPreps') || Immutable.List();
    const allOptions = allIngredients.concat(allAdvancedIngredientPreps);
    return {
      allOptions,
      productIdToFullNameMap: Immutable.Map(allOptions.map(p => [p.get('id'), p.get('name')])),
    };
  }
)
export default class AddInventoryItemModal extends Component {

  render() {

    const {
      hide, onChange, openNewIngredientModal, allOptions, usedIngredientIdsMap, productIdToFullNameMap,
    } = this.props;

    const ingredientOptions = allOptions
      .filter(i => !i.get('tombstone'))
      .filter(i => !usedIngredientIdsMap[i.get('id')])
      .sortBy(i => i.get('name'))
      .map(i => ({
          value: i.get('id'),
          label: i.get('name'),
          className: i.get('isSubRecipe') ? 'text-italic' : null,
        })
      ).toJS();

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Add Ingredient</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Select
            creatable
            onChange={id => onChange(id, productIdToFullNameMap.get(id))}
            createNew={product => {
              hide();
              product.setProductId = onChange;
              openNewIngredientModal(product);
            }}
            promptTextCreator={label => 'Add ingredient "' + label + '"'}
            options={ingredientOptions}
            searchIndexName={indexTypes.INVENTORY_ITEMS}
          />
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }

  componentWillMount() {
    getIngredientList();
    getAdvancedIngredientPreps();
  }
}
