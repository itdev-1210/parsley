import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { Modal, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { addAllergensCharacteristicsToIngredients } from '../../webapi/endpoints';
import { allergenFields, characteristicFields } from '../../utils/form-utils';
import { deepOmit } from '../../utils/general-algorithms';
import { NutritionIngredientAllergens, NutritionIngredientCharacteristics } from '../NutritionDisplay';
import { actionCreators } from '../../reducers/uiState/searchableList';
import Immutable from 'immutable';

@reduxForm({
    form: 'add-allergens-characteristics-to-ingredients',
    fields: [
      ...allergenFields.map(f => `allergens.${f}`),
      ...characteristicFields.map(f => `characteristics.${f}`),
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => ({
    initialValues: {
      allergens: allergenFields.reduce((m, field) => {
        m[field] = null;
        return m;
      }, {}),
      characteristics: characteristicFields.reduce((m, field) => {
        m[field] = null;
        return m;
      }, {}),
    },
    targets: state.get('ingredients').filter(i => ownProps.targetIds.has(i.get('id'))),
  }),
  dispatch => Immutable.Map(bindActionCreators({ uncheckAll: actionCreators.uncheckAll }, dispatch)).map(
    creator => creator.bind(this, 'ingredients')
  ).toObject()
)
export default class AddAllergensCharacteristicsModal extends Component {

  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit(values) {
    const { hide, targetIds, uncheckAll } = this.props;
    const { allergens, characteristics } = deepOmit(values, v => v === null);
    addAllergensCharacteristicsToIngredients(targetIds, allergens, characteristics);
    uncheckAll();
    hide();
  }

  render() {

    const { hide, targets, handleSubmit, submitting } = this.props;
    const { allergens, characteristics } = this.props.fields;

    return <Modal show onHide={hide} className="small-modal ingredients-allergens-characteristics-modal">
        <Modal.Header closeButton>
          <Modal.Title>Add Allergens & Characteristics</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p style={{ fontWeight: 'bold' }}>Add allergens and characteristics to the following ingredients</p>
          <ListGroup className="green-barred-list-group">
            {targets.map(target =>
              <ListGroupItem key={target.get('name')}>{target.get('name')}</ListGroupItem>
            )}
          </ListGroup>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <NutritionIngredientAllergens allergens={allergens}/>
            <NutritionIngredientCharacteristics fromAddCharacteristicsModal characteristics={characteristics}/>
          </div>
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            <Button onClick={hide} className="btn-danger">Cancel</Button>
            <Button bsStyle="primary"
              onClick={handleSubmit(this.submit)}
              disabled={allergens.length <= 0 || characteristics.length <= 0 || submitting}
            >Save</Button>
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }
}
