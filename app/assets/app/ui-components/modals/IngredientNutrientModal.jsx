import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Form, Button, Modal } from 'react-bootstrap';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import { connect } from 'react-redux';
import { newIngredientTemplate } from '../pages/IngredientEdit';
import Immutable from 'immutable';
import CrudEditor from '../pages/helpers/CrudEditor';
import { bindActionCreators } from 'redux';

import { actionCreators as ProductActions } from '../../reducers/productDetails';
import { getProductDetails, getProductUsageInfo, saveProduct, getNutrients } from '../../webapi/endpoints';
import { convertProductFormValuesToJSON, productFormFields, convertProductJSONToFormValues } from '../../utils/form-utils';
import { checkFields, isBlank, commonProductValidators, required } from '../../utils/validation-utils';
import { deepOmit } from '../../utils/general-algorithms';
import NutritionDisplay from '../NutritionDisplay';

function validateIngredient(values) {

  let ingredientValidators = {
    ...commonProductValidators,
  };

  // custom logic: if the user is here, assume they intend to have nutritional
  // info entered, and don't let them go away without entering it
  if (!values.characteristics.nonEdible) {
    ingredientValidators.nutrientServingAmount = required();
    ingredientValidators.nutrientServingUnit = required();
  }

  return checkFields(values, ingredientValidators);
}

@reduxForm(
    {
      form: 'ingredientNutrientEditForm',
      fields: productFormFields,
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate: validateIngredient,
    },
    (state, ownProps) => {

      const allNutrients = state.get('nutrients');
      const productDetails = state.getIn(['productDetails', ownProps.ingredientId]);
      const params = { allNutrients };

      return ({
        initialValues: ownProps.ingredientId
            ? CrudEditor.initialValues(
                productDetails,
                params,
                convertProductJSONToFormValues)
            : newIngredientTemplate.toJS(),
        productDetails,
        mainInfoLoaded: Boolean(productDetails),
        allMeasures: state.getIn(['measures', 'measures']),
        allSuppliers: state.get('suppliers'),
        allNutrients: state.get('nutrients'),
      });
    },
    dispatch => ({
      productActions: bindActionCreators(ProductActions, dispatch),
    })
)
class IngredientNutrientForm extends Component {

  submit = (deets) => {
    saveProduct(this.props.ingredientId, convertProductFormValuesToJSON(deepOmit(deets, isBlank)))
        .then(() => this.props.hide())
        .then(() => {
          getProductUsageInfo(this.props.ingredientId);
          getProductDetails(this.props.ingredientId); // or else IngredientSourcesModal would ignore these changes if used
        });
  };

  render() {

    const {
      nutrientInfo, allergens, characteristics,
      nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
    } = this.props.fields;
    const {
      ingredientId, allMeasures, allNutrients,
      values: { measures },
      handleSubmit, submitting,
    } = this.props;

    const conversions = Immutable.fromJS(measures);

    const conversionsReady = (!ingredientId || conversions.size > 0) && Boolean(allMeasures);
    const sourcesReady = conversionsReady && conversions.size > 0;

     return sourcesReady ?
         <Form>
           <NutritionDisplay
             productType="ingredients"
             nutrientInfo={nutrientInfo}
             allNutrients={allNutrients}
             nutrientServingAmount={nutrientServingAmount}
             nutrientServingMeasure={nutrientServingMeasure}
             nutrientServingUnit={nutrientServingUnit}
             conversions={conversions}
             measures={allMeasures}
             allergens={allergens}
             characteristics={characteristics}
           />
           <Button
               type="submit"
               disabled={submitting}
               bsStyle={submitting ? 'default' : 'primary'}
               onClick={handleSubmit(this.submit)}
               className="pull-right green-btn"
           >
             {submitting ? 'saving' : 'save'}
           </Button>
         </Form> :
         <LoadingSpinner />;
  }

  componentWillMount() {
    getNutrients();
  }

  componentWillUnmount() {
    const { productActions, ingredientId } = this.props;
    productActions.dropDetails(ingredientId);
  }
}

@connect(
    (state, ownProps) => ({
      productDetails: state.getIn(['productDetails', ownProps.ingredientId]),
    })
)
export default class IngredientNutrientModal extends Component {
  render() {

    const {
      ingredientId, hide, productDetails,
    } = this.props;

    return <Modal show onHide={hide} className="wider-modal">
      <Modal.Header closeButton>
        <Modal.Title>{ productDetails ? 'Nutritional information for ' + productDetails.name : 'Loading...'}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { productDetails ? <IngredientNutrientForm
          ingredientId={ingredientId}
          hide={hide}
          inModal
        /> : null }
      </Modal.Body>
      <Modal.Footer />
    </Modal>;
  }

  componentDidMount() {
    // or else this would ignore IngredientSourcesModal's previous changes if there was any
    getProductDetails(this.props.ingredientId);
  }
}
