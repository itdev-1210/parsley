import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import Immutable from 'immutable';
import _ from 'lodash';
import { Row, Col, Form, FormGroup, ControlLabel, Button, Modal, Collapse } from 'react-bootstrap';
import {
  getProductDetails, saveProduct, getProductUsageInfo, lookupProductName,
  getIngredientList, getMeasures, getSupplierList, getNutrients, getReverseDependencies,
} from '../../webapi/endpoints';
import {
  productFormFields, shallowToObject,
  convertProductJSONToFormValues, convertProductFormValuesToJSON,
  getNNDBSRIdToNutrientIdMap,
} from '../../utils/form-utils';
import Section from '../Section';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import Validate from 'common/ui-components/Validate';
import TitleInput from '../TitleInput';
import Checkbox from '../Checkbox';
import TagSelector from '../TagSelector';
import ConversionEditor from '../ConversionEditor';
import SimplePreparationsEditor from '../SimplePreparationsEditor';
import SourceEditor, { InHouseDummySource } from '../SourceEditor';
import CrudEditor from '../pages/helpers/CrudEditor';
import {
  validate, usedMeasuresWithMessages, sortSourcesBySupplierNameTransformer,
} from '../pages/IngredientEdit';
import { actionCreators as ProductActions } from '../../reducers/productDetails';
import NutritionDisplay, {
  nutrient_fields,
} from '../NutritionDisplay';
import { handleLowestPricePreferred } from './IngredientSourcesModal';
import { FieldArray } from '../../utils/redux-upgrade-shims';
import { noticeError } from 'common/utils/errorAnalytics';

function asyncValidate(values: Object, dispatch: $FlowTODO, props: $FlowTODO): Promise<void> {
  if (props.initialValues.name === values.name.trim()) {
    return Promise.resolve();
  }
  return lookupProductName(values.name.trim())
    .then(response => {
      if (response && props.productId !== response) {
        throw { name: 'Name is already used' };
      }
    });
}

@reduxForm(
  {
    form: 'editIngredientModal',
    fields: productFormFields,
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
    validate,
    asyncValidate,
    asyncBlurFields: ['name'],
  },
  (state, ownProps) => {

    const allNutrients = state.get('nutrients');
    const allSuppliers = state.get('suppliers');
    const productDetails = state.getIn(['productDetails', ownProps.productId]);

    let initialValues = CrudEditor.initialValues(
      productDetails,
      { allNutrients },
      convertProductJSONToFormValues,
      _.partial(sortSourcesBySupplierNameTransformer, allSuppliers),
    );

    let mainInfoLoaded = Boolean(allNutrients);

    return {
      initialValues,
      mainInfoLoaded,
      allMeasures: state.getIn(['measures', 'measures']),
      allSuppliers,
      allNutrients,
      productDetails,
      detailedIngredients: state.get('detailedIngredients') || Immutable.Map(),
      uiState: state.get(ownProps.uiStateName),
    };
  },
  dispatch => ({
    productActions: bindActionCreators(ProductActions, dispatch),
  }),
)
class EditIngredient extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showNutrition: false,
    };
  }

  submit = (deets) => {
    let { productId, onHide } = this.props;
    saveProduct(productId, convertProductFormValuesToJSON(deets))
      .then(id => Promise.all([
        getIngredientList(),
        getProductDetails(id),
        getProductUsageInfo(id),
      ]))
      .then(() => onHide());
  };

  render() {
    const {
      name, measures, sources, ingredientTags, inventoryTags, recipe, simplePreps,
      nutrientInfo, nutrientServingAmount, nutrientServingMeasure, nutrientServingUnit,
      allergens, characteristics,
      info: { lowestPricePreferred }
    } = this.props.fields;
    const {
      allMeasures, allSuppliers, allNutrients,
      handleSubmit, untouch, hideSuppliersAndCostSection,
      productId, productDetails, onHide,
    } = this.props;
    const conversions = Immutable.fromJS(
      measures.map(shallowToObject),
    );

    const conversionsReady = (!productId || measures.length > 0) && Boolean(allMeasures);
    const sourcesReady = conversionsReady && measures.length > 0 && Boolean(allSuppliers);

    const nndbsrIdToNutrientIdMap = getNNDBSRIdToNutrientIdMap(allNutrients);
    const haveEnoughNutrientData = _.every(nutrient_fields, nndbsrId => nndbsrIdToNutrientIdMap[nndbsrId]);
    if (allNutrients && !haveEnoughNutrientData) {
      noticeError('Lacking nutrient data for NutritionDisplay');
    }

    return (
      <Form horizontal className="ingredient_edit">
        <TitleInput
          inModal
          noun="Ingredient"
          {...name}
        />
        <FormGroup>
          <Col xs={2} componentClass={ControlLabel}>GL Categories:</Col>
          <Col xs={10} md={6}>
            <TagSelector
              {...ingredientTags}
              optional
              tagType="ingredients"
              optionName="category"
            />
          </Col>
        </FormGroup>
        <FormGroup>
            <Col xs={2} componentClass={ControlLabel}>Ingredient Locations:</Col>
            <Col xs={10} md={6}>
              <Validate compact {...inventoryTags}>
                <TagSelector
                    {...inventoryTags}
                    optional
                    tagType="inventories"
                    optionName="location"
                />
              </Validate>
            </Col>
          </FormGroup>
        <Section title="Measurement Conversions">
          {conversionsReady ?
            <FieldArray
              component={ConversionEditor}
              fields={measures}
              props={{
                inModal: true,
                uniqueName: 'edit-ingredient-popup-conversion-editor',
                usedMeasures: this.usedMeasuresWithMessages(),
                allMeasures,
                reverseDependencies: this.reverseDependencies,
              }}
            /> : <LoadingSpinner />}
        </Section>
        {(productDetails && !productDetails.recipe) && haveEnoughNutrientData ?
          <Section title="Nutrition Info" collapsible>
            <NutritionDisplay
              productType="ingredients"
              nutrientInfo={nutrientInfo}
              allNutrients={allNutrients}
              nutrientServingAmount={nutrientServingAmount}
              nutrientServingMeasure={nutrientServingMeasure}
              nutrientServingUnit={nutrientServingUnit}
              conversions={conversions}
              measures={allMeasures}
              allergens={allergens}
              characteristics={characteristics}
            />
          </Section>
        : null}
        {!hideSuppliersAndCostSection ?
          <Section className="supplied-ingredient-section">
            {sourcesReady && <React.Fragment>
              <Row id="ing-edit-suppliers-and-costs">
                <Col xs={12}>
                  <div>
                    <h4>Suppliers & Costs</h4>
                    <Checkbox
                      {...lowestPricePreferred}
                      onChange={ev => handleLowestPricePreferred(
                        ev,
                        lowestPricePreferred,
                        sources,
                        conversions
                      )}
                      >
                      Automatically Select the Lowest Cost Option
                  </Checkbox>
                  </div>
                </Col>
              </Row>
            </React.Fragment>}
            {sourcesReady ?
              recipe.recipeSize.amount.value ? // proxy for 'do we even have a recipe'
                <InHouseDummySource productId={productId}
                  uniqueName="edit-ingredient-popup-source-editor"
                  conversions={conversions}
                  measures={allMeasures}
                  recipeFields={recipe}
                  inModal
                  // editRecipeOnClick={onHide}
                  editRecipeOnClick="disabled"
                /> :
                <FieldArray component={SourceEditor}
                  fields={sources}
                  props={{
                    inModal: true,
                    uniqueName: 'edit-ingredient-popup-source-editor',
                    conversions,
                    suppliers: allSuppliers,
                    measures: allMeasures,
                    untouch,
                    isSelectable: !lowestPricePreferred.value,
                    isLowestPricePreferred: lowestPricePreferred.value,
                  }}
                />
              : <LoadingSpinner />
            }
          </Section>
        : null}
        <Section title="Preparations & Yields">
          <FieldArray
            component={SimplePreparationsEditor}
            fields={simplePreps}
            props={{ parentMeasures: measures }}
          />
        </Section>
        <Button type="submit"
          onClick={handleSubmit(this.submit)}
          bsStyle="primary"
          className="green-btn pull-right">
          save
          </Button>
      </Form>);
  }

  usedMeasuresWithMessages() {
    const {
      values: {
        sources,
        simplePreps,
        nutrientServingMeasure,
      },
    } = this.props;
    return usedMeasuresWithMessages(sources, simplePreps, nutrientServingMeasure);
  }

  componentWillMount() {
    const productId = this.props.productId;
    getProductDetails(productId).then(product => {
      const ingredients = [productId,
        ..._.flatMap(product.simplePreps, val => val.outputProduct)];
      this.reverseDependencies = {};
      ingredients.forEach(ingredient => {
        getReverseDependencies(ingredient)
          .then(recipes => this.reverseDependencies[ingredient] = recipes);
      });
    });

    getMeasures();
    getSupplierList();
    getNutrients();
  }

  componentWillUnmount() {
    const sourceId = this.props.productId;
    if (sourceId !== null)
      this.props.productActions.dropDetails(sourceId);
  }
}

export default class EditIngredientModal extends Component {

  render() {

    const {
      productId, onHide, hideSuppliersAndCostSection,
    } = this.props;

    return <Modal show onHide={onHide} className="wider-modal">
      <Modal.Header closeButton>
        <Modal.Title>Edit Ingredient</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <EditIngredient
          productId={productId}
          onHide={onHide}
          hideSuppliersAndCostSection={hideSuppliersAndCostSection}
        />
      </Modal.Body>
      <Modal.Footer />
    </Modal>;
  }
}
