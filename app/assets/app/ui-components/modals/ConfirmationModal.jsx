import React, { Component } from 'react';

import ReactPropTypes from 'prop-types'
import { Button, Modal } from 'react-bootstrap';

export default class ConfirmationModal extends Component {
  static propTypes = {
    onHide: ReactPropTypes.func.isRequired,
    onSubmit: ReactPropTypes.func.isRequired,
    title: ReactPropTypes.string,
    titleNoun: ReactPropTypes.string,
    longDescription: ReactPropTypes.string,
    fullDescription: ReactPropTypes.string,
    submitError: ReactPropTypes.string,
    submitVerb: ReactPropTypes.string,
    submitVerbParticle: ReactPropTypes.string,
    inProgress: ReactPropTypes.bool,
    className: ReactPropTypes.string,
  };

  static defaultProps = {
    submitVerb: 'confirm',
    submitVerbParticle: 'Submitting',
    className: '',
  }

  render() {
    var {
      onHide, onSubmit,
      title, titleNoun,
      longDescription,
      fullDescription,
      submitError,
      submitVerb, submitVerbParticle,
      inProgress,
      className,
    } = this.props;

    return <Modal show onHide={onHide} className={className}>
      <Modal.Header closeButton>
        <Modal.Title>{title || `${submitVerb} ${titleNoun}`}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        { submitError ? <p className="text-danger">{submitError}</p> :
          fullDescription ? <p>{fullDescription}</p>
          : <p>Are you sure you want to {submitVerb} {longDescription || `this ${titleNoun.toLowerCase()}`}?</p>
        }
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={onHide} disabled={inProgress}>
          cancel
        </Button>
        <Button bsStyle="danger" onClick={onSubmit} disabled={inProgress}>
          {inProgress ? submitVerbParticle || submitVerb : submitVerb}
        </Button>
      </Modal.Footer>
    </Modal>
  }
}

export const DeleteConfirmationModal =
  ({
    onDelete, deleteError,
    deleteVerb = 'delete',
    deleteVerbParticle = 'deleting',
    ...props
  }) => <ConfirmationModal
    {...props}
    onSubmit={onDelete}
    submitError={deleteError}
    submitVerb={deleteVerb}
    submitVerbParticle={deleteVerbParticle}
  />
