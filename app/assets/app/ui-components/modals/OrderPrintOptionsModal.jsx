import React, { Component } from 'react';
import Immutable from 'immutable';
import { reduxForm } from 'redux-form';
import { Modal, Row, Col, Button } from 'react-bootstrap';

import Checkbox from '../Checkbox';

import {
  saveUserInfo,
  getUserInfo,
} from '../../webapi/endpoints';

@reduxForm(
  {
    form: 'OrderPrintOptions',
    fields: [
      'printPreps',
      'printSubRecipes',
      'printRecipes',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    const userInfo = state.get('userInfo');
    return {
      userInfo: userInfo,
      initialValues: {
        'printPreps': userInfo.getIn(['printPreference', 'orderIncludePreps']),
        'printSubRecipes': userInfo.getIn(['printPreference', 'orderIncludeSubRecipes']),
        'printRecipes': userInfo.getIn(['printPreference', 'orderIncludeRecipes']),
      },
    };
  },
)
export default class OrderPrintOptionsModal extends Component {

  printOrder(values) {
    const { printPreps, printSubRecipes, printRecipes } = values;
    const {
      userInfo,
      onPrintCalled,
    } = this.props;

    if (
        printPreps !== userInfo.getIn(['printPreference', 'orderIncludePreps'])
        || printSubRecipes !== userInfo.getIn(['printPreference', 'orderIncludeSubRecipes'])
        || printRecipes !== userInfo.getIn(['printPreference', 'orderIncludeRecipes'])
      ) {
      const userUpdate = userInfo
        .setIn(['printPreference', 'orderIncludePreps'], printPreps)
        .setIn(['printPreference', 'orderIncludeSubRecipes'], printSubRecipes)
        .setIn(['printPreference', 'orderIncludeRecipes'], printRecipes);
      saveUserInfo(userUpdate).then(getUserInfo);
    }

    const selectedOptions = [];
    if (printPreps) selectedOptions.push('print_preps');
    if (printSubRecipes) selectedOptions.push('print_sub_recipes');
    if (printRecipes) selectedOptions.push('print_recipes');
    onPrintCalled(Immutable.fromJS(selectedOptions));
  }

  render() {
    const { hide, handleSubmit } = this.props;
    const { printPreps, printSubRecipes, printRecipes } = this.props.fields;
    const options = [
      { key: 'print_preps', label: 'Preps', value: 'print_preps', field: printPreps },
      { key: 'print_sub_recipes', label: 'Sub-Recipes', value: 'print_sub_recipes', field: printSubRecipes },
      { key: 'print_recipes', label: 'Recipes', value: 'print_recipes', field: printRecipes },
    ];

    return <Modal show onHide={hide}>
      <Modal.Header closeButton>
        <Modal.Title>Select Items to Print</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {options.map(opt => <Row key={opt.key}>
          <Col md={12} xs={12} className="mb-1">
            <Checkbox {...opt.field}>
              {opt.label}
            </Checkbox>
          </Col>
        </Row>)}
      </Modal.Body>
      <Modal.Footer>
        <Button
          bsStyle="default"
          disabled={!_.some([printPreps.value, printSubRecipes.value, printRecipes.value], v => v === true)}
          onClick={handleSubmit(this.printOrder.bind(this))}
          >
          Continue
        </Button>
      </Modal.Footer>
    </Modal>;
  }
}
