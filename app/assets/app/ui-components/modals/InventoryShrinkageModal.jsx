import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Table } from 'react-bootstrap';
import _ from 'lodash';
import Immutable from 'immutable';

import { getCurrentInventory } from '../../webapi/endpoints';
import { convert, convertPerQuantityValue, truncated } from '../../utils/unit-conversions';

import PackagedAmountInput from '../PackagedAmountInput';
import CurrencyDisplay from '../CurrencyDisplay';

@connect(
  state => ({
    currentInventoryMap: Immutable.Map(
      (state.get('inventoryCurrent') || Immutable.List()).map(m => [m.get('product'), m]),
    ),
  }),
)
export default class InventoryShrinkageModal extends Component {
  render() {
    const { hide, items, currentInventoryMap, detailedIngredients } = this.props;

    let processedItems = [];
    items.forEach(item => {
      const productId = item.product.value;
      const itemDetails = detailedIngredients.get(productId);

      // TODO: Handle sub recipes & advance preps
      if (itemDetails.get('inHouse')) {
        return;
      }

      const name = detailedIngredients.getIn([productId, 'name']);

      const conversions = itemDetails.get('measures');
      const packaging = PackagedAmountInput.fullPackageName(Immutable.fromJS({
        unit: item.quantity.unit.value,
        packaged: item.packaged.value,
        packageName: item.packageName.value,
        subPackageName: item.subPackageName.value,
        superPackageSize: item.superPackageSize.value,
        packageSize: item.packageSize.value,
      }), conversions);

      let amountInCurrentInventory = 0;
      let currentInventory = currentInventoryMap.get(productId);
      if (currentInventory && currentInventory.getIn(['quantity', 'unit'])) {
        ['amount', 'measure', 'unit'].forEach(field => {
          const path = ['quantity', field];
          currentInventory = currentInventory.set(field, currentInventory.getIn(path)).deleteIn(path);
        });

        amountInCurrentInventory = convert(
          currentInventory.get('amount'),
          currentInventory.get('unit'),
          item.quantity.unit.value,
          conversions,
        );
      }
      let shrinkage = amountInCurrentInventory - item.quantity.amount.value;

      const cost = convertPerQuantityValue(itemDetails.get('unitCost'),
        1, itemDetails.get('primaryUnit'),
        shrinkage, item.quantity.unit.value,
        conversions,
      );

      shrinkage = truncated(shrinkage / item.superPackageSize.value / item.packageSize.value, 2);

      processedItems.push({ productId, name, shrinkage, packaging, cost });
    });

    processedItems = _.sortBy(processedItems, i => -i.cost);

    return <Modal show onHide={hide} className="medium-modal">
      <Modal.Header closeButton>
        <Modal.Title>Shrinkage</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Table style={{ marginBottom: '20px' }}>
          <colgroup>
            <col style={{ width: '50%' }}/>
            <col style={{ width: '35%' }}/>
            <col style={{ width: '15%' }}/>
          </colgroup>
          <thead>
            <tr>
              <th>Ingredient</th>
              <th>Quantity</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            {processedItems.map(item =>
              <tr key={item.productId}>
                <td className="inventory-name">
                  {item.name}
                </td>
                <td>
                  {item.shrinkage}
                  &nbsp;&nbsp;
                  {item.packaging}
                </td>
                <td><CurrencyDisplay value={item.cost}/></td>
              </tr>,
            )}
          </tbody>
        </Table>
      </Modal.Body>
    </Modal>;
  }

  componentWillMount() {
    getCurrentInventory(true);
  }
}
