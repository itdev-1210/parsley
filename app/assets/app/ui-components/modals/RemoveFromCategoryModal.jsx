import React, { Component } from 'react';
import { some } from 'lodash';
import { reduxForm } from 'redux-form';
import { Modal, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { removeFromCategory } from '../../webapi/endpoints';
import Checkbox from '../Checkbox';
import { allTagTypes } from '../../utils/tags';

@reduxForm(
  {
    form: 'remove-from-category',
    fields: [
      'tags[]',
    ],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
  (state, ownProps) => {
    // while we can use the same `tagType` to get targets for both ingredients & recipes,
    // we have to use targetType === 'inventoryItems'` to get targets and
    // use `tagType === 'ingredients'` for inventory items.
    const targets = state.get(ownProps.targetType || ownProps.tagType).filter(i => ownProps.targetIds.has(i.get('id')));
    const targetsTag = targets.map(t => t.get('tags')).flatten(true).toSet().sort();
    const allTags = state.getIn(['tags', ownProps.tagType]).filter(t => targetsTag.contains(t.get('id')));
    return {
      initialValues: { tags: [] },
      targets,
      allTags,
    };
  },
)
export default class RemoveFromCategoryModal extends Component {

  constructor(props) {
    super(props);
    ['submit', 'checkAllCategories', 'unCheckAllCategories', 'checkCategory', 'unCheckCategory'].forEach(funcName => {
      this[funcName] = this[funcName].bind(this);
    });
  }

  submit(values) {
    const { hide, tagType, targetIds } = this.props;
    return removeFromCategory(tagType, targetIds, values.tags)
      .then(() => hide())
      .then(() => values.tags);
  }

  checkAllCategories() {
    const { allTags, fields: { tags } } = this.props;
    // RF-MIGRATION: do all these operations with the array helper functions
    allTags.forEach(tag => {
      if (tags.findIndex(t => t.value === tag.get('id')) === -1) {
        tags.addField(tag.get('id'));
      }
    });
  }

  unCheckAllCategories() {
    const { tags } = this.props.fields;
    for (let i in tags) {
      tags.removeField();
    }
  }

  checkCategory(tag) {
    this.props.fields.tags.addField(tag.get('id'));
  }

  unCheckCategory(tag) {
    const { tags } = this.props.fields;
    const idx = tags.findIndex(t => t.value === tag.get('id'));
    tags.removeField(idx);
  }

  render() {

    let { tags } = this.props.fields;
    const { hide, targets, allTags, handleSubmit, submitting, tagType } = this.props;

    const getBody = () => {
      if (allTags.size === 0) {
        return <div>The selected ingredients don&apos;t belong to any {allTagTypes[tagType].pluralName}</div>;
      } else {
        return <div>
          <p style={{ fontWeight: 'bold' }}>Remove</p>
          <ListGroup className="green-barred-list-group">
            {targets.map(target =>
              <ListGroupItem key={target.get('name')}>{target.get('name')}</ListGroupItem>
            )}
          </ListGroup>
          <p style={{ fontWeight: 'bold' }}>from the following {allTagTypes[tagType].pluralName}:</p>
          <div>
            <h5>
              <Checkbox
                  inline
                  checked={tags.length === allTags.size}
                  indeterminate={tags.length > 0 && tags.length < allTags.size}
                  onChange={ev => ev.target.checked ? this.checkAllCategories() : this.unCheckAllCategories()}
              /> Select All
            </h5>
            <ListGroup className="green-barred-list-group">
              {allTags.map(tag => {
                const isSelected = some(tags, t => t.value === tag.get('id'));
                return <ListGroupItem key={tag.get('id')} style={(isSelected) ? { backgroundColor: '#bae08e' } : null}>
                  <Checkbox inline checked={isSelected}
                            onChange={ev => ev.target.checked ? this.checkCategory(tag) : this.unCheckCategory(tag)}
                  /> {tag.get('name')}
                </ListGroupItem>;
              })}
            </ListGroup>
          </div>
        </div>;
      }
    };

    return <Modal show onHide={hide} className="small-modal">
        <Modal.Header closeButton>
          <Modal.Title>Remove from {allTagTypes[tagType].singularName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {getBody()}
          <div style={{ marginTop: '25px', textAlign: 'right' }}>
            <Button onClick={hide} className="btn-danger">Cancel</Button>
            <Button bsStyle="primary" onClick={handleSubmit(this.submit)} disabled={tags.length <= 0 || submitting}>Save</Button>
          </div>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>;
  }
}
