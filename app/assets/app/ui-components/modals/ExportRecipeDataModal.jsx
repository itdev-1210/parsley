import React, { Component } from 'react';
import { Modal, Button, Row, Col } from 'react-bootstrap';
import Radio from 'common/ui-components/Radio';
import { featureIsSupported } from '../../utils/features';

const options = [{
  name: 'goodeggs',
  text: 'Good Eggs Recipe Summary',
  condition: u => {
    const domain = u.get('email').split('@')[1];
    return domain === 'goodeggs.com' || domain === 'parsleycooks.com';
  },
}, {
  name: 'recipes',
  text: 'Recipes',
}, {
  name: 'cost',
  text: 'Cost Information',
}, {
  name: 'nutrition',
  text: 'Nutrition Facts',
  condition: () => featureIsSupported('NUTRITIONAL_INFO'),
}, {
  name: 'cost_price_food_percent',
  text: 'Recipe Cost, Price, and Food Cost Percent Summary',
}];

export default class ExportRecipeDataModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    };
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(option) {
    this.setState({
      selectedOption: option,
    });
  }

  render() {
    const { user, hide, output } = this.props;

    return <div><Modal show onHide={hide} className="medium-modal">
      <Modal.Header closeButton>
        <Modal.Title>Export Recipe Data</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {options.map(o => {
          if (o.condition && !o.condition(user)) return null;

          return <Row key={o.name}>
            <Col md={1} xs={1}>
              <Radio
                checked={this.state.selectedOption === o.name}
                name={o.name}
                onChange={() => this.onSelect(o.name)}
              />
            </Col>
            <Col md={6} xs={6} onClick={() => this.onSelect(o.name)}>{o.text}</Col>
          </Row>;
        })}
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="default" disabled={!this.state.selectedOption} onClick={() => output(this.state.selectedOption)}>
          Output
        </Button>
        <Button bsStyle="default" onClick={hide}>
          Cancel
        </Button>
      </Modal.Footer>
    </Modal></div>;
  }
}
