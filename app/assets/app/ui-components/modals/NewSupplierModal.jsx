import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import isEmail from 'validator/lib/isEmail';
import { Row, Col, Form, FormGroup, FormControl, ControlLabel, Button, Modal } from 'react-bootstrap';
import { getSupplierList, saveSupplier, lookupSupplierName } from '../../webapi/endpoints';
import { isBlank, checkFields, optional, required } from '../../utils/validation-utils';
import { deepOmit } from '../../utils/general-algorithms';
import TitleInput from '../TitleInput';
import { inputLabelWidths, shortInputControlWidths, commentInputControlWidths } from '../pages/SupplierEdit';
import Validate from 'common/ui-components/Validate';

function asyncValidate(values, dispatch, props) {
  return lookupSupplierName(values.name.trim())
    .then(response => {
      if (response !== null) {
        throw { name: 'Name is already used' };
      }
    });
}

function validate(values) {
  return checkFields(values, {
    name: required(),
    email: optional(val => !isEmail(val) ? 'Not valid e-mail' : null)
  });
}

@reduxForm(
    {
      form: 'quickSupplierEdit',
      fields: [
        'name',
        'accountId',
        'contactName',
        'phone',
        'email',
        'comments',
        'instructions',
      ],
      getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
      validate,
      asyncValidate,
      asyncBlurFields: ['name']
    },
    (state, ownProps) => ({
      initialValues: {
        name: (ownProps.newSupplier && ownProps.newSupplier.value !== 'New Supplier') ? ownProps.newSupplier.value : ''
      }
    })
)
export default class NewSupplierModal extends Component {

  submit = deets => {
    saveSupplier('new', deepOmit(deets, isBlank))
      .then(id => {
        getSupplierList();
        this.props.newSupplier.setSupplierId(id);
        this.props.hide();
      });
  }

  render() {
    const {
        name, accountId, contactName, phone, email, comments, instructions
    } = this.props.fields;

    return (
      <Modal show onHide={this.props.hide} className='wider-modal new-supplier-modal'>
        <Modal.Header closeButton>
          <Modal.Title>Add New Supplier</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form horizontal>
            <Row>
              <Col xs={12} md={6}>
                <TitleInput noun='Supplier'
                            inputRef={input => this.initialInput = input}
                            {...name} inModal
                />
              </Col>
            </Row>
            <FormGroup>
              <Col componentClass={ControlLabel} {...inputLabelWidths}>Account ID:</Col>
              <Col {...shortInputControlWidths}>
                <Validate inModal controlId='account-id'>
                  <FormControl type='text' placeholder='Optional' {...accountId} />
                </Validate>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} md={2}>Contact Name:</Col>
              <Col {...shortInputControlWidths}>
                <Validate inModal {...contactName}>
                  <FormControl type='text' placeholder='Optional' {...contactName} />
                </Validate>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} md={2}>Phone:</Col>
              <Col {...shortInputControlWidths}>
                <Validate inModal {...phone}>
                  <FormControl type='text' placeholder='Optional' {...phone} />
                </Validate>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} md={2}>Email:</Col>
              <Col {...shortInputControlWidths}>
                <Validate inModal {...email}>
                  <FormControl type='email' placeholder='Optional' {...email}/>
                </Validate>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} md={2}>Comments:</Col>
              <Col {...commentInputControlWidths}>
                <Validate inModal {...comments}>
                  <FormControl componentClass='textarea' {...comments}/>
                </Validate>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} md={2}>Special Instructions:</Col>
              <Col {...commentInputControlWidths}>
                <Validate inModal {...instructions}>
                  <FormControl componentClass='textarea' {...instructions}/>
                </Validate>
              </Col>
            </FormGroup>
            <Button type='submit'
                    onClick={this.props.handleSubmit(this.submit)}
                    bsStyle='primary'
                    className='green-btn pull-right'>
              save
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer/>
      </Modal>
    );
  }
}
