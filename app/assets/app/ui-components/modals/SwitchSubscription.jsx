import { availableSubscriptionLevels } from 'common/utils/features';
import marked from 'common/utils/marked';
import { changePlan } from 'common/webapi/stripeEndpoints';
import { BadResponseError } from 'common/webapi/utils';
import _ from 'lodash';

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import moment from 'moment';

import {
  Button, Modal,
  Row, Col,
} from 'react-bootstrap';
import Radio from 'common/ui-components/Radio';
import LoadingSpinner from 'common/ui-components/LoadingSpinner';
import SelectPlanForm from 'common/ui-components/SelectPlanForm';

import {
  getUserInfo,
  getStripeSummary,
  getStripeProRationPreview,
} from '../../webapi/endpoints';
import { dateFormat, dateWithWeekdayFormat } from 'common/utils/constants';

@connect(
  (state, ownProps) => ({
    stripeProducts: state.getIn(['stripeInfo', 'products']),
    stripeSummary: state.getIn(['stripeInfo', 'summary']),
  })
)
export class SwitchPlan extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: undefined,
    };
  }

  async selectPlan(values) {
    const {
      stripeSummary, stripeProducts,
      hide, showConfirmPlan,
      } = this.props;
    const selectedPlan = stripeProducts.flatMap(prod => prod.get('plans'))
        .find(plan => plan.get('id') === values.plan);
    const proRationDate = moment().unix();
    const proRationPreview = await getStripeProRationPreview(values.plan, proRationDate);
    const { dueNow } = proRationPreview;

    if (stripeSummary.get('isTrialing') || dueNow === 0) {
      return changePlan(
          selectedPlan.get('id'), proRationDate,
      ).then(
          resp => {
            getUserInfo();
            getStripeSummary();
            hide();
          },
          err => {
            if (err instanceof BadResponseError) {
              this.setState({ errorMessage: err.reduxFormErrors._error });
            }
            return Promise.reject(err);
          },
      );
    } else {
      showConfirmPlan(selectedPlan, proRationPreview);
      return Promise.resolve();
    }
  }

  render() {

    const { stripeProducts, userInfo } = this.props;
    const { errorMessage } = this.state;

    let allowedStripeProducts = stripeProducts;

    if (stripeProducts && userInfo.get('isMultiLocationDownstream')) {
      allowedStripeProducts = stripeProducts.filter(
          prod => {
            const supportedLevels = availableSubscriptionLevels('MULTI_LOCATION');
            return supportedLevels && supportedLevels.includes(prod.get('subscriptionLevel'));
          },
      );
    }

    const errorDisplay = errorMessage
        ? <div className="crud-global-errors"
            dangerouslySetInnerHTML={{ __html: marked(errorMessage) }}
        />
        : null;

    return <Modal show onHide={this.props.hide}>
      <Modal.Header closeButton>
        <Modal.Title>Subscription Level</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {errorDisplay}
        <div>
          <SelectPlanForm
            stripeProducts={allowedStripeProducts}
            userInfo={userInfo}
            inModal={true}
            onSelect={this.selectPlan.bind(this)}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
      </Modal.Footer>
    </Modal>
  }
}

@reduxForm(
  {
    form: 'confirmPlan',
    fields: [],
    getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint),
  },
    state => ({
    stripeSummary: state.getIn(['stripeInfo', 'summary']),
  })
)
export class ConfirmPlan extends Component {

  constructor(props) {
    super(props);
    this.state = {
      errorMessage: undefined,
    };
  }

  componentWillMount() {
  }

  confirmPlan() {
    const { hide, selectedPlan, proRationPreview } = this.props;
    return changePlan(
        selectedPlan.get('id'),  moment(proRationPreview.proRationDate).unix(),
    ).then(
        resp => {
          getUserInfo();
          getStripeSummary();
          hide();
        },
        err => {
          if (err instanceof BadResponseError) {
            this.setState({ errorMessage: err.reduxFormErrors._error });
          }
          return Promise.reject(err);
        },
    );
  }

  render() {
    const {
      hide,
      handleSubmit, submitting,
    } = this.props;
    const { errorMessage } = this.state;

    const { selectedPlan, stripeSummary, proRationPreview } = this.props;

    let infoText;

    if (stripeSummary.get('isTrialing')) {
      // dead code unless we start showing ConfirmPlan during the trial period
      const periodEnd = stripeSummary && moment(stripeSummary.get('periodEnd'), moment.ISO_8601);

      // TODO: use proRationPreview to get monthly rate (to take into account discounts)
      infoText =
          `Your new subscription level is ${selectedPlan.get('name')}` +
          `at monthly rate of $${selectedPlan.get('price') / 100}` +
          `and will be billed on ${periodEnd.format(dateFormat)}`;
    } else {
      const { dueNow, dueAtPeriodEnd, periodEnd } = proRationPreview;
      const dueAtPeriodEndDisplay = dueAtPeriodEnd / 100;
      const periodEndDisplay = moment(periodEnd).format(dateWithWeekdayFormat);

      if (dueNow > 0) {
        infoText = `You will be billed the pro-rated amount of $${dueNow / 100} for the rest of this month ` +
            `and then at a monthly rate of $${dueAtPeriodEndDisplay} ` +
            `starting on ${periodEndDisplay}`;
      } else {
        infoText = `Your account will be credited the pro-rated amount of $${-dueNow / 100} ` +
            `and then billed at a monthly rate of $${dueAtPeriodEndDisplay} ` +
            `starting on ${periodEndDisplay}`;
      }
    }

    const errorDisplay = errorMessage
        ? <div className="crud-global-errors"
            dangerouslySetInnerHTML={{ __html: marked(errorMessage) }}
        />
        : null;

    return <Modal show onHide={hide} className='modal-base'>
      <Modal.Header closeButton>
        <Modal.Title>Confirm New Subscription</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {infoText ?
          <div>
            <Row><Col xs={12}>Are you sure you want to switch to new subscription?</Col></Row>
            <Row><Col xs={12}>{infoText}</Col></Row>
          </div>
          : <LoadingSpinner />
        }
        {errorDisplay}
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="warning" onClick={hide} disabled={submitting}>cancel</Button>
        <Button bsStyle="danger" onClick={handleSubmit(this.confirmPlan.bind(this))} disabled={submitting}>
          {submitting ? 'Processing': 'Confirm'}
        </Button>
      </Modal.Footer>
    </Modal>
  }
}