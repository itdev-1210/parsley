// @flow
import React, { Component } from 'react';
import type { Node } from 'react';
import { Row, Col } from 'react-bootstrap';

type Props = {
  titleText?: string,
  bigIcon: Node,
  statsText: string,
  statsValue: string,
  statsIcon: Node,
  statsIconText: string,
}


export class StatsCard extends Component<Props> {

  static defaultProps = {
    titleText: '',
  };

  render() {
    const { titleText, bigIcon, statsText, statsIcon, statsIconText, statsValue } = this.props;
    return (
      <div className="card card-stats">
        <div className="content">
          {
            titleText &&
            <div className="card-header">
              <div className="stats">
                {titleText}
              </div>
              <hr />
            </div>
          }
          <Row>
            <Col xs={5}>
              <div className="icon-big text-center icon-warning">
                {bigIcon}
              </div>
            </Col>
            <Col xs={7}>
              <div className="numbers">
                <p>{statsText}</p>
                {statsValue}
              </div>
            </Col>
          </Row>
          <div className="footer">
            <hr />
            <div className="stats">
              {statsIcon} {statsIconText}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default StatsCard;