// @flow
import React, { Component } from 'react';
import ChartistGraph from 'react-chartist';

type Props = {
  labels?: Array<string>,
  series?: Array<*>,
  options?: Object,
  responsiveOptions?: Object,
  type: string,
}

export default class Chart extends Component<Props> {

  render() {
    const { labels, series, options, responsiveOptions, type } = this.props;
    const data = {
        labels,
        series,
    };

    return <div className="ct-chart">
      <ChartistGraph
        data={data}
        type={type}
        options={options}
        responsiveOptions={responsiveOptions}
      />
    </div>;
  }
}