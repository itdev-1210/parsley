import _ from 'lodash';
import React, { Component } from 'react';
import ReactPropTypes from 'prop-types';
import Select from './Select';

import Immutable from 'immutable';

import { FormControl, Tooltip, OverlayTrigger } from 'react-bootstrap';

import * as PropTypes from './utils/PropTypes';

import ImmutablePropTypes from 'react-immutable-proptypes';

import Validate from 'common/ui-components/Validate';
import QuantityInput from './QuantityInput';
import { MaskedNumberInput } from './MaskedInput';

import RadioList from './RadioList';

import { conversionRound, convert, truncated, unitDisplayName, getPrimaryMeasureId, rescaleUnits, getMeasure } from '../utils/unit-conversions';
import { shallowToObject } from '../utils/form-utils';

let columnWidths = [
  { xs: 1 },
  { xs: 2 },
  { xs: 3 },
  { xs: 2 },
];
let headerColumnWidths = [
  { xs: 1 },
  { xs: 2 },
  { xs: 6 },
];

// will crash and burn (mostly just crash) if there are loops.
// point is to *prevent* loops
function dependentMeasures(measureId, conversions) {
  const directDependencies = conversions.filter(c =>
      c.get('measure') !== measureId && c.get('conversionMeasure') === measureId,
  ).map(c => c.get('measure'));

  return directDependencies.concat(
      directDependencies.flatMap(d => dependentMeasures(d, conversions)),
  );
}

function conversionsFromFields(fields) {
  return Immutable.fromJS(
      fields.map(shallowToObject),
  );
}

export default class ConversionEditor extends Component {

  constructor(props) {
    super(props);

    this.addConversion = this.addConversion.bind(this);
    // this.deleteConversion = this.deleteConversion.bind(this); // gets bound when passed to ConversionRow
    this.measureBaseUnitId = this.measureBaseUnitId.bind(this);
    this.rebaseMeasures = this.rebaseMeasures.bind(this);
    this.value = this.value.bind(this);

    this.state = {
      alreadyAddedConversion: false,
    };
  }

  static propTypes = {
    fields: ReactPropTypes.arrayOf(PropTypes.conversionFormField).isRequired,

    uniqueName: ReactPropTypes.string.isRequired, // for radio
    // takes one argument (new measure, when one is changed) or zero arguments (when one is deleted)
    onChangeMeasure: ReactPropTypes.func,

    allMeasures: ImmutablePropTypes.mapOf(PropTypes.measure).isRequired,
    // map from ids of used measures (i.e. those that can't be removed) to user-visible description of
    // where it's used
    usedMeasures: ImmutablePropTypes.mapOf(ReactPropTypes.string).isRequired,
    disabled: ReactPropTypes.bool,
    inModal: ReactPropTypes.bool,

    // if product that this conversion is going to add is synced one, then pass info of that upstream product
    upstreamProductInfo: ImmutablePropTypes.mapOf(PropTypes.upstreamProductInfo),
  };

  static defaultProps = {
    onChangeMeasure: _.noop,
    disabled: false,
    inModal: false,
  };

  render() {

    // Recipe info (product details) gets replaced by the product info queried for the ingredients in the steps
    const {
        disabled, uniqueName,
        fields,
        reverseDependencies,
        allMeasures, usedMeasures,
        onChangeMeasure,

        upstreamProductInfo,
    } = this.props;

    const allRowProps = {
      allMeasures, usedMeasures,
      allConversionFields: fields,
      onChangeMeasure,
      reverseDependencies,
      disabled, grabFocus: this.state.alreadyAddedConversion,
    };

    const primaryMeasureId = getPrimaryMeasureId(this.value());
    const primaryMeasureIndex = primaryMeasureId ?
        fields.findIndex(c => _.isMatch(c, {
          'measure': {
            'value': primaryMeasureId,
          },
        })) :
        // if no inherent primary measure, we're in the initialization state where
        0;

    return (
        <RadioList radioInputName={uniqueName}
            disabled={disabled}
            onSelected={this.rebaseMeasures}
            columnWidths={columnWidths}
            headerColumnWidths={headerColumnWidths}
            headerNames={['Primary', 'Type', 'Conversion from one  measurement type to another']}
            onAdd={() => {
              this.addConversion();
              this.setState({ alreadyAddedConversion: true });
            }}
            canAdd={fields.length < allMeasures.size}
        >
          {fields.map((f, i) => <ConversionRow
              key={i}
              inModal
              deleteConversion={this.deleteConversion.bind(this, i)}
              fields={f}
              isPrimary={i === primaryMeasureIndex}
              {...allRowProps}
              syncedMeasureIds={upstreamProductInfo && upstreamProductInfo.get('measureIds')}
          />)}
        </RadioList>);
  }

  rebaseMeasures(baseFields) {
    const newBase = shallowToObject(baseFields);
    const oldConversions = this.value();

    const { fields } = this.props;
    for (let conv of fields) {
      if (conv.measure.value === newBase.measure) {
        // this is the new base - do the things manually to avoid rounding
        // error throwing things off
        conv.conversion.onChange(1);
        conv.amount.onChange(1);
        conv.conversionMeasure.onChange(newBase.measure);
        conv.conversionUnit.onChange(newBase.preferredUnit);
      } else {
        // convert unit from previous value to new basemeasure unit
        let converted = convert(
            conv.conversion.value, conv.conversionUnit.value,
            newBase.preferredUnit, oldConversions,
        );
        // scale new value to optimal unit
        let scaled = rescaleUnits({
          amount: converted,
          unit: newBase.preferredUnit,
          measure: newBase.measure,
        });
        conv.conversion.onChange(scaled.amount);
        conv.conversionUnit.onChange(scaled.unit);

        // add default unit based on scaled
        conv.conversionMeasure.onChange(newBase.measure);
      }
    }
  }

  value() {
    return conversionsFromFields(this.props.fields);
  }

  /**
   * The unit to use for preferredUnit by default when first selecting a measure
   */
  measureBaseUnitId(measureId) {
    return this.props.allMeasures.getIn([measureId, 'units']).filter(
        u => u.get('size') === 1,
    ).keySeq().first();
  }

  addConversion() {
    const primaryConversion = this.value().find(
        c => c.get('measure') === getPrimaryMeasureId(this.value()),
    );

    const newConversion = primaryConversion ?
        {
          'conversionUnit': primaryConversion.get('unit'),
          'conversionMeasure': primaryConversion.get('measure'),
        } :
        // if there's no primary measure, the new measure is it
        { 'conversion': 1 };

    // if only one unused measure, default new conversion to that measure
    let usedMeasures = this.props.fields.filter(measure => measure.measure.value).map(measure => measure.measure.value);
    if (this.props.allMeasures.size - 1 === usedMeasures.length) {
      let unusedMeasure = this.props.allMeasures.find(measure => usedMeasures.indexOf(measure.get('id')) === -1);
      newConversion.measure = unusedMeasure.get('id');
      newConversion.preferredUnit = unusedMeasure.get('allowsCustomNames') ? unusedMeasure.get('units').first().get('id') : undefined;
    }

    this.props.fields.addField(newConversion);
  }
  deleteConversion(i) {
    let conversionFields = this.props.fields;

    this.props.onChangeMeasure(conversionFields[i].measure.value);
    conversionFields.removeField(i);
  }
}

class ConversionRow extends Component {
  static propTypes = {
    fields: PropTypes.conversionFormField.isRequired, // conversionFormFields.map(???)
    allConversionFields: ReactPropTypes.arrayOf(
        PropTypes.conversionFormField,
    ).isRequired,

    allMeasures: ImmutablePropTypes.mapOf(PropTypes.measure),
    usedMeasures: ImmutablePropTypes.mapOf(ReactPropTypes.string),

    deleteConversion: ReactPropTypes.func.isRequired,
    onChangeMeasure: ReactPropTypes.func,

    disabled: ReactPropTypes.bool,
    grabFocus: ReactPropTypes.bool.isRequired,
    inModal: ReactPropTypes.bool,

    syncedMeasureIds: ImmutablePropTypes.listOf(ReactPropTypes.string),
  };

  constructor(props) {
    super(props);

    this.availableMeasures = this.availableMeasures.bind(this);
    this.changeConversionMeasure = this.changeConversionMeasure.bind(this);

  }

  render() {
    const {
        fields: {
            amount, measure, preferredUnit,
            conversion, conversionMeasure, conversionUnit,
            customName,
        },
        reverseDependencies,
        inModal,
        isPrimary,
        deleteConversion,
        allMeasures, usedMeasures, disabled: wholeEditorDisabled,
        syncedMeasureIds,
    } = this.props;

    const conversionIsSynced = syncedMeasureIds && syncedMeasureIds.contains(measure.value);
    const allConversions = this.allConversions();
    // const primaryMeasureId = getPrimaryMeasureId(allConversions);

    let measureId = measure.value;
    let measureInfo = allMeasures.get(measureId);

    let availableMeasures = this.availableMeasures();
    if (measureId) {
      availableMeasures = availableMeasures.set(measureId, measureInfo);
    }
    let measureOptions = availableMeasures
        .map((m, id) => ({ value: id, label: m.get('name') }))
        .valueSeq()
        .sortBy(
          (m) => m.label.toLowerCase(),
        )
        .reverse()
        .toJS();

    let isUsed = usedMeasures.has(measureId);
    let isUsedMessage = `Measurement is used by ${usedMeasures.get(measureId)}`;

    const recipesUsing = this.recipesUsingMeasure(measureId, reverseDependencies);
    const isUsedByRecipe = recipesUsing.length > 0;
    let isUsedByRecipesMessage = isUsedByRecipe ? 'Measurement is used by recipes ' +
                                   recipesUsing.map(a => a.name).join(', ') : '';

    let measureSelector = <Validate inModal={inModal} compact {...measure}>
        <Select disabled={(isUsed && Boolean(measureId)) || conversionIsSynced}
                options={measureOptions}
                value={measureId}
                onChange={newMeasure => this.changeConversionMeasure(newMeasure)}
                inputRef={select => this.initialInput = select}
        />
    </Validate>;

    if (isUsed) {
      let tooltip = <Tooltip>
        {isUsedMessage}
      </Tooltip>;
      measureSelector =
          <OverlayTrigger placement="top" overlay={tooltip}>
            {measureSelector}
          </OverlayTrigger>;
    }

    let defaultUnitControl = null;
    if (wholeEditorDisabled) {
      // plain text name

      defaultUnitControl = !isPrimary &&
          `${amount && amount.value} ${unitDisplayName(preferredUnit.value, allConversions)}`;
    } else if (allMeasures.getIn([measureId, 'allowsCustomNames'])) {
      let measurePrimaryUnit = allMeasures.getIn([measureId, 'units'])
          .find(unit => unit.get('size') === 1);
      let defaultName = measurePrimaryUnit.get('name');

      let helpTooltip = <Tooltip>
        If you like, enter a custom name for the "{defaultName}" unit (e.g. "clove", "can", "head")
      </Tooltip>;

      defaultUnitControl = <div>
          <MaskedNumberInput {...amount}/>
          <OverlayTrigger
              overlay={helpTooltip}
              placement="top"
          >
            <FormControl placeholder={defaultName} {...customName} style={{ width: '83px', marginLeft: '3px' }}/>
          </OverlayTrigger>
      </div>;
    } else {
      let unitOptions = measureInfo ?
          measureInfo.get('units')
              .map((u, id) => ({ value: id, label: u.get('name') }))
              .valueSeq().toJS() :
          [];

      let changeUnit = newUnit => {
        if (isPrimary) {
          // no rescaling required; the conversion stays at 1, and the other units
          // specify exactly which unit their conversions refer to. but the
          // conversionUnit needs to match the new preferredUnit
          conversionUnit.onChange(newUnit);
        } else if (conversionUnit.value && conversion.value && preferredUnit.value) {
          // let the conversion library calculate the size of the new unit using the
          // old conversion
          const newConversionFactor = conversionRound(convert(
              1, newUnit,
              conversionUnit.value,
              allConversions));
          const newConversionQuantity = rescaleUnits({
            amount: newConversionFactor,
            unit: conversionUnit.value,
            measure: getMeasure(conversionUnit.value),
          });

          conversion.onChange(newConversionQuantity.amount);
          conversionUnit.onChange(newConversionQuantity.unit);
          conversionMeasure.onChange(newConversionQuantity.measure);
        }

        preferredUnit.onChange(newUnit);
      };

      // auto set primary measure or it breaks,
      // it is weird ui/ux to manually set it then have it disappear
      if (isPrimary && !preferredUnit.value && unitOptions.length > 0) {
        changeUnit(unitOptions[0].value);
      }
      defaultUnitControl = <div className={isPrimary ? 'hidden' : ''}><Validate inModal={inModal} compact {...preferredUnit}>
        <QuantityInput
          inModal={inModal}
          compact
          disableRounding
          autoAdjust
          disabled={wholeEditorDisabled && measureInfo}
          measures={allMeasures.filter(m => m.get('id') === measure.value)}
          multiMeasure={false}
          reduxFormFields={{
            amountField: amount,
            measureField: measure,
            unitField: preferredUnit,
          }}
          conversions={allConversions}
        />
      </Validate></div>;
    }

    // NB: the onChange handler only fires for the radio button that gets
    // turned on, so we can safely ignore the actual event contents
    const deleteBehavior = wholeEditorDisabled || conversionIsSynced ? 'hidden' :
        isPrimary || isUsed || isUsedByRecipe ? 'disabled' :
            'enabled';

    const usable = measure.valid && preferredUnit.valid && conversion.valid && conversionMeasure.valid && conversionUnit.valid;

    let allowedConversionMeasures = Immutable.Set(
        this.props.allConversionFields.map(c => c.measure.value),
    );

    if (measure.valid) {
      allowedConversionMeasures = allowedConversionMeasures.subtract(
          dependentMeasures(measure.value, allConversions),
      ).remove(measure.value);
    }
    const equalToSign = <span style={{ 'paddingRight': '16px' }}>=</span>;

    return <RadioList.Row
                       isSelected={isPrimary}
                       onSelectedArgument={this.props.fields}
                       selectable={!syncedMeasureIds && (isPrimary || usable)}

                       onDelete={deleteConversion}
                       deleteBehavior={deleteBehavior}
                       deleteTooltip={
                         isPrimary ? 'Cannot delete the primary measure'
                             : isUsed ? isUsedMessage
                             : isUsedByRecipe ? isUsedByRecipesMessage
                             : ''
                       }
                       striped={!wholeEditorDisabled}
        >
          {wholeEditorDisabled ? allMeasures.getIn([measureId, 'name']) : measureSelector}

          {defaultUnitControl}

          { isPrimary ? <div /> :
              wholeEditorDisabled ?
                  <span>
                    {equalToSign}
                    {`${truncated(conversion.value, 2)} ${unitDisplayName(
                      conversionUnit.value, allConversions, false,
                    )}`}
                  </span>
                  :
                  // add equals sign to front of quantity for readability
                  <div style={{ 'display': 'flex' }}>
                    {equalToSign}
                    <QuantityInput
                      inModal={inModal}
                      compact
                      disableRounding
                      autoAdjust
                      disabled={wholeEditorDisabled}
                      measures={allMeasures.filter((measureOption, measureOptionId) =>
                          allowedConversionMeasures.includes(measureOptionId))
                      }
                      multiMeasure={allowedConversionMeasures.size > 1}
                      reduxFormFields={{
                        amountField: conversion,
                        measureField: conversionMeasure,
                        unitField: conversionUnit,
                      }}
                      conversions={allConversions}
                  /></div>
          }
        </RadioList.Row>;

  }

  recipesUsingMeasure(measure, preparations) {
    let recipes = [];
    for (let x in preparations) {
      recipes = recipes.concat(preparations[x].filter(
          recipe => recipe.measures.includes(measure),
      ));
    }

    return recipes;
  }

  availableMeasures() {
    const usedIds = this.allConversions().map(conv => conv.get('measure'));
    return this.props.allMeasures.filter((m, id) =>
        !usedIds.includes(id),
    );
  }

  allConversions() {
    return conversionsFromFields(this.props.allConversionFields);
  }

  changeConversionMeasure(newMeasure) {
    const {
        allMeasures,
        onChangeMeasure,
        fields: thisConversion,
        allConversionFields,
        isPrimary,
    } = this.props;

    const newMeasureInfo = allMeasures.get(newMeasure);
    const newPreferredUnit = newMeasureInfo.get('allowsCustomNames')
        ? newMeasureInfo.get('units').first().get('id')
        : undefined;

    const oldMeasure = thisConversion.measure.value;

    thisConversion.measure.onChange(newMeasure);
    thisConversion.preferredUnit.onChange(newPreferredUnit);
    if (isPrimary) {
      thisConversion.amount.onChange(1);
      thisConversion.conversionMeasure.onChange(newMeasure);
      thisConversion.conversionUnit.onChange(newPreferredUnit);
    }

    const directlyDependentConversions = allConversionFields.filter(conv =>
        conv.measure.value !== oldMeasure && conv.conversionMeasure.value === oldMeasure,
    );

    for (let otherConversion of directlyDependentConversions) {
      otherConversion.conversionMeasure.onChange(newMeasure);
      otherConversion.conversionUnit.onChange(this.measureBaseUnitId(newMeasure));
    }

    onChangeMeasure(oldMeasure, newMeasure);
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }
}
