// @flow
import { MenuItem } from 'react-bootstrap';
import React, { Component } from 'react';
import Immutable from 'immutable';

import { withRouter } from 'react-router';
import { featureIsSupported } from '../utils/features';
import AccountDropdown from 'common/ui-components/AccountDropdown';

type Props = {
  userInfo: ?Immutable.Map<string, $FlowTODO>,
  router: any,
};

@withRouter
export default class AppAccountDropdown extends Component<Props> {

  static defaultProps = {
    isSharedUser: true,
    isReadOnlyUser: false,
    isMultiLocationDownstream: false,
  }

  render() {
    const { userInfo, router } = this.props;

    const {
      isSharedUser, isReadOnlyUser, isMultiLocationDownstream,
    } = userInfo ? userInfo.toJS() : {};

    return <AccountDropdown userInfo={userInfo}>
        <MenuItem href="/account" onClick={ev => {
          router.push('/account');
          ev.preventDefault();
        }}>
          Your Account
        </MenuItem>

        { userInfo && !userInfo.get('isReadOnlyUser') ? [
          <MenuItem key="measure-settings" href="/measure-settings" onClick={ev => {
            router.push('/measure-settings');
            ev.preventDefault();
          }}>
            Settings
          </MenuItem>,

          <MenuItem key="edit_categories" href="/edit_categories" onClick={ev => {
            router.push('/edit_categories');
            ev.preventDefault();
          }}>
            Edit Categories
          </MenuItem>,
        ] : null }

        {
          featureIsSupported('POS') && !isSharedUser && !isReadOnlyUser
              ? [
                <MenuItem key="pos-setup" href="/pos-setup" onClick={ev => {
                  router.push('/pos-setup');
                  ev.preventDefault();
                }}>
                  POS Setup
                </MenuItem>,
              ]
              : null
        }
        {
          featureIsSupported('MULTI_USER') && !isSharedUser
              ? [
                <MenuItem key="menu-item-proper" href="/manage-users" onClick={ev => {
                  router.push('/manage-users');
                  ev.preventDefault();
                }}>
                  Manage Users
                </MenuItem>,
              ]
              : null

        }
        {
          featureIsSupported('MULTI_LOCATION') && !isSharedUser && !isReadOnlyUser && !isMultiLocationDownstream
            ? [
              <MenuItem key="multi-location-proper" href="/manage-locations" onClick={ev => {
                router.push('/manage-locations');
                ev.preventDefault();
              }}>
                Manage Locations
              </MenuItem>,
            ]
            : null
        }
    </AccountDropdown>;
  }
}
