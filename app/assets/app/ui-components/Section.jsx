// @flow
import React, { Component } from 'react';
import type { Node } from 'react';
import { Collapse } from 'react-bootstrap';
import { FeatureGuardedButton } from './FeatureGuardedInputs';

type Props = {
  collapsible: boolean,
  title ?: string,
  children: ?Node,
  toggleCondition: Function,
  onEnter: Function,
  onExit: Function,
  additionalButtons?: Node,
  style?: Object,
  toggleButtonStyle?: Object,
}

type State = {
  collapsed: boolean,
}

export default class Section extends Component<Props, State> {
  contentContainer: ?Element;

  static defaultProps = {
    collapsible: false,
    toggleCondition: () => true,
    onEnter: () => {},
    onExit: () => {},
  };

  constructor(props: Props) {
    super(props);
    this.state = { collapsed : this.props.collapsible };
  }

  render() {
    const {
      title, collapsible, additionalButtons, onEnter, onExit,
      style, toggleButtonStyle, toggleCondition,
      ...passthrough
    } = this.props;
    const beingCollapsed = this.state.collapsed;

    const toggleButton = collapsible ?
      <FeatureGuardedButton
        style={toggleButtonStyle || {}}
        onClick={() => {
          if (toggleCondition()) this.setState({ collapsed: !beingCollapsed });
        }}
        {...passthrough}>
        {(beingCollapsed ? '+ show ' : '- hide ')}
      </FeatureGuardedButton> :
      null;

    return (
        <section style={style || {}} {...passthrough}>
          <hr className="green-hr" />
          { title ?
            <h4 className="indent">
              {title}
              &nbsp;&nbsp;
              {toggleButton}
              {!beingCollapsed ? additionalButtons : null}
            </h4>
          : null }
          <Collapse
            in={!beingCollapsed}
            onEnter={() => {
              onEnter();
              setTimeout(() => {
                if (this.contentContainer) {
                  this.contentContainer.scrollIntoView({
                    behavior: 'smooth',
                    block: 'center',
                  });
                }
              }, 0);
            }}
            onExit={onExit}
          >
            <div className="section-content-container" ref={contentContainer => this.contentContainer = contentContainer}>
              {this.props.children}
            </div>
          </Collapse>
        </section>);
  }
}
