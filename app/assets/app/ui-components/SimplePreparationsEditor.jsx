import React, { Component } from 'react';
import _ from 'lodash';
import ReactPropTypes from 'prop-types';
import Immutable from 'immutable';

import { connect } from 'react-redux';

import {
  ControlLabel, FormControl,
  Form, FormGroup,
  InputGroup,
  Col, OverlayTrigger, Tooltip,
} from 'react-bootstrap';

import ImmutablePropTypes from 'react-immutable-proptypes';

import Validate from 'common/ui-components/Validate';
import PercentInput from './PercentInput';
import AddButton from './AddButton';
import Checkbox from './Checkbox';
import Select from './Select';
import Section from './Section';

import ConversionEditor from './ConversionEditor';
import { shallowToObject } from '../utils/form-utils';

import { getPrimaryMeasureId } from '../utils/unit-conversions';
import { FieldArray } from '../utils/redux-upgrade-shims';

import { getReverseDependencies } from '../webapi/endpoints';

export default class SimplePreparationsEditor extends Component {
  static propTypes = {
    fields: ReactPropTypes.array.isRequired,
    parentMeasures: ReactPropTypes.array.isRequired,
    inModal: ReactPropTypes.bool,
    upstreamPrepKeys: ImmutablePropTypes.listOf(ReactPropTypes.string),
  }

  static defaultProps = {
    inModal: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      alreadyAddedPrep: false,
    };
  }

  static validationFunc(formValues) {
    let errors = {};

    let duplicatePrepNames = _(formValues).countBy('name')
        .pickBy(n => n > 1);
    errors = _.merge(errors, formValues.map(prep => {
      if (duplicatePrepNames.has(prep.name)) {
        return { name: 'Preparation names must be unique' };
      } else {
        return {};
      }
    }));

    return errors;
  }

  newPreparation() {
    return {
      yieldFraction: 1,
      advancePrep: true,
      usesParentMeasures: true,
      measures: [],
      yieldMeasure: getPrimaryMeasureId(
          Immutable.fromJS(_.mapValues(this.props.parentMeasures, shallowToObject)),
      ),
    };
  }

  render() {
    const {
      fields: simplePrepFields, parentMeasures, inModal,
      upstreamPrepKeys, isReadOnly,
    } = this.props;
    return <div>
      {simplePrepFields.map((prep, i) =>
        <SinglePreparationEditor key={i} parentMeasures={parentMeasures} fields={prep}
                                 index={i} grabFocus={this.state.alreadyAddedPrep}
                                 deletePrep={() => simplePrepFields.removeField(i)}
                                 inModal={inModal}
                                 upstreamPrepKeys={upstreamPrepKeys}
                                 isReadOnly={isReadOnly}
        />,
      )}
      {!isReadOnly && <AddButton noun="Preparation" onClick={() => {
        simplePrepFields.addField(this.newPreparation());
        this.setState({ alreadyAddedPrep: true });
      }}/>}
    </div>;
  }
}

@connect(
  (state, ownProps) => ({
    allMeasures: state.getIn(['measures', 'measures']),
    reverseDependencies: state.getIn(['reverseDependencies', ownProps.fields.outputProduct.value]),
  }),
)
class SinglePreparationEditor extends Component {
  render() {
    const {
      index,
      fields: {
        name, yieldFraction, yieldMeasure, advancePrep, usesParentMeasures, measures: ownMeasures,
      },
      allMeasures,
      parentMeasures,
      deletePrep,
      inModal,
      reverseDependencies,
      upstreamPrepKeys,
      isReadOnly,
    } = this.props;

    // only disable for existing prep not for newly added
    const editorIsDisabled = isReadOnly || upstreamPrepKeys && index < upstreamPrepKeys.size && upstreamPrepKeys.contains(name.value);
    const measures = usesParentMeasures.value ? parentMeasures : ownMeasures;

    const parentMeasureIds = parentMeasures.map(m => m.measure.value);
    const myMeasureIds = measures.map(m => m.measure.value);

    const yieldMeasureOptions = _.intersection(myMeasureIds, parentMeasureIds)
            .map(id => ({ value: id, label: allMeasures.getIn([id, 'name']) }));

    function onParentMeasuresChecked(baseCallback, ev) {
      const newValue = ev.target.checked;
      if (usesParentMeasures.value != newValue) {
        // only do complicated stuff if actual changes happened
        if (newValue) {
          // went from defining own measures to using parents'; get rid of own
          for (let i in ownMeasures) {
            ownMeasures.removeField();
          }
        } else {
          // went from using parents' to defining own measures; make a copy
          for (let m of parentMeasures) {
            let measureValues = shallowToObject(m);
            ownMeasures.addField(measureValues);
          }
        }
      }
      baseCallback(ev);
    }

    let deleteButton;
    if (!reverseDependencies || reverseDependencies.size === 0) {
      deleteButton = <span onClick={deletePrep}
        className="pull-right list-item-delete-link">
        delete
      </span>;
    } else {
      const tooltip = <Tooltip>{'Preparation is used by recipes ' + reverseDependencies.map(a => a.get('name')).join(', ')}</Tooltip>;
      deleteButton = <OverlayTrigger placement="top" overlay={tooltip}>
        <span className="pull-right list-item-delete-link disabled">
          delete
        </span>
      </OverlayTrigger>;
    }

    return <div>
      <Form inline className="form-inline-striped simple-preps-editor">
        <Col xsOffset={1} xs={3}>
          <Validate inModal={inModal} compact {...name}>
            <FormControl
                type="text" placeholder="Name"
                inputRef={input => this.initialInput = input}
                {...name}
                disabled={editorIsDisabled}
            />
          </Validate>
        </Col>
        <Col xs={4}>
          <FormGroup className="multi-input-form-group">
            <ControlLabel>Yield</ControlLabel>
            <Validate inModal={inModal} compact {...yieldFraction}>
              <PercentInput {...yieldFraction} disabled={editorIsDisabled} />
              <InputGroup.Addon>%</InputGroup.Addon>
            </Validate>
            <ControlLabel>by</ControlLabel>
            <Validate inModal={inModal} compact {...yieldMeasure}>
              <Select {...yieldMeasure} options={yieldMeasureOptions} disabled={editorIsDisabled} />
            </Validate>
          </FormGroup>
        </Col>
        <Col xs={3}>
          <Checkbox {...advancePrep} disabled={editorIsDisabled} >Advance Prep</Checkbox>
        </Col>
        <Col xs={1}>{!editorIsDisabled && deleteButton}</Col>
      </Form>
      <Section title="Measurement Conversions">
        <Checkbox {...usesParentMeasures}
                  onChange={onParentMeasuresChecked.bind(this, usesParentMeasures.onChange)}
                  onBlur={onParentMeasuresChecked.bind(this, usesParentMeasures.onBlur)}
                  disabled={editorIsDisabled}
        >
          Same as unprepared ingredient
        </Checkbox>
        {usesParentMeasures.value ? null :
            <FieldArray
                component={ConversionEditor}
                fields={measures}
                props={{
                  uniqueName: `simple-prep-conversion-editor-${index}`,
                  disabled: usesParentMeasures.value,
                  usedMeasures: Immutable.Map([[yieldMeasure.value, 'yield']]),
                  allMeasures,
                  disabled: editorIsDisabled,
                }}
            />
        }
      </Section>
    </div>;
  }

  componentWillMount() {
    if (this.props.fields.outputProduct.value) {
      getReverseDependencies(this.props.fields.outputProduct.value);
    }
  }

  componentDidMount() {
    if (this.props.grabFocus) {
      this.initialInput.focus();
    }
  }
}
