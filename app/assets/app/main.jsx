// @flow
import { router } from './navigation';

import { gaInit, newScreen } from 'common/utils/analytics';
import { Provider } from 'react-redux';
import React from 'react';
import store from './store';
import { getUserInfo } from './webapi/endpoints';
import ReactDOM from 'react-dom';

/*
 * polyfills
 */
import '@babel/polyfill'; // automatically sets up polyfill
import fetch from 'whatwg-fetch'; // automatically sets up polyfill

// Chrome (and probably other browsers as well)
// loads a local file into the current tab when it is dropped in,
// which can be annoying when we mis-drop a file into a dropzone
// -> disable this default behaviour
window.addEventListener('dragover', e => e.preventDefault(), false);
window.addEventListener('drop', e => e.preventDefault(), false);

// promise rejections are not by default logged by New Relic
function logToNewRelic(e) {
  if (window.NREUM) // NREUM = New Relic End User Monitoring
    window.NREUM.noticeError(e);
}
window.addEventListener('unhandledrejection', logToNewRelic);

// Setup router according to the user (isReadOnly?, isStdlib?, etc)
getUserInfo()
  .then(user => {
    ReactDOM.render(
      <Provider store={store}>{router(user)}</Provider>,
      // $FlowFixMe we rely on this element existing
      document.querySelector('#app-container'),
    );
  });

gaInit();
// for the main app, we don't care too much what people do exactly; but we want
// to treat the whole app as a "screen" for duration-tracking purposes
newScreen('app');
