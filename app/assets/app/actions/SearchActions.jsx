export function searchListError(error) {
  console.log(error.toString());
  console.log(error.stack);
  return {
    type: 'SEARCH_LIST_ERROR',
    content: { error },
  };
}