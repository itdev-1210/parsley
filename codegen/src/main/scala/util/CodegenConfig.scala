package util

import com.typesafe.config.{Config, ConfigFactory}

object CodegenConfig {
  def apply(classLoader: ClassLoader): Config = {
    val appConfig = ConfigFactory.parseResources(classLoader, "application.conf")
    val referenceConfig = ConfigFactory.parseResources(classLoader, "reference.conf")
    appConfig.withFallback(referenceConfig)
  }
}
