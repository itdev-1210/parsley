import util.CodegenConfig

import play.api.Configuration
import play.api.db.evolutions._
import play.api.db.slick.DefaultSlickApi
import play.api.db.slick.evolutions.SlickDBApi
import play.api.inject.DefaultApplicationLifecycle

import java.io.File

import scala.concurrent.ExecutionContext.Implicits._

object RunEvolutions extends App {

  val Array(configPath, appRoot) = args
  val classLoader = this.getClass.getClassLoader

  val conf = Configuration(CodegenConfig(classLoader)) ++
      Configuration("slick.dbs.default.profile" -> "com.parsleycooks.pgdriver.PgDriver$")

  val dummyLifecycle = new DefaultApplicationLifecycle
  val underlyingSlickApi = new DefaultSlickApi(
    null, // argument never used
    conf,
    dummyLifecycle,
  )
  val dbApi = SlickDBApi(underlyingSlickApi)

  OfflineEvolutions.applyScript(new File(appRoot),
    classLoader, dbApi,
    "default", autocommit = false
  )

  dummyLifecycle.stop() // closes DB connections
}
