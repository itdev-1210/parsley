import slick.codegen.SourceCodeGenerator
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile
import slick.jdbc.meta.MTable
import slick.model.Model
import slick.sql.SqlProfile.ColumnOption
import util.CodegenConfig

import com.github.tminglei.slickpg.ExPostgresProfile.ExModelBuilder

import java.io.{BufferedWriter, File, FileWriter}

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.Try

/**
  *  Copied from https://github.com/slick/slick/issues/1382#issuecomment-161364214
  *  This customizes the Slick code generator to fix type mismatch problems with Play! 2.4 JSON inception
  *  http://stackoverflow.com/questions/31048780/custom-slick-code-generator-3-0-0
  */
object CustomSlickCodeGenerator {

  import util.MiscellaneousImplicits.SeqExtensions

  def enumerateEnums(driver: PostgresProfile)(implicit ec: ExecutionContext) = {
    import driver.api._
    for {
      rawValues <- sql"""
            SELECT t.typname, e.enumlabel, e.enumsortorder
            FROM pg_enum e JOIN pg_type t on t.oid=e.enumtypid
      """.as[(String, String, Double)]
    } yield {
      case class EnumValue(label: String, sortOrder: Double)
      val asEnumValues = rawValues.map { case (typeName, label, sortOrder) =>
        typeName -> EnumValue(label, sortOrder)
      }

      val byName: Map[String, Seq[EnumValue]] = asEnumValues.groupByFirst
      byName.mapValues { singleEnumValues =>
        singleEnumValues.sortBy(_.sortOrder).map(_.label)
      }
    }
  }

  def run(outputDir: String, pkg: String): Unit = {
    implicit val ec = ExecutionContext.global
    val classLoader = this.getClass.getClassLoader

    val conf = CodegenConfig(classLoader)
    val driver = com.parsleycooks.pgdriver.PgDriver
    val driverName = "com.parsleycooks.pgdriver.PgDriver"
    val generatedDriverName = "GeneratedPgDriver"

    val user = conf.getString("slick.dbs.default.db.user")
    val url = conf.getString("slick.dbs.default.db.url")

    val database = driver.api.Database.forURL(url, user, driver = driverName)

    val fetchTables: DBIO[Seq[MTable]] = driver.defaultTables.map(tables =>
        tables.filter(_.name.name != "play_evolutions")
    )

    try {
      val (m, e) = Await.result(
        database.run(for {
          t <- fetchTables
          model <- (new ExModelBuilder(t, ignoreInvalidDefaults = true)).buildModel
          enums <- enumerateEnums(driver)
        } yield (model, enums)),
        Duration.Inf
      )

      new SlickDriverGenerator(e).writeToFile(driverName, outputDir, pkg, generatedDriverName)
      new CustomSlickCodeGenerator(m, e).writeToFile(generatedDriverName, outputDir, pkg)
    } finally database.close
  }

  def main(args: Array[String]) = {
    args.toList match {
      case outputDir :: outputPackage :: Nil =>
        run(outputDir, outputPackage)
      case _ =>
        throw new IllegalArgumentException("CustomSlickCodeGenerator takes only one argument, got " + args.mkString(", "))
    }
  }
}

class SlickDriverGenerator(enums: Map[String, Seq[String]]) {
  def enumValueDef(v: String) =
    s"""val ${NameConversionUtils.pgToCamelCase(v)} = Value("$v")"""

  def enumDefs: Iterable[String] = enums.map { case (enumName, enumVals) =>
      s"""object ${NameConversionUtils.pgToCamelCase(enumName)} extends Enumeration {
         |  ${enumVals.map(enumValueDef).mkString("\n  ")}
         |}
         |""".stripMargin
  }

  def enumImplicits: Iterable[String] = enums.keys.map { enumName =>
      val className = NameConversionUtils.pgToCamelCase(enumName)
    s"""|implicit val ${className}TypeMapper = createEnumJdbcType("$enumName", $className)
    |    implicit val ${className}ListTypeMapper = createEnumListJdbcType("$enumName", $className)
    |    implicit val ${className}ColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder($className)
    |    implicit val ${className}OptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder($className)
    |""".stripMargin
  }

  def code(pkg: String, driverType: String, parentType: String) =
    s"""package ${pkg}
       |
       |${enumDefs.mkString("\n\n")}
       |
       |// AUTO-GENERATED Slick profile
       |trait $driverType extends $parentType {
       |
       |  trait ${driverType}API extends PgDriverAPI {
       |    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
       |
       |    ${enumImplicits.mkString("\n\n")}
       |  }
       |  override val api = new ${driverType}API {}
       |}
       |
       |object $driverType extends $driverType
       |""".stripMargin

  // mostly copied from slick-codegen's OutputHelpers.scala
  def writeToFile(parentDriver: String, outputDir: String, pkg: String, driverName: String) {
    val outputFolder = outputDir + "/" + pkg.replace(".", "/") + "/"
    new File(outputFolder).mkdirs()
    val outputFile = new File(outputFolder + driverName + ".scala")
    if (!outputFile.exists()) {
      outputFile.createNewFile()
    }
    val content = code(pkg, driverName, parentDriver)

    Try {
      val w = new FileWriter(outputFile)
      val bw = new BufferedWriter(w)
      bw.write(content)
      if (!content.endsWith("\n")) bw.write("\n")
      bw.close()
    }
  }
}

object NameConversionUtils {
  def pgToCamelCase(str: String): String = {
    // pg-case being either snake case (some_word) or kebab case dashed-case
    val tokens = str.split("[_-]")
    tokens.map(_.capitalize).mkString
  }
}

class CustomSlickCodeGenerator(model: Model, enums: Map[String, Seq[String]] )
    extends SourceCodeGenerator(model) {
  val models = new mutable.MutableList[String]

  override def packageCode(profile: String, pkg: String, container: String, parentType: Option[String]) =
    s"""
package ${pkg}

${topLevelDeps}
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object ${container} extends {
  val profile = $profile
} with ${container}

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait ${container}${parentType.map(t => s" extends $t").getOrElse("")} {
  val profile: $profile
  import profile.api._
  ${indent(code)}
}
${indent(models.mkString("\n"))}
      """.trim()

  // stuff we want visible both inside the Tables trait and out in the model case-class definitions
  def topLevelDeps = Seq("java.time.{ Instant, LocalDate }").map(dep =>s"import $dep\n").mkString

  /**
    * Moves the Row(s) outside of the auto-generated 'trait Tables'
    */
  override def Table = new Table(_) {
    override def EntityType = new EntityTypeDef {
      override def docWithCode: String = {
        models += super.docWithCode.toString + "\n"
        ""
      }
    }

    override def Column = new Column(_) {
      override def rawType = model.tpe match {
        case "java.sql.Timestamp" => "Instant"
        case "java.sql.Date" => "LocalDate"
        case "String" => model.options.collect {
          case typeOpt: ColumnOption.SqlType => typeOpt.typeName
        }.map {
          case "_text" => "List[String]"
          case enumTypeName if enums.contains(enumTypeName) =>
            s"${NameConversionUtils.pgToCamelCase(enumTypeName)}.Value"
          case typeName => "String"
        }.headOption.getOrElse("String")
        case _ => super.rawType
      }
    }
  }
}
