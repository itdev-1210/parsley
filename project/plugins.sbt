resolvers ++= Seq(
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  "bintray-sbt-plugin-releases" at "https://dl.bintray.com/content/sbt/sbt-plugin-releases"
)

lazy val root = (project in file("."))
        .dependsOn(RootProject(uri("git://github.com/JetBrains/sbt-tc-logger#80aff89")))
// Pretty TeamCity logging
// addSbtPlugin("org.jetbrains.teamcity.plugins" % "sbt-teamcity-logger" % "0.4.0")

// for devel convenience
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.3.4")

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.16")

addSbtPlugin("com.typesafe.sbt" % "sbt-gzip" % "1.0.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.4")

addSbtPlugin("com.softwaremill.clippy" % "plugin-sbt" % "0.6.0")

// addSbtPlugin("com.github.tototoshi" % "sbt-slick-codegen" % "1.2.0")


updateOptions := updateOptions.value.withCachedResolution(true)
