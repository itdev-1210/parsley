import play.sbt.PlayRunHook
import sbt.{File, Path, Logger, file}
import sbt.Path._

import scala.sys.process._

object Webpack {
  def makeBuildProcess(outputPath: Option[File], nodeEnv: String, logger: Logger) = {
    val blackHoleLogger = new FileProcessLogger(file("/dev/null"))

    val npmBin = Path(Seq("npm", "bin").!!.trim)
    val checkDeps = npmBin / "check-dependencies"
    if (
      !checkDeps.exists || (Seq(checkDeps.getPath, "--check-git-urls").!(blackHoleLogger) != 0)
    ) {
      // should hit this path if check-dependencies fails OR if this is a new checkout (nonexistent node_modules/.bin)
      logger.info("JS dependencies out of date; installing")
      if (Seq("npm", "install").! != 0) {
        throw new Exception("npm install failed")
      }
    }

    val binName = if (outputPath.isDefined) "webpack" else "webpack-dev-server"

    Process(
      Seq(
        (npmBin / binName).getPath,
        "--mode", nodeEnv, // the strings happen to be the same
      ) ++ {
        outputPath match {
          case Some(path) =>
            Seq(
              "--display-error-details",
              "--output-path", path.getPath,
              "--bail"
            )
          case None => Seq("--host=0.0.0.0", "--watch", "--progress")
        }
      },
      cwd = None,
      // despite passing the mode to webpack, we need to pass env variable to
      // make it visible to webpack.config.js
      "NODE_ENV" -> nodeEnv,
    )
  }

  def develRunHook(logger: Logger) = new PlayRunHook {
    var buildProcess: Option[Process] = None

    def startDevResourceServer() = {
      buildProcess = Some(makeBuildProcess(None, "development", logger).run())
    }

    def stopDevResourceServer() = {
      buildProcess.foreach(_.destroy())
      buildProcess = None
    }

    override def beforeStarted() = startDevResourceServer()
    override def afterStopped() = stopDevResourceServer()
    override def onError() = stopDevResourceServer()
  }
}
