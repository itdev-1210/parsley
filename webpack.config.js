const webpack = require('webpack');
const childProcess = require('child_process');
const HashOutputPlugin = require('webpack-plugin-hash-output');


function makeBuildTag() {
  // a rough equivalent to script/lib/imgutil.py:make_img_tag
  if (process.env.BUILD_VCS_NUMBER) {
    // want a git hash instead of build number because it's opaque to user
    // but can't use git commands because teamcity runs in a directory tree without git repo stuff
    return process.env.BUILD_VCS_NUMBER.substr(0, 8);
  } else {
    const headCommitHash = childProcess.execSync('git rev-parse --short HEAD').toString().trim();
    let hasDiffs = false;
    try {
      childProcess.execSync('git diff-index --quiet HEAD'); // throws if exit code non-zero ie diffs exist
    } catch (e) {
      // inside the docker image, don't have the necessary git setup to use git stash create, so just indicate
      // that diffs exist
      hasDiffs = true;
    }

    return headCommitHash + (hasDiffs ? "-M" : "");
  }
}

const
    assetsRoot = __dirname + "/app/assets/",
    env = process.env.NODE_ENV;


const targetBrowsers = [
    'Chrome', 'Edge', 'Firefox', 'Opera',
].map(browser => `last 2 ${browser} versions`);
targetBrowsers.push("safari >= 10");

const config = {
  target: 'web',
  entry: {
    app: assetsRoot + "app/main",
    auth: assetsRoot + "auth/login",
    plan: assetsRoot + "plan/plan",
  },
  output: {
    filename: 'js/[name].bundle.js',
    devtoolModuleFilenameTemplate: info => '/' + info.resourcePath.replace('./', '').replace('app/assets/', '../'),
    hashFunction: 'md5',
    hashDigestLength: 32,
  },
  resolve: {
    alias: {
      'common': assetsRoot + "common",
      'react-select': '@parsley-oss/react-select',
      'react-bootstrap-date-picker': '@parsley-oss/react-bootstrap-date-picker',
    },
    extensions: [".js", ".jsx"],
  },
  plugins: [
    new webpack.DefinePlugin({
      // NB: DefinePlugin substitutes code fragments, not JS values
      "process.env.NODE_ENV": `"${env}"`,
      // reflects id AT THE TIME WEBPACK WAS STARTED. not really useful for dev webpack server
      "GIT_ID": `"${makeBuildTag()}"`
    }),
    // use HashOutputPlugin because Play's versioned assets controller requires
    // the cachebusting tag to actually be the md5 hash of the file
    new HashOutputPlugin(),
  ],
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /\.(jsx|js)$/,
        options: {
          presets: [
            '@babel/preset-react',
            ['@babel/preset-env', {
              'targets': {
                'browsers': targetBrowsers
              },
            }],
          ],
          plugins: [
            ['@babel/plugin-proposal-decorators', {
              // TODO find changelog from legacy to new proposal
              'legacy': true,
            }],
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-transform-flow-strip-types',
          ],
          cacheDirectory: true,
        },
      },
    ],
  },
  devtool: "cheap-module-eval-source-map",
};

if (env == "production") {
  config.devtool = "nosources-source-map";

  // cachebusting use of name
  config.output.filename = 'js/[chunkhash]-[name].bundle.js';
  // webpack-hash-output-plugin doesn't support sourcemaps, because of cyclic
  // dependency issues. that's fine, because this is only consumed by developers
  // who know how to check the "disable caches" box
  config.output.sourceMapFilename = 'js/[name].bundle.js.map';
}


module.exports = config;
